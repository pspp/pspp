
#ifndef MANACAM_TOOLS_CONSTEXPR_STR_CREATRO_HPP
#define MANACAM_TOOLS_CONSTEXPR_STR_CREATRO_HPP

#include <utility>
#include <string_view>
#include <concepts>

#include <pspp/utility/utility.hpp>

#include <pspp/constr/constr_impl.hpp>
#include <pspp/constr/constr_numbers_helpers.hpp>


namespace pspp
{

template <std::size_t _t_size>
inline constexpr std::size_t get_const_chars_len(char const (&)[_t_size])
{
	return _t_size - 1;
}

template <auto first, auto ...rest>
constexpr auto get_first()
{
	return first;
}

template <auto first, auto ...rest>
constexpr void for_each(auto to_do)
{
	to_do.template operator()<first>();
  (..., to_do.template operator()<rest>());
}

template <auto first, auto ...rest>
constexpr std::size_t get_total_len(auto get_len_lambda)
{
  return (get_len_lambda.template operator()<first>() + ... + get_len_lambda.template operator()<rest>());
}

template <std::size_t _t_l>
inline constexpr constr<_t_l> to_constr(constr<_t_l> data)
{
	return data;
}

template <std::size_t _t_l>
inline constexpr constr<_t_l - 1> to_constr(char const (&data)[_t_l])
{
	return constr<_t_l - 1>(data);
}

template <typename T>
concept not_char_integral = requires()
{
	requires c_integral<T> and (not std::is_same_v<char, T>);
};

template <typename T>
concept to_constr_convertiable = requires (T t)
{
	t.to_constr();
	requires is_constr(t);
};

template <auto val>
inline constexpr auto to_constr()
{
	using T = decltype(val);
	if constexpr (c_constr<T>)
		return val;
	else if constexpr (std::is_same_v<char, T>)
	{ return constr{val}; }
	else if constexpr (is_num_value_with_min_max(val))
		return to_str<typename T::val_t, val._m_val, base::dec, T::min, T::max>();
	else if constexpr (is_num_with_base(val))
		return to_str<typename T::val_t, val._m_val, val.get_base()>();
	else if constexpr (c_integral<T> )
		return to_str<T, val>();
	else 
	{
		static_assert(to_constr_convertiable<T>);
		return val.to_constr();
	}
}

template <auto first, auto second, auto... rest>
constexpr auto to_constr()
{
	constexpr auto caller = []<auto t_data>()
	{
		using data_t = decltype(t_data);

		if constexpr (c_integral<data_t> or std::same_as<data_t, char> or
				is_num_value_with_min_max(t_data) or is_num_with_base(t_data)
				)
			return to_constr<t_data>();
		else
			return to_constr(t_data);
	};

	return concat_constr(caller.template operator()<first>(), 
			caller.template operator()<second>(), caller.template operator()<rest>()...);
}

template <auto first, auto ... rest>
inline constexpr auto to_constr_v = to_constr<first, empty_cstr, rest...>();

} // namespace pspp

#endif // MANACAM_TOOLS_CONSTEXPR_STR_CREATRO_HPP

