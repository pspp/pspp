
#ifndef MANACAM_TOOLS_CONSTEXPR_NUM_HELPER_HPP
#define MANACAM_TOOLS_CONSTEXPR_NUM_HELPER_HPP


#include <array>
#include <utility>
#include <numeric>
#include <concepts>
#include <cstdint>
#include <string_view>

#include <pspp/constr/constr_impl.hpp>
#include <pspp/constr/num_bases.hpp>

namespace pspp
{
	template <typename T>
	concept numeric = requires(T t)
	{
		static_cast<int>(t);
	};

  template <std::size_t _t_val>
  struct prepared_num_str
  {
    static_prop_of constr value = empty_cstr;
  };

  template <>
  struct prepared_num_str<npos>
  { static_prop_of constr value = "18446744073709551615"; };

  template <auto t_val>
  inline constexpr constr prepared_num_str_v = prepared_num_str<t_val>::value;


	template <auto _t_val, base _t_base>
	inline constexpr std::size_t get_str_res_len()
	{
		decltype(_t_val) tmp {1};
		std::size_t res {0};
		while (_t_val / tmp)
		{
			res++;
			tmp *= static_cast<decltype(_t_val)>(_t_base);
		}

		return res;
	}

	template <typename T, T val, std::size_t power>
	constexpr T pow()
	{
		if constexpr (power == 0)
			return 1;
		else
		{
			T res {val};

			for (std::size_t i = 1; i < power; i++)
				res *= val;

			return res;
		}
	}

	template <typename T, T _t_val, base _t_base = base::dec, T min_val = std::numeric_limits<T>::min(), T max_val = std::numeric_limits<T>::max()>
	constexpr auto to_str()
	{
		// XXX: add support for minus values

		static_assert (_t_val <= max_val, "const_num::to_str: _t_val must be less or equal max_val");
		static_assert (_t_val >= min_val, "const_num::to_str: _t_val must be greater or equal min_val");

		constexpr char char_vals[]{"0123456789abcdef"};
		if constexpr (_t_val < static_cast<T>(_t_base) and _t_val >= 0)
		{
			constr<2> res;
			res._m_data[0] = char_vals[_t_val];
			res._m_data[1] = 0;

			return res;
		}
		else
		{
      if constexpr (not prepared_num_str_v<_t_val>.empty())
        return prepared_num_str_v<_t_val>;
      else
      {
        constr<get_str_res_len<_t_val, _t_base>()> res{};

        T tmp {pow<T, static_cast<T>(_t_base), res.size() - 2>()};
        T tmp_val = _t_val;

        for (std::size_t i = 0; i < res.size() - 1 ; i++)
        {
          int rest = tmp_val / tmp;
          tmp_val = tmp_val % tmp;
          res._m_val[i] = char_vals[rest];

          tmp /= static_cast<T>(_t_base);
        }

        res.escape();
        return res;
      }
		}
	}


	template <typename ValT, ValT _t_min, ValT _t_max>
	struct num_value_with_min_max_t
	{
		using val_t = ValT;
		inline static constexpr ValT max = _t_max;
		inline static constexpr ValT min = _t_min;

		ValT _m_val;

		template <base base>
		inline constexpr std::size_t get_str_res_len() const
		{
			uint128_t tmp {1};
			std::size_t res {0};
			while (static_cast<uint128_t>(_m_val)  / tmp)
			{
				res++;
				tmp *= static_cast<uint128_t>(base);
			}

			return res;
		}

		constexpr num_value_with_min_max_t(ValT val):
			_m_val{val}
		{}

		void assert_bounds()
		{
			if (_m_val > max)
				_m_val = max;
			else if (_m_val < min)
				_m_val = min;
		}
	};

	template <typename T>
	concept constexpr_stringizable_num = requires (T val)
	{
		typename T::val_t;
		requires numeric<typename T::val_t>;

		T::max;
		T::min;

		requires std::is_same_v<typename T::val_t, decltype(T::max)>;
		requires std::is_same_v<typename T::val_t, decltype(T::max)>;
	};

	template <typename T, T min, T max>
	constexpr bool is_num_value_with_min_max([[maybe_unused]] num_value_with_min_max_t<T, min, max>)
	{
		return true;
	}

	constexpr bool is_num_value_with_min_max([[maybe_unused]] auto)
	{
		return false;
	}

	typedef num_value_with_min_max_t<std::size_t, 1, 2048> at_max_sendable_len;

	template <typename T, base _t_base>
		requires std::is_integral_v<T>
	struct num_with_base
	{
		using val_t = T;
		T _m_val;

		inline static constexpr base get_base()
		{
			return _t_base;
		}
	};


	template <typename T, base _t_base>
		requires std::is_integral_v<T>
	inline constexpr bool is_num_with_base(num_with_base<T, _t_base>)
	{
		return true;
	}
	inline constexpr bool is_num_with_base(auto)
	{
		return false;
	}

} // namespace pspp

#endif // MANACAM_TOOLS_CONSTEXPR_NUM_HELPER_HPP

