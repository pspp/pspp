#ifndef PSPP_CONSTEXPR_STR_IMPL_HPP
#define PSPP_CONSTEXPR_STR_IMPL_HPP

#include <utility>
#include <cstdint>
#include <string>
#include <string_view>
#include <pspp/useful/sequence.hpp>
#include <pspp/meta.hpp>


namespace pspp
{

namespace
{
}

template <std::size_t t_size>
struct constr
{
	char _m_data[t_size + 1];

	constexpr constr(char const (&str)[t_size])
	{
		for (std::size_t i = 0; i < t_size; i++)
			_m_data[i] = str[i];
	}

  constexpr constr(std::string_view str)
  {
    for (std::size_t i = 0; i < t_size - 1; i++)
      _m_data[i] = str[i];
    _m_data[t_size] = '\0';
  }

  constexpr constr(std::array<char, t_size> && data)
  {
    for (std::size_t i = 0; i < t_size; i++)
      _m_data[i] = data[i];

    _m_data[t_size] = '\0';
  }

  template <std::ranges::range RangeT>
    requires (not c_same_as<RangeT, char const *>)
  constexpr constr(RangeT && range) noexcept
  {
    for (std::size_t i = 0; char const c : range)
      _m_data[i++] = c;

    _m_data[t_size] = '\0';
  }

  template <c_same_as<char> CharT>
  constexpr constr(CharT c) :
    _m_data{c, '\0'}
  {}

  constexpr void escape() noexcept
  { _m_data[t_size] = '\0'; }

  template <std::size_t t_n, template <std::size_t> class HolderT, c_same_as<char> CharT>
  constexpr constr(CharT c, HolderT<t_n>) :
    _m_data{}
  {
    for (char & c_: _m_data)
      c_ = c;

    escape();
  }

  template <std::size_t t_j, std::size_t t_n, template <std::size_t> class HolderT>
  constexpr constr(constr<t_j> const & init_val, HolderT<t_n>) :
    _m_data{}
  {
    static constexpr std::size_t init_length = t_j;

    for (std::size_t _i = 0ul; _i < t_n; ++_i)
      _m_data[_i] = init_val[_i % init_length];

    escape();
  }

	constexpr constr() {}

  [[nodiscard]]
  inline constexpr char * begin() noexcept
  { return _m_data; }

  [[nodiscard]]
  inline constexpr char const * begin() const noexcept
  { return _m_data; }

  [[nodiscard]]
  inline constexpr char * end() noexcept
  { return _m_data + t_size; }

  [[nodiscard]]
  inline constexpr char const * end() const noexcept
  { return _m_data + t_size; }

  [[nodiscard]]
	inline constexpr std::size_t size() const noexcept
	{ return t_size; }

  [[nodiscard]]
	inline constexpr std::int64_t ssize() const noexcept
	{ return static_cast<std::int64_t>(t_size); }

  [[nodiscard]]
  inline constexpr std::size_t length() const noexcept
  { return size(); }

  [[nodiscard]]
  inline constexpr std::int64_t slength() const noexcept
  { return length(); }

  [[nodiscard]]
	inline constexpr std::string_view strv() const noexcept
	{ return {_m_data, t_size}; }

  [[nodiscard]]
  inline constexpr std::string str() const noexcept
  { return {_m_data, t_size}; }

  [[nodiscard]]
	inline constexpr operator std::string_view() const noexcept
	{ return strv(); }

  [[nodiscard]]
  inline constexpr char * data() noexcept
  { return _m_data; }

  [[nodiscard]]
	inline constexpr char const * data() const noexcept
	{ return _m_data; }

  [[nodiscard]]
	inline constexpr bool empty() const noexcept
	{
		return t_size == 0;
	}

  template <typename PosT>
  inline constexpr char operator[](PosT && pos) const noexcept
  { return _m_data[static_cast<std::size_t>(std::forward<PosT>(pos))]; }

  inline constexpr bool operator==(char const (&str)[t_size + 1]) const noexcept
  {
    for (std::size_t i = 0; i < t_size + 1; ++i)
      if (str[i] != _m_data[i]) return false;

    return true;
  }

  template <typename PosT>
  inline constexpr char & operator[](PosT && pos) noexcept
  { return _m_data[static_cast<std::size_t>(std::forward<PosT>(pos))]; }


  template <std::size_t t_other_n>
    requires (t_other_n != t_size + 1)
  inline constexpr bool operator==(char const (&)[t_other_n]) const noexcept
  { return false; }

  inline constexpr bool operator==(constr<t_size> const & other) const noexcept
  {
    return
    [this, &other]<std::size_t ... t_is>(std::index_sequence<t_is...>)
    { return (false + ... + (_m_data[t_is] == other._m_data[t_is])); }
    (std::make_index_sequence<t_size>{});
  }

  inline constexpr bool operator==(char c) const noexcept
  {
    if constexpr (t_size != 1) return false;
    else return _m_data[0] == c;
  }

  template <std::size_t t_other_n>
    requires (t_other_n != t_size + 1)
  inline constexpr bool operator==(constr<t_other_n> const &) const noexcept
  { return false; }

  template <std::size_t t_other_n>
    requires (t_other_n != t_size + 1)
  inline constexpr bool operator!=(char const (& str)[t_other_n]) const noexcept
  { return not operator==(str); }

  template <std::size_t t_other_n>
    requires (t_other_n != t_size + 1)
  inline constexpr bool operator!=(constr<t_other_n> const & other) const noexcept
  { return not operator==(other); }

  inline constexpr bool operator!=(char c) const noexcept
  { return not operator==(c); }

};

template <std::size_t t_len>
constr(char const (&val)[t_len]) -> constr<t_len - 1>;

template <std::size_t t_len, template <std::size_t> class HolderT>
constr(c_same_as<char> auto, HolderT<t_len>) -> constr<t_len>;

template <std::size_t t_init_len, std::size_t t_len, template <std::size_t> class HolderT>
constr(constr<t_init_len>, HolderT<t_len>) -> constr<t_len>;

constr(char const) -> constr<1>;


template <std::size_t _t_len>
inline constexpr bool is_constr(constr<_t_len>)
{
	return true;
}

template <typename T>
inline constexpr bool is_constr(T)
{
	return false;
}

template <typename FirstT, typename ... RestTs>
inline constexpr bool is_constrs(FirstT first, RestTs...rest)
{
	if constexpr (sizeof...(rest))
	{
		return is_constr(first) and is_constrs(rest...);
	}

	return is_constr(first);
}

template <std::size_t t_s1, std::size_t ... t_rest>
constexpr std::size_t len_sum()
{
  return (t_s1 + ... + t_rest);
}

template <std::size_t _t_l1, std::size_t _t_l2>
constexpr constr<_t_l1 + _t_l2> concat_two_constr(constr<_t_l1> s1, constr<_t_l2> s2)
{
	constr<_t_l1 + _t_l2> res{};

	std::size_t i = 0;

	for (;i < _t_l1; i++)
		res._m_data[i] = s1._m_data[i];
	for (;i < _t_l1 + _t_l2; i++)
		res._m_data[i] = s2._m_data[i - _t_l1];

  res.escape();

	return res;
}

template <std::size_t t_n1, std::size_t t_n2>
constexpr auto operator+(constr<t_n1> const & first, constr<t_n2> const & second) noexcept
{
  return concat_two_constr(first, second);
}

template <std::size_t t_n1, std::size_t t_n2>
constexpr auto operator+(constr<t_n1> const & first, char const (&second)[t_n2]) noexcept
{ return concat_constr(first, constr{second}); }

template <std::size_t t_n1, std::size_t t_n2>
constexpr auto operator+(char const (&first)[t_n2], constr<t_n2> const & second) noexcept
-> constr<t_n1 + t_n2 - 1>
{ return concat_constr(constr{first}, constr{second}); }


template <constr t_to_check, constr t_if_empty>
inline constexpr auto default_if_empty = []{
  if constexpr (t_to_check.empty())
    return t_if_empty;
  else return t_to_check;
}();

template <std::size_t t_n>
constexpr auto operator+(char const c, constr<t_n> const & cstr) noexcept -> constr<t_n + 1>
{ return concat_constr(constr{c}, cstr); }

template <std::size_t t_n>
constexpr auto operator+(constr<t_n> const & cstr, char const c) -> constr<t_n + 1>
{ return concat_constr(cstr, constr{c}); }



template <std::size_t t_l1, std::size_t t_l2, std::size_t ... t_lrest>
constexpr auto concat_constr(constr<t_l1> s1, constr<t_l2> s2, constr<t_lrest>... rest) ->
	constr<((t_l1 + t_l2) + ... + t_lrest)>
{
    return ((s1 + s2) + ... + t_lrest);
}


template <std::size_t t_size>
inline constexpr constr<t_size> cst(char const (&str)[t_size])
{
	return constr{str};
}

#ifndef __clang__
	template <std::size_t N>
	using cstr = constr<N>;
#endif

template <typename MaybeConstStr>
concept c_constr = is_constr(MaybeConstStr{});

inline constexpr constr empty_cstr{""};

template <constr t_str>
struct cstr_holder : value_holder<t_str>
{
	inline static constexpr std::string_view strv = t_str.strv();
	inline static consteval auto cstr() noexcept { return t_str; }
	inline static std::string str() noexcept { return std::string(t_str.strv()); }
	inline static consteval bool empty() noexcept
	{ return t_str.empty(); }

	consteval operator constr<t_str.size()>() const noexcept
	{ return t_str; }
};

template <constr ... t_strs>
struct cstr_sequence : value_sequence<t_strs...>
{
	using base = value_sequence<t_strs...>;

	template <std::size_t t_n>
	inline static constexpr std::string_view strv_at  
	{
		cstr_holder<base::template at<t_n>>::strv()
	};

	template <std::size_t t_n>
	inline static std::string str_at() noexcept
	{
		return std::string{strv_at<t_n>};
	}

	inline static constexpr bool is_every_unique = std::is_same_v<base, typename base::unique>;
};

template <typename T>
concept c_constr_sequence = c_complex_vtarget<T, cstr_sequence>;

template <constr t_str>
using constr_holder = cstr_holder<t_str>;

template <constr ... t_strs>
using constr_sequence = cstr_sequence<t_strs...>;

template <constr ... t_strs>
inline constexpr constr_sequence<t_strs...> cstrseq {};


inline namespace meta
{
	template <constr ... t_strs>
	struct is_value_sequence<cstr_sequence<t_strs...>> : std::true_type{};
	
	template <constr t_str>
	struct is_value_holder<cstr_holder<t_str>> : std::true_type{};

template <std::size_t t_result_length, constr t_padder = ' '>
struct padder_t
{ static_val value = t_padder; };

template <std::size_t t_result_length, constr t_padder, std::size_t t_current_length>
constexpr auto operator*(padder_t<t_result_length, t_padder>, constr<t_current_length> const & curr) noexcept
{
  if constexpr (t_current_length > t_result_length) return curr;
  else
    return constr{t_padder, val<t_result_length - t_current_length>} + curr;
}

template <std::size_t t_result_length, constr t_padder, std::size_t t_current_length>
constexpr auto operator*(constr<t_current_length> const & curr, padder_t<t_result_length, t_padder>) noexcept
{
  if constexpr (t_current_length > t_result_length) return curr;
  else
    return curr + constr{t_padder, val<t_result_length - t_current_length>};
}


template <std::size_t t_result_length, constr t_padder, std::size_t t_current_length>
constexpr auto operator+(constr<t_current_length> const & curr, padder_t<t_result_length, t_padder> p) noexcept
{ return curr * p; }

template <std::size_t t_result_length, constr t_padder, std::size_t t_current_length>
constexpr auto operator+(padder_t<t_result_length, t_padder> p, constr<t_current_length> const & curr) noexcept
{ return p * curr; }


template <std::size_t t_result_length, constr t_padder = constr{' '}>
inline constexpr padder_t<t_result_length, t_padder> padder {};

#ifndef PSPP_META_V1
	template <constr ... t_strs>
	struct is_sequence<cstr_sequence<t_strs...>> : std::true_type {};
#endif
} // namespace meta
} // namespace pspp

#include <pspp/constr/constr_literal.hpp>

namespace pspp {
	namespace constr_literal = pspp::cstr_literal;
}

#endif // PSPP_CONSTEXPR_STR_IMPL_HPP 
