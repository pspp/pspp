#ifndef PSPP_CONSTEXPR_STR_LITERAL_HPP
#define PSPP_CONSTEXPR_STR_LITERAL_HPP

#include <pspp/constr.hpp>

namespace pspp
{
namespace cstr_literal
{

template <char ... Chars>
constexpr auto to_str(std::integer_sequence<char, Chars...>)
{
	constexpr char const arr[sizeof...(Chars) + 1] {Chars...};
	return pspp::constr{arr};
}

template <constr t_res>
consteval auto operator"" _cs() noexcept
{ return t_res; }

template <constr t_res>
consteval auto operator"" _csh() noexcept
{ return constr_holder<t_res>{}; }

template <constr t_res>
consteval auto operator"" _c() noexcept
{ return value_holder<t_res>{}; }

} // namespace cstr_literal
} // namespace pspp

#endif // PSPP_CONSTEXPR_STR_LITERAL_HPP

