
#ifndef MANACAM_TOOLS_NUM_BASE_HPP
#define MANACAM_TOOLS_NUM_BASE_HPP

namespace pspp
{

enum class base : unsigned char
{
	bin = 2,
	dec = 10,
	hex = 16,
};

}
#endif // MANACAM_TOOLS_NUM_BASE_HPP
