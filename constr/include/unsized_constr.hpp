#ifndef PSPP_CNSTR_HPP
#define PSPP_CNSTR_HPP

#include <pspp/constr.hpp>
#include <pspp/unsized_array.hpp>

namespace pspp
{

struct unsized_constr : data_holder<char const>
{
	using base = data_holder<char const>;
	using base::m_data;
	using base::m_size;

	
	template <constr t_str>
	consteval unsized_constr(cstr_holder<t_str> data):
		data_holder<char const>{&data.value._m_data[0], t_str.size()}
	{}

	constexpr std::string_view strv() const noexcept
	{
		return {m_data, m_size};
	}

	constexpr bool operator==(std::string_view other) const noexcept
	{
		return other == strv();
	}

  constexpr bool operator==(char c) const noexcept
  { return m_size == 1 and m_data[0] == c; }
};

template <constr t_str>
inline constexpr unsized_constr make_unsized_constr = unsized_constr(cstr_holder<t_str>{});


template <constr t_first,
					constr ... t_rest>
struct template_unsized_constr_array
{
	using data_type = unsized_constr;
	using holders_seq = type_sequence<cstr_holder<t_first>, cstr_holder<t_rest>...>;

	inline static constexpr std::array<unsized_constr, sizeof...(t_rest) + 1> data
	{
			unsized_constr{cstr_holder<t_first>{}}, 
			unsized_constr{cstr_holder<t_rest>{}}...
	};

	template <constr t_first1, constr ... t_rest1>
	consteval template_unsized_constr_array<t_first, t_rest..., t_first1, t_rest1...>
		operator+(template_unsized_constr_array<t_first1, t_rest1...>) const noexcept
	{ return {}; }

  template <template <auto...> class ValsHolderT>
	consteval template_unsized_constr_array(ValsHolderT<t_first, t_rest...>) {}

  consteval template_unsized_constr_array() {}
};

template <auto t_first, auto ... t_rest, template <auto...> class ValsHolderT>
template_unsized_constr_array(ValsHolderT<t_first, t_rest...>) ->
	template_unsized_constr_array<t_first, t_rest...>;

template <constr t_first,
					constr ... t_rest>
struct is_template_array<template_unsized_constr_array<t_first, t_rest...>> : std::true_type {};

inline constexpr unsized_constr empty_unsized_constr = make_unsized_constr<empty_cstr>;
} // namespace pspp

#endif // PSPP_CNSTR_HPP

