#ifndef pspp_constexpr_str_hpp
#define pspp_constexpr_str_hpp

#include <pspp/constr/constr_impl.hpp>
#include <pspp/constr/constr_numbers_helpers.hpp>
#include <pspp/constr/constr_creator.hpp>

#endif // pspp_constexpr_str_hpp

