
#ifndef MANACAM_TOOLS_CONSTEXPR_STR_CREATRO_HPP
#define MANACAM_TOOLS_CONSTEXPR_STR_CREATRO_HPP

#include <utility>
#include <string_view>
#include <concepts>

#include "overload.hpp"
#include "constexpr_str.hpp"
#include "constexpr_numbers_helper.hpp"


namespace pspp::cnst 
{
inline namespace str
{

template <std::size_t _t_size>
inline constexpr std::size_t get_const_chars_len(char const (&)[_t_size])
{
	return _t_size;
}

template <auto first, auto ...rest>
constexpr auto get_first()
{
	return first;
}

template <auto first, auto ...rest>
constexpr void for_each(auto to_do)
{
	to_do.template operator()<first>();

	if constexpr (sizeof...(rest))
		for_each<rest...>();
}

template <auto first, auto ...rest>
constexpr std::size_t get_total_len(auto get_len_lambda)
{
	if constexpr (sizeof...(rest))
		return get_len_lambda.template operator()<first>() + get_total_len<rest...>(get_len_lambda);
	else
		return get_len_lambda.template operator()<first>();
}

template <std::size_t _t_l>
inline constexpr const_str<_t_l> to_const_str(const_str<_t_l> data)
{
	return data;
}

template <std::size_t _t_l>
inline constexpr const_str<_t_l> to_const_str(char const (&data)[_t_l])
{
	return const_str<_t_l>(data);
}

template <typename T>
concept not_char_integral = requires()
{
	requires std::is_integral_v<T> and (not std::is_same_v<char, T>);
};

template <typename T>
concept to_const_str_convertiable = requires (T t)
{
	t.to_const_str();
	requires is_const_str(t);
};

template <auto val>
inline constexpr auto to_const_str()
{
	using T = decltype(val);
	if constexpr (std::is_same_v<char, T>)
	{
		const_str<2> res{};
		res._m_val[0] = val;
		res._m_val[1] = 0;

		return res;
	}
	else if constexpr (num::is_num_value_with_min_max(val))
		return num::to_str<typename T::val_t, val._m_val, base::dec, T::min, T::max>();
	else if constexpr (num::is_num_with_base(val))
		return num::to_str<typename T::val_t, val._m_val, val.get_base()>();
	else if constexpr (std::is_integral_v<T>)
		return num::to_str<T, val>();
	else 
	{
		static_assert(to_const_str_convertiable<T>);
		return val.to_const_str();
	}
}

template <auto first, auto second, auto... rest>
constexpr auto to_const_str()
{
	/*using get_len_lambda_t = decltype([]<auto data_>(){
				using data_t = decltype(data_);
				if constexpr (std::is_integral_v<data_t>)
					return num::get_str_res_len<data_, num::base::dec>();
				else if constexpr (num::is_num_with_base(data_))
					return num::get_str_res_len<data_._m_val, data_t::get_base()>();
				else if constexpr (is_const_str(data_))
					return data_.size();
				else if constexpr (num::is_num_value_with_min_max(data_))
					return num::get_str_res_len<data_._m_val, num::base::dec>();
				else if constexpr (is_const_chars(data_))
					return get_const_chars_len(data_) - 1;
				else 
					return 0;
			});
			*/

	constexpr auto caller = []<auto data_>()
	{
		using data_t = decltype(data_);

		if constexpr (std::is_integral_v<data_t> or std::is_same_v<data_t, char> or
				num::is_num_value_with_min_max(data_) or num::is_num_with_base(data_)
				)
			return to_const_str<data_>();
		else
			return to_const_str(data_);
	};

	return concat_const_str(caller.template operator()<first>(), 
			caller.template operator()<second>(), caller.template operator()<rest>()...);
}

} //namespace str
} //inline namespace pspp::cnst

#endif // MANACAM_TOOLS_CONSTEXPR_STR_CREATRO_HPP

