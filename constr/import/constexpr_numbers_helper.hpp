
#ifndef MANACAM_TOOLS_CONSTEXPR_NUM_HELPER_HPP
#define MANACAM_TOOLS_CONSTEXPR_NUM_HELPER_HPP


#include <array>
#include <utility>
#include <numeric>
#include <concepts>
#include <cstdint>
#include <string_view>

#include "constexpr_str.hpp"
#include "num_bases.hpp"

namespace pspp::cnst::num
{
	template <typename T>
	concept numeric = requires(T t)
	{
		static_cast<int>(t);
	};

	
	template <auto _t_val, base _t_base>
	inline constexpr std::size_t get_str_res_len()
	{
		decltype(_t_val) tmp {1};
		std::size_t res {0};
		while (_t_val / tmp)
		{
			res++;
			tmp *= static_cast<decltype(_t_val)>(_t_base);
		}

		return res + 1;
	}

	template <typename T, T val, std::size_t power>
	constexpr T pow()
	{
		if constexpr (power == 0)
			return 1;
		else
		{
			T res {val};

			for (std::size_t i = 1; i < power; i++)
				res *= val;

			return res;
		}
	}

	template <typename T, T _t_val, base _t_base = base::dec, T min_val = std::numeric_limits<T>::min(), T max_val = std::numeric_limits<T>::max()>
	constexpr auto to_str()
	{
		static_assert (_t_val <= max_val, "const_num::to_str: _t_val must be less or equal max_val");
		static_assert (_t_val >= min_val, "const_num::to_str: _t_val must be greater or equal min_val");

		constexpr char char_vals[]{"0123456789abcdef"};
		str::const_str<get_str_res_len<_t_val, _t_base>()> res{};

		T tmp {pow<T, static_cast<T>(_t_base), res.size() - 2>()};
		T tmp_val = _t_val;

		for (std::size_t i = 0; i < res.size() - 1 ; i++)
		{
			int rest = tmp_val / tmp;
			tmp_val = tmp_val % tmp;
			res._m_val[i] = char_vals[rest];

			tmp /= static_cast<T>(_t_base);
		}
		res._m_val[res.size() - 1] = 0;

		return res;
	}


	template <typename ValT, ValT _t_min, ValT _t_max>
	struct num_value_with_min_max_t
	{
		using val_t = ValT;
		inline static constexpr ValT max = _t_max;
		inline static constexpr ValT min = _t_min;

		ValT _m_val;

		template <base base>
		inline constexpr std::size_t get_str_res_len() const
		{
			ValT tmp {1};
			std::size_t res {0};
			while (_m_val / tmp)
			{
				res++;
				tmp *= static_cast<ValT>(base);
			}

			return res;
		}

		constexpr num_value_with_min_max_t(ValT val):
			_m_val{val}
		{}

		void assert_bounds()
		{
			if (_m_val > max)
				_m_val = max;
			else if (_m_val < min)
				_m_val = min;
		}
	};

	template <typename T>
	concept constexpr_stringizable_num = requires (T val)
	{
		typename T::val_t;
		requires numeric<typename T::val_t>;

		T::max;
		T::min;

		requires std::is_same_v<typename T::val_t, decltype(T::max)>;
		requires std::is_same_v<typename T::val_t, decltype(T::max)>;
	};

	template <typename T, T min, T max>
	constexpr bool is_num_value_with_min_max([[maybe_unused]] num_value_with_min_max_t<T, min, max>)
	{
		return true;
	}

	constexpr bool is_num_value_with_min_max([[maybe_unused]] auto)
	{
		return false;
	}

	using at_baudrate = num_value_with_min_max_t<std::size_t, 110, 40*115200>;
	using at_databits = num_value_with_min_max_t<std::uint8_t, 5, 8>;
	using at_stopbits = num_value_with_min_max_t<std::uint8_t, 1, 3>;
	using at_parity   = num_value_with_min_max_t<std::uint8_t, 0, 2>;
	using at_flow_control = num_value_with_min_max_t<std::uint8_t, 0, 3>;

	typedef num_value_with_min_max_t<std::size_t, 1, 2048> at_max_sendable_len; 

	template <typename T, base _t_base>
		requires std::is_integral_v<T>
	struct num_with_base
	{
		using val_t = T;
		T _m_val;

		inline static constexpr base get_base()
		{
			return _t_base;
		}
	};


	template <typename T, base _t_base>
		requires std::is_integral_v<T>
	inline constexpr bool is_num_with_base(num_with_base<T, _t_base>)
	{
		return true;
	}
	inline constexpr bool is_num_with_base(auto)
	{
		return false;
	}

} // namespace cnst::num

#endif // MANACAM_TOOLS_CONSTEXPR_NUM_HELPER_HPP

