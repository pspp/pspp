export module pspp.constexpr_str;

import <utility>;
import <cstdint>;
import <string_view>;

export namespace pspp::cnst
{
	inline namespace str
	{
#define cstr const_str

		template <std::size_t _t_size>
		struct const_str
		{
			char _m_val[_t_size];
			inline constexpr std::size_t size() const
			{
				return _t_size;
			}
			inline constexpr std::int64_t ssize() const
			{
				return static_cast<std::int64_t>(_t_size);
			}

			inline constexpr std::string_view strv() const
			{
				return {_m_val, _t_size - 1};
			}

			inline constexpr operator std::string_view() const
			{
				return strv();
			}
			inline constexpr char const * data() const
			{
				return _m_val;
			}

			inline constexpr bool empty() const
			{
				return _t_size == 1 and _m_val[0] == '\0';
			}
		};

		template <std::size_t _t_len>
		const_str(char const (&val)[_t_len]) -> const_str<_t_len>;

		template <std::size_t _t_len>
		inline constexpr bool is_const_str(const_str<_t_len>)
		{
			return true;
		}

		inline constexpr bool is_const_str(auto)
		{
			return false;
		}

		inline constexpr bool is_const_strs(auto first, auto ...rest)
		{
			if constexpr (sizeof...(rest))
			{
				return is_const_str(first) and is_const_strs(rest...);
			}

			return is_const_str(first);
		}

		template <std::size_t _t_s1, std::size_t ..._t_rest>
		constexpr std::size_t len_sum()
		{
			if constexpr (sizeof...(_t_rest))
				return _t_s1 + len_sum<_t_rest...>();
			else
				return _t_s1;
		}

		template <std::size_t _t_l1, std::size_t _t_l2>
		constexpr const_str<_t_l1 + _t_l2 -1> concat_two_const_str(const_str<_t_l1> s1, const_str<_t_l2> s2)
		{
			const_str<_t_l1 + _t_l2 - 1> res{};

			std::size_t i = 0;

			for (;i < _t_l1 - 1; i++)
				res._m_val[i] = s1._m_val[i];
			for (;i < _t_l1 - 1 + _t_l2; i++)
				res._m_val[i] = s2._m_val[i - _t_l1 + 1];

			return res;
		}

		template <std::size_t _t_l1, std::size_t _t_l2, std::size_t ..._t_lrest>
		constexpr auto concat_const_str(const_str<_t_l1> s1, const_str<_t_l2> s2, const_str<_t_lrest>... rest) ->
			const_str<len_sum<_t_l1, _t_l2, _t_lrest...>() - sizeof...(_t_lrest) - 1>
		{
			if constexpr (sizeof...(rest))
				return concat_const_str(concat_two_const_str(s1, s2), rest...);
			else
				return concat_two_const_str(s1, s2);
		}

		template <std::size_t _t_size>
		inline constexpr const_str<_t_size> cst(char const (&str)[_t_size])
		{
			return const_str{str};
		}

		template <std::size_t N>
		using cstr = const_str<N>;

		template <typename MaybeConstStr>
		concept c_const_str = is_const_str(MaybeConstStr{});

		inline constexpr const_str empty_cstr{""};
	}
}

