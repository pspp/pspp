#include <array>

#include <pspp/array.hpp>
#include <pspp/custom_struct.hpp>
#include <pspp/utility/iota_range.hpp>
#include <pspp/data_holder.hpp>
#include <pspp/flat_map.hpp>

namespace pspp
{

namespace detail::json
{

using string_view = data_holder<char const>;


enum struct pretokenizer_result_codes : int
{
  ok, unexpected_eof, unknown_character
};

enum struct token_type : int
{
  number, string, null, key, object, array, count
};

constexpr flat_map_with_default<
  token_type,
  std::string_view,
  static_cast<std::size_t>(token_type::count)
  > token_type_strs {
    std::pair{token_type::number, std::string_view{"number"}},
    {token_type::string, "string"},
    {token_type::null, "null"},
    {token_type::key, "key"},
    {token_type::object, "object"},
    {token_type::array, "array"},
    {token_type::count, "unknown"}
  };


struct token {
  token_type t;
  std::size_t count_of_sub_els;
  std::size_t offset;
  std::string_view str;
};

template <std::size_t t_size = npos>
struct pretokenizer_result
{
  pretokenizer_result_codes code;
  char error_c;
  std::conditional_t<is_npos_v<t_size>,
    std::size_t,
    std::array<std::string_view, is_npos_v<t_size> ? 0 : t_size>
  > result {};
};

template <std::size_t t_size, pretokenizer_result_codes t_code>
inline constexpr auto json_tokenizer_error = pretokenizer_result<t_size>{
  .code = t_code,
  .error_c = 0,
  .result {}
};

template <constr t_input>
constexpr auto pretokenize_str() noexcept
{
  static constexpr std::string_view raw_view = t_input.strv();
  static constexpr std::array specials = {'{', '}', '[', ']', ',', ':',};
  static constexpr std::array to_ignore = {' ', '\n', '\t', '\r'};

  static constexpr auto skip_comment = [](string_view & view) {
    return view.move_head_until('\n');
  };

  static constexpr auto skip_until_special = [](string_view & view) {
    return view.move_head_until([](char const * const __restrict c) {
      return std::ranges::contains(specials, *c) or std::ranges::contains(to_ignore, *c);
    });
  };

  static constexpr auto try_to_skip_null = [](string_view & view) {
    if (view.size() < 4) return false;
    else
    {
      if (
          view[0] == 'n' and
          view[1] == 'u' and
          view[2] == 'l' and
          view[3] == 'l'
          )
      {
        view.shrink_head<4>();
        return true;
      }
      else return false;
    }
  };

  static constexpr auto skip_to_next_quote = [](string_view & view) {
    view.shrink_head();
    return view.move_head_until([last='\0'](char const * const __restrict c) mutable{
      if (*c == '"')
      {
        if (last != '\\')
          return true;
      }

      last = *c;

      return false;
    });
  };

  constexpr auto tokenize_impl = []<std::size_t t_size = npos>{
    std::conditional_t<
        is_npos_v<t_size>,
        std::size_t,
        std::array<std::string_view, t_size>
    > res {};

    auto add_token = [&res, i=0](std::string_view token) mutable {
      if constexpr (is_npos_v<t_size>) ++res;
      else res[i++] = token;
    };


    data_holder<char const> view {raw_view};
    data_holder<char const> current {};

    char const * last_it = nullptr;

    using enum pretokenizer_result_codes;

    while(not view.empty())
    {
      char c = view.front();

      if (std::ranges::contains(specials, c))
      {
        add_token(std::string_view{view.data(), 1});
        view.shrink_head();
      }

      else if (c == '\"')
      {
        current = string_view(view.data(), 1);
        std::size_t const moved = skip_to_next_quote(view);

        if (moved == npos)
          return json_tokenizer_error<t_size, unexpected_eof>;

        current.expand_tail(moved + 1);
        view.shrink_head();

        add_token(current.as<std::string_view>());
        current.clear();
      }

      else if (is_integer_begin(c))
      {
        current = string_view(view.data(), 1);
        std::size_t const moved = skip_until_special(view);

        if (moved == npos)
          return json_tokenizer_error<t_size, unexpected_eof>;

        current.expand_tail(moved - 1);

        add_token(current.as<std::string_view>());
      }
      else if (std::ranges::contains(to_ignore, c))
        view.shrink_head();
      else if (c == '/')
      {
        if (view.size() > 1)
        {
          if (view[1] == '/')
            skip_comment(view);
          else view.shrink_head();
        }
        else view.shrink_head();
      }
      else
      {
        if (try_to_skip_null(view))
        {
          add_token(std::string_view{view.data() - 4, 4});
          continue;
        }
        else
        {
          pretokenizer_result tmp =
              json_tokenizer_error<t_size, unknown_character>;
          tmp.error_c = c;
          return tmp;
        }
      }

      if (last_it == view.data())
        return pretokenizer_result<t_size>{
          .code = unexpected_eof,
          .error_c = *view.data(),
          .result {}
        };
      else
        last_it = view.data();
    }

    return pretokenizer_result<t_size>{
      .code = ok,
      .error_c = 0,
      .result = res
    };
  };

  constexpr pretokenizer_result<> tokens_count = tokenize_impl();
  if constexpr (tokens_count.code == pretokenizer_result_codes::ok)
  {
    return tokenize_impl.template operator()<tokens_count.result>().result;
  }
  else
  {
    static_assert (static_cast<std::size_t>(tokens_count.code) == 0,
                   "an error occured while tokenizing json string");
    static_assert (tokens_count.error_c == 0);

    return nulltv;
  }
}

enum struct tokenize_error_codes
{ ok, no_closing_bracket, no_closing_curly, wrong_key, bad_comma };

template <typename T>
struct json_str
{
  template <auto t_size = 0>
  using type = T;
};

template <template <auto> class StrT>
struct json_sized_str
{
  template <auto t_size>
  using type = StrT<t_size>;
};


template <std::size_t t_size>
struct tokenize_result
{
  tokenize_error_codes code;
  std::string_view msg;
  std::array<token, t_size> result;
  std::size_t real_size = 0;
};


using tokens_view_type = data_holder<token>;
using real_offsets_type = std::vector<std::pair<std::size_t, std::size_t>>;


constexpr std::size_t set_sort_indexes(tokens_view_type & tokens,
                                       std::size_t to_skip,
                                       real_offsets_type & offs_storage) noexcept
{
  auto & front = tokens.pop_head();
  front.offset = to_skip;

  if (front.t <= token_type::null)
    return 1;
  else
  {
    std::size_t totally_added = 1;
    std::size_t total_obj_offset = 0;

    if (front.t == token_type::array)
    {
      for (auto i : iota_range(front.count_of_sub_els))
        if (tokens.front().t >= token_type::object)
        {
          auto & object_begin = tokens.front();

          std::size_t const obj_offset = to_skip + front.count_of_sub_els + total_obj_offset;

          total_obj_offset += set_sort_indexes(tokens, obj_offset, offs_storage);
          object_begin.offset = to_skip + i + 1;
          offs_storage.emplace_back(object_begin.offset, obj_offset + (object_begin.t == token_type::object));

        }
        else
          totally_added += set_sort_indexes(tokens, to_skip + i + 1, offs_storage);

      return totally_added + total_obj_offset;
    } else if (front.t == token_type::object)
    {
      std::size_t totally_added = 1;

      for (auto i : iota_range(1, front.count_of_sub_els + 1))
      {
        if (tokens.front().t == token_type::key)
        {
          auto & key = tokens.pop_head();
          key.offset = to_skip + i;
        }

        totally_added += set_sort_indexes(tokens, to_skip + front.count_of_sub_els + totally_added, offs_storage);
      }

      return totally_added + front.count_of_sub_els;
    }
  }
}

template <std::size_t t_n>
constexpr tokenize_result<t_n>
    postprocess_tokens(std::array<token, t_n> & res, std::size_t real_size) noexcept
{
  tokens_view_type view = tokens_view_type(res, real_size);
  real_offsets_type real_offsets {};

  set_sort_indexes(view, 0, real_offsets);
  //std::vector<std::pair<std::size_t, std::size_t>> real_offsets;

  for (auto i: iota_range(real_size))
    if (res[i].t == token_type::key)
    {
      res[i].count_of_sub_els =
          res[i + 1].count_of_sub_els +
          (res[i + 1].t >= token_type::object);

      if (res[i + 1].t == token_type::key)
        return {
          .code = tokenize_error_codes::wrong_key,
          .msg = "can not have two keys in row",
          .result = res,
          .real_size = real_size,
        };
      // res[i].str = res[i].str.substr(1, res[i].str.size() - 1);

      // save indexes of data begin
      real_offsets.emplace_back(res[i].offset, res[i + 1].offset);
    }

  std::ranges::sort(
      res | std::views::take(real_size),
      {}, &token::offset);

  std::ranges::for_each(real_offsets, [&res](auto v){
    res[v.first].offset = v.second;
    ++res[v.second].offset;
  });

  res[0].offset = 1;

  return {
    .code = tokenize_error_codes::ok,
    .msg = "",
    .result = res,
    .real_size = real_size,
  };
}


template <constr t_json_str>
consteval auto tokenize() noexcept
{
  static constexpr std::array pretokenized = pretokenize_str<t_json_str>();
  using result_t = tokenize_result<pretokenized.size()>;

  std::array<token, pretokenized.size()> res {};
  token dummy {};

  std::vector<std::pair<bool, std::size_t * const>> els_count {{false, &dummy.count_of_sub_els}};

  std::size_t expecting_brackets = 0;
  std::size_t expecting_curly = 0;

  token * previous = &dummy;
  token * current = res.data();

  bool last_was_comma = false;

  for (std::string_view const ptok : pretokenized)
  {
    current->str = ptok;
    current->count_of_sub_els = 1;

    switch(ptok[0])
    {
      case '"':
      {
        current->t = token_type::string;
        current->count_of_sub_els = 0;
        break;
      }
      case 'n':
      {
        if (ptok == "null")
          current->t = token_type::null;
        break;
      }
      case ':':
      {
        if (previous->t == token_type::string
            and els_count.back().first // parsing object
            )
          previous->t = token_type::key;
        else
          return result_t{
              .code = tokenize_error_codes::wrong_key,
              .msg = "",
              .result {}
          };

        auto last = els_count.back();
        *last.second += last.first;

        last_was_comma = false;
        continue;
      }
      case '{':
      {
        current->t = token_type::object;
        current->count_of_sub_els = 0;
        els_count.emplace_back(true, &current->count_of_sub_els);

        ++expecting_curly;
        break;
      }
      case '[':
      {
        current->t = token_type::array;
        current->count_of_sub_els = 0;
        els_count.emplace_back(false, &current->count_of_sub_els);

        ++expecting_brackets;
        break;
      }
      case '}':
      {
        els_count.pop_back();
        --expecting_curly;

        last_was_comma = false;
        continue;
      }
      case ']':
      {
        if (!last_was_comma)
          ++*els_count.back().second;

        els_count.pop_back();
        --expecting_brackets;

        last_was_comma = false;
        continue;
      }
      case ',':
      {
        if (
            previous->t == token_type::object or
            previous->t == token_type::array
            )
          return result_t{
              .code = tokenize_error_codes::bad_comma,
              .msg = "comma witout previous element",
              .result {}
          };

        auto last = els_count.back();

        *last.second += not last.first;
        last_was_comma = true;

        continue;
      }
    }

    previous = current;
    ++current;

    last_was_comma = false;
  }

  if (expecting_brackets + expecting_curly != 0)
    return result_t{
        .code = tokenize_error_codes::no_closing_bracket,
        .msg = "",
        .result = res
    };
  else
  {
    auto const real_size = current - res.data();
    return postprocess_tokens(res, real_size);
  }

}

template <typename DoubleT = long double, typename IntT = std::int64_t>
struct from_chars_to_double_res
{
  IntT first = 0;
  IntT second = 0;

  IntT e = 0;
  IntT second_degree = 0;

  static constexpr auto int_pow10(IntT degree, IntT start = 1) noexcept -> IntT
  {
    while (degree-- != 0)
      start *= 10;

    return start;
  };

  static constexpr auto double_pow10(IntT degree, DoubleT start = 1) noexcept -> DoubleT
  { return start / int_pow10(-degree); };

  pspp_always_inline [[nodiscard]]
  constexpr bool can_be_int() const noexcept
  { return second_degree >= 0; }

  pspp_always_inline [[nodiscard]]
  constexpr IntT as_int() const noexcept
  {
    return first * int_pow10(e) + second * int_pow10(second_degree);
  }

  constexpr DoubleT as_double() const noexcept
  {
    if (e > 0)
    {
      IntT true_first = first * int_pow10(e);
      DoubleT true_second = static_cast<DoubleT>(second) * double_pow10(second_degree);

      return static_cast<DoubleT>(true_first) + true_second;
    }
    else
    {
      return
        static_cast<DoubleT>(first) * double_pow10(e) +
        static_cast<DoubleT>(second) * double_pow10(second_degree);
    }
  }

};

template <typename DoubleT = long double, typename IntT = std::int64_t>
[[nodiscard]]
constexpr
  std::pair<
      from_chars_to_double_res<DoubleT, IntT>,
      std::string_view
  >
  from_chars_to_double(std::string_view str) noexcept
{
  std::size_t e_count = 0;

  auto e_splited = str | std::views::split('e');

  auto e_first = e_splited.begin();
  auto e_second = e_first;
  auto e_end = e_splited.end();

  ++e_second;

  auto dot_splited = *e_first | std::views::split('.');
  auto first_part = std::string_view(*dot_splited.begin());

  IntT num = 0;
  IntT second_num = 0;
  IntT second_degree = 0;
  IntT first_degree = 0;

  {
    auto [_, ec] = std::from_chars(first_part.begin(), first_part.end(), num);

    if (ec != std::errc{})
      return {{}, "error while converting first part of double"};
  }

  auto second_part = std::string_view(*++dot_splited.begin());
  {
    auto [_, ec] = std::from_chars(second_part.begin(), second_part.end(), second_num);

    if (ec != std::errc{})
      return {{}, "error while converting second part of double"};
  }

  {
    IntT second_num_cp = second_num;
    while (second_num_cp > 0)
    {
      second_num_cp /= 10;
      --second_degree;
    }
  }

  if (e_second != e_end)
  {
    if (++auto{e_second} != e_splited.end())
    return {{}, "error: two or more `e' in number"};

    auto e_str = std::string_view(*e_second);

    IntT e;
    auto [_, ec] = std::from_chars(e_str.begin(), e_str.end(), e);

    if (ec != std::errc{})
      return {{}, "error: could not convert e value"};

    int incrementor = -1 + 2 * (e > 0); // if e > 0: incrementor = 1
    first_degree = e;
    while (e != 0)
    {
      second_degree += incrementor;
      e -= incrementor;
    }
  }

  return {{
    .first = num,
    .second = second_num,
    .e = first_degree,
    .second_degree = second_degree,
    }, ""};
}

template <from_chars_to_double_res t_res>
consteval auto get_double() noexcept
{
  if constexpr (t_res.can_be_int())
    return t_res.as_int();
  else return t_res.as_double();
}

template <
    auto t_get_token_at, std::size_t t_pos,
    auto t_json_str_ind = json_sized_str<constr>{}
    >
consteval auto reformat_string() noexcept
{
  static constexpr token t = t_get_token_at(t_pos);
  static constexpr std::string_view str = t.str.substr(1, t.str.size() - 2);

  using string_t = typename decltype(t_json_str_ind)::template type<str.size() + 1>;

  static constexpr std::size_t count = std::ranges::count(str, '\\');
  if constexpr (count == 0)
  {
    if constexpr (c_constr<string_t>)
      return constr<str.size() + 1>(str);
    else return str;
  }
  else
  {
    static constexpr std::array<std::string_view, count + 1> splitted = []
    {
      std::array<std::string_view, count + 1> res { str };
      for (std::size_t i = 0; i < count; ++i)
      {
        std::size_t pos = res[i].find_first_of('\\');
        res[i + 1] = res[i].substr(pos + 1);
        res[i] = res[i].substr(0, pos);
      }
      return res;
    }();


    static constexpr constr res_str =
      []<std::size_t ... t_is>(std::index_sequence<t_is...>)
      {
        constexpr std::string_view first = splitted[0];
        return (
            constr<first.size() + 1>(first) + ... +
            []<std::size_t t_i>
            {
              constexpr auto istr = splitted[t_i + 1];
              static constexpr auto is_last = t_i == count - 1;

              if constexpr (istr[0] == '\n')
                return constr<istr.size() - is_last>(istr.substr(1));
              else return constr<istr.size() + 2 - is_last>(
                  std::string_view{istr.data() - 1, istr.size() + 1});
            }.template operator()<t_is>()
            );
      }(std::make_index_sequence<count>{});

    if constexpr (c_constr<string_t>)
      return res_str;
    else return string_t{res_str.strv()};
  }
}

template <auto t_get_token_at, std::size_t t_pos, auto t_str_ind>
consteval auto parse_json_part_at(value_holder<t_pos> = {}) noexcept
{
  static constexpr token tok = t_get_token_at(t_pos);
  static constexpr token_type tt = tok.t;

  using enum token_type;

  if constexpr (tt == object)
  {
    return []<std::size_t ... t_is>(std::index_sequence<t_is...>)
    {
      return custom_struct{
          []<std::size_t t_i>{
            static constexpr token tkey = t_get_token_at(tok.offset + t_i);

            return namedval<
                reformat_string<t_get_token_at, tok.offset + t_i>()
            >(parse_json_part_at<t_get_token_at, tkey.offset, t_str_ind>());
          }.template operator()<t_is>()...
      };
    }(std::make_index_sequence<tok.count_of_sub_els>{});
  } else if constexpr (tt == array)
  {
    return []<std::size_t ... t_is>(std::index_sequence<t_is...>)
    {
      static constexpr auto res = custom_struct{
        []<std::size_t t_i>
        {
          return namedval<t_i>(parse_json_part_at<t_get_token_at, tok.offset + t_i, t_str_ind>());
        }.template operator()<t_is>()...
      };

      if constexpr ((res.types | make_unique | get_size) == 1)
        return ::pspp::array{
          res.template get<t_is>()...
        };
      else
        return res;
    }(std::make_index_sequence<tok.count_of_sub_els>{});
  } else if constexpr (tt == key)
  {
    return namedval<reformat_string<t_get_token_at, t_pos>()>(
        parse_json_part_at<t_get_token_at, tok.offset, t_str_ind>()
    );
  } else if constexpr (tt == null)
  {
    return nullptr;
  } else if constexpr (tt == string)
  {
    return reformat_string<t_get_token_at, t_pos, t_str_ind>();
  } else if constexpr (tt == number)
  {
    // TODO: replace to std::double64_t when possible (C++26)
    if constexpr (not tok.str.contains('.'))
    {
      constexpr auto res = []{
        std::int64_t res;

        auto tmp = std::from_chars(
            tok.str.begin(),
            tok.str.end(),
            res
        );
        return std::pair{tmp.ec, res};
      }();

      // TODO: better error msg
      static_assert(res.first == std::errc{}, "could not convert str to integer");
      return res.second;
    }
    else
    {
      static constexpr auto res = from_chars_to_double(tok.str);
      static_assert(res.second.empty(), "could not convert str to double");

      return get_double<res.first>();
    }
  } else
  {
    static_assert(pspp_false(t_pos), "unknown type of token: this is a bug (maybe report?)");
    return nullptr;
  }
}
} // namespace detail::json

template <typename StrT>
consteval auto json_str_ind() noexcept
{ return detail::json::json_str<StrT>{}; }

template <template <auto> class StrT>
consteval auto json_str_ind() noexcept
{ return detail::json::json_sized_str<StrT>{}; }

template <constr t_input, auto t_json_str = json_str_ind<std::string_view>()>
consteval auto parse_json() noexcept
{
  static constexpr detail::json::tokenize_result tokens =
      detail::json::tokenize<t_input>();

  if constexpr (tokens.code != detail::json::tokenize_error_codes::ok)
  {
    return nullptr;
  } else
  {
    static constexpr auto get_tokens = []{ return tokens.result; };

    static constexpr auto get_token_at = [](std::size_t pos) -> detail::json::token {
      return tokens.result[pos];
    };

    return detail::json::parse_json_part_at<get_token_at, 0, t_json_str>();
  }
}

}


