#include <string>
#include <tuple>
#include <iostream>

#include <pspp/defer.hpp>


void test_void_func()
{
	std::cout << "defered test void func withour args\n";
}

struct test_struct {
	double a; char b;

	std::string str() const;
};

std::string test_struct::str() const 
{
	return std::string("[" + std::to_string(a) + ";" + std::to_string(b) + "(" + std::to_string((std::uint8_t)b) + ")]");
}

void test_void_func_with_args(int a, std::string b, test_struct c)
{
	std::cout << "defer test void func with args: " << a << ", " << b << ", " << c.str() << '\n';
}

test_struct test_func_with_res_and_args(test_struct a, test_struct b)
{

	std::cout << "defered func with res and args: " << a.str() << ", " << b.str() << '\n';
	return {a.a + b.a, (char)(a.b + b.b)};
}

void some_func(test_struct* res)
{
	auto _d1 = pspp::dfr::defer{test_func_with_res_and_args, res, test_struct{1.0, 'b'}, test_struct{5.0, 'c'}};
	std::cout << "some calculations from some func...\n";
}

int main()
{
	auto _d = pspp::dfr::defer{test_void_func};
	pspp::dfr::defer<decltype(&test_void_func), void, std::tuple<>> a(test_void_func);
	test_struct* res = new test_struct();
	some_func(res);

	auto _d1 = pspp::dfr::defer{test_void_func_with_args, nullptr, 1, "a", test_struct{1, 'a'}};
	std::cout << "res getted by defering from some functions: " << res->str() << '\n';

	std::cout << "some calculations\n";

	return 0;
}








