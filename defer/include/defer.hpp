#ifndef PISOOSPP_DEFER_HPP
#define PISOOSPP_DEFER_HPP

#include <tuple>
#include <type_traits>

namespace pspp
{
inline namespace dfr
{
	template <class Func, class ReturnV, class Tuple>
	class defer
	{
		public:
			defer(const Func _func, ReturnV* _res = nullptr, auto ...args);
			~defer();

		private:
			const Func f;
			const Tuple args;
			ReturnV* res;

			defer(const Func _func, const Tuple _args, ReturnV* _res);
	};

	//deduction guides
	template <class Func>
	defer(Func _func, std::nullptr_t res, auto ...args) ->
	defer<Func, void, decltype(std::make_tuple(args...))>;

	template <class Func, class ReturnV>
	defer(Func _func, ReturnV* _res, auto ...args) -> 
	defer<Func, ReturnV, decltype(std::make_tuple(args...))>;

	template <class Func>
	defer(Func _func) ->
	defer<decltype(_func), void, std::tuple<>>;

	//implementations
	//
	//private constructor
	template <class Func, class ReturnV, class Tuple>
	defer<Func, ReturnV, Tuple>::defer(const Func _func, const Tuple _args, ReturnV* _res)
	:
		f(_func),
		args(_args),
		res(_res)
	{}

	//public constructor
	template <class Func, class ReturnV, class Tuple>
	defer<Func, ReturnV, Tuple>::defer(const Func _func, ReturnV* _res, auto ..._args)
	: defer(_func, std::make_tuple(_args...), _res)
	{}
	

	template <class Func, class ReturnV, class Tuple>
	defer<Func, ReturnV, Tuple>::~defer()
	{
		if constexpr (std::is_same<ReturnV, void>())
			std::apply(f, args);
		else
			*res = std::apply(f, args);
	}

} // namespace dfr
} // namepsace pspp
#endif // PISOOSPP_DEFER_HPP
