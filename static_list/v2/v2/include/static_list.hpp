#ifndef PSPP_STATIC_LIST_HPP
#define PSPP_STATIC_LIST_HPP

#include <ranges>
#include <bitset>
#include <pspp/type_sequence.hpp>
#include <pspp/utility/iota_range.hpp>
#include <pspp/utility/utility.hpp>
#include <pspp/iterator.hpp>

#include <format>

namespace pspp
{
	namespace list_opts
	{
		struct bidirectional  {};
		struct unidirectional {};
		struct throw_when_no_next {};

		template <auto t_sorter>
		struct static_sorter : value_holder<t_sorter> {};

		template <auto t_checker>
		struct static_checker : value_holder<t_checker> {};

		struct runtime_sorter {};
		struct runtime_checker {};

		struct enable_indexes_table {};

		template <typename ... Options>
		struct options_set
		{
			static_val opts = type_sequence<Options...>{};

			static_flag is_bi = not (opts | contains<unidirectional>);
			static_flag throws = opts | contains<throw_when_no_next>;
			static_flag is_view = opts | contains<ranges_options::view>;
		};
	} // namespace list_opts

	template <typename DataT, typename ... Options>
	struct list_node;

	namespace detail
	{
		constinit char _address_holder [100] {};
		constinit void * _end_address {&_address_holder};
	}

	namespace detail
	{

	template <bool t_is_bi, typename DataT, typename ... Options>
	struct previous_holder
	{
		list_node<DataT, Options...> * m_prev;
	};

	template <typename DataT, typename ... Options>
	struct previous_holder<false, DataT, Options...>
	{};

	inline constexpr bool gcc_find_first_enabled = requires {std::bitset<1>{}._Find_first();};

	template <std::size_t t_chunk_size>
	struct indexes_chunk
	{
		std::bitset<t_chunk_size> _m_data {};
		std::size_t _m_count {t_chunk_size};

		pspp_always_inline constexpr void set_to_1(std::integral auto pos) noexcept
		{
			_m_data.set(pos);
			++_m_count;
		}

		pspp_always_inline constexpr void set_to_0(std::integral auto pos) noexcept
		{
			_m_data.set(pos, false);
			--_m_count;
		}

		pspp_always_inline constexpr bool any() const noexcept
		{ return _m_count != 0; }
	};

	template <std::size_t t_size, std::size_t t_chunk_size = 64>
		requires is_mod_of_2<t_chunk_size>
	struct indexes_manager
	{
		static_size chunks_count = (t_size + t_chunk_size - 1)/ t_chunk_size;
		static_size chunk_power = sqrt<t_chunk_size>;
		static_size npos = pspp::npos;

		std::size_t _m_total_count {t_size};
		mutable std::size_t _m_simple_iterator {0};


		/*static_prop_of std::bitset<t_chunk_size> _ones = []{
			std::bitset<t_chunk_size> res {};
			for (auto i : pspp::iota_range<t_chunk_size>())
				res[i] = 1;

			return res;
		}();
		*/

		std::array<indexes_chunk<t_chunk_size>, chunks_count> _m_chunks;

		constexpr void set_all_flags_to_0() noexcept
		{
			for (auto & chunk : _m_chunks)
				chunk = 0;
		}

		constexpr void set_all_flags_to_1() noexcept
		{
			for (auto & chunk : _m_chunks)
				chunk._m_data.set();

			if constexpr (t_size % t_chunk_size != 0)
			{
				indexes_chunk<t_chunk_size> & last = _m_chunks.back();
				last._m_data.set();

				for (auto i : iota_range<t_size - (chunks_count - 1) * t_chunk_size, t_chunk_size>())
					last.set_to_0(i);
			}

			_m_total_count = t_size;
		}

		constexpr std::size_t find_first_1() const noexcept
		{
			if (_m_simple_iterator != t_size)
				return _m_simple_iterator++;

			for (auto i : iota_range<chunks_count, std::size_t>())
			{
				auto const & chunk = _m_chunks[i];
				if (chunk.any())
				{
					if constexpr (not gcc_find_first_enabled)
					{
						for (auto j : iota_range<t_chunk_size>())
							if (chunk[j] == 1)
								return j + (i << chunk_power);
					}
					else
					{
						return chunk._m_data._Find_first() + (i << chunk_power);
					}

					std::unreachable();
				}
			}
			return static_cast<std::size_t>(-1);
		}

		pspp_always_inline void constexpr set_to_0(std::integral auto pos) noexcept
		{
			--_m_total_count;
			std::size_t chunk_index = static_cast<std::size_t>(pos) >> chunk_power;
			std::size_t bit_index = static_cast<std::size_t>(pos) xor (chunk_index << chunk_power);
			_m_chunks[chunk_index].set_to_0(bit_index);
		}

		pspp_always_inline void constexpr set_to_1(std::integral auto pos) noexcept
		{
			++_m_total_count;
			std::size_t chunk_index = static_cast<std::size_t>(pos) >> chunk_power;
			std::size_t bit_index = static_cast<std::size_t>(pos) xor (chunk_index << chunk_power);
			_m_chunks[chunk_index].set_to_1(bit_index);
		}

		auto operator[](std::integral auto pos) const noexcept
		{
			std::size_t chunk_index = static_cast<std::size_t>(pos) >> chunk_power;
			std::size_t bit_index = static_cast<std::size_t>(pos) xor (chunk_index << chunk_power);
			return _m_chunks[chunk_index]._m_data[bit_index];
		}

		constexpr std::size_t count() const noexcept
		{ return _m_total_count; }
	};

	/*
	template <std::size_t t_size, std::size_t t_chunk_size>
		requires (is_mod_of_2<t_chunk_size> and requires { std::bitset<t_chunk_size>{}._Find_first(); })
	struct indexes_manager<t_size, t_chunk_size>
	{
		static_size npos = t_size;
		std::size_t _m_simple_iterator {0};
		bool _m_is_end_reached;
		std::bitset<t_size> _m_data;

		pspp_always_inline constexpr void set_all_flags_to_0() noexcept
		{ _m_data = 0; }

		pspp_always_inline constexpr void set_all_flags_to_1() noexcept
		{ _m_data.set(); }

		pspp_always_inline constexpr std::size_t find_first_1() noexcept
		{
			if (_m_simple_iterator == t_size)
				return _m_data._Find_first();
			else
				return _m_simple_iterator++;
		}

		pspp_always_inline constexpr auto operator[](std::integral auto pos) noexcept
		{ return _m_data[pos]; }

		pspp_always_inline constexpr auto const operator[](std::integral auto pos) const noexcept
		{ return _m_data[pos]; }

		pspp_always_inline constexpr std::size_t count() const noexcept
		{ return _m_data.count(); }
	};
	*/

	} // namespace detail

	template <std::integral OffsetT>
	struct checked_offset
	{
		OffsetT m_offset;
	};

	checked_offset(std::integral auto offset) -> checked_offset<decltype(offset)>;

	template <typename DataT, typename ... Options>
	struct list_node : 
		detail::previous_holder<list_opts::options_set<Options...>::is_bi, DataT, Options...>
	{
		using opts = list_opts::options_set<Options...>;
		using prev_holder = detail::previous_holder<opts::is_bi, DataT, Options...>;

		DataT m_data;
		list_node * m_next;

		pspp_always_inline 
		constexpr std::conditional_t<opts::is_view,
							DataT const &,
							DataT &> operator*() noexcept
		{ return m_data; }

		pspp_always_inline 
		constexpr DataT const & operator*() const noexcept
		{ return m_data; }

		constexpr inline list_node const & operator+(std::integral auto offset) const noexcept (not opts::throws)
		{
			list_node const * iter = this;
			for (decltype(offset) i = 0; i < offset; i++)
			{
				if (iter->m_next)
					iter = iter->m_next;
				else
					if constexpr (opts::throws)
						throw std::runtime_error(
								std::format("list_node: operator+({}): out of range", offset)
								);
					else
						return *iter;
			}

			return *iter;
		}

		constexpr inline list_node & operator+(std::integral auto offset) noexcept (not opts::throws)
		{
			list_node * iter = this;
			for (decltype(offset) i = 0; i < offset; i++)
			{
				if (iter->m_next)
					iter = iter->m_next;
				else
					if constexpr (opts::throws)
						throw std::runtime_error(
								std::format("list_node: operator+({}): out of range", offset)
								);
					else
						return *iter;
			}

			return *iter;
		}

		template <std::integral OffsetT>
		constexpr inline list_node & operator+(checked_offset<OffsetT> offset) noexcept
		{
			list_node * iter = this;
			for (OffsetT i = 0; i < offset.m_offset; i++)
				iter = iter->m_next;

			return *iter;
		}

		template <std::integral OffsetT>
		constexpr inline list_node & operator-(checked_offset<OffsetT> offset) noexcept
			requires (opts::is_bi)
		{
			list_node * iter = this;
			for (OffsetT i = 0; i < offset.m_offset; i++)
				iter = iter->m_prev;

			return *iter;
		}

		template <std::integral OffsetT>
		constexpr inline list_node const & operator-(checked_offset<OffsetT> offset) const noexcept
			requires (opts::is_bi)
		{
			list_node * iter = this;
			for (OffsetT i = 0; i < offset.m_offset; i++)
				iter = iter->m_prev;

			return *iter;
		}

		template <std::integral OffsetT>
		constexpr inline list_node const & operator+(checked_offset<OffsetT> offset) const noexcept
		{
			list_node * iter = this;
			for (OffsetT i = 0; i < offset.m_offset; i++)
				iter = iter->m_next;

			return *iter;
		}


		/*constexpr inline list_node & operator++() noexcept(not opts::throws)
		{ 
			if (m_next)
				*this = *m_next;
			else
			{
				if constexpr (opts::throws)
					throw std::runtime_error("list_node: operator++(): trying to access not existing element");
			}

			return *this;
		}

		constexpr inline list_node const & operator++() const noexcept(not opts::throws)
		{ 
			if (m_next)
				*this = *m_next;
			else
			{
				if constexpr (opts::throws)
					throw std::runtime_error("list_node: operator++(): trying to access not existing element");
			}

			return *this;
		}

		constexpr inline list_node & operator--(int) noexcept(not opts::throws)
			requires (opts::is_bi)
		{ 
			if (prev_holder::m_prev)
				*this = *prev_holder::m_prev;
			else 
			{
				if constexpr (opts::throws)
					throw std::runtime_error("list_node: operator--(int): trying to access not existing element");
			}

			return *this;
		}

		constexpr inline list_node const & operator--(int) const noexcept(not opts::throws)
			requires (opts::is_bi)
		{ 
			if (prev_holder::m_prev)
				*this = *prev_holder::m_prev;
			else 
			{
				if constexpr (opts::throws)
					throw std::runtime_error("list_node: operator--(int): trying to access not existing element");
			}

			return *this;
		}
		*/


		constexpr inline bool operator==(list_node const & other) const noexcept
		{ return m_data == other.m_data; }

		constexpr inline bool operator!=(list_node const & other) const noexcept
		{ return m_data != other.m_data; }
	};


	template <typename DataT, typename ... Options>
	struct list_node_iterator
	{
		using opts = list_opts::options_set<Options...>;
		list_node<DataT, Options...> * node;
		std::size_t index;

		pspp_always_inline constexpr auto & operator*() noexcept(noexcept(*node))
		{ return node->operator*(); }

		pspp_always_inline constexpr list_node_iterator & operator++() noexcept(not opts::throws)
		{ 
			node = std::addressof(node->operator+(1)); 
			++index;
			return *this;
		}

		pspp_always_inline constexpr list_node_iterator operator+(auto && offset) noexcept(not opts::throws)
		{ return {std::addressof(node->operator+(std::forward<decltype(offset)>(offset))), index + offset}; }

		pspp_always_inline constexpr list_node_iterator operator-(auto && offset) noexcept(not opts::throws)
		{ return {std::addressof(node->operator-(std::forward<decltype(offset)>(offset))), index - offset}; }

		pspp_always_inline constexpr list_node_iterator & operator--() noexcept(not opts::throws)
			requires (opts::is_bi)
		{
			node = std::addressof(node->operator-(1));
			--index;
			return *this;
		}

		pspp_always_inline constexpr bool operator==(list_node_iterator<DataT, Options...> const &) const noexcept = default;
		pspp_always_inline constexpr bool operator!=(list_node_iterator<DataT, Options...> const &) const noexcept = default;
	};


	template <typename DataT, std::size_t t_size, typename ... Options>
	struct static_list
	{
		using node_ref_type = list_node<DataT, Options...> &;
		using node_ptr_type = list_node<DataT, Options...> *;
		using node_const_ref_type = list_node<DataT, Options...> const &;
		using opts = list_opts::options_set<Options...>;
		using iterator_type = list_node_iterator<DataT, Options...>;

		using indexes_manager_type = detail::indexes_manager<t_size>;

		std::array<list_node<DataT, Options...>, t_size> _m_data_storage;

		list_node<DataT, Options...> * _m_head_edge;
		list_node<DataT, Options...> * _m_tail_edge;

		detail::indexes_manager<t_size> _m_indexes;

		std::size_t _m_size {0};

		std::size_t nodes_moved {0};

		pspp_always_inline std::size_t space_left() const noexcept
		{ return t_size - _m_size; };

		pspp_always_inline std::size_t size() const noexcept
		{ return _m_size; }

		pspp_always_inline std::ptrdiff_t ssize() const noexcept
		{ return static_cast<std::ptrdiff_t>(size()); }

		template <bool t_throws = false, typename NewEl, typename OffsetT>
		pspp_always_inline constexpr bool insert_into(NewEl && new_el, OffsetT && pos) noexcept(not t_throws)
		{
			using offset_type = std::remove_cvref_t<OffsetT>;
			if constexpr (std::integral<offset_type>) 
				return _insert_data_in_pos<t_throws>(std::forward<NewEl>(new_el), std::forward<OffsetT>(pos));
			else
				return _insert_data_in_iter_point<t_throws>(std::forward<NewEl>(new_el), std::forward<OffsetT>(pos));
		}

		template <bool t_throws = false, typename PosT>
		inline constexpr bool insert_range_into(std::ranges::range auto range, PosT && pos) noexcept (not t_throws)
		{
			return _insert_data_sequence_into_pos<false, t_throws>(std::forward<decltype(range)>(range), std::forward<PosT>(pos));
		}

		template <bool t_throws = false, typename PosT>
		inline constexpr bool insert_range_as_much_as_can_into(std::ranges::range auto range, PosT && pos) noexcept (not t_throws)
		{
			return _insert_data_sequence_into_pos<true, t_throws>(std::forward<decltype(range)>(range), std::forward<PosT>(pos));
		}



		template <bool t_throw_if_no_space = false>
		bool push_back(auto && new_el) noexcept(not t_throw_if_no_space)
		{
			if constexpr (opts::is_bi)
				return insert_into(std::forward<decltype(new_el)>(new_el), iterator_type{&_m_tail_edge, size()});
			else
				return insert_into(std::forward<decltype(new_el)>(new_el), size());
		}

		static_list() :
			_m_data_storage{},
			_m_nodes_storage{},
			_m_head_edge{},
			_m_tail_edge{},
			_m_indexes{}
		{
			_m_indexes.set_all_flags_to_1();

			_m_tail_edge.m_data = static_cast<DataT*>(detail::_end_address);

			if constexpr (opts::is_bi)
				_m_tail_edge.m_prev = &_m_head_edge;
			_m_head_edge.m_next = &_m_tail_edge;
		}

		private:

		template <bool t_checked = false>
		node_ref_type _get_node_at(std::ptrdiff_t pos) noexcept(not opts::throws)
		{
			//nodes_moved += pos;
			auto _size = ssize();

			if constexpr (opts::is_bi and not t_checked)
				if (pos >= (_size >> 1))
					return _get_node_at_from_back(_size - pos);

			if (pos >= _size)
			{
				if constexpr (opts::throws)
					throw std::runtime_error(
							std::format("static_list: _get_node_at: pos ({}) is bigger than size ({})", pos, _size)
							);

				return _m_tail_edge;
			}

			return _m_head_edge + checked_offset{pos + 1};
		}

		template <bool t_checked = false>
		node_ref_type _get_node_at_from_back(std::size_t pos) noexcept (not opts::throws)
			requires (opts::is_bi)
		{
			std::size_t _size = size();

			if constexpr (not t_checked)
				if (pos >= (_size >> 1))
					return _get_node_at<true>(size() - pos);

			if (pos >= _size)
			{
				if constexpr (opts::throws)
					throw std::runtime_error(
							std::format("static_list: _get_node_at_from_back: pos ({}) is bigger than size ({})", pos, _size)
							);

				return _m_head_edge;
			}

			return _m_tail_edge - checked_offset{pos};
		}

		template <bool t_throw_if_no_space>
		constexpr std::size_t _insert_data_into_nearest_cell(auto && data) noexcept(not t_throw_if_no_space)
		{
//			std::cout << "trying to insert " << data;

			std::size_t new_el_pos = _m_indexes.find_first_1();

			if (new_el_pos == indexes_manager_type::npos)
			{
				if constexpr (t_throw_if_no_space)
				{
					throw std::runtime_error("static_list: push_back: reached space limit");
					return npos;
				}
				return npos;
			}

			if constexpr ( requires { data.operator*(); })
			{
				if constexpr (
					std::same_as<
						std::remove_cvref_t<DataT>,
						std::remove_cvref_t<decltype(*data)>
					>) // if data is an iterator
				{
//					std::cout << "; inserting as iterator;";
					_m_data_storage[new_el_pos] = std::forward<decltype(data)>(data)->operator*();
				}
			}
			else
			{
//				std::cout << "; inserting as data;";
				_m_data_storage[new_el_pos] = std::forward<decltype(data)>(data);
			}

			_m_indexes.set_to_0(new_el_pos);
//			std::cout << "done at: " << new_el_pos << '\n';

			return new_el_pos;
		}

		pspp_always_inline constexpr node_ref_type _update_node_storage_cell_on(std::size_t pos) noexcept
		{
//			std::cout << "updating node stored at: " << pos << '\n';
			node_ref_type new_pos = _m_nodes_storage[pos];
			new_pos.m_data = std::addressof(_m_data_storage[pos]);

			return new_pos;
		}

		template <bool t_throw_if_no_space, typename NewEl>
		inline constexpr bool _insert_data_in_iter_point(NewEl && data, iterator_type iter) noexcept (not t_throw_if_no_space)
		{
//			std::cout << "inserting data into iter: " << iter.index << '\n';
			std::size_t new_pos = _insert_data_into_nearest_cell<t_throw_if_no_space>(std::forward<NewEl>(data));

			if constexpr (not t_throw_if_no_space)
				if (new_pos == npos)
					return false;

			node_ref_type new_node = _update_node_storage_cell_on(new_pos);
			if constexpr (opts::is_bi)
				_insert_node_in_place_of(new_node, *iter.node->m_prev);
			else
				_insert_node_in_place_of(new_node, _get_node_at(iter.index - 1));

			++_m_size;
			return true;
		}

		inline constexpr bool _remove_node_at(std::integral auto pos) noexcept (opts::throws)
		{
			node_ref_type one_before_remove = _get_node_at(pos - 1);

			if constexpr (not opts::throws)
				if (one_before_remove.is_end())
					return false;

			one_before_remove.m_next = one_before_remove.m_next->m_next;
			if constexpr (opts::is_bi)
				one_before_remove.m_next->m_prev = one_before_remove;

			return true;
		}

		inline constexpr void _remove_node_at(iterator_type iter) noexcept
		{
			iter.node->m_next = iter.node->m_next->m_next;
			if constexpr (opts::is_bi)
				iter.node->m_next->m_prev = iter.node;
		}

		template <bool t_throw_if_no_space, typename NewEl>
		inline constexpr bool _insert_data_in_pos(NewEl && data, std::integral auto pos) noexcept (not (
					t_throw_if_no_space or opts::throws))
		{
//			std::cout << "inserting data in: " << pos << '\n';

			node_ref_type one_before = _get_node_at(pos - 1);

			if constexpr (not opts::throws)
				if (one_before.m_data == detail::_end_address)
					return false;

			std::size_t new_el_pos = _insert_data_into_nearest_cell<t_throw_if_no_space>(std::forward<NewEl>(data));
			_update_node_storage_cell_on(new_el_pos);

			_insert_node_in_place_of(_m_nodes_storage[new_el_pos], one_before);

			++_m_size;

			return true;
		}

		template <bool t_insert_as_much_as_can, bool t_throw_if_no_space = false, typename PosT>
		inline constexpr bool _insert_data_sequence_into_pos(std::ranges::range auto && data, PosT && pos) noexcept (not opts::throws)
		{
			using datas_type = decltype(data);
			using element_type = copy_extensions_t<datas_type, decltype(*std::begin(std::forward<datas_type>(data)))>;

//			std::cout << "starting inserting range\n";

			std::size_t new_els_count = std::size(std::forward<datas_type>(data));
			if (new_els_count == 0)
				return true;

			if constexpr (not t_insert_as_much_as_can)
				if (std::size(std::forward<decltype(data)>(data)) > space_left())
				{
					if constexpr (t_throw_if_no_space)
						throw std::runtime_error(
								std::format("static_list: insert: trying to insert more elements than space obtained ({} > {})",
										new_els_count, space_left()
									));

					return false;
				}
			// using throws = std::bool_constant<not t_insert_as_much_as_can and t_throw_if_no_space>;

			std::size_t left = space_left();
			if (left == 0)
			{
				++_m_size;
				return true;
			}

			auto iter = std::begin(data);
			auto end = std::end(data);

			auto insert_one = [&] -> node_ref_type {
				std::size_t new_pos = _insert_data_into_nearest_cell<false>(std::forward<element_type>(*iter));
//				std::cout << "inserting next into " << new_pos << '\n';

				++iter;
				if (new_pos == npos)
					return _m_tail_edge;

				++_m_size;
				return _update_node_storage_cell_on(new_pos);
			};

			node_ref_type start_node = insert_one();
			node_ptr_type node_iter = std::addressof(start_node);
			do
			{
				if (iter == end)
					break;

				node_ref_type next_node = insert_one();
				if (next_node.is_end())
					break;

//				std::cout << ">> updating tmp nodes\n";
//				std::cout << ">>\tnode_iter address: " << node_iter << '\n';
				_insert_node_in_place_of(next_node, *node_iter);
				node_iter = std::addressof(next_node);
//				std::cout << ">>>> going to next sicle\n";
			} while (true);

			if constexpr (std::integral<PosT>)
				_insert_node_range_in_place_of(_get_node_at(std::forward<PosT>(pos) - 1), start_node, *node_iter);
			else
			{
				static_assert(std::same_as<iterator_type, std::remove_cvref_t<PosT>>);
				if constexpr (opts::is_bi)
					_insert_node_range_in_place_of(*pos.node->m_prev, start_node, *node_iter);
				else
					_insert_node_range_in_place_of(_get_node_at(pos.index), start_node, *node_iter);
			}

			return true;
		}

		constexpr void _insert_node_range_in_place_of(node_ref_type pos_before_first, node_ref_type first, node_ref_type last) noexcept
		{
			last.m_next = pos_before_first.m_next;
			pos_before_first.m_next = std::addressof(first);
			if constexpr (opts::is_bi)
			{
				last.m_next->m_prev = std::addressof(last);
				first.m_prev = std::addressof(pos_before_first);
			}
		}

		constexpr void _insert_node_in_place_of(node_ref_type to_insert, node_ref_type one_before) noexcept
		{
//			std::cout << ">>>> inside\n";
			if constexpr (opts::is_bi)
				to_insert.m_prev = std::addressof(one_before);

			to_insert.m_next = one_before.m_next;
			one_before.m_next = std::addressof(to_insert);

//			std::cout << ">>>> updated\n";
			if constexpr (opts::is_bi)
			{
				if (to_insert.m_next)
					to_insert.m_next->m_prev = std::addressof(to_insert);
//				std::cout << ">>>> updated back\n";
			}
		}

		constexpr bool _insert_node_into(std::size_t data_index, std::ptrdiff_t node_pos) noexcept(not opts::throws)
		{
			node_ref_type new_pos = _update_node_storage_cell_on(data_index);

			node_ref_type old_pos = _get_node_at(node_pos - 1);
			if constexpr (not opts::throws)
				if (old_pos.m_data == detail::_end_address)
					return false;

			_insert_node_in_place_of(new_pos, old_pos);
			return true;
		}
	};
} // namespace pspp

namespace std
{
template <typename DataT, std::size_t t_size, typename ... Options>
pspp::list_node_iterator<DataT, Options...> begin(pspp::static_list<DataT, t_size, Options...> & list) 
{
	return ++pspp::list_node_iterator<DataT, Options...>{std::addressof(list._m_head_edge), pspp::npos};
}

template <typename DataT, std::size_t t_size, typename ... Options>
pspp::list_node_iterator<DataT, Options...> end(pspp::static_list<DataT, t_size, Options...> & list) 
{
	return pspp::list_node_iterator<DataT, Options...>{std::addressof(list._m_tail_edge), list.size()};
}
} // namespace std


#endif // PSPP_STATIC_LIST_HPP
