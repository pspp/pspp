#!/usr/bin/bash
#

function display_help {
  echo "Usage: your_script_name [options]"
  echo "Options:"
  echo "  -h, --help, help             Display this help message."
  echo "  to <install_path>            Set the installation path to <install_path>."
  echo "  using-links                  Use links for installation."
  echo "  using-hardcopy               Do not use links for installation."
  echo "  -v                           Enable verbose mode."
}

root=$(dirname $(realpath $0))
echo "[INFO] executing from $root"

install_path=""
use_links="1"
verbous=""

while (( "$#" )); do
  case "$1" in
    -h | --help | help)
      display_help
      exit 0
      ;;
    to)
      install_path="$2"
      shift
      shift
      ;;
    using-links)
      use_links="1"
      shift
      ;;
    using-hardcopy)
      use_links=""
      shift
      ;;
    -v)
      verbous="1"
      shift
      ;;
    *) 
      echo "Unknown option: $1"
      exit 1
      ;;
  esac
done

include_install_path="$install_path/include"
source_install_path="$install_path/source"

function ensure_that_exists {
	if [ ! -d "$1" ]; then
		info "creating: $1"
		mkdir -p "$1"
	fi
}


function info {
	echo -e "[INFO] $@"
}
function debug {
	if [ ! -z "$verbous" ]; then
		echo -e "\e[0;32mDEBUG\e[0;0m $@"
	fi
}

function install_to {
	debug "install_to $1 $2"
	info "installing: $(realpath -m $1)\n               to: $(realpath -m $2)"
	if [ ! -z "$use_links" ]; then
		ln -sf "$(realpath -m $1)" "$(realpath -m $2)"
	else
		cp "$(realpath -m $1)" "$(realpath -m $2)"
	fi
}

function install_for {
	debug "install_for $1 $2"
	base="$(basename $2)"
	l_include_install_path="${include_install_path}/$1"
	l_source_install_path="${source_install_path}/$1"

	if [[ "$base" == "include" ]]; then
		return
	fi
	if [[ "$base" == "source" ]]; then
		return
	fi
	if [[ "$base" == "import" ]]; then
		return
	fi

	if [ ! -d $2 ]; then
		return
	fi
	if [ -e "$2/.ignore" ]; then
		return
	fi

	if [ -e "$2/.path_part" ]; then
		info "creating $2 dirs"
		mkdir -p "${include_install_path}/$1/$base"
		mkdir -p "${source_install_path}/$1/$base"

		for_each_in_install "$1/$base" "$2"
		return 
	fi
	
	if [ -d "$2/include" ]; then
		for header in "$2/include"/*; do
			if [[ $header != *.cpp* ]]; then
				install_to "$header" "$l_include_install_path/$(basename $header)"
			fi
		done
	fi

	if [ -d "$2/source" ]; then
		for src in "$2/source"/*; do
			install_to "$src" "$l_source_install_path/$(basename $src)"
		done
	fi

	if [ -e "$2/.recursive" ]; then
		for_each_in_install "$1" "$2"
	fi

}

function for_each_in_install {
	debug "for_each_in_install $1 $2"
	for entry in $2/*; do
		install_for $1 "$entry"
	done
}


###############################################
###############################################


ensure_that_exists "$include_install_path/pspp"
ensure_that_exists "$source_install_path/pspp"

touch "$install_path/.ignore"

for_each_in_install "pspp" "$root"

rm "$install_path/.ignore"




