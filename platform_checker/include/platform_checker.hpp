#ifndef PSPP_PLATFORM_CHECKER_HPP
#define PSPP_PLATFORM_CHECKER_HPP

#include <array>
#include <pspp/utility/declaration_macros.hpp>

#ifdef __APPLE__
#include "TargetConditionals.h"
#endif

namespace pspp::pc
{

struct os_
{};

template <typename T>
struct vos_
{
  using pspp_parent_os_type = T;
};

template <std::derived_from<os_> OST>
constexpr bool operator==(OST, vos_<OST>) noexcept
{ return true; }

template <std::derived_from<os_> OST>
constexpr bool operator!=(OST, vos_<OST>) noexcept
{ return false ; }

template <std::derived_from<os_> OST>
constexpr bool operator==(vos_<OST>, OST) noexcept
{ return true; }

template <std::derived_from<os_> OST>
constexpr bool operator!=(vos_<OST>, OST) noexcept
{ return false ; }

template <std::derived_from<os_> OST, std::derived_from<os_> OSU>
constexpr bool operator==(OST, vos_<OSU>) noexcept
{ return false; }

template <std::derived_from<os_> OST, std::derived_from<os_> OSU>
constexpr bool operator!=(OST, vos_<OSU>) noexcept
{ return true ; }

template <std::derived_from<os_> OST, std::derived_from<os_> OSU>
constexpr bool operator==(vos_<OSU>, OST) noexcept
{ return false; }

template <std::derived_from<os_> OST, std::derived_from<os_> OSU>
constexpr bool operator!=(vos_<OSU>, OST) noexcept
{ return true; }

template <std::derived_from<os_> OST>
constexpr bool operator==(OST, OST) noexcept { return true; }

template <std::derived_from<os_> OST, std::derived_from<os_> OtherT>
constexpr bool operator==(OST, OtherT) noexcept { return false; }

template <std::derived_from<os_> OST>
constexpr bool operator!=(OST, OST) noexcept { return false; }

template <std::derived_from<os_> OST, std::derived_from<os_> OtherT>
constexpr bool operator!=(OST, OtherT) noexcept { return true; }

inline constexpr std::size_t npos_ {static_cast<std::size_t>(-1)};

template <std::size_t t_parts_count>
struct version_t
{
  std::array<std::size_t, t_parts_count> version_parts;

  constexpr version_t() noexcept = default;
  constexpr version_t(version_t &&) noexcept = default;
  constexpr version_t(version_t const &) noexcept = default;

  template <std::integral FirstT, std::integral ... RestTs>
  constexpr version_t(FirstT first, RestTs ... rest) noexcept :
    version_parts{static_cast<std::size_t>(first), static_cast<std::size_t>(rest)...}
  {}

  template <std::size_t t_other>
    requires (t_other != t_parts_count)
  constexpr version_t(version_t<t_other> other) noexcept :
    version_parts{npos_}
  {
    static constexpr std::size_t count = std::min(t_other, t_parts_count);
    std::size_t i = 0;
    for (;i <   count; ++i) version_parts[i] = other.version_parts[i];
    for (;i < t_other; ++i) version_parts[i] = -1;
  }

  template <std::integral FirsT, std::integral ... RestTs>
    requires (sizeof...(RestTs) < t_parts_count - 1)
  constexpr version_t(FirsT first, RestTs ... rest) noexcept :
    version_parts{first, rest...}
  {
    for (std::size_t i = sizeof...(RestTs); i < t_parts_count; ++i)
      version_parts[i] = npos_;
  }

  template <std::size_t ... t_is, std::size_t t_other>
  constexpr bool get_equality(std::index_sequence<t_is...>, version_t<t_other> other) const noexcept
  {
    return (... and (version_parts[t_is] == other.version_parts[t_is] or
                     version_parts[t_is] == npos_ or
                     other.version_parts[t_is] == npos_));
  }

  template <auto t_op, std::size_t ... t_is, std::size_t t_other>
  constexpr bool get_greatness(std::index_sequence<t_is...>, version_t<t_other> other) const noexcept
  {
    return (... or (t_op(version_parts[t_is], other.version_parts[t_is]) or
                    version_parts[t_is] == npos_ or
                    other.version_parts[t_is] == npos_));
  }

  template <std::size_t t_other_parts_count>
  constexpr bool operator==(version_t<t_other_parts_count> other) const noexcept
  {
    static constexpr std::size_t count = std::min(t_parts_count, t_other_parts_count);
    if constexpr (count == 0) return true;
    else
    { return get_equality(std::make_index_sequence<count>{}, other); }
  }

  enum struct op_ {gt, ge, l, le};

  template <op_ t_op>
  struct gt_t
  {
    template<typename T, typename U>
    constexpr bool operator()(T l, U r) const noexcept
    {
      switch(t_op)
      {
        case op_::gt: return l >  r;
        case op_::ge: return l >= r;
        case op_::l : return l <  r;
        case op_::le: return l <= r;
      }
    }
  };

  template <auto t_op, std::size_t t_other>
  constexpr bool check_(version_t<t_other> other) const noexcept
  {
    return get_greatness<t_op>(std::make_index_sequence<std::min(t_parts_count, t_other)>{}, other);
  }

  template <std::size_t t_other_parts_count>
  constexpr bool operator>(version_t<t_other_parts_count> other) const noexcept
  { return check_<gt_t<op_::gt>{}>(other); }

  template <std::size_t t_other_parts_count>
  constexpr bool operator>=(version_t<t_other_parts_count> other) const noexcept
  { return check_<gt_t<op_::ge>{}>(other); }

  template <std::size_t t_other_parts_count>
  constexpr bool operator<(version_t<t_other_parts_count> other) const noexcept
  { return check_<gt_t<op_::l>{}>(other); }

  template <std::size_t t_other_parts_count>
  constexpr bool operator<=(version_t<t_other_parts_count> other) const noexcept
  { return check_<gt_t<op_::le>{}>(other); }

};

template <typename ... Ts>
version_t(Ts...) -> version_t<sizeof...(Ts)>;


template <version_t t_version>
struct _versioned_platform {
  inline static constexpr version_t pspp_os_version = t_version;
};

template <typename T>
concept c_versioned_os = requires { T::pspp_os_version; typename T::pspp_parent_os_type; };

template <typename T, typename U>
concept c_versioned_os_with_same_parent = c_versioned_os<T> and
  std::same_as<typename T::pspp_parent_os_type, typename U::pspp_parent_os_type>;

template <typename T, typename U>
concept c_versioned_os_with_dif_parent = c_versioned_os<T> and
                                          not std::same_as<typename T::pspp_parent_os_type, typename U::pspp_parent_os_type>;

template <typename T>
concept c_os = std::derived_from<T, os_>;

template <c_versioned_os T, c_versioned_os_with_same_parent<T> U>
constexpr bool operator==(T, U) noexcept
{ return T::pspp_os_version == U::pspp_os_version; }

template <c_versioned_os T, c_versioned_os_with_dif_parent<T> U>
constexpr bool operator==(U, T) noexcept { return false; }

template <c_versioned_os T, c_versioned_os_with_dif_parent<T> U>
constexpr bool operator==(T, U) noexcept { return false; }

template <c_versioned_os T, c_os U>
constexpr bool operator==(T, U) noexcept { return std::same_as<typename T::pspp_parent_os_type, U>; }

template <c_versioned_os T, c_os U>
constexpr bool operator!=(T, U) noexcept { return T{} != U{}; }

template <c_versioned_os T, c_os U>
constexpr bool operator==(U, T) noexcept { return std::same_as<typename T::pspp_parent_os_type, U>; }

template <c_versioned_os T, c_os U>
constexpr bool operator!=(U, T) noexcept { return T{} != U{}; }



inline constexpr struct Linux_t : os_ {} Linux {};
inline constexpr struct UNIX_t  : os_ {} UNIX {};


inline constexpr struct Android_t : os_ {
  static constexpr version_t<3> get_current_version() noexcept
  {
#ifndef __ANDROID_API__
#define __ANDROID_API__ 0
#endif
    static constexpr std::array<version_t<3>, 36> versions {
      version_t<3>{0, 0, 0},
      {1 , 0, 0}, {1 , 1, 0}, {1, 5, 0}, {1, 6, 0},
      {2 , 0, 0}, {2 , 0, 1}, {2, 1, 0}, {2, 2, 0}, {2, 3, 0}, {2, 3, 3},
      {3 , 0, 0}, {3 , 1, 0}, {3, 2, 0},
      {4 , 0, 0}, {4 , 0, 3}, {4, 1, 0}, {4, 2, 0}, {4, 3, 0}, {4, 4, 0}, {4, 4, 5},
      {5 , 0, 0}, {5 , 1, 0},
      {6 , 0, 0},
      {7 , 0, 0}, {7 , 1, 0},
      {8 , 0, 0}, {8 , 1, 0},
      {9 , 0, 0},
      {10, 0, 0},
      {11, 0, 0},
      {12, 0, 0}, {12, 1, 0},
      {13, 0, 0},
      {14, 0, 0},
      {15, 0, 0}
    };

    return versions[__ANDROID_API__];
#if (__ANDROID_API__ == 0)
#undef __ANDROID_API__
#endif
  }
} Android {};
template <std::size_t ... t_ver>
struct AndroidV_t : vos_<Android_t>, _versioned_platform<version_t{t_ver...}> {};
template <std::size_t ... t_ver>
inline constexpr AndroidV_t<t_ver...> AndroidV {};


inline constexpr struct AIX_t : os_ {} AIX {};

template <std::size_t ... t_ver>
struct AIXV_t : vos_<AIX_t>, _versioned_platform<version_t{t_ver...}> {};
template <std::size_t ... t_ver>
inline constexpr AIXV_t<t_ver...> AIXV {};

inline constexpr struct AmdahlUTS_t : os_ {} AmdahlUTS {};
inline constexpr struct Amiga_t : os_ {} Amiga {};
inline constexpr struct ApolloAEGIS_t : os_ {} ApolloAEGIS {};
inline constexpr struct ApolloDomain_t : os_ {} APolloDomain {};

inline constexpr struct Bada_t : os_ {} Bada {};
inline constexpr struct BeOS_t : os_ {} BeOS {};
inline constexpr struct BlueGeneQ_t : os_ {} BlueGeneQ {};
inline constexpr struct BlueGeneP_t : os_ {} BlueGeneP {};
inline constexpr struct BSD_t : os_ {} BSD {};
inline constexpr struct BSD_OS_t : vos_<BSD_t>, _versioned_platform<0> {} BSD_OS {};

inline constexpr struct Convex_t : os_ {} Convex {};
inline constexpr struct Cygwin_t : os_ {} Cygwin {};

inline constexpr struct DG_UX_t : os_ {} DG_UX {};
inline constexpr struct DragonFly_t : vos_<BSD_t>, _versioned_platform<1> {} DragonFly {};
inline constexpr struct DYNIXPtx_t : os_ {} DYNIXPtx {};

inline constexpr struct ECos_t : os_ {} ECos {};
inline constexpr struct EMXEnvironment_t : os_ {} EMXEnvironment {};

inline constexpr struct FreeBSD_t : os_ {} FreeBSD {};
template <std::size_t t_i>
struct FreeBSDV_t : vos_<FreeBSD_t>, _versioned_platform<t_i> {};
template <std::size_t t_i>
inline constexpr FreeBSDV_t<t_i> FreeBSDV {};

inline constexpr struct GnuHurd_t : os_ {} GnuHurd {};
inline constexpr GnuHurd_t Gnu {};

inline constexpr struct GnuKFreeBSD_t : os_ {} GnuKFreeBSD {};
inline constexpr struct GnuLinux : vos_<Linux_t>, _versioned_platform<0> {} GnuLinux {};

inline constexpr struct HIUX_MPP_t : os_ {} HIUX_MPP {};
inline constexpr struct HPUX_t : os_ {} HPUX {};

inline constexpr struct IBM_OS_400_t : os_ {} IBM_OS_400 {};
inline constexpr struct Integrity_t : os_ {} Integrity {};
inline constexpr struct InterixEnvironment_t : os_ {} InterixEnvironment {};
inline constexpr struct IRIX_t : os_ {} IRIX {};

inline constexpr struct LynxOS_t : os_ {} LynxOS {};

inline constexpr struct MacOS_t : os_ {} MacOS {};
inline constexpr struct Iphone_t : os_ {} Iphone {};
inline constexpr struct IphoneSimulator_t : os_ {} IphoneSimulator {};
inline constexpr struct MacCatalyst_t : os_ {} MacCatalyst {};

inline constexpr struct MicrowareOS9_t : os_ {} MicrowareOS9 {};
inline constexpr struct Minix_t : os_ {} Minix {};
inline constexpr struct MorphOS_t : os_ {} MorphOS {};
inline constexpr struct MPEiX_t : os_ {} MPEiX {};
inline constexpr struct MSDOS_t : os_ {} MSDOS {};

inline constexpr struct NetBSD_t : vos_<BSD_t>, _versioned_platform<2> {} NetBSD {};
inline constexpr struct NonStop_t : os_ {} NonStop {};
inline constexpr struct NucleusRTOS_t : os_ {} NucleusRTOS {};

inline constexpr struct OpenBSD_t : vos_<BSD_t>, _versioned_platform<3> {} OpenBSD {};
inline constexpr struct OS2_t : os_ {} OS2 {};

inline constexpr struct PalmOS_t : os_ {} PalmOS {};
inline constexpr struct Plan9_t : os_ {} Plan9 {};
inline constexpr struct Pyramid_t : os_ {} Pyramid {};

inline constexpr struct QNX_t : os_ {} QNX {};

inline constexpr struct ReliantUNIX_t : os_ {} ReliantUNIX {};

inline constexpr struct SCOOpenServer_t : os_ {} SCOOpenServer {};
inline constexpr struct Solaris_t : os_ {} Solaris {};
inline constexpr struct SunOs_t : os_ {} SunOs {};
inline constexpr struct StratusVOS_t : os_ {} StratusVOS {};
template <std::size_t t_ver>
struct StratusVOSV_t : vos_<StratusVOS_t>, _versioned_platform<t_ver> {};
template <std::size_t t_ver> inline constexpr StratusVOSV_t<t_ver> StratusVOSV {};
inline constexpr struct SVR4_t : os_ {} SVR4 {};
inline constexpr struct Syllable_t : os_ {} Syllable {};
inline constexpr struct SymbianOS_t : os_ {} SymbianOS {};

inline constexpr struct Tru64_t : os_ {} Tru64 {};
inline constexpr Tru64_t OSF1 {};

inline constexpr struct Ultrix_t : os_ {} Ultrix {};
inline constexpr struct UNICOS_t : os_ {} UNICOS {};
inline constexpr struct UNICOS_MP_t : os_ {} UNICOS_MP {};
inline constexpr struct UNIXWare_t : os_ {} UNIXWare {};
inline constexpr struct UWin_t : os_ {} UWin {};

inline constexpr struct VMS_t : os_ {} VMS {};
inline constexpr struct VxWorks_t : os_ {} VxWorks {};

inline constexpr struct Windows_t : os_ {} Windows {};
inline constexpr struct Win16 : vos_<Windows_t>, _versioned_platform<16> {} Win16 {};
inline constexpr struct Win32 : vos_<Windows_t>, _versioned_platform<32> {} Win32 {};
inline constexpr struct Win64 : vos_<Windows_t>, _versioned_platform<64> {} Win64 {};


inline constexpr struct WindowsCE_t : os_ {} WindowsCE {};
inline constexpr struct WindU_t : os_ {} WindU {};

inline constexpr struct ZOS_t : os_ {} ZOS {};
inline constexpr struct ZOS_Host : vos_<ZOS_t>, _versioned_platform<0> {} ZOS_Host {};
inline constexpr struct ZOS_Target : vos_<ZOS_t>, _versioned_platform<1> {} ZOS_Target {};


#ifdef __gnu_linux__
inline constexpr auto Current  = GnuLinux;
#elifdef _AIX
#define PSPP_CUSTOM_OSV_
inline constexpr auto Current = AIX;
#ifdef _AIX51
inline constexpr auto CurrentV = AIXV<5, 1>;
#elifdef _AIX52
inline constexpr auto CurrentV = AIXV<5, 2>;
#elifdef _AIX53
inline constexpr auto CurrentV = AIXV<5, 3>;
#elifdef _AIX61
inline constexpr auto CurrentV = AIXV<6, 1>;
#elifdef _AIX71
inline constexpr auto CurrentV = AIXV<7, 1>;
#elifdef _AIX72
inline constexpr auto CurrentV = AIXV<7, 2>;
#elifdef _AIX73
inline constexpr auto CurrentV = AIXV<7, 3>;
#endif
#elifdef __ANDROID__
#define PSPP_CUSTOM_OSV_
inline constexpr auto Current = Android;
consteval auto get_current_android_version() noexcept
{
  constexpr version_t<3> v = Android::get_current_version();
  return AndroidV<v.version_parts[0], v.version_parts[1], v.version_parts[2]>{};
}

using current_android_version_t = decltype(get_current_android_version());
inline constexpr auto CurrentV = current_android_version_t;
#elifdef UTS
inline constexpr auto Current = AmdahlUTS;
#elifdef AMIGA
inline constexpr auto Current  = AmigaOS;
#elifdef aegis
inline constexpr auto Current  = ApolloAegis;
#elifdef apollo
inline constexpr auto Current  = ApolloDomain;
#elifdef __BEOS__
inline constexpr auto Current  = BeOS;
#elif (defined(__bg__) || defined(__THW_BLUEGENE__))
inline constexpr auto Current  = BlueGeneP;
#elif (defined(__bgq__) || defined(__TOS_BGQ__))
inline constexpr auto Current  = BlueGeneQ;
#elifdef __bsdi__
inline constexpr auto Current  = BSD_OS;
#elifdef __convex__
inline constexpr auto Current  = Convex;
#elifdef __CYGWIN__
inline constexpr auto Current  = Cygwin;
#elif (defined(DGUX) || defined(__DGUX__) || defined(__dgux__))
inline constexpr auto Current  = DG_UX;
#elifdef __DragonFly__
inline constexpr auto Current  = DragonFly;
#elif (defined(_SEQUENT_) || defined(sequent))
inline constexpr auto Current  = DYNIXPtx;
#elifdef __ECOS
inline constexpr auto Current  = ECos;
#elifdef __EMX__
inline constexpr auto Current  = EMXEnvironment;
#elif (defined(__FreeBSD__) || defined(BSD) ||\
      (defined(__FreeBsd_kernel__) && !defined(__GLIBC__)))
#define PSPP_CUSTOM_OSV_
inline constexpr auto Current  = FreeBSD;
inline constexpr auto CurrentV = FreeBSDV<__FreeBSD__>;
#elif (defined(__GNU__) or defined(__gnu_hurd__))
inline constexpr auto Current  = GnuHurd;
#elif (defined(__FreeBSD_kernel__) && defined(__GNUC__))
inline constexpr auto Current  = GnuKFreeBSD;
#elifdef __hiuxmpp
inline constexpr auto Current  = HIUX_MPP;
#elif (defined(_hpux) || defined(hpux) || defined(__hpux))
inline constexpr auto Current  = HPUX;
#elifdef __OS400__
inline constexpr auto Current  = IBM_OS_400;
#elifdef __INTEGRITY
inline constexpr auto Current  = Integrity;
#elifdef __INTERIX
inline constexpr auto Current  = InterixEnvironment;
#elif (defined(sgi) || defined(__sgi))
inline constexpr auto Current  = IRIX;
#elifdef __Lynx__
inline constexpr auto Current  = LynxOS;
#elifdef __APPLE__
#define PSPP_CUSTOM_OSV_
#if TARGET_IPHONE_SIMULATOE
inline constexpr auto Current = IphoneSumilator;
#elif TARGET_OS_MACCATALYST
inline constexpr auto Current = MacCatalyst;
#elif TARGET_OS_IPHONE
inline constexpr auto Current = Iphone;
#else
inline constexpr auto Current  = MacOS;

#endif // mac target

inline constexpr auto CurrentV = Current;
#elif defined(__OS9000) || defined(_OSK)
inline constexpr auto Current  = MicrowareOS9;
#elifdef __minix
inline constexpr auto Current  = MINIX;
#elifdef __MORPHOS__
inline constexpr auto Current  = MORPHOS;
#elif defined(mpeix) || defined(__mpexl)
inline constexpr auto Current  = MPEiX;
#elif defined(MSDOS) || defined(__MSDOS__) || defined(_MSDOS) || defined(__DOS__)
inline constexpr auto Current  = MSDOS;
#elif defined(__NetBSD__)
inline constexpr auto Current  = NetBSD;
#elifdef __TANDEM
inline constexpr auto Current  = NonStop;
#elifdef __nucleus__
inline constexpr auto Current  = NucleusRTOS;
#elifdef __OpenBSD__
inline constexpr auto Current  = OpenBSD;
#elif defined(OS2) || defined(_OS2) || defined(__OS2__) || defined(__TOS_OS2__)
inline constexpr auto Current = OS2;
#elifdef __palmos__
inline constexpr auto Current = PalmOS;
#elifdef EPLAN9
inline constexpr auto Current = Plan9;
#elifdef pyr
inline constexpr auto Current = Pyramid;
#elif defined(__QNX__) || defined(__QNXNTO__)
inline constexpr auto Current = QNX;
#elifdef sinux
inline constexpr auto Current = ReliantUNIX;
#elif defined(sun) || defined(__sun)
#if defined(__SVR4) || defined(__svr4__)
inline constexpr auto Current = Solaris;
#else
inline constexpr auto Current = SunOS;
#endif
#elifdef __VOS__
inline constexpr auto Current = StratusVOS;
#elifdef __SYLLABLE__
inline constexpr auto Current = Syllable;
#elifdef __SYMBIAN32__
inline constexpr auto Current = SymbianOS;
#elif defined(__osf__) || defined(__osf)
inline constexpr auto Current = Tru64;
#elif defined(ultrix) || defined(__ultrix) || defined(__ultrix__) || (defined(unix) && defined(vax))
inline constexpr auto Current = Ultrix;
#elifdef _UNICOS
inline constexpr auto Current = UNICOS;
//TODO: version
#elif defined(_CRAY) || defined(__crayx1)
inline constexpr auto Current = UNICOS_MP;
#elif defined(sco) || defined(_UNIXWARE7)
inline constexpr auto Current = UNIXWare;
#elifdef _UWIN
inline constexpr auto Current = UWin;
#elif defined(VMS) || defined(__VMS)
inline constexpr auto Current = VMS;
#elif defined(__VXWORKS__) || defined(__vxworks__)
//TODO: version
#elifdef _WIN32_WCE
//TODO: version
inline constexpr auto Current = WindowsCE:inline constexpr auto Current = VxWorks;
#elifdef _WIN16
inline constexpr auto Current = Win16;
#elif defined(_WIN32) || defined(__WIN32__)
inline constexpr auto Current = Win32;
#elifdef _WIN64
inline constexpr auto Current = Win64;
#elif defined(_WINDU_SOURCE)
inline constexpr auto Current = WindU;
#elifdef __HOS_MVS__
inline constexpr auto Current = ZOS_Host;
#elifdef __TOS_MVS__
inline constexpr auto Current = ZOS_Target;
#elifdef __MVS__
inline constexpr auto Current = ZOS;
#elifdef __linux__
inline constexpr auto Current = Linux;
#elifdef __unix__
inline constexpr auto Current = Unix;
#elifdef
#endif

using Current_t = decltype(auto{Current});

#ifndef PSPP_CUSTOM_OSV_
inline constexpr Current_t CurrentV = Current;
#endif

using CurrentV_t = decltype(auto{CurrentV});

template <c_os auto OST>
inline constexpr bool is_on_v = Current == OST;

template <c_os auto OST>
inline constexpr bool is_not_on_v = Current != OST;

template <c_versioned_os auto OSVT>
inline constexpr bool is_onv_v = CurrentV == OSVT;

template <c_versioned_os auto OSVT>
inline constexpr bool is_not_onv_v = CurrentV != OSVT;

template <c_os auto ... OSTs>
inline constexpr bool is_on_some_of_v = (... or is_on_v<OSTs>);

template <c_versioned_os auto ... OSTs>
inline constexpr bool is_on_some_ofv_v = (... or is_onv_v<OSTs>);

template <c_os auto ... OSTs>
inline constexpr bool is_not_on_any_of_v = (... and is_not_on_v<OSTs>);

template <c_versioned_os auto ... OSVTs>
inline constexpr bool is_not_on_any_ofv_v = (... and is_not_on_v<OSVTs>);

template <auto ... t_os>
constexpr bool is_on() noexcept { return (... or (Current == t_os)); }

template <auto ... t_os>
constexpr bool is_not_on() noexcept { return not is_on<t_os...>(); }


template <typename T>
concept c_current_os = std::same_as<T, Current_t>;

template <typename T>
concept c_current_osv = std::same_as<T, CurrentV_t>;

template <typename T>
concept c_current_os_like = is_on_v<T{}>;

template <typename T>
concept c_current_osv_like = is_onv_v<T{}>;

struct arch_
{};

template <typename T>
concept c_arch = std::derived_from<T, arch_>;

template <c_arch T>
constexpr bool operator==(T, T) noexcept { return true; }
template <c_arch T, c_arch U>
constexpr bool operator==(T, U) noexcept { return false; }
template <c_arch T>
constexpr bool operator!=(T, T) noexcept { return false; }
template <c_arch T, c_arch U>
constexpr bool operator!=(T, U) noexcept { return true; }

inline constexpr struct arm_t : arch_ { constexpr static bool pspp_is_arm = true; } arm {};

template <typename T>
concept c_arm = requires { requires T::pspp_is_arm; };


inline constexpr struct x86_64_t : arch_ {} x86_64 {};
inline constexpr struct x86_32_t : arch_ {} x86_32 {};
inline constexpr struct arm2_t : arm_t {} arm2 {};
inline constexpr struct arm3_t : arm_t {} arm3 {};
inline constexpr struct arm4t_t : arm_t {} arm4t {};
inline constexpr struct arm5_t : arm_t {} arm5 {};
inline constexpr struct arm6_t : arm_t {} arm6 {};
inline constexpr struct arm6t2_t : arm_t {} arm6t2 {};
inline constexpr struct arm7_t : arm_t {} arm7 {};
inline constexpr struct arm7a_t : arm_t {} arm7a {};
inline constexpr struct arm7r_t : arm_t {} arm7r {};
inline constexpr struct arm7m_t : arm_t {} arm7m {};
inline constexpr struct arm7s_t : arm_t {} arm7s {};
inline constexpr struct arm64_t : arm_t {} arm64 {};
inline constexpr struct mips_arch_t : arch_ {} mips_arch {};
inline constexpr struct superh_t : arch_ {} superh {};
inline constexpr struct powerpc_t : arch_ {} powerpc {};
inline constexpr struct powerpc64_t : arch_ {} powerpc64 {};
inline constexpr struct sparc_t : arch_ {} sparc {};
inline constexpr struct m68k_t : arch_ {} m68k {};

#if defined(__x86_64__) || defined(_M_X64)
inline constexpr auto current_arch = x86_64;
#elif defined(i386) || defined(__i386) || defined(_M_IX86)
inline constexpr auto current_arch = x86_32;
#elif defined(__ARM_ARCH_2__)
inline constexpr auto current_arch = arm2;
#elif defined(__ARM_ARCH_3__) || defined(__ARM_ARCH_3M__)
inline constexpr auto current_arch = arm3;
#elif defined(__ARM_ARCH_4T__) || defined(__TARGET_ARM_4T)
inline constexpr auto current_arch = arm4t;
#elif defined(__ARM_ARCH_5_) || defined(__ARM_ARCH_5E_)
inline constexpr auto current_arch = arm5;
#elif defined(__ARM_ARCH_6T2_)
inline constexpr auto current_arch = arm6t2;
#elif defined(__ARM_ARCH_6__) || defined(__ARM_ARCH_6J__) || defined(__ARM_ARCH_6K__) ||\
      defined(__ARM_ARCH_6Z__) || defined(__ARM_ARCH_6ZK__)
inline constexpr auto current_arch = arm6;
#elif defined(__ARM_ARCH_7A__)
inline constexpr auto current_arch = arm7a;
#elif defined(__ARM_ARCH_7R__)
inline constexpr auto current_arch = arm7r;
#elif defined(__ARM_ARCH_7M__)
inline constexpr auto current_arch = arm7m;
#elif defined(__ARM_ARCH_7S__)
inline constexpr auto current_arch = arm7s;
#elif defined(__ARM_ARCH_7__)
inline constexpr auto current_arch = arm7;
#elif defined(__aarch64__) || defined(_M_ARM64)
inline constexpr auto current_arch = arm64;
#elif defined(mips) || defined(__mips__) || defined(__mips)
inline constexpr auto current_arch = mips_arch;
#elif defined(__sh__)
inline constexpr auto current_arch = superh;
#elif defined(__powerpc) || defined(__powerpc__) || defined(__powerpc64__) || defined(__POWERPC__) || defined(__ppc__) || defined(__PPC__) || defined(_ARCH_PPC)
inline constexpr auto current_arch = powerpc;
#elif defined(__PPC64__) || defined(__ppc64__) || defined(_ARCH_PPC64)
inline constexpr auto current_arch = powerpc64;
#elif defined(__sparc__) || defined(__sparc)
inline constexpr auto current_arch = sparc;
#elif defined(__m68k__)
inline constexpr auto current_arch = m68k;
#endif // arch

using current_arch_t = decltype(auto{current_arch});

constexpr bool operator==(arm_t, c_arm auto) noexcept { return true; }
constexpr bool operator==(c_arm auto, arm_t) noexcept { return true;}
constexpr bool operator!=(arm_t, c_arm auto) noexcept { return false; }
constexpr bool operator!=(c_arm auto, arm_t) noexcept { return false;}

template <typename OtherT> requires (not c_arm<OtherT>)
constexpr bool operator==(arm_t, OtherT) noexcept { return true; }
template <typename OtherT> requires (not c_arm<OtherT>)
constexpr bool operator==(OtherT, arm_t) noexcept { return true;}
template <typename OtherT> requires (not c_arm<OtherT>)
constexpr bool operator!=(arm_t, OtherT) noexcept { return false; }
template <typename OtherT> requires (not c_arm<OtherT>)
constexpr bool operator!=(OtherT, arm_t) noexcept { return false;}

// TODO : add compiler detection

using compiler_version_t = version_t<3>;
struct compiler_t
{
  compiler_version_t version {-1, -1, -1};
};

template <std::derived_from<compiler_t> CompT, std::size_t t_parts_count >
constexpr auto operator*(CompT comp, version_t<t_parts_count> ver) noexcept
{ return CompT{ver}; }


inline constexpr struct gxx_t :compiler_t {} gxx {};
inline constexpr struct clangxx_t : compiler_t {} clangxx {};
inline constexpr struct msvc_t : compiler_t {} msvc {};
inline constexpr struct apple_gxx_t : compiler_t {} apple_gxx {};
inline constexpr struct apple_clangxx_t : compiler_t {} apple_clangxx {};

inline constexpr struct edg_eccp_t : compiler_t {} edg_eccp {};

inline constexpr struct icc_t : compiler_t {} icc {};
inline constexpr struct icx_t : compiler_t {} icx {};

inline constexpr struct ibm_xl_C_t : compiler_t {} ibm_xl_C {};
inline constexpr struct ibm_xl_clang_t :compiler_t {} ibm_xl_clang {};
inline constexpr struct ibm_open_xl_cxx_t : compiler_t {} ibm_openxl_cxx {};

inline constexpr struct ibm_xl_C_for_aix_t : compiler_t {} ibm_xl_C_for_aix {};
inline constexpr struct ibm_xl_clang_for_aix_t :compiler_t {} ibm_xl_clang_for_aix {};
inline constexpr struct ibm_open_xl_cxx_for_aix_t : compiler_t {} ibm_openxl_cxx_for_aix {};

inline constexpr struct ibm_xl_C_for_zos_t : compiler_t {} ibm_xl_C_for_zos {};
inline constexpr struct ibm_xl_clang_for_zos_t :compiler_t {} ibm_xl_clang_for_zos {};
inline constexpr struct ibm_open_xl_cxx_for_zos_t : compiler_t {} ibm_openxl_cxx_for_zos {};

inline constexpr struct sun_oracle_cxx_t : compiler_t {} sun_oracle_cxx {};
inline constexpr struct embarcadero_cxx_builder_t : compiler_t {} embarcadero_cxx_builder_t {};
inline constexpr struct cray_t : compiler_t {} cray {};
inline constexpr struct nvidia_hpc_cxx_t : compiler_t {} nvidia_hpc_cxx {};
inline constexpr struct nvidia_nvcc_t : compiler_t {} nvidia_nvcc {};
// zig/misp

template <std::size_t ... t_is>
inline constexpr version_t<sizeof...(t_is)> ver = version_t{t_is...};


// TODO: add compilers version saving

// https://www.intel.com/content/www/us/en/developer/articles/guide/porting-guide-for-icc-users-to-dpcpp-or-icx.html
// https://www.intel.com/content/www/us/en/docs/cpp-compiler/developer-guide-reference/2021-10/additional-predefined-macros.html
// https://www.ibm.com/docs/en/xl-c-and-cpp-aix/16.1?topic=macros-identify-xl-cc-compiler
// https://www.ibm.com/docs/en/openxl-c-and-cpp-lop/17.1.1?topic=infrastructure-compiler-macros
// https://docwiki.embarcadero.com/RADStudio/Athens/en/Predefined_Macros
// https://support.hpe.com/hpesc/public/docDisplay?docId=a00113894en_us&docLocale=en_US&page=Predefined_Macro_Use.html#macros-based-on-the-compiler
// https://docs.nvidia.com/hpc-sdk/compilers/hpc-compilers-ref-guide/index.html#unique_1587248234
// https://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/index.html#nvcc-identification-macro
// https://docs.oracle.com/cd/E19205-01/820-7599/6nirkt75m/index.html#bkaoe
// https://www.intel.com/content/www/us/en/docs/dpcpp-cpp-compiler/developer-guide-reference/2024-0/use-predefined-macros-to-specify-intel-compilers.html



#if defined(__EDG__) && !defined(PSPP_IGNORE_EDG)
inline constexpr auto current_comp = edg_eccp_t{{{__EDG_VERSION__}}};
#elif __INTEL_LLVM_COMPILER
inline constexpr auto current_comp = icx;
#elifdef __INTEL_COMPILER
inline constexpr auto current_comp = icc;
#elifdef __IBMCPP__
inline constexpr auto current_comp = []{
  static constexpr compiler_version_t ver { __xlC__ >> 2, __xlC__ & 0x00FF, __xlC_ver__ >> 2 };
  if constexpr (is_on<ZOS>()) return ibm_xl_C_for_zos_t{{ver}};
  else if constexpr (is_on<AIX>()) return ibm_xl_C_for_aix_t{{ver}};
  else return ibm_xl_C_t{{ver}};
}();
#elifdef __ibmxl__
inline constexpr auto current_comp = []{
  static constexpr compiler_version_t ver {__ibmxl_version__, __ibmxl_release__, __ibmxl_modification__};
  if constexpr (is_on<ZOS>()) return ibm_xl_clang_for_zos_t{{ver}};
  else if constexpr (is_on<AIX>()) return ibm_xl_clang_for_aix_t{{ver}};
  else return ibm_xl_clang_t{{ver}};
}();
#elifdef __open_xl__
inline constexpr auto current_comp = []{
  static constexpr compiler_version_t ver {__openxl_version__, __openxl_release__, __openxl_modification__};
  if constexpr (is_on<ZOS>()) return ibm_open_xl_cxx_for_zos_t{{ver}};
  else if constexpr (is_on<AIX>()) return ibm_open_xl_cxx_for_aix_t{{ver}};
  else return ibm_open_xl_cxx_t{{ver}};
}();
#elifdef __CODEGEARC__
inline constexpr auto current_comp = embarcadero_cxx_builder_t{{{
  (__CODEGEARC_VERSION__ & 0xFF000000) >> 24,
  (__CODEGEARC_VERSION__ & 0x00FF0000) >> 16,
  (__CODEGEARC_VERSION__ & 0x0000FFFF)
}}};
#elif _CRAYC
inline constexpr auto current_comp = cray_t{{{_RELEASE_MAJOR, _RELEASE_MINOR, _RELEASE_PATCHLEVEL}}};
#elifdef __NVCOMPILER
inline constexpr auto current_comp = nvidia_hpc_cxx_t{{{__NVCOMPILER_MAJOR, __NVCOMPILER_MINOR, __NVCOMPILER_PATCHLEVEL}}};
#elifdef __NVCC__
inline constexpr auto current_comp = nvidia_nvcc_t{{{__CUDACC_VER_MAJOR__, __CUDACC_VER_MINOR__, __CUDACC_VER_BUILD__}}};
#elifdef __SUNPRO_CC
inline constexpr auto current_comp = sun_oracle_cxx_t{{{__SUNPRO_CC}}};
#elifdef __clang__
#ifdef __APPLE_CC__
inline constexpr auto current_comp = apple_clangxx_t{{{__clang_major__, __clang_minor__, __clang_patchlevel__}}};
#else
inline constexpr auto current_comp = clangxx_t{{{__clang_major__, __clang_minor__, __clang_patchlevel__}}};
#endif
#elifdef _MSC_VER
inline constexpr auto current_comp = msvc_t{{{_MSC_VER >> 2, _MSC_VER & 0x00FF, }}};
#elifdef __GNUG__
#ifdef __APPLE_CC__
inline constexpr auto current_comp = apple_gxx_t{{{__GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__}}};
#else
inline constexpr auto current_comp = gxx_t{{{__GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__}}};
#endif
#endif
using current_comp_t = decltype(auto{current_comp});

template <typename T>
concept c_compiler = std::derived_from<T, compiler_t>;

template <c_compiler T>
constexpr bool operator==(T const l, T const r) noexcept
{ return l.version == r.version; }

template <c_compiler T, c_compiler U>
constexpr bool operator==(T const l, U const r) noexcept
{ return false; }

template <c_compiler T>
constexpr bool operator>(T const l, T const r) noexcept
{ return l.version > r.version; }

template <c_compiler T, c_compiler U>
constexpr bool operator>(T const l, U const r) noexcept
{ return false; }

template <c_compiler T>
constexpr bool operator<(T const l, T const r) noexcept
{ return l.version < r.version; }

template <c_compiler T, c_compiler U>
constexpr bool operator<(T const l, U const r) noexcept
{ return false; }

template <c_compiler T>
constexpr bool operator>=(T const l, T const r) noexcept
{ return l.version >= r.version; }

template <c_compiler T, c_compiler U>
constexpr bool operator>=(T const l, U const r) noexcept
{ return false; }

template <c_compiler T>
constexpr bool operator<=(T const l, T const r) noexcept
{ return l.version >= r.version; }

template <c_compiler T, c_compiler U>
constexpr bool operator<=(T const l, U const r) noexcept
{ return false; }

template <c_compiler T>
constexpr bool operator!=(T const l, T const r) noexcept
{ return l.version != r.version; }

template <c_compiler T, c_compiler U>
constexpr bool operator!=(T const l, U const r) noexcept
{ return true; }


static_assert(version_t{1, 2, 231} < version_t{1, 2, 233});
static_assert(version_t{1, 2, 231} > version_t{1, 1});
static_assert(version_t{3, 123, 2} <= version_t{3, -1, 3});

} // namespace pspp::pc

#endif // PSPP_PLATFORM_CHECKER_HPP
