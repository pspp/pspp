#ifndef PSPP_CUSTOM_STRUCT_HPP
#define PSPP_CUSTOM_STRUCT_HPP

#include <pspp/meta.hpp>
#include <pspp/constr.hpp>
#include <pspp/with_reason.hpp>
#include <pspp/utility/utility.hpp>

namespace pspp
{
inline namespace meta
{
	template <typename Data, typename Target>
	concept c_assignable_to = requires(Target t, Data d)
	{
		t = d;
	};

	template <auto t_key, typename ValT>
	struct custom_value_container 
	{
		using value_type = ValT;
		using key_holder = value_holder<t_key>;

		ValT m_value;

		[[pspp_attr_always_inline, nodiscard]] inline ValT & operator()(key_holder) noexcept
		{
			return m_value;
		}

		[[pspp_attr_always_inline, nodiscard]] inline ValT const & operator()(key_holder) const noexcept
		{
			return m_value;
		}


		template <c_assignable_to<ValT> DataT>
		[[pspp_attr_always_inline]] inline void operator()(key_holder, DataT && data) noexcept
		{
			m_value = std::forward<DataT>(data);
		}
	};

	template <constr t_name, typename ValT>
	using named_custom_value_container = custom_value_container<t_name, ValT>;

	template <typename T>
	struct is_custom_value_container : std::false_type {};

	template <auto t_key, typename ValT>
	struct is_custom_value_container<custom_value_container<t_key, ValT>> : std::true_type {};

	template <typename Specified, typename T>
	struct is_custom_value_container_with_type : std::false_type {};

	template <typename Specified, Specified t_key, typename ValT>
	struct is_custom_value_container_with_type<Specified, custom_value_container<t_key, ValT>> : std::true_type {};

	template <typename T>
	struct is_named_custom_value_container : std::false_type {};

	template <constr t_name, typename ValT>
	struct is_named_custom_value_container<custom_value_container<t_name, ValT>> : std::true_type {};

	template <typename T>
	concept c_custom_value_container = is_custom_value_container<T>::value;

	template <typename T, typename Specified>
	concept c_custom_value_container_with_type = is_custom_value_container_with_type<Specified, T>::value;

	template <typename T>
	concept c_named_custom_value_container = is_named_custom_value_container<T>::value;

	template <typename T>
	concept c_not_cstr = not c_constr<T>;

	template <typename ... t_data>
	struct custom_struct
	{};

	template <c_custom_value_container ... Datas>
	struct custom_struct<Datas...> : Datas...
	{
		static_assert(value_sequence<Datas::key_holder::value...>::is_unique, "custom_struct: keys must be unique");
		using Datas::operator()...;
		using types = type_sequence<typename Datas::value_type...>;
		using references = type_sequence<typename Datas::value_type & ...>;
		using const_references = type_sequence<typename Datas::value_type & ...>;
		using keys = value_sequence<Datas::key_holder::value...>;

		template <auto t_key>
		[[nodiscard]] pspp_always_inline constexpr auto & get() noexcept
		{
			if constexpr (t_key < sizeof...(Datas))
				return operator()(typename keys::values::template at<t_key>{});
			else
				static_assert(keys::template contains<t_key>, "custom_struct: get(): t_key is wrong");
		}

		template <auto t_key>
		[[nodiscard]] pspp_always_inline constexpr auto & get() const noexcept
		{
			if constexpr (t_key < sizeof...(Datas))
				return operator()(typename keys::values::template at<t_key>{});
			else
				static_assert(keys::template contains<t_key>, "custom_struct: get(): t_key is wrong");
		}


		template <auto t_key>
			requires (not is_constr(t_key))
		[[pspp_attr_always_inline, nodiscard]] inline auto & get_at () noexcept
		{
			if constexpr (keys::template contains<t_key>)
				return operator()(value_holder<t_key>{});
			else if constexpr (std::integral<decltype(t_key)> and 
					t_key < sizeof...(Datas))
				return operator()(typename keys::values::template at<t_key>{});
			else
			{
				static_assert(keys::template contains<t_key>, "custom_struct: get_at(): no such key");
				return operator()(value_holder<keys::head>{});
			}
		}

		template <auto t_key>
			requires (not is_constr(t_key))
		[[pspp_attr_always_inline, nodiscard]] inline auto & get_at () const noexcept
		{
			if constexpr (keys::template contains<t_key>)
				return operator()(value_holder<t_key>{});
			else if constexpr (std::integral<decltype(t_key)> and 
					t_key < sizeof...(Datas))
				return operator()(typename keys::values::template at<t_key>{});

			else
			{
				static_assert(keys::template contains<t_key>, "custom_struct: get(): no such key");
				return operator()(value_holder<keys::head>{});
			}
		}


		template <auto t_first, auto t_second, auto ... t_keys>
			requires (not (is_constrs(t_first, t_second, t_keys...)))
		[[pspp_attr_always_inline, nodiscard]] inline auto tget () noexcept
		{
			return std::make_tuple(get_at<t_first>(), get_at<t_second>(), get_at<t_keys>()...);
		}

		template <pspp::constr t_first, pspp::constr t_second, pspp::constr ... t_keys>
		[[pspp_attr_always_inline, nodiscard]] inline auto tget () noexcept
		{
			return std::make_tuple(get_at<t_first>(), get_at<t_second>(), get_at<t_keys>()...);
		}

		[[nodiscard]] pspp_always_inline auto tget() noexcept
		{
			return std::tuple{get_at<Datas::key_holder::value>()...};
		}

		[[nodiscard]] pspp_always_inline auto tget() const noexcept
		{
			return std::tuple{get_at<Datas::key_holder::value>()...};
		}

		template <constr t_name>
		[[pspp_attr_always_inline, nodiscard]] inline auto & get_at() noexcept
		{
			if constexpr (keys::template contains<t_name>)
				return operator()(value_holder<t_name>{});
			else
			{
				static_assert(keys::template contains<t_name>, "custom_struct: get(): no such key");
				return operator()(value_holder<keys::head>{});
			}
		}

		template <constr t_name>
		[[pspp_attr_always_inline, nodiscard]] inline auto & get_at() const noexcept
		{
			if constexpr (keys::template contains<t_name>)
				return operator()(value_holder<t_name>{});
			else
			{
				static_assert(keys::template contains<t_name>, "custom_struct: get(): no such key");
				return operator()(value_holder<keys::head>{});
			}
		}


/*		pspp_always_inline constexpr operator typename references::template passed_type<std::tuple>() const noexcept 
		{
			return tget<Datas::key_holder::value...>();
		}
		*/

		template <auto ... t_key>
			requires (not is_constrs(t_key...))
		[[pspp_attr_always_inline]] inline void set(auto && ... data) noexcept 
			requires (
					with_reason<sizeof...(t_key) == sizeof...(data), 
						"count of values to be assigned must be the same as count of keys">
					)
		{
			(..., []{ static_assert(keys::template contains<t_key>, "custom_struct: set(): no such key"); }());
			if constexpr (keys::template contains<t_key...>)
			{
				(operator()(value_holder<t_key>{}, std::forward<decltype(data)>(data)), ...);
			}
			else
				return operator()(value_holder<keys::head>{});
		}

		template <constr ... t_names>
		[[pspp_attr_always_inline]] inline void set(auto && ... data) noexcept
			requires (
					with_reason<sizeof...(t_names) == sizeof...(data), 
						"count of values to be assigned must be the same as count of keys">
					)
		{
			(..., []{ static_assert(keys::template contains<t_names>, "custom_struct: set(): no such key"); }());
			if constexpr (keys::template contains<t_names...>)
			{
				(operator()(value_holder<t_names>{}, std::forward<decltype(data)>(data)), ...);
			}
			else
				return operator()(value_holder<keys::head>{});

		}
	};


	template <typename SpecifiedT, c_custom_value_container_with_type<SpecifiedT> ... Datas>
	struct typed_custom_struct : Datas...
	{
		using Datas::operator()...;
		using types = type_sequence<typename Datas::value_type...>;

		template <SpecifiedT t_key>
		[[pspp_attr_always_inline, nodiscard]] inline auto & get_at() noexcept
		{
			return operator()(value_holder<t_key>{});
		}

		template <SpecifiedT ... t_key>
		[[pspp_attr_always_inline]] inline void set(auto && ... data) noexcept 
			requires (
					with_reason<sizeof...(t_key) == sizeof...(data), 
						"count of values to be assigned must be the same as count of keys"> and
					(not is_constrs(t_key...))
					)
		{
			(operator()(value_holder<t_key>{}, std::forward<decltype(data)>(data)), ...);
		}
	};

}//inline namespace meta
 //
namespace detail
{
template <c_value_sequence Keys, c_type_sequence Types>
struct custom_struct_from_impl
{
	using combined = combine<Keys, Types>;
	using containers_seq = typename combined::template transform<
			[]<typename T> { 
			return change_to<custom_value_container< T::head::value, typename T::template at<1>>>;}
		>;

	using res = extract_type<[]<typename ... Ts>(type_sequence<Ts...>)
	{return type<custom_struct<Ts...>>; }(containers_seq{})>;
};
} // namespace detail

inline namespace meta
{

	/*template <c_value_sequence Keys, c_type_sequence Types>
	using custom_struct_from = typename combine<Keys, Types>::
	template transform< []<typename T> { 
		return change_to<custom_value_container< T::head::value, typename T::template at<1>>>;}
	>::template passed_type<custom_struct>;
	*/
	template <c_value_sequence Keys, c_type_sequence Types>
	using custom_struct_from = typename detail::custom_struct_from_impl<Keys, Types>::res;

	template <c_type_sequence Types, std::size_t t_start = 0>
	using enumerated_custom_struct_from = custom_struct_from<vindexes_range<t_start, Types::count + t_start>, Types>;

	template <typename SpecifiedT, c_value_sequence Keys, c_type_sequence Types>
	using typed_custom_struct_from = typename combine<Keys, Types>::template transform<
		[]<typename T> { return change_to<custom_value_container<T::head::value, typename T::template at<1>>>; }
	>::template append_types_to_front<SpecifiedT>::template passed_type<typed_custom_struct>;

	template <pspp::constr t_name, typename DataT>
	inline constexpr custom_value_container<t_name, std::remove_cvref_t<DataT>> make_named_value(DataT && data) noexcept
	{
		return {std::forward<DataT>(data)}; 
	}

	template <constr ... t_strs, typename ... DataTs>
	inline constexpr custom_struct<custom_value_container<t_strs, DataTs>...> make_custom_struct_of_named(
			DataTs && ... datas) noexcept
	requires (sizeof...(t_strs) == sizeof...(DataTs))
	{
		return {std::forward<DataTs>(datas)...};
	}

	template <constr ... t_strs, typename ... DataTs>
	inline constexpr custom_struct<custom_value_container<t_strs, DataTs>...> make_custom_struct_of_named(
			custom_value_container<t_strs, DataTs> const & ... datas) noexcept
	requires (sizeof...(t_strs) == sizeof...(DataTs))
	{
		return {datas...};
	}

	template <constr ... t_strs, typename ... DataTs>
	inline constexpr custom_struct<custom_value_container<t_strs, DataTs>...> make_custom_struct_of_named(
			custom_value_container<t_strs, DataTs> && ... datas) noexcept
	requires (sizeof...(t_strs) == sizeof...(DataTs))
	{
		return {std::move(datas)...};
	}


	template <typename T>
	concept c_custom_struct = pspp::is_complex_target_t<custom_struct>::is_target<std::remove_cvref_t<T>>::value;

} // inline namespace meta
} // namespace pspp

namespace std
{
	template <pspp::c_custom_value_container ... Datas>
	struct tuple_size<pspp::custom_struct<Datas...>> :
		std::integral_constant<std::size_t, sizeof...(Datas)> {};

	template <std::size_t t_n, pspp::c_custom_value_container ... Datas>
	struct tuple_element<t_n, pspp::custom_struct<Datas...>>
	{
		using type = typename pspp::custom_struct<Datas...>::types::template at<t_n>;
	};

	template <std::size_t t_n, pspp::c_custom_struct CustomStruct>
	constexpr auto get(CustomStruct && data) noexcept
	{
		using pure_type = std::remove_cvref_t<CustomStruct>;
		using to_get_type = typename pure_type::types::template at<t_n>;
		static constexpr auto key = pure_type::keys::template at<t_n>;

		return std::forward<pspp::copy_extensions_t<CustomStruct, to_get_type>>(
				std::forward<CustomStruct>(data).template get_at<key>());
	}
} // namespace std

#endif // PSPP_CUSTOM_STRUCT_HPP
