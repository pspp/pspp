#ifndef PSPP_CUSTOM_STRUCT_HPP
#define PSPP_CUSTOM_STRUCT_HPP

#include <pspp/adaptors.hpp>
#include <pspp/meta.hpp>
#include <pspp/constr.hpp>
#include <pspp/with_reason.hpp>
#include <pspp/utility/utility.hpp>
#include <pspp/type_name.hpp>
#include <pspp/useful/sequence.hpp>

namespace pspp
{
	template <typename Data, typename Target>
	concept c_assignable_to = requires(Target t, Data d)
	{
		t = d;
	};

	template <auto t_key, typename ValT>
	struct cvc_t
	{
		using value_type = ValT;
		using key_holder = value_holder<t_key>;

		ValT m_value;

		[[pspp_attr_always_inline, nodiscard]]
    constexpr ValT & operator()(key_holder) noexcept
		{
			return m_value;
		}

		[[pspp_attr_always_inline, nodiscard]]
    constexpr ValT const & operator()(key_holder) const noexcept
		{
			return m_value;
		}


		template <c_assignable_to<ValT> DataT>
		[[pspp_attr_always_inline]]
    constexpr void operator()(key_holder, DataT && data) noexcept
		{
			m_value = std::forward<DataT>(data);
		}
	};

  template <auto t_key, typename DataT>
  using custom_value_container = cvc_t<t_key, DataT>;

	template <constr t_name, typename ValT>
	using named_cvc_t = cvc_t<t_name, ValT>;

	template <typename T>
	struct is_cvc_t : std::false_type {};

	template <auto t_key, typename ValT>
	struct is_cvc_t<cvc_t<t_key, ValT>> : std::true_type {};

	template <typename Specified, typename T>
	struct is_cvc_t_with_type : std::false_type {};

	template <typename Specified, Specified t_key, typename ValT>
	struct is_cvc_t_with_type<Specified, cvc_t<t_key, ValT>> : std::true_type {};

	template <typename T>
	struct is_named_cvc_t : std::false_type {};

	template <constr t_name, typename ValT>
	struct is_named_cvc_t<cvc_t<t_name, ValT>> : std::true_type {};

	template <typename T>
	concept c_cvc_t = is_cvc_t<T>::value;

	template <typename T, typename Specified>
	concept c_cvc_t_with_type = is_cvc_t_with_type<Specified, T>::value;

	template <typename T>
	concept c_named_cvc_t = is_named_cvc_t<T>::value;

	template <typename T>
	concept c_not_cstr = not c_constr<T>;

	template <typename ... CustomValContTs>
	struct custom_struct
	{};

namespace detail
{
template <c_value_sequence auto t_keys, auto t_types>
struct custom_struct_from_impl
{
	template <typename T>
	struct to_container
	{
		static_val v = (T{} | vhead).value;
		//static_val _ = T{} | at<1>;

		using data_type = typename decltype(T{} | at<1>)::t;
		using type = cvc_t<v, data_type>;
	};

	static_val combined = combine(t_keys, t_types);
	static_val containers_seq = combined | ttransform<to_container>;
	
	using res = extract_type<containers_seq | pass_to<custom_struct>>;
};
} // namespace detail

	template <typename SpecifiedT, c_cvc_t_with_type<SpecifiedT> ... Datas>
	struct typed_custom_struct : Datas...
	{
		using Datas::operator()...;
		using types = type_sequence<typename Datas::value_type...>;

		template <SpecifiedT t_key>
		[[pspp_attr_always_inline, nodiscard]] inline auto & get_at() noexcept
		{
			return operator()(value_holder<t_key>{});
		}

		template <SpecifiedT ... t_key>
		[[pspp_attr_always_inline]] inline void set(auto && ... data) noexcept 
			requires (
					with_reason<sizeof...(t_key) == sizeof...(data), 
						constr{"count of values to be assigned must be the same as count of keys"}> and
					(not is_constrs(t_key...))
					)
		{
			(operator()(value_holder<t_key>{}, std::forward<decltype(data)>(data)), ...);
		}
	};


	template <c_value_sequence auto t_keys, c_type_sequence auto t_types>
	using custom_struct_fromv = typename detail::custom_struct_from_impl<t_keys, t_types>::res;

	template <c_value_sequence Keys, c_type_sequence Types>
	using custom_struct_from = typename detail::custom_struct_from_impl<Keys{}, Types{}>::res;


	template <c_type_sequence Types, std::size_t t_start = 0>
	using enumerated_custom_struct_from = custom_struct_fromv<vs_indexes_range<t_start, Types::size + t_start>, Types{}>;

	template <c_type_sequence auto t_types, std::size_t t_start = 0>
	using enumerated_custom_struct_fromv = custom_struct_fromv<vs_indexes_range<t_start, t_types.size + t_start>, t_types>;



	template <typename SpecifiedT, c_value_sequence auto t_keys, c_type_sequence auto t_types>
	using typed_custom_struct_from = extract_type<
		combine(t_keys, t_types) | transform<
			[]<typename T> { return type<cvc_t<(T{} | vhead).value, extract_type<T{} | at<1>>>>; }
		> | append_types_to_front<SpecifiedT> | pass_to<typed_custom_struct>
	>;

	template <pspp::constr t_name, typename DataT>
	inline constexpr cvc_t<t_name, std::remove_cvref_t<DataT>> make_named_value(DataT && data) noexcept
	{
		return {std::forward<DataT>(data)}; 
	}

	template <constr ... t_strs, typename ... DataTs>
	inline constexpr custom_struct<cvc_t<t_strs, DataTs>...> make_custom_struct_of_named(
			DataTs && ... datas) noexcept
	requires (sizeof...(t_strs) == sizeof...(DataTs))
	{
		return {std::forward<DataTs>(datas)...};
	}

	template <constr ... t_strs, typename ... DataTs>
	inline constexpr custom_struct<cvc_t<t_strs, DataTs>...> make_custom_struct_of_named(
			cvc_t<t_strs, DataTs> const & ... datas) noexcept
	requires (sizeof...(t_strs) == sizeof...(DataTs))
	{
		return {datas...};
	}

	template <constr ... t_strs, typename ... DataTs>
	inline constexpr custom_struct<cvc_t<t_strs, DataTs>...> make_custom_struct_of_named(
			cvc_t<t_strs, DataTs> && ... datas) noexcept
	requires (sizeof...(t_strs) == sizeof...(DataTs))
	{
		return {std::move(datas)...};
	}

	template <constr t_name, typename DataT>
	constexpr auto namedval(DataT && val) noexcept
	{ return cvc_t<t_name, DataT>{pspp_fwd(val)}; }

  template <auto t_key, typename DataT>
    requires (not is_constr(t_key))
  constexpr auto namedval(DataT && val) noexcept
  { return cvc_t<t_key, DataT>{pspp_fwd(val)}; }

	template <c_cvc_t ... Datas>
	struct custom_struct<Datas...> : Datas...
	{
		static_val keys = valseq<Datas::key_holder::value...>;
		static_assert(keys | is_unique, "custom_struct: keys must be unique");
		using Datas::operator()...;
		static_val types = type_sequence<typename Datas::value_type...>{};
		static_val references = typeseq<typename Datas::value_type & ...>;
		static_val const_references = typeseq<typename Datas::value_type const & ...>;

		template <auto t_key>
		static_val key_index = keys.tsv | get_last_index_of<value_holder<t_key>>;

		template <auto t_key>
		static_val value_type_at = types | ::pspp::at<key_index<t_key>>;

    template <auto t_key>
    using value_type_at_t = decltype(types | ::pspp::atv<key_index<t_key>>);

    template <auto t_ind>
    using self_at = extract_type<typeseq<Datas...> | at<t_ind>>;

		template <auto t_key>
		[[nodiscard]] pspp_always_inline constexpr auto & get() noexcept
		{
			if constexpr (t_key < sizeof...(Datas))
				return operator()(keys.tsv | atv<t_key>);
			else
				static_assert(keys | vcontains<t_key>, "custom_struct: get(): t_key is wrong");
		}

		template <auto t_key>
		[[nodiscard]] pspp_always_inline constexpr auto & get() const noexcept
		{
			if constexpr (t_key < sizeof...(Datas))
				return operator()(keys.tsv | atv<t_key>);
			else
				static_assert(keys | vcontains<t_key>, "custom_struct: get(): t_key is wrong");
		}


		template <auto t_key>
			requires (not is_constr(t_key) and not c_constr_sequence<clean_type<t_key>>)
		[[pspp_attr_always_inline, nodiscard]] inline constexpr auto & get_at () noexcept
      requires (keys | vcontains<t_key>)
		{ return operator()(val<t_key>); }

		template <auto t_key>
			requires (not is_constr(t_key) and not c_constr_sequence<clean_type<t_key>>)
		[[pspp_attr_always_inline, nodiscard]] inline constexpr auto & get_at () const noexcept
      requires (keys | vcontains<t_key>)
		{ return operator()(value_holder<t_key>{}); }

    template <constr t_name>
    [[pspp_attr_always_inline, nodiscard]] inline constexpr auto & get_at() noexcept
      requires (keys | vcontains<t_name> )
    { return operator()(value_holder<t_name>{}); }

    template <constr t_name>
    [[pspp_attr_always_inline, nodiscard]] inline constexpr auto & get_at() const noexcept
      requires (keys | vcontains<t_name> )
    { return operator()(value_holder<t_name>{}); }

    template <constr_sequence t_keys>
    pspp_always_inline [[nodiscard]] constexpr auto & get_at() noexcept
      requires ((t_keys | get_size) > 0)
    {
      return []<constr ... t_keys_> pspp_always_inline
          (auto & self, constr_sequence<t_keys_...>) -> auto &
      {
        return self.template at<t_keys_...>();
      }(*this, t_keys);
    }

    template <constr_sequence t_keys>
    pspp_always_inline [[nodiscard]] constexpr auto const & get_at() const noexcept
      requires ((t_keys | get_size) > 0 and keys | vcontains<t_keys | at<0>>)
    {
      return []<constr ... t_keys_> pspp_always_inline
          (auto const & self, constr_sequence<t_keys_...>) -> auto const &
      {
        return self.template at<t_keys_...>();
      }(*this, t_keys);
    }

		template <auto t_first, auto ... t_keys>
		  requires (not is_constrs(t_first, t_keys...))
		[[nodiscard]] pspp_always_inline constexpr auto & at() noexcept
      requires (keys | vcontains<t_first>)
		{
      if constexpr (sizeof...(t_keys) == 0)
        return get_at<t_first>();
      else return get_at<t_first>().template at<t_keys...>();
    }

    template <auto t_first, auto ... t_keys>
      requires (not is_constrs(t_first, t_keys...))
    [[nodiscard]] pspp_always_inline constexpr auto const & at() const noexcept
      requires (keys | vcontains<t_first>)
    {
      if constexpr (sizeof...(t_keys) == 0)
        return get_at<t_first>();
      else return get_at<t_first>().template at<t_keys...>();
    }

    template <constr t_first, constr ... t_keys>
    [[nodiscard]] pspp_always_inline constexpr auto & at() noexcept
      requires (keys | vcontains<t_first>)
    {
      if constexpr (sizeof...(t_keys) == 0)
        return get_at<t_first>();
      else return get_at<t_first>().template at<t_keys...>();
    }

    template <constr t_first, constr ... t_keys>
    [[nodiscard]] pspp_always_inline constexpr auto const & at() const noexcept
      requires (keys | vcontains<t_first>)
    {
      if constexpr (sizeof...(t_keys) == 0)
        return get_at<t_first>();
      else return get_at<t_first>().template at<t_keys...>();
    }

    template <auto t_first, auto ... t_keys>
    requires (not is_constrs(t_first))
    [[nodiscard]] pspp_always_inline constexpr auto && at_or(auto && value) noexcept
    {
      if constexpr (sizeof...(t_keys) == 0)
      {
        pspp_execute_and_return_if_can(get_at<t_first>());
        else return pspp_fwd(value);
      } else
      {
        pspp_execute_and_return_if_can(at<t_first>().template at_or<t_keys...>(value));
        else return pspp_fwd(value);
      }
    }

    template <auto t_first, auto ... t_keys>
    requires (not is_constrs(t_first))
    [[nodiscard]] pspp_always_inline constexpr auto && at_or(auto && value) const noexcept
    {
      if constexpr (sizeof...(t_keys) == 0)
      {
        pspp_execute_and_return_if_can(get_at<t_first>());
        else return pspp_fwd(value);
      } else
      {
        pspp_execute_and_return_if_can(at<t_first>().template at_or<t_keys...>(value));
        else return pspp_fwd(value);
      }
    }

    template <constr t_first, constr ... t_keys>
    [[nodiscard]] pspp_always_inline constexpr auto && at_or(auto && value) noexcept
    {
      if constexpr (sizeof...(t_keys) == 0)
      {
        pspp_execute_and_return_if_can(get_at<t_first>());
        else return pspp_fwd(value);
      } else
      {
        pspp_execute_and_return_if_can(at<t_first>().template at_or<t_keys...>(value));
        else return pspp_fwd(value);
      }
    }

    template <constr t_first, constr ... t_keys>
    [[nodiscard]] pspp_always_inline constexpr auto && at_or(auto && value) const noexcept
    {
      if constexpr (sizeof...(t_keys) == 0)
      {
        pspp_execute_and_return_if_can(get_at<t_first>());
        else return pspp_fwd(value);
      } else
      {
        pspp_execute_and_return_if_can(at<t_first>().template at_or<t_keys...>(value));
        else return pspp_fwd(value);
      }
    }

    template <auto t_key, auto ... t_rest>
      requires (not is_constrs(t_key, t_rest...))
    static constexpr bool has() noexcept
    {
      if constexpr (keys | vcontains<t_key>)
      {
        if constexpr (sizeof...(t_rest) == 0) return true;
        else pspp_execute_and_return_if_can(decltype(*value_type_at<t_key>)::template has<t_rest...>());
        else return false;
      }
      else return false;
    }

    template <constr t_key, constr ... t_rest>
    static constexpr bool has() noexcept
    {
      if constexpr (keys | vcontains<t_key>)
      {
        if constexpr (sizeof...(t_rest) == 0) return true;
        else pspp_execute_and_return_if_can(extract_type<value_type_at<t_key>>::template has<t_rest...>());
        else return false;
      }
      else return false;
    }


		template <auto t_key, template <auto> class HolderT>
		[[nodiscard]] pspp_always_inline constexpr auto & operator[](HolderT<t_key>) noexcept
		{ return get_at<t_key>(); }
		template <auto t_key, template <auto> class HolderT>
		[[nodiscard]] pspp_always_inline
		constexpr auto const & operator[](HolderT<t_key>) const noexcept
		{ return get_at<t_key>(); }



		template <auto t_first, auto t_second, auto ... t_keys>
			requires (not (is_constrs(t_first, t_second, t_keys...)))
		[[pspp_attr_always_inline, nodiscard]] inline auto tget () noexcept
		{
			return std::make_tuple(get_at<t_first>(), get_at<t_second>(), get_at<t_keys>()...);
		}

		template <pspp::constr t_first, pspp::constr t_second, pspp::constr ... t_keys>
		[[pspp_attr_always_inline, nodiscard]] inline constexpr auto tget () noexcept
		{
			return std::make_tuple(get_at<t_first>(), get_at<t_second>(), get_at<t_keys>()...);
		}

		[[nodiscard]] pspp_always_inline constexpr auto tget() noexcept
		{
			return std::tuple{get_at<Datas::key_holder::value>()...};
		}

		[[nodiscard]] pspp_always_inline constexpr auto tget() const noexcept
		{
			return std::tuple{get_at<Datas::key_holder::value>()...};
		}

/*		pspp_always_inline constexpr operator typename references::template passed_type<std::tuple>() const noexcept 
		{
			return tget<Datas::key_holder::value...>();
		}
		*/

		template <auto ... t_key>
			requires (not is_constrs(t_key...))
		[[pspp_attr_always_inline]] inline constexpr void set(auto && ... data) noexcept 
			requires (
					with_reason<sizeof...(t_key) == sizeof...(data), 
						constr{"count of values to be assigned must be the same as count of keys"}>
					)
		{
			(..., []{ static_assert(keys | vcontains<t_key>, "custom_struct: set(): no such key"); }());
			if constexpr (keys | vcontains<t_key...>)
			{
				(operator()(value_holder<t_key>{}, std::forward<decltype(data)>(data)), ...);
			}
			else
				return operator()(keys.tsv | vhead);
		}

		template <constr ... t_names>
		[[pspp_attr_always_inline]] inline constexpr void set(auto && ... data) noexcept
			requires (
					with_reason<sizeof...(t_names) == sizeof...(data), 
						constr{"count of values to be assigned must be the same as count of keys"}>
					)
		{
			(..., []{ static_assert(keys | vcontains<t_names>, "custom_struct: set(): no such key"); }());
			if constexpr (keys | vcontains<t_names...>)
			{
				(operator()(value_holder<t_names>{}, std::forward<decltype(data)>(data)), ...);
			}
			else
				return operator()(keys.tsv | vhead);

		}

    [[nodiscard]] pspp_always_inline
    static constexpr std::size_t size() noexcept
    { return keys | get_size; }

    inline static constexpr auto indices = std::make_index_sequence<keys | get_size>{};

		constexpr custom_struct() = default;

		constexpr custom_struct(c_value_sequence auto, c_type_sequence auto):
			Datas{}...
		{}

		constexpr custom_struct(auto && ... vals) requires(sizeof...(vals) == sizeof...(Datas)): 
			Datas{std::forward<decltype(vals)>(vals)}... {};

		template <auto ... t_keys, typename ... DataTs>
		constexpr custom_struct(cvc_t<t_keys, DataTs>... vals):
			Datas{vals.m_value}...
		{}

		constexpr custom_struct(c_value_sequence auto, c_type_sequence auto,
				auto && ... rest)
			requires (sizeof...(rest) == sizeof...(Datas)) :
			Datas{std::forward<decltype(rest)>(rest)}...
		{}

		template <auto t_key> requires (not is_constr<t_key>())
		static consteval auto get_member_ptr_to() noexcept
		{
			using value_type = typename decltype(value_type_at<t_key>)::t;
			return &cvc_t<t_key, value_type>::m_value;
		}

		template <constr t_key>
		static consteval auto get_member_ptr_to() noexcept
		{
			using value_type = extract_type<value_type_at<t_key>>;
			return &cvc_t<t_key, value_type>::m_value;
		}

		template <auto t_key>
		static_val member_ptr_to = get_member_ptr_to<t_key>();

		template <auto t_func>
		constexpr auto apply() const noexcept
		{
			return t_func(Datas::m_value...);
		}

		template <auto t_func>
		constexpr auto apply() noexcept
		{
			return t_func(Datas::m_value...);
		}

    template <template <typename> class SimpleHolder,
      template <typename> class FirstTConverter = std::remove_cvref
    >
    constexpr auto for_each(auto && func) const noexcept
    {
      return for_each(pspp_fwd(func), [](auto && ... vals)
        requires (sizeof...(vals) > 0)
      {
        using first_t = typename FirstTConverter<decltype(get_first_from_seq(vals...))>::type;
        pspp_execute_and_return_if_can(SimpleHolder<first_t>(pspp_fwd(vals)...));
        else return SimpleHolder<first_t>{pspp_fwd(vals)...};
      });
    }

    template <
        template <typename, auto> class ArrayLikeT,
        template <typename> class FirstTConverter = std::remove_cvref
    >
    constexpr auto for_each(auto && func) const noexcept
    {
      return for_each(pspp_fwd(func), [](auto && ... vals)
      {
        if constexpr (sizeof...(vals) == 0) return nulltv;
        else
        {
          using first_t = typename FirstTConverter<decltype(get_first_from_seq(vals...))>::type;
          pspp_execute_and_return_if_can(ArrayLikeT<first_t, sizeof...(vals)>{pspp_fwd(vals)...});
          else return ArrayLikeT<first_t, sizeof...(vals)>(pspp_fwd(vals)...);
        }
      }
      );
    }

    template <typename DataT = void, typename ResFuncT = nullt>
    constexpr auto for_each(auto && func, ResFuncT res = nulltv) noexcept
    {
      auto call = [&func]<std::size_t t_i>(auto & self, value_holder<t_i>)
      {
        pspp_execute_and_return_if_can(func(self.template get<t_i>()));
        else pspp_execute_and_return_if_can(func(keys | ::pspp::at<t_i>, self.template get<t_i>()));
        else pspp_execute_and_return_if_can(func.template operator()<keys | ::pspp::at<t_i>>(self.template get<t_i>()));
        else return keys | ::pspp::at<t_i>;
      };

      return [&call, &res]<std::size_t ... t_is>(auto & self, std::index_sequence<t_is...>)
      {
        if constexpr (std::same_as<DataT, void>)
        {
          if constexpr (c_nullt<decltype(res)>)
            (call(self, val<t_is>), ...);
          else
            return res(call(self, val<t_is>)...);
        } else
        {
          if constexpr (c_nullt<decltype(res)>)
            return DataT{
                call(self, val<t_is>)...
            };
          else
            return res(call(self, val<t_is>)...);
        }
      }(*this, std::make_index_sequence<size()>{});
    }

    template <typename DataT = void, typename ResFuncT = nullt>
    constexpr auto for_each(auto func, ResFuncT res = nulltv) const noexcept
    {
      auto call = [&func]<std::size_t t_i>(auto & self, value_holder<t_i>)
      {
        pspp_execute_and_return_if_can(func(self.template get<t_i>()));
        else pspp_execute_and_return_if_can(func(keys | ::pspp::at<t_i>, self.template get<t_i>()));
        else pspp_execute_and_return_if_can(func.template operator()<keys | ::pspp::at<t_i>>(self.template get<t_i>()));
        else pspp_execute_and_return_if_can(func.template operator()<keys | ::pspp::at<t_i>>());
        else return keys | ::pspp::at<t_i>;
      };

      return [&call, &res]<std::size_t ... t_is>(auto & self, std::index_sequence<t_is...>)
      {
        if constexpr (std::same_as<DataT, void>)
        {
          if constexpr (c_nullt<decltype(res)>)
            (call(self, val<t_is>), ...);
          else
            return res(call(self, val<t_is>)...);
        } else
        {
          if constexpr (c_nullt<decltype(res)>)
            return DataT{
              call(self, val<t_is>)...
            };
          else
            return res(call(self, val<t_is>)...);
        }
      }(*this, std::make_index_sequence<size()>{});
    }

    constexpr auto for_each_which(auto && func, auto && to_res, auto && filter) noexcept
    {
      return for_each(
          pspp_fwd(func),
          [&](auto && ... vals)
          {
            return clean_typeseq<decltype(vals)...>
                   | remove_like<pspp_fwd(filter)>
                   | pass_and_execute<pspp_fwd(to_res)>;
          }
      );
    }

    constexpr auto for_each_which(auto && func, auto && to_res, auto && filter) const noexcept
    {
      return for_each(
          pspp_fwd(func),
          [&](auto && ... vals)
          {
            return clean_typeseq<decltype(vals)...>
                   | remove_like<pspp_fwd(filter)>
                   | pass_and_execute<pspp_fwd(to_res)>;
          }
      );
    }

    template <typename ... BadTypes>
    constexpr auto for_each_except(auto && func, auto && to_res) noexcept
    {
      return for_each(
          pspp_fwd(func),
          [&to_res](auto && ... vals)
          {
            return clean_typeseq<decltype(vals)...>
                   | remove<BadTypes...>
                   | pass_and_execute<pspp_fwd(to_res)>;
          }
      );
    }
    template <typename ... BadTypes>
    constexpr auto for_each_except(auto && func, auto && to_res) const noexcept
    {
      return for_each(
          pspp_fwd(func),
          [&to_res](auto && ... vals)
          {
            return clean_typeseq<decltype(vals)...>
                   | remove<BadTypes...>
                   | convert_to_vals_and_execute<to_value, decltype(to_res){}>;
          }
      );
    }
	};

	template <auto ... t_keys, typename ... Ts>
	custom_struct(value_sequence<t_keys...> vs, type_sequence<Ts...> ts) ->
		custom_struct<cvc_t<t_keys, Ts>...>;

	template <auto ... t_keys, typename ... Ts>
	custom_struct(cvc_t<t_keys, Ts>...) ->
		custom_struct<cvc_t<t_keys, Ts>...>;

	template <auto ... t_keys, typename ... Ts>
	custom_struct(value_sequence<t_keys...>, type_sequence<Ts...>, auto ... rest) ->
		custom_struct<cvc_t<t_keys, Ts>...>;

	template <typename T>
	concept c_custom_struct = pspp::is_complex_target_t<custom_struct>::is_target<std::remove_cvref_t<T>>::value;

  template <typename ... ArgTs>
    [[nodiscard]]
  constexpr auto make_custom_struct(ArgTs && ... args) ->
    enumerated_custom_struct_fromv<clean_typeseq<ArgTs...>, 0>
  {
    return {
      std::forward<ArgTs>(args)...
    };
  }

template <typename ... Containers>
struct type_name_t<custom_struct<Containers...>> :
	constr_holder<
		to_constr_v<
			constr{"custom_struct { "}, 
			[]<typename Container>{
				return to_constr_v<
					type_name_constr<typename Container::value_type>, ' ',
					Container::key_holder::value, constr{"; "} 
				>;
			}.template operator()<Containers>()...,
			constr{"}"}
		>
	>
{};

template <auto t_key, typename ValueT>
struct typed_named_value_key_t
{
  using type = ValueT;
  static_val value = t_key;

  template <typename ValueU>
  constexpr auto operator=(ValueU && value) const noexcept
  { return custom_value_container<t_key, ValueT>(std::forward<ValueT>(value)); }
};

template <auto t_val>
struct named_value_key_t
{
  static_val value = t_val;

  template <typename ValueT>
  static_prop_of typed_named_value_key_t<t_val, ValueT> as {};

  template <typename ValueT>
  constexpr auto operator=(ValueT && value) const noexcept
  { return namedval<t_val>(std::forward<ValueT>(value)); }
};

inline namespace custom_key_literal
{
template <constr t_str>
constexpr named_value_key_t<t_str> operator""_k() noexcept
{ return {}; }

template <char ... t_cs>
constexpr auto operator""_k() noexcept
{ return named_value_key_t<operator""_c<t_cs...>()>{}; }
}

template <auto t_val>
constexpr auto operator--(value_holder<t_val>, int) noexcept
{ return named_value_key_t<t_val>{}; }

template <auto t_val, typename ValueT>
constexpr auto operator>(named_value_key_t<t_val>, ValueT && value) noexcept
{ return namedval<t_val>(std::forward<ValueT>(value)); }

} // namespace pspp

namespace std
{
	template <pspp::c_cvc_t ... Datas>
	struct tuple_size<pspp::custom_struct<Datas...>> :
		std::integral_constant<std::size_t, sizeof...(Datas)> {};

	template <std::size_t t_n, pspp::c_cvc_t ... Datas>
	struct tuple_element<t_n, pspp::custom_struct<Datas...>>
	{
		using type = decltype(pspp::custom_struct<Datas...>::types | pspp::atv<t_n>);
	};

	template <std::size_t t_n, pspp::c_custom_struct CustomStruct>
	constexpr auto get(CustomStruct && data) noexcept
	{
		using pure_type = std::remove_cvref_t<CustomStruct>;
		using to_get_type = typename pure_type::types::template at<t_n>;
		static constexpr auto key = pure_type::keys::template at<t_n>;

		return std::forward<pspp::copy_extensions_t<CustomStruct, to_get_type>>(
				std::forward<CustomStruct>(data).template get_at<key>());
	}
} // namespace std

#endif // PSPP_CUSTOM_STRUCT_HPP
