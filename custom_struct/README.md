# pspp::custom\_struct

Compile-time struct creator, which based on multi-inheritance with key value holder (pspp::value\_holder) used at getter/setter funtions overload.


## Usage


```cpp
#include <string>
#include <pspp/custom_struct.hpp>

int main()
{
    using custom_struct = pspp::custom_struct<
        pspp::custom_value_container<1 /* any type key */, int /* sotred value type */>,
        pspp::custom_value_container<2, double>,
        pspp::named_custom_value_container<"name", std::string> // alias for pspp::const_str implicit convertion
    >;

    using namespace std::string_literals;

    custom_struct obj{};
    obj.set<1, 2, pspp::const_str{"a"}>(2, 6.2, "hello world"s); // set separate values at once

    std::cout << "1: " << obj.get<1>() << '\n' <<
                "2: " << obj.get<2>() << '\n' <<
                "name: " << obj.get<"name">() << '\n';
}

```


For simplicity keys and types can be passed by pspp::value\_sequence (for keys) or pspp::cstr\_sequence (for pspp::const\_str only keys) or other or other struct derived from pspp::value\_sequence (pspp::meta::is\_value\_sequence need to be partially specialized with new type). 


```cpp
#include <string>
#include <pspp/custom_struct.hpp>

int main()
{
    using keys = pspp::value_sequence<1, 2, pspp::const_str{"name"}>;
    using types = pspp::type_sequence<int, double, std::string>;

    using custom_struct = pspp::custom_struct_from<keys, types>;

    // custom_struct usage is the same as in example above
    // ...
}

```


