#include "ustring.h"


namespace pspp
{
namespace ustr
{

ustring::ustring()
{
	error = 0;
	encoding = encoding_type_enum::UTF8;
}

ustring::ustring(uint* indx, int _n, std::string newString)
{
	copy(indx, _n, newString);
}
ustring::ustring(std::string _o_str)
{
	init(_o_str);
}
ustring::ustring(std::string_view str)
{
	std::string&& _str = std::string(str);
	init(_str);
}
ustring::ustring(const char str[])
{
	std::string&& _str = std::string(str);
	init(_str);
}

void ustring::copy(uint* indx, int __n, std::string newString){
	l.wlog("creating new ustring with indexes...\n");
	l.ilog("Indexes length is " + std::to_string(__n++) + "\n");
	l.ilog("New std::string is " + newString +  "\n");
	l.ilog(std::string(__FUNCTION__) + ": std::string = " + newString + "\n"); 

	this->length = __n -1;
	this->real_length = newString.length();
	this->indexes = new uint[__n];
	this->text = newString;

	uint tempFirstValue = *indx;

	l.ilog("creating new empty indexes...\n");

	for(int i = 0; i < __n; i++)
	{
		this->indexes[i] = *(indx + i) - tempFirstValue;

		l.ilog("indexes["+std::to_string(i) + "]:\t" + std::to_string(indexes[i]) + "\n");
	}

	l.oklog("finished creating.\n");
}
void ustring::init(std::string o_str){
	l.wlog("creating new ustring...\n");
	l.ilog("new std::string is: " + o_str + "\n");
	this->text = o_str;
	
	this->length = 0;

	this->real_length = o_str.length();

	for(int i = 0; i < o_str.length(); i++)
	{
			
		if ((o_str[i] & 0x80) == 0)
			i = i;		

		else if ((o_str[i] & 0xE0) == 0xC0)
			i++;

		else if ((o_str[i] & 0xF0) == 0xE0)
			i += 2;

		else if ((o_str[i] & 0xF8) == 0xF0)
			i += 3;
		
		length++;
	}	

	this->indexes = new uint[length + 1];

	for(int i = 0, k = 0; i < real_length && k < length; i++, k++)
	{

		indexes[k] = i;
		l.ilog("indexes[" + std::to_string(k) + "] = " + std::to_string(indexes[k]) +"\n");
	       	if ((o_str[i] & 0xE0) == 0xC0)
		{
			i++;	
		}
		else if ((o_str[i] & 0xF0) == 0xE0)
		{	
			i+=2;	
		}

		else if ((o_str[i] & 0xF8) == 0xF0)
		{
			i += 3;
		}
	}

	indexes[this->length] = this->real_length;
	l.ilog("indexes[" + std::to_string(this->length) + "] = " + std::to_string(indexes[this->length]) +"\n");

	l.oklog("ustring created.\n");

}
//---------------------------------------------
//----------------functions--------------------
//---------------------------------------------
std::string ustring::str()
{
	return this->text;
}


ustring* ustring::split(ustring separator)
{
	pspp::log::logger l1(logging, "U8_CHAR_IMPROVED_LIGHT " + std::string(__PRETTY_FUNCTION__));
	std::vector<int>pos;

	l1.ilog("starting split function...\n");
	l1.ilog("this->test = " + this->text + "(length: " + std::to_string(this->length) + ")\n");
	l1.ilog("separator is: " + separator.text + "\nseparator[0] = " + separator[0] + "\n");

	l1.ilog( "THIS->Length: " + std::to_string( this->length) + "\n");

	for(int i = 0; i < this->length; i++)
	{

		bool triger = false;
		if((*this)[i] == separator[0])
		{
			triger = true;
			for(int j = 1; j < separator.length && i + j < this->length; j++)
				if((*this)[i + j] != separator[j])
					triger = false;
		}
		if (triger)
		{
			l1.ilog("pushed (" + std::to_string(i) + ")\t" +(*this)[i] +"\n");
			pos.push_back(i);
		}
	}

	l1.ilog("pos.size() = " + std::to_string(pos.size()) + "\n");
	ustring* res = new ustring[(unsigned long)(pos.size() + 1)];

	if(pos.size() == 0)
	{
		l1.ilog("adding this->std::string value to res[0]...\n");
		res[0] = (*this);

		return res;
	}
	int newStringStartPos = 0;

	l1.ilog("TEMP:start creating new temp...\n");
	ustring temp1 = this->substr(newStringStartPos, pos[0] - newStringStartPos ); 
	l1.ilog("TEMP:created new temp.\ntemp: "+temp1.text + "\n");
	l1.ilog("TEMP:temp1.length = " + std::to_string(temp1.length) + "\n");
	l1.ilog("start giving it to res["+std::to_string(0) + "]...\n");
	l1.ilog("temp1's type: " + std::string(typeid(temp1).name()) +"\n");
	l1.ilog("res's type: " + std::string(typeid(res).name()) + "\n");
	l1.ilog("res[0]'s type: " + std::string(typeid(res[0]).name()) + "\n");
	res[0] = temp1;
	l1.oklog("done.\n");

	for(int i = 0; i < pos.size(); i++)
	{

		l1.ilog("start creating new temp...\n");
		ustring temp  = this->substr(newStringStartPos, pos[i] - newStringStartPos ); 
		l1.ilog("created new temp.\ntemp: "+temp.text + "\nstart giving it to res["+std::to_string(i) + "]...\n");


		res[i] = temp;		
		l1.oklog("done.\n");
		newStringStartPos = pos[i] + separator.length;
	}

	ustring temp = this->substr(newStringStartPos, this->length - newStringStartPos);
	l1.ilog("created new temp.\ntemp: "+temp.text + "\nstart giving it to res["+std::to_string(pos.size()) + "]...\n");
	res[pos.size()] = temp;
	l1.oklog("done.\n");
	return res;
}

void ustring::swap(ustring& __first, ustring& __seccond)
{
	ustring temp_str = __seccond;
	__seccond = __first;
	__first = temp_str;
}

ustring ustring::substr(int __pos, int __n )
{

	l.ilog("creating substr...\n__pos = " + std::to_string(__pos) + "\n__n = " + std::to_string(__n) + "\n");
	if(__pos + __n > length)
		return ustring("");

	l.ilog("res will be:\n");
	l.ilog("this->text.substr(" + std::to_string(this->indexes[__pos]) + ", " + std::to_string(this->indexes[__pos + __n] - this->indexes[__pos])+")\n");

	return ustring(&(this->indexes[__pos]),__n ,this->text.substr(this->indexes[__pos], this->indexes[__pos + __n] - this->indexes[__pos]));
}

int ustring::count(std::string element)
{
	int pos= 0 - element.length();
	int count = -1;

	do{
		pos = this->text.find(element, pos + element.length());
		count++;
	}while(pos != -1);

	return count;
}

int ustring::count(ustring element)
{
	return this->count(element.text);
}

ustring::ustring(ustring* newValue)
{

	l.ilog("copying ustring...\n");
	this->length = newValue->length;
	this->text = newValue->text;

//	delete[] this->indexes;

	this->indexes = new uint[this->length];
	for(int i = 0; i < this->length; i++)
	{
		this->indexes[i] = newValue->indexes[i];
	}
	l.oklog("copyin finished");
}

void ustring::renew(ustring newstring)
{
	this->copy(newstring.indexes, newstring.length, newstring.text);
}

void ustring::renew(std::string o_str)
{
	this->init(o_str);
}

void ustring::turn_log_off()
{
	this->logging = false;
}

void ustring::turn_log_on()
{
	this->logging = true;
}
//---------------------------------------------
//----------operators--------------------------
//---------------------------------------------
ustring ustring::operator+(ustring& add_value)
{
	ustring res(this->text + add_value.text);
	return res;
}
ustring ustring::operator+=(ustring& add_value)
{
	return (*this + add_value);
}
ustring ustring::operator=(ustring& newValue)
{
	l.wlog(std::string(__FUNCTION__) + "\tstarting copying with = operator\n");
	this->length = newValue.length;
	l.ilog("this->length = " + std::to_string(this->length) + "\n");
	this->text = newValue.text;
	l.ilog("this->text = " + this->text + "\n");
	this->real_length = newValue.real_length;
//	delete[] this->indexes;
	l.ilog("creating new indexes...\n");
	this->indexes = new uint[this->length + 1];
	for(int i = 0; i < this->length; i++)
	{
		this->indexes[i] = newValue.indexes[i];
		l.ilog("\tthis->indexes["+std::to_string(i)+"] = " + std::to_string(this->indexes[i]) + "\n");
	}
	this->indexes[this->length] = this->real_length;
	l.ilog("\tthis->indexes[" + std::to_string(this->length) + "] = " + std::to_string(indexes[this->length]) +"\n");
	l.ilog("returning value...\n");
	return *this;
}
void* ustring::operator new[](std::size_t sz)
{
	return ::operator new(sz);
}

std::string ustring::operator[](std::size_t ind)
{
	if(ind >= this->length)
	{
		l.elog(std::string(__PRETTY_FUNCTION__) + ": sigment-overflow\n");
		return nullptr;
	}
	return this->text.substr(indexes[ind], indexes[ind+1] - indexes[ind]);
}

const std::string ustring::operator[](std::size_t ind) const
{
	if(ind >= this->length)
	{
		return nullptr;
	}
	return this->text.substr(indexes[ind], indexes[ind+1] - indexes[ind]);
}

int ustring::index(ustring* str)
{
	if ( str == nullptr)
		return -1;

	auto _ = pspp::dfr::defer{[&](){delete str;}};

	for (int i = 0; i < length - str->length + 1; i++)
	{
		if ( (*this)[i] == (*str)[0])
		{
			bool t = true;
			for (int j = 1; j < str->length && j < length; j++)
			{
				if ((*this)[i+j] != (*str)[j])
				{
					t = false;
					break;
				}
			}

			if (t)
			{
				l.ilogn("index:res:i = "+ std::to_string(i));
				return i;
			}
		}
	}
	return -1;
}

int ustring::index(std::string str)
{
	return this->index(new ustring(str));
}

} // namepsace ustr
} // namespace pspp
