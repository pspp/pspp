#ifndef PISOOSPP_USTRING_H
#define PISOOSPP_USTRING_H

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string_view>

#include "plog.h"
#include "../../15_ppdeferc/defer.hpp"

namespace pspp
{
namespace ustr
{

class ustring
{
	bool logging = false;
	
	int my_output();
	int current_index;
	uint* indexes;
	std::string text;

	void init(std::string);
	void copy(uint*, int, std::string);
public:

	int encoding;
	int length;
	int real_length;
	int error;

	enum  encoding_type_enum 
	{
		UTF8 = 0, JIS, SHIFT_JIS
	};
	
	pspp::log::logger l{true,"U8_CHAR_IM"};


	ustring();
	ustring(ustring*);
	ustring(const char[]);
	ustring(std::string_view);
	ustring(std::string);
	ustring(std::string, std::string*);
	ustring(uint*,int, std::string);

	int count(ustring);
	int count(std::string);
	
	std::string str();
	ustring* split(ustring);
	ustring* usplit(ustring);	
	ustring substr(int __pos, int __n);
	void swap(ustring&,ustring&);	
	void renew(std::string);
	void renew(ustring);

	void turn_log_off();
	void turn_log_on();

	int index(ustring*);
	int index(std::string);

	ustring operator+(ustring&);
	ustring operator=(ustring&);
	ustring operator=(std::string&);
	ustring operator+=(ustring&);
	std::string operator[](std::size_t);
	const std::string operator[](std::size_t) const;
	static void* operator new[](std::size_t);


	void output()
	{
		std::cout << text;
	}
	void output(std::string plus_text)
	{
		std::cout << text << plus_text;
	}
	void output(std::string front_string, std::string back_string)
	{
		std::cout << front_string << text << back_string;
	}

	bool operator==(ustring& a)
	{
		return (this->text == a.text);
	}
	friend std::ostream& operator<<(std::ostream& os, const ustring& kn)
	{
		os << kn.text;
		return os;
	}
	friend std::ostream& operator<<(std::ostream& os, ustring& kn)
	{
		os << kn.text;
		return os;
	}
};


} // namespace ustr
} // namespace pspp
#endif // PISOOSPP_USTRING_H
