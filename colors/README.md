# pspp::clr (Colors)

Simplest library for unix terminal colored output. 
Macro version just puts string literal in place. Non-macro version creates `std::string_view` at compile time. 

`pspp::color<val>()` currently do not works.

## Example

```cpp
#include <pspp/macro_colors.hpp>
#include <iostream>

int main()
{
  std::cout << pspp_red "hello world" pspp_clcol "\n"
               pspp_blue "hello world" pspp_clcol "\n"
               pspp_yellow "hello world" pspp_clcol "\n"
               pspp_col(123) "hello world" pspp_clcol "\n";

}
```


