#ifndef PSPP_COLORS_HPP
#define PSPP_COLORS_HPP

#include <string>
#include <string_view>
#include <charconv>
#include <array>

#include <pspp/useful/static.hpp>
#include <pspp/constr.hpp>
#include <pspp/adaptors.hpp>

namespace pspp
{
inline namespace clr
{
	inline constexpr std::string_view clcol = "\033[0;0m";
	inline constexpr std::string_view red = "\033[0;31m";
	inline constexpr std::string_view green = "\033[0;32m";
	inline constexpr std::string_view yellow = "\033[0;33m";
	inline constexpr std::string_view blue = "\033[0;34m";
	inline constexpr std::string_view purple = "\033[035m";
	inline constexpr std::string_view cyan = "\033[0;36m";

  enum struct term_font_type : std::uint8_t
  { regular, bold, italic, under, blink, z_count_ };

  struct term_rgb
  {
    std::uint8_t const r;
    std::uint8_t const g;
    std::uint8_t const b;
  };

  inline constexpr void to_color_str(std::uint8_t const n, char * const __restrict dest) noexcept
  {
    static constexpr std::uint8_t  const first_degree[3] {0, 100, 200};

    std::uint8_t const first = n / 100;
    std::uint8_t const tmp = n - first_degree[first];
    std::uint8_t const second = tmp / 10;
    std::uint8_t const third = tmp % 10;

    dest[0] = '0' + first  | as<char>;
    dest[1] = '0' + second | as<char>;
    dest[2] = '0' + third  | as<char>;
  }

  inline constexpr std::size_t term_rgb_cstr_size  = std::string_view{"\033[38;2;000;000;000m"}.size();
  inline constexpr std::size_t term_font_cstr_size = std::string_view{"\033[0m"}.size();

  using term_rgb_str      = constr<term_rgb_cstr_size >;
  using term_font_str     = constr<term_font_cstr_size>;
  using term_font_rgb_str = constr<term_rgb_cstr_size + term_font_cstr_size>;

  inline constexpr auto color(term_rgb color_data) noexcept -> term_rgb_str
  {
    static constexpr std::size_t r_offset = std::string_view{"\033[38;2;"}.size();
    static constexpr std::size_t g_offset = r_offset + 4;
    static constexpr std::size_t b_offset = g_offset + 4;

    term_rgb_str res {"\033[38;2;000;000;000m"};
    to_color_str(color_data.r, std::next(res.data(), r_offset));
    to_color_str(color_data.g, std::next(res.data(), g_offset));
    to_color_str(color_data.b, std::next(res.data(), b_offset));

    res.escape();

    return res;
  }

  inline constexpr auto color(term_font_type font_type) noexcept -> term_font_str
  {
    static constexpr array pre = []<std::size_t ... t_is>(std::index_sequence<t_is...>){
      return array{
        (empty_cstr + "\033[" + ('0' + t_is/as<char>) + 'm')...
      };
    }(std::make_index_sequence<term_font_type::z_count_ | as<std::size_t>>{});

    return pre[font_type];
  }

  inline constexpr auto color(term_font_type font_type, term_rgb color_data) noexcept
    -> term_font_rgb_str
  {
    return color(font_type) + color(color_data);
  }


	template <char number>
	inline constexpr std::string_view color()
	{
		return to_string_view<12, number>(
				[]<char n>()
				{
					char res[12] = "\033[38;5;000m";
					char tmp = n % 256, i = 0;
					while(tmp > 0)
					{
						res[9 - i++] = '0' + (tmp % 10);
						tmp /= 10;
					}
					return std::string{res, 12};
				}
			);
	}

} // clr
} // pspp

#endif // PSPP_COLORS_HPP
