#ifndef PSPP_MACRO_COLORS_HPP
#define PSPP_MACRO_COLORS_HPP

#define pspp_clcol  "\033[0;0m"
#define pspp_nocol  "\033[0;0m"
#define pspp_red    "\033[0;31m"
#define pspp_green  "\033[0;32m"
#define pspp_yellow "\033[0;33m"
#define pspp_blue   "\033[0;34m"
#define pspp_purple "\033[035m"
#define pspp_cyan   "\033[0;36m"
#define pspp_bold   "\033[1m"
#define pspp_col(color) "\033[38;5;" #color "m"


#endif // PSPP_MACRO_COLORS_HPP

