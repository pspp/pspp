
#ifndef PSPP_META_HPP
#define PSPP_META_HPP

#include <pspp/value_sequence.hpp>
#include <pspp/useful/concepts.hpp>

namespace pspp
{
inline namespace meta
{
	template <typename T>
		concept c_has_ts = requires {typename T::ts; };

	namespace meta_detail
	{
		template <typename T, typename U>
		struct append_two_
		{ using type = type_sequence<T, U>; };
		template <typename ... Ts, typename U>
		struct append_two_<type_sequence<Ts...>, U>
		{ using type = type_sequence<Ts..., U>; };
		template <typename ... Ts, typename ... Us>
		struct append_two_<type_sequence<Ts...>, type_sequence<Us...>>
		{ using type = type_sequence<Ts..., Us...>; };

		template <typename T, typename U>
		using append_two_t_ = typename append_two_<T, U>::type;

		template <typename T>
		struct to_combine_holder_t
		{ using type = T; };

		template <typename ... Ts, typename ... Us>
		constexpr auto combine_two(type_sequence<Ts...>, type_sequence<Us...>)
		{ return to_combine_holder_t<type_sequence<meta_detail::append_two_t_<Ts, Us>...>>{}; }

		template <typename ... Us>
		constexpr auto combine_two(type_sequence<>, type_sequence<Us...>)
		{ return to_combine_holder_t<type_sequence<type_sequence<Us>...>>{}; }

		template <c_type_sequence T, c_type_sequence U>
		constexpr auto operator+(to_combine_holder_t<T>, to_combine_holder_t<U>)
		{ return combine_two(T{}, U{}); }

		template <typename T>
		struct to_ts
		{ using type = type_t<typename T::ts>; };

		template <typename ... Ts>
		struct to_ts<type_sequence<Ts...>>
		{ using type = type_t<type_sequence<Ts...>>; };
	} // namespace meta_detail
	
	template <typename T>
	struct forbid_plus_operator_for<meta_detail::to_combine_holder_t<T>> : std::true_type {};

	struct combine_fnt
	{
		constexpr static auto operator()(c_sequence auto ... ss) noexcept
		{
			static constexpr auto info = []<std::size_t ... t_sizes>(std::index_sequence<t_sizes...>){
				std::size_t i {0};
				std::array sizes {std::pair{i++, t_sizes}...};

				return std::pair{sizes, 
					std::ranges::min(sizes, {}, &std::pair<std::size_t, std::size_t>::second/*[](auto const & f, auto const & s) {return f.second > s.second; }*/).second};
			}(std::index_sequence<ss.size...>{});

			if constexpr (info.second == 0)
				return typeseq<>;
			else 
			{
				constexpr auto ts = typeseq<decltype(ss)...> | filter<[]<typename T>
																												 { return T::size >= info.second; }> 
																										 | ttransform<meta_detail::to_ts>
																										 | pass_and_execute<
																												 []<typename ... Ts>() 
																												 { return (meta_detail::to_combine_holder_t<type_sequence<>>{} + ... + meta_detail::to_combine_holder_t<Ts>{});}
																										  >;
				return typename decltype(ts)::type{};
			}
		}
	};

	inline constexpr combine_fnt combine {};

	template <auto t_func>
	struct function_caller
	{
		template <typename ... Args>
		pspp_always_inline constexpr auto operator()(Args && ... args) const noexcept
		{
			return t_func(std::forward<Args>(args)...);
		}
	};

	template <concepts::c_member_function_pointer auto t_func>
	struct function_caller<t_func>
	{
		template <typename OwnerT, typename ... Args>
		pspp_always_inline constexpr auto operator()(OwnerT && owner, Args && ... args) const noexcept
		{
			return (owner->*t_func)(std::forward<Args>(args)...);
		}
	};

	template <auto t_func>
	inline constexpr function_caller<t_func> call_func{};

	template <typename ... Ts>
	constexpr auto seq() noexcept
	{ return type_sequence<Ts...>{}; }

	template <auto ... t_vals>
	constexpr auto seq() noexcept
	{ return value_sequence<t_vals...>{}; }

} // inline namespace meta
} // namespace pspp

#endif // PSPP_META_HPP
