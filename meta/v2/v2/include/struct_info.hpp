#ifndef PSPP_STRUCT_INFO_HPP
#define PSPP_STRUCT_INFO_HPP

#include <pspp/custom_struct.hpp>

namespace pspp::inline meta
{

namespace meta_detail
{
template <typename T>
struct type_extractor
{ using type = T; };

template <typename T>
	requires ( requires {typename T::type; })
struct type_extractor<T>
{
	using type = typename T::type;
};
} // namespace meta_detail

template <typename T>
struct struct_info{};

template <typename T>
using class_info = struct_info<T>;
template <typename TargetT, typename ... TargetPartTs>
struct struct_info_spec
{
	static constexpr auto raw_types = typeseq<TargetPartTs...>;
	static constexpr auto types = raw_types | ttransform<meta_detail::type_extractor>;
	using access_type = enumerated_custom_struct_fromv<types>;

	/*static_assert(sizeof(TargetT) == ( sizeof(TargetPartTs) + ...),
		"struct info specialization: sizes is different");
		*/
};

template <typename TargetT>
struct struct_info_spec<TargetT>
{
	static_val raw_types = emptytseq;
	static_val types = emptytseq;
	using access_type = struct custom_struct<>;

	static_assert(sizeof(TargetT) == 1,
		"struct info specialization: struct has not 1b size");
};

template <typename T>
concept c_with_known_struct_info = requires {
	struct_info<std::remove_cvref_t<T>>::raw_types;
	struct_info<std::remove_cvref_t<T>>::types;
	typename struct_info<std::remove_cvref_t<T>>::access_type;
};

template <typename T, auto t_name>
struct with_name :
	cstr_holder<"<?>">
{
	using type = T;
};

template <typename T, constr t_name>
struct with_name<T, t_name> :
    cstr_holder<t_name>
{
  using type = T;
};

template <constr t_name, typename T>
using with_name_ = with_name<T, t_name>;




template <typename T, constr t_prefix, constr t_suffix = "">
struct surrounded
{
	using type = T;
	using preffix_holder = cstr_holder<t_prefix>;
	using suffix_holder = cstr_holder<t_suffix>;
};

template <typename T>
struct hidden : std::type_identity<T>
{};


template <typename ... Containers>
struct struct_info<custom_struct<Containers...>>:
	struct_info_spec<custom_struct<Containers...>,
		std::conditional_t<
			c_constr<typename Containers::key_holder::type>,
			with_name<typename Containers::value_type, Containers::key_holder::value>,

      std::conditional_t<
          std::integral<typename Containers::key_holder::type>,
          with_name<typename Containers::value_type, to_constr<Containers::key_holder::value>()>,
			    typename Containers::value_type
      >
		>...
	>
{};

/*
template <typename ... Containers>
struct struct_info<custom_struct<Containers...>>:
	struct_info_spec<custom_struct<Containers...>,
  Containers...
	>
{};*/

template <auto t_key, typename DataT>
struct struct_info<custom_value_container<t_key, DataT>> :
	struct_info_spec<custom_value_container<t_key, DataT>,
		std::conditional_t<
			is_constr(t_key),
			with_name<DataT, t_key>,
      std::conditional_t<
        std::integral<std::remove_cvref_t<decltype(t_key)>>,
        with_name<DataT, to_constr<t_key>()>,
			  DataT
      >
		>
	>
{};

} // namespace pspp::inline meta

#endif // PSPP_STRUCT_INFO_HPP

