#ifndef PSPP_STRUCT_INFO_HPP
#define PSPP_STRUCT_INFO_HPP

#include <pspp/custom_struct.hpp>

namespace pspp::detail
{
inline constexpr auto type_extractor = []<typename T>
{
	if constexpr (requires {typename T::type;})
		return change_to<typename T::type>;
};
} // namespace pspp::detail

namespace  pspp::inline meta
{

template <typename T>
struct struct_info{};

template <typename T>
using class_info = struct_info<T>;

template <typename TargetT, typename ... TargetPartTs>
struct struct_info_spec
{
	using raw_types = type_sequence<TargetPartTs...>;
	using types = typename raw_types::template transform<detail::type_extractor>;
	using access_type = enumerated_custom_struct_from<types>;

	/*static_assert(sizeof(TargetT) == ( sizeof(TargetPartTs) + ...),
		"struct info specialization: sizes is different");
		*/
};

template <typename TargetT>
struct struct_info_spec<TargetT>
{
	using raw_types = empty_type_sequence;
	using types = empty_type_sequence;
	using access_type = struct custom_struct<>;

	static_assert(sizeof(TargetT) == 1,
		"struct info specialization: struct has not 1b size");
};

template <typename T>
concept c_with_known_struct_info = requires {
	typename struct_info<std::remove_cvref_t<T>>::raw_types;
	typename struct_info<std::remove_cvref_t<T>>::types;
	typename struct_info<std::remove_cvref_t<T>>::access_type;
};

} // namespace pspp::inline meta

#endif // PSPP_STRUCT_INFO_HPP

