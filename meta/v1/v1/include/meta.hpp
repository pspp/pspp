
#ifndef PSPP_META_HPP
#define PSPP_META_HPP

#include <pspp/value_sequence.hpp>
#include <pspp/useful/concepts.hpp>

namespace pspp
{
inline namespace meta
{
	namespace helpers{
		struct combinator 
		{

			using get_fn = decltype([]<std::size_t t_n, typename FirstT, typename ... RestT>
			{
				return type_t<type_sequence<typename FirstT::template at<t_n>,
								typename RestT::template at<t_n>...>>{};
			});

			template <size_t n, typename FirstT, typename ... RestT>
			struct types_getter
			{
				using type_seq = extract_type<get_fn{}.template operator()<n, FirstT, RestT...>()>;
			};

			template <typename First, typename ... Rest>
			struct wrapper 
			{
				template <std::size_t t_i>
				using res = typename types_getter<t_i, First, Rest...>::type_seq;
			};
		};

		using default_types_transformator = overloaded_lambda<
				[]<template <typename ... Ts> class Target, c_type_sequence Ts>() {
					return type_t<typename Ts::template passed_type<Target>>{};
				}
			>;
	};

	template <typename MaybeSequence>
	concept c_sequence = c_value_sequence<MaybeSequence> or c_type_sequence<MaybeSequence>;

	template <c_sequence First, c_sequence Second, c_sequence ... Rest>
	inline consteval auto combine_impl()
	{
		static constexpr auto empty_checker = []<typename T>(){ return (T::count != 0); };

		using simply_combined = type_sequence<First, Second, Rest...>;
		using without_empty = typename simply_combined::template succeed<empty_checker>;

		if constexpr (without_empty::count == 0)
			return type_t<empty_type_sequence>{};
		else
		{
			using min_len = value_holder<without_empty::template converted_values_passed_type<
				[]<typename T>{ return T::count; }, value_sequence>::template most<small>>;

			static constexpr auto cutter = []<typename T> { 
					if constexpr (c_value_sequence<T>)
						return change_to<typename T::values::template sub_sequence<0, min_len::value>>;
					else
						return change_to<typename T::template sub_sequence<0, min_len::value>>; };


			using cutted_types = typename without_empty::template transform<cutter>;
		
			return []<std::size_t ... Is>(std::index_sequence<Is...>)
				{
					using wrapper = typename cutted_types::template passed_type<helpers::combinator::wrapper>;
					return type<type_sequence<typename wrapper::template res<Is>...>>;
				}(std::make_index_sequence<min_len::value>());
		}
	}

	template <c_sequence Seq1, c_sequence Seq2, c_sequence ... Seqs>
	using combine = extract_type<combine_impl<Seq1, Seq2, Seqs...>()>;


	template <auto t_func>
	struct function_caller
	{
		template <typename ... Args>
		pspp_always_inline constexpr auto operator()(Args && ... args) const noexcept
		{
			return t_func(std::forward<Args>(args)...);
		}
	};

	template <concepts::c_member_function_pointer auto t_func>
	struct function_caller<t_func>
	{
		template <typename OwnerT, typename ... Args>
		pspp_always_inline constexpr auto operator()(OwnerT && owner, Args && ... args) const noexcept
		{
			return (owner->*t_func)(std::forward<Args>(args)...);
		}
	};

	template <auto t_func>
	inline constexpr function_caller<t_func> call_func{};

	template <typename ... Ts>
	consteval auto sequence() noexcept
	{
		return to_type_sequence<Ts...>{};
	}

	template <auto ... t_vals>
	consteval auto sequence() noexcept
	{
		return value_sequence<t_vals...>{};
	}
} // inline namespace meta
} // namespace pspp

#endif // PSPP_META_HPP
