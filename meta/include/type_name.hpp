#ifndef PSPP_TYPE_NAME_HPP
#define PSPP_TYPE_NAME_HPP

#include <pspp/constr.hpp>
#include <pspp/utility/utility.hpp>
#include <cxxabi.h>

namespace std
{

template <typename T, typename Allocator>
class vector;

};

namespace pspp::inline meta
{


template <typename T>
struct type_name_t : constr_holder<"">
{};

template <typename T>
inline constexpr auto type_name_constr = 
	[]{ if constexpr (type_name_t<T>::empty()) return to_constr<'?', sizeof(T)>();
			else return type_name_t<T>::value;
		}();

template <typename T>
inline constexpr std::string_view type_name = type_name_constr<T>.strv();


template <typename T>
struct type_name_t<T const> : constr_holder<type_name_constr<T> + constr{" const"}>
{};

template <typename T>
struct type_name_t<T &> : constr_holder<type_name_constr<T> + constr{" &"}> {};

template <typename T>
struct type_name_t<T &&> : constr_holder<type_name_constr<T> + constr{" &&"}> {};

template <typename T>
struct type_name_t<T volatile> : constr_holder<type_name_constr<T> + constr{" volatile"}> {};

template <typename T>
struct type_name_t<T volatile const> : constr_holder<type_name_constr<T> + constr{" volatile const"}> {};

template <typename T>
struct type_name_t<T *> : constr_holder<type_name_constr<T> + constr{" *"}> {};

template <typename T, std::size_t t_n>
struct type_name_t<T[t_n]> : constr_holder<type_name_constr<T> + to_constr_v<'[', t_n, ']'>> {};

template <typename T, std::size_t t_n>
struct type_name_t<T const [t_n]> : constr_holder<type_name_constr<T> + constr{" const"} + to_constr_v<'[', t_n, ']'>> {};

template <typename T, std::size_t t_n>
struct type_name_t<T volatile [t_n]> : constr_holder<type_name_constr<T> + constr{" volatile"} + to_constr_v<'[', t_n, ']'>> {};

template <typename T, std::size_t t_n>
struct type_name_t<T volatile const [t_n]> : constr_holder<type_name_constr<T> + constr{" volatile const"} + to_constr_v<'[', t_n, ']'>> {};

template <auto t_val>
inline constexpr auto type_value_name_constr = to_constr_v<'@', sizeof(t_val)>;

template <auto t_val>
inline constexpr std::string_view type_value_name = type_value_name_constr<t_val>.strv();


template <> struct type_name_t<void> : constr_holder<"void"> {};
template <> struct type_name_t<bool> : constr_holder<"bool"> {};
template <> struct type_name_t<char> : constr_holder<"char"> {};

template <> struct type_name_t<float> : constr_holder<"float"> {};
template <> struct type_name_t<double> : constr_holder<"double"> {};

template <> struct type_name_t<std::int8_t > : constr_holder<"char"> {};
template <> struct type_name_t<std::int16_t> : constr_holder<"short"> {};
template <> struct type_name_t<std::int32_t> : constr_holder<"int"> {};
template <> struct type_name_t<std::int64_t> : constr_holder<"int64"> {};

template <> struct type_name_t<std::uint8_t > : constr_holder<"byte"> {};
template <> struct type_name_t<std::uint16_t> : constr_holder<"uint16"> {};
template <> struct type_name_t<std::uint32_t> : constr_holder<"uint32"> {};
template <> struct type_name_t<std::uint64_t> : constr_holder<"uint64"> {};

template <c_int128 T>  struct type_name_t<T> : constr_holder<"int128"> {};
template <c_uint128 T> struct type_name_t<T> : constr_holder<"uint128"> {};

template <typename T>
struct type_name_t<std::allocator<T>> : constr_holder<constr{"std:allocator<"} + type_name_constr<T> + constr{">"}> {};

template <>
struct type_name_t<std::string> : constr_holder<"std:string"> {};

template <typename T>
struct type_name_t<std::basic_string<T>> : constr_holder<constr{"std:string<"} + type_name_constr<T> + constr{">"}> {};

template <>
struct type_name_t<std::string_view> : constr_holder<"std:string_view"> {};

template <typename T>
struct type_name_t<std::basic_string_view<T>> : constr_holder<constr{"std:basic_string_view"} + type_name_constr<T> + constr{">"}> {};

template <typename T, typename AllocatorT>
  requires (not c_same_as<AllocatorT, std::allocator<T>>)
struct type_name_t<std::vector<T, AllocatorT>> :
  constr_holder<empty_cstr + "std:vector<" + type_name_constr<T> + ", " + type_name_constr<AllocatorT> + ">"> {};

template <typename T>
struct type_name_t<std::vector<T, std::allocator<T>>> :
  constr_holder<empty_cstr + "std:vector<" + type_name_constr<T> + ">"> {};

template <> struct type_name_t<std::ostream> : constr_holder<"std:ostream"> {};
template <> struct type_name_t<std::istream> : constr_holder<"std:istream"> {};
template <> struct type_name_t<std::iostream> : constr_holder<"std:iostream"> {};

template <typename T, std::size_t t_n>
struct type_name_t<std::array<T, t_n>> : constr_holder<to_constr_v<constr{"std:array<"}, type_name_constr<T>, ',', ' ', t_n, '>'>> {};

template <typename T>
struct type_name_t<type_name_t<T>> : 
	constr_holder<to_constr_v<constr{"pspp:type_name_t<pspp:type_name_t<"}, type_name_constr<T>, constr{">>"}>> {};

template <typename T>
constexpr auto get_type_name() noexcept
{ 
	if consteval 
	{
		return type_name<T>;
	}
	else
	{
		if constexpr (type_name<T>.front() == '?')
		{
			int status = 0;
			return std::string_view{abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &status)};
		}
		else return type_name<T>;
	}
}

} // namespace pspp::inline meta

#endif // PSPP_TYPE_NAME_HPP
