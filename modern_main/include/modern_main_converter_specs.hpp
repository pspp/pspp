#ifndef PSPP_MODERN_MAIN_CONVERTER_SPECS_HPP
#define PSPP_MODERN_MAIN_CONVERTER_SPECS_HPP

#include <filesystem>

#include <pspp/modern_main.hpp>
#include <pspp/utility/fs.hpp>
#include <pspp/io.hpp>

namespace pspp
{

template <typename StringT, typename HandlerT, file_check_types t_type>
struct conversion_specs<
  basic_checked_file_path<StringT, HandlerT, t_type>
>
{
  template <c_conversion_context ContextT, c_conversion_info InfoT>
  constexpr static auto convert(conversion_input input, InfoT && info, ContextT)
    noexcept (is_conv_noexcept<InfoT>)
  -> conversion_result<basic_checked_file_path<StringT, HandlerT, t_type>>
  {
    return {
      basic_checked_file_path<StringT, HandlerT, t_type>{
        StringT{input.front()}
      }
    };
  }
};



} // namespace pspp

#endif // PSPP_MODERN_MAIN_CONVERTER_SPECS_HPP
