# pspp::modern\_main

Library to write simple main function which takes input from user.

`c++23` required.

## Simple Example 

```cpp
#include <pspp/modern_main.hpp>
#include <iostream>

void true_main(int a, int b) noexcept {
    std::cout << a << " + " << b << " = " << a + b << '\n'; 
}

PSPP_TRUE_MAIN_ENTRY()
```

expected output:

```bash
$ ./a.out 2 5
2 + 5 = 7
```

## Usage

```cpp
pspp::modern_main<function_name, max_arguments_count = 255>
```

### function_name

Excepts any not-overloaded function. Deducing expected elements from function arguments.

Can take `pspp::modern_args_type` as first argument to get all passed arguments.

* `std::vector` can be used to get variadic arguments. For example:

```cpp
#include <numeric>

#include <pspp/modern_main.hpp>
#include <pspp/useful/ostream_overloads.hpp>

void true_main(std::vector<int> args) noexcept
{ 
    std::cout << "sum of: " << args << '\n' << 
                 std::accumulate(args.begin(), args.end(), 0) << '\n'; 
}

PSPP_TRUE_MAIN_ENTRY()
```

expected output: 

```bash
$ ./a.out 1 2 3 4
sum of: [1, 2, 3, 4]
10
```


* `std::array` can be used to get several elements at once

* custom range type can be used by partial specialization of `pspp::range_argument_info<T>`.

  * `using value_type = DataT` required. Tells which type must be constructed.
  * `static auto construct()` returns range which is not default constructable.
  * `static auto prepare(auto & range, std::size_t size)` can be used to prepare struct to add values (`std::vector::reserve(size)`).
  * `static auto add_value(auto & range, auto && value, std::size_t pos)` tells how to add new values. `pos` argument is optional. Required when size is variadic.
  * `static constexpr std::size_t size` can be provided if count of arguments is constant. 
  * `static auto construct(auto && ... vals)` if count of arguments is constant, can be provided to construct range inline. Returns constructed range.

* `std::integral` and `std::floating_point` will be automatically converted using `std::from_chars` 
* custom types can be used if partial specialization of `pspp::modern_main_type_convertion_info<T>`. One of next two must be provided.
  * `template <bool use_exceptions> static auto convert(std::string_view data) noexcept(not use_exception)` can be provided to convert std::string_view to underlying type.
  * `static constexpr type_sequence<U> types {}` can be provided when T can be constructed with type U (conversion from std::string_view to U must be known).

### max_arguments_count

`pspp::modern_main` uses `std::array<std::string_view, max_arguments_count>` to store passed arguments.

### Exceptions

If passed function is not noexcept, exceptions used.

`A.cpp`: 

```cpp
#include <iostream>
#include <pspp/modern_main.hpp>

void true_main(int a) noexcept { std::cout << a << '\n'; }

PSPP_TRUE_MAIN_ENTRY()
```

`B.cpp`

```cpp
#include <pspp/modern_main.hpp>

void true_main(int a) { std::cout << a << '\n'; }

PSPP_TRUE_MAIN_ENTRY()
```

Expected result:
```bash
$ ./A 
0
$ ./B
terminate called after throwing an instance of 'std::runtime_error'
  what():  modern_main: error while converting to integral type; base string: ``
```

## Advanced usage

(to be added)





