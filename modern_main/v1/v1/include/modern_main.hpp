#ifndef PSPP_MODERN_MAIN_HPP
#define PSPP_MODERN_MAIN_HPP

#include <functional>
#include <variant>
#include <string_view>
#include <cassert>

#include <pspp/type_helpers.hpp>
#include <pspp/meta.hpp>
#include <pspp/constr.hpp>
#include <pspp/useful/overloaded_lambda.hpp>

namespace pspp
{

using main_args_type = std::vector<std::string_view> const &;

namespace detail
{
	template <typename ResT, typename ... Args>
	inline consteval std::size_t get_count_of_args(ResT (Args...)) noexcept
	{
		using _args = pspp::to_type_sequence<Args...>;
		using args = extract_type<
			[]{
				if constexpr (std::same_as<typename _args::head, main_args_type>)
					return type<typename _args::template sub_sequence<1>>;
				else return type<_args>;
			}()>;

		return args::count + 1;
	}
} // detail

namespace modern_args 
{

inline constexpr bool check_safely = true;
inline constexpr bool check_unsafely = false;

template <std::size_t t_n, bool t_check_safely = check_safely>
struct absolute_position : value_holder<t_n> 
{
	static_assert(t_n >= 0, "modern_args: absolute_position: t_n must be greated than 0");
	using is_safe = value_holder<t_check_safely>;

	inline static consteval std::size_t get(int, std::size_t) noexcept
	{
		return t_n;
	}

	inline static std::size_t get(int, std::size_t size)
		requires (t_check_safely)
	{
		if (t_n >= size)
			throw "modern_args: absolute_position: trying to access out of bounds value";

		return t_n;
	}
};

template <int t_n, bool t_check_safely = check_unsafely>
struct relative_position : value_holder<t_n> 
{
	using is_safe = value_holder<t_check_safely>;
	inline static std::size_t get(int current_pos, [[maybe_unused]] std::size_t size) noexcept
	{
		if constexpr (t_check_safely)
		{
			auto res = current_pos + t_n;
			if (res >= size)
				throw "modern_args: relative_position: trying to access out of bounds value (bigger than size)";
			else if (res < 0)
				throw "modern_args: relative_position: trying to access out of bounds value (less than 0)";
			return res;
		}
		else
			return current_pos + t_n;
	}
};

template <std::size_t t_accessing_pos>
struct e_bad_args_access : public std::exception
{
	e_bad_args_access(std::size_t args_count):
		m_message(
			cstr_holder<to_constr<constr{"at pos `"}, t_accessing_pos, constr{"`: count of args recieved: "}>()>::str() + 
			std::to_string(args_count)
			)
	{};

	char const * what() const noexcept override
	{
		return m_message.c_str();
	}

	private:
	std::string m_message;
};

template <typename T>
struct is_special_position : std::false_type {};

template <int t_n, bool t_check_safely>
struct is_special_position<relative_position<t_n, t_check_safely>> : std::true_type 
{
	inline static constexpr bool is_absolute = false;
};

template <std::size_t t_n, bool t_check_safely>
struct is_special_position<absolute_position<t_n, t_check_safely>> : std::true_type {
	inline static constexpr bool is_absolute = true;
};

template <typename T>
concept c_special_position = is_special_position<T>::value;


struct current_position 
{
	inline static int get(int current_pos, [[maybe_unused]] std::size_t) noexcept
	{
		return current_pos;
	}
};

template <c_special_position Pos, std::size_t t_current>
inline consteval auto get_pos() noexcept
{
	if constexpr (is_special_position<Pos>::is_absolute)
		return Pos::value;
	else
	{
		static_assert(t_current + Pos::value >= 0, "better_main: args: get_pos: trying to get out of bounds argument");
		return t_current + Pos::value;
	}
}

template <std::same_as<current_position> Pos, std::size_t t_current>
inline consteval auto get_pos() noexcept
{
	return t_current;
}

template <class DataT, typename ... Info>
struct type_at
{
	using data_type = DataT;
	using pos_types = pspp::to_type_sequence<Info...>;
};

template <typename T>
struct is_type_at : std::false_type {};

template <class ArgT, typename ... Info>
struct is_type_at<type_at<ArgT, Info...>> : std::true_type {};

template <typename T>
concept c_type_at = is_type_at<T>::value;


template <typename DataT, auto t_default, typename ... Info>
struct with_default 
{
	// using default_vh = value_holder<t_default>;
	DataT data;
	bool is_default_value;
};

template <typename T>
struct is_with_default : std::false_type {};

template <typename DataT, auto t_default, typename ... Info>
struct is_with_default<with_default<DataT, t_default, Info...>> : std::true_type {};

template <typename ResT, typename ... ArgsT>
struct result_type 
{
	using res_t = ResT;
	using args_type = to_type_sequence<ArgsT...>;
};

template <typename ResT, typename ... TypesWithPos>
struct converter;

using converter_data = main_args_type;



namespace _private
{
	template <typename ResT, typename ... ArgTs>
	inline consteval auto get_function_args_impl(type_t<std::function<ResT(ArgTs...)>>)
	{
		return type<std::conditional_t<static_cast<bool>(sizeof...(ArgTs)),
						to_type_sequence<ArgTs...>,
						type_sequence<void>
					>>;
	}

	template <auto t_func>
	using function_args_types = extract_type<
		get_function_args_impl(type<decltype(std::function(t_func))>)
	>;
	
	template <typename ... Info>
	using get_first_or_current = typename std::conditional_t<static_cast<bool>(sizeof...(Info)), 
				to_type_sequence<Info...>,
				type_sequence<current_position>
				>::head;
	
	template <typename ... Infos>
	struct info_types_holder { using info_types = to_type_sequence<Infos...>; };
}


#ifdef PSPP_MODERN_MAIN_EXCEPTION
#define CONVERT_NOEXCEPT
#else
#define CONVERT_NOEXCEPT noexcept
#endif

template <typename ResT, typename ... TypesWithPos>
struct converter : _private::info_types_holder<TypesWithPos...>
{
	/*template <typename ... NextTypeTypesWithPos>
	using next_type = converter<ResT, NextTypeTypesWithInfo...>;

	*/

	template <std::size_t t_current>
	inline static auto convert(converter_data data) CONVERT_NOEXCEPT
	{
		// std::cout << "regular conver\n";
		return convert_impl<t_current>(std::make_index_sequence<sizeof...(TypesWithPos)>(), data);
	}

	private:

	template <typename TypeWithPos, std::size_t t_def_pos, std::size_t t_current>
	inline static auto get_converted_data(converter_data data) CONVERT_NOEXCEPT
	{
		// std::cout << "regular converter: \n";
		if constexpr (c_type_at<TypeWithPos>)
		{
			return TypeWithPos::pos_types::template passed_type<converter, 
							typename TypeWithPos::data_type>::template convert<t_current>(data);
		}
		else
		{
			return converter<TypeWithPos, relative_position<t_def_pos>>::template convert<t_current>(data);
		}
	}

	using this_t = converter<ResT, TypesWithPos...>;

	template <std::size_t t_current, std::size_t ... Is>
	inline static ResT convert_impl(std::index_sequence<Is...>, converter_data data) CONVERT_NOEXCEPT
	{
		if constexpr (sizeof...(Is) == 0)
			return ResT{};
		else 
			if constexpr (sizeof...(Is) == 1 and std::same_as<
					decltype(get_converted_data<TypesWithPos..., Is..., t_current>(data)),
					ResT
				>)
			{
				return get_converted_data<TypesWithPos..., Is..., t_current>(data);
			}
		else 
			if constexpr (requires {ResT(get_converted_data<TypesWithPos, Is>(data)...);})
				return ResT(get_converted_data<TypesWithPos, Is>(data)...);
			else if constexpr (requires {ResT{get_converted_data<TypesWithPos, Is, t_current>(data)...};})
			{
				return ResT{get_converted_data<TypesWithPos, Is, t_current>(data)...};
			}
			else
				static_assert(std::same_as<void, 
						type_sequence<ResT, decltype(get_converted_data<TypesWithPos, Is, t_current>(data))...>
					>, "could not create ResT (type at 0 pos) from given args (rest of types)"
				);
	}
};

template <std::integral IntT, typename ... Info>
struct converter<IntT, Info...> : _private::info_types_holder<Info...>
{
	template <std::size_t t_current>
	inline static IntT convert(converter_data data) CONVERT_NOEXCEPT
	{
		using pos_t = _private::get_first_or_current<Info...>;
		constexpr auto pos = get_pos<pos_t, t_current>();

#ifdef PSPP_MODERN_MAIN_EXCEPTION
		if (pos >= data.size())
			throw e_bad_args_access<pos>(data.size());
#endif 

		if constexpr (static_cast<IntT>(-1) > 0)
		{
			return static_cast<IntT>(std::stoull(data[pos].data()));
		}
		else
		{
			return static_cast<IntT>(std::stoll(data[pos].data()));
		}
	}
};

template <std::floating_point FloatT, typename ... Info>
struct converter<FloatT, Info...> : _private::info_types_holder<Info...>
{
	template <std::size_t t_current>
	inline static FloatT convert(converter_data data) CONVERT_NOEXCEPT
	{
		using pos_t = _private::get_first_or_current<Info...>;
		constexpr std::size_t pos = get_pos<pos_t, t_current>();

#ifdef PSPP_MODERN_MAIN_EXCEPTION
		if (pos > data.size())
			throw e_bad_args_access<pos>(data.size());
#endif

		// std::cout << "float?\n";
		return static_cast<FloatT>(std::stold(std::string{data[pos]}));
	}
};

template <typename CharT, typename ... Info>
struct converter<std::basic_string<CharT>, Info...> : _private::info_types_holder<Info...>
{
	template <std::size_t t_current>
	inline static std::basic_string<CharT> convert(converter_data data) CONVERT_NOEXCEPT
	{
		using pos_t = _private::get_first_or_current<Info...>;
		constexpr std::size_t pos = get_pos<pos_t, t_current>();

#ifdef PSPP_MODERN_MAIN_EXCEPTION
		if (pos > data.size())
			throw e_bad_args_access<pos>(data.size());
#endif

		if constexpr (std::same_as<CharT, char>)
			return std::string{data[pos]};
		else
			return std::basic_string<CharT>{
				reinterpret_cast<CharT const *>(data[pos].data()),
				data[pos].size() / sizeof(CharT)
			};
	}
};

template <typename ... ConstructInfo>
struct converter<std::string_view, ConstructInfo...> : 
	_private::info_types_holder<ConstructInfo...>
{
	template <std::size_t t_current>
	inline static std::string_view convert(converter_data data) CONVERT_NOEXCEPT
	{
		using pos_t = _private::get_first_or_current<ConstructInfo...>;
		constexpr std::size_t pos = get_pos<pos_t, t_current>();

		return data[pos];
	}
};

template <typename DataT, typename ... ConstructInfo>
struct converter_wraper
{
	DataT data;
};

template <typename DataT, typename ... ConstructInfo>
struct converter<converter_wraper<DataT, ConstructInfo...>> : 
	converter<converter_wraper<DataT, ConstructInfo...>, ConstructInfo...>
{};


namespace _private
{

template <typename ... Infos>
struct get_args_count_to_be_moved_t
{
	inline static consteval std::size_t get() noexcept
	{
		return (0u + ... + []<typename T>
		{
			if constexpr (c_type_at<T>)
				return []<typename ArgT, typename ... Ts>(type_t<type_at<ArgT, Ts...>>)
				{ return get_args_count_to_be_moved_t<Ts...>::get(); }(type<T>);
			else if constexpr (std::same_as<T, current_position>)
				return 1u;
			else if constexpr (c_special_position<T>)
				return 0u;
			else if constexpr (requires {converter<T>{};})
			{
				if constexpr (std::same_as<typename converter<T>::info_types, empty_type_sequence>)
					return 1u;
				else
					return []<typename ... Ts>(type_t<type_sequence<Ts...>>)
					{ return get_args_count_to_be_moved_t<Ts...>::get(); }(type<typename converter<T>::info_types>);
			}
		}.template operator()<Infos>());
	}
};

template <typename ... Infos>
inline consteval std::size_t get_args_count_to_be_moved() noexcept
{
	return get_args_count_to_be_moved_t<Infos...>::get();
}

template <c_value_sequence Res, typename First, typename ... Rest>
inline consteval auto get_indexes_for_args() noexcept
{
	constexpr std::size_t first_len = get_args_count_to_be_moved<First>();
	using next = typename Res::template append_vals<Res::last + first_len>;
	if constexpr (sizeof...(Rest) > 0)
		return get_indexes_for_args<
			next, Rest...>();
	else
		return type<next>;
}

template <typename First, typename ... Rest>
using indexes_for_args = typename extract_type<get_indexes_for_args<
		value_sequence<1u>, First, Rest...>()
	>::template pop_from_back<1u>::template all_custed_to<std::size_t>;

} // _private

template <typename DataT, typename ... Info>
struct converter<type_at<DataT, Info...>> : converter<DataT, Info...>
{};

template <typename DataT, auto t_default, typename ... Info>
struct converter<with_default<DataT, t_default, Info...>> : 
	_private::info_types_holder<Info...>
{
	using data_type = with_default<DataT, t_default, Info...>;

	template <std::size_t t_current>
	inline static data_type convert(converter_data data) CONVERT_NOEXCEPT
	{
		constexpr auto getter = [] -> DataT
		{
			if constexpr (requires { {t_default()} -> std::convertible_to<DataT>; })
				return t_default();
			else
				return t_default;
		};
		using indexes = 
			extract_type<
				[]{
					if constexpr ((sizeof...(Info) > 0))
						return type<typename to_type_sequence<Info...>::template passed_type<_private::indexes_for_args>>;
					else
						return type<value_sequence<0>>;
				}()>;

		if ((t_current + indexes::template most<big>) >= data.size())
			return {getter(), true};
		else
			return {
				converter<type_at<DataT, Info...>>::template convert<t_current>(data),
				false
			};
	}
};



} // namespace modern_args

template <typename T>
concept c_modern_main_body_with_res = std::convertible_to<T, std::function<int (main_args_type)>>;

template <typename T>
concept c_modern_main_body_without_res = std::convertible_to<T, std::function<void (main_args_type)>>;

template <typename T>
concept c_modern_main_body = c_modern_main_body_with_res<T> or
	c_modern_main_body_without_res<T>;

namespace modern_args
{
	template <std::size_t t_n>
	struct less_than : value_holder<t_n> {};

	template <std::size_t t_n>
	struct more_than : value_holder<t_n> {};

	template <std::size_t t_n>
	struct equal : value_holder<t_n> {};

	template <std::size_t t_start, std::size_t t_end>
	struct in_range
	{
		struct start : value_holder<t_start> {};
		struct end : value_holder<t_end> {};
	};
}


template <auto t_main_body, 
					typename ArgsCountCond = modern_args::equal<1>>
struct modern_main
{
	std::vector<std::string_view> args;
	using body_type = clean_type<t_main_body>;

	modern_main(int argc, char const * const * const argv):
		args{}
	{
		args.reserve(argc);

		for (int i = 0; i < argc; i++)
			args.emplace_back(argv[i]);
	}

	struct args_checker_t
	{

		template <modern_args::equal t_cond> 
		inline constexpr bool operator()(std::size_t argc) const noexcept
		{ return argc == clean_type<t_cond>::value; }
		template <modern_args::less_than t_cond> 
		inline constexpr bool operator()(std::size_t argc) const noexcept
		{ return argc < clean_type<t_cond>::value; }
		template <modern_args::more_than t_cond> 
		inline constexpr bool operator()(std::size_t argc) const noexcept
		{ return argc > clean_type<t_cond>::value; }
		template <modern_args::in_range t_cond> 
		inline constexpr bool operator()(std::size_t argc) const noexcept
		{
			return (argc >= clean_type<t_cond>::start::value and 
				argc <= clean_type<t_cond>::end::value);
		}
		/*template <auto t_unknown> 
		inline consteval bool operator()(std::size_t argc) const noexcept
		{
			static_assert(sizeof(t_unknown) < 0, "modern_main: args_checker: unknown args count condition");
			return false;
		}
		*/
	};

	/*inline static constexpr auto args_checker = overloaded_lambda<
		[]<modern_args::equal t_cond> (std::size_t argc)
		{ return argc == clean_type<t_cond>::value; },
		[]<modern_args::less_than t_cond> (std::size_t argc)
		{ return (argc < clean_type<t_cond>::value); },
		[]<modern_args::more_than t_cond> (std::size_t argc)
		{ return (argc > clean_type<t_cond>::value); },
		[]<modern_args::in_range t_cond> (std::size_t argc)
		{ 
			return (argc >= clean_type<t_cond>::start::value and 
				argc <= clean_type<t_cond>::end::value);
		},
		[]<auto t_unknown> (int)
		{
			static_assert(sizeof(t_unknown) < 0, "modern_main: args_checker: unknown args count condition");
		}
		>{};
		*/
	inline static constexpr auto args_checker = args_checker_t{};

	inline bool is_propriate_args_count() const noexcept
	{
		return args_checker.template operator()<ArgsCountCond{}>(args.size());
	}

	template <auto fail_handler>
	inline modern_main const & assert_args_count() const noexcept
	{
		if (not is_propriate_args_count())
		{
			if constexpr (requires { fail_handler(); })
				fail_handler();
			else if constexpr (requires {fail_handler(2u); })
				fail_handler(args.size());
			else if constexpr (requires {fail_handler(args);})
				fail_handler(args);

			std::exit(EXIT_FAILURE);
		}

		return *this;
	}

	int inline execute() const noexcept(noexcept(t_main_body))
	{
		if constexpr (c_modern_main_body_with_res<body_type>)
			return t_main_body(args);
		else if constexpr (c_modern_main_body_without_res<body_type>)
		{
			t_main_body(args);
			return 0;
		}
		else if constexpr (std::same_as<void,
				typename modern_args::_private::function_args_types<t_main_body>::head
			>)
		{
			t_main_body();
			return 0;
		}
		else 
		{
			using namespace modern_args::_private;
			using main_args = function_args_types<t_main_body>;
			using first = typename main_args::head;
			using corrected_args = extract_type<
					[]{
						if constexpr (std::same_as<first, main_args_type>)
							return type<typename main_args::shift>;
						else return type<main_args>;
					}()
			>;

			using indexes = typename corrected_args::template passed_type<indexes_for_args>;
//			static_assert(std::same_as<combine<corrected_args, indexes>, int>);

			if constexpr (std::same_as<typename 
					main_args::template passed_type<std::invoke_result_t, decltype(t_main_body)>, void>)
			{
				execute_body<first>(type<corrected_args>, type<indexes>);
				return 0;
			}
			else
				return execute_body<first>(type<corrected_args>, type<indexes>);
		}
	}

	private:
	template <typename First, typename ... Ts, std::size_t ... t_indexes>
	inline auto execute_body(type_t<type_sequence<Ts...>>, type_t<value_sequence<t_indexes...>>) const noexcept(noexcept(t_main_body))
	{
			if constexpr (std::same_as<First, main_args_type>)
				return t_main_body(args, modern_args::converter<Ts>::template convert<t_indexes>(args)...);
			else
				return t_main_body(modern_args::converter<Ts>::template convert<t_indexes>(args)...);
	}
};

template <auto t_main_body>
using to_modern_main = modern_main<t_main_body, 
			modern_args::equal<detail::get_count_of_args(t_main_body)>>;


template <pspp::c_modern_main_body auto t_body, 
					typename ArgsCountCond = pspp::modern_args::equal<1>,
					auto pre_checker = nulltv
				>
int main(int argc, char ** argv)
{
	pspp::modern_main<t_body, ArgsCountCond> new_main (argc, argv);
	if constexpr (not is_vnullt<pre_checker>)
		pre_checker(new_main);

	return new_main.execute();
}

using main_func_type = int(int, char**);

} // namespace  pspp

#define PSPP_MAIN_ENTRY_FOR(true_main_func) int main(int argc, char const * const * const argv) \
{ pspp::to_modern_main<true_main_func>(argc, argv).execute(); }

#define PSPP_TRUE_MAIN_ENTRY() PSPP_MAIN_ENTRY_FOR(true_main)

#undef CONVERT_NOEXCEPT

#endif // PSPP_MODERN_MAIN_HPP

