#ifndef PSPP_MODERN_MAIN_CONVERTER_SPECS_HPP
#define PSPP_MODERN_MAIN_CONVERTER_SPECS_HPP

#include <pspp/modern_main.hpp>
#include <filesystem>

namespace pspp
{
namespace modern_args
{

template <typename ... Infos>
struct converter<std::filesystem::path, Infos...> : 
	converter<type_at<std::filesystem::path, std::string_view>, Infos...> {};

} // modern_args
} // namespace pspp

#endif // PSPP_MODERN_MAIN_CONVERTER_SPECS_HPP
