#ifndef PSPP_MODERN_MAIN_HPP
#define PSPP_MODERN_MAIN_HPP

#include <format>
#include <pspp/meta.hpp>
#include <pspp/utility/iota_range.hpp>
#include <pspp/functional.hpp>
#include <pspp/input_converter.hpp>


// TODO: REWRITE CONVERSION SYSTEM TO USE pspp::convert_for
//       AND MAKE POSSIBLE TO PASS pspp::conversion_info TO
//       APPROPRIATE ARGS (FOR EACH INDIVIDUAL OR FOR ALL).



namespace pspp
{

using modern_args_type = std::span<std::string_view const>;

template <std::size_t t_n, auto t_op, constr t_msg_fmt>
struct mm_count_is
{
  template <std::size_t t_required = t_n>
  static constexpr void operator()(std::size_t was) noexcept
  {
    static constexpr std::size_t required = is_npos_v<t_n> ? t_required : t_n;

    if (not t_op(was, required))
    {
      std::string const tmp = std::format(t_msg_fmt.strv(), required, was);
      std::puts(tmp.c_str());

      std::exit(EXIT_FAILURE);
    }
  }
};

template <std::size_t t_n = npos>
struct count_equal_t :
  mm_count_is<t_n, equal_to, "was expecting {} arguments, but got only {}">
{};

template <std::size_t t_n = npos>
struct count_greater_t :
  mm_count_is<t_n, greater, "was expecting more than {} arguments, but got only {}">
{};

template <std::size_t t_n = npos>
struct count_greater_equal_t :
  mm_count_is<t_n, greater_equal, "was expecting more than or equal to {} arguments, but got only {}">
{};

template <std::size_t t_n = npos>
struct count_less_t :
  mm_count_is<t_n, less, "was expecting less than {} arguments, but got only {}">
{};

template <std::size_t t_n = npos>
inline constexpr count_equal_t<t_n> count_equal {};

template <std::size_t t_n = npos>
inline constexpr count_greater_t<t_n> count_greater {};

template <std::size_t t_n = npos>
inline constexpr count_greater_equal_t<t_n> count_greater_equal {};

template <std::size_t t_n = npos>
inline constexpr count_less_t<t_n> count_less {};




using all_args = modern_args_type;

namespace
{
inline constexpr struct execute_modern_main_t
{

} execute_modern_main_t;
}

template <auto t_fun, std::size_t t_max_args = 255>
struct modern_main
{
	using arguments_type = std::array<std::string_view, t_max_args>;
	static_val raw_function_info = get_func_info(t_fun);
	static_val raw_argument_types = raw_function_info.argument_types | remove_cvref;


	private:
	struct modern_main_info
	{
		std::size_t arguments_count;
		bool need_all_first;
	};

	constexpr static auto get_modern_main_function_info() noexcept
	{
		if constexpr (raw_function_info.argument_types | is_empty)
			return modern_main_info{0, false};
		else 
		{
			static constexpr bool need_all_first =
        raw_function_info.argument_types | head == type<modern_args_type>;

			return modern_main_info{raw_function_info.argument_types.size - need_all_first, need_all_first};
		}
	}

	public:

	static_val info = get_modern_main_function_info();
	static_flag use_exceptions = not raw_function_info.is_noexcept;

  static_size required_minimum_args_count =
    raw_argument_types
    | subseq<info.need_all_first>
    | convert_to_vals_and_execute<
      []<typename T>{ return convert_for_fnt<T>::naive_count; },
      []<auto ... t_is>{ return (0 + ... + t_is); }
    >;



      arguments_type m_arguments;
	std::size_t m_arguments_count;

	constexpr explicit(true) modern_main(int const count, char const * const * const args) :
    m_arguments{},
    m_arguments_count{std::min(static_cast<std::size_t>(count), t_max_args)}
	{
		for (auto i : iota_range(m_arguments_count))
			m_arguments[i] = std::string_view(*std::next(args, i));
	}

  /**
   * @brief checks if enough arguments was passed from user;
   * @warning args[0] (program name) is not counted.
   * @tparam CallBackFuncT function type which will proceed assertion.
   * can take default calculated required arguments count as template argument.
   * @param func function instance
   * @return reference to self
   */
	template <typename CallBackFuncT = count_greater_equal_t<> const &>
	constexpr modern_main & assert_arguments_count(CallBackFuncT && func = count_greater_equal<>) noexcept
	{
    pspp_execute_if_can(std::forward<CallBackFuncT>(func).template
      operator()<required_minimum_args_count>(m_arguments_count - 1));
    else { std::forward<CallBackFuncT>(func)(m_arguments_count - 1); }

    return *this;
	}

  /**
   * @brief checks if enough arguments was passed from user;
   * @warning args[0] (program name) is not counted.
   * @tparam CallBackFuncT function type which will proceed assertion.
   * can take default calculated required arguments count as template argument.
   * @param func function instance
   * @return reference to self
   */
  template <typename CallBackFuncT = count_greater_equal_t<> const &>
  constexpr modern_main const & assert_arguments_count(CallBackFuncT && func = count_greater_equal<>) const noexcept
  {
    pspp_execute_if_can(std::forward<CallBackFuncT>(func).template
      operator()<required_minimum_args_count>(m_arguments_count - 1));
    else { std::forward<CallBackFuncT>(func)(m_arguments_count - 1); }

    return *this;
  }

  /**
   * @brief executes function passed to modern_main
   * @return result of main func execution or 0 if void was returned
   */
	constexpr int execute() const noexcept
	{
		if constexpr (raw_function_info.result == type<void>)
		{
			_execute();
			return 0;
		}
		else return _execute();
	}

  /**
   * @brief executes function passed to modern_main with passed conversion info
   * @param convinfo info for conversion (default values/error handlers...)
   * @return result of main func execution or 0 if void was returned
   */
  template <c_conversion_info InfoT>
  constexpr int execute(InfoT && convinfo) const noexcept
  {
    if constexpr (raw_function_info.result == type<void>)
    {
      _execute(std::forward<InfoT>(convinfo));
      return 0;
    }
    else return _execute(std::forward<InfoT>(convinfo));
  }

	private:

  template <c_conversion_info InfoT = default_convinfo_t const &>
	constexpr auto _execute(InfoT && convinfo = default_conversion_info) const noexcept (not use_exceptions)
	{
		return [this, convinfo = std::forward<InfoT>(convinfo)]<typename ... Ts>(type_sequence<Ts...>)
		{
      auto args_view = data_holder<std::string_view const>(m_arguments, m_arguments_count);
      args_view.shrink_head();

      auto tmp = convert_for<typename Ts::type...>(args_view, std::forward<InfoT>(convinfo));

			if constexpr (info.need_all_first)
      {
        if constexpr (sizeof...(Ts) == 1)
          return t_fun(std::span<std::string_view const>(m_arguments.begin(),
                                                         std::next(m_arguments.begin(), m_arguments_count)),
                       std::move(tmp)
                       );
        else
        return t_fun(std::span<std::string_view const>(m_arguments.begin(),
                                                       std::next(m_arguments.begin(), m_arguments_count)),
                     std::move(tmp).template get<Ts::index>()...);
      }
			else {
        if constexpr (sizeof...(Ts) == 1)
          return t_fun(std::move(tmp));
        else
          return t_fun(std::move(tmp).template get<Ts::index>()...);
      }
		}(raw_argument_types | subseq<info.need_all_first> | index);
	}
};

} // namespace pspp

/**
 * @param main_func_name
 * @param vaargs
 */
#define PSPP_SIMPLE_MODERN_MAIN_ENTRY(main_func_name, ...)\
	int main(int argc, char const * const * const argv)\
	{ return pspp::modern_main<main_func_name>(argc, argv).execute(__VA_ARGS__); \
	}

#define PSPP_MODERN_MAIN(main_func_name, ...)       \
  int main(int argc, char const * const * const argv)     \
  {                                                       \
     return pspp::modern_main<main_func_name>(argc, argv).__VA_ARGS__ ;\
  }

#define PSPP_TRUE_MAIN(...) \
  PSPP_MODERN_MAIN(true_main __VA_OPT__(,) __VA_ARGS__)

#define PSPP_TRUE_MAIN_ENTRY()\
	PSPP_TRUE_MAIN(execute())

#endif // PSPP_MODERN_MAIN_HPP
