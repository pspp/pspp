# pspp::value\_sequence

Helper class for working with value sequence at compile time (passed as non-type template parameters).

## Usage Example: 

```cpp
#include <pspp/value_sequence.hpp>

template <auto ... values>
void values_handler()
{
    using seq = pspp::value_sequence<values...>;
    static_assert(seq::count > 2); // same as sizeof...(values) > 2

    typename seq::types::template at<1> val2 = seq::template at<1>; 
    //↑just create val2 with the same type as second element and init with value of second element

    if constexpr (seq::is_unique)
    {
        // some data processing
    }

    auto biggest = seq::template most<pspp::bigger>;
    // ...
}
```


