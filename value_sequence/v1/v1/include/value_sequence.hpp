#ifndef PSPP_VALUES_SEQUENCE_HPP
#define PSPP_VALUES_SEQUENCE_HPP

#include <pspp/useful/sequence_helpers.hpp>
#include <pspp/value_holder.hpp>
#include <pspp/type_helpers.hpp>
#include <pspp/type_sequence.hpp>

namespace pspp
{
inline namespace meta
{
using simple_comparator = decltype([]<typename T1, typename T2>{return std::same_as<T1, T2>;});

template <auto t_func = simple_comparator{}>
struct compare_func
{
	using valueh = value_holder<t_func>;
	inline constexpr type_t<valueh> to_type() const noexcept { return {}; }
};

template <auto t_first, auto t_second>
struct vpair
{
	using first_type = clean_type<t_first>;
	using second_type = clean_type<t_second>;

	inline static constexpr first_type first = t_first;
	inline static constexpr second_type second = t_second;
};

template <auto ... t_vals>
struct value_sequence;

template <typename T>
struct is_value_sequence : std::false_type {};

template <auto ... t_vals>
struct is_value_sequence<value_sequence<t_vals...>> : std::true_type {};

template <typename T>
struct is_not_empty_value_sequence : std::false_type {};

template <auto t_first, auto ... t_rest>
struct is_not_empty_value_sequence<value_sequence<t_first, t_rest...>> : std::true_type {};

template <typename T>
concept c_value_sequence = is_value_sequence<T>::value;

template <typename T>
concept c_not_empty_value_sequence = is_not_empty_value_sequence<T>::value;



template <typename T>
struct is_compare_func : std::false_type {};

template <auto t_func>
struct is_compare_func<compare_func<t_func>> : std::true_type {};

template <typename MaybeCompareFunc>
concept c_compare_func = is_compare_func<MaybeCompareFunc>::value;

inline constexpr auto big = []<c_value_holder T1, c_value_holder T2>{ return T1::value > T2::value; };
inline constexpr auto small = []<c_value_holder T1, c_value_holder T2>{ return T1::value < T2::value; };

template <auto ... t_opts>
struct value_sequence_options
{
	using compare_func_opt = extract_type<
		get_last_satisfied_check_value<
			[]<typename T>{ return c_compare_func<T>; },
			compare_func{}, t_opts...>().to_type()>;
};

template <typename ToBeChange, typename NewType>
inline constexpr auto cast_to = change_to<value_holder<static_cast<NewType>(ToBeChange::value)>>;

template <auto ... t_values>
struct value_sequence 
{
	using values = type_sequence<value_holder<t_values>...>;
	using types = typename values::template transform<[]<typename T>{ return change_to<typename T::type>; }>;
	using this_t = value_sequence<t_values...>;

	struct helpers 
	{
		template <c_type_sequence ValsHolder>
		struct from_types_holder 
		{
			using seq = typename ValsHolder::template converted_values_passed_type<
			[]<typename T>{ return T::value; }, value_sequence>;
		};
	};

	inline static constexpr std::size_t count = sizeof...(t_values);
	inline static constexpr bool is_empty = count == 0;

	template <c_type_sequence Seq>
	using from_seq = typename Seq::template converted_values_passed_type<
		[]<typename T>{ return T::value; },
		this_t
	>;

	inline static constexpr auto head = values::head::value;
	inline static constexpr auto last = values::last::value;

	template <std::size_t t_n>
	inline static constexpr auto at = values::template at<t_n>::value;

	template <std::size_t t_n>
	inline static constexpr auto at_from_back = values::template at_from_back<t_n>::value;
	
	template <auto t_comp_fn>
	using custom_unique = typename values::template custom_unique<t_comp_fn>::
		template converted_values_passed_type<
		[]<typename T>{ return T::value; },
		value_sequence> 
	;

	template <std::size_t t_start = 0, std::size_t t_end = count, std::size_t t_step = 1>
	using sub_sequence = typename helpers::template from_types_holder<
		typename values::template sub_sequence<t_start, t_end, t_step>
	>::seq;

	template <std::size_t t_count>
	using pop_from_back = typename helpers::template from_types_holder<
		typename values::template pop_from_back<t_count>>::seq;

	template <auto t_transformer>
	using transform = typename helpers::template from_types_holder<typename values::template transform<t_transformer>>::seq;

	template <c_value_sequence ToAppend>
	using append = typename helpers::template from_types_holder<typename values::template append<typename ToAppend::values>>::seq;

	template <auto ... t_to_append>
	using append_vals = value_sequence<t_values..., t_to_append...>;

	template <typename NewT>
	using all_custed_to = transform<[]<typename T>{ return cast_to<T, NewT>; }>;

	using unique = typename values::template custom_unique<[]<typename T1, typename T2>{return std::is_same_v<T1, T2>; } >::
		template converted_values_passed_type<
		[]<typename T>{ return T::value; },
		value_sequence>;

	template <template <auto ...> class ToBePassed>
	using passed_type = ToBePassed<t_values...>;

	template <auto t_func>
	inline static constexpr auto passed_lambda = []{ return static_call<t_func, t_values...>()(); };

	template <auto t_func>
	using passed_lambda_type = std::remove_cvref_t<decltype(passed_lambda<t_func>)>;

	template <auto t_checker>
	inline static constexpr auto most_satisfied = values::template most_satisfied<t_checker>::value;

	template <auto t_checker>
	inline static constexpr auto most = most_satisfied<t_checker>;

	template <auto t_checker>
	inline static constexpr auto most_failed = values::template most_failed<t_checker>::value;

	template <auto t_val, auto ... t_vals>
	inline static constexpr bool contains = values::template contains<value_holder<t_val>, value_holder<t_vals>...>;

	inline static constexpr bool is_unique = values::is_unique;

	template <auto t_sorter>
	using sorted = typename helpers::template from_types_holder<typename values::template sorted<t_sorter>>::seq;

	value_sequence(auto);
};

template <std::size_t ... t_is>
value_sequence(std::index_sequence<t_is...>) -> value_sequence<t_is...>;

template <>
struct value_sequence<>
{
	inline static constexpr std::size_t size = 0;
	inline static constexpr bool is_empty = true;

	template <c_value_sequence T>
	using append = T;

	template <auto ... t_vals>
	using append_vals = value_sequence<t_vals...>;

	template <c_value_sequence T>
	using append_to_front = T;

	template <auto ... t_vals>
	using append_vals_to_front = value_sequence<t_vals...>;
};

template <class T>
struct nested_type_to_valseq_impl
{
	template <template <auto ...> class ToExtract, auto ... vals>
	inline static consteval type_t<value_sequence<vals...>> get(type_t<ToExtract<vals...>>) 
	{ return {}; }

	template <template <typename, auto ...> class ToExtract, typename ValsT, auto ... vals>
	inline static consteval type_t<value_sequence<vals...>> get(type_t<ToExtract<ValsT, vals...>>)
	{ return {}; }

	using res = extract_type<get(type_t<T>{})>;
};

template <class T>
using type_with_nested_to_vs= typename nested_type_to_valseq_impl<T>::res;

template <std::size_t t_end>
using vindexes = type_with_nested_to_vs<
	std::remove_cvref_t<decltype(std::make_index_sequence<t_end>())>
>;

template <std::size_t t_start, std::size_t t_end, std::size_t t_step = 1>
using vindexes_range = type_with_nested_to_vs<
		pspp::index_sequence_range<t_start, t_end, t_step>
	>;

/*using indexised = 
		typename helpers::template from_types_holder<
			typename values::indexised::template transform<[]<typename T>{
			return change_to<
				value_holder<
					vpair<T::first::value, T::second::value>{}
				>
			>;
		}>
	>;

	using indexised_vs = typename indexised::template transform<[]<typename T>
	{
		return change_to<value_sequence<T::type::first, T::type::second>>;
	}>;*/

template <c_value_sequence ValSeq>
consteval auto vs_indexise_impl() noexcept
{
	return pspp::type<typename pspp::value_sequence<0>::helpers::from_types_holder<
				typename pspp::type_sequence<void>::helpers::from_base_t<
					ValSeq::values::helpers::indexise_impl()>::template transform<[]<typename T>{
						return pspp::change_to<pspp::value_holder<pspp::vpair<T::first::value, T::second::value>{}>>;
					}>
			>::seq>;
}

template <c_value_sequence ValSeq>
consteval auto vs_indexise_with_vs_impl() noexcept
{
	return pspp::type<typename pspp::value_sequence<0>::helpers::from_types_holder<
				typename pspp::type_sequence<void>::helpers::from_base_t<
					ValSeq::values::helpers::indexise_impl()>::template transform<[]<typename T>{
						return pspp::change_to<pspp::value_holder<pspp::value_sequence<T::first::value, T::second::value>{}>>;
					}>
			>::seq>;
}

template <c_value_sequence ValSeq>
using vs_indexised = extract_type<vs_indexise_impl<ValSeq>()>;

template <c_value_sequence ValSeq>
using vs_indexised_with_vs = extract_type<vs_indexise_with_vs_impl<ValSeq>()>;

} // namespace meta
} // namespace pspp

#endif // PSPP_VALUES_SEQUENCE_HPP
