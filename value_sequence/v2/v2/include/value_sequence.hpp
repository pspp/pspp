#ifndef PSPP_VALUES_SEQUENCE_HPP
#define PSPP_VALUES_SEQUENCE_HPP

#include <pspp/type_sequence.hpp>

namespace pspp::inline meta
{
using simple_comparator = decltype([]<typename T1, typename T2>{return std::same_as<T1, T2>;});

template <auto t_func = simple_comparator{}>
struct compare_func
{
	using valueh = value_holder<t_func>;
	inline constexpr type_t<valueh> to_type() const noexcept { return {}; }
};

template <auto t_first, auto t_second>
struct vpair
{
	using first_type = clean_type<t_first>;
	using second_type = clean_type<t_second>;

	inline static constexpr first_type first = t_first;
	inline static constexpr second_type second = t_second;
};

template <std::size_t t_i, auto t_val1, auto t_val2>
constexpr auto get(vpair<t_val1, t_val2>)
{ return conditional_v<t_i == 0, t_val1, t_val2>; }

template <auto ... t_vals>
struct value_sequence;

template <typename T>
struct is_value_sequence : std::false_type {};

template <auto ... t_vals>
struct is_value_sequence<value_sequence<t_vals...>> : std::true_type {};

template <typename T>
struct is_not_empty_value_sequence : std::false_type {};

template <auto t_first, auto ... t_rest>
struct is_not_empty_value_sequence<value_sequence<t_first, t_rest...>> : std::true_type {};

template <typename T>
concept c_value_sequence = is_value_sequence<T>::value;

template <typename T>
concept c_not_empty_value_sequence = is_not_empty_value_sequence<T>::value;

namespace meta_detail
{
	template <auto ... t_val>
	struct _extract_type_impl<value_sequence<t_val...>>
	{ using type = value_sequence<t_val...>; };
};


template <typename T>
struct is_compare_func : std::false_type {};

template <auto t_func>
struct is_compare_func<compare_func<t_func>> : std::true_type {};

template <typename MaybeCompareFunc>
concept c_compare_func = is_compare_func<MaybeCompareFunc>::value;

inline constexpr auto big = []<c_value_holder T1, c_value_holder T2>{ return T1::value > T2::value; };
inline constexpr auto small = []<c_value_holder T1, c_value_holder T2>{ return T1::value < T2::value; };



template <auto ... t_vals>
struct value_sequence
{
	using ts = type_sequence<value_holder<t_vals>...>;
	static_size size = sizeof...(t_vals);
	static constexpr auto tsv = ts{};

	constexpr bool operator==(value_sequence) const noexcept { return true; }

	template <auto ... t_uvals>
	constexpr bool operator==(value_sequence<t_uvals...>) const noexcept
  { return false; }

	constexpr auto operator[](auto t_n) const noexcept
	{ return extract_type<ts{}[t_n]>::value; }

	template <template <auto...> class HolderT>
	constexpr value_sequence(HolderT<t_vals...>) {}

	template <typename T, template <typename, auto...> class TypedHolderT>
	constexpr value_sequence(TypedHolderT<T, t_vals...>) {}

	constexpr value_sequence() = default;
	// constexpr value_sequence(ts) {}
};

template <auto ... t_vals, template <auto...> class HolderT>
value_sequence(HolderT<t_vals...>) -> value_sequence<t_vals...>;

template <typename T, auto ... t_vals, template <typename, auto ...> class TypedHolderT>
value_sequence(TypedHolderT<T, t_vals...>) -> value_sequence<t_vals...>;

template <typename T>
struct ts2vs {
	using type = int;
	static_assert(std::same_as<T, void>, "tried to convert type_sequence to value_sequence but got wrong type");
};

template <auto ... t_vals>
struct ts2vs<type_sequence<value_holder<t_vals>...>> :
	std::type_identity<value_sequence<t_vals...>>
{};


template <typename T>
inline constexpr auto ts2vs_v = typename ts2vs<T>::type{};

} // namespace pspp::inline meta

namespace pspp::detail
{
	constexpr auto convert_if_needed(auto val) noexcept
	{
		using res_type = std::remove_cvref_t<decltype(val)>;

    if constexpr (c_complex_target<res_type, type_sequence>)
    {
      return ts2vs_v<res_type>; // for type_sequence
    }
    else if constexpr (c_type_holder<res_type>)
    {
      if constexpr (c_value_holder<extract_type<res_type{}>>)
        return extract_type<res_type{}>::value; // result like at(1_c) and so on
      else
        return res_type{};
    }
    else
    {
      //static_assert(pspp::c_complex_vtarget<res_type, mimpl::conditional_info_wrapper>);
      return val; // result like is_unique/index_of and so on
    }
	}
} // namespace pspp::detail

namespace pspp::inline meta
{

template <auto ... t_vals, typename FuncT>
constexpr auto operator|(value_sequence<t_vals...> vs, FuncT&& func) noexcept
  requires (not c_complex_target<FuncT, adaptor_closure_operation_pipe>)
{
  pspp_execute_and_return_if_can(std::forward<FuncT>(func)(value_sequence<t_vals...>{}));
  else return detail::convert_if_needed(std::forward<FuncT>(func)(typename decltype(vs)::ts{}));
}

template <auto ... t_vals, auto t_func, typename ... Ts>
constexpr auto operator|(value_sequence<t_vals...>, mimpl::pass_and_execute_fnt<t_func, Ts...>) noexcept
{
  return static_call<t_func, t_vals..., Ts{}...>();
}

template <auto ... t_vals>
constexpr auto operator|(value_sequence<t_vals...> vs, mimpl::_if_fnt) noexcept
{ return mimpl::conditional_holder<decltype(vs){}>{}; }

template <auto ... t_vals>
constexpr auto operator|(value_sequence<t_vals...> vs, mimpl::_else_fnt) noexcept
{ return mimpl::conditional_info_wrapper<true, true, decltype(vs){}>{}; }


template <auto t_convertor, auto t_projector, auto ... t_vals>
constexpr auto operator|(value_sequence<t_vals...> vs, mimpl::transform_fnt<t_convertor, t_projector> tsf)
{
	static constexpr auto caller = []<typename T>
	{
		constexpr auto res = static_call<t_convertor, T>();
		//static_assert(res == -1);
		using res_t = decltype(res);
		if constexpr (c_value_sequence<res_t>)
			return res_t{};
		else return std::type_identity<value_holder<res>>{};
	};

	using ts = typename value_sequence<t_vals...>::ts;
	constexpr auto tmp_transformed = ts{} | mimpl::transform_fnt<caller, t_projector>{};
	return detail::convert_if_needed(tmp_transformed);
}

static_assert(
  value_sequence<1, 2, 3, 4>{} | transform<[]<auto val>{ return val * 2; }> ==
    value_sequence<2, 4, 6, 8>{}
  );

template <auto ... t_vals>
constexpr auto operator|(value_sequence<t_vals...>, mimpl::index_fnt)
{
	return 
		[]<auto ... t_is>(std::index_sequence<t_is...>)
		{ return value_sequence<vpair<t_vals, t_is>{}...>{}; }
		(std::make_index_sequence<sizeof...(t_vals)>{});
}


template <auto ... t_vals>
struct is_sequence<value_sequence<t_vals...>> : std::true_type {};

template <auto ... t_vals>
inline constexpr value_sequence<t_vals...> valseq {};

namespace mimpl
{
	template <typename T>
	struct cast_to_fnt : adaptor_pipe_closure
	{
		template <auto ... t_vals>
		constexpr auto pspp_static_call_const_operator(value_sequence<t_vals...>) noexcept
		{ return value_sequence<static_cast<T>(t_vals)...>{}; }
	};
}

template <typename T>
inline constexpr mimpl::cast_to_fnt<T>                                              cast_to {};
template <auto t_comparator>
inline constexpr mimpl::most_fnt<[]<typename T>{return T::value; }, t_comparator>   vmost {};
template <auto t_value>
inline constexpr mimpl::contains_fnt<value_holder<t_value>>                         vcontains {};
template <auto ... t_vals>
inline constexpr mimpl::append_types_fnt<value_holder<t_vals>...>                   append_values {};
template <auto ... t_vals>
inline constexpr mimpl::append_types_to_front_fnt<value_holder<t_vals>...>          append_values_to_front {};


namespace meta_detail 
{
template <auto t_start, auto t_end, auto t_step>
constexpr auto vs_indexes_range() noexcept
{
	constexpr mimpl::subseq_info info = mimpl::check_and_get_subseq_info<t_start, t_end, t_step, npos>();
	return value_sequence{index_sequence_range<info.begin, info.end, info.step>{}};
}
}

template <auto t_start, auto t_end, auto t_step = 1>
inline constexpr auto vs_indexes_range = meta_detail::vs_indexes_range<t_start, t_end, t_step>();

} // namespace pspp::inline meta

template <auto t_val1, auto t_val2>
struct std::tuple_size<pspp::vpair<t_val1, t_val2>>
{ static_size value = 2; };

template <std::size_t t_i, auto t_val1, auto t_val2>
struct std::tuple_element<t_i, pspp::vpair<t_val1, t_val2>> : 
	std::conditional<t_i == 0, decltype(t_val1), decltype(t_val2)>
{};


#endif // PSPP_VALUES_SEQUENCE_HPP
