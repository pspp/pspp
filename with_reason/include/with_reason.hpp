#ifndef pspp_with_reason_hpp 
#define pspp_with_reason_hpp 

namespace pspp
{

	/*
template <auto ... t_values>
struct additional_information {};

template <typename T>
struct is_additional_information : std::false_type {};

template <auto ... t_values>
struct is_additional_information<additional_information<t_values...>> : std::true_type {};

template <typename T>
concept c_additional_information = is_additional_information<T>::value;
*/
template <bool t_condition, auto ... t_reasons>
inline constexpr auto with_reason = t_condition;

} // namespace pspp

#endif // pspp_with_reason_hpp
