# pspp::with\_reason

An simple bool constant with some pspp::const\_str message to be desplayed at compile time if requirement is not satisfied.


## Usage


```cpp
#include <pspp/with_reason.hpp>

template <int n>
void foo() requires (pspp::with_reason<n > 2, "n must be greater than 2")
{}

int main()
{
    foo<0>(); // error with message also displayed
}

```



