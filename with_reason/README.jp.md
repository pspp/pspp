# pspp::with\_reason

簡単な、pspp:const\_strで表された問題の詳細を説明する文を添えたconst bool変数。


## Usage


```cpp
#include <pspp/with_reason.hpp>

template <int n>
void foo() requires (pspp::with_reason<n > 2, "n must be greater than 2")
{}

int main()
{
    foo<0>(); // error with message also displayed
}

```




