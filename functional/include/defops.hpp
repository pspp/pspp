#ifndef PSPP_DEFOPS_HPP
#define PSPP_DEFOPS_HPP

#include <pspp/ops.hpp>
#include <pspp/utility/utility.hpp>
#include <pspp/simple_overloaded_lambda.hpp>

namespace pspp
{

/*inline constexpr auto all_operations = typeseq<
  plus_t, minus_t, multiplies_t, divides_t, modulus_t, negate_t,

  equal_to_t, not_equal_to_t, greater_t, less_t, greater_equal_t, less_equal_t,

  logical_and_t, logical_or_t, logical_not_t,

  bit_and_t, bit_or_t, bit_xor_t, bit_not_t,
  bit_shift_left_t, bit_shift_right_t
>;

inline constexpr auto all_arithmetic_operations = typeseq<
  plus_t, minus_t, multiplies_t, divides_t, modulus_t, negate_t
>;

inline constexpr auto all_comparison_operations = typeseq<
  equal_to_t, not_equal_to_t, greater_t, less_t, greater_equal_t, less_equal_t
>;

inline constexpr auto all_logical_operations = typeseq<
  logical_and_t, logical_or_t, logical_not_t
>;

inline constexpr auto all_bit_operations = typeseq<
  bit_and_t, bit_or_t, bit_xor_t, bit_not_t,
  bit_shift_left_t, bit_shift_right_t
>;

inline constexpr auto all_bin_operations = typeseq<
  plus_t, minus_t, multiplies_t, divides_t, modulus_t,

  equal_to_t, not_equal_to_t, greater_t, less_t, greater_equal_t, less_equal_t,

  logical_and_t, logical_or_t,

  bit_and_t, bit_or_t, bit_xor_t, bit_shift_left_t, bit_shift_right_t
>;
 */

template <typename ... OperatorTs>
struct enable_ops;

namespace
{
#define pspp_range_adaptor_closure adaptor_pipe_closure, std::ranges::range_adaptor_closure
template <typename OperatorT>
struct op_enabler
{
  template <typename U>
  consteval bool is_enabled(U) const noexcept = delete;

  consteval bool is_enabled(type_t<OperatorT>) const noexcept
  { return true; }

  template <typename T, typename OperatorU>
  consteval bool is_enabled_for(type_t<OperatorU>, type_t<T> = {}) const noexcept = delete;

  /// @brief always returns true and exists only in case when type checking is added in future
  template <typename T>
  consteval bool is_enabled_for(type_t<OperatorT>, type_t<T> = {}) const noexcept
  { return true; }
};

template <typename T>
concept c_op_enabler = requires { { T::is_enabled() } -> std::same_as<bool>; };


struct dummy_is_enabled_holder
{
  consteval bool is_enabled() const noexcept { return false; }
};

template <typename MaybeHasIsEnabled>
struct is_enabled_holder_checker :
  dummy_is_enabled_holder,
  MaybeHasIsEnabled
{};

template <typename T>
concept c_has_is_enabled = std::is_class_v<T> and not requires {
  &is_enabled_holder_checker<T>::is_enabled;
};

template <c_has_is_enabled T>
struct op_enabler<T> : T
{};

} // namespace


template <typename ... OpsToEnable>
struct enable_ops : op_enabler<OpsToEnable>...
{
  using op_enabler<OpsToEnable>::is_enabled...;
  using op_enabler<OpsToEnable>::is_enabled_for...;
};

struct enable_all_arithmetic_ops :
  enable_ops<
    plus_t, minus_t, multiplies_t, divides_t, modulus_t, negate_t
  >
{ };

struct enable_all_comparison_ops :
  enable_ops<
    equal_to_t, not_equal_to_t, greater_t, less_t, greater_equal_t, less_equal_t
  >
{ };

struct enable_all_bit_ops :
  enable_ops<
    bit_and_t, bit_or_t, bit_xor_t, bit_not_t
  >
{ };

struct enable_all_logical_ops :
  enable_ops<
    logical_and_t, logical_or_t, logical_not_t
  >
{ };

struct enable_all_bin_ops :
  enable_ops<
    plus_t, minus_t, multiplies_t, divides_t, modulus_t,
    equal_to_t, not_equal_to_t, greater_t, less_t, greater_equal_t, less_equal_t,
    logical_and_t, logical_or_t,
    bit_and_t, bit_or_t, bit_xor_t, bit_shift_left_t, bit_shift_right_t
  >
{ };

struct enable_all_ops :
  enable_ops<
    enable_all_bin_ops,
    negate_t, logical_not_t, bit_not_t
  >
{ };

struct enable_all_bin_ops_except_pipe :
  enable_ops<
    plus_t, minus_t, multiplies_t, divides_t, modulus_t,
    equal_to_t, not_equal_to_t, greater_t, less_t, greater_equal_t, less_equal_t,
    logical_and_t, logical_or_t,
    bit_and_t,  bit_xor_t, bit_shift_left_t, bit_shift_right_t
  >
{};



template <typename T>
struct define_ops 
{ using not_overloaded = void; };

template <auto t_func>
struct arithmetic_ops_mediator 
{ static_val arithmetic_mediator = t_func; };

template <auto t_func>
struct comparison_ops_mediator 
{ static_val comparison_mediator = t_func; };

template <auto t_func>
struct logic_ops_mediator 
{ static_val logical_mediator = t_func; };

template <auto t_func>
struct bit_ops_mediator 
{ static_val bit_mediator = t_func; };

template <auto ... t_func>
struct ops_mediator
{ static_val mediator = simple_overloaded_lambda{t_func...}; };


template <typename TargetT>
struct forbid_automatic_operators_overload_for
{};


template <typename T, typename Operation>
concept c_op_enabled =
  requires {
    requires define_ops<std::remove_cvref_t<T>>{}.is_enabled(type<Operation>) and
      not
        (
          requires {
            requires
            forbid_automatic_operators_overload_for<std::remove_cvref_t<T>>{}.is_enabled(type<Operation>);
          }
        );
  };


template <typename EnabledForTarget, typename Target, typename Operation>
concept c_op_enabled_for =
  define_ops<std::remove_cvref_t<Target>>{}.template
    is_enabled_for<std::remove_cvref_t<EnabledForTarget>>(type<Operation>) and
    not
      (
        requires {
          requires
          forbid_automatic_operators_overload_for<std::remove_cvref_t<EnabledForTarget>>{}.is_enabled(type<Operation>);
        }
      );


template <typename T>
concept c_has_arithmetic_ops_mediator = requires {
	define_ops<std::remove_cvref_t<T>>::arithmetic_mediator;
};

template <typename T>
concept c_has_logic_ops_mediator = requires {
	define_ops<std::remove_cvref_t<T>>::logic_mediator;
};

template <typename T>
concept c_has_comparison_ops_mediator = requires {
	define_ops<std::remove_cvref_t<T>>::comparison_mediator;
};

template <typename T>
concept c_has_bit_ops_mediator = requires {
	define_ops<std::remove_cvref_t<T>>::bit_mediator;
};

template <typename T>
concept c_has_ops_mediator = requires {
	define_ops<std::remove_cvref_t<T>>::mediator;
};



namespace detail
{
template <auto t_func, typename First, typename ... ArgTs>
pspp_always_inline constexpr auto execute_arithmetic_op(First && first, ArgTs && ... vals) noexcept
{
	using first_type = std::remove_cvref_t<First>;
	if constexpr (c_has_arithmetic_ops_mediator<First>)
		return static_call<define_ops<first_type>::arithmetic_mediator, t_func>(
      std::forward<First>(first), std::forward<ArgTs>(vals)...);
	else if constexpr (c_has_ops_mediator<std::remove_cvref_t<first_type>>)
		return static_call<define_ops<first_type>::mediator, t_func>(std::forward<First>(first), std::forward<ArgTs>(vals)...);
	else return t_func(std::forward<First>(first), std::forward<ArgTs>(vals)...);
}

template <bool is_reversed, auto t_func, typename First, typename Second>
pspp_always_inline constexpr auto execute_bin_arithmetic_op(First && first, Second && second) noexcept
{
  using first_type = std::conditional_t<
    is_reversed,
    std::remove_cvref_t<Second>,
    std::remove_cvref_t<First>
  >;

  if constexpr (c_has_arithmetic_ops_mediator<first_type>)
    return static_call<define_ops<first_type>::arithmetic_mediator, t_func>(
      std::forward<First>(first), std::forward<Second>(second));
  else if constexpr (c_has_ops_mediator<first_type>)
    return static_call<define_ops<first_type>::mediator, t_func>(
      std::forward<First>(first), std::forward<Second>(second));
  else return t_func(std::forward<First>(first), std::forward<Second>(second));
}

template <auto t_func, typename First, typename ... ArgTs>
pspp_always_inline constexpr auto execute_logic_op(First && first, ArgTs && ... vals) noexcept
{
	using first_type = std::remove_cvref_t<First>;
	if constexpr (c_has_logic_ops_mediator<First>)
		return static_call<define_ops<first_type>::logic_mediator, t_func>(std::forward<First>(first), std::forward<ArgTs>(vals)...);
	else if constexpr (c_has_ops_mediator<std::remove_cvref_t<first_type>>)
		return static_call<define_ops<first_type>::mediator, t_func>(std::forward<First>(first), std::forward<ArgTs>(vals)...);
	else return t_func(std::forward<First>(first), std::forward<ArgTs>(vals)...);
}

template <bool t_is_reversed, auto t_func, typename First, typename Second>
pspp_always_inline constexpr auto execute_bin_logic_op(First && first, Second && second) noexcept
{
  using first_type = std::conditional_t<
    t_is_reversed,
    std::remove_cvref_t<Second>,
    std::remove_cvref_t<First>
  >;

  if constexpr (c_has_logic_ops_mediator<first_type>)
    return static_call<define_ops<first_type>::logic_mediator, t_func>(
      std::forward<First>(first), std::forward<Second>(second));
  else if constexpr (c_has_ops_mediator<first_type>)
    return static_call<define_ops<first_type>::mediator, t_func>(
      std::forward<First>(first), std::forward<Second>(second));
  else return t_func(std::forward<First>(first), std::forward<Second>(second));
}

template <auto t_func, typename First, typename ... ArgTs>
pspp_always_inline constexpr auto execute_comparison_op(First && first, ArgTs && ... vals) noexcept
{
	using first_type = std::remove_cvref_t<First>;
	if constexpr (c_has_comparison_ops_mediator<First>)
    return static_call<define_ops<first_type>::logic_mediator, t_func>(std::forward<First>(first), std::forward<ArgTs>(vals)...);
	else if constexpr (c_has_ops_mediator<std::remove_cvref_t<first_type>>)
		return static_call<define_ops<first_type>::mediator, t_func>(std::forward<First>(first), std::forward<ArgTs>(vals)...);
	else return t_func(std::forward<First>(first), std::forward<ArgTs>(vals)...);
}

template <auto t_is_reversed, auto t_func, typename First, typename Second>
pspp_always_inline constexpr auto execute_bin_comparison_op(First && first, Second && second) noexcept
{
  using first_type = std::conditional_t<
    t_is_reversed,
    std::remove_cvref_t<Second>,
    std::remove_cvref_t<First>
  >;

  if constexpr (c_has_comparison_ops_mediator<first_type>)
    return static_call<define_ops<first_type>::logic_mediator, t_func>(
      std::forward<First>(first), std::forward<Second>(second));
  else if constexpr (c_has_ops_mediator<first_type>)
    return static_call<define_ops<first_type>::mediator, t_func>(
      std::forward<First>(first), std::forward<Second>(second));
  else return t_func(std::forward<First>(first), std::forward<Second>(second));
}

template <auto t_func, typename First, typename ... ArgTs>
pspp_always_inline constexpr auto execute_bit_op(First && first, ArgTs && ... vals) noexcept
{
	using first_type = std::remove_cvref_t<First>;
	if constexpr (c_has_bit_ops_mediator<First>)
		return static_call<define_ops<first_type>::bit_mediator, t_func>(std::forward<First>(first), std::forward<ArgTs>(vals)...);
	else if constexpr (c_has_ops_mediator<first_type>)
		return static_call<define_ops<first_type>::mediator, t_func>(std::forward<First>(first), std::forward<ArgTs>(vals)...);
	else return t_func(std::forward<First>(first), std::forward<ArgTs>(vals)...);
}

template <bool t_is_reversed, auto t_func, typename First, typename Second>
pspp_always_inline constexpr auto execute_bin_bit_op(First && first, Second && second) noexcept
{
  using first_type = std::conditional_t<
    t_is_reversed,
    std::remove_cvref_t<Second>,
    std::remove_cvref_t<First>
  >;

  if constexpr (c_has_bit_ops_mediator<first_type>)
    return static_call<define_ops<first_type>::bit_mediator, t_func>(
      std::forward<First>(first), std::forward<Second>(second));
  else if constexpr (c_has_ops_mediator<first_type>)
    return static_call<define_ops<first_type>::mediator, t_func>(
      std::forward<First>(first), std::forward<Second>(second));
  else return t_func(std::forward<First>(first), std::forward<Second>(second));
}

} // namespace detail


template <auto t_convertor>
struct simple_wrapper_mediator 
{
	static_val convertor = simple_overloaded_lambda{t_convertor, [](auto && val){return pspp_fwd(val);}};

	static_val mediator = []<auto t_func, typename First, typename ... ArgTs>(First && first, ArgTs && ... rest)
	{
		if constexpr (c_bool_return_op<decltype(t_func)>)
			return t_func(convertor(std::forward<First>(first)), convertor(std::forward<ArgTs>(rest))...);
		else return std::remove_cvref_t<First>{
      t_func(convertor(std::forward<First>(first)), convertor(std::forward<ArgTs>(rest))...)
    };
	};
};

// TODO: add positional operation definition

template <c_op_enabled<plus_t> T, c_op_enabled_for<T, plus_t> U>
pspp_always_inline constexpr auto operator+(T && left, U && right) noexcept
{ return detail::execute_bin_arithmetic_op<false, plus>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<minus_t> T, c_op_enabled_for<T, minus_t> U>
pspp_always_inline constexpr auto operator-(T && left, U && right) noexcept
{ return detail::execute_bin_arithmetic_op<false, minus>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<multiplies_t> T, c_op_enabled_for<T, multiplies_t> U>
pspp_always_inline constexpr auto operator*(T && left, U && right) noexcept
{ return detail::execute_bin_arithmetic_op<false, multiplies>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<divides_t> T, c_op_enabled_for<T, divides_t> U>
pspp_always_inline constexpr auto operator/(T && left, U && right) noexcept
{ return detail::execute_bin_arithmetic_op<false, divides>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<modulus_t> T, c_op_enabled_for<T, modulus_t> U>
pspp_always_inline constexpr auto operator%(T && left, U && right) noexcept
{ return detail::execute_bin_arithmetic_op<false, modulus>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<negate_t> T>
pspp_always_inline constexpr auto operator-(T && left) noexcept
{ return detail::execute_arithmetic_op<negate>(std::forward<T>(left)); }


template <c_op_enabled<equal_to_t> T, c_op_enabled_for<T, equal_to_t> U>
pspp_always_inline constexpr auto operator==(T && left, U && right) noexcept
{ return detail::execute_bin_comparison_op<false, equal_to>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<not_equal_to_t> T, c_op_enabled_for<T, not_equal_to_t> U>
pspp_always_inline constexpr auto operator!=(T && left, U && right) noexcept
{ return detail::execute_bin_comparison_op<false, not_equal_to>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<greater_t> T, c_op_enabled_for<T, greater_t> U>
pspp_always_inline constexpr auto operator>(T && left, U && right) noexcept
{ return detail::execute_bin_comparison_op<false, greater>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<greater_equal_t> T, c_op_enabled_for<T, greater_equal_t> U>
pspp_always_inline constexpr auto operator>=(T && left, U && right) noexcept
{ return detail::execute_bin_comparison_op<false, greater_equal>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<less_t> T, c_op_enabled_for<T, less_t> U>
pspp_always_inline constexpr auto operator<(T && left, U && right) noexcept
{ return detail::execute_bin_comparison_op<false, less>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<less_equal_t> T, c_op_enabled_for<T, less_equal_t> U>
pspp_always_inline constexpr auto operator<=(T && left, U && right) noexcept
{ return detail::execute_bin_comparison_op<false, less_equal>(std::forward<T>(left), std::forward<U>(right)); }


template <c_op_enabled<logical_and_t> T, c_op_enabled_for<T, logical_and_t> U>
pspp_always_inline constexpr auto operator&&(T && left, U && right) noexcept
{ return detail::execute_bin_logic_op<false, logical_and>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<logical_or_t> T, c_op_enabled_for<T, logical_or_t> U>
pspp_always_inline constexpr auto operator||(T && left, U && right) noexcept
{ return detail::execute_bin_logic_op<false, logical_or>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<logical_not_t> T>
pspp_always_inline constexpr auto operator!(T && left) noexcept
{ return detail::execute_logic_op<logical_not>(std::forward<T>(left)); }


template <c_op_enabled<bit_and_t> T, c_op_enabled_for<T, bit_and_t> U>
pspp_always_inline constexpr auto operator&(T && left, U && right) noexcept
{ return detail::execute_bin_bit_op<false, bit_and>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<bit_or_t> T, c_op_enabled_for<T, bit_or_t> U>
pspp_always_inline constexpr auto operator|(T && left, U && right) noexcept
{ return detail::execute_bin_bit_op<false, bit_or>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<bit_xor_t> T, c_op_enabled_for<T, bit_xor_t> U>
pspp_always_inline constexpr auto operator^(T && left, U && right) noexcept
{ return detail::execute_bin_bit_op<false, bit_xor>(std::forward<T>(left), std::forward<U>(right)); }

template <c_op_enabled<bit_not_t> T>
pspp_always_inline constexpr auto operator~(T && left) noexcept
{ return detail::execute_bit_op<bit_not>(std::forward<T>(left)); }





template <c_op_enabled<plus_t> T, c_op_enabled_for<T, plus_t> U>
pspp_always_inline constexpr auto operator+(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, plus_t>)
{ return detail::execute_bin_arithmetic_op<true, plus>(std::forward<U>(left), std::forward<T>(right)); }

template <c_op_enabled<minus_t> T, c_op_enabled_for<T, minus_t> U>
pspp_always_inline constexpr auto operator-(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, minus_t>)
{ return detail::execute_bin_arithmetic_op<true, minus>(std::forward<U>(left), std::forward<T>(right)); }

template <c_op_enabled<multiplies_t> T, c_op_enabled_for<T, multiplies_t> U>
pspp_always_inline constexpr auto operator*(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, multiplies_t>)
{ return detail::execute_bin_arithmetic_op<true, multiplies>(std::forward<U>(left), std::forward<T>(right)); }

template <c_op_enabled<divides_t> T, c_op_enabled_for<T, divides_t> U>
pspp_always_inline constexpr auto operator/(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, divides_t>)
{ return detail::execute_bin_arithmetic_op<true, divides>(std::forward<U>(left), std::forward<T>(right)); }

template <c_op_enabled<modulus_t> T, c_op_enabled_for<T, modulus_t> U>
pspp_always_inline constexpr auto operator%(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, modulus_t>)
{ return detail::execute_bin_arithmetic_op<true, modulus>(std::forward<U>(left), std::forward<T>(right)); }


template <c_op_enabled<equal_to_t> T, c_op_enabled_for<T, equal_to_t> U>
pspp_always_inline constexpr auto operator==(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, equal_to_t>)
{ return detail::execute_bin_comparison_op<true, equal_to>(std::forward<U>(left), std::forward<T>(right)); }

template <c_op_enabled<not_equal_to_t> T, c_op_enabled_for<T, not_equal_to_t> U>
pspp_always_inline constexpr auto operator!=(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, not_equal_to_t>)
{ return detail::execute_bin_comparison_op<true, not_equal_to>(std::forward<U>(left), std::forward<T>(right)); }

template <c_op_enabled<greater_t> T, c_op_enabled_for<T, greater_t> U>
pspp_always_inline constexpr auto operator>(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, greater_t>)
{ return detail::execute_bin_comparison_op<true, greater>(std::forward<U>(left), std::forward<T>(right)); }

template <c_op_enabled<greater_equal_t> T, c_op_enabled_for<T, greater_equal_t> U>
pspp_always_inline constexpr auto operator>=(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, greater_equal_t>)
{ return detail::execute_bin_comparison_op<true, greater_equal>(std::forward<U>(left), std::forward<T>(right)); }

template <c_op_enabled<less_t> T, c_op_enabled_for<T, less_t> U>
pspp_always_inline constexpr auto operator<(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, less_t>)
{ return detail::execute_bin_comparison_op<true, less>(std::forward<U>(left), std::forward<T>(right)); }

template <c_op_enabled<less_equal_t> T, c_op_enabled_for<T, less_equal_t> U>
pspp_always_inline constexpr auto operator<=(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, less_equal_t>)
{ return detail::execute_bin_comparison_op<true, less_equal>(std::forward<U>(left), std::forward<T>(right)); }


template <c_op_enabled<logical_and_t> T, c_op_enabled_for<T, logical_and_t> U>
pspp_always_inline constexpr auto operator&&(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, logical_and_t>)
{ return detail::execute_bin_logic_op<true, logical_and>(std::forward<U>(left), std::forward<T>(right)); }

template <c_op_enabled<logical_or_t> T, c_op_enabled_for<T, logical_or_t> U>
pspp_always_inline constexpr auto operator||(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, logical_or_t>)
{ return detail::execute_bin_logic_op<true, logical_or>(std::forward<U>(left), std::forward<T>(right)); }


template <c_op_enabled<bit_and_t> T, c_op_enabled_for<T, bit_and_t> U>
pspp_always_inline constexpr auto operator&(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, bit_and_t>)
{ return detail::execute_bin_bit_op<true, bit_and>(std::forward<U>(left), std::forward<T>(right)); }

template <c_op_enabled<bit_or_t> T, c_op_enabled_for<T, bit_or_t> U>
pspp_always_inline constexpr auto operator|(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, bit_or_t>)
{ return detail::execute_bin_bit_op<true, bit_or>(std::forward<U>(left), std::forward<T>(right)); }

template <c_op_enabled<bit_xor_t> T, c_op_enabled_for<T, bit_xor_t> U>
pspp_always_inline constexpr auto operator^(U && left, T && right) noexcept
  requires (not c_op_enabled_for<T, U, bit_xor_t>)
{ return detail::execute_bin_bit_op<true, bit_xor>(std::forward<U>(left), std::forward<T>(right)); }

} // namespace pspp

#endif // PSPP_DEFOPS_HPP

