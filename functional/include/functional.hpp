#ifndef PSPP_FUNCTIONAL_HPP
#define PSPP_FUNCTIONAL_HPP

#include <pspp/type_sequence.hpp>
#include <pspp/type_name.hpp>


namespace pspp
{

namespace detail
{
struct const_call_operator_ind {};
struct non_const_call_operator_ind {};
} // namespace detail

template <typename ResT, type_sequence t_args, bool t_is_noexcept, typename CallerT = void>
struct function_info
{
  using result_type = ResT;
	using caller_type = std::remove_cvref_t<CallerT>;

  static_val result = type<ResT>;
  static_val argument_types = t_args;
  static_flag is_noexcept = t_is_noexcept;
};

template <typename ResT, type_sequence t_args, bool t_is_noexcept, typename CallerT = void>
struct advanced_function_info
{
  using result_type = ResT;
  using caller_type = std::remove_cvref_t<CallerT>;
  using this_type = CallerT;

  static_val result = type<ResT>;
  static_val caller = type<caller_type>;
  static_val argument_types = t_args;
  static_flag is_noexcept = t_is_noexcept;
  static_flag is_const = std::is_const_v<CallerT>;
  static_flag is_volatile = std::is_volatile_v<CallerT>;
  static_flag is_lref = std::is_lvalue_reference_v<CallerT>;
  static_flag is_rref = std::is_rvalue_reference_v<CallerT>;
  static_flag is_both_ref = std::same_as<CallerT, std::remove_reference_t<CallerT>>;

  template <typename T>
  using with_this_qulifiers = copy_extensions_t<CallerT, T>;

  consteval advanced_function_info() = default;
  consteval advanced_function_info(function_info<ResT, t_args, t_is_noexcept, CallerT>) {};
};



template <typename T>
struct is_function_info : std::false_type {};

template <typename ResT, type_sequence t_args, bool t_is_noexcept, typename CallerT>
struct is_function_info<function_info<ResT, t_args, t_is_noexcept, CallerT>> :
    std::true_type
{};


namespace detail
{

template<typename ... ArgTs, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept>
    get_func_info(ResT (ArgTs...) noexcept(t_is_noexcept)) noexcept
{ return {}; }

template<typename ... ArgTs, typename CallerT, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT>
    get_func_info(ResT (CallerT::*)(ArgTs...) noexcept(t_is_noexcept)) noexcept
{ return {}; }

template<typename ... ArgTs, typename CallerT, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT const>
    get_func_info(ResT (CallerT::*)(ArgTs...) const noexcept (t_is_noexcept)) noexcept
{ return {}; }

template<typename ... ArgTs, typename CallerT, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT volatile>
    get_func_info(ResT (CallerT::*)(ArgTs...) volatile noexcept (t_is_noexcept)) noexcept
{ return {}; }

template<typename ... ArgTs, typename CallerT, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT volatile const>
    get_func_info(ResT (CallerT::*)(ArgTs...) volatile const noexcept (t_is_noexcept)) noexcept
{ return {}; }

template<typename ... ArgTs, typename CallerT, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT &>
    get_func_info(ResT (CallerT::*)(ArgTs...) & noexcept(t_is_noexcept)) noexcept
{ return {}; }

template<typename ... ArgTs, typename CallerT, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT const &>
    get_func_info(ResT (CallerT::*)(ArgTs...) const & noexcept (t_is_noexcept)) noexcept
{ return {}; }

template<typename ... ArgTs, typename CallerT, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT volatile &>
    get_func_info(ResT (CallerT::*)(ArgTs...) volatile & noexcept (t_is_noexcept)) noexcept
{ return {}; }

template<typename ... ArgTs, typename CallerT, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT volatile const &>
    get_func_info(ResT (CallerT::*)(ArgTs...) volatile const & noexcept (t_is_noexcept)) noexcept
{ return {}; }

template<typename ... ArgTs, typename CallerT, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT &&>
    get_func_info(ResT (CallerT::*)(ArgTs...) && noexcept(t_is_noexcept)) noexcept
{ return {}; }

template<typename ... ArgTs, typename CallerT, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT const &&>
    get_func_info(ResT (CallerT::*)(ArgTs...) const && noexcept (t_is_noexcept)) noexcept
{ return {}; }

template<typename ... ArgTs, typename CallerT, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT volatile &&>
    get_func_info(ResT (CallerT::*)(ArgTs...) volatile && noexcept (t_is_noexcept)) noexcept
{ return {}; }

template<typename ... ArgTs, typename CallerT, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT volatile const &&>
    get_func_info(ResT (CallerT::*)(ArgTs...) volatile const && noexcept (t_is_noexcept)) noexcept
{ return {}; }

template<typename MaybeFuncT, template <typename> class HolderT>
consteval auto get_func_info(HolderT<MaybeFuncT> val) noexcept
{
  return decltype(
    get_func_info(std::declval<MaybeFuncT>())
    ){};
}

template<auto t_val, template <auto> class HolderT>
consteval auto get_func_info(HolderT<t_val> val) noexcept
{
  return decltype(
      get_func_info(t_val)
      ){};
}

template<typename FunctorT>
consteval auto get_func_info(FunctorT &&) noexcept
requires (
requires { &std::remove_cvref_t<FunctorT>::operator(); }
)
{ return get_func_info(&std::remove_cvref_t<FunctorT>::operator()); }

template <typename ResT, typename ... ArgTs, typename CallerT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT>
    get_ovfunc_info_by_res(ResT (CallerT::*)(ArgTs...) noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ResT, typename ... ArgTs, typename CallerT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT const>
    get_ovfunc_info_by_res(ResT (CallerT::*)(ArgTs...) const noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ResT, typename ... ArgTs, typename CallerT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT volatile>
    get_ovfunc_info_by_res(ResT (CallerT::*)(ArgTs...) volatile noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ResT, typename ... ArgTs, typename CallerT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT volatile const>
    get_ovfunc_info_by_res(ResT (CallerT::*)(ArgTs...) volatile const noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ResT, typename ... ArgTs, typename CallerT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT &>
    get_ovfunc_info_by_res(ResT (CallerT::*)(ArgTs...) & noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ResT, typename ... ArgTs, typename CallerT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT const &>
    get_ovfunc_info_by_res(ResT (CallerT::*)(ArgTs...) const & noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ResT, typename ... ArgTs, typename CallerT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT volatile &>
    get_ovfunc_info_by_res(ResT (CallerT::*)(ArgTs...) volatile & noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ResT, typename ... ArgTs, typename CallerT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT volatile const &>
    get_ovfunc_info_by_res(ResT (CallerT::*)(ArgTs...) volatile const & noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ResT, typename ... ArgTs, typename CallerT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT &&>
    get_ovfunc_info_by_res(ResT (CallerT::*)(ArgTs...) && noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ResT, typename ... ArgTs, typename CallerT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT const &&>
    get_ovfunc_info_by_res(ResT (CallerT::*)(ArgTs...) const && noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ResT, typename ... ArgTs, typename CallerT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT volatile &&>
    get_ovfunc_info_by_res(ResT (CallerT::*)(ArgTs...) volatile && noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ResT, typename ... ArgTs, typename CallerT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<ArgTs...>, t_is_noexcept, CallerT volatile const &&>
    get_ovfunc_info_by_res(ResT (CallerT::*)(ArgTs...) volatile const && noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller1>()()) (Caller1::*)(),
		decltype(std::declval<Caller1>()()) (Caller2::*)()) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller2>()()) (Caller2::*)(),
		decltype(std::declval<Caller2>()()) (Caller1::*)()) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller1>()()) (Caller1::*)() const,
		decltype(std::declval<Caller1>()()) (Caller2::*)() const) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller2>()()) (Caller2::*)() volatile,
		decltype(std::declval<Caller2>()()) (Caller1::*)() volatile) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller1>()()) (Caller1::*)() volatile,
		decltype(std::declval<Caller1>()()) (Caller2::*)() volatile) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller2>()()) (Caller2::*)() const volatile,
		decltype(std::declval<Caller2>()()) (Caller1::*)() const volatile) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller1>()()) (Caller1::*)() &,
		decltype(std::declval<Caller1>()()) (Caller2::*)() &) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller2>()()) (Caller2::*)() &,
		decltype(std::declval<Caller2>()()) (Caller1::*)() &) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller1>()()) (Caller1::*)() const &,
		decltype(std::declval<Caller1>()()) (Caller2::*)() const &) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller2>()()) (Caller2::*)() volatile &,
		decltype(std::declval<Caller2>()()) (Caller1::*)() volatile &) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller1>()()) (Caller1::*)() volatile &,
		decltype(std::declval<Caller1>()()) (Caller2::*)() volatile &) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller2>()()) (Caller2::*)() const volatile &,
		decltype(std::declval<Caller2>()()) (Caller1::*)() const volatile &) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller1>()()) (Caller1::*)() &&,
		decltype(std::declval<Caller1>()()) (Caller2::*)() &&) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller2>()()) (Caller2::*)() &&,
		decltype(std::declval<Caller2>()()) (Caller1::*)() &&) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller1>()()) (Caller1::*)() const &&,
		decltype(std::declval<Caller1>()()) (Caller2::*)() const &&) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller2>()()) (Caller2::*)() volatile &&,
		decltype(std::declval<Caller2>()()) (Caller1::*)() volatile &&) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller1>()()) (Caller1::*)() volatile &&,
		decltype(std::declval<Caller1>()()) (Caller2::*)() volatile &&) noexcept {}

template <typename Caller1, typename Caller2>
consteval void have_same_over(decltype(std::declval<Caller2>()()) (Caller2::*)() const volatile &&,
		decltype(std::declval<Caller2>()()) (Caller1::*)() const volatile &&) noexcept {}

template <typename Caller1, typename Caller2, typename ResT, typename ... Ts>
consteval void have_same_over(ResT (Caller1::*)(Ts...),
		ResT (Caller2::*)(Ts...) ) noexcept {}

template <typename Caller1, typename Caller2, typename ResT, typename ... Ts>
consteval void have_same_over(ResT (Caller1::*)(Ts...) const,
		ResT (Caller2::*)(Ts...) const) noexcept {}

template <typename Caller1, typename Caller2, typename ResT, typename ... Ts>
consteval void have_same_over(ResT (Caller1::*)(Ts...) volatile,
		ResT (Caller2::*)(Ts...) volatile) noexcept {}

template <typename Caller1, typename Caller2, typename ResT, typename ... Ts>
consteval void have_same_over(ResT (Caller1::*)(Ts...) const volatile,
		ResT (Caller2::*)(Ts...) const volatile) noexcept {}

template <typename Caller1, typename Caller2, typename ResT, typename ... Ts>
consteval void have_same_over(ResT (Caller1::*)(Ts...) const &,
		ResT (Caller2::*)(Ts...) const &) noexcept {}

template <typename Caller1, typename Caller2, typename ResT, typename ... Ts>
consteval void have_same_over(ResT (Caller1::*)(Ts...) volatile &,
		ResT (Caller2::*)(Ts...) volatile &) noexcept {}

template <typename Caller1, typename Caller2, typename ResT, typename ... Ts>
consteval void have_same_over(ResT (Caller1::*)(Ts...) const volatile &,
		ResT (Caller2::*)(Ts...) const volatile &) noexcept {}

template <typename Caller1, typename Caller2, typename ResT, typename ... Ts>
consteval void have_same_over(ResT (Caller1::*)(Ts...) const &&,
		ResT (Caller2::*)(Ts...) const &&) noexcept {}

template <typename Caller1, typename Caller2, typename ResT, typename ... Ts>
consteval void have_same_over(ResT (Caller1::*)(Ts...) volatile &&,
		ResT (Caller2::*)(Ts...) volatile &&) noexcept {}

template <typename Caller1, typename Caller2, typename ResT, typename ... Ts>
consteval void have_same_over(ResT (Caller1::*)(Ts...) const volatile &&,
		ResT (Caller2::*)(Ts...) const volatile &&) noexcept {}




template <typename ... Ts>
struct ovfunc_wrapper
{
  template <typename ResT>
  using type = ResT (Ts...);
};

template <c_type_sequence auto t_ts>
consteval auto get_ovfunc_type_wrapper() noexcept
{ return t_ts | pass_to<ovfunc_wrapper> | unwrap; }

} // namespace detail

consteval auto get_func_info(auto && functor) noexcept
	requires (requires {detail::get_func_info(functor);})
{ return detail::get_func_info(functor); }

template <c_type_sequence auto t_ts, typename ResT>
consteval auto get_ovfunc_info_by_args(typename
    decltype(detail::get_ovfunc_type_wrapper<t_ts>())::template type<ResT> func) noexcept
{
  return detail::get_func_info(decltype(func){});
}

template <typename ... Ts, typename ResT, bool t_is_noexcept>
consteval function_info<ResT, typeseq<Ts...>, t_is_noexcept>
    get_ovfunc_info_by_args(ResT (Ts...) noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ... Ts, c_has_call_operator FunctorT>
consteval auto get_ovfunc_info_by_args(FunctorT &&) noexcept 
	requires (requires {detail::get_func_info<Ts...>(&std::remove_cvref_t<FunctorT>::operator());})
{
  return detail::get_func_info<Ts...>(&std::remove_cvref_t<FunctorT>::operator());
}

template <type_sequence t_ts, c_has_call_operator FunctorT>
consteval auto get_ovfunc_info_by_args(FunctorT &&) noexcept
	requires (requires {
	t_ts | pass_and_execute<[]<typename ... Us>
  {
    return decltype(get_ovfunc_info_by_args<Us...>(std::declval<FunctorT>())){};
  }
  >;
	})
{
  return decltype(t_ts){} | pass_and_execute<[]<typename ... Us>
  {
    return decltype(get_ovfunc_info_by_args<Us...>(std::declval<FunctorT>())){};
  }
  >;
}

template <typename ... Ts, auto t_val, template <auto> class HolderT>
consteval auto get_ovfunc_info_by_args(HolderT<t_val>) noexcept
	requires (requires {decltype(get_ovfunc_info_by_args<Ts...>(t_val)){};})
{ return decltype(get_ovfunc_info_by_args<Ts...>(t_val)){};}

template <typename ... Ts, typename FuncT, template <typename> class HolderT>
consteval auto get_ovfunc_info_by_args(HolderT<FuncT>) noexcept
	requires (requires { decltype(get_ovfunc_info_by_args<Ts...>(std::declval<FuncT>())){};})
{ return decltype(get_ovfunc_info_by_args<Ts...>(std::declval<FuncT>())){};}

template <type_sequence t_ts, auto t_val, template <auto> class HolderT>
consteval auto get_ovfunc_info_by_args(HolderT<t_val>) noexcept
	requires ( requires { decltype(get_ovfunc_info_by_args<t_ts>(t_val)){};} )
{ return decltype(get_ovfunc_info_by_args<t_ts>(t_val)){};}

template <type_sequence t_ts, typename FuncT, template <typename> class HolderT>
consteval auto get_ovfunc_info_by_args(HolderT<FuncT>) noexcept
	requires ( requires {decltype(get_ovfunc_info_by_args<t_ts>(std::declval<FuncT>())){};})
{ return decltype(get_ovfunc_info_by_args<t_ts>(std::declval<FuncT>())){};}

template <typename ResT, typename ... Ts, bool t_is_noexcept>
consteval function_info<ResT, typeseq<Ts...>, t_is_noexcept>
    get_ovfunc_info_by_res(ResT (Ts...) noexcept(t_is_noexcept)) noexcept
{ return {}; }

template <typename ResT, c_has_call_operator FunctorT>
consteval auto get_ovfunc_info_by_res(FunctorT) noexcept
	requires (requires {detail::get_ovfunc_info_by_res<ResT>(&std::remove_cvref_t<FunctorT>::operator());})
{
  return detail::get_ovfunc_info_by_res<ResT>(&std::remove_cvref_t<FunctorT>::operator());
}

template <typename ResT, c_has_call_operator FunctorT, template <typename> class HolderT>
consteval auto get_ovfunc_info_by_res(HolderT<FunctorT>)
	requires (requires { decltype(get_ovfunc_info_by_res<ResT>(std::declval<FunctorT>())){}; })
{
  return decltype(get_ovfunc_info_by_res<ResT>(std::declval<FunctorT>())){};
}

template <typename ResT, c_has_call_operator auto t_functor, template <auto> class HolderT>
consteval auto get_ovfunc_info_by_res(HolderT<t_functor>)
	requires (requires {decltype(get_ovfunc_info_by_res<ResT>(t_functor)){};})
{
  return decltype(get_ovfunc_info_by_res<ResT>(t_functor)){};
}

template <typename FuncT>
inline constexpr auto func_result_v = decltype(get_func_info(std::declval<FuncT>()))::result_type;

template <typename FuncT>
using func_result_t = extract_type<func_result_v<FuncT>>;

template <typename FuncT>
inline constexpr auto func_arguments_v =
    decltype(get_func_info(std::declval<FuncT>()))::argument_types;

template <typename FuncT>
using func_arguments_t = extract_type<func_arguments_v<FuncT>>;

template <auto t_func>
inline constexpr auto vfunc_result_v = get_func_info(t_func).result_type;

template <auto t_func>
using vfunc_result_t = extract_type<vfunc_result_v<t_func>>;

template <auto t_func>
inline constexpr auto vfunc_arguments_v = get_func_info(t_func).arguments_type;

template <auto t_func>
using vfunc_arguments_t = extract_type<vfunc_arguments_v<t_func>>;

template <typename FuncT>
inline constexpr bool is_noexcept = decltype(get_func_info(std::declval<FuncT>()))::is_noexcept;

template <typename FuncT>
inline constexpr bool is_const = decltype(get_func_info(std::declval<FuncT>()))::is_const;

template <auto t_func>
inline constexpr bool vis_noexcept = decltype(get_func_info(t_func))::is_noexcept;

template <typename ... Ts>
consteval auto get_advanced_func_info(auto && ... args) noexcept
{
  return advanced_function_info{get_func_info<Ts...>(pspp_fwd(args)...)};
}

template <typename ... Ts>
consteval auto get_advanced_ovfunc_info_by_args(auto && ... args) noexcept
{
  return advanced_function_info{get_ovfunc_info_by_args<Ts...>(pspp_fwd(args)...)};
}

template <typename ... Ts>
consteval auto get_advanced_ovfunc_info_by_res(auto && ... args) noexcept
{
  return advanced_function_info{get_ovfunc_info_by_res<Ts...>(pspp_fwd(args)...)};
}

template <typename T, typename ... Ts>
concept c_has_overloaded_operator_with_args = requires (T t){
	get_ovfunc_info_by_args<Ts...>(t);
};

template <typename T, typename ResT>
concept c_has_overloaded_operator_with_res = requires (T t){
	get_ovfunc_info_by_res<ResT>(t);
};

template <typename T, typename U>
concept c_has_same_olverload = requires {
	detail::have_same_over<T, U>(&T::operator(), &U::operator());
} or requires {
	detail::have_same_over<U, T>(&U::operator(), &T::operator());
};


// =================================
// overloaded lambda

namespace detail 
{

template <typename T>
struct has_call_operator { static_flag value = c_has_call_operator<T>; };

template <typename ... Ts>
struct overloaded_lambda_impl: Ts...
{
	static_val tsv = typeseq<Ts...>;
  using Ts::operator()...;
};

template <>
struct overloaded_lambda_impl<> 
{
	static_val tsv = emptytseq;
};

template <typename T, typename U>
concept c_overloadable = requires {overloaded_lambda_impl<T, U>{};};

template <typename ... Ts>
struct safe_overloaded_lambda_impl : Ts...
{
	static_val tsv = typeseq<Ts...>;
	using Ts::operator()...;
};

template <>
struct safe_overloaded_lambda_impl<>
{
	static_val tsv = emptytseq;
};

template <typename ... Holders>
struct overloaded_lambda_creator
{
  static_val types = typeseq<Holders...> | remove_cvref
                                         | tfilter<detail::has_call_operator>
                                         | make_unique;

  static_val value = types | pass_to<overloaded_lambda_impl> | unwrap;
  using value_type = std::remove_cvref_t<decltype(value)>;
};

template <typename ... Holders>
struct safe_overloaded_lambda_creator
{
  static_val types = typeseq<Holders...> | remove_cvref | tfilter<detail::has_call_operator> | 
		make_custom_unique<[]<typename T, typename U>{ return c_has_same_olverload<T, U>; }>;
  static_val value = types | pass_to<safe_overloaded_lambda_impl> | unwrap;
};


template <typename ... ts, template <auto ...> class holdert, auto ... t_vals>
consteval auto operator+(overloaded_lambda_impl<ts...>, holdert<t_vals...>) noexcept
{
  static_assert(std::same_as<decltype(vtypeseq<t_vals...>), void>);
  return overloaded_lambda_creator<ts..., decltype(t_vals)...>::value; }

template <typename ... ts, template <typename...> class holdert, typename ... us>
consteval auto operator+(overloaded_lambda_impl<ts...>, holdert<us...>) noexcept
{ return overloaded_lambda_creator<ts..., us...>::value; }

template <typename ... ts>
consteval auto operator+(overloaded_lambda_impl<ts...>, auto val) noexcept
  requires (not c_holder<std::remove_cvref_t<decltype(val)>>)
{
    //static_assert(std::same_as<decltype(val), void>);
    static_assert(c_has_call_operator<clean_type<val>>);
    return overloaded_lambda_creator<ts..., decltype(val)>::value; }

template <typename ... ts, template <auto ...> class holdert, auto ... t_vals>
consteval auto operator+(safe_overloaded_lambda_impl<ts...>, holdert<t_vals...>) noexcept
{ return safe_overloaded_lambda_creator<ts..., decltype(t_vals)...>::value; }

template <typename ... ts, template <typename...> class holdert, typename ... us>
consteval auto operator+(safe_overloaded_lambda_impl<ts...>, holdert<us...>) noexcept
{ return safe_overloaded_lambda_creator<ts..., us...>::value; }

template <typename ... ts>
consteval auto operator+(safe_overloaded_lambda_impl<ts...>, auto val) noexcept
  requires (not c_holder<std::remove_cvref_t<decltype(val)>>)
{ return safe_overloaded_lambda_creator<ts..., decltype(val)>::value; }

template <typename ... ts, template <typename...> class holdert, typename ... us>
consteval auto operator-(overloaded_lambda_impl<ts...>, holdert<us...>) noexcept
{
	//static_assert(std::same_as<decltype(typeseq<ts...>), void>);
	//static_assert(std::same_as<decltype(typeseq<us...>), void>);
	//static_assert(std::same_as<decltype(typeseq<ts...> | remove<us...>), void>);

 	return 
	(typeseq<ts...> | remove_ts<clean_type_sequence<us...>> 
								 | pass_to<overloaded_lambda_creator> 
								 | unwrap).value;
}

template <typename ... ts, template <auto ...> class holdert, auto ... t_vals>
consteval auto operator-(overloaded_lambda_impl<ts...> left, holdert<t_vals...>) noexcept
{ return left - (typeseq<decltype(t_vals)...> | remove_cvref); }

template <typename ... ts>
consteval auto operator-(overloaded_lambda_impl<ts...> left, auto val) noexcept
  requires (not c_holder<std::remove_cvref_t<decltype(val)>>)
{ return left - typeseq<decltype(val)>; }


template <typename ... ts, template <typename...> class holdert, typename ... us>
consteval auto operator-(safe_overloaded_lambda_impl<ts...>, holdert<us...>) noexcept
{ 
	return 
	(typeseq<ts...> | remove_ts<clean_type_sequence<us...>>
                  | pass_to<safe_overloaded_lambda_creator>
                  | unwrap).value;
}

template <typename ... ts, template <auto ...> class holdert, auto ... t_vals>
consteval auto operator-(safe_overloaded_lambda_impl<ts...> left, holdert<t_vals...>) noexcept
{ return left - vtypeseq<t_vals...>; }

template <typename ... ts>
consteval auto operator-(safe_overloaded_lambda_impl<ts...> left, auto val) noexcept
  requires (not c_holder<std::remove_cvref_t<decltype(val)>>)
{ return left - typeseq<decltype(val)>; }

} // namespace detail

#ifdef PSPP_OVERLAM_BY_VALS
template <auto ... t_vals>
inline constexpr auto overloaded_lambda =
    detail::overloaded_lambda_creator<decltype(t_vals)...>::value | unwrap;
#else

namespace detail
{
inline constexpr struct overloaded_lambda_ctor_args_filter_t {
  template <c_indexed_type T>
  constexpr auto pspp_static_call_const_operator() noexcept
  {
    return c_has_call_operator<typename T::type>;
  }
} overloaded_lambda_ctor_args_filter {};

inline constexpr struct overloaded_lambda_ctor_args_passer_t {

  template <typename ... Ts>
  constexpr auto pspp_static_call_const_operator(Ts && ... args) noexcept
  {
    static constexpr std::integer_sequence is_ =
    clean_typeseq<Ts...>
      | index
      | filter<detail::overloaded_lambda_ctor_args_filter>
      | convert_to_vals_and_pass_to<extract_index_from_type, std::index_sequence>
      | unwrap;

    using res_t = overloaded_lambda_creator<Ts...>::value_type;
    return
      []<std::size_t ... t_is>(std::index_sequence<t_is...>, Ts && ... args)
      {
        return res_t {
          get_ell_at<t_is>(std::forward<Ts>(args)...)...
        };
      }(is_, std::forward<Ts>(args)...);
  }
} overloaded_lambda_ctor_args_passer {};
} // namespace detail


template <typename ... LambdaTs>
struct overloaded_lambda :
  detail::overloaded_lambda_creator<LambdaTs...>::value_type
{
  using base = typename detail::overloaded_lambda_creator<LambdaTs...>::value_type;

  constexpr overloaded_lambda() noexcept
    requires requires {
      (LambdaTs{}, ...);
    } = default;

  template <typename ... LambdaUs>
  constexpr overloaded_lambda(LambdaUs && ... lambdas)
    requires (sizeof...(LambdaUs) > 0) and
      c_same_as<detail::overloaded_lambda_impl<LambdaTs...>, base> :
    base{std::forward<LambdaUs>(lambdas)...}
  {}

  template <typename ... LambdaUs>
  constexpr overloaded_lambda(LambdaUs && ... lambdas)
    requires (sizeof...(LambdaUs) > 0) and
    (not c_same_as<detail::overloaded_lambda_impl<LambdaTs...>, base>) :

    base {detail::overloaded_lambda_ctor_args_passer(std::forward<LambdaUs>(lambdas)...)}
  {}
};

template <typename ... LambdaTs>
overloaded_lambda(LambdaTs && ...) ->
  overloaded_lambda<std::remove_cvref_t<LambdaTs>...>;

#endif




template <auto ... t_vals>
using overloaded_lambda_t = extract_type<detail::overloaded_lambda_creator<decltype(t_vals)...>::value>;

template <typename ... Holders>
using overloaded_lambda_from_types_t = extract_type<detail::overloaded_lambda_creator<Holders...>::value>;

template <auto ... t_vals>
inline constexpr auto safe_overloaded_lambda =
    detail::safe_overloaded_lambda_creator<decltype(t_vals)...>::value | unwrap;

template <auto ... t_vals>
using safe_overloaded_lambda_t = extract_type<detail::safe_overloaded_lambda_creator<decltype(t_vals)...>::value>;

template <typename ... Holders>
using safe_overloaded_lambda_from_types_t = extract_type<detail::safe_overloaded_lambda_creator<Holders...>::value>;



} // namespace pspp


#endif // PSPP_FUNCTIONAL_HPP
