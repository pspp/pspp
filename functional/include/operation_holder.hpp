#ifndef PSPP_OPERATION_HOLDER_HPP
#define PSPP_OPERATION_HOLDER_HPP

#include <typeinfo>

#include <pspp/ops.hpp>
#include <pspp/type_helpers.hpp>
#include <pspp/utility/universal.hpp>
#include <pspp/defops.hpp>


namespace pspp
{

namespace
{
enum struct operation_pos
{
  left, right, unary /* <- ? */
};

}

namespace
{
constexpr struct _return_as_is_fnt
{

  template <typename ValueT>
  pspp_always_inline
#ifdef __cpp_static_call_operator
  constexpr static auto && operator()(auto &&, ValueT && value) noexcept (noexcept (
    std::forward<ValueT>(value)
    ))
#else
  constexpr auto && operator()(auto &&, ValueT && value) const noexcept (noexcept (
    std::forward<ValueT>(value)
    ))
#endif
  { return std::forward<ValueT>(value); }

} _return_as_is;

}

template <
  auto t_left = _return_as_is,
  auto t_right = _return_as_is,
  auto t_result = _return_as_is
>
struct operation_pipe_preprocessors
{
  static_val left = t_left;
  static_val right = t_right;
  static_val result = t_result;
};

template <
  auto t_old = _return_as_is,
  auto t_new = _return_as_is,
  auto t_result = _return_as_is
>
struct relative_operation_pipe_preprocessors
{
  static_val oldp = t_old;
  static_val newp = t_new;
  static_val resultp = t_result;
};

template <typename T>
concept c_relative_operation_pipe_preprocessors = requires
{
  T::oldp;
  T::newp;
  T::resultp;
};

template <
  typename DataT,
  operation_pos t_pos,
  auto t_operation,
  auto t_preprocessor = operation_pipe_preprocessors<>{}
>
struct operation_holder
{
  typename decltype(
  []
  {
    if constexpr (pspp_is_same(DataT, void))
      return type<nullt>;
    else
    {
      if constexpr (std::is_rvalue_reference_v<DataT &&>)
        return type<std::remove_const_t<std::remove_reference_t<DataT>>>;
      else
        return type<DataT>;
    }
  }()
  )::t value;

  static_flag can_be_const = c_const_lref<DataT>;
  static_flag is_unary = t_pos == operation_pos::unary;

  static_val preprocessor = t_preprocessor;
  static_val pos = t_pos;

  static_val raw_operation = t_operation;

  static_val operation = []
  {
    if constexpr (c_relative_operation_pipe_preprocessors<decltype(t_preprocessor)>)
    {
      if constexpr (t_pos == operation_pos::right)
        return [] pspp_always_inline (auto && self, auto && left, auto && right) noexcept (noexcept (
          preprocessor.result(
            pspp_fwd(self),
            t_operation(
              preprocessor.newp(pspp_fwd(self), pspp_fwd(left)),
              preprocessor.oldp(pspp_fwd(self), pspp_fwd(right))
              )
            )
          ))
        {
          return preprocessor.result(
            pspp_fwd(self),
            t_operation(
              preprocessor.newp(pspp_fwd(self), pspp_fwd(left)),
              preprocessor.oldp(pspp_fwd(self), pspp_fwd(right))
            )
          );
        };
      else return [] pspp_always_inline (auto && self, auto && left, auto && right) noexcept (noexcept (
        t_preprocessor.result(
          pspp_fwd(self),
          t_operation(
            preprocessor.oldp(pspp_fwd(self), pspp_fwd(left)),
            preprocessor.newp(pspp_fwd(self), pspp_fwd(right))
          )
        )
      ))
      {
        return t_preprocessor.result(
          pspp_fwd(self),
          t_operation(
            preprocessor.oldp(pspp_fwd(self), pspp_fwd(left)),
            preprocessor.newp(pspp_fwd(self), pspp_fwd(right))
          )
        );
      };
    }
    else return
      [] pspp_always_inline (auto && self, auto && left, auto && right) noexcept (noexcept (
        preprocessor.result(
          pspp_fwd(self),
          t_operation(
            preprocessor.left(pspp_fwd(self), pspp_fwd(left)),
            preprocessor.right(pspp_fwd(self), pspp_fwd(right))
          )
        )
      ))
      {
        return preprocessor.result(
          pspp_fwd(self),
          t_operation(
            preprocessor.left(pspp_fwd(self), pspp_fwd(left)),
            preprocessor.right(pspp_fwd(self), pspp_fwd(right))
          )
        );
      };
  }();


  static_val unary_operation = [] pspp_always_inline (auto && self, auto && v) noexcept (noexcept (
      preprocessor.result(
        pspp_fwd(self),
        t_operator(preprocessor.left(pspp_fwd(self), pspp_fwd(v))))
    ))
  {
    return
      preprocessor.result(
        pspp_fwd(self),
        t_operator(preprocessor.left(pspp_fwd(self), pspp_fwd(v))));
  };

  using value_type = std::remove_cvref_t<DataT>;
  using passed_value_type = DataT;

  pspp_always_inline
  constexpr DataT get_value() noexcept (noexcept (std::forward<DataT>(value)))
  requires (not can_be_const)
  {
    return std::forward<DataT>(value);
  }

  pspp_always_inline
  constexpr auto const & get_value() const noexcept
  { return value; }

  pspp_always_inline
  constexpr auto execute(auto && self, auto && other_value) noexcept (noexcept (
    operation(pspp_fwd(self), get_value(), pspp_fwd(other_value))
  )) -> decltype(auto)
  requires (not can_be_const) and (t_pos == operation_pos::right)
  { return operation(pspp_fwd(self), get_value(), pspp_fwd(other_value)); }

  pspp_always_inline
  constexpr auto execute(auto && self, auto && other_value) noexcept (noexcept (
    operation(pspp_fwd(self), pspp_fwd(other_value), get_value())
  ))
  requires (not can_be_const) and (t_pos == operation_pos::left)
  { return operation(pspp_fwd(self), pspp_fwd(other_value), get_value()); }

  pspp_always_inline
  constexpr auto execute(auto && self, auto && other_value) const noexcept (noexcept (
    operation(pspp_fwd(self), get_value(), pspp_fwd(other_value))
  ))
  requires (t_pos == operation_pos::right)
  { return operation(pspp_fwd(self), get_value(), pspp_fwd(other_value)); }

  pspp_always_inline
  constexpr auto execute(auto && self, auto && other_value) const noexcept (noexcept (
    operation(pspp_fwd(self), pspp_fwd(other_value), get_value())
  ))
  requires (t_pos == operation_pos::left)
  { return operation(pspp_fwd(self), pspp_fwd(other_value), get_value()); }

  pspp_always_inline
  constexpr auto execute(auto && self) noexcept (noexcept (
    operation(pspp_fwd(self), get_value())
  ))
  requires (not can_be_const) and (t_pos == operation_pos::unary)
  { return operation(pspp_fwd(self), get_value()); }

  pspp_always_inline
  constexpr auto execute(auto && self) const noexcept (noexcept (
    operation(pspp_fwd(self), get_value())
  ))
  requires (t_pos == operation_pos::unary)
  { return operation(pspp_fwd(self), get_value()); }
};

template <
  typename DataT,
  operation_pos t_pos,
  auto t_operation,
  auto t_preprocessor = operation_pipe_preprocessors<>{}
>
struct operation_pipe : operation_holder<DataT, t_pos, t_operation, t_preprocessor>
{
  using base = operation_holder<DataT, t_pos, t_operation, t_preprocessor>;
};

template <typename T, typename OtherT>
concept c_operation_pipe = requires (T t, OtherT other) {
  t.execute(t, other);
};

template <typename OtherValue, c_operation_pipe<OtherValue> Pipe>
pspp_always_inline
constexpr auto operator|(
  Pipe && oppipe, OtherValue && other_value
) noexcept (noexcept ( oppipe.execute(oppipe, pspp_fwd(other_value)) ))
{ return oppipe.execute(oppipe, pspp_fwd(other_value)); }

template <typename OtherValue, c_operation_pipe<OtherValue> Pipe>
pspp_always_inline
constexpr auto operator|(
  OtherValue && other_value, Pipe && oppipe
) noexcept (noexcept ( oppipe.execute(oppipe, pspp_fwd(other_value)) ))
{ return oppipe.execute(oppipe, pspp_fwd(other_value)); }


namespace
{
constexpr struct _default_pipe_creator_fnt {
  template <typename OperationHolder>
  constexpr auto operator()(auto &&, auto && value) const noexcept (noexcept (pspp_fwd(value)))
  {
    return OperationHolder{pspp_fwd(value)};
  }
} _default_pipe_creator {};
}


template <
  template <typename, operation_pos, auto, auto> class OperationHolderT,
  auto t_preproc = operation_pipe_preprocessors<>{},
  auto t_pipe_creator = _default_pipe_creator
  >
struct operation_pipe_creator
{
  template <
    typename DataT, operation_pos t_pos, auto t_op
  >
  using operation_pipe_type = OperationHolderT<DataT, t_pos, t_op, t_preproc>;

  static_val pipe_creator = t_pipe_creator;
  static_val preprocessor = t_preproc;

  template <typename DataT, operation_pos t_pos, auto t_op>
  static constexpr auto get_pipe(auto && self, auto && value) noexcept
  {
    return t_pipe_creator.template
      operator()<operation_pipe_type<DataT, t_pos, t_op>>(pspp_fwd(self), pspp_fwd(value));
  }

  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator+(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator-(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator*(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator/(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator%(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator&(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator|(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator&&(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator||(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator>(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator<(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator>=(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator<=(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator==(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
  template <template <typename, operation_pos, auto, auto> class OtherOperationHolderT, auto t_opreproc>
  constexpr auto operator!=(operation_pipe_creator<OtherOperationHolderT, t_opreproc>) const = delete;
};

using default_operation_pipe_creator = operation_pipe_creator<operation_pipe>;

template <typename T,
  typename DefDataT = int,
  operation_pos t_def_pos = operation_pos::right,
  auto t_def_op = plus
>
concept c_operation_pipe_creator = requires
{
  std::remove_cvref_t<T>::template get_pipe<
    DefDataT, t_def_pos, t_def_op
  >(std::declval<T>(), std::declval<DefDataT>());
};

template <c_operation_pipe_creator PipeCreator>
struct define_ops<PipeCreator> :
  enable_all_bin_ops_except_pipe,
  ops_mediator<
  []<auto t_op>(auto && left, auto && right) noexcept (
    noexcept(pspp_fwd(left)) and
    noexcept(pspp_fwd(right))
  )
  {
    using left_type = pspp_declean(left);
    using right_type = pspp_declean(right);

    if constexpr (c_operation_pipe_creator<left_type>)
    {
      return left_type::template get_pipe<
        decltype(right), operation_pos::left, t_op
      >(pspp_fwd(left), pspp_fwd(right));
    } else
    {
      return right_type::template get_pipe<
        decltype(left), operation_pos::right, t_op
      >(pspp_fwd(right), pspp_fwd(left));
    }
  }
  >
{};

inline constexpr default_operation_pipe_creator eval {};
inline constexpr default_operation_pipe_creator res_of_left {};
inline constexpr default_operation_pipe_creator _is {};

} // namespace pspp


#endif // PSPP_OPERATION_HOLDER_HPP