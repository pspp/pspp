#ifndef PSPP_OPS_HPP
#define PSPP_OPS_HPP

#include <pspp/simple/meta.hpp>
#include <pspp/simple/temporary_storage.hpp>
#include <pspp/utility/declaration_macros.hpp>

namespace pspp
{

namespace
{

struct true_type_
{ static_flag value = true; };

struct false_type_
{ static_flag value = false; };


template <typename OptT, typename RightT>
struct bin_op_pipe
{
  simple::tmpstorage<RightT &&> right;

  template <typename LeftT, simple::same_as<bin_op_pipe> SelfT>
  friend
  constexpr auto operator|(LeftT && left, SelfT && self) noexcept
  {
    return OptT{}(simple::forward<LeftT>(left),
                  simple::forward<SelfT>(self).right.template get<0>());
  }
};

template <typename OptT>
struct bin_op_base
{
  template <typename RightT>
  constexpr auto pspp_static_call_const_operator(RightT && right) noexcept ->
    bin_op_pipe<OptT, RightT>
  { return {std::forward<RightT>(right)}; }
};

struct un_op_base
{
  template <typename LeftT, simple::derived_from<un_op_base> SelfT>
  friend
  constexpr auto operator|(LeftT && left, SelfT && self) noexcept
  {
    return simple::forward<SelfT>(self)(std::forward<LeftT>(left));
  }
};

}

inline constexpr struct plus_t : bin_op_base<plus_t> {
  using bin_op_base<plus_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) + simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) + simple::forward<RightT>(right); }
} plus {};

inline constexpr struct minus_t : bin_op_base<minus_t> {
  using bin_op_base<minus_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) - simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) - simple::forward<RightT>(right); }
} minus {};

inline constexpr struct multiplies_t : bin_op_base<multiplies_t> {
  using bin_op_base<multiplies_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) * simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) * simple::forward<RightT>(right); }
} multiplies {};

inline constexpr struct divides_t : bin_op_base<divides_t> {
  using bin_op_base<divides_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) / simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) / simple::forward<RightT>(right); }
} divides {};

inline constexpr struct modulus_t : bin_op_base<modulus_t> {
  using bin_op_base<modulus_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) % simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) % simple::forward<RightT>(right); }
} modulus {};

inline constexpr struct negate_t : un_op_base {
  template <typename LeftT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left)
    noexcept (noexcept (-left))
  { return -simple::forward<LeftT>(left); }
} negate {};


inline constexpr struct equal_to_t : bin_op_base<equal_to_t> {
  using bin_op_base<equal_to_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) == simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) == simple::forward<RightT>(right); }
} equal_to {};

inline constexpr struct not_equal_to_t : bin_op_base<not_equal_to_t> {
  using bin_op_base<not_equal_to_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) != simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) != simple::forward<RightT>(right); }
} not_equal_to {};

inline constexpr struct greater_t : bin_op_base<greater_t> {
  using bin_op_base<greater_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) > simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) > simple::forward<RightT>(right); }
} greater {};

inline constexpr struct less_t : bin_op_base<less_t> {
  using bin_op_base<less_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) < simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) < simple::forward<RightT>(right); }
} less {};

inline constexpr struct greater_equal_t : bin_op_base<greater_equal_t> {
  using bin_op_base<greater_equal_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) >= simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) >= simple::forward<RightT>(right); }
} greater_equal {};

inline constexpr struct less_equal_t : bin_op_base<less_equal_t> {
  using bin_op_base<less_equal_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr bool pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) <= simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) <= simple::forward<RightT>(right); }
} less_equal {};


inline constexpr struct logical_and_t : bin_op_base<logical_and_t> {
  using bin_op_base<logical_and_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) && simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) && simple::forward<RightT>(right); }
} logical_and {};

inline constexpr struct logical_or_t : bin_op_base<logical_or_t> {
  using bin_op_base<logical_or_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) || simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) || simple::forward<RightT>(right); }
} logical_or {};

inline constexpr struct logical_not_t : un_op_base {
  template <typename LeftT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left)
    noexcept (noexcept (!simple::forward<LeftT>(left)))
  { return !simple::forward<LeftT>(left); }
} logical_not {};


inline constexpr struct bit_and_t : bin_op_base<bit_and_t> {
  using bin_op_base<bit_and_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) & simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) & simple::forward<RightT>(right); }
} bit_and {};

inline constexpr struct bit_or_t : bin_op_base<bit_or_t> {
  using bin_op_base<bit_or_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) | simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) | simple::forward<RightT>(right); }
} bit_or {};

using pipe_operation_t = bit_or_t;
inline constexpr pipe_operation_t pipe_operation {};

inline constexpr struct bit_xor_t : bin_op_base<bit_xor_t> {
  using bin_op_base<bit_xor_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) ^ simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) ^ simple::forward<RightT>(right); }
} bit_xor {};

inline constexpr struct bit_not_t : un_op_base {
  template <typename LeftT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left)
    noexcept (noexcept (~simple::forward<LeftT>(left)))
  { return ~simple::forward<LeftT>(left); }
} bit_not {};


inline constexpr struct bit_shift_left_t : bin_op_base<bit_shift_left_t> {
  using bin_op_base<bit_shift_left_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) << simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) << simple::forward<RightT>(right); }
} bit_shift_left {};

inline constexpr struct bit_shift_right_t : bin_op_base<bit_shift_right_t> {
  using bin_op_base<bit_shift_right_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left) >> simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left) >> simple::forward<RightT>(right); }
} bit_shift_right {};

inline constexpr struct ptr_member_access_t : bin_op_base<ptr_member_access_t> {
  using bin_op_base<ptr_member_access_t>::operator();

  template <typename LeftT, typename RightT>
    pspp_always_inline
  constexpr auto pspp_static_call_const_operator(LeftT && left, RightT && right)
    noexcept (noexcept (simple::forward<LeftT>(left)->*simple::forward<RightT>(right)))
  { return simple::forward<LeftT>(left)->*simple::forward<RightT>(right); }
} ptr_member_access {};


template <typename T>
struct is_arithmetic_op : false_type_ {};
template <typename T>
struct is_logical_op : false_type_ {};
template <typename T>
struct is_comparison_op : false_type_ {};
template <typename T>
struct is_bit_op : false_type_ {};

template <typename T>
struct is_bin_op : false_type_ {};

template <> struct is_arithmetic_op<plus_t>       : true_type_ {};
template <> struct is_arithmetic_op<minus_t>      : true_type_ {};
template <> struct is_arithmetic_op<multiplies_t> : true_type_ {};
template <> struct is_arithmetic_op<divides_t>    : true_type_ {};
template <> struct is_arithmetic_op<modulus_t>    : true_type_ {};
template <> struct is_arithmetic_op<negate_t>     : true_type_ {};

template <> struct is_comparison_op<equal_to_t>      : true_type_ {};
template <> struct is_comparison_op<not_equal_to_t>  : true_type_ {};
template <> struct is_comparison_op<greater_t>       : true_type_ {};
template <> struct is_comparison_op<less_t>          : true_type_ {};
template <> struct is_comparison_op<greater_equal_t> : true_type_ {};
template <> struct is_comparison_op<less_equal_t>    : true_type_ {};

template <> struct is_logical_op<logical_and_t> : true_type_ {};
template <> struct is_logical_op<logical_or_t>  : true_type_ {};
template <> struct is_logical_op<logical_not_t> : true_type_ {};

template <> struct is_bit_op<bit_and_t> : true_type_ {};
template <> struct is_bit_op<bit_or_t>  : true_type_ {};
template <> struct is_bit_op<bit_xor_t> : true_type_ {};
template <> struct is_bit_op<bit_not_t> : true_type_ {};

template <> struct is_bit_op<bit_shift_left_t>  : true_type_ {};
template <> struct is_bit_op<bit_shift_right_t> : true_type_ {};



template <> struct is_bin_op<plus_t>       : true_type_ {};
template <> struct is_bin_op<minus_t>      : true_type_ {};
template <> struct is_bin_op<multiplies_t> : true_type_ {};
template <> struct is_bin_op<divides_t>    : true_type_ {};
template <> struct is_bin_op<modulus_t>    : true_type_ {};

template <> struct is_bin_op<equal_to_t>      : true_type_ {};
template <> struct is_bin_op<not_equal_to_t>  : true_type_ {};
template <> struct is_bin_op<greater_t>       : true_type_ {};
template <> struct is_bin_op<less_t>          : true_type_ {};
template <> struct is_bin_op<greater_equal_t> : true_type_ {};
template <> struct is_bin_op<less_equal_t>    : true_type_ {};

template <> struct is_bin_op<logical_and_t> : true_type_ {};
template <> struct is_bin_op<logical_or_t>  : true_type_ {};

template <> struct is_bin_op<bit_and_t> : true_type_ {};
template <> struct is_bin_op<bit_or_t>  : true_type_ {};
template <> struct is_bin_op<bit_xor_t> : true_type_ {};

template <> struct is_bin_op<bit_shift_left_t>  : true_type_ {};
template <> struct is_bin_op<bit_shift_right_t> : true_type_ {};



template <typename T>
concept c_arithmetic_op = is_arithmetic_op<T>::value;

template <typename T>
concept c_comparison_op = is_comparison_op<T>::value;

template <typename T>
concept c_logical_op = is_logical_op<T>::value;

template <typename T>
concept c_bit_op = is_bit_op<T>::value;

template <typename T>
concept c_bin_op = is_bin_op<T>::value;

template <typename T>
concept c_arithmetic_bin_op = c_arithmetic_op<T> and c_bin_op<T>;

template <typename T>
concept c_comparison_bin_op = c_comparison_op<T> and c_bin_op<T>;

template <typename T>
concept c_logical_bin_op = c_logical_op<T> and c_bin_op<T>;

template <typename T>
concept c_bit_bin_op = c_bit_op<T> and c_bin_op<T>;



template <typename T>
struct is_bool_return_op
{
  static_flag value = c_comparison_op<T> or c_logical_op<T>;
};

template <typename T>
concept c_bool_return_op = is_bool_return_op<T>::value;

inline constexpr struct vsum_t {
  template <typename FirstT, typename ... ArgTs>
  pspp_always_inline
  constexpr auto pspp_static_call_const_operator(FirstT && first, ArgTs && ... args)
    noexcept (noexcept ((simple::forward<FirstT>(first) + ... + simple::forward<ArgTs>(args))))
  { return (simple::forward<FirstT>(first) + ... + simple::forward<ArgTs>(args)); }
} vsum {};

} // namespace pspp

#endif // PSPP_OPS_HPP
