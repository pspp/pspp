#ifndef PSPP_SIMPLE_OVERLOADED_LAMBDA_HPP
#define PSPP_SIMPLE_OVERLOADED_LAMBDA_HPP

#include <pspp/simple/meta.hpp>

namespace pspp
{
template <typename ... LambdaTs>
struct simple_overloaded_lambda : LambdaTs...
{
	using LambdaTs::operator()...;

	constexpr simple_overloaded_lambda(auto && ... lambdas):
		LambdaTs{simple::forward<decltype(lambdas)>(lambdas)}...
	{}
};

template <typename ... LambdaTs>
simple_overloaded_lambda(LambdaTs && ... lambdas) ->
	simple_overloaded_lambda<simple::remove_cvref_t<LambdaTs>...>;

} // namespace pspp

#endif // PSPP_SIMPLE_OVERLOADED_LAMBDA_HPP
