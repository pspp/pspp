# Functional

Function related stuff.

## functional.hpp

`struct function_info`

`function_info::result` -> `type_t<__functio_return_type__>`
`function_info::argument_types` -> `type_sequece<__function_argument_types__>`
`function_info::is_noexcept` -> `bool`; tells if function is noexcept

static_val result = type<ResT>;
  static_val caller = type<caller_type>;
  static_val argument_types = t_args;
  static_flag is_noexcept = t_is_noexcept;
  static_flag is_const = std::is_const_v<CallerT>;
  static_flag is_volatile = std::is_volatile_v<CallerT>;
  static_flag is_lref = std::is_lvalue_reference_v<CallerT>;
  static_flag is_rref = std::is_rvalue_reference_v<CallerT>;
  static_flag is_both_ref = std::same_as<CallerT, std::remove_reference_t<CallerT>>;


`struct advanced_function_info` 
    same as function_info but tells also if function is `const` or `volatile`, and if can it be used with &, && or both objects;
    `advanced_function_info` can be created from `function_info` or from `get_advance_func_info(func)` directly.


* `get_func_info`
* `get_ovfunc_info_by_res`
* `get_ovfunc_info_by_args`

`get_func_ifo(func)` takes func ptr/functor object(lambda/struct with call operator) and returns `function_info`.

Also excepts any type/value wrapper (`type_t<FuncT>`, `value_holder<t_func>`) in the case when `FuncT` cannot be default constructed.


`get_ovfunc_info_by_res` -> requires function return type as template argument. Can be used if only one function overloads returns specified type.

`get_ovfunc_info_by_args` -> requires function arg types as template argument. Can be passed as `type_sequence` or just type sequence (`<int, char, ...>`). Requires that only one function overload accepts that argument types. (Do not work when the only difference is const/volatile specifier).


`c_has_overloaded_operator_with_args` and `c_has_overloaded_operator_with_res` concepts are provided to check if requirements above is satisfied.


### `overloaded_lambda`

create an object which have overloaded call operator by inheriting from passed values/types.


## ops.hpp

Has basic operators lambdas.

## defops.hpp

Allow to easily overload basic operators 

Example 

```cpp
#include <pspp/defops.hpp>
#include <iostream>

struct S
{ int val; };

std::ostream & operator<<(std::ostream& o, S s) 
{ return o << s.val; }

template <>
struct pspp::define_ops<S> :
    pspp::enable_all_ops<>,
    pspp::simple_wrapper_mediator<[](S s){return s.val;}>
{};

int main()
{
  std::cout << S{5} + S{2} << '\n'; // outputs 7
}
```

### TODO: add additional info about defops.hpp