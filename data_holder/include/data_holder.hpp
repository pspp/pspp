/****************************************
 * data holder header for `view` struct *
 * with edit rights                     *
 ****************************************/

#ifndef PSPP_DATA_HOLDER_HPP
#define PSPP_DATA_HOLDER_HPP

#include <pspp/iterator.hpp>
#include <pspp/utility/utility.hpp>

namespace pspp
{

namespace detail
{
}

template <typename DataT, typename ... IteratorOptions>
struct data_holder
{
	static_flag is_view = iterator_options::options_set<IteratorOptions...>::is_view;
	using stored_value_type = 
		std::conditional_t<
			is_view,
			DataT const,
			DataT 
				>;

	using stored_pointer_type = stored_value_type *;

	stored_pointer_type m_data;
	std::size_t m_size;

	using value_type = DataT;
	using iterator_type = iterator<value_type, IteratorOptions...>;
	using const_value_type = std::remove_const_t<DataT> const;
  using reference_type = DataT &;
	using pointer = DataT *;

	using const_iterator_type = iterator<const_value_type, 
				IteratorOptions...>;
	using reversed_iterator_type = iterator<value_type, 
				iterator_options::reverse, IteratorOptions...>;
	using const_reversed_iterator_type = iterator<const_value_type, 
				iterator_options::reverse, IteratorOptions...>;

	inline constexpr iterator_type begin() noexcept
	{
		return {m_data};
	}

	inline constexpr reversed_iterator_type rbegin() noexcept
	{
		return {m_data + m_size - 1};
	}

	inline constexpr const_reversed_iterator_type rbegin() const noexcept
	{
		return {m_data + m_size - 1};
	}

	inline constexpr const_iterator_type begin() const noexcept
	{
		return {m_data};
	}

	inline constexpr auto & front() noexcept
	{ return *begin(); }

	inline constexpr auto & front() const noexcept
	{ return *begin(); }

	inline constexpr auto & back() noexcept
	{ return *--end(); }

	inline constexpr auto & back() const noexcept
	{ return *--end(); }

	inline constexpr iterator_type end() noexcept
	{
		return {m_data + m_size};
	}

	inline constexpr reversed_iterator_type rend() noexcept
	{
		return {m_data - 1};
	}

	inline constexpr const_reversed_iterator_type rend() const noexcept
	{
		return {m_data - 1};
	}

	inline constexpr const_iterator_type end() const noexcept
	{
		return {m_data + m_size};
	}

	pspp_always_inline constexpr void expand_head() noexcept
	{
		m_data--;
		m_size++;
	}

	pspp_always_inline constexpr void expand_tail() noexcept
	{
		m_size++;
	}

	pspp_always_inline constexpr void expand_head(std::unsigned_integral auto offset) noexcept
	{
		m_data -= offset;
		m_size += offset;
	}

	pspp_always_inline constexpr void expand_tail(std::unsigned_integral auto offset) noexcept
	{
		m_size += offset;
	}

	template <auto t_offset>
	pspp_always_inline constexpr void expand_tail() noexcept
	{
		m_size += t_offset;
	}

	template <auto t_offset>
	pspp_always_inline constexpr void expand_head() noexcept
	{
		m_data -= t_offset;
		m_size += t_offset;
	}

	template <auto t_offset>
		requires (not is_npos_v<t_offset>)
	pspp_always_inline constexpr void shrink_head() noexcept
	{
		m_data += t_offset;
		
		if (t_offset > m_size)
			m_size = 0;
		else
			m_size -= t_offset;
	}

	template <auto t_offset>
		requires (not is_npos_v<t_offset>)
	pspp_always_inline constexpr void shrink_tail() noexcept
	{
		if (t_offset > m_size)
			m_size = 0;
		else
			m_size -= t_offset;
	}

	template <auto t_offset>
		requires (is_npos_v<t_offset>)
	pspp_always_inline constexpr void shrink_head() noexcept
	{
		m_data += m_size;
		m_size = 0;
	}

	template <auto t_offset>
		requires (is_npos_v<t_offset>)
	pspp_always_inline constexpr void shrink_tail() noexcept
	{ m_size = 0; }

	pspp_always_inline constexpr void shrink_head(auto offset) noexcept
	{
		m_data += offset;
		if (offset > m_size)
			m_size = 0;
		else
			m_size -= offset;
	}

	pspp_always_inline constexpr void shrink_tail(auto offset) noexcept
	{
		if (offset > m_size)
			m_size = 0;
		else
			m_size -= offset;
	}

	pspp_always_inline constexpr void shrink_head() noexcept
	{
		++m_data;
		if (m_size != 0)
			--m_size;
	}

  pspp_always_inline constexpr reference_type pop_head() noexcept
  {
    shrink_head();
    return *(m_data - 1);
  }

  pspp_always_inline constexpr reference_type pop_tail() noexcept
  {
    shrink_tail();
    return *(m_data + m_size);
  }

	pspp_always_inline constexpr void shrink_tail() noexcept
	{
		if (m_size != 0)
			--m_size;
	}

	pspp_always_inline constexpr stored_pointer_type data() noexcept
	{ return m_data; }

	pspp_always_inline constexpr stored_pointer_type data() const noexcept
	{ return m_data; }

	pspp_always_inline constexpr bool empty() const noexcept
	{ return m_size == 0; }

	pspp_always_inline constexpr std::size_t size() const noexcept
	{ return m_size; }

  pspp_always_inline constexpr void shift() noexcept
  { ++m_data; }

  pspp_always_inline constexpr void shift(auto offset) noexcept
  { m_data += offset; }

  template <auto t_offset>
  pspp_always_inline constexpr void shift() noexcept
  { m_data += t_offset; }

  pspp_always_inline constexpr void clear() noexcept
  { m_size = 0; m_data = nullptr; }

private:
  template <bool t_forward = false>
  constexpr std::size_t move_until(auto && value) noexcept
  {
    if (empty()) return npos;

    using target_type = std::remove_reference_t<decltype(value)>;


    auto * it = [*this] pspp_always_inline {
      if constexpr (t_forward) return m_data;
      else return m_data + m_size - 1;
    }();
    auto * const current = it;

    auto * const last = [*this] pspp_always_inline {
      if constexpr (t_forward) return m_data + m_size - 1;
      else return m_data;
    }();

    constexpr auto move = [] pspp_always_inline (auto * & it_) {
      if constexpr (t_forward) ++it_;
      else --it_;
    };

    if constexpr (c_same_as<target_type, value_type>)
    {
      while (*it != value )
      {
        if (it == last) goto not_found;
        move(it);
      }
    }
    else
    {
      while (not value(it))
      {
        if (it == last) goto not_found;
        move(it);
      }
    }


    {
      m_data = it;

      const auto diff = [current, *this] pspp_always_inline {
        if constexpr (t_forward) return m_data - current;
        else return current - m_data;
      }();

      m_size -= diff;

      return static_cast<std::size_t>(diff);
    }


    not_found:
    {
      m_size = 0;
      m_data = last;
      return npos;
    }

  }
public:

  pspp_always_inline
  constexpr std::size_t move_head_until(auto && value) noexcept
  { return move_until<true>(pspp_fwd(value)); }

  pspp_always_inline
  constexpr std::size_t move_tail_until(auto && value) noexcept
  { return move_until<false>(pspp_fwd(value)); }

	inline constexpr data_holder():
			m_data{nullptr},
			m_size{0}
	{}

	template <typename Iterator>
		requires (
			requires {
				requires std::same_as<
					std::remove_cv_t<decltype(*std::declval(Iterator()))>,
					DataT&
				>;

				typename Iterator::difference_type;
				{std::declval(Iterator()) -
					std::declval(Iterator())} -> std::same_as<typename Iterator::difference_type>;
		})
	inline constexpr data_holder(Iterator first, Iterator second):
		m_data{std::addressof(*first)},
		m_size{static_cast<std::size_t>(second - first)}
	{}

	inline constexpr data_holder(pointer first, pointer last) requires (is_view):
		m_data{first},
		m_size{static_cast<std::size_t>(last - first)}
	{}

	inline constexpr data_holder(stored_pointer_type first, stored_pointer_type last):
		m_data{first},
		m_size{static_cast<std::size_t>(last - first)}
	{}

	inline constexpr data_holder(pointer first, std::integral auto size) requires (is_view):
		m_data{first},
		m_size{static_cast<std::size_t>(size)}
	{}

	inline constexpr data_holder(stored_pointer_type first, std::integral auto size):
		m_data{first},
		m_size{static_cast<std::size_t>(size)}
	{}

	template <std::ranges::range RangeT>
	inline constexpr data_holder(RangeT && range):
		m_data{&*std::begin(std::forward<RangeT>(range))},
		m_size{std::size(std::forward<RangeT>(range))}
	{}

	template <std::size_t t_n>
	inline constexpr data_holder(value_type (&s)[t_n]):
		m_data{s},
		m_size{t_n}
	{}

	inline constexpr data_holder(std::initializer_list<stored_value_type> init_list):
		m_data{init_list.begin()},
		m_size{init_list.size()}
	{}

	template <std::ranges::range RangeT>
	inline constexpr data_holder(RangeT && range, std::size_t size):
		m_data{&*std::begin(std::forward<RangeT>(range))},
		m_size{size}
	{}

	inline constexpr data_holder(data_holder &&) = default;
	inline constexpr data_holder& operator=(data_holder&&) = default;
	inline constexpr data_holder(data_holder const &) = default;
	inline constexpr data_holder & operator=(data_holder const &) = default;

	template <typename ... OtherOptions>
	inline constexpr data_holder(data_holder<DataT, OtherOptions...> const & other):
		m_data{other.m_data},
		m_size{other.m_size}
	{}

	template <typename ... OtherOptions>
	inline constexpr data_holder(data_holder<DataT, OtherOptions...> && other):
		m_data{other.m_data},
		m_size{other.m_size}
	{
		other.m_data = nullptr;
		other.m_size = 0;
	}

	inline constexpr auto view() const noexcept 
	{
		return data_holder<const_value_type, iterator_options::view, IteratorOptions...>
			{m_data, m_size};
	}

	inline constexpr auto & operator[](auto pos) noexcept
	{
		return *(m_data + static_cast<std::int64_t>(pos));
	}

	inline constexpr auto const & operator[](auto pos) const noexcept
	{
		return *(m_data + static_cast<std::int64_t>(pos));
	}

  template <typename NewViewT>
  inline constexpr auto as() const noexcept
  { return NewViewT{m_data, m_size}; }
};

template <std::ranges::range RangeT>
data_holder(RangeT && range) -> data_holder<
	std::remove_reference_t<decltype(*std::begin(range))>
>;

template <typename DataT>
data_holder(DataT *, std::size_t) -> data_holder<DataT>;

template <typename DataT, std::size_t t_n>
data_holder(DataT (&)[t_n]) -> data_holder<DataT>;

template <std::ranges::range RangeT>
data_holder(RangeT && range, std::size_t) -> data_holder<
	std::remove_reference_t<decltype(*std::begin(range))>
>;

}

#endif // PSPP_DATA_HOLDER_HPP
