#ifndef PSPP_COMPILE_TIME_MESSAGES_HPP
#define PSPP_COMPILE_TIME_MESSAGES_HPP

#include <pspp/utility/declaration_macros.hpp>

namespace pspp
{

template <auto ... t_messages>
struct cwarning
{
	[[deprecated("WARNING")]] constexpr cwarning(){}
};

template <bool t_condition, auto ... t_messages>
struct cwarning_if
{
	static_flag condition = true;
	[[deprecated("WARNING")]] constexpr cwarning_if(){};
};

template <auto ... messages>
struct cwarning_if<false, messages...>
{
	static_flag condition = false;
	constexpr cwarning_if(){};
};

template <auto ... messages>
struct cerror
{
	static_assert(
			sizeof...(messages) < 0,
			"ERROR: see messages in template arguments");
};

template <bool t_condition, auto ... t_messages>
struct cerror_if
{
	static_assert(
			not t_condition,
			"ERROR: see messages in template arguments");
};

template <bool t_check_condition,
					bool t_condition_to_check,
					auto ... t_messages>
struct conditional_error_if
{};

template <bool t_condition_to_check,
					auto ... t_messages>
struct conditional_error_if<true, t_condition_to_check, t_messages...>
{
	static_assert(t_condition_to_check,
			"ERROR: see messages in template arguments");
};

}

#endif // PSPP_COMPILE_TIME_MESSAGES_HPP
