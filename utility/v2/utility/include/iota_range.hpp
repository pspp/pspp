#ifndef PSPP_UTILITY_IOTA_RANGE_HPP
#define PSPP_UTILITY_IOTA_RANGE_HPP

#include <cstdint>
#include <utility>

#include <pspp/meta.hpp>
#include <pspp/iterator.hpp>
#include <pspp/data_holder.hpp>

namespace pspp
{

template <
					typename IndexT,
					auto t_step,
					typename DataT,
					typename ... DataIterOpts
				>
struct iota_range_iterator;

namespace iota_options
{

namespace detail 
{ 

template <typename T1, typename T2>
struct bigger_type
{
	using type = extract_type<
		[](){
			if constexpr ((sizeof(T1) > sizeof(T2)))
			return type<T1>;
			else return type<T2>;
		}()>;

};

template <typename T1, typename T2>
using bigger_type_t = typename bigger_type<T1, T2>::type;


struct runtime_value_t
{
	inline constexpr bool operator==(auto) const noexcept
	{ return false; }

	inline constexpr bool operator==(runtime_value_t) const noexcept
	{ return true; }
}; 
}


inline constexpr detail::runtime_value_t runtime_value{}; 

template <typename T>
struct index_type : std::type_identity<T>
{};

namespace detail
{

template <typename ... Options>
static_flag is_view = typeseq<Options...> | contains_some_of<ranges_options::view, iterator_options::view>;

template <
					typename IndexT,
					typename DataT,
					typename ... DataIterOpts
				>
struct iota_range_iterator_base
{
	using data_iterator_type = iterator<DataT, DataIterOpts...>;
	using difference_type = std::ptrdiff_t;
	using value_type = IndexT;
	using reference_type = std::conditional_t<
		is_view<DataIterOpts...>, IndexT const &, IndexT &>;
	using const_reference_type = IndexT const &;

	IndexT m_index_value {};
	data_iterator_type m_data_iter {};

	constexpr iota_range_iterator_base & operator=(iota_range_iterator_base const &) = default;
	constexpr iota_range_iterator_base & operator=(iota_range_iterator_base &&) = default;

	constexpr iota_range_iterator_base() = default;
	constexpr iota_range_iterator_base(iota_range_iterator_base const &) = default;
	constexpr iota_range_iterator_base(iota_range_iterator_base &&) = default;

	constexpr iota_range_iterator_base(IndexT index, data_iterator_type data):
		m_index_value{index}, m_data_iter{data} {}

	[[nodiscard]] pspp_always_inline constexpr auto operator*() noexcept
	{
		return std::pair{static_cast<reference_type>(m_index_value), *m_data_iter};
	}

	[[nodiscard]] pspp_always_inline constexpr auto operator*() const noexcept
	{
		return std::pair{static_cast<const_reference_type>(m_index_value), *m_data_iter};
	}
};

template <
					typename IndexT,
					typename ... DataIterOpts
				>
struct iota_range_iterator_base<IndexT, void, DataIterOpts...>
{
	using difference_type = std::ptrdiff_t;
	using value_type = IndexT;
	using reference_type = std::conditional_t<
		is_view<DataIterOpts...>, IndexT const &, IndexT &>;
	using const_reference_type = IndexT const &;

	IndexT m_index_value {};

	constexpr iota_range_iterator_base & operator=(iota_range_iterator_base const &) = default;
	constexpr iota_range_iterator_base & operator=(iota_range_iterator_base &&) = default;

	constexpr iota_range_iterator_base() = default;
	constexpr iota_range_iterator_base(iota_range_iterator_base const &) = default;
	constexpr iota_range_iterator_base(iota_range_iterator_base &&) = default;

	constexpr iota_range_iterator_base(IndexT index):
		m_index_value{index} {}


	[[nodiscard, pspp_attr_always_inline]] inline constexpr const_reference_type operator*() const noexcept
	{
		return m_index_value;
	}

	[[nodiscard, pspp_attr_always_inline]] inline constexpr reference_type operator*() noexcept
	{
		return m_index_value;
	}
};



} // namespace detail
}

template <auto t_offset = iota_options::runtime_value>
struct absolute_index_offset : pspp::value_holder<t_offset>
{
};

template <>
struct absolute_index_offset<iota_options::runtime_value>
{
	long int m_offset;
};

template <
					typename IndexT,
					auto t_step,
					typename DataT,
					typename ... DataIterOpts
				>
struct iota_range_iterator : 
	iota_options::detail::iota_range_iterator_base<IndexT, DataT, DataIterOpts...>
{
	using m_data_iterator_type = iterator<DataT, DataIterOpts...>;
	using base = iota_options::detail::iota_range_iterator_base<DataT, DataIterOpts...>;

	using this_type = iota_range_iterator<IndexT, t_step, DataT, DataIterOpts...>;
	using reference_to_this_type = this_type &;
	using const_reference_to_this_type = this_type const &;

	using base::m_index_value;
	using base::m_data_iter;

	inline constexpr this_type operator++(int) noexcept
	{
		return *this;
		this_type old = *this;
		++(*this);
		return old;
	}

	inline constexpr reference_to_this_type operator++() noexcept
	{
		m_index_value += t_step;
		m_data_iter += t_step;
		return *this;
	}

	inline constexpr this_type operator--(int) noexcept
	{
		this_type old = *this;
		--(*this);
		return old;
	}

	inline constexpr reference_to_this_type operator--() noexcept
	{
		m_index_value -= t_step;
		m_data_iter -= t_step;

		return *this;
	}

	inline constexpr this_type operator+(auto && offset) const noexcept
	{
		auto tmp = offset * t_step;
		return {
			m_index_value + tmp,
			m_data_iter + tmp 
		};
	}

	inline constexpr reference_to_this_type operator+=(auto && offset) noexcept
	{
		auto tmp = offset * t_step;
		m_index_value += tmp;
		m_data_iter += tmp;
		return *this;
	}

	inline constexpr reference_to_this_type operator-=(auto && offset) noexcept
	{
		auto tmp = offset * t_step;
		m_index_value -= tmp;
		m_data_iter -= tmp;
		return *this;
	}

	inline constexpr this_type operator-(auto && offset) const noexcept
	{
		auto tmp = offset * t_step;
		return {
			m_index_value - tmp,
			m_data_iter - tmp 
		};
	}

	constexpr iota_range_iterator() = default;
	/*constexpr iota_range_iterator(iota_range_iterator const &) = default;
	constexpr iota_range_iterator(iota_range_iterator&&) = default;
	*/
};

template <
					typename IndexT,
					typename DataT,
					typename ... DataIterOpts
				>
struct iota_range_iterator<IndexT, iota_options::runtime_value, DataT,DataIterOpts...> : 
	iota_options::detail::iota_range_iterator_base<IndexT, DataT, DataIterOpts...>
{
	using data_iterator_type = iterator<DataT, DataIterOpts...>;
	using base = iota_options::detail::iota_range_iterator_base<IndexT, DataT, DataIterOpts...>;

	using this_type = iota_range_iterator<IndexT, iota_options::runtime_value, DataT, DataIterOpts...>;
	using reference_to_this_type = this_type &;
	using const_reference_to_this_type = this_type const &;

	using base::m_index_value;
	using base::m_data_iter;

	IndexT m_step;

	inline constexpr this_type operator++(int) noexcept
	{
		this_type old = *this;
		++(*this);
		return old;
	}

	inline constexpr reference_to_this_type operator++() noexcept
	{
		m_index_value += m_step;
		m_data_iter += m_step;

		return *this;

	}

	inline constexpr this_type operator--(int) noexcept
	{
		this_type old = *this;
		--(*this);
		return old;
	}

	inline constexpr reference_to_this_type operator--() noexcept
	{
		m_index_value -= m_step;
		m_data_iter -= m_step;

		return *this;
	}

	inline constexpr this_type operator+(auto && offset) const noexcept
	{
		auto tmp = offset * m_step;
		return {
			m_index_value + tmp,
			m_data_iter + tmp 
		};
	}

	inline constexpr reference_to_this_type operator+=(auto && offset) noexcept
	{
		auto tmp = offset * m_step;
		m_index_value += tmp;
		m_data_iter += tmp;
	}

	inline constexpr reference_to_this_type operator-=(auto && offset) noexcept
	{
		auto tmp = offset * m_step;
		m_index_value -= tmp;
		m_data_iter -= tmp;
	}

	inline constexpr this_type operator-(auto && offset) const noexcept
	{
		auto tmp = offset * m_step;
		return {
			m_index_value - tmp,
			m_data_iter - tmp 
		};
	}

	/*constexpr iota_range_iterator(iota_range_iterator const &) = default;
	constexpr iota_range_iterator(iota_range_iterator&&) = default;
	*/

	constexpr iota_range_iterator() = default;
	constexpr iota_range_iterator(IndexT index_value, IndexT step_value, DataT * data_pointer):
		iota_options::detail::iota_range_iterator_base<IndexT, DataT, DataIterOpts...>{index_value, data_pointer},
		m_step{step_value}
	{}
};


/*template <
					auto t_step,
					typename DataT,
					typename ... DataIterOpts
				>
struct iota_range_iterator<t_step, DataT,DataIterOpts...> : 
	iota_options::detail::iota_range_iterator_base<DataT, DataIterOpts...>
{
	using m_data_iterator_type = iterator<DataT, DataIterOpts...>;
	using base = iota_options::detail::iota_range_iterator_base<DataT, DataIterOpts...>;

	using this_type = iota_range_iterator<iota_options::runtime_value, DataT, DataIterOpts...>;
	using reference_to_this_type = this_type &;
	using const_reference_to_this_type = this_type const &;

	using base::m_index_value;
	using base::m_data_iter;

	inline constexpr this_type operator++(int) noexcept
	{
		m_index_value += t_step;
		m_data_iter += t_step;

		return *this;
	}

	inline constexpr reference_to_this_type operator++() noexcept
	{
		auto old = auto{*this};
		(*this)++;
		return old;
	}

	inline constexpr this_type operator--(int) noexcept
	{
		m_index_value -= t_step;
		m_data_iter -= t_step;

		return *this;
	}

	inline constexpr reference_to_this_type operator--() noexcept
	{
		auto old = auto{*this};
		(*this)--;
		return old;
	}

	inline constexpr this_type operator+(auto && offset) const noexcept
	{
		auto tmp = offset * t_step;
		return {
			m_index_value + tmp,
			m_data_iter + tmp 
		};
	}

	inline constexpr reference_to_this_type operator+=(auto && offset) noexcept
	{
		auto tmp = offset * t_step;
		m_index_value += tmp;
		m_data_iter += tmp;
	}

	inline constexpr reference_to_this_type operator-=(auto && offset) noexcept
	{
		auto tmp = offset * t_step;
		m_index_value -= tmp;
		m_data_iter -= tmp;
	}

	inline constexpr this_type operator-(auto && offset) const noexcept
	{
		auto tmp = offset * t_step;
		return {
			m_index_value - tmp,
			m_data_iter - tmp 
		};
	}

	iota_range_iterator(long int index_value, DataT * data_pointer):
		iota_options::detail::iota_range_iterator_base<DataT, DataIterOpts...>{index_value, data_pointer}
	{}
};
*/


template <
					typename IndexT,
					auto t_step,
					typename ... DataIterOpts
				>
struct iota_range_iterator<IndexT, t_step, void, DataIterOpts...> : 
	iota_options::detail::iota_range_iterator_base<IndexT, void, DataIterOpts...>
{
	using base = iota_options::detail::iota_range_iterator_base<IndexT, void, DataIterOpts...>;

	using this_type = iota_range_iterator<IndexT, t_step, void, DataIterOpts...>;
	using reference_to_this_type = this_type &;
	using const_reference_to_this_type = this_type const &;

	using base::m_index_value;

	inline constexpr this_type operator++(int) noexcept
	{
		this_type old = *this;
		(*this)++;
		return old;

	}

	inline constexpr reference_to_this_type operator++() noexcept
	{
		m_index_value += t_step;

		return *this;
	}

	inline constexpr this_type operator--(int) noexcept
	{
		this_type old = *this;
		(*this)--;
		return old;
	}

	inline constexpr reference_to_this_type operator--() noexcept
	{
		m_index_value -= t_step;
		return *this;
	}

	inline constexpr this_type operator+(auto && offset) const noexcept
	{
		return {
			m_index_value + offset * t_step
		};
	}

	inline constexpr reference_to_this_type operator+=(auto && offset) noexcept
	{
		m_index_value += offset * t_step;
	}

	inline constexpr reference_to_this_type operator-=(auto && offset) noexcept
	{
		m_index_value -= offset * t_step;
	}

	inline constexpr this_type operator-(auto && offset) const noexcept
	{
		return {
			m_index_value - offset * t_step,
		};
	}

/*	constexpr iota_range_iterator(iota_range_iterator const &) = default;
	constexpr iota_range_iterator(iota_range_iterator&&) = default;
	*/


	constexpr iota_range_iterator() = default;
	constexpr iota_range_iterator(IndexT index_value):
		iota_options::detail::iota_range_iterator_base<IndexT, void, DataIterOpts...>{index_value}
	{}
};


template <
					typename IndexT,
					typename ... DataIterOpts
				>
struct iota_range_iterator<IndexT, iota_options::runtime_value, void, DataIterOpts...> : 
	iota_options::detail::iota_range_iterator_base<IndexT, void, DataIterOpts...>
{
	using base = iota_options::detail::iota_range_iterator_base<IndexT, void, DataIterOpts...>;

	using this_type = iota_range_iterator<IndexT, iota_options::runtime_value, void, DataIterOpts...>;
	using reference_to_this_type = this_type &;
	using const_reference_to_this_type = this_type const &;

	using base::m_index_value;

	IndexT m_step;

	inline constexpr this_type operator++(int) noexcept
	{
		this_type old = *this;
		m_index_value += m_step;
		return old;
	}

	inline constexpr reference_to_this_type operator++() noexcept
	{
		m_index_value += m_step;
		return *this;
	}

	inline constexpr this_type operator--(int) noexcept
	{
		this_type old = *this;
		m_index_value -= m_step;
		return old;
	}

	inline constexpr reference_to_this_type operator--() noexcept
	{
		m_index_value -= m_step;
		return *this;
	}

	inline constexpr this_type operator+(auto && offset) const noexcept
	{
		return {
			m_index_value + offset * m_step
		};
	}

	inline constexpr reference_to_this_type operator+=(auto && offset) noexcept
	{
		m_index_value += offset * m_step;
	}

	inline constexpr reference_to_this_type operator-=(auto && offset) noexcept
	{
		m_index_value -= offset * m_step;
	}

	inline constexpr this_type operator-(auto && offset) const noexcept
	{
		return {
			m_index_value - offset * m_step,
		};
	}

/*	constexpr iota_range_iterator(iota_range_iterator const &) = default;
	constexpr iota_range_iterator(iota_range_iterator&&) = default;
	*/


	constexpr iota_range_iterator() = default;
	constexpr iota_range_iterator(IndexT index_value, IndexT step):
		iota_options::detail::iota_range_iterator_base<IndexT, void, DataIterOpts...>{index_value},
		m_step{step}
	{}

};


template <typename IndexT, auto t_step, typename DataT, typename ... IterOpts, 
					typename OtherIndexT, auto t_other_step, typename OtherDataT, typename ... OtherIterOpts>
	[[nodiscard]] inline constexpr bool operator==(
		iota_range_iterator<IndexT, t_step, DataT, IterOpts...> const & first,
		iota_range_iterator<OtherIndexT, t_other_step, OtherDataT, OtherIterOpts...> second) noexcept
	requires (not std::same_as<DataT, void> and not std::same_as<OtherDataT, void>)
{
	using bigger = iota_options::detail::bigger_type_t<IndexT, OtherIndexT>;

	return first.m_data_iter == second.m_data_iter and 
		static_cast<bigger>(first.m_index_value) ==
		static_cast<bigger>(second.m_index_value);
}

template <typename IndexT, auto t_step, typename DataT, typename ... IterOpts, 
					typename OtherIndexT, auto t_other_step, typename OtherDataT, typename ... OtherIterOpts>
	[[nodiscard]] inline constexpr bool operator!=(
		iota_range_iterator<IndexT, t_step, DataT, IterOpts...> const & first,
		iota_range_iterator<OtherIndexT, t_other_step, OtherDataT, OtherIterOpts...> second) noexcept
	requires (not std::same_as<DataT, void> and not std::same_as<OtherDataT, void>)
{
	using bigger = iota_options::detail::bigger_type_t<IndexT, OtherIndexT>;

	return first.m_data_iter != second.m_data_iter or 
		static_cast<bigger>(first.m_index_value) != 
		static_cast<bigger>(second.m_index_value);
}

template <typename IndexT, auto t_step, typename DataT, typename ... IterOpts, 
					typename OtherIndexT, auto t_other_step, typename OtherDataT, typename ... OtherIterOpts>
	[[nodiscard]] inline constexpr bool operator<(
		iota_range_iterator<IndexT, t_step, DataT, IterOpts...> const & first,
		iota_range_iterator<OtherIndexT, t_other_step, OtherDataT, OtherIterOpts...> const & second) noexcept
	requires (not std::same_as<DataT, void> and not std::same_as<OtherDataT, void>)
{
	using bigger = iota_options::detail::bigger_type_t<IndexT, OtherIndexT>;

	return first.m_data_iter < second.m_data_iter or 
		(first.m_data_iter == second.m_data_iter and static_cast<bigger>(first.m_index_value) < static_cast<bigger>(second.m_index_value));
}

template <typename IndexT, auto t_step, typename DataT, typename ... IterOpts, 
					typename OtherIndexT, auto t_other_step, typename OtherDataT, typename ... OtherIterOpts>
	[[nodiscard]] inline constexpr bool operator>(
		iota_range_iterator<IndexT, t_step, DataT, IterOpts...> const & first,
		iota_range_iterator<OtherIndexT, t_other_step, OtherDataT, OtherIterOpts...> const & second) noexcept
	requires (not std::same_as<DataT, void> and not std::same_as<OtherDataT, void>)
{
	using bigger = iota_options::detail::bigger_type_t<IndexT, OtherIndexT>;

	return first.m_data_iter > second.m_data_iter or 
		(first.m_data_iter == second.m_data_iter and static_cast<bigger>(first.m_index_value) > static_cast<bigger>(second.m_index_value));
}

template <typename IndexT, auto t_step, typename DataT, typename ... IterOpts, 
					typename OtherIndexT ,auto t_other_step, typename OtherDataT, typename ... OtherIterOpts>
	[[nodiscard]] inline constexpr bool operator<=(
		iota_range_iterator<IndexT, t_step, DataT, IterOpts...> const & first,
		iota_range_iterator<OtherIndexT, t_other_step, OtherDataT, OtherIterOpts...> const & second) noexcept
	requires (not std::same_as<DataT, void> and not std::same_as<OtherDataT, void>)
{
	using bigger = iota_options::detail::bigger_type_t<IndexT, OtherIndexT>;

	return first.m_data_iter < second.m_data_iter or 
		(first.m_data_iter == second.m_data_iter and static_cast<bigger>(first.m_index_value) <= static_cast<bigger>(second.m_index_value));
}

template <typename IndexT, auto t_step, typename DataT, typename ... IterOpts, 
					typename OtherIndexT ,auto t_other_step, typename OtherDataT, typename ... OtherIterOpts>
	[[nodiscard]] inline constexpr bool operator>=(
		iota_range_iterator<IndexT, t_step, DataT, IterOpts...> const & first,
		iota_range_iterator<OtherIndexT, t_other_step, OtherDataT, OtherIterOpts...> const & second) noexcept
	requires (not std::same_as<DataT, void> and not std::same_as<OtherDataT, void>)
{
	using bigger = iota_options::detail::bigger_type_t<IndexT, OtherIndexT>;

	return first.m_data_iter > second.m_data_iter or 
		(first.m_data_iter == second.m_data_iter and static_cast<bigger>(first.m_index_value) >= static_cast<bigger>(second.m_index_value));
}

template <typename IndexT, auto t_step, typename DataT, typename ... IterOpts, 
					typename OtherIndexT ,auto t_other_step, typename OtherDataT, typename ... OtherIterOpts>
	[[nodiscard]] inline constexpr bool operator==(
		iota_range_iterator<IndexT, t_step, DataT, IterOpts...> const & first,
		iota_range_iterator<OtherIndexT, t_other_step, OtherDataT, OtherIterOpts...> const & second) noexcept
{
	using bigger = iota_options::detail::bigger_type_t<IndexT, OtherIndexT>;

	return static_cast<bigger>(first.m_index_value) == static_cast<bigger>(second.m_index_value);
}

template <typename IndexT, auto t_step, typename DataT, typename ... IterOpts, 
					typename OtherIndexT ,auto t_other_step, typename OtherDataT, typename ... OtherIterOpts>
	[[nodiscard]] inline constexpr bool operator!=(
		iota_range_iterator<IndexT, t_step, DataT, IterOpts...> const & first,
		iota_range_iterator<OtherIndexT, t_other_step, OtherDataT, OtherIterOpts...> const & second) noexcept
{
	using bigger = iota_options::detail::bigger_type_t<IndexT, OtherIndexT>;

	return static_cast<bigger>(first.m_index_value) != static_cast<bigger>(second.m_index_value);
}

template <typename IndexT, auto t_step, typename DataT, typename ... IterOpts, 
					typename OtherIndexT ,auto t_other_step, typename OtherDataT, typename ... OtherIterOpts>
	[[nodiscard]] inline constexpr bool operator<(
		iota_range_iterator<IndexT, t_step, DataT, IterOpts...> const & first,
		iota_range_iterator<OtherIndexT, t_other_step, OtherDataT, OtherIterOpts...> const & second) noexcept
{
	using bigger = iota_options::detail::bigger_type_t<IndexT, OtherIndexT>;

	return static_cast<bigger>(first.m_index_value) < static_cast<bigger>(second.m_index_value);
}

template <typename IndexT, auto t_step, typename DataT, typename ... IterOpts, 
					typename OtherIndexT ,auto t_other_step, typename OtherDataT, typename ... OtherIterOpts>
	[[nodiscard]] inline constexpr bool operator>(
		iota_range_iterator<IndexT, t_step, DataT, IterOpts...> const & first,
		iota_range_iterator<OtherIndexT, t_other_step, OtherDataT, OtherIterOpts...> const & second) noexcept
{
	using bigger = iota_options::detail::bigger_type_t<IndexT, OtherIndexT>;

	return static_cast<bigger>(first.m_index_value) > static_cast<bigger>(second.m_index_value);
}

template <typename IndexT, auto t_step, typename DataT, typename ... IterOpts, 
					typename OtherIndexT ,auto t_other_step, typename OtherDataT, typename ... OtherIterOpts>
	[[nodiscard]] inline constexpr bool operator<=(
		iota_range_iterator<IndexT, t_step, DataT, IterOpts...> const & first,
		iota_range_iterator<OtherIndexT, t_other_step, OtherDataT, OtherIterOpts...> const & second) noexcept
{
	using bigger = iota_options::detail::bigger_type_t<IndexT, OtherIndexT>;

	return static_cast<bigger>(first.m_index_value) <= static_cast<bigger>(second.m_index_value);
}

template <typename IndexT, auto t_step, typename DataT, typename ... IterOpts, 
					typename OtherIndexT ,auto t_other_step, typename OtherDataT, typename ... OtherIterOpts>
	[[nodiscard]] inline constexpr bool operator>=(
		iota_range_iterator<IndexT, t_step, DataT, IterOpts...> const & first,
		iota_range_iterator<OtherIndexT, t_other_step, OtherDataT, OtherIterOpts...> const & second) noexcept
{
	using bigger = iota_options::detail::bigger_type_t<IndexT, OtherIndexT>;

	return static_cast<bigger>(first.m_index_value) >= static_cast<bigger>(second.m_index_value);
}


namespace iota_options
{

template <auto t_step>
struct step : pspp::value_holder<t_step>
{};

template <auto t_start>
struct start : pspp::value_holder<t_start>
{};

template <auto t_end>
struct end : pspp::value_holder<t_end>
{};

template <typename DataT>
struct with_data : pspp::type_holder<DataT>
{};

struct make_editable {};

struct do_not_filter_iterator_types {};

namespace detail
{
template <typename T>
struct is_iota_option : std::false_type {};

template <>
struct is_iota_option<do_not_filter_iterator_types> : std::true_type {};
template <>
struct is_iota_option<make_editable> : std::true_type {};
template <auto t_step>
struct is_iota_option<step<t_step>> : std::true_type {};
template <auto t_start>
struct is_iota_option<start<t_start>> : std::true_type {};
template <auto t_end>
struct is_iota_option<end<t_end>> : std::true_type {};
template <typename DataT>
struct is_iota_option<with_data<DataT>> : std::true_type {};
template <typename IndexT>
struct is_iota_option<index_type<IndexT>> : std::true_type {};


template <typename ... Options>
struct iota_range_options
{
	using options = pspp::type_sequence<Options...>;
	using start_opt = extract_type<options{} | get_last_like_or<
		pspp::same_as_complex_value<start>, start<runtime_value>>>;

	using end_opt = extract_type<options{} | get_last_like_or<
		pspp::same_as_complex_value<end>, start<runtime_value>>>;
	using step_opt = extract_type<options{} | get_last_like_or<
		pspp::same_as_complex_value<step>, start<runtime_value>>>;
	using data_type = typename extract_type<options{} | get_last_like_or<
		pspp::same_as_complex<with_data>, with_data<void>>>::type;

	using iterator_under_type = typename extract_type<options{} | get_last_like_or<
		pspp::same_as_complex<index_type>, index_type<long int>>>::type;

	static_val start_val = start_opt::value;
	static_val end_val = end_opt::value;
	static_val step_val = step_opt::value;

	static_flag has_static_start = (start_val != runtime_value);
	static_flag has_static_end = (end_val != runtime_value);
	static_flag has_static_step = (step_val != runtime_value);

	static_flag can_filter = not (options{} | contains<do_not_filter_iterator_types>);
	static_flag can_be_editable = options{} | contains<make_editable>;
};

template <typename Parameter, typename DataT, typename ... Options>
concept c_iterator_argument = requires (Parameter p)
{
	iterator<DataT, Options...>(p);
};

template <typename Parameter, typename DataT, typename ... Options>
concept c_data_holder_argument = requires (Parameter p)
{
	data_holder<DataT, Options...>(p);
};

template <bool t_can_be_editable, auto t_options>
struct editable_maker
{
	static_val types = t_options;
};

template <auto t_options>
struct editable_maker<false, t_options>
{
	static_val types = t_options | append_types_to_front<ranges_options::view>;
};

template <bool t_do_filter, bool t_can_be_editable, typename ... Options>
struct iterator_options_filter
{
	static_val types = editable_maker<t_can_be_editable, typeseq<Options...>>::types;
};

template <bool t_can_be_editable, typename ... Options>
struct iterator_options_filter<true, t_can_be_editable, Options...>
{
	static_val types = editable_maker<t_can_be_editable, typeseq<Options...> | tremove_like<is_iota_option>>::types;
};

template <bool t_do_filter, bool t_can_be_editable>
struct iterator_options_filter<t_do_filter, t_can_be_editable>
{
	static_val types = editable_maker<t_can_be_editable, emptytseq>::types; 
};

template <
					typename DataT, 
					auto t_start,
					auto t_end,
					auto t_step,
					typename IndexT,
					typename ... Options
				>
struct iota_range_impl
{
	using iterator_type = iota_range_iterator<IndexT, t_step, DataT, Options...>;

	iterator<DataT, Options...> m_data;

	inline constexpr iterator_type begin() const noexcept
	{
		return {t_start, m_data};
	}

	inline constexpr iterator_type end() const noexcept
	{
		return {t_end, m_data + t_end - t_start};
	}

	constexpr explicit(true) iota_range_impl(c_iterator_argument<DataT, Options...> auto && data_iter) noexcept:
		m_data{data_iter}
	{}
};

template <
					typename DataT,
					auto t_step,
					typename IndexT,
					typename ... Options
				>
struct iota_range_impl<DataT, runtime_value, runtime_value, t_step, IndexT, Options...>
{
	using iterator_type = iota_range_iterator<IndexT, t_step, DataT, Options...>;

	iterator<DataT, Options...> m_data;
	IndexT m_start;
	IndexT m_end;

	inline constexpr iterator_type begin() const noexcept
	{
		return {m_start, m_data};
	}

	inline constexpr iterator_type end() const noexcept
	{
		return {m_end, m_data + m_end - m_start};
	}

	constexpr explicit(true) iota_range_impl(
			auto start, auto end,
			c_iterator_argument<DataT, Options...> auto && data_iter) noexcept:
		m_start{static_cast<IndexT>(start)},
		m_end{static_cast<IndexT>(end)},
		m_data{data_iter}
	{}

	template <c_data_holder_argument<DataT, Options...> DataHolderArgT>
	constexpr explicit(true) iota_range_impl(DataHolderArgT && data) noexcept:
		m_start{0}
	{
		auto tmp = data_holder<DataT, Options...>(std::forward<DataHolderArgT>(data));

		m_end = static_cast<IndexT>(tmp.size());
		m_data = static_cast<IndexT>(tmp.begin());
	}

	template <c_data_holder_argument<DataT, Options...> DataHolderArgT>
	constexpr explicit(true) iota_range_impl(long int start,
			DataHolderArgT&& data) noexcept:
		m_start{start}
	{
		auto tmp = data_holder<DataT, Options...>(std::forward<DataHolderArgT>(data));

		m_end = static_cast<IndexT>(tmp.size());
		m_data = static_cast<IndexT>(tmp.begin() + start);
	}
};


template <
					auto t_start,
					auto t_end,
					auto t_step,
					typename IndexT,
					typename ... Options
				>
struct iota_range_impl<void, t_start, t_end, t_step, IndexT, Options...>
{
	using iterator_type = iota_range_iterator<IndexT, t_step, void, Options...>;

	inline constexpr iterator_type begin() const noexcept
	{
		return {t_start};
	}

	inline constexpr iterator_type end() const noexcept
	{
		return {t_end};
	}
};

template <
					auto t_start,
					auto t_step,
					typename IndexT,
					typename ... Options
				>
struct iota_range_impl<void, t_start, runtime_value, t_step, IndexT, Options...>
{
	using iterator_type = iota_range_iterator<IndexT, t_step, void, Options...>;

	IndexT m_end;

	inline constexpr iterator_type begin() const noexcept
	{
		return {t_start}; 
	}

	inline constexpr iterator_type end() const noexcept
	{
		return {m_end};
	}

	constexpr explicit(true) iota_range_impl(auto end) noexcept:
		m_end{static_cast<IndexT>(end)}
	{}

};

template <
					auto t_step,
					typename IndexT,
					typename ... Options
				>
struct iota_range_impl<void, runtime_value, runtime_value, t_step, IndexT, Options...>
{
	using iterator_type = iota_range_iterator<IndexT, t_step, void, Options...>;

	IndexT m_start;
	IndexT m_end;

	inline constexpr iterator_type begin() const noexcept
	{
		return {m_start};
	}

	inline constexpr iterator_type end() const noexcept
	{
		return {m_end};
	}

	constexpr explicit(true) iota_range_impl(auto start, auto end) noexcept:
		m_start{static_cast<IndexT>(start)},
		m_end{static_cast<IndexT>(end)}
	{}

};

template <
					auto t_end,
					auto t_step,
					typename IndexT,
					typename ... options
				>
struct iota_range_impl<void, runtime_value, t_end, t_step, IndexT, options...>
{
	using iterator_type = iota_range_iterator<IndexT, t_step, void, options...>;

	IndexT m_start;

	inline constexpr iterator_type begin() const noexcept
	{
		return {m_start};
	}

	inline constexpr iterator_type end() const noexcept
	{
		return {t_end};
	}

	constexpr explicit(true) iota_range_impl(auto start) noexcept:
		m_start{static_cast<IndexT>(start)}
	{}
};


template <
			typename IndexT,
			typename ... Options>
struct iota_range_impl<void, runtime_value, runtime_value, runtime_value, IndexT, Options...>
{
	using iterator_type = iota_range_iterator<IndexT, runtime_value, void, Options...>;

	IndexT m_start;
	IndexT m_end;
	IndexT m_step;

	inline constexpr iterator_type begin() const noexcept
	{
		return {m_start, m_step};
	}

	inline constexpr iterator_type end() const noexcept
	{
		return {m_end, m_step};
	}

	inline constexpr explicit(true) iota_range_impl(auto start, auto end, auto step):
		m_start{static_cast<IndexT>(start)},
		m_end{static_cast<IndexT>(end)},
		m_step{static_cast<IndexT>(step)}
	{}
};


template <typename ... Options>
struct iota_range_holder
{
	using options = iota_range_options<Options...>;

	using type = extract_type<
		[]<typename ... IterOpts>(type_sequence<IterOpts...>)
		{
			return type<iota_range_impl<
			typename options::data_type,
			options::start_val,
			options::end_val,
			options::step_val,
			typename options::iterator_under_type,
			IterOpts...>>;
		}(iterator_options_filter<options::can_filter, 
				options::can_be_editable, Options...>::types)>;
};

} // namespace detail
} // namespace iota_options

template <typename ... Options>
struct iota_range_t : iota_options::detail::iota_range_holder<Options...>::type
{
	template <typename ... Args>
	inline constexpr iota_range_t(Args&& ... args):
		iota_options::detail::iota_range_holder<Options...>::type{std::forward<Args>(args)...}
	{}

	/*inline constexpr auto to_editable() const noexcept
	{
		return iota_range_t<Options..., iota_options::make_editable>{*this};
	}
	*/
};

template <auto t_start, auto t_end, auto t_step = 1>
inline constexpr auto static_iota_range = iota_range_t<
	iota_options::start<t_start>,
	iota_options::end<t_end>,
	iota_options::step<t_step>
>{};

template <
					auto t_start,
					auto t_end,
					auto t_step = 1,
					typename IndexT = long int
				>
inline consteval iota_range_t<
	iota_options::start<t_start>,
	iota_options::end<(t_end / t_step + t_end % t_step) * t_step>,
	iota_options::step<t_step>,
	iota_options::index_type<IndexT>
 > iota_range() noexcept { return {}; }

template <auto t_start, auto t_end,
					typename IndexT>
inline consteval iota_range_t<
	iota_options::start<t_start>,
	iota_options::end<t_end>,
	iota_options::index_type<IndexT>
> iota_range() noexcept { return {}; }

template <
					auto t_end, typename IndexT = long int
				>
inline consteval iota_range_t<
	iota_options::start<0>,
	iota_options::end<t_end>,
	iota_options::step<1>,
	iota_options::index_type<IndexT>
> iota_range() noexcept { return {}; }

template <
					auto t_step = 1,
					typename IndexT = long int
				>
inline constexpr 
iota_range_t<
		iota_options::step<t_step>,
		iota_options::index_type<IndexT>
	> iota_range(long int start, long int end) noexcept 
{
	if constexpr (t_step != 1)
	{
		return {start, (end / t_step + end % t_step) * t_step};
	}
	else
		return {start, end};
}

template <
					typename IndexT
				>
inline constexpr 
iota_range_t<
	iota_options::step<1>,
	iota_options::index_type<IndexT>
	> iota_range(long int start, long int end) noexcept 
{
		return {start, end};
}


template <typename IndexT = long int>
inline constexpr 
iota_range_t<
	iota_options::start<0>,
	iota_options::step<1>,
	iota_options::index_type<IndexT>
> iota_range(auto t_end) noexcept
{
	return {t_end};
}


template <typename IndexT = long int>
inline constexpr iota_range_t<iota_options::index_type<IndexT>> 
	iota_range(auto start, auto end, auto step) noexcept
{
	return {start, (end / step + end % step) * step, step};
}

template <auto t_step = 1, typename IndexT = long int, typename RangeT>
inline constexpr auto iota_range(RangeT && data) noexcept
	requires (requires {data_holder(data);})
{
	using data_type = typename decltype(data_holder(data))::value_type;
	return iota_range_t<
		iota_options::with_data<data_type>,
		iota_options::step<t_step>,
		iota_options::index_type<IndexT>
	>{std::forward<RangeT>(data)};
}

template <typename IndexT, typename RangeT>
inline constexpr auto iota_range(RangeT && data) noexcept
	requires (requires {data_holder(data);})
{
	using data_type = typename decltype(data_holder(data))::value_type;
	return iota_range_t<
		iota_options::with_data<data_type>,
		iota_options::step<1>,
		iota_options::index_type<IndexT>
	>{std::forward<RangeT>(data)};

}

} // namespace pspp

#endif // PSPP_UTILITY_IOTA_RANGE_HPP
