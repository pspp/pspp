#ifndef PSPP_UTILITY_UTILITY_HPP
#define PSPP_UTILITY_UTILITY_HPP

#include <pspp/utility/declaration_macros.hpp>
#include <pspp/value_holder.hpp>

#include <cstdint>
#include <utility>

namespace std
{
  template <typename T>
  constexpr auto size(T) requires (sizeof(T) < 1);
}

namespace pspp
{

namespace detail
{
template <auto t_val>
inline consteval decltype(t_val) sqrt_impl() noexcept
{
	decltype(t_val) res {0};
	auto tmp = t_val;

	while (tmp > 1)
	{
		tmp >>= 1;
		res++;
	}

	return res;
}
} // namespace detail

template <auto t_val>
inline constexpr decltype(t_val) sqrt = detail::sqrt_impl<t_val>();

template <std::size_t t_size>
constexpr bool is_mod_of_2 = (t_size & (t_size - 1)) == 0;

template <std::size_t t_maybe_power_of_2>
pspp_always_inline constexpr auto multiply_by(std::integral auto to_be_powered) noexcept
{
	if constexpr (is_mod_of_2<t_maybe_power_of_2>())
		return to_be_powered << sqrt<t_maybe_power_of_2>();
	else
		return to_be_powered * t_maybe_power_of_2;
}

template <std::size_t t_maybe_power_of_2>
pspp_always_inline constexpr auto divive_by(std::integral auto to_be_powered) noexcept
{
	if constexpr (is_mod_of_2<t_maybe_power_of_2>())
		return to_be_powered >> sqrt<t_maybe_power_of_2>();
	else
		return to_be_powered / t_maybe_power_of_2;
}

inline constexpr std::size_t npos {static_cast<std::size_t>(-1)};

template <typename T>
inline constexpr T npos_for = static_cast<T>(-1);

template <auto t_val>
concept c_128t_enabled = requires(){
  typename __int128_t;
  requires t_val == 16;
};

inline constexpr bool is_int128_enabled = requires{
  typename __int128_t;
};

namespace detail
{

template <std::size_t>
struct _sized_num_type_from_size
{};
template <>
struct _sized_num_type_from_size<1>
{ using type = std::int8_t; };
template <>
struct _sized_num_type_from_size<2>
{ using type = std::int16_t; };
template <>
struct _sized_num_type_from_size<4>
{ using type = std::int32_t; };
template <>
struct _sized_num_type_from_size<8>
{ using type = std::int64_t; };

template <>
struct _sized_num_type_from_size<16>
{ using type = decltype(
  []{
    if constexpr (requires { typename __int128_t; }) return __int128_t{0};
    else return std::int64_t{0};
  }());
};

template <std::size_t t_size>
struct _unsized_num_type_from_size
{
  using type = std::make_unsigned_t<typename _sized_num_type_from_size<t_size>::type>;
};

template <>
struct _unsized_num_type_from_size<16>
{
  using type = decltype([]{
    if constexpr (is_int128_enabled)
      return __uint128_t{0};
    else return uint64_t{0};
  }());
};

} // namespace detail

template <std::size_t t_size>
using num_type_from_size_t = typename detail::_sized_num_type_from_size<t_size>::type;

template <std::size_t t_size>
using unum_type_from_size_t = typename detail::_unsized_num_type_from_size<t_size>::type;

using int128_t = num_type_from_size_t<16>;
using uint128_t = unum_type_from_size_t<16>;

inline constexpr auto max_supported_size = sizeof(int128_t);

template <typename T>
concept c_int128 = is_int128_enabled and std::same_as<T, int128_t>;

template <typename T>
concept c_uint128 = is_int128_enabled and std::same_as<T, uint128_t>;


template <typename WithSpec, typename WithotSpecs>
struct copy_extensions
{ using type = std::decay_t<WithotSpecs>; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec const, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> const; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec &&, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> &&; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec const &&, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> const &&; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec &, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> &; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec const &, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> const &; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec *, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> *; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec const *, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> const *; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec * const, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> * const; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec const * const, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> const * const; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec volatile const, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> volatile const; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec volatile &, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> volatile &; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec const volatile &, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> const volatile &; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec volatile &&, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> volatile &&; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec const volatile &&, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> volatile &&; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec volatile *, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> volatile *; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec const volatile *, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> const volatile *; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec volatile * const, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> volatile * const; };

template <typename WithSpec, typename WithoutSpecs>
struct copy_extensions<WithSpec const volatile * const, WithoutSpecs>
{ using type = std::decay_t<WithoutSpecs> const volatile * const; };


template <typename WithSpec, typename WithoutSpecs>
using copy_extensions_t = typename copy_extensions<WithSpec, WithoutSpecs>::type;

template <std::integral auto t_min, decltype(t_min) t_max>
struct numrange
{
	using value_type = decltype(t_min);
	static_prop_of value_type min = t_min;
	static_prop_of value_type max = t_max;
};


namespace detail
{


template <typename T, typename U>
consteval auto get_bigger_integral_impl() noexcept
{
  constexpr T t_npos = static_cast<T>(-1);
  constexpr U u_npos = static_cast<U>(-1);

  constexpr auto
      t_size   = sizeof(T),
      u_size   = sizeof(U),
      max_size = max_supported_size;

  constexpr int
      t_is_signed =  ((t_npos + 1) > t_npos),
      u_is_signed = -((u_npos + 1) > u_npos);

  // different sign-nes
  if constexpr (t_is_signed + u_is_signed == 0)
  {
    if constexpr (t_size > u_size)
      return t_npos;
    else return u_npos;
  } else
  {
    if constexpr (t_is_signed)
    {
      if constexpr (u_size < t_is_signed) return t_npos;
      else if constexpr (u_size < max_size)
        return num_type_from_size_t<u_size * 2>{0};
      else return num_type_from_size_t<max_size>{0};
    } else if constexpr (u_is_signed)
    {
      if constexpr (t_size < u_size) return u_npos;
      else if constexpr (t_size < max_size)
        return num_type_from_size_t<t_size * 2>{0};
      else return num_type_from_size_t<max_size>{0};
    }
  }
}


template <typename T, typename U>
using _bigger_integral_t = decltype(get_bigger_integral_impl<T, U>());

template <typename T1, typename T2, typename ... Ts>
consteval auto get_bigger_integral() noexcept
{
  using tmp_common = _bigger_integral_t<T1, T2>;

  if constexpr (sizeof...(Ts) == 0)
    return tmp_common{0};
  else
    return get_bigger_integral<tmp_common, Ts...>();
}

}

template <typename T1, typename T2, typename ... Ts>
using bigger_integral_t = decltype(detail::get_bigger_integral<T1, T2, Ts...>());


	template <auto t_check, auto t_min, auto t_max>
	consteval bool in_range() noexcept
	{
    using common_t = bigger_integral_t<decltype(t_check), decltype(t_min), decltype(t_max)>;

		return std::cmp_greater_equal(
               static_cast<common_t>(t_check),
               static_cast<common_t>(t_min)
           ) and
		       std::cmp_less_equal(
               static_cast<common_t>(t_check),
               static_cast<common_t>(t_max)
           );
	}

	template <auto t_check, typename NumRangeT>
	consteval bool in_range() noexcept { return in_range<NumRangeT::min(), NumRangeT::max()>(); }

	template <auto t_min, auto t_max>
	constexpr bool in_range(auto to_check) noexcept
	{
    using common_t = bigger_integral_t<decltype(t_min), decltype(t_max), decltype(to_check)>;

		return
      static_cast<common_t>(to_check) >= static_cast<common_t>(t_min) and
      static_cast<common_t>(to_check) <= static_cast<common_t>(t_max);
	}

using address_word_t = unum_type_from_size_t<sizeof(void*)>;
using signed_address_word_t = num_type_from_size_t<sizeof(void*)>;

template <bool t_cond, auto t_if_true, auto t_if_false>
struct conditional_value
{
	static_val value = t_if_true;
};

template <auto t_if_true, auto t_if_false>
struct conditional_value<false, t_if_true, t_if_false>
{ static_val value = t_if_false; };

template <bool t_cond, auto t_if_true, auto t_if_false>
inline constexpr auto conditional_v = conditional_value<t_cond, t_if_true, t_if_false>::value;

template <typename T, auto t_n>
using raw_array_t = T[t_n];

template <typename ... Ts>
struct inheritance_base : Ts...
{};


namespace detail
{
struct call_operator_holder
{
	struct result {};
	constexpr result operator()() noexcept { return {}; }
};

} // namespace detail

template <typename T>
concept c_has_call_operator = std::is_class_v<std::remove_cvref_t<T>> and not requires{
	&inheritance_base<detail::call_operator_holder, std::remove_cvref_t<T>>::operator();
};

template <typename T>
concept c_callable = std::is_function_v<std::remove_reference_t<T>> or c_has_call_operator<T>;

template <auto t_val>
inline constexpr bool is_npos_v = (t_val == npos_for<std::remove_cvref_t<decltype(t_val)>>);

template <typename T, template <typename> class CheckerT>
concept c_satisfies_checker = CheckerT<T>::value;

template <typename ...>
constexpr void non_callable(auto ...) noexcept
{ static_assert(false, "caller of this function is not callable"); }

template <auto ...>
constexpr void non_callable(auto ...) noexcept
{ static_assert(false, "caller of this function is not callable"); }

inline constexpr auto non_callable_v = [](auto ...){
	static_assert(false, "caller of this function is not callable"); 
};

pspp_always_inline
constexpr auto is_integer_part(char c) noexcept
{ return c >= '0' and c <= '9'; }

pspp_always_inline
constexpr auto is_number_part(char c) noexcept
{ return is_integer_part(c) or c == '.' or c == 'e'; }

pspp_always_inline
constexpr auto is_integer_begin(char c) noexcept
{ return is_integer_part(c) or c == '-' or c == '+'; }

template <typename To, typename From>
concept c_implicitly_convertible_from = requires (To to, From from)
{ to = from; };

template <auto t_executor, typename T, typename U>
using common_type_for_bin_t = decltype(t_executor(std::declval<T>(), std::declval<U>()));

template <typename SizeT = std::size_t, typename DataT>
[[nodiscard]]
constexpr auto size(DataT && std_size_gettable) noexcept -> SizeT
  pspp_if_possible(
    std::size(std::declval<DataT>())
  )
{
  return static_cast<SizeT>(
    std::size(std::forward<DataT>(std_size_gettable))
  );
}

template <typename SizeT = std::size_t, typename KnownTupleSize>
  pspp_if_possible(
    std::tuple_size<std::remove_cvref_t<KnownTupleSize>>::value
    ) and (
      not requires {std::size(std::declval<KnownTupleSize>()); }
    )
constexpr auto size(KnownTupleSize &&) noexcept
{
  return static_cast<SizeT>(std::tuple_size_v<std::remove_cvref_t<KnownTupleSize>>);
}

template <typename SizeT = std::size_t, typename KnownTupleSizeT, template <typename> class HolderT>
  pspp_if_possible(std::tuple_size<std::remove_cvref_t<KnownTupleSizeT>>::value )
constexpr auto static_size_of(HolderT<KnownTupleSizeT>) -> SizeT
{ return static_cast<std::size_t>(std::tuple_size_v<KnownTupleSizeT>); }

template <typename SizeT = std::size_t, typename KnownSize>
  requires std::integral<
    std::remove_cvref_t<decltype(std::remove_cvref_t<KnownSize>::size)>
  >
constexpr auto size(KnownSize &&) -> SizeT
{
  return static_cast<SizeT>(std::remove_cvref_t<KnownSize>::size);
}

template <typename SizeT = std::size_t, typename KnownSizeT, template <typename> class HolderT>
  pspp_if_possible( KnownSizeT::size ) and (not requires { std::tuple_size<KnownSizeT>::value; })
constexpr auto static_size_of(HolderT<KnownSizeT>) -> SizeT
{ return static_cast<std::size_t>(KnownSizeT::size); }


template <typename DataT>
constexpr auto make_index_sequence_from_size_of(DataT const &) noexcept
{
  return std::make_index_sequence<size(DataT{})>{};
}


template <typename DerivedT, typename BaseT>
concept c_derived_from = std::derived_from<std::remove_cvref_t<DerivedT>, BaseT>;


template <typename T>
concept c_integral = std::integral<T> or c_int128<T> or c_uint128<T>;

template <typename DataT, auto t_expected, bool t_is_true = true>
struct checkable_value
{
  DataT info;

  [[nodiscard]] pspp_always_inline
  constexpr bool is_true() const noexcept
  { return (t_expected == info) and t_is_true; }

  [[nodiscard]] pspp_always_inline
  constexpr bool is_false() const noexcept
  { return (t_expected != info) and not t_is_true; }

  [[nodiscard]] pspp_always_inline
  constexpr operator bool() const noexcept
  { return is_true(); }
};


template <typename T>
struct dval_t
{
  constexpr T operator!() const noexcept;
 // { return std::declval<T>(); }

  constexpr T operator*() const noexcept
  { return std::declval<T>(); }

  constexpr T operator-() const noexcept
  { return std::declval<T>(); }
};

template <typename T>
inline constexpr dval_t<T> dval {};



} // namespace pspp


#endif // PSPP_UTILITY_UTILITY_HPP
