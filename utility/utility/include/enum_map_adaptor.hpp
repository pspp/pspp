#ifndef PSPP_ENUM_MAP_ADAPTOR
#define PSPP_ENUM_MAP_ADAPTOR

#include <pspp/constr.hpp>
#include <pspp/runtime_handlers_map.hpp>
#include <pspp/unsized_constr.hpp>


namespace pspp
{

namespace detail
{

template <typename EnumT, template_array t_indexes,
					template_unsized_constr_array t_strs,
					auto t_def_index, constr t_def_str>
struct enum_map_adaptor_implementation
{
	template <typename T>
	struct from_implementation 
	{
		consteval std::string_view operator()() noexcept
		{ return ""; }
	};

	template <typename T>
	struct to_implementation
	{
		consteval EnumT operator()() noexcept
		{
		}
	};

	template <auto t_val>
	struct to_implementation<value_holder<t_val>>
	{
		consteval EnumT operator()() noexcept
		{ return static_cast<EnumT>(t_val); }
	};


	template <pspp::constr t_str>
	struct from_implementation<cstr_holder<t_str>>
	{
		consteval std::string_view operator()() noexcept
		{ return t_str.strv(); }
	};
	
	static_prop_of runtime_handlers_map strings_map {t_strs, type<value_holder<t_def_index>>, 
		handler<to_implementation>(), typename decltype(t_indexes)::holders_seq{}};
	static_prop_of runtime_handlers_map indexes_map {t_indexes, type<cstr_holder<t_def_str>>,
		handler<from_implementation>(), typename decltype(t_strs)::holders_seq{}};


	inline static constexpr std::string_view from(EnumT val) noexcept
	{ return indexes_map.call(val); }
};

} // namespace detail

template <auto t_def_index, constr t_def_str>
struct default_enum_values
{
	using index_holder = value_holder<t_def_index>;
	using str_holder = cstr_holder<t_def_str>;
};

namespace detail
{
	template <typename T>
	struct is_default_enum_values : std::false_type {};

	template <auto t_def_index, constr t_def_str>
	struct is_default_enum_values<default_enum_values<t_def_index, t_def_str>> :
	std::true_type {};

	template <typename T>
	concept c_default_enum_values = is_default_enum_values<T>::value;
}


template <typename EnumT, detail::c_default_enum_values DefaultVals, constr ... t_strings>
struct simple_enum_map_adaptor : 
{
};

template <typename EnumT, c_value_sequence IndexesT, constr ... t_strs>
struct one_to_one_enum_map_adaptor
{
	static_assert(IndexesT::count == sizeof...(t_strs), "one_to_one_enum_map_adaptor: not paired");
};


enum tmp {a};
template <typename EnumT, auto t_min, auto t_max, auto t_step, constr ... t_strs>
struct continuous_enum_map_adaptor
{
	static_assert(((t_max - t_min ) / t_step) * t_step == t_max - t_min, "continuous_enum_map_adaptor: range is not dividable by t_step");
	static_assert((t_max - t_min) / t_step == sizeof...(t_strs), "continuous_enum_map_adaptor: strs count not equal enum elements count");
};

} // namespace pspp

#endif // PSPP_ENUM_MAP_ADAPTOR
