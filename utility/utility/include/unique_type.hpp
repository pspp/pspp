#ifndef PSPP_UNIQUE_TYPE_HPP
#define PSPP_UNIQUE_TYPE_HPP

namespace pspp
{


//template <auto _ = []{}>
//using unique_type = decltype(_);

#define unique_type decltype([]{})

} // namespace pspp

#endif // PSPP_UNIQUE_TYPE_HPP
