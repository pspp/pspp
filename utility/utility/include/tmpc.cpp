#include <pspp/macro_colors.hpp>
#include <iostream>

int main()
{
	std::cout << pspp_red "hello world" pspp_clcol "\n"
		pspp_blue "hello world" pspp_clcol "\n"
		pspp_yellow "hello world" pspp_clcol "\n"
		pspp_col(123) "hello world" pspp_clcol "\n";
}
