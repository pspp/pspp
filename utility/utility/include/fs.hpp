#ifndef PSPP_UTILITY_FS_HPP
#define PSPP_UTILITY_FS_HPP

#include <sstream>
#include <fstream>
#include <vector>
#include <format>
#include <filesystem>

#include <pspp/simple/string_view.hpp>


namespace pspp
{


template <typename StringT = std::string>
StringT read_file(simple_string_view file_path) noexcept
{
  std::stringstream file_data;
  std::ifstream stream(file_path.data());

  if (not stream)
    return {};

  stream.seekg(std::ostream::end);
  std::size_t file_len = stream.tellg();
  stream.seekg(std::ostream::beg);

  std::string data_buff_holder;
  data_buff_holder.reserve(file_len);

  file_data.str(std::move(data_buff_holder));
  file_data << stream.rdbuf();

  if constexpr (std::same_as<std::stringstream, StringT>)
    return file_data;
  else return std::move(file_data).str();
}

void write_file(std::string_view path, auto ... file_data) noexcept
{
  std::ofstream stream{path.data()};
  (..., stream.write(file_data.data(), file_data.size()));
  stream.close();
}

template <typename DataT, typename AllocatorT>
void write_file(std::string_view path, std::vector<DataT, AllocatorT> data, std::string_view delim = "\n") noexcept
{
  std::ofstream stream {path.data()};
  for (auto const & str : data)
  {
    stream.write(str.data(), str.size());
    stream.write(delim.data(), delim.size());
  }
  stream.close();
}



enum struct file_check_types {
  file_expected,
  dir_expected,
  any
};

template <file_check_types t_type>
struct default_file_path_error_handler_t {
  [[noreturn]]
  static void operator()(std::string_view path) noexcept
  {
    static constexpr constr fmt_str =
      constr{"specified path was not valid: `{}`; no such "} +
      []{
        using enum file_check_types;

             if constexpr (t_type == file_expected) return constr{"file"};
        else if constexpr (t_type == dir_expected)  return constr{"directory"};
        else if constexpr (t_type == any)           return constr{"file or directory"};
      }() + " exists";

    std::string const tmp_ = std::format(fmt_str.strv(), path);
    std::puts(tmp_.c_str());

    std::exit(EXIT_FAILURE);
  }
};

template <file_check_types t_type>
inline constexpr default_file_path_error_handler_t<t_type> default_file_path_error_handler {};


template <typename StringT, typename HandlerT, file_check_types t_type>
struct basic_checked_file_path
{
  StringT raw_file_path;
  HandlerT handler;

  constexpr basic_checked_file_path() = default;
  constexpr basic_checked_file_path(basic_checked_file_path const &) = default;
  constexpr basic_checked_file_path(basic_checked_file_path &&) = default;

  constexpr bool is_valid() const noexcept
  {
    if not consteval
    {
      using enum file_check_types;
      namespace sfs_ = std::filesystem;

      if constexpr (t_type == any)
        return sfs_::is_regular_file(raw_file_path) or sfs_::is_directory(raw_file_path);
      else if constexpr (t_type == file_expected)
        return sfs_::is_regular_file(raw_file_path);
      else if constexpr (t_type == dir_expected)
        return sfs_::is_directory(raw_file_path);
    } else
    { return false; }
  }

  constexpr void assert() const noexcept
  {
    if not consteval
    {
      if (not is_valid())
        handler(raw_file_path);
    }
  }

  constexpr basic_checked_file_path(StringT string_, HandlerT handler_ = default_file_path_error_handler<t_type>) :
    raw_file_path{std::move(string_)},
    handler{handler_}
  { assert(); }

  constexpr basic_checked_file_path(HandlerT handler_) :
    raw_file_path{},
    handler{handler_}
  {}

  operator std::filesystem::path()
  {
    return {raw_file_path};
  }

  constexpr std::string read_all() const noexcept
  {
    return read_file(raw_file_path);
  }
};

using checked_file_path = basic_checked_file_path<
  std::string,
  default_file_path_error_handler_t<file_check_types::file_expected>,
  file_check_types::file_expected>;

using checked_dir_path = basic_checked_file_path<
  std::string,
  default_file_path_error_handler_t<file_check_types::dir_expected>,
  file_check_types::dir_expected
>;

using checked_path = basic_checked_file_path<
  std::string,
  default_file_path_error_handler_t<file_check_types::any>,
  file_check_types::any
>;

using checked_file_path_view = basic_checked_file_path<
  std::string_view,
  default_file_path_error_handler_t<file_check_types::file_expected>,
  file_check_types::file_expected>;

using checked_dir_path_view = basic_checked_file_path<
  std::string_view,
  default_file_path_error_handler_t<file_check_types::dir_expected>,
  file_check_types::dir_expected
>;

using checked_path_view = basic_checked_file_path<
  std::string_view,
  default_file_path_error_handler_t<file_check_types::any>,
  file_check_types::any
>;
}

#endif // PSPP_UTILITY_FS_HPP
