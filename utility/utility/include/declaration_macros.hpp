#ifndef PSPP_USEFUL_UTILS_DECLARATION_MACROS_HPP
#define PSPP_USEFUL_UTILS_DECLARATION_MACROS_HPP

#include <version>

#ifdef __clang__ 
#define pspp_attr_always_inline clang::always_inline
#define pspp_always_inline [[clang::always_inline]]
#define pspp_false(expr) always_false<expr>()
#define pspp_restr __restrict
#elifdef _MSC_VER
#define pspp_always_inline __forceinline
#define pspp_false(expr) always_false<expr>()
#define pspp_restr __restrict
#elifdef __GNUG__
#define pspp_attr_always_inline gnu::always_inline
#define pspp_always_inline [[gnu::always_inline]]
#define pspp_false(...) false
#define pspp_restr __restrict
#else
#define pspp_attr_always_inline
#define pspp_always_inline
#define pspp_false(...) false
#define pspp_restr
#endif

#define instantfunc consteval auto
#define constfunc inline constexpr auto
#define static_flag inline static constexpr bool
#define static_size inline static constexpr unsigned long
#define static_val inline static constexpr auto
#define static_prop_of inline static constexpr
#define global_static_flag inline constexpr bool

#ifdef  __cpp_static_call_operator
#define pspp_static_call_operator(...) static operator()(__VA_ARGS__)
#define pspp_static_call_const_operator(...) static operator()(__VA_ARGS__)
#else 
#define pspp_static_call_operator(...) operator()(__VA_ARGS__)
#define pspp_static_call_const_operator(...) operator()(__VA_ARGS__) const
#endif

#define pspp_execute_if_can(...) if constexpr (requires {__VA_ARGS__;}) __VA_ARGS__
#define pspp_execute_and_return_if_can(...) if constexpr (requires {__VA_ARGS__;}) return __VA_ARGS__
#define pspp_execute_multiple_if_can(...) if constexpr (requires {__VA_ARGS__;}) { __VA_ARGS__
#define pspp_close_scope }

#if __cpp_lib_ranges > 202200L
#define pspp_range_adaptor_closure adaptor_pipe_closure, std::ranges::range_adaptor_closure
#define pspp_range_adaptor_closure adaptor_pipe_closure, std::ranges::range_adaptor_closure
#else
#define pspp_range_adaptor_closure std::__range_adaptor_closure
#endif

#define pspp_fwd(val_name) std::forward<decltype(val_name)>(val_name)
#define pspp_if_possible(...) requires requires { __VA_ARGS__; }
#define pspp_if_impossible(...) requires (not requires { __VA_ARGS__; })

#define pspp_noexcept_if_possible(...) noexcept (noexcept (__VA_ARGS__)) \
  pspp_if_possible(__VA_ARGS__)

#define pspp_void_auto_noexcept(...) noexcept (noexcept (__VA_ARGS__)) { __VA_ARGS__; }
#define pspp_oneline_auto_noexcept(return_line) noexcept (noexcept (return_line) ) { return return_line; }

#define pspp_void_auto_noexcept_if_possible(...) noexcept (noexcept (__VA_ARGS__)) requires requires { __VA_ARGS__; } \
{ __VA_ARGS__; }

#define pspp_oneline_auto_noexcept_if_possible(return_line) noexcept (noexcept (return_line)) \
  requires requires { return_line; } \
{ return return_line; }
#define pspp_declean(value) std::remove_cvref_t<decltype(value)>
#define pspp_declval(value) std::remove_reference_t<decltype(value)>
#define pspp_decltype(type_t_val) typename decltype(type_t_val)::t

#define pspp_satisfies(concept_name, ...) []<typename T>{ return concept_name<T __VA_OPT__(,) __VA_ARGS__>; }

#define pspp_is_same(left_type, right_type) (&typeid(left_type) == &typeid(right_type))


#endif // PSPP_USEFUL_UTILS_DECLARATION_MACROS_HPP
