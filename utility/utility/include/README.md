
# utility::setter

Tool for creating value passing rules at compiletime to pass those values at runtime.
Can be used when there is need to tell program how to store value into some variable which will be passed at runtime.

## Example

```cpp
#include <pspp/utility/setter.hpp>

#include <pspp/modern_main.hpp>
#include <pspp/useful/ostream_overloads.hpp>


struct U {
	int i;
	std::string_view s;
};

struct S {
	U u;
	char c;
	double d;
	int sum;
};

template <>
struct pspp::struct_info<U> :
    pspp::struct_info_spec<U,
			pspp::with_name<int, "i">,
			pspp::with_name<std::string_view, "s">
    >
{};

template <>
struct pspp::struct_info<S> :
    pspp::struct_info_spec<S,
			pspp::with_name<U, "u">,
			pspp::with_name<char, "c">,
			pspp::with_name<double, "d">,
			pspp::with_name<int, "sum">
		>
{};

// compiletime defined rule for static value
inline constexpr auto c_setter = pspp::setter<&S::c, 'b'>;
// compiletime defined rule for runtime val
inline constexpr auto d_setter = pspp::setter<&S::d>;

// rule for composed value
inline constexpr auto i_setter =
  pspp::setter<&S::u,
	  pspp::setter<&U::i>
	>;

// rule for composed value created by recursive function call
inline constexpr auto s_setter = []{
	return
   	pspp::static_call<
	    []<auto t_prev>(){
		    return t_prev.template value<pspp::setter_holder<&U::s>>;
	    },
        pspp::setter_holder<&S::u>
	>();
}().value<>;

// rule with custom setter
inline constexpr auto sum_setter = pspp::setter<&S::sum, [](auto const & vals, int first = 0){
	return std::accumulate(vals.begin(), vals.end(), first);
}>;

void true_main(int i, std::string_view s, double d, std::vector<int> const & vals) noexcept {
	S var{}; // variable created ar runtime
	c_setter(var);
	i_setter(var, i);
	s_setter(var, s);
	d_setter(var, d);
	sum_setter(var, vals);

	std::cout << var << '\n';
}

PSPP_TRUE_MAIN_ENTRY()
```

Expected output:

```bash
$ ./a.out 9 hello 2.5 1 2 3 4 5 6 7 8 9
{u: {i: 9, s: hello}, c: b, d: 2.5, sum: 45}
```

Current example compiles with g++ 13.2.1 20230801, but not with clang.
(compiled with: `g++ -std=c++23 -O3 -Wall -Wextra -Wshadow -Wconversion -Wpedantic -Werror example.cpp`)

If `pspp/useful/ostream_overloads.hpp` removed, it must work with clang to.

# utility::iota\_range

Create simple iota range (works like `std::views::iota`).
Implements step value.

## Example 

(to be added)


