#ifdef __unix__

#ifndef PSPP_UTILITY_AUTCOMPLETION_HPP
#define PSPP_UTILITY_AUTCOMPLETION_HPP

#include <charconv>
#include <vector>
#include <iostream>
#include <ranges>

#include <pspp/array.hpp>
#include <pspp/constr.hpp>
#include <pspp/unsized_constr.hpp>
#include <pspp/unsized_array.hpp>
#include <pspp/input_converter.hpp>
#include <pspp/utility/utility.hpp>
#include <pspp/io.hpp>


namespace pspp
{
  pspp_always_inline [[nodiscard]]
  std::vector<std::string_view> get_previous_words() noexcept
  { return get_custom_previous_words<"COMP_LINE">(); }

  template <>
  inline constexpr auto required_args<supported_shels::bash> = array{
    std::string_view{"PSPP_COLUMNS"}, "PSPP_COMP_WORDS"};

  template <supported_shels t_shel>
  using required_args_t = std::remove_cvref_t<decltype(required_args<t_shel>)>;



template <supported_shels t_shel>
struct acomp_info
{
  inline static std::size_t const current_word_index = get_current_word_index();
  inline static auto const words = get_previous_words();

  pspp_always_inline [[nodiscard]]
  static auto current_word() noexcept -> std::string_view
  {
    if (words.empty()) return "";
    else return words.back();
  }

  pspp_always_inline [[nodiscard]]
  static auto previous() noexcept -> std::span<std::string_view const>
  { return {words.begin(), current_word_index - 1}; }
};

template <>
struct acomp_info<supported_shels::bash>
{
  inline static std::size_t const current_word_index = get_current_word_index();
  inline static auto const words = get_previous_words();

  pspp_always_inline [[nodiscard]]
  static auto current_word() noexcept -> std::string_view
  {
    if (words.empty()) return "";
    else return words.back();
  }

  pspp_always_inline [[nodiscard]]
  static auto previous() noexcept -> std::span<std::string_view const>
  { return {words.begin(), current_word_index - 1}; }
};

namespace experimental
{
  void test_autocomplete(std::string_view program_name, std::string_view completer_name) noexcept
  {
    using ai = acomp_info<supported_shels::zsh>;
    if (ai::current_word_index == 0)
    {
      std::cout << "complete -o default -o nospace -F " << completer_name
                << ' ' <<  program_name << '\n';
    } else {
      for (auto const word : ai::previous())
        std::cout << word << "\n";

      std::cout << "LAST__" << ai::current_word() << "\n./" << program_name;
    }

  }
} // namespace experimental

} // namespace pspp

#undef AUTOCOMP_CONSTEXPR

#endif // PSPP_UTILITY_AUTCOMPLETION_HPP
#else // __unix__
#error THIS LIBRARY SUPPORTS ONLY UNIX-BASED OS; FOR WINDOWS USE WINDOWS API
#endif // __unix__