#ifndef PSPP_UTILITY_UNIVERSAL_HPP
#define PSPP_UTILITY_UNIVERSAL_HPP

namespace pspp
{
struct universal_type
{
  template <typename T>
  constexpr operator T() const noexcept (noexcept (T{})) { return T{}; }
};

inline constexpr universal_type universal {};

struct unevaluated_universal_type
{
  template <typename T>
  constexpr operator T() const noexcept;
};
} // namespace pspp

#endif // PSPP_UTILITY_UNIVERSAL_HPP
