#ifndef PSPP_PIPE_HPP
#define PSPP_PIPE_HPP

#include <pspp/type_sequence.hpp>
#include <pspp/adaptors.hpp>
#include <pspp/temporary_storage.hpp>
#include <pspp/functional.hpp>

namespace pspp
{

struct pipe_creator;
struct place_holder;


template <typename FuncT, typename ... ArgTs>
struct pipe_;

template <typename PipeArgsT, typename FuncT>
struct piped_pipe_arguments_holder;

template <typename ... ArgTs>
struct pipe_arguments_holder;

template <typename WaitedT, typename ... ArgTs>
struct pipe_arguments_holder_with_waited;

template <typename MemPtrT>
struct piped_member_access;

template <typename T>
concept c_pipe = c_complex_target<T, pipe_>;
  /*std::is_base_of_v<pipe, T>;*/

template <typename T>
concept c_pipe_child = requires {requires std::remove_cvref_t<T>::has_pipe_as_base; };

template <typename T>
concept c_pipe_args = c_complex_target<T, pipe_arguments_holder> or
                      c_complex_target<T, pipe_arguments_holder_with_waited>;

/*

template <auto t_name>
struct named_place_holder
{};


template <typename KeysT, typename ... Ts>
struct named_tmpstorage : tmpstorage<Ts && ...>
{
  using base = tmpstorage<Ts && ...>;

  [[no_unique_address]]
  KeysT _;

  static_prop_of KeysT keys;

  template <auto t_key>
  requires ((keys | get_first_index_of<value_holder<t_key>>) != npos) and (not is_constr(t_key))
  constexpr auto && get_at() noexcept
  {
    return base::template get<keys | get_first_index_of<value_holder<t_key>>>();
  }

  template <constr t_key>
  requires ((keys | get_first_index_of<value_holder<t_key>>) != npos)
  constexpr auto && get_at() noexcept
  {
    return base::template get<keys | get_first_index_of<value_holder<t_key>>>();
  }

  template <auto t_key>
  requires ((keys | get_first_index_of<value_holder<t_key>>) != npos) and (not is_constr(t_key))
  constexpr auto const & get_at() const noexcept
  {
    return base::template get<keys | get_first_index_of<value_holder<t_key>>>();
  }

  template <constr t_key>
  requires ((keys | get_first_index_of<value_holder<t_key>>) != npos)
  constexpr auto const & get_at() const noexcept
  {
    return base::template get<keys | get_first_index_of<value_holder<t_key>>>();
  }

  template <auto t_key>
  requires ((keys | get_first_index_of<value_holder<t_key>>) != npos) and (not is_constr(t_key))
  constexpr auto & get_ref_at() noexcept
  {
    return base::template get<keys | get_first_index_of<value_holder<t_key>>>();
  }

  template <constr t_key>
  requires ((keys | get_first_index_of<value_holder<t_key>>) != npos)
  constexpr auto & get_ref_at() noexcept
  {
    return base::template get<keys | get_first_index_of<value_holder<t_key>>>();
  }

  template <auto t_key>
  requires ((keys | get_first_index_of<value_holder<t_key>>) != npos) and (not is_constr(t_key))
  constexpr auto const & get_ref_at() const noexcept
  {
    return base::template get<keys | get_first_index_of<value_holder<t_key>>>();
  }

  template <constr t_key>
  requires ((keys | get_first_index_of<value_holder<t_key>>) != npos)
  constexpr auto const & get_ref_at() noexcept
  {
    return base::template get<keys | get_first_index_of<value_holder<t_key>>>();
  }
};

template <typename ... Ts, typename KeyTs>
  requires (KeyTs{} | is_unique)
named_tmpstorage(tmpstorage<Ts...>, KeyTs) ->
  named_tmpstorage<KeyTs, Ts && ...>;



template <auto t_name, typename DataT>
struct named_waited_value
{
  [[no_unique_address]]
  value_holder<t_name> _;

  tmpstorage<DataT &&> value;

  static_val name = t_name;
  using value_type = DataT;
};

template <typename LastWaitedT, typename KeyTs, typename ... PrevTs>
struct named_waited_value_sequence
{
  static_prop_of KeyTs keys {};

  tmpstorage<LastWaitedT &&> last;
  named_tmpstorage<KeyTs, PrevTs && ...> prev;
};



template <auto t_ind>
constexpr named_place_holder<t_ind> save_as() noexcept
{ return {}; }

template <constr t_name>
constexpr named_place_holder<t_name> save_as() noexcept
{ return {}; }

template <typename ValueT, auto t_name>
constexpr auto operator|(ValueT && value, named_place_holder<t_name>)
{
  return named_waited_value<t_name, ValueT &&>{{}, tmpstorage{std::forward<ValueT>(value)}};
}

template <typename T>
concept c_named_waited_value = c_complex_vttarget<T, named_waited_value>;


template <typename ValueT, c_named_waited_value NamedT>
constexpr auto operator|(ValueT && value, NamedT && named) noexcept
{
  using named_type = std::remove_cvref_t<NamedT>;
  return named_waited_value_sequence<
    ValueT &&,
    value_sequence<named_type::name>,
    typename named_type::value_type &&
    >
  {
     tmpstorage{std::forward<ValueT>(value)},
    {tmpstorage{std::forward<NamedT>(named).value}, {}}
  };
}*/

/*template <c_complex_vttarget<named_waited_value> PrevT, auto t_new_name>
constexpr auto operator|(PrevT && prev, named_place_holder<t_new_name>)
{
  return named_waited_value_sequence<
  >;
}*/
/*
template <typename KeysT, typename ... Ts>
struct named_pipe_arguments_holder :
  tmpstorage<Ts && ...>
{
  using base = tmpstorage<Ts && ...>;
  static_prop_of KeysT keys {};
};

template <auto t_ind>
inline constexpr named_place_holder<t_ind>  indexed_place_holder_v {};

template <constr t_name>
inline constexpr named_place_holder<t_name> named_place_holder_v {};

template <char ... t_cs>
constexpr auto operator""_x() noexcept
{
  return named_place_holder<
    operator""_c<t_cs...>().value
  >{};
}

template <char ... t_cs>
constexpr auto operator""_() noexcept
{
  return named_place_holder<
    operator""_c<t_cs...>().value
  >{};
}

template <constr t_str>
constexpr auto operator""_x() noexcept
{ return named_place_holder<t_str>{}; }

template <constr t_str>
constexpr auto operator""_() noexcept
{ return named_place_holder<t_str>{}; }
*/



using pipe_operation_pipe_creator =
  operation_pipe_creator<
    operation_holder,
    operation_pipe_preprocessors<>{},
    []<typename OperationHolder>(auto && self, auto && value)
    {
      if constexpr (OperationHolder::pos == operation_pos::left)
        return pipe_{
          OperationHolder::raw_operation,
          std::forward<decltype(self)>(self),
          std::forward<decltype(value)>(value)
        };
      else if constexpr (OperationHolder::pos == operation_pos::right)
        return pipe_{
          OperationHolder::raw_operation,
          std::forward<decltype(value)>(value),
          std::forward<decltype(self)>(self)
        };
      else return pipe_{
          OperationHolder::raw_operation,
          std::forward<decltype(self)>(self)
        };
    }
  >;


struct piped_member_access_fnt
{
  template <typename ParentT, c_member_ptr MemberPtrT>
  pspp_always_inline
  constexpr auto operator()(ParentT && parent, MemberPtrT && mptr) const noexcept -> decltype(auto)
  {
    if constexpr (c_member_function_pointer<MemberPtrT>)
      if constexpr (requires {(std::forward<ParentT>(parent).*auto{std::forward<MemberPtrT>(mptr)})();})
        return (std::forward<ParentT>(parent).*auto{std::forward<MemberPtrT>(mptr)})();
      else
        return (std::forward<ParentT>(parent)->*auto{std::forward<MemberPtrT>(mptr)})();
    else if constexpr (requires {std::forward<ParentT>(parent).*auto{std::forward<MemberPtrT>(mptr)};})
      return std::forward<ParentT>(parent).*auto{std::forward<MemberPtrT>(mptr)};
    else return
        std::forward<ParentT>(parent)->*auto{std::forward<MemberPtrT>(mptr)};
  }

  template <typename ParentT, c_callable MemberPtrGetterT>
  pspp_always_inline
  constexpr auto operator()(ParentT && parent, MemberPtrGetterT && getter) const noexcept -> decltype(auto)
  {
    return operator()(std::forward<ParentT>(parent),
      std::forward<MemberPtrGetterT>(getter).template operator()<std::remove_cvref_t<ParentT>>());
  }
};

template <typename MemPtrT>
struct piped_member_function_call
{
  MemPtrT m_ptr;

  template <typename ParentT, typename ... ArgTs>
  constexpr auto operator()(ParentT && parent, ArgTs && ... args) const noexcept -> decltype(auto)
  {
    if constexpr (c_member_ptr<MemPtrT>)
    {
      if constexpr (requires {(std::forward<ParentT>(parent).*m_ptr)(std::forward<ArgTs>(args)...);})
        return (std::forward<ParentT>(parent).*m_ptr)(std::forward<ArgTs>(args)...);
      else return (std::forward<ParentT>(parent)->*m_ptr)(std::forward<ArgTs>(args)...);
    }
    else if constexpr (c_callable<MemPtrT>)
    {
      auto ptr = m_ptr.template operator()<std::remove_cvref_t<ParentT>>();

      return piped_member_function_call<decltype(auto{ptr})>{
        ptr
        }(std::forward<ParentT>(parent), std::forward<ArgTs>(args)...);
    }
  }
};

template <typename MemPtrT>
piped_member_function_call(MemPtrT &&) ->
  piped_member_function_call<MemPtrT>;


template <typename MemPtrT>
struct piped_member_access :
  pipe_<
    piped_member_access_fnt,
    place_holder, MemPtrT
  >
{
  using base = pipe_<
    piped_member_access_fnt,
    place_holder, MemPtrT
  >;

  constexpr piped_member_access(auto && p_, MemPtrT && mptr):
    base{piped_member_access_fnt{}, auto{p_}, std::forward<MemPtrT>(mptr)}
  {}
};

template <typename MemPtrT>
piped_member_access(auto, MemPtrT &&) ->
  piped_member_access<MemPtrT &&>;


struct place_holder :
  pipe_operation_pipe_creator
{
  template <typename AccessorT>
  constexpr auto operator->*(AccessorT && accessor) const noexcept
  {
    return piped_member_access{
      place_holder{},
      std::forward<AccessorT>(accessor)
    };
  }

  template <typename AccessorFunc>
    requires (not c_member_ptr<AccessorFunc>)

  constexpr auto operator!() const noexcept
  { return place_holder{}; }

  constexpr auto operator~() const noexcept
  { return place_holder{}; }

  constexpr auto operator-() const noexcept
  {
    return place_holder{};
  }

  constexpr auto operator--() const noexcept
  {
    return place_holder{};
    //return pipe_arguments_holder<type_sequence<place_holder>>{place_holder{}};
  }
};


template <typename PipeArgsT, typename FuncT>
struct piped_pipe_arguments_holder :
  tmpstorage<PipeArgsT &&, FuncT &&>
{
  using base = tmpstorage<PipeArgsT &&, FuncT &&>;


  constexpr piped_pipe_arguments_holder(PipeArgsT && pa_, FuncT && func) :
    base{std::forward<PipeArgsT>(pa_), std::forward<FuncT>(func)}
  {}



  /*
  constexpr piped_pipe_arguments_holder() = default;
  constexpr piped_pipe_arguments_holder(piped_pipe_arguments_holder &&) = default;
  constexpr piped_pipe_arguments_holder(piped_pipe_arguments_holder const &) = default;
   */
};

piped_pipe_arguments_holder(auto && args, auto && func) ->
  piped_pipe_arguments_holder<
    decltype(args),
    decltype(func)
  >;


using piped_pipe_arguments_holder_creator =
  operation_pipe_creator
  <
    operation_holder,
    operation_pipe_preprocessors<>{},
    []<typename OpHolder>(auto && self, auto && right)
    {
      return piped_pipe_arguments_holder{
        std::forward<decltype(self)>(self),
        pipe_{OpHolder::raw_operation, place_holder{}, std::forward<decltype(right)>(right)}
      };
    }
  >;

template <typename ... ArgTs>
struct pipe_arguments_holder :
  piped_pipe_arguments_holder_creator
{
  using types = type_sequence<ArgTs...>;

  typename decltype(
    types{} | pass_to<tmpstorage>
  )::t args;

  constexpr pipe_arguments_holder(pipe_arguments_holder const &) = default;
  constexpr pipe_arguments_holder(pipe_arguments_holder &&) = default;

  template <typename FirstT_, typename ... ArgTs_>
  constexpr pipe_arguments_holder(FirstT_ && first, ArgTs_ && ... args_) noexcept (
    noexcept (std::forward<FirstT_>(first)) and
    noexcept ((..., std::forward<ArgTs_>(args_)))
    )
    requires (not c_complex_target<FirstT_, pipe_arguments_holder>)
  :
    piped_pipe_arguments_holder_creator{},
    args{std::forward<FirstT_>(first), std::forward<ArgTs_>(args_)...}
  {}
};

template <typename ... ArgTs>
struct forbid_automatic_operators_overload_for<
  pipe_arguments_holder<ArgTs...>
  > : enable_ops<less_equal_t, greater_t>
{};


template <typename FirstT, typename ... Ts>
pipe_arguments_holder(FirstT &&, Ts && ...) ->
  pipe_arguments_holder<FirstT &&, Ts && ...>;



template <typename WaitedT, typename ... ArgTs>
struct pipe_arguments_holder_with_waited :
  pipe_arguments_holder<ArgTs...>,
  tmpvalue_container<WaitedT, 0>
{
  using value_holder = tmpvalue_container<WaitedT, 0>;

  using value_holder::m_value;

  constexpr auto && get() noexcept
  { return value_holder::get(val<0ul>); }

  constexpr auto && get() const noexcept
  { return value_holder::get(val<0ul>); }


  template <typename ArgsHolderT>
  constexpr pipe_arguments_holder_with_waited(WaitedT && waited, ArgsHolderT && argsholder) noexcept :
    pipe_arguments_holder<ArgTs...>{std::forward<ArgsHolderT>(argsholder)},
    value_holder{std::forward<decltype(waited)>(waited)}
  {}
};


template <typename WaitedT, typename ... ArgTs>
pipe_arguments_holder_with_waited(WaitedT &&, pipe_arguments_holder<ArgTs...> &&) ->
  pipe_arguments_holder_with_waited<
    WaitedT, ArgTs...
  >;

template <typename WaitedT, typename ... ArgTs>
pipe_arguments_holder_with_waited(WaitedT &&, pipe_arguments_holder<ArgTs...> const &) ->
  pipe_arguments_holder_with_waited<
    WaitedT, ArgTs...
  >;

template <typename WaitedT, typename ... ArgTs>
pipe_arguments_holder_with_waited(WaitedT &&, pipe_arguments_holder<ArgTs...> &) ->
  pipe_arguments_holder_with_waited<
    WaitedT, ArgTs...
  >;

template <c_pipe_child PipeT>
struct forbid_automatic_operators_overload_for<PipeT>:
  enable_ops<greater_t, less_equal_t>
{};

// pipe(1, x * 2 + 3,

template <c_pipe_child PipeT>
constexpr auto && operator--(PipeT && p_, int) noexcept
{ return std::forward<PipeT>(p_); }

template <c_pipe_args PipeArgsT>
constexpr auto && operator--(PipeArgsT && pa_, int) noexcept
{ return std::forward<PipeArgsT>(pa_); }

constexpr auto operator--(place_holder, int) noexcept
{
  return pipe_arguments_holder<place_holder>{place_holder{}};
}


template <typename FuncT, typename ... ArgTs>
struct pipe_ :
  pipe_operation_pipe_creator,
  range_adaptor_closure
{

  using types = type_sequence<ArgTs...>;

  FuncT func;

  typename decltype(
    types{} | pass_to<tmpstorage>
  )::t args;

  constexpr pipe_() noexcept = default;
  constexpr pipe_(pipe_ const &) = default;
  constexpr pipe_(pipe_ &&) = default;

  template <typename FuncT_, typename ... ArgTs_>
  constexpr pipe_(FuncT_ && func_, ArgTs_ && ... args_) noexcept (
    noexcept (std::forward<FuncT_>(func_)) and
    noexcept ((..., std::forward<ArgTs_>(args_)))
    )
    requires std::convertible_to<FuncT, FuncT_ &&> and (sizeof...(args_) > 1) :

    pipe_operation_pipe_creator(),
    range_adaptor_closure(),
    func(std::forward<FuncT_>(func_)),
    args{std::forward<ArgTs_>(args_)...}
  {}

  template <typename FuncT_, typename ArgT_>
  constexpr pipe_(FuncT_ && func_, ArgT_ && arg_) noexcept (
    noexcept (std::forward<FuncT_>(func_)) and
    noexcept (std::forward<ArgT_>(arg_))
    )
    requires std::convertible_to<FuncT, FuncT_ &&> :

    pipe_operation_pipe_creator(),
    range_adaptor_closure(),
    func(std::forward<FuncT_>(func_)),
    args{std::forward<ArgT_>(arg_)}
  {}

  template <typename DataT, std::size_t t_count_of_required>
  struct waited_value_holder
  {
    std::remove_reference_t<DataT> & ref;

    constexpr DataT && get() noexcept
    {
      if constexpr (t_count_of_required > 1)
        return ref;
      else return std::forward<DataT>(ref);
    }

    constexpr std::remove_reference_t<DataT> & get_ref() noexcept
    { return ref; }

    constexpr std::remove_reference_t<DataT> const & get() const noexcept
    { return ref; }

    constexpr std::remove_reference_t<DataT> const & get_ref() const noexcept
    { return ref; }
  };

  template <typename WaitedT, std::size_t ... t_is>
  constexpr auto impl(WaitedT && waited_value_, std::index_sequence<t_is...>) noexcept
  {
    static constexpr std::size_t count_of_required = types{} | kind_count_of<::pspp::pipe_creator>;

    auto value_getter = [
      waited_value=waited_value_holder<WaitedT, count_of_required>{waited_value_},
      this]<std::size_t t_i, bool t_get_waited_by_ref = false, bool t_just_return_waited = false> mutable -> decltype(auto) {

      if constexpr (t_just_return_waited)
        return waited_value.get();
      else
      {
        using  t_at = pspp_decltype(types{} | at<t_i>);

        if constexpr (c_same_as<t_at, place_holder>)
        {
          if constexpr (t_get_waited_by_ref)
            return waited_value.get_ref();
          else
            return waited_value.get();
        }
        else if constexpr (c_pipe_child<t_at>)
          return std::get<t_i>(args)(waited_value.get());
        else
          return std::get<t_i>(args);
      }
    };

#define _func_call__(by_ref) func(value_getter.template operator()<t_is, by_ref>()...)

    if constexpr (requires { _func_call__(false);})
    {
      using result_type = decltype(_func_call__(false));
      if constexpr (c_same_as<result_type, void>)
      {
        _func_call__(false);
        return value_getter.template operator()<0, false, true>();        // <- just returns waited value properly
      }
      else return _func_call__(false);
    }
    else if constexpr (requires {_func_call__(true);})  // <- for cases when
                                                        //    rvalue is passed when lvalue required
    {
      using result_type = decltype(_func_call__(true));
      if constexpr (c_same_as<result_type, void>)
      {
        _func_call__(true);
        return value_getter.template operator()<0, false, true>();        // <- just returns waited value properly
      }
      else return _func_call__(true);
    }
    else
    {
      static_assert(sizeof(WaitedT) < 0, "function was not callable with passed value");
    }
  }


  template <typename WaitedT, std::size_t ... t_is>
  constexpr auto impl(WaitedT && waited_value_, std::index_sequence<t_is...>) const noexcept
  {
    static constexpr std::size_t count_of_required = types{} | kind_count_of<::pspp::pipe_creator>;

    auto value_getter = [
      waited_value=waited_value_holder<WaitedT, count_of_required>{waited_value_},
      this]<std::size_t t_i, bool t_get_waited_by_ref = false, bool t_just_return_waited = false> mutable -> decltype(auto) {

      if constexpr (t_just_return_waited)
        return waited_value.get();
      else
      {
        using  t_at = pspp_decltype(types{} | at<t_i>);

        if constexpr (c_same_as<t_at, place_holder>)
        {
          if constexpr (t_get_waited_by_ref)
            return waited_value.get_ref();
          else
            return waited_value.get();
        }
        else if constexpr (c_pipe_child<t_at>)
          return std::get<t_i>(args)(waited_value.get());
        else
          return std::get<t_i>(args);
      }
    };

    if constexpr (requires { _func_call__(false);})
    {
      using result_type = decltype(_func_call__(false));
      if constexpr (c_same_as<result_type, void>)
      {
        _func_call__(false);
        return value_getter.template operator()<0, false, true>();        // <- just returns waited value properly
      }
      else return _func_call__(false);
    }
    else if constexpr (requires {_func_call__(true);})  // <- for cases when
      //    rvalue is passed when lvalue required
    {
      using result_type = decltype(_func_call__(true));
      if constexpr (c_same_as<result_type, void>)
      {
        _func_call__(true);
        return value_getter.template operator()<0, false, true>();        // <- just returns waited value properly
      }
      else return _func_call__(true);
    }
    else
    {
      static_assert(sizeof(WaitedT) < 0, "function was not callable with passed value");
    }

#undef _func_call__
  }

  constexpr auto operator()(auto && waited_value) const noexcept
  {
    return impl(std::forward<decltype(waited_value)>(waited_value), std::make_index_sequence<types{} | get_size>{});
  }

  constexpr auto operator()(auto && waited_value) noexcept
  {
    return impl(std::forward<decltype(waited_value)>(waited_value), std::make_index_sequence<types{} | get_size>{});
  }

  static_flag has_pipe_as_base = true;
};

template <c_pipe_child PipeT>
constexpr auto operator--(PipeT && p_) noexcept
{ return std::forward<PipeT>(p_); }

template <typename FuncT, typename ... ArgTs>
pipe_(FuncT &&, ArgTs && ...) ->
  pipe_<FuncT, ArgTs...>;

template <typename FuncT, typename ... ArgTs>
pipe_(FuncT &&, tmpstorage<ArgTs...> &&) ->
  pipe_<FuncT, ArgTs...>;

template <typename FuncT, typename ... ArgTs>
pipe_(FuncT &&, tmpstorage<ArgTs...> const &) ->
  pipe_<FuncT, ArgTs...>;

template <typename FuncT, typename ... ArgTs>
pipe_(FuncT &&, tmpstorage<ArgTs...> &) ->
  pipe_<FuncT, ArgTs...>;




template <typename FuncT>
struct pipe_holder_
{
  FuncT func;

  template <typename ... ArgTs>
  constexpr auto operator()(ArgTs && ... args) const noexcept
  {
    return pipe_{
      std::forward<FuncT>(func), std::forward<ArgTs>(args)...
    };
  }
};

template <typename ValT>
pipe_holder_(ValT && val) -> pipe_holder_<ValT>;

struct pipe_creator
{
  constexpr auto operator!() const noexcept
  { return pipe_creator{}; }

  constexpr auto operator~() const noexcept
  { return pipe_creator{}; }

  /*template <c_callable FuncT, typename ... ArgTs>
  [[deprecated(
    "The syntax: pipe(func_name)(args...) considered to be removed, "
    "because it is hard to distinct case when func_name(other_func_name)."
    "Consider useing pipe[func_name](args...) syntax instead.")]]
  constexpr static auto operator()(FuncT && func, ArgTs && ... args) noexcept
  {
    if constexpr (sizeof...(args) == 0)
      return pipe_holder_{std::forward<FuncT>(func)};
    else return pipe_{
      std::forward<FuncT>(func), std::forward<ArgTs>(args)...
    };
  }
   */

  template <typename ... ArgTs>
  constexpr static auto operator()(ArgTs && ... args) noexcept
  {
    return pipe_arguments_holder<ArgTs &&...>{
        std::forward<ArgTs>(args)...
      };
  }

  template <typename FuncT>
  constexpr static auto operator[](FuncT && func) noexcept
  { return pipe_holder_{std::forward<FuncT>(func)};}
};

struct pipe_arguments_holder_creator
{
  template <typename ... ArgTs>
  pspp_always_inline
  constexpr static auto operator()(ArgTs && ... args) noexcept
    requires (
      not (sizeof...(args) == 1 and
        (
           c_complex_target<decltype(get_first_from_seq(args...)), pipe_arguments_holder> or
           c_complex_target<decltype(get_first_from_seq(args...)), piped_member_access>
        )
      )
   )
  {
    return pipe_arguments_holder<ArgTs &&...>{std::forward<ArgTs>(args)...};
  }

  template <typename ArgT>
  pspp_always_inline
  constexpr static auto operator()(ArgT && arg) noexcept
    requires
      c_complex_target<ArgT, pipe_arguments_holder> or
      c_complex_target<ArgT, piped_member_access>
  { return std::forward<decltype(arg)>(arg); }

  template <typename ... ArgTs>
  pspp_always_inline
  constexpr static auto operator[](ArgTs && ... args) noexcept
  {
    return operator()(std::forward<ArgTs>(args)...);
  }

  pspp_always_inline
  constexpr auto operator--(int) const noexcept
  {
    return operator()(place_holder{});
  }

};


template <typename T>
concept c_pipe_args_with_waited = c_complex_target<T, pipe_arguments_holder_with_waited>;

template <typename T>
concept c_piped_pipe_args = c_complex_target<T, piped_pipe_arguments_holder>;


template <typename FuncT, c_pipe_args PipeArgsT>
  requires (not c_complex_target<FuncT, piped_member_access>)
constexpr auto operator<=(FuncT && func, PipeArgsT && p_) noexcept
{
  return pipe_{
    std::forward<decltype(func)>(func), std::forward<decltype(p_)>(p_).args
  };
}

template <typename FuncT, c_piped_pipe_args PPArgs>
constexpr auto operator<=(FuncT && func, PPArgs && p_) noexcept
{
  return pipe_{
    std::forward<PPArgs>(p_).template get<1>(),
    std::forward<FuncT>(func) <= std::forward<PPArgs>(p_).template get<0>()
  };
}

template <typename FuncT, c_piped_pipe_args PPArgs>
constexpr auto operator<(FuncT && func, PPArgs && p_) noexcept
{
  return std::forward<FuncT>(func) <= std::forward<PPArgs>(p_);
}

template <c_pipe_args PipeArgsT, typename FuncT>
constexpr auto operator>(PipeArgsT && p_, FuncT && func) noexcept
{
  return pipe_{
    std::forward<FuncT>(func), std::forward<PipeArgsT>(p_).args
  };
}

template <typename FuncT, c_pipe_args PipeArgsT>
constexpr auto operator<(FuncT && func, PipeArgsT && p_) noexcept
{
  return pipe_{
    std::forward<FuncT>(func), std::forward<PipeArgsT>(p_).args
  };
}

template <typename FuncT>
constexpr auto operator<(FuncT && func, place_holder) noexcept
{
  return pipe_ {
    std::forward<FuncT>(func), place_holder{}
  };
}

template <typename FuncT>
constexpr auto operator<=(FuncT && func, place_holder) noexcept
{
  return pipe_ {
    std::forward<FuncT>(func), place_holder{}
  };
}

template <c_pipe_child PipeT, typename FuncT>
constexpr auto operator>(PipeT && p_, FuncT && func) noexcept
{
  return pipe_ {
    std::forward<FuncT>(func),
    std::forward<PipeT>(p_)
  };
}

template <typename WaitedValue, c_complex_target<pipe_> PipeT>
constexpr auto operator>(WaitedValue && waited_value, PipeT && pipe_ ) noexcept
{
  return std::forward<PipeT>(pipe_)(std::forward<WaitedValue>(waited_value));
}


/*
constexpr auto operator<<(auto && func, c_pipe_args auto && p_) noexcept
{
  return pipe_{
    std::forward<decltype(func)>(func), std::forward<decltype(p_)>(p_).args
  };
}



constexpr auto operator>>(c_pipe_args auto && p_, auto && func) noexcept
{
  return pipe_{
    std::forward<decltype(func)>(func), std::forward<decltype(p_)>(p_).args
  };
}
*/


inline namespace pipe_declarations
{
inline constexpr pipe_creator pipe {};
inline constexpr pipe_creator p {};
inline constexpr place_holder x {};
inline constexpr pipe_arguments_holder_creator o {};
}


template <typename FuncT, c_pipe_child PipeT>
constexpr auto operator<=(FuncT && func, PipeT && p_) noexcept
{
  if constexpr (requires { p_.to_pipe(); } )
    return std::forward<PipeT>(p_) --> std::forward<FuncT>(func).to_pipe();
  else
    return std::forward<PipeT>(p_) --> std::forward<FuncT>(func);
}

template <typename FuncT>
constexpr auto operator|(place_holder, FuncT && func) noexcept
{ return x --> std::forward<FuncT>(func); }

template <c_pipe_child PipeChildT, typename FuncT>
constexpr auto operator|(PipeChildT && p_, FuncT && func) noexcept
{ return std::forward<PipeChildT>(p_) --> std::forward<FuncT>(func); }

template <c_pipe_args PipeArgsT, typename FuncT>
constexpr auto operator|(PipeArgsT && p_, FuncT && func) noexcept
{
   return std::forward<PipeArgsT>(p_) --> std::forward<FuncT>(func);
}

template <typename WaitedValueT, c_pipe_args PipeArgsT>
constexpr auto operator|(WaitedValueT && waited, PipeArgsT && pa) noexcept
{
  return pipe_arguments_holder_with_waited{std::forward<WaitedValueT>(waited), std::forward<PipeArgsT>(pa)};
}

template <c_pipe_args_with_waited PipeArgsWithWaitedT, typename FuncT>
constexpr auto operator|(PipeArgsWithWaitedT && paw, FuncT && func) noexcept
{
  return std::forward<PipeArgsWithWaitedT>(paw).get()
          | (std::forward<PipeArgsWithWaitedT>(paw) --> std::forward<FuncT>(func));
}

namespace
{


template <
  c_complex_target<pipe_arguments_holder> LeftT,
  c_complex_target<pipe_arguments_holder> RightT
>
constexpr auto concat_arguments_holder(LeftT && left, RightT && right) noexcept
{
  return []<std::size_t ... t_is, std::size_t ... t_js>(
    LeftT&& left_, RightT && right_,
    std::index_sequence<t_is...>, std::index_sequence<t_js...>
  )
  {
    return pipe_arguments_holder{
      std::forward<LeftT>(left_).args.template get<t_is>()...,
      std::forward<RightT>(right_).args.template get<t_js>()...
    };
  }(std::forward<LeftT>(left), std::forward<RightT>(right),
    std::make_index_sequence<std::remove_cvref_t<LeftT >::types::size>{},
    std::make_index_sequence<std::remove_cvref_t<RightT>::types::size>{});
}

template <typename T>
concept c_pipe_comma_operand =
  c_same_as<T, place_holder> or
  c_pipe<T> or
  c_complex_target<T, pipe_arguments_holder> or
  c_complex_target<T, piped_member_access>;

template <typename LeftT, typename RightT>
constexpr auto append_pipe_argument_to_holder(LeftT && left, RightT && right) noexcept (noexcept (
  std::forward<LeftT>(left)
  ) and noexcept (
  std::forward<RightT>(right)
  ))
{
  if constexpr (c_complex_target<LeftT, pipe_arguments_holder> and
                c_complex_target<RightT, pipe_arguments_holder>)
    return concat_arguments_holder(
      std::forward<LeftT>(left),
      std::forward<RightT>(right)
    );
  else if constexpr (c_complex_target<LeftT, pipe_arguments_holder>)
    return concat_arguments_holder(
      std::forward<LeftT>(left),
      pipe_arguments_holder{std::forward<RightT>(right)}
    );
  else if constexpr (c_complex_target<RightT, pipe_arguments_holder>)
    return concat_arguments_holder(
      pipe_arguments_holder{std::forward<LeftT>(left)},
      std::forward<RightT>(right)
      );
  else
    return pipe_arguments_holder{
      std::forward<LeftT>(left),
      std::forward<RightT>(right)
    };
}
}

template <typename LeftArgT, typename RightArgT>
  requires (
    c_pipe_comma_operand<LeftArgT> or
    c_pipe_comma_operand<RightArgT>
  )
  pspp_always_inline
constexpr auto operator,(LeftArgT && left, RightArgT && right) noexcept (noexcept (
  std::forward<LeftArgT>(left)
  ) and noexcept (
  std::forward<RightArgT>(right)
  ))
{
  return append_pipe_argument_to_holder(
    std::forward<LeftArgT>(left),
    std::forward<RightArgT>(right)
  );
}


template <c_complex_target<piped_member_access> PipedMemgerAccessT, typename PipeArgsT>
constexpr auto operator<=(PipedMemgerAccessT && member_access, PipeArgsT && args) noexcept
{
  return
    piped_member_function_call{std::forward<PipedMemgerAccessT>(member_access).args. template get<1>()}
    <= append_pipe_argument_to_holder(place_holder{}, std::forward<PipeArgsT>(args));
}

template <constr>
struct memptr_getter
{};


#define pspp_lambdize(func_name, ...) [](auto && ... args) __VA_ARGS__ { return func_name(std::forward<decltype(args)>(args)...); }

#define pspp_m(memptr_name) []<typename T>{ return &T::memptr_name; }

#ifdef PSPP_ENABLE_MEMBER_GETTER
#define _m(memptr_name) pspp_m(memptr_name)
#endif

#ifdef PSPP_ENABLE_LAMBDIZE
#define L(func_name) pspp_lambdize(func_name)
#endif

} // namespace pspp

#endif // PSPP_PIPE_HPP
