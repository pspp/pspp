#ifndef PSPP_PIPES_HPP
#define PSPP_PIPES_HPP

#include <pspp/pipe.hpp>

namespace pspp
{

inline constexpr struct msize_t
{
  template <typename ValT>
  constexpr auto pspp_static_call_const_operator(ValT && val) noexcept (noexcept(
    std::forward<ValT>(val).size()
  ))-> decltype(auto)
  { return std::forward<ValT>(val).size(); }
} msize {};

inline constexpr struct mbeg_t 
{
	template <typename ValT>
	constexpr auto pspp_static_call_const_operator(ValT && val) noexcept (noexcept(
    std::forward<ValT>(val).begin()
  ))-> decltype(auto)
	{ return std::forward<ValT>(val).begin(); }
} mbeg {};

inline constexpr struct mend_t
{
	template <typename ValT>
	constexpr auto pspp_static_call_const_operator(ValT && val) noexcept (noexcept(
    std::forward<ValT>(val).end()
  )) -> decltype(auto)
	{ return std::forward<ValT>(val).end(); }

} mend {};

inline constexpr struct mfront_t
{
  template <typename ValT>
  constexpr auto pspp_static_call_const_operator(ValT && val) noexcept (noexcept(
    std::forward<ValT>(val).front()
  )) -> decltype(auto)
  { return std::forward<ValT>(val).front(); }
} mfront {};

inline constexpr struct mback_t
{
  template <typename ValT>
  constexpr auto pspp_static_call_const_operator(ValT && val) noexcept (noexcept(
    std::forward<ValT>(val).back()
  )) -> decltype(auto)
  { return std::forward<ValT>(val).back(); }
} mback {};

inline constexpr struct mdata_t
{
  template <typename ValT>
  constexpr auto pspp_static_call_const_operator(ValT && val) noexcept (noexcept(
    std::forward<ValT>(val).data()
  )) -> decltype(auto)
  { return std::forward<ValT>(val).data(); }
} mdata {};

inline constexpr struct mcstr_t
{
  template <typename ValT>
  constexpr auto pspp_static_call_const_operator(ValT && val) noexcept (noexcept(
    std::forward<ValT>(val).data()
  )) -> decltype(auto)
  { return std::forward<ValT>(val).data(); }
} mcstr {};


inline constexpr pipe_<mbeg_t,    place_holder>      x_begin     {mbeg,    place_holder{}};
inline constexpr pipe_<msize_t,   place_holder>      x_size      {msize,   place_holder{}};
inline constexpr pipe_<mend_t,    place_holder>      x_end       {mend,    place_holder{}};
inline constexpr pipe_<mfront_t,  place_holder>      x_front     {mfront,  place_holder{}};
inline constexpr pipe_<mback_t,   place_holder>      x_back      {mback,   place_holder{}};


inline constexpr pipe_arguments_holder<
    pipe_<mbeg_t, place_holder>,
    pipe_<mend_t, place_holder>
> x_range {auto{x_begin}, auto{x_end}};


} // namespace pspp

#endif // PSPP_PIPES_HPP
