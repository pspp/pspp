
# pspp::pipe

Pipe "operator" implementation.

## Example

```c++
#include <pspp/pipe.hpp>

using namespace pspp::pipe_declarations;

constexpr int sum3(int a, int b, int c)
{ return a + b + c; }

constexpr int mult2(int a)
{ return a * 2; }

static_assert(
  4
   | sum3 <= (2, x + 2, x | mult2)
   | mult2 <= x
  == 
   mult2(sum3(2, 4 + 2, mult2(4)))
);

```

### What can you do:

You can use pipe in several ways:

```c++
value
  | func <= x;         // when function takes only placeholder
  
value
  | x --> func;        // same as above
  
value
  | func <- x;         // same as above but do not work with pure functions
  
value
  | func <-- x         // same as above; -- can be used as mach as wanted
  | func <-------- x;
  
value  
  | func <= (x, 1);    // when function takes several arguments
  
value
  | (x | func1 | func2, x) --> func;

value
  | func <= ((x --> func1) --> func2, x);
  
value
  | pipe[func](x, 1);   // same as above
  
value
  | func <= (x, 1) * 2; // equal func(x, 1) * 2
  
value  
  | func <= (x, 1)       // same as above
  | x * 2;

// but: 

value
  | func <=o (x) * 2     // equal func(x) * 2
!=    
value
  | func <=  (x) * 2     // equal func(x * 2)
```

### What you can not do with this library:

#### Using overloaded functions:

```c++
void foo(auto) {...}

value | foo <= x; // error

```

##### solution:

```c++
/*constexpr*/ auto foo_ = [](auto && arg){ return foo(std::forward<decltype(arg)>(arg)) };
/*constexpr*/ auto foo__ = pspp_lambdize(foo); // ! macro usage

value | foo_  <= x; // ok
value | foo__ <= x; // ok

```

#### Accessing results member methods

```c++

value
  | func <= (x.a); // error; place_holder has no such a member

```

##### solution:

```c++
struct S
{ int a;};

constexpr auto s_a = &S::a;

value
  | func <= (x->*s_a);

value
  | func <= (x->*&S::a);

value
  | func <= (x->*[]<typename T>{ return &T::a; });

value
  | func <= (x->*pspp_m(a));

//if before including pipe.hpp PSPP_ENABLE_MEMBER_GETTER was defined:

value
  | func <= (x->*_m(a));

//or just:

value
  | func <= (x | [](S s){ return s.a; });

```

This solution also works with member functions without arguments.
If you want to use some member function with arguments you need:

```c++
struct S
{
  int a;
  
  int get_with(int b) const { return a + b; }
};

S{2} 
  | func <= (x->*_m(get_with) <= 3); // equal to: func(S{2}.get_with(3));
                                     // any other way to access S::get_with can be provided
  
```


#### Using `<=` not as "pipe" operator but as less_equal

This is also relevant to others operators which is used to make "pipe" able.

##### solution:

```c++

S{2}
  | func <= (pspp::less_equal <= (x, 2));

// or

S{2}
  | func <= (std::less_equal{} <= (x, 2));

// or any other lambda

```


