#ifndef PSPP_INPUT_CONVERTER_HPP
#define PSPP_INPUT_CONVERTER_HPP

#include <pspp/input_converter/basic.hpp>
#include <pspp/input_converter/conversion_result.hpp>
#include <pspp/input_converter/conversion_context.hpp>
#include <pspp/input_converter/conversion_handler.hpp>
#include <pspp/input_converter/conversion_info.hpp>

#include <pspp/data_holder.hpp>
#include <pspp/utility/declaration_macros.hpp>
#include <pspp/struct_info.hpp>
#include <pspp/functional.hpp>
#include <pspp/temporary_storage.hpp>
#include <pspp/array.hpp>
#include <format>
#include <cstdio>

namespace pspp
{

// TODO: make default value/error handler call more meaningfull:
//        * make check if handler is callable and if not try to find callable one at previous conversion etap


// Handler


template <typename T>
struct conversion_specs
{};

using namespace ::pspp::constr_literal;

struct no_argument_error_ind {};

namespace detail
{

using convarg_t = std::optional<std::string_view>;

template <typename ToConvertT>
struct conversion_helpers
{
  template <typename ... TArgTs, c_conversion_context ContextT, c_conversion_info InfoT, typename InputT, typename ... ArgTs>
  static constexpr auto handle_no_arg(ContextT context, InfoT && info,
                                            InputT && input, ArgTs && ... args)
    noexcept (std::remove_cvref_t<InfoT>::is_noexcept)
  {
    using info_ = std::remove_cvref_t<InfoT>;
    static constexpr auto tmp_targs = []{
      if constexpr (sizeof...(TArgTs) == 0)
        return typeseq<no_argument_error_ind>;
      else return typeseq<TArgTs...>;
    }();

    if constexpr (
      convinfo_impl::can_call_from_info<
        conversion_handler_ind::default_value,
        InfoT,
        ContextT,
        InputT
      >(tmp_targs, typeseq<ArgTs...>)
      )
    {
      if constexpr (sizeof...(TArgTs))
        return std::forward<InfoT>(info).template call<
          conversion_handler_ind::default_value, TArgTs...
        >(
          context, input, std::forward<ArgTs>(args)...
        );
      else
        std::forward<InfoT>(info).template call<
          conversion_handler_ind::default_value,
          no_argument_error_ind
        >(
          context, input, std::forward<ArgTs>(args)...
        );
    }
    else
    {
      using namespace constr_literal;

      static constexpr auto error_msg = "error while conevrting input to: "_cs +
                                        type_name_constr<ToConvertT> + "; No input provided";

      if constexpr (is_conv_noexcept<InfoT>)
      {
        if constexpr (std::remove_cvref_t<InfoT>::has_error_handler())
        {
          if constexpr (sizeof...(TArgTs))
            return std::forward<InfoT>(info).template call<
              conversion_handler_ind::error, TArgTs...
            >(context, input, std::forward<ArgTs>(args)...);
          else
            return std::forward<InfoT>(info).template call<
              conversion_handler_ind::error, no_argument_error_ind
            >(context, input, std::forward<ArgTs>(args)...);
        }
        else
        {
          std::puts(error_msg.data());
          std::exit(EXIT_FAILURE);

          return no_return;
        }
      }
      else
      {
        throw std::runtime_error(error_msg.str());
        return no_return;
      }
    }
    std::unreachable();
  }
};

template <typename T>
struct simple_converter
{
  template <c_conversion_info InfoT, c_conversion_context ContextT>
  static constexpr auto convert(conversion_input data, InfoT &&, ContextT)
    noexcept (true) -> conversion_result<T>
  { return {T{data.front()}}; }
};

}

template <typename T>
  requires (std::integral<T> or std::floating_point<T>)
struct conversion_specs<T>
{
  template <c_conversion_context ContextT, c_conversion_info InfoT>
  [[nodiscard]]
  static constexpr auto convert(conversion_input input, InfoT && info, ContextT context)
    noexcept (is_conv_noexcept<InfoT>) ->
    conversion_result<T>
  {
    T res {};

    const std::string_view data = input.front();
    auto [ptr, ec] = std::from_chars(data.begin(), data.end(), res);

    if (ec != std::errc{})
      return info.template call<conversion_handler_ind::error>(context, res, input, ptr, ec);
    else return {res, 1};
  }
};

template <>
struct conversion_specs<std::string> :
    detail::simple_converter<std::string>
{};

template <>
struct conversion_specs<std::string_view> :
    detail::simple_converter<std::string_view>
{};

namespace {
inline constexpr struct deduce_constructor_usage_t
{
  template <typename ... Ts>
  constexpr auto pspp_static_call_const_operator() noexcept
  { return requires { T(std::declval<Ts>()...); }; }
} deduce_constructor_usage;
}

template <typename T, c_complex_target<type_sequence> auto t_ts, bool t_use_constructor =
    t_ts | pass_and_execute<deduce_constructor_usage>
  >
struct known_types_struct_conversion_spec
{
  static_val ts = t_ts;
  static_size size = t_ts.size;

  pspp_always_inline
  static constexpr auto convert(auto && ... values) noexcept
  {
    if constexpr (t_use_constructor)
      return T(pspp_fwd(values)...);
    else return T{pspp_fwd(values)...};
  }
};

template <typename ... Conts>
struct conversion_specs<custom_struct<Conts...>> :
    known_types_struct_conversion_spec<custom_struct<Conts...>,
      typeseq<typename Conts::value_type...>, false>
{};

template <template <typename, auto> class RangeT, typename DataT, std::size_t t_n>
struct simple_static_size_range_conversion_spec
{
  using value_type = DataT;
  static_size size = t_n;

  pspp_always_inline
  static constexpr auto convert(std::same_as<DataT> auto && ... values) noexcept
  { return RangeT<DataT, t_n>{pspp_fwd(values)...}; }
};

template <typename DataT, std::size_t t_n>
struct conversion_specs<std::array<DataT, t_n>> :
    simple_static_size_range_conversion_spec<std::array, DataT, t_n>
{};


template <typename DataT, std::size_t t_n>
struct conversion_specs<array<DataT, t_n>> :
  simple_static_size_range_conversion_spec<array, DataT, t_n>
{};


template <typename DataT, std::size_t t_n>
struct conversion_specs<raw_array_t<DataT, t_n>> :
    simple_static_size_range_conversion_spec<raw_array_t, DataT, t_n>
{};

struct dynamic_range_size_spec
{ static_size size = npos; };

template <typename DataT, typename AllocatorT>
struct conversion_specs<std::vector<DataT, AllocatorT>> :
    dynamic_range_size_spec
{
  using value_type = DataT;
  using vector_type = std::vector<DataT, AllocatorT>;

  pspp_always_inline
  static constexpr void prepare(vector_type & d, std::size_t size) noexcept
  { d.reserve(size); d.shrink_to_fit(); }

  template <c_same_as<DataT> ValueT>
  pspp_always_inline
  static constexpr void add_value(vector_type & d, ValueT && value) noexcept
  { d.emplace_back(std::forward<ValueT>(value)); }
};


template <c_with_known_struct_info T>
struct conversion_specs<T> :
   known_types_struct_conversion_spec<T, struct_info<T>::types>
{};


namespace convdet
{
template <typename T>
concept c_range_has_static_size =
    requires { requires conversion_specs<T>::size != npos; };

template <typename T>
concept c_variadic_range = requires { requires conversion_specs<T>::size == npos; };

template <typename T>
concept c_range_has_prepare_step =
    requires (T & t, std::size_t s){ conversion_specs<T>::prepare(t, s); };

template <typename T>
concept c_range_need_pos_to_add_value =
    requires (T & t, std::ranges::range_value_t<T> && val, std::size_t pos)
    { conversion_specs<T>::add_value(t, val, pos); };

template <typename T>
concept c_range_can_add_one_value =
  requires (T & t, conversion_specs<T>::value_type val)
  { conversion_specs<T>::add_value(t, val); };

template <typename T>
concept c_range_can_save_all_at_once = requires {
  conversion_specs<T>::convert(
    std::declval<typename conversion_specs<T>::value_type>()
  );
};

template <typename T>
concept c_range_need_to_be_constructed = requires {
  conversion_specs<T>::construct();
};

template <typename T>
concept c_struct_with_known_types = requires {
  requires c_complex_target<decltype(conversion_specs<T>::ts), type_sequence>;
};

template <typename T>
struct conversion_specs_context
{
  static consteval auto get_context() noexcept
  {
    if constexpr (c_struct_with_known_types<T>)
    {
      return conversion_specs<T>::ts
               | pass_as_indexseq<
                 []<std::size_t ... t_is>(std::index_sequence<t_is...>)
                 {
                   return typeseq<
                     decltype(conversion_specs<T>::ts | subseq<0, t_is + 1>)...
                     >;
                 }
               >;
    }
    else if constexpr (c_range_has_static_size<T>)
      return
        []<std::size_t ... t_is>(std::index_sequence<t_is...>)
        {
          return typeseq<nth_conv_type<typename conversion_specs<T>::value_type, t_is>...>;
        }(std::make_index_sequence<conversion_specs<T>::size>{});

    else if constexpr (c_variadic_range<T>)
      return typeseq<nth_conv_type<typename conversion_specs<T>::value_type, npos>>;
    else return typeseq<T>;
  }

  using type = extract_type<get_context()>;
};

using my_struct_ = enumerated_custom_struct_fromv<typeseq<int, char, std::array<int, 2>>>;


template <typename T>
struct count_of_arguments_needed
{
  template <std::size_t t_sum = 0>
  static consteval std::size_t get_naive_count() noexcept
  {
    if constexpr (c_struct_with_known_types<T>)
    {
      return conversion_specs<T>::ts | and_execute |
             []<typename ... Ts>
             {
               return (t_sum + ... + count_of_arguments_needed<Ts>::naive_count);
             };
    }
    else if constexpr (c_range_has_static_size<T>)
      return count_of_arguments_needed<typename conversion_specs<T>::value_type>::naive_count *
        conversion_specs<T>::size;
    else if constexpr (c_variadic_range<T>)
      return 0;
    else return 1;
  }

  static_size naive_count = get_naive_count();
};

template <typename T>
concept c_range = requires { typename conversion_specs<T>::value_type; };

template <typename T, typename InfoT, typename ContextT>
concept c_known_how_to_convert = requires (conversion_input input, InfoT && info, ContextT context)
  { { conversion_specs<T>::convert(input, std::forward<InfoT>(info), context) } -> std::same_as<conversion_result<T>>; };

}

// TODO: consider moving from shrink_head system to index for maybe better paralel handling.

template <
    typename ResultT
    >
struct convert_for_fnt
{
  using info_type = conversion_specs<std::remove_cvref_t<ResultT>>;
  static_size naive_count = convdet::count_of_arguments_needed<ResultT>::naive_count;


  template <
    c_complex_target<data_holder> DataHolderT,
    typename InfoT = conversion_info<no_except_t>,
    typename ContextT = conversion_context<>
    >
  pspp_always_inline [[nodiscard]]
  constexpr ResultT pspp_static_call_const_operator(DataHolderT & strings_range, InfoT && info = InfoT{}, ContextT context = ContextT{})
    noexcept (true)
  {
    if constexpr (std::is_const_v<std::remove_reference_t<DataHolderT>>)
    {
      std::remove_cvref_t<DataHolderT> tmp = auto{strings_range};
      return convert_impl(tmp, std::forward<InfoT>(info), context);
    }
    else
      return convert_impl(strings_range, std::forward<InfoT>(info), context);
  }

  template <
    typename InfoT = conversion_info<no_except_t>,
    typename ContextT = conversion_context<>
  >
  pspp_always_inline [[nodiscard]]
  constexpr ResultT pspp_static_call_const_operator(std::string_view str, InfoT && info = {}, ContextT context = {})
    noexcept (true)
  {
    auto tmp_ = data_holder<std::string_view const>(&str, 1);
    return convert_impl(tmp_, std::forward<InfoT>(info), context);
  }

  template <
    typename InfoT = conversion_info<no_except_t>,
    typename ContextT = conversion_context<>
  >
  pspp_always_inline [[nodiscard]]
  constexpr ResultT pspp_static_call_const_operator(char const * const str, InfoT && info = {}, ContextT context = {})
  {
    return operator()(std::string_view{str}, std::forward<InfoT>(info), context);
  }

private:
  struct convert_known_struct_t
  {
    template <typename ... Ts, c_conversion_context ContextT, c_conversion_info InfoT>
    constexpr auto pspp_static_call_const_operator(data_holder<std::string_view const> & dh,
                                                   InfoT && info, ContextT context,
                                                   type_sequence<Ts...> = {})
      noexcept (std::remove_cvref_t<InfoT>::is_noexcept)
    {
      return [&dh, context]<std::size_t ... t_is>(InfoT && info, std::index_sequence<t_is...>){

        // to garantee evaulation order
        tmpstorage tmp_ {
          convert_for_fnt<Ts>{}(dh, std::forward<InfoT>(info), context + nth_conv_type<Ts, t_is>{})...
        };

        return
        info_type::convert(
          tmp_.template get<t_is>()...
        );
      }(std::forward<InfoT>(info), std::make_index_sequence<sizeof...(Ts)>{});
    }

    template <
      typename T, std::size_t ... t_is, c_conversion_info InfoT,
      c_conversion_context ContextT, typename DataT = nullt const
    >
    constexpr auto pspp_static_call_const_operator(data_holder<std::string_view const> & dh,
                                                   std::index_sequence<t_is...>,
                                                   InfoT && info, ContextT context,
                                                   DataT & range = nulltv
                                                  )
    {
      static constexpr auto one_value_convertor =
        [context]<std::size_t t_i>
        (data_holder<std::string_view const> & dh, value_holder<t_i>, InfoT && info) {
          return convert_for_fnt<T>{}(dh, std::forward<InfoT>(info), context + nth_conv_type<T, t_i>{});
        };

      if constexpr (convdet::c_range_can_save_all_at_once<ResultT>)
      {
        tmpstorage tmp_ {one_value_convertor(dh, val<t_is>, std::forward<InfoT>(info))...};
        return info_type::convert(tmp_.template get<t_is>()...);
      }
      else
      {
        (range_value_adder(range, one_value_convertor(dh, val<t_is>, std::forward<InfoT>(info)), t_is), ...);
      }
    }
  };

  inline static constexpr convert_known_struct_t convert_known_struct {};


  template <c_conversion_info InfoT>
  static constexpr auto range_constructor([[maybe_unused]] std::size_t count = 0) noexcept (InfoT::is_noexcept)
  {
    if constexpr (convdet::c_range_need_to_be_constructed<ResultT>)
    {
      if constexpr (convdet::c_range_has_prepare_step<ResultT>)
      {
        auto tmp = info_type::construct();
        info_type::prepare(tmp, count);

        return tmp;
      }
      else return info_type::construct();
    }
    else return ResultT{};
  }

  inline static constexpr struct range_value_adder_t
  {
    template <c_conversion_info InfoT, typename ValueT>
    pspp_always_inline
    constexpr void pspp_static_call_const_operator(ResultT & range, ValueT && val, [[maybe_unused]] std::size_t pos)
      noexcept (InfoT::is_noexcept)
      requires convdet::c_range_can_add_one_value<ResultT>
    {
      if constexpr (convdet::c_range_need_pos_to_add_value<ResultT>)
        info_type::add_value(range, std::forward<ValueT>(val), pos);
      else info_type::add_value(range, std::forward<ValueT>(val));
    }
  } range_value_adder {};


  template <bool t_has_default, typename InputDataT, c_conversion_context ContextT, c_conversion_info InfoT>
  static constexpr auto convert_static_sized_range_impl(data_holder<InputDataT> & input, InfoT && info, ContextT context)
    noexcept (std::remove_cvref_t<InfoT>::is_noexcept)
  {
    using value_type = typename info_type::value_type;
    static constexpr auto is = std::make_index_sequence<info_type::size>{};

    if constexpr (t_has_default)
    {
      if (input.size() < info_type::size)
        return convinfo_impl::call_from_info<
          conversion_handler_ind::default_value,
          no_argument_error_ind
          >(std::forward<InfoT>(info), context, input);
    }

    if constexpr (convdet::c_range_can_save_all_at_once<ResultT>)
    {
      return convert_known_struct.template
        operator()<value_type>(input, is, std::forward<InfoT>(info), context);
    }
    else
    {
      // TODO: redudant?

      return [&input, context](InfoT && info){
        ResultT res = range_constructor<std::remove_cvref_t<InfoT>>(input.size());

        convert_known_struct.template
          operator()<value_type>(input, is, std::forward<InfoT>(info), context, res);

        return res;
      }(std::forward<InfoT>(info));
    }
  }

  template <bool t_has_default, typename InputDataT, c_conversion_info ConvInfoT, c_conversion_context ContextT>
  static constexpr auto convert_range_impl(data_holder<InputDataT> & input,
                                           ConvInfoT && convinfo, ContextT & context
                                           ) noexcept (std::remove_cvref_t<ConvInfoT>::is_noexcept)
    requires (not pspp_is_same(InputDataT, char))
  {
    if constexpr (convdet::c_range_has_static_size<ResultT>)
      return convert_static_sized_range_impl<t_has_default>(input, std::forward<ConvInfoT>(convinfo), context);
    else
    {
      using value_type = typename info_type::value_type;

      auto result = range_constructor<std::remove_cvref_t<ConvInfoT>>(input.size());

      static constexpr std::size_t needed_count =
        convdet::count_of_arguments_needed<value_type>::naive_count;

      for (std::size_t i = input.size(), j = 0; i >=  needed_count; i -= needed_count, ++j)
      {
        range_value_adder.template operator()<std::remove_cvref_t<ConvInfoT>>(
          result, convert_for_fnt<value_type>{}(
          input, std::forward<ConvInfoT>(convinfo), context + nth_conv_type<value_type, npos>{}), j);
      }

      return result;
    }
  }

  template <typename InputDataT, c_conversion_context ContextT, c_conversion_info InfoT>
  static constexpr ResultT convert_impl(data_holder<InputDataT> & input,
                                        InfoT && info, ContextT context_
  ) noexcept (std::remove_cvref_t<InfoT>::is_noexcept)
  {
    using res_t = std::remove_cvref_t<ResultT>;
    static constexpr auto context = []{
      if constexpr (c_same_as<conversion_context<>, ContextT>)
        return conversion_context<res_t>{};
      else return ContextT{};
    }();

    using context_type = clean_type<context>;


    static constexpr bool has_default = convinfo_impl::can_call_from_info<
      conversion_handler_ind::default_value,
      InfoT, context_type, InputDataT
    >(typeseq<no_argument_error_ind>);

    if constexpr (convdet::c_range<res_t>)
      return convert_range_impl<has_default>(input, std::forward<InfoT>(info), context);
    else if constexpr (convdet::c_struct_with_known_types<res_t>)
    {
      if constexpr (has_default)
      {
        if (input.size() < info_type::size)
          return  [&input]<typename ResT_>(ResT_ && res_)
          {
            if constexpr (c_complex_tvtarget<ResT_, nth_conv_type>)
            {
              input.shrink_head(res_.used_tokens);
              return std::forward<ResT_>(res_).value;
            }
            else return std::forward<ResT_>(res_);
          }(
            convinfo_impl::call_from_info<
              conversion_handler_ind::default_value,
              no_argument_error_ind
            >(std::forward<InfoT>(info), context, input)
          );
      }

      return convert_known_struct(input, std::forward<InfoT>(info), context, info_type::ts);
    } else
    {
      if constexpr (convdet::c_known_how_to_convert<res_t, InfoT, ContextT>)
      {
        if constexpr (has_default)
        {
          if (input.empty())
            return convinfo_impl::call_from_info<
              conversion_handler_ind::default_value,
              no_argument_error_ind>(std::forward<InfoT>(info), context, input);
          else
          {
            auto [res, count] = conversion_specs<res_t>::convert(input, std::forward<InfoT>(info), context);

            input.shrink_head(count);
            return res;
          }
          // TODO: need to handle no arg error because
          //   if no default value was provided
          //   then there is no other way to get value without arg at this step
        }
        else
        {
          auto [res, count] = conversion_specs<res_t>::convert(input, std::forward<InfoT>(info), context);

          input.shrink_head(count);
          return res;
        }
      }
      else static_assert(sizeof(info_type) < 0, "no way to convert is known");
    }
  }
};

namespace
{
inline constexpr struct convert_for_creator_t
{
  template <typename ... Ts>
  static constexpr auto get(type_sequence<Ts...> = {}) noexcept
  {
    if constexpr (sizeof...(Ts) == 1)
    {
      if constexpr (c_complex_target<decltype(get_first_type<Ts...>()), type_sequence>)
        return get(Ts{}...);
      else return convert_for_fnt<Ts...>{};
    }
    else return convert_for_fnt<enumerated_custom_struct_from<type_sequence<Ts...>>>{};
  }
} convert_for_creator {};
}

template <typename ... ResultT>
inline constexpr auto convert_for = convert_for_creator_t::get<ResultT...>();

} // namespace pspp

#endif // PSPP_INPUT_CONVERTER_HPP
