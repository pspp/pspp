#pragma once

#include <pspp/temporary_storage.hpp>

namespace pspp
{

enum struct conversion_handler_ind
{ default_value, error, precheck, postcheck };

template <c_noexceptness NoexceptnessT, typename ... HandlerTs>
struct conversion_info;

template <typename T>
concept c_conversion_info =
c_complex_target<T, conversion_info>;

template <c_conversion_info InfoT>
inline constexpr bool is_conv_noexcept = std::remove_cvref_t<InfoT>::is_noexcept;



namespace convinfo_impl
{

template <
  typename ... TArgTs,
  typename HandlerT,
  c_conversion_context ConversionContextT,
  typename InfoT,
  typename InputRangeT,
  typename ... ArgTs
>
pspp_always_inline
constexpr auto call(
  HandlerT && handler, ConversionContextT context,
  InfoT & info,
  InputRangeT & input,
  ArgTs && ... args)
{
#define handler_ std::forward<HandlerT>(handler)
#define exr_if_can(...) pspp_execute_and_return_if_can(__VA_ARGS__)
#define callop template operator()

  exr_if_can(handler_.callop<TArgTs...>(std::forward<ArgTs>(args)...));
  else exr_if_can(handler_.callop<TArgTs...>(context, std::forward<ArgTs>(args)...));
  else exr_if_can(handler_.callop<TArgTs...>(input, std::forward<ArgTs>(args)...));
  else exr_if_can(handler_.callop<TArgTs...>(info, std::forward<ArgTs>(args)...));
  else exr_if_can(handler_.callop<TArgTs...>(context, info, std::forward<ArgTs>(args)...));
  else exr_if_can(handler_.callop<TArgTs...>(context, input, std::forward<ArgTs>(args)...));
  else exr_if_can(handler_.callop<TArgTs...>(input, info, std::forward<ArgTs>(args)...));
  else exr_if_can(handler_.callop<TArgTs...>(context, input, info, std::forward<ArgTs>(args)...));
  else exr_if_can(handler_());
  else exr_if_can(handler_(context));
  else exr_if_can(handler_(input));
  else exr_if_can(handler_(info));
  else exr_if_can(handler_(context, info));
  else exr_if_can(handler_(context, input));
  else exr_if_can(handler_(input, info));
  else exr_if_can(handler_(context, input, info));

  else return uncallable_t{};
#undef exr_if_can
#undef handler_
#undef callop
}

template <
  typename HandlerT,
  c_conversion_context ConversionContextT,
  typename InfoT,
  typename InputRangeT,
  typename ... ArgTs,
  typename ... TArgTs
>
constexpr bool can_call(type_sequence<TArgTs...>, type_sequence<ArgTs...> = {})
{
  return not c_same_as<uncallable_t, decltype(
  call<TArgTs...>(
    std::declval<HandlerT>(),
    std::declval<ConversionContextT>(),
    std::declval<InfoT &>(),
    std::declval<InputRangeT &>(),
    std::declval<ArgTs &&>()...
  )
  )>;
}


template <
  typename ... TArgTs,
  typename PrevT,
  typename HandlerT,
  c_conversion_context ConversionContextT,
  typename InfoT,
  typename InputRangeT,
  typename ... ArgTs,
  std::size_t t_i
>
pspp_always_inline
constexpr auto call_one(value_holder<t_i>,
                        PrevT && previous,
                        HandlerT && handler, ConversionContextT context,
                        InfoT & info, InputRangeT & input,
                        ArgTs && ... args)
{
  if constexpr (c_same_as<PrevT, uncallable_t> and t_i != npos)
  {
    return
      call_one<TArgTs...>(
        val<t_i - 1ul>,
        call<TArgTs...>(
          std::forward<HandlerT>(handler).handlers.template get<t_i>(),
          context, info, input, std::forward<ArgTs>(args)...
        ),
        std::forward<HandlerT>(handler),
        context, info, input,
        std::forward<ArgTs>(args)...
      );
  }
  else return previous;
}


template <
  typename ... TArgTs,
  typename HandlerT,
  c_conversion_context ConversionContextT,
  typename InfoT,
  typename InputRangeT,
  typename ... ArgTs
>
pspp_always_inline
constexpr auto call_handler(HandlerT && handler, ConversionContextT context,
                            InfoT & info,
                            InputRangeT & input,
                            ArgTs && ... args) -> decltype(auto)
{
  return call_one<TArgTs...>(
    val<std::remove_cvref_t<HandlerT>::size - 1>,
    uncallable_t{},
    std::forward<HandlerT>(handler),
    context, info, input,
    std::forward<ArgTs>(args)...
  );
}

template <
  typename HandlerT,
  c_conversion_context ConversionContextT,
  typename InfoT,
  typename InputRangeT,
  typename ... TArgTs,
  typename ... ArgTs
>
pspp_always_inline
constexpr auto can_call_handler(type_sequence<TArgTs...>, type_sequence<ArgTs...> = {}) -> decltype(auto)
{
  return not c_same_as<uncallable_t,
    decltype(
    call_handler<TArgTs...>(
      std::declval<HandlerT>(),
      std::declval<ConversionContextT>(),
      std::declval<InfoT &>(),
      std::declval<InputRangeT &>(),
      std::declval<ArgTs &&>()...
    )
    )>;
}

template <conversion_handler_ind t_ind, typename ConversionInfoT>
constexpr auto && get_handler(ConversionInfoT && convinfo) noexcept (noexcept(
  std::forward<ConversionInfoT>(convinfo)
))
{
  using enum conversion_handler_ind;

  if constexpr (t_ind == default_value)
    return std::forward<ConversionInfoT>(convinfo).default_value_handler;
  else if constexpr (t_ind == error)
    return std::forward<ConversionInfoT>(convinfo).error_handler;
  else if constexpr (t_ind == precheck)
    return std::forward<ConversionInfoT>(convinfo).precheck_handler;
  else if constexpr (t_ind == postcheck)
    return std::forward<ConversionInfoT>(convinfo).postcheck_handler;
}

template <
  conversion_handler_ind t_ind,
  typename ... TArgTs,
  typename ContextT,
  typename InputT,
  typename ... ArgTs,
  typename ConversionInfoT
>
constexpr auto call_from_info(
  ConversionInfoT && convinfo,
  ContextT context, InputT && input,
  ArgTs && ... args
) -> decltype(auto)
{
  return call<TArgTs...>(
    get_handler<t_ind>(std::forward<ConversionInfoT>(convinfo)),
    context, convinfo, std::forward<InputT>(input),
    std::forward<ArgTs>(args)...
  );
}

template <
  conversion_handler_ind t_ind,
  typename ConversionInfoT,
  typename ContextT,
  typename InputT,
  typename ... TArgTs,
  typename ... ArgTs
>
constexpr bool can_call_from_info(
  type_sequence<TArgTs...>, type_sequence<ArgTs...> = {}
)
{
  return not c_same_as<uncallable_t,
    decltype(
    call_from_info<t_ind, TArgTs...>(
      std::declval<ConversionInfoT &&>(),
      std::declval<ContextT>(),
      std::declval<InputT &&>(),
      std::declval<ArgTs &&>()...
    )
    )
  >;
}
}

template <typename ... HandlerTs>
struct conversion_handler
{
  tmpstorage<HandlerTs...> handlers;

  static_val ris = std::make_index_sequence<sizeof...(HandlerTs)>{} | reverse;
  static_size size = sizeof...(HandlerTs);

  template <std::size_t t_i>
  using handler_type_at = std::conditional_t<
    (t_i > size),
    void,
    decltype(handlers.template get<t_i>())
  >;

  template <
    typename ... TArgTs,
    typename InfoT,
    c_conversion_context ContextT,
    typename InputRangeT,
    typename ... ArgTs
  >
  pspp_always_inline
  constexpr auto operator()(ContextT context, InfoT & info, InputRangeT && input, ArgTs && ... args) -> decltype(auto)
  requires (
  convinfo_impl::can_call_handler<
    decltype(*this), ContextT, InfoT, InputRangeT
  >(typeseq<TArgTs...>, typeseq<ArgTs...>)
  )
  {
    return
      convinfo_impl::call_handler<TArgTs...>(
        *this, context, info, std::forward<InputRangeT>(input),
        std::forward<ArgTs>(args)...
      );
  }

  template <
    typename ... TArgTs,
    typename InfoT,
    c_conversion_context ContextT,
    typename InputRangeT,
    typename ... ArgTs
  >
  pspp_always_inline
  constexpr auto operator()(ContextT context, InfoT & info, InputRangeT && input, ArgTs && ... args) const -> decltype(auto)
  requires (
  convinfo_impl::can_call_handler<
    decltype(*this), ContextT, InfoT, InputRangeT
  >(typeseq<TArgTs...>, typeseq<ArgTs...>)
  )
  {
    return
      convinfo_impl::call_handler<TArgTs...>(
        *this, context, info, std::forward<InputRangeT>(input),
        std::forward<ArgTs>(args)...
      );
  }
};

template <typename ... Ts>
conversion_handler(tmpstorage<Ts...>) ->
conversion_handler<Ts...>;

conversion_handler() ->
conversion_handler<>;

template <typename T>
concept c_conversion_handler = c_derived_from_complex<T, conversion_handler>;

template <c_conversion_handler LeftT, typename RightT>
constexpr auto operator+(LeftT && left, RightT && right)
{
  return conversion_handler{
    std::forward<LeftT>(left).handlers +
    [](RightT && right)
    {
      if constexpr (c_tmpstorage<RightT>) return right;
      else return tmpstorage{std::forward<RightT>(right)};
    }(std::forward<RightT>(right))
  };
}

template <c_conversion_handler RightT, typename LeftT>
requires (not c_conversion_handler<LeftT>)
constexpr auto operator+(LeftT && left, RightT && right)
{
  return conversion_handler{
    val<std::remove_cvref_t<RightT>::ind>,
    [](LeftT && left)
    {
      if constexpr (c_tmpstorage<LeftT>) return left;
      else return tmpstorage{std::forward<LeftT>(left)};
    }(std::forward<LeftT>(left)) +
    std::forward<RightT>(right).handlers
  };
}



template <typename ... HandlerTs>
struct converr_handler :
  conversion_handler<HandlerTs...>
{
  using base = conversion_handler<HandlerTs...>;

  constexpr converr_handler() = default;
  constexpr converr_handler(converr_handler const &) = default;
  constexpr converr_handler(converr_handler &&) = default;

  constexpr converr_handler(HandlerTs && ... handlers) :
    base{tmpstorage{std::forward<HandlerTs>(handlers)...}}
  {}
};

template <typename T, typename ... HandlerTs>
requires (not c_complex_target<T, converr_handler>)
converr_handler(T &&, HandlerTs && ...) ->
converr_handler<T &&, HandlerTs && ...>;


template <typename ... HandlerTs>
struct convdef_handler :
  conversion_handler<HandlerTs...>
{
  using base = conversion_handler<HandlerTs...>;

  constexpr convdef_handler() = default;
  constexpr convdef_handler(convdef_handler const &) = default;
  constexpr convdef_handler(convdef_handler &&) = default;

  constexpr convdef_handler(HandlerTs && ... handlers)
  requires (sizeof...(HandlerTs) > 0) :
    base{tmpstorage{std::forward<HandlerTs>(handlers)...}}
  {}
};

template <typename T, typename ... HandlerTs>
requires (not c_complex_target<T, convdef_handler>)
convdef_handler(T &&, HandlerTs && ...) ->
  convdef_handler<T &&, HandlerTs && ...>;

template <typename ... HandlerTs>
struct convpre_handler :
  conversion_handler<HandlerTs...>
{
  using base = conversion_handler<HandlerTs...>;

  constexpr convpre_handler() = default;
  constexpr convpre_handler(convpre_handler const &) = default;
  constexpr convpre_handler(convpre_handler &&) = default;

  constexpr convpre_handler(HandlerTs && ... handlers)
  requires (sizeof...(HandlerTs) > 0) :
    base{tmpstorage{std::forward<HandlerTs>(handlers)...}}
  {}
};

template <typename T, typename ... HandlerTs>
requires (not c_complex_target<T, convpre_handler>)
convpre_handler(T &&, HandlerTs && ...) ->
  convpre_handler<T &&, HandlerTs && ...>;


template <typename ... HandlerTs>
struct convpost_handler :
  conversion_handler<HandlerTs...>
{
  using base = conversion_handler<HandlerTs...>;

  constexpr convpost_handler() = default;
  constexpr convpost_handler(convpost_handler const &) = default;
  constexpr convpost_handler(convpost_handler &&) = default;

  constexpr convpost_handler(HandlerTs && ... handlers)
  requires (sizeof...(HandlerTs) > 0) :
    base{tmpstorage{std::forward<HandlerTs>(handlers)...}}
  {}
};

template <typename T, typename ... HandlerTs>
requires (not c_complex_target<T, convpost_handler>)
convpost_handler(T &&, HandlerTs && ...) ->
  convpost_handler<T &&, HandlerTs && ...>;







inline constexpr struct default_conversion_error_handler_t
{
  template <typename = void, typename ... ArgTs>
  [[noreturn]]
  constexpr no_return_t pspp_static_call_const_operator(ArgTs && ...) noexcept
  {
    std::exit(1);

    std::unreachable();
    return {};
  }
} default_conversion_error_handler {};




}