#pragma once

#include <format>

#include <pspp/input_converter/conversion_handler.hpp>
#include <pspp/type_sequence.hpp>

namespace pspp
{

namespace
{
template <template <typename ...> class HandlerT, typename ... HandlerTs>
using conversion_handler_t = typename
  decltype(typeseq<HandlerTs...> | tget_last_like_or<is_complex_target_t<HandlerT>::template is_target, nullt>)::t;
}

/***
 * @tparam DefValHandlerT default value handler type
 * @tparam ErrorHandlerT  error handler type
 * @tparam NoexceptnessT  user specified/automaticaly deduced noexceptness
 */
template <c_noexceptness NoexceptnessT, typename ... HandlerTs>
struct conversion_info
{
  using default_value_handler_type = conversion_handler_t<convdef_handler, HandlerTs...>;
  using error_handler_type = conversion_handler_t<converr_handler, HandlerTs...>;
  using precheck_handler_type = conversion_handler_t<convpre_handler, HandlerTs...>;
  using postcheck_handler_type = conversion_handler_t<convpost_handler, HandlerTs...>;

  default_value_handler_type default_value_handler;
  error_handler_type error_handler;
  precheck_handler_type precheck_handler;
  postcheck_handler_type postcheck_handler;

  [[no_unique_address]]
  NoexceptnessT _;

  constexpr conversion_info() = default;
  constexpr conversion_info(conversion_info const &) = default;
  constexpr conversion_info(conversion_info &&) = default;

  template <c_conversion_handler ... HandlerUs>
  constexpr conversion_info(HandlerUs && ... handlers) :
    default_value_handler(get_ell_of_type_or<default_value_handler_type>(nulltv, std::forward<HandlerUs>(handlers)...)),
    error_handler(get_ell_of_type_or<error_handler_type>(nulltv, std::forward<HandlerUs>(handlers)...)),
    precheck_handler(get_ell_of_type_or<precheck_handler_type>(nulltv, std::forward<HandlerUs>(handlers)...)),
    postcheck_handler(get_ell_of_type_or<postcheck_handler_type>(nulltv, std::forward<HandlerUs>(handlers)...)),
    _()
  {}

  template <c_conversion_handler ... HandlerUs>
  constexpr conversion_info(NoexceptnessT noexceptness, HandlerUs && ... handlers) :
    default_value_handler(get_ell_of_type_or<default_value_handler_type>(nulltv, std::forward<HandlerUs>(handlers)...)),
    error_handler(get_ell_of_type_or<error_handler_type>(nulltv, std::forward<HandlerUs>(handlers)...)),
    precheck_handler(get_ell_of_type_or<precheck_handler_type>(nulltv, std::forward<HandlerUs>(handlers)...)),
    postcheck_handler(get_ell_of_type_or<postcheck_handler_type>(nulltv, std::forward<HandlerUs>(handlers)...)),
    _(noexceptness)
  {}

  static_flag is_noexcept = NoexceptnessT::value == 1;
  static_flag throws = NoexceptnessT::value == 2;

  template <conversion_handler_ind t_ind>
  pspp_always_inline
  constexpr auto & get_handler() const noexcept
  { return convinfo_impl::get_handler<t_ind>(*this); }

  template <conversion_handler_ind t_ind>
  pspp_always_inline
  constexpr auto & get_handler() noexcept
  { return convinfo_impl::get_handler<t_ind>(*this); }


  template <
    conversion_handler_ind t_ind,
    typename ... TArgTs,
    c_conversion_context ContextT,
    typename InputRangeT,
    typename ... ArgTs
  >
  pspp_always_inline
  constexpr auto call(ContextT context, InputRangeT && input, ArgTs && ... args) -> decltype(auto)
  {
    return convinfo_impl::call_from_info<t_ind, TArgTs...>(
      *this, context, std::forward<InputRangeT>(input), std::forward<ArgTs>(args)...
    );
  }

  template <
    conversion_handler_ind t_ind,
    typename ... TArgTs,
    c_conversion_context ContextT,
    typename InputRangeT,
    typename ... ArgTs
  >
  pspp_always_inline
  constexpr auto call(ContextT context, InputRangeT && input, ArgTs && ... args) const -> decltype(auto)
  {
    return convinfo_impl::call_from_info<t_ind, TArgTs...>(
      *this, context, std::forward<InputRangeT>(input), std::forward<ArgTs>(args)...
    );
  }

  template <
    conversion_handler_ind t_ind,
    c_conversion_context ContextT,
    typename InputRangeT,
    typename ... ArgTs,
    typename ... TArgTs
  >
  pspp_always_inline [[nodiscard]]
  constexpr bool can_call(type_sequence<TArgTs...> targs, type_sequence<ArgTs...> args = {}) noexcept
  {
    return convinfo_impl::can_call_from_info<t_ind, decltype(*this), ContextT, InputRangeT>(targs, args);
  }

  template <
    conversion_handler_ind t_ind,
    c_conversion_context ContextT,
    typename InputRangeT,
    typename ... ArgTs,
    typename ... TArgTs
  >
  pspp_always_inline [[nodiscard]]
  constexpr bool can_call(type_sequence<TArgTs...> targs, type_sequence<ArgTs...> args = {}) const noexcept
  {
    return convinfo_impl::can_call_from_info<t_ind, decltype(*this), ContextT, InputRangeT>(targs, args);
  }


  static constexpr bool has_default_value_handler() noexcept
  { return c_complex_target<default_value_handler_type, conversion_handler>; }

  static constexpr bool has_error_handler() noexcept
  { return c_complex_target<error_handler_type, conversion_handler>; }

  constexpr auto & get_default_value_handler() noexcept
  { return default_value_handler; }

  constexpr auto & get_default_value_handler() const noexcept
  { return default_value_handler; }

  constexpr auto & get_error_handler() noexcept
  { return error_handler; }

  constexpr auto & get_error_handler() const noexcept
  { return error_handler; }
};



template <c_conversion_handler ... HandlerTs>
conversion_info(HandlerTs && ...) ->
  conversion_info<no_except_t, HandlerTs && ...>;

template <c_noexceptness NoexceptnessT, c_conversion_handler ... HandlerTs>
conversion_info(NoexceptnessT, HandlerTs && ...) ->
  conversion_info<NoexceptnessT, HandlerTs && ...>;

inline constexpr struct def_converr_handler_t
{
  template <c_conversion_context ContextT, c_conversion_info InfoT>
  constexpr auto pspp_static_call_const_operator(ContextT context, conversion_input input, InfoT &&)
    noexcept (is_conv_noexcept<InfoT>)
    -> no_return_t
  {
    std::string const str = std::format("an error occured while converting to: {} ({}) at: `{}`",
                                        get_type_name<typename ContextT::last_type>(),
                                        context.to_string(),
                                        input.empty() ? "NO_INPUT" : input.front()
    );

    if constexpr (is_conv_noexcept<InfoT>)
    {
      std::puts(str.c_str());
      std::exit(1);
    }
    else
    {
      throw std::runtime_error(str);
    }
    return no_return;
  }
} def_converr_handler {};


constexpr auto default_conversion_info = conversion_info{
  no_except,
  converr_handler{def_converr_handler_t{}},
};


using default_convinfo_t = decltype(default_conversion_info);

template <typename NT, typename ... HandlerTs>
struct type_name_t<conversion_info<NT, HandlerTs...>> :
  constr_holder<"conversion_info <def: " +
                type_name_constr<typename conversion_info<NT, HandlerTs...>::default_value_handler_type> + "; err: "
                + type_name_constr<typename conversion_info<NT, HandlerTs...>::error_handler_type> + "; "
                + type_name_constr<NT> + ">"
  >
{};


}