#pragma once

#include <pspp/data_holder.hpp>

namespace pspp
{
struct no_return_t {};
inline constexpr no_return_t no_return {};

inline constexpr struct uncallable_t{} uncallable;

using conversion_input = data_holder<std::string_view const> &;


template <int t_value>
struct noexceptness : std::integral_constant<int, t_value>
{};

using auto_noexcept_t = noexceptness<0>;
using no_except_t     = noexceptness<1>;
using throws_t        = noexceptness<2>;

inline constexpr auto_noexcept_t auto_noexcept {};
inline constexpr no_except_t     no_except {};
inline constexpr throws_t        throws {};


template <typename T>
concept c_noexceptness = c_complex_vtarget<T, noexceptness>;

template <typename T>
struct is_noexceptness : std::bool_constant<c_noexceptness<T>>
{};

} // namespace pspp