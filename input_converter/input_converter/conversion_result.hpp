#pragma once

#include <pspp/input_converter/basic.hpp>

namespace pspp
{

template <typename ResultDataT>
struct conversion_result
{
  ResultDataT value;
  std::size_t tokens_used = 1;

  constexpr conversion_result() = default;
  //constexpr conversion_result(conversion_result const &) = default;
  //constexpr conversion_result(conversion_result &&) = default;

  template <typename DataT>
  requires (not c_same_as<DataT, conversion_result>)
  constexpr conversion_result(DataT && val, std::size_t used = 1) :
    value{std::forward<DataT>(val)},
    tokens_used{used}
  {}

  constexpr conversion_result(no_return_t) :
    conversion_result()
  {}

  constexpr conversion_result(uncallable_t) :
    conversion_result()
  {}
};

template <typename DataT>
struct resumable : conversion_result<DataT>
{};

}