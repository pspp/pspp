#pragma once

#include <pspp/type_name.hpp>

namespace pspp
{

template <typename T, std::size_t t_n>
struct nth_conv_type
{
  using type = T;
  static_size value = t_n;

  static_flag is_variadic = t_n == npos;
};

namespace
{

inline constexpr struct nth_convtype_convertor_t
{
  template <typename T>
  constexpr auto pspp_static_call_const_operator() noexcept
  {
    if constexpr (c_complex_tvtarget<T, nth_conv_type>)
      return type<typename T::type>;
    else return type<T>;
  }
} nth_convtype_convertor {};

template <typename T>
using is_nth_convtype = is_complex_tvtarget_t<nth_conv_type>::is_target<T>;

} // namespace pspp

template <typename ... Ts>
struct conversion_context
{
#ifdef __cpp_pack_indexing
  template <std::size_t t_i>
  using type_at = Ts...[t_i];

  using head_type = Ts...[0];
  using last_type = Ts...[sizeof...(Ts) - 1];
#else

  static_prop_of struct type_getter_t{
    template <std::size_t t_i>
    static constexpr auto operator()(value_holder<t_i>) noexcept
    {
      if constexpr (t_i >= sizeof...(Ts))
        return type<void>;
      else
        return typeseq<Ts...> | at<t_i>;
    }
  } type_getter {};

  template <std::size_t t_i>
  using type_at = typename decltype(
  type_getter(val<t_i>)
  )::t;

  using head_type = type_at<0>;
  using last_type = type_at<sizeof...(Ts) - 1>;

#endif // __cpp_pack_indexing

  static_size size = sizeof...(Ts);


  template <typename T>
  inline static constexpr bool contains_exect = typeseq<Ts...> | ::pspp::contains<T>;

  template <typename ... Us>
  inline static constexpr bool contains_exect_subseq = typeseq<Ts...> | ::pspp::contains_subseq<Ts...>;

  template <typename ... Us>
  inline static constexpr bool contains_exect_subseq_from_end = []{
    if constexpr (sizeof...(Us) > sizeof...(Ts)) return false;
    else
    {
      return (typeseq<Ts...> | subseq_from_end<0, sizeof...(Us)>) == typeseq<Us...>;
    }
  }();


  template <typename U>
  inline static constexpr bool contains = []{
    if constexpr (c_complex_tvtarget<U, nth_conv_type>)
      return contains_exect<U>;
    else
    {
      return
        typeseq<Ts...>
        | transform<nth_convtype_convertor>
        | contains<U>;
    }
  }();

  template <typename ... Us>
  inline static constexpr bool contains_subseq = []{
    if constexpr (typeseq<Us...> | tcontains_such_as<is_nth_convtype>)
      return contains_exect_subseq<Us...>;
    else
      return
        typeseq<Ts...>
        | transform<nth_convtype_convertor>
        | contains_subseq<Us...>;
  }();

  template <typename T>
  static_flag is_last = c_same_as<T, typename conversion_context::last_type> or
                        requires {
                          requires
                          c_complex_tvtarget<typename conversion_context::last_type, nth_conv_type> and
                          conversion_context::last_type::value != npos and
                          c_same_as<typename conversion_context::last_type::type, T>;
                        };

  static constexpr auto to_constr () noexcept
  {
    static constexpr constr tmp = (constr{"|"} + ... + []{ return constr{":"} + type_name_constr<Ts>; }());
    return tmp;
  }

  static constexpr std::string_view to_string() noexcept
  {
    static constexpr auto tmp = to_constr();
    return tmp.strv();
  }
};

template <>
struct conversion_context<>
{
  template <std::size_t t_i>
  using type_at = void;

  using head_type = void;
  using last_type = void;

  static_size size = 0;


  template <typename T>
  inline static constexpr bool contains_exect = false;

  template <typename ... Us>
  inline static constexpr bool contains_exect_subseq = false;

  template <typename ... Us>
  inline static constexpr bool contains_exect_subseq_from_end = false;


  template <typename U>
  inline static constexpr bool contains = false;

  template <typename ... Us>
  inline static constexpr bool contains_subseq = false;

  template <typename T>
  static_flag is_last = false;

  static constexpr std::string_view to_string() noexcept
  { return "|:"; }

  static constexpr auto to_constr() noexcept
  { return constr{"|:"}; }
};

template <typename T, typename ... Ts>
constexpr conversion_context<Ts..., T> operator+(conversion_context<Ts...>, type_t<T>) noexcept
{ return {}; }

template <typename ... Ts, typename T,  std::size_t t_i>
constexpr auto operator+(conversion_context<Ts...>, nth_conv_type<T, t_i>) noexcept
{
  if constexpr (c_complex_tvtarget<typename conversion_context<Ts...>::last_type, nth_conv_type>)
    return
      typeseq<Ts...>
      | subseq_from_end<1> + typeseq<typename conversion_context<Ts...>::last_type::type, nth_conv_type<T, t_i>>
      | pass_to<conversion_context>
      | unwrap;
  else return conversion_context<Ts..., nth_conv_type<T, t_i>>{};
}

template <typename ... Ts, typename ... Us>
constexpr conversion_context<Ts..., Us...> operator+(conversion_context<Ts...>, type_sequence<Us...>) noexcept
{ return {}; }


template <typename T>
concept c_conversion_context =
requires { T::size; } and
requires (T t)
{
  typename T::template type_at<0>;
  typename T::head_type;
  typename T::last_type;

  T::template contains_exect<void>;
  T::template contains_exect_subseq<void, void>;
  T::template contains_exect_subseq_from_end<void, void>;

  T::template contains<void>;
  T::template contains_subseq<void, void>;
  T::template is_last<void>;
};



template <typename T, std::size_t t_n>
struct type_name_t<nth_conv_type<T, t_n>> :
  constr_holder<type_name_constr<T> + "[" +
                conditional_v<
                  is_npos_v<t_n>, constr{"_"}, to_constr_v<t_n>
                > + "]"
  >
{};

template <typename ... Ts>
struct type_name_t<conversion_context<Ts...>>:
  constr_holder<"conversion_context <" + type_name_constr<Ts...> + ">">
{};

}