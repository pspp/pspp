#ifndef PSPP_VALUE_HOLDER_HPP
#define PSPP_VALUE_HOLDER_HPP

#include <type_traits>
#include <pspp/type_helpers_macro.hpp>

namespace pspp
{
inline namespace meta
{

template <auto t_val>
struct value_holder
{
	using type = std::remove_cvref_t<decltype(t_val)>;

	inline static constexpr type value = t_val;
	inline static consteval type get() noexcept { return t_val; }

	constexpr bool operator==(value_holder) const noexcept
	{ return true; }
	template <auto t_other>
	constexpr bool operator==(value_holder<t_other>) const noexcept
	{ return false; }

  consteval operator type () const noexcept
  { return t_val; }

  template <typename T>
    requires (requires {static_cast<T>(t_val);})
  explicit (true) consteval operator T () const noexcept
  { return static_cast<T>(t_val); }

  /*template <typename OtherT>
  constexpr auto operator<=>(OtherT && val) const noexcept
  { return t_val <=> val; }
   */
};



template <typename T>
struct is_value_holder : std::false_type {};

template <auto t_val>
struct is_value_holder<value_holder<t_val>> : std::true_type {};

template <typename MaybeValueHolder>
concept c_value_holder = is_value_holder<MaybeValueHolder>::value;

template <c_value_holder ValueHolder>
inline constexpr typename ValueHolder::type value = ValueHolder::get();

template <auto t_val>
inline constexpr value_holder<t_val> val {};

} // inline namespace meta
} // namespace pspp

#endif // PSPP_VALUE_HOLDER_HPP
