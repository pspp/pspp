
#include "args_parser.hpp"
#include <pspp/useful/ostream_overloads.hpp>
#include <pspp/input_converter.hpp>
#include <pspp/modern_main.hpp>
#include <pspp/utility/utility.hpp>
#include <pspp/input_converter.hpp>
#include <pspp/setter.hpp>

using namespace pspp::arginfo_literals;

using namespace pspp;


#if 1
#if 1
#if 2
using parser_t = args_parser<
    info_scope{
        isolated_scope{
          command_scope{"tmp"_name, "arguments delimiter"_explanation},
        },
        "options"_name,
        arginfo{"i"_short, "info"_long, "info about program"_explanation},
        info_scope{
          "additional"_name,
          arginfo{"h"_short, "help"_long, "shows program usage"_explanation,
                  indexed_func{val<0>, []{std::cout << "\nhelp is called\n"; } }
          }
        },
        isolated_scope{
          command_scope{
            "start"_name,
            indexed_func{val<1>, []{std::cout << "starting:\n"; }},
            arginfo{"inline"_long, "I"_short,
                    indexed_func{val<3>, []{std::cout << "inline was specified\n"; }},
                    },
            command_scope{"program"_name, indexed_func{val<2>, []{std::cout << "\tprogram\n"; }}},
            store_args_specification_info_as_optional,
          },
        },
    }
   >;


#else
using parser_t = pspp::args_parser<
      command_scope{
        "start"_name,
        arginfo{"inline"_long, "I"_short},
        command_scope{"program"_name}
      }
    >;
#endif

int main([[maybe_unused]]int argc, [[maybe_unused]] char ** argv)
{
#if 1
  auto const [res, other] = parser_t::parse(argc, argv);
  std::cout << "parse res: " << res << "\n\n"
               "unknown  : " << other << '\n';
#else
  static_assert(std::same_as<parser_t::result_type, void>);
#endif
}

#else

using namespace pspp;

template <typename T>
struct is_my_func : std::false_type {};

template <typename ResT>
struct is_my_func<ResT (char const *)> : std::true_type {};

template <typename T>
concept c_my_func = is_my_func<T>::value;


template <c_my_func auto t_func, typename ResT>
struct functor_base {
  constexpr ResT & operator()(const char * arg ) noexcept {
    auto original = t_func(arg);
    // link_builder(original);
    return static_cast<ResT*>(this);
  }
};

template <c_my_func auto t_func, typename ResT>
inline constexpr functor_base<t_func, ResT> functor_base_v {};

struct builder {
};

using custom = decltype(custom_struct{
  namedval<"a">(std::vector<int>{}),
  namedval<"b">(std::array<int, 5>{}),
  namedval<"c">(4)
});

int main([[maybe_unused]] int argc, char**)
{

  int * a;
  std::advance(a, 2);
  static_assert(convdet::count_of_arguments_needed<custom>::naive_count == 6);
}

#endif
#endif
