#pragma once

#include <pspp/constr.hpp>
#include <pspp/args_parser/args_specification_storage_options.hpp>
#include <pspp/type_helpers.hpp>

namespace pspp
{

template<constr t_n>
struct arg_name : constr_holder<t_n>
{
};

template<constr t_n>
struct short_name : constr_holder<t_n>
{
};

template<constr t_n>
struct long_name : constr_holder<t_n>
{
};

template<constr t_n>
struct explanation : constr_holder<t_n>
{
};

template<constr t_n>
struct false_short_name : constr_holder<t_n>
{
};

template<constr t_n>
struct false_long_name : constr_holder<t_n>
{
};

template<auto t_count>
struct args_count : value_holder<t_count>
{
};

template<typename HandlerT>
struct args_event_handler
{
  HandlerT handler;
};

inline namespace args_parser_literals
{
template<constr t_sn>
constexpr auto operator ""_short() noexcept
{ return short_name<t_sn>{}; }

template<constr t_ln>
constexpr auto operator ""_long() noexcept
{ return long_name<t_ln>{}; }

template<constr t_sn>
constexpr auto operator ""_false_short() noexcept
{ return false_short_name<t_sn>{}; }

template<constr t_ln>
constexpr auto operator ""_false_long() noexcept
{ return false_long_name<t_ln>{}; }

template<constr t_expl>
constexpr auto operator ""_explanation() noexcept
{ return explanation<t_expl>{}; }

template<constr t_n>
constexpr auto operator ""_name() noexcept
{ return arg_name<t_n>{}; }

template<constr t_n>
constexpr auto operator ""_command() noexcept
{ return arg_name<t_n>{}; }

template<char ... t_chars>
constexpr auto operator ""_count() noexcept
{ return args_count<operator ""_c<t_chars...>().value>{}; }
}


namespace detail
{
struct scope_returns_control {};
struct scope_takes_control {};
}

struct scope_control_type
{
  static_val takes    = detail::scope_takes_control  {};
  static_val returns  = detail::scope_returns_control{};
};


namespace detail
{
template<typename T>
using is_sn = is_complex_vtarget_t<short_name>::is_target<T>;
template<typename T>
using is_ln = is_complex_vtarget_t<long_name>::is_target<T>;
template<typename T>
using is_expl = is_complex_vtarget_t<explanation>::is_target<T>;
template<typename T>
using is_name = is_complex_vtarget_t<arg_name>::is_target<T>;
template<typename T>
using is_false_sn = is_complex_vtarget_t<false_short_name>::is_target<T>;
template<typename T>
using is_false_ln = is_complex_vtarget_t<false_long_name>::is_target<T>;

template <typename T>
struct is_args_spec_storage_ind
{
  using clean_type = std::remove_cvref_t<T>;
  static_flag value =
      c_complex_vtarget<T, store_args_specification_info> or
      std::same_as<T, omit_args_specification_info> or
      std::same_as<T, store_args_as_optional>;
};

} // namespace detail

} // namespace pspp