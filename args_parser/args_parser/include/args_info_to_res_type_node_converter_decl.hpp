#pragma once

#include <pspp/args_parser/args_specification_storage_options.hpp>

namespace pspp
{

template<
    typename T,
    auto t_prev_spec =
        detail::arg_spec_storage_type::not_specified
   >
struct args_info_to_res_type_node_converter
{
  static_assert(always_false<T>(), "unknown type");
};

} // namespace detail
