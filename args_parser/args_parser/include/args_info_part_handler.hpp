#pragma once

#include <pspp/args_parser/args_tokenizer.hpp>
#include <pspp/args_parser/basic_args_types.hpp>
#include <pspp/args_parser/args_info_part_extractor_decl.hpp>
#include <pspp/args_parser/args_info_part_handler_info.hpp>
#include <pspp/utility/unique_type.hpp>

#include <pspp/custom_struct_setter.hpp>
#include <pspp/data_holder.hpp>
#include <pspp/unsized_constr.hpp>


namespace pspp
{

template <detail::part_handler_info t_prev_info, typename ToHandle>
struct args_info_part_handler
{};

namespace detail
{

using short_opt_ind = value_holder<detail::args_token_type::short_opt>;
using long_opt_ind = value_holder<detail::args_token_type::long_opt>;
using command_ind = value_holder<detail::args_token_type::entity>;

using args_token_view = basic_args_token <data_holder<char const>>;
using args_to_parse_type = data_holder<args_token_view>;

struct args_info_part_handler_holder
{
  constexpr auto operator+(auto val) const noexcept
  { return val; }
};


template <detail::part_handler_info t_prev_info, constr t_name>
consteval auto get_next_accessor_by_name() noexcept
{
  if constexpr (t_name.empty()) return t_prev_info.accessor;
  else return t_prev_info.accessor.template next<t_name>;
}

template <detail::part_handler_info t_prev_info, c_type_sequence auto t_new>
consteval auto append_args() noexcept
{
  if constexpr (t_new.empty())
    return t_prev_info.prev_strs;
  else
  {
    return t_prev_info.prev_strs +
           (t_new | convert_to_vals_and_pass_to<
               []<typename T>{ return T::nm; },
               template_unsized_constr_array
           >);
  }
}

template <detail::part_handler_info t_prev_info, c_type_sequence auto t_new>
consteval auto get_new_args_handler() noexcept
{
  if constexpr (t_new.empty())
    return t_prev_info.handler;
  else
  {
    return t_prev_info.handler +
           (t_new | convert_to_vals_and_pass_to<
               []<typename T>{return args_info_part_handler<t_prev_info, T>{};}, value_sequence>);
  }
}

template <detail::part_handler_info t_prev_info, c_type_sequence auto t_new, typename Ind>
consteval auto get_scope_base_info_for() noexcept
{
  if constexpr (t_new.empty())
    return t_prev_info.template info<Ind>;
  else
  {
    return t_new | pass_and_execute<[]<typename ... Ts>{
      return (t_prev_info.template info<Ind> + ... + args_info_part_extractor<Ind,
          args_info_part_handler<t_prev_info, Ts>>::value);
    }>;
  }
}

template <detail::part_handler_info t_old_prev_info, detail::part_handler_info t_new_prev_info,
    typename ScopeBase, typename ... Inds>
consteval auto get_full_scope_info() noexcept
{
  if constexpr (sizeof...(Inds) == 0)
    return t_new_prev_info;
  else
  {
    return
        t_old_prev_info.template appended<
            t_new_prev_info.accessor,
            t_new_prev_info.spec_opt,
            get_scope_base_info_for<t_new_prev_info, ScopeBase::other, Inds>()...
        >;
  }
}

template <detail::part_handler_info t_prev_info, typename ScopeBase, typename ... Inds>
consteval auto get_scope_base_info() noexcept
{
  static constexpr auto next_accessor = get_next_accessor_by_name<t_prev_info, ScopeBase::name>();
  static constexpr auto next_info = t_prev_info.template with_new_accessor<next_accessor>;

  if constexpr (sizeof...(Inds) == 0)
    return next_info;
  else
  {
    return
        next_info.template appended<
            next_accessor,
            ScopeBase::spec_store_opt,
            get_scope_base_info_for<next_info, ScopeBase::args, Inds>()...
        >;
  }
}


template <detail::part_handler_info t_prev_info, arginfo t_info>
struct short_opt
{
  static_val short_value = t_info.sn;
  static_val accessor = t_prev_info.accessor.template next<t_info.nm>.template value<>;

  template <template <auto> class HolderT, bool t_can_merge>
  pspp_always_inline
  constexpr auto pspp_static_call_const_operator(
      std::bool_constant<t_can_merge>,
      HolderT<short_value.value>,
      [[maybe_unused]] args_to_parse_type & args, auto & to_set, auto &) noexcept

  {
    //auto & val = to_set[::pspp::val<t_info.nm>];
    args.front().str.shrink_head();
    if constexpr (t_info.data_type == void_t)
      accessor(to_set, true);
    else
    {
// TODO: parsing
    }

    if constexpr (not is_nullt_v<t_info.handler>)
      t_info.handler.value();
  }
};

template <detail::part_handler_info t_prev_info, arginfo t_info>
requires (t_info.sn.empty())
struct short_opt<t_prev_info, t_info>
{
  constexpr void operator()(unique_type) const noexcept {};
};

template <detail::part_handler_info t_prev_info, arginfo t_info>
struct long_opt
{
  static_val long_value = t_info.ln;
  static_val accessor = t_prev_info.accessor.template next<t_info.nm>.template value<>;

  template <template <auto> class HolderT>
  pspp_always_inline
  constexpr auto pspp_static_call_const_operator(
      HolderT<long_value.value>,
      [[maybe_unused]] args_to_parse_type & args, auto & to_set, auto &) noexcept
  {
    args.shrink_head();
    if constexpr (t_info.data_type == void_t)
      accessor(to_set, true);

    if constexpr (not is_nullt_v<t_info.handler>)
      t_info.handler.value();
  }
};

template <detail::part_handler_info t_prev_info, arginfo t_info>
requires (t_info.ln.empty())
struct long_opt<t_prev_info, t_info>
{
  constexpr void operator()(unique_type) const noexcept {};
};

} // namespace detail


template <detail::part_handler_info t_prev_info,
    auto ... t_opts>
struct args_info_part_handler<t_prev_info, arginfo<t_opts...>> :
    detail::short_opt<t_prev_info, arginfo<t_opts...>{}>,
    detail::long_opt<t_prev_info, arginfo<t_opts...>{}>
{
  using detail::short_opt<t_prev_info, arginfo<t_opts...>{}>::operator();
  using detail::long_opt<t_prev_info, arginfo<t_opts...>{}>::operator();
};

template <detail::part_handler_info t_prev_info,
    auto ... t_opts>
struct args_info_part_handler<t_prev_info, info_scope<t_opts...>>
{
  using os = info_scope<t_opts...>;

  static_val basic_info = detail::get_scope_base_info<t_prev_info, os,
      detail::short_opt_ind, detail::long_opt_ind
  >();

  static_val info = detail::get_full_scope_info<
      t_prev_info,
      basic_info, os,
      detail::short_opt_ind,
      detail::long_opt_ind,
      detail::command_ind>();
};

namespace detail
{

template <detail::part_handler_info t_info>
pspp_always_inline
constexpr auto parse_args(detail::args_to_parse_type & args,
                          auto & to_set, auto & other) noexcept
{
  using enum detail::args_token_type;
  static constexpr auto info = t_info;

  while (not args.empty())
  {
#ifdef PARGS_DEBUG
    std::cout << "parsing: " << args.front().str.as<std::string_view>() << " (" << args.front().type << ")\n";
#endif

    switch (args.front().type)
    {
      case short_opt:
      {
        auto & tmp = args.front();
        //static constexpr auto tmp_ = info.template handler<detail::short_opt_ind>;
        //static_assert(std::same_as<void, decltype(tmp_)>);
        //static_assert(std::same_as<void, decltype(tmp_.caller.tsv)>);

        info.template handler<detail::short_opt_ind>.call(tmp.str.as<std::string_view>().substr(0, 1), args, to_set, other);
        break;
      }
      case long_opt:
        info.template handler<detail::long_opt_ind>.call(args.front().str.as<std::string_view>(),
                                                         args, to_set, other);
        break;
      case entity:
        info.template handler<detail::command_ind>.call(args.front().str.as<std::string_view>(),
                                                        args, to_set, other);
        break;
      default:
        std::unreachable();
    }

    if (args.front().str.empty())
      args.shrink_head();
  }
}

template <detail::part_handler_info t_prev_info, auto t_value_accessor, command_scope cs>
struct command
{
  static_val command_name = cs.name;

  template <template <auto> class HolderT>
  pspp_always_inline
  constexpr void operator()
      (HolderT<command_name>,
       args_to_parse_type & to_parse,
       auto & to_set, [[maybe_unused]] auto & other) const noexcept
  {
    //static_assert(std::same_as<void, PrevInfo>);

#ifdef PARGS_DEBUG
    std::cout << "inside command: " << command_name.strv() << '\n';
#endif
    to_parse.shrink_head();
    if constexpr (cs.opts | is_empty)
      t_prev_info.accessor.template value<true>(to_set);
    else if constexpr (cs.spec_store_opt == detail::arg_spec_storage_type::store_as_opt)
      t_value_accessor.optional_setter_with_default(to_set);
    else if constexpr (cs.spec_store_opt == detail::arg_spec_storage_type::store_as_val)
      t_value_accessor.template value<true>(to_set);

    if constexpr (not is_nullt_v<cs.handler>)
      cs.handler.value();

#ifdef PARGS_DEBUG
    std::cout << ">> starting new parsing loop <<\n";
#endif
    parse_args<t_prev_info>(to_parse, to_set, other);

#ifdef PARGS_DEBUG
    std::cout << ">>  ending new parsing loop  <<\n";
#endif
  }
};


} // namespace detail

template <detail::part_handler_info t_prev_info,
    auto ... t_opts>
struct args_info_part_handler<t_prev_info, command_scope<t_opts...>>
{
  using cs = command_scope<t_opts...>;

private:
  static_val accessors_storage =
      []{
        using enum detail::arg_spec_storage_type;
        static constexpr auto accessor = detail::get_next_accessor_by_name<t_prev_info, cs::name>();

        if constexpr (cs::spec_store_opt == not_specified or cs::spec_store_opt == omit)
        { return std::pair{accessor, 0}; }

        // TODO: XXX: Fix
        else if constexpr (cs::spec_store_opt == store_as_opt)
        { return std::pair{accessor.template next<>, accessor}; }
        else if constexpr (cs::spec_store_opt == store_as_val)
        {
          return std::pair{
                accessor.template next<
                    cs::spec_store_opt.data_str
                >,
                accessor.template next<
                    cs::spec_store_opt.flag_str
                >
          };
        }
      }();
  // gets prev_info with new accessor

  // gets info from other scopes without passing info about command specific arguments
public:
  static_val without_basic = t_prev_info.template with_new_accessor<accessors_storage.first>;
  static_val info = detail::get_full_scope_info<
      t_prev_info,
      without_basic, cs,
      detail::short_opt_ind,
      detail::long_opt_ind,
      detail::command_ind
      >();

  // merges command specific arguments
  static_val with_basic_info = info.template info_only_appended<
    detail::get_scope_base_info_for<without_basic, cs::args, detail::short_opt_ind>(),
    detail::get_scope_base_info_for<without_basic, cs::args, detail::long_opt_ind>()
  >;

  struct implementation : detail::command<with_basic_info, accessors_storage.second,cs{}>
  {};
};

template <detail::part_handler_info t_prev_info,
    auto ... t_opts>
struct args_info_part_handler<t_prev_info, isolated_scope<t_opts...>> :
    args_info_part_handler<
      detail::part_handler_info<
        t_prev_info.can_merge,
        detail::arg_spec_storage_type::not_specified,
        t_prev_info.accessor
      >{},
     info_scope<t_opts...>
    >
{};

} // namespace pspp