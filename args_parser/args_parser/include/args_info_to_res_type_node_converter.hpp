#pragma once

#include <pspp/args_parser/basic_args_types.hpp>
#include <pspp/named_variant.hpp>

namespace pspp
{
template <auto ... t_info, auto t_prev>
struct args_info_to_res_type_node_converter<arginfo<t_info...>, t_prev>
{
  using info = arginfo<t_info...>;
  static_val key = info::nm;
  using data_type = decltype(*info::template get_data_type<t_prev>());
  using type = custom_value_container<key, data_type>;
};

template <auto ... t_info, auto t_prev>
struct args_info_to_res_type_node_converter<flag_arginfo<t_info...>, t_prev>
{
  using info = flag_arginfo<t_info...>;
  using type = custom_value_container<info::nm, bool>;
};


namespace detail
{

template <auto t_prev, bool t_apply_spec_opt = false>
struct scope_to_type_converter
{
  template <auto t_next_opt>
  struct converter_wrapper
  {
    template <typename T>
    struct converter
    {
      using type = typename args_info_to_res_type_node_converter<T, t_next_opt>::type;
    };
  };

  constexpr static auto operator()(c_scope auto && val) noexcept
  {
    using value_type = pspp_declean(val);

    if constexpr (value_type::opts.empty())
      return type<void>;
    else
    {
      static constexpr auto converted_opts =
          value_type::opts |
            ttransform<converter_wrapper<value_type::spec_store_opt>::template converter> |
            absorb_recursively;

      /*static_assert(std::same_as<decltype(val.opts), void>);
      static_assert(std::same_as<decltype(converted_opts), void>);
       */

      if constexpr (value_type::name.empty()) return converted_opts;
      else
      {
        using result_type = decltype(*(converted_opts | pass_to<custom_struct>));

        if constexpr (t_apply_spec_opt)
        {
          return type<custom_value_container<
              value_type::name,
              decltype(*
                  get_next_type_for_arg_with_data<
                      type<result_type>,
                      value_type::spec_store_opt,
                      t_prev
                  >()
              )
          > // custom_value_container
          >;
        }
        else
          return type<custom_value_container<
              value_type::name,
              result_type
              >
            >;
      }
    }
  }
};
} // namespace detail

template <c_scope Scope, auto t_prev_opt>
struct args_info_to_res_type_node_converter<Scope, t_prev_opt>
{
  static_val key = conditional_v<Scope::name.empty(), type<void>, Scope::name>;
  using data_type = extract_type<detail::scope_to_type_converter<t_prev_opt>{}(Scope{})>;
  using type = data_type;
};

template <auto ... t_opts, auto t_prev_opt>
struct args_info_to_res_type_node_converter<exclusive_scope<t_opts...>, t_prev_opt>
{
  using type =
      custom_value_container<
          exclusive_scope<t_opts...>::name,
          extract_type<
              exclusive_scope<t_opts...>::opts | detail::transform_to_res
              | pass_to<named_variant>
          >
      >;
};

template <auto ... t_opts, auto t_prev_spec_opt>
struct args_info_to_res_type_node_converter<command_scope<t_opts...>, t_prev_spec_opt>
{
  using cs = command_scope<t_opts...>;
  using type =
          std::conditional_t<cs::opts | is_empty,
              custom_value_container<cs::name, bool>,
              extract_type<
                  detail::scope_to_type_converter<t_prev_spec_opt, true>{}(cs{})
              >
          >;
};

} // namespace pspp