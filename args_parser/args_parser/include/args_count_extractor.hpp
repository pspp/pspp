#pragma once

#include <pspp/args_parser/basic_args_types.hpp>

namespace pspp
{
template <typename T>
struct args_count_extractor
{ static_size value = 1; };

template <>
struct args_count_extractor<bool>
{ static_size value = 0; };

template <>
struct args_count_extractor<void>
{ static_size value = 0; };

template <template <typename, auto> class HolderT, typename DataT, auto t_size>
struct args_count_extractor<HolderT<DataT, t_size>>
{ static_size value = t_size; };

template <template <auto, typename> class HolderT, typename DataT, auto t_size>
struct args_count_extractor<HolderT<t_size, DataT>>
{ static_size value = t_size; };

namespace detail
{
template <typename T>
struct is_static_sized_data_holder : std::false_type
{};
template <typename DataT, auto t_size, template <typename, auto> class HolderT>
struct is_static_sized_data_holder<HolderT<DataT, t_size>> : std::true_type
{};
template <typename DataT, auto t_size, template <auto, typename> class HolderT>
struct is_static_sized_data_holder<HolderT<t_size, DataT>> : std::true_type
{};

template <typename T>
concept c_static_sized_data_holder = is_static_sized_data_holder<T>::value;

} // namespace detail

template <std::ranges::range RangeT>
requires (not detail::c_static_sized_data_holder<RangeT>)
struct args_count_extractor<RangeT>
{ static_size value = npos; };

template <typename T>
concept c_known_args_count = requires {
  args_count_extractor<T>::value;
};

} // namespace pspp
