#pragma once

#include <pspp/utility/declaration_macros.hpp>
#include <compare>
#include <pspp/constr.hpp>

namespace pspp
{
namespace detail
{

enum struct arg_spec_storage_type {
  not_specified,
  store_as_val,
  store_as_opt,
  omit,
  count
};

template <arg_spec_storage_type t_val>
struct arg_spec_storage_type_holder
{
  static_val value = t_val;

  consteval operator arg_spec_storage_type() const noexcept { return t_val; }

  consteval auto operator<=>(arg_spec_storage_type other) const noexcept
  { return t_val <=> other; }
};

template <constr t_spec_stat_str, constr t_data_str>
struct store_args_specification_info :
    arg_spec_storage_type_holder<arg_spec_storage_type::store_as_val>
{
  static_val data_str = default_if_empty<t_data_str, "data">;
  static_val flag_str = default_if_empty<t_spec_stat_str, "was_specified">;
};

struct store_args_as_optional :
    arg_spec_storage_type_holder<arg_spec_storage_type::store_as_opt>
{};

struct omit_args_specification_info :
    arg_spec_storage_type_holder<arg_spec_storage_type::omit>
{};
}

inline constexpr detail::store_args_as_optional        store_args_specification_info_as_optional{};

template <constr t_spec_stat_str = "", constr t_data_str = "">
inline constexpr detail::store_args_specification_info<t_spec_stat_str, t_data_str>
  store_args_specification_info_as_variable {};

inline constexpr detail::omit_args_specification_info  omit_args_specification_info{};

} // namespace pspp

