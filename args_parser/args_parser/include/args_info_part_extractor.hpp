#pragma once

#include <pspp/args_parser/args_info_part_handler.hpp>

namespace pspp
{

template <typename Ind, auto t_prev_info, auto ... t_opts>
struct args_info_part_extractor<Ind, args_info_part_handler<t_prev_info, arginfo<t_opts...>>>
{
  using info = arginfo<t_opts...>;
  using handler = args_info_part_handler<t_prev_info, info>;

  static_val target = conditional_v<type<Ind> == type<detail::short_opt_ind>,
      info::sn, info::ln>;

  using handler_t = decltype([]<template <auto> class HolderT> pspp_always_inline (HolderT<target.value>, auto && ... vals) {
    if constexpr (is_same<Ind, detail::short_opt_ind>())
      return
          static_call<handler{}>(
              std::bool_constant<t_prev_info.can_merge>{}, HolderT<target.value>{}, pspp_fwd(vals)...
          );
    else
      return static_call<handler{}>(HolderT<target.value>{}, pspp_fwd(vals)...);
  });

  static_val value = conditional_v<target.empty(),
      detail::strs_and_handler<Ind>{},
      detail::strs_and_handler<
          Ind,
          template_unsized_constr_array<target.value>{},
          handler_t{}
      >{}
  >;
};

template <typename Ind, auto t_prev_info, auto ... t_opts>
struct args_info_part_extractor<Ind, args_info_part_handler<t_prev_info, command_scope<t_opts...>>>
{
  using cs = args_info_part_handler<t_prev_info, command_scope<t_opts...>>;

  static_val value = conditional_v<
        std::same_as<Ind, detail::command_ind>,
        detail::strs_and_handler<Ind,
          template_unsized_constr_array<cs::cs::name>{},
          typename cs::implementation{}
        >{},
        detail::strs_and_handler<Ind>{}>;

  /*
   static_val return_for_command = []{
    return detail::strs_and_handler<Ind,
      template_unsized_constr_array<cs::cs::name>{},
      typename cs::implementation{}
  >{};};

  static_val value = []{
    if constexpr (t_is_forward)
    {

    }
    else
    {
      if constexpr (std::same_as<Ind, detail::command_ind>)
        return return_for_command();
      else return detail::strs_and_handler<Ind>{};
    }
  }();
   */
};


template <typename Ind, auto t_prev_info, auto ... t_opts>
struct args_info_part_extractor<Ind, args_info_part_handler<t_prev_info, info_scope<t_opts...>>>
{
  using os = args_info_part_handler<t_prev_info, info_scope<t_opts...>>;
  static_val value = os::info.template info<Ind>;
};

template <typename Ind, auto t_prev_info, auto ... t_opts>
struct args_info_part_extractor<Ind, args_info_part_handler<t_prev_info, isolated_scope<t_opts...>>>
{
  using is = args_info_part_handler<t_prev_info, isolated_scope<t_opts...>>;
  static_val value = is::info.template info<Ind>;
};
} // namespace pspp
