#pragma once

#include <pspp/utility/declaration_macros.hpp>
#include <pspp/meta.hpp>
#include <pspp/custom_struct.hpp>
#include <pspp/indexed_args.hpp>

#include <pspp/args_parser/args_detail_storage.hpp>
#include <pspp/args_parser/args_info_to_res_type_node_converter_decl.hpp>

namespace pspp
{
namespace detail
{
template <auto t_own_spec, auto t_prev_spec>
inline constexpr auto next_spec_storage_opt = []{
  if constexpr (t_own_spec == arg_spec_storage_type::not_specified) return t_prev_spec;
  else return t_own_spec;
}();

template <auto t_data_type, auto t_own_spec, auto t_prev_spec>
consteval auto get_next_type_for_arg_with_data() noexcept
{
  //static_assert(not is_same<t_prev_spec, t_own_spec>());

  using enum detail::arg_spec_storage_type;

  static constexpr auto spec = next_spec_storage_opt<t_own_spec, t_prev_spec>;

  if constexpr (spec == not_specified or spec == omit)
    return t_data_type;
  else if constexpr (spec == store_as_val)
    return type<custom_struct<
        custom_value_container<spec.flag_str, bool>,
        custom_value_container<spec.data_str, decltype(*t_data_type)>
    >>;
  else if constexpr (spec == store_as_opt)
    return type<std::optional<decltype(*t_data_type)>>;
  else static_assert(false, "lfdsa");
}

template <typename T>
struct is_indexed_func : std::bool_constant<c_complex_vtarget<T, indexed_func>>
{};
}


template <auto ... t_info>
struct arginfo
{
  consteval arginfo(auto...) {}

  static_val ts = typeseq<decltype(t_info)...> | remove_cvref;
  static_val sn = ts | tget_last_like_or<detail::is_sn, decltype(""_short)> | unwrap;
  static_val ln = ts | tget_last_like_or<detail::is_ln, decltype(""_long)> | unwrap;
  static_val _nm = ts | tget_last_like_or<detail::is_name, decltype(""_name)> | unwrap;
  static_val nm = conditional_v<_nm.empty(), ln.value, _nm.value>;
  static_val expl = ts | tget_last_like_or<detail::is_expl, decltype(""_explanation)> | unwrap;
  static_val data_type = ts | tget_last_like_or<is_type_t, type_t<void>> | unwrap;
  static_val default_value = ts | tget_last_like_or<is_value_sequence, value_holder<nulltv>> | unwrap;
  static_val spec_opt = ts | tget_last_like_or<
      detail::is_args_spec_storage_ind,
      detail::omit_args_specification_info > | unwrap;
  static_val handler = ts | tget_last_like_or<detail::is_indexed_func, nullt> | unwrap;

  static_flag is_flag_arg = false;

  static_val has_short() noexcept
  { return not sn.empty(); }

  static_val has_long() noexcept
  { return not ln.emty(); }

  static_assert(not (sn.empty() and ln.empty()));
  static_val get_name() noexcept
  {
    if constexpr (nm.empty())
      if constexpr (not ln.empty())
        return ln.strv();
      else return std::string_view{""};
    else return nm.strv();
  }

  template <auto t_prev
      = detail::arg_spec_storage_type::not_specified>
  static_val get_data_type() noexcept
  {
    if constexpr (data_type == type<void>)
    {
      if constexpr (default_value == val<nulltv>) return type<bool>;
      else return type<typename decltype(default_value)::type>;
    }
    else
    {
      return detail::get_next_type_for_arg_with_data<data_type, spec_opt, t_prev>();
    }
  }

  static_flag has_default_value() noexcept
  { return not std::same_as<typename decltype(default_value)::type, nullt>; }

  static_val get_default_value() noexcept
  {
    if constexpr (data_type != type<void>)
      return static_cast<extract_type<data_type>>(_get_default_value());
    else return _get_default_value();
  }

private:
  static_val _get_default_value() noexcept
  {
    if constexpr (requires { default_value.value(); })
      return default_value.value();
    else return default_value.value;
  }
};

template <auto ... t_info>
struct flag_arginfo
{
  static_val ts = typeseq<decltype(t_info)...> | remove_cvref;
  static_val sn = ts | tget_last_like_or<detail::is_sn, decltype(""_short)> | unwrap;
  static_val ln = ts | tget_last_like_or<detail::is_ln, decltype(""_long)> | unwrap;
  static_val _nm = ts | tget_last_like_or<detail::is_name, decltype(""_name)> | unwrap;
  static_val nm = conditional_v<_nm.empty(), ln.value, _nm.name>;
  static_val expl = ts | tget_last_like_or<detail::is_expl, decltype(""_explanation)> | unwrap;

  static_val false_sn = ts | tget_last_like_or<detail::is_false_sn, decltype(""_false_short)> | unwrap;
  static_val false_ln = ts | tget_last_like_or<detail::is_false_ln, decltype(""_false_long)> | unwrap;
  static_val spec_opt = ts | tget_last_like_or<
      detail::is_args_spec_storage_ind,
      detail::omit_args_specification_info > | unwrap | get_holded_value;
  static_flag is_flag_arg = true;
  static_val handler = ts | tget_last_like_or<detail::is_indexed_func, nullt> | unwrap;

  consteval flag_arginfo(auto...) {}
};

template <auto ... t_info>
inline constexpr arginfo<t_info...> arginfo_v {};

template <auto ... t_info>
inline constexpr flag_arginfo<t_info...> flag_arginfo_v {};

namespace detail
{
template <typename T>
using default_args_info_to_res_type_node_converter =
    args_info_to_res_type_node_converter<T>;

inline constexpr auto transform_to_res = ttransform<default_args_info_to_res_type_node_converter>;
template <auto ... t_opts>
struct scope_base
{
  ///@brief passed opts storage
  static_val ts = typeseq<decltype(t_opts)...> | remove_cvref;
  ///@brief holds name of scope; if not empty adds new custom struct layer
  static_val name_holder = ts | tget_last_like_or<is_name, decltype(""_name)> | unwrap;
  ///@brief value stored in name_holder
  static_val name = name_holder.value;
  ///@brief stores explanation; for help info generation
  static_val expl = ts
                    | tget_last_like_or<is_expl, decltype(""_explanation)>
                    | unwrap;
  static_val handler = ts | tget_last_like_or<is_complex_vtarget_t<indexed_func>::is_target, nullt> | unwrap;
private:
  static_val spec_opt_extr = ts | textract_like<is_args_spec_storage_ind>;

public:
  ///@brief options only (explanation and name excluded)
  static_val opts = spec_opt_extr.failed | tremove_like<is_expl, is_name, is_indexed_func>;

  static_val spec_store_opt = []{
    if constexpr (spec_opt_extr.succeeded | is_empty)
      return detail::arg_spec_storage_type::not_specified;
    else return spec_opt_extr.succeeded | last | unwrap;
  }();

private:
  static_val extracted = opts | textract_like<is_some_of_complex_vtargets_t<arginfo, flag_arginfo>::is_target>;

public:
  ///@brief stores only \arginfo and \flag_arginfo
  static_val args = extracted.succeeded;
  ///@brief stores everything what was not stored in \args
  static_val other = extracted.failed;
};

} // namespace detail

/**
 * @tparam t_opts
 */
template <auto ... t_opts>
struct info_scope : detail::scope_base<t_opts...>
{
  static_assert(not detail::scope_base<t_opts...>::opts.empty(), "options scope must have one or more options");
  static_assert(is_nullt_v<detail::scope_base<t_opts...>::handler>, "options scope do not takes handlers");

  consteval info_scope(auto...): detail::scope_base<t_opts...>{} {}
  consteval info_scope(): detail::scope_base<t_opts...>{} {}
};

template <auto ... t_opts>
struct isolated_scope : detail::scope_base<t_opts...>
{
  static_assert(not detail::scope_base<t_opts...>::opts.empty(), "isolated scope must have one or more options");
  static_assert(is_nullt_v<detail::scope_base<t_opts...>::handler>, "isolated scope do not takes handlers");
  consteval isolated_scope(auto...): detail::scope_base<t_opts...>{} {}
  consteval isolated_scope(): detail::scope_base<t_opts...>{} {}
};

template <auto ... t_opts>
struct command_scope : detail::scope_base<t_opts...>
{
  static_assert(not detail::scope_base<t_opts...>::name.empty(), "command scope must have a name");
  consteval command_scope(auto ...): detail::scope_base<t_opts...>{} {}
  consteval command_scope(): detail::scope_base<t_opts...>{} {}
};

template <auto ... t_opts>
struct exclusive_scope : detail::scope_base<t_opts...>
{
  static_assert(not detail::scope_base<t_opts...>::name.empty(), "exclusive scope must have a name");
  static_assert(not detail::scope_base<t_opts...>::opts.empty(), "exclusive scope must have one or more options/commands");

  consteval exclusive_scope(): detail::scope_base<t_opts...>{} {}
  consteval exclusive_scope(auto ...): detail::scope_base<t_opts...>{} {}
};

template <typename ... Args>
arginfo(Args...) -> arginfo<Args{}...>;
template <typename ... Args>
flag_arginfo(Args...) -> flag_arginfo<Args{}...>;
template <typename ... Args>
info_scope(Args...) -> info_scope<Args{}...>;
template <typename ... Args>
isolated_scope(Args...) -> isolated_scope<Args{}...>;
template <typename ... Args>
command_scope(Args...) -> command_scope<Args{}...>;
template <typename ... Args>
exclusive_scope(Args...) -> exclusive_scope<Args{}...>;


template <auto ... t_opts>
inline constexpr info_scope<t_opts...> info_scope_v {};

template <auto ... t_opts>
inline constexpr command_scope<t_opts...> command_scope_v{};

template <auto ... t_opts>
inline constexpr exclusive_scope<t_opts...> exclusive_scope_v{};

template <typename T>
concept c_arginfo = c_complex_vtarget<T, arginfo> or
                    c_complex_vtarget<T, flag_arginfo>;

template <typename T>
concept c_scope = requires {
  T::name;
  { T::name.empty() } -> std::same_as<bool>;
  T::expl;
  T::opts;
  T::spec_store_opt;
};


} // namespace pspp

