#pragma once

#include <pspp/runtime_handlers_map.hpp>
#include <pspp/functional.hpp>

#include <pspp/args_parser/args_specification_storage_options.hpp>
#include <pspp/type_sequence.hpp>

namespace pspp::detail
{

inline constexpr auto default_handler = overloaded_lambda{
    [](nullt, auto str, auto &to_parse, [[maybe_unused]] auto &,
       std::vector <std::string_view> &ignored_args)
    {
      ignored_args.emplace_back(str);
      to_parse.shrink_head();
    },
    [](nullt, char, auto &to_parse, [[maybe_unused]] auto &to_set,
       std::vector <std::string_view> &ignored_args)
    {
      ignored_args.emplace_back(to_parse.front().str.data(), 1);
      to_parse.shrink_head();
    }
};

struct default_strs {
  constexpr auto operator+(auto val) const noexcept { return val; }
};

constexpr auto operator+(auto && val, default_strs) noexcept
  requires (not std::same_as<pspp_declean(val), default_strs>)
{ return pspp_fwd(val); }

template <
    typename Ind,
    auto t_strs = default_strs{},
    auto t_handler = default_handler>
struct strs_and_handler
{
  using ind = Ind;
  static_val strs = t_strs;
  static_val handler = t_handler;

  template <auto t_strs1, auto t_handler1>
  constexpr auto operator+(strs_and_handler<Ind, t_strs1, t_handler1>) const noexcept
  { return strs_and_handler<Ind, t_strs + t_strs1, t_handler + t_handler1>{}; }
};

template <typename T>
struct part_handler_info_to_handler
{};

template <auto t_handler>
inline constexpr auto default_args_to_handler_passer = []<typename T>
    (auto && ... vals) requires (sizeof...(vals) > 2)
{
  return t_handler(T{}, pspp_fwd(vals)...);
};


template <typename Ind, auto t_strs, auto t_handler>
struct part_handler_info_to_handler<strs_and_handler<Ind, t_strs, t_handler>>
{
  static_val value = runtime_handlers_map{
      t_strs, handler<default_args_to_handler_passer<t_handler>>()
  };
};

struct dummy_arginfo_runtime_handler
{
  template <typename ...>
  constexpr void call([[maybe_unused, gnu::unused]] auto key, auto & to_parse,
                      [[maybe_unused, gnu::unused]] auto & to_set, auto & other)const noexcept
  {
    other.emplace_back(to_parse.front().str.template as<std::string_view>());
    to_parse.shrink_head();
  }
};

template <typename Ind>
struct part_handler_info_to_handler<strs_and_handler<Ind, default_strs{}, default_handler>>
{
  static_val value = dummy_arginfo_runtime_handler{};
};

template <
    bool t_can_merge,
    auto t_spec_opt,
    auto t_accessor,
    strs_and_handler ... t_infos>
struct part_handler_info
{
  static_val accessor = t_accessor;
  static_val ts = vtypeseq<t_infos...>;
  static_val can_merge = t_can_merge;
  static_val spec_opt = t_spec_opt;

  template <typename Ind>
  static_val info = ts | get_first_like_or<
                         []<typename T>{return std::same_as<typename T::ind, Ind>; },
      strs_and_handler<Ind>
  > | unwrap;

  template <auto t_next_accessor>
  static_val with_new_accessor = part_handler_info<t_can_merge, t_spec_opt,
   t_next_accessor, t_infos...>{};

  template <typename Ind>
  static_val handler = part_handler_info_to_handler<clean_type<info<Ind>>>::value;

  template <auto t_new_accessor,
      auto t_new_spec_opt,
      strs_and_handler ... t_new>
  static_val appended =
  []{
    static constexpr auto new_spec_opt =
    []{
      if constexpr (t_new_spec_opt == detail::arg_spec_storage_type::not_specified)
          return t_spec_opt;
      else return t_new_spec_opt;
    }();

    if constexpr (sizeof...(t_infos) == 0)
      return part_handler_info<t_can_merge, new_spec_opt, t_accessor, t_new...>{};
    else
    {
      static constexpr auto old_types = ts | remove_like<[]<typename T>
      {
        return (... or std::same_as<typename T::ind, typename decltype(t_new)::ind>);
      }>;

      if constexpr (old_types | is_empty)
        return part_handler_info<t_can_merge, new_spec_opt, t_new_accessor,
            (info<typename decltype(t_new)::ind> + t_new)...>{};
      else
        return old_types | pass_as_values_to<
                           part_handler_info, t_can_merge, new_spec_opt, t_new_accessor,
            (info<typename decltype(t_new)::ind> + t_new)...>
        | unwrap;
    }
  }();

  template <strs_and_handler ... t_new>
  static_val info_only_appended = appended<t_accessor, t_spec_opt, t_new...>;
};

} // namespace pspp::detail

namespace pspp
{
template <typename T>
concept c_part_handler_info = c_complex_vtarget<T, detail::part_handler_info>;
}
