#pragma once

#include <vector>
#include <iostream>

#include <pspp/utility/declaration_macros.hpp>
#include <pspp/constr.hpp>

namespace pspp
{
namespace detail
{
enum struct args_token_type : std::uint32_t
{ short_opt, long_opt, args_sep, assign, separator, comma_arg, entity, _count_ };

template <typename StringViewT = std::string_view>
struct basic_args_token
{
  args_token_type type;
  StringViewT str;
};

using args_token = basic_args_token<>;

} // namespace detail

template <constr t_args_separators>
struct args_tokenizer
{
  char const * iter;
  char const * word_begin;
  char const * word_end;
  detail::args_token_type last_check_result;
  std::vector<detail::args_token> result;

  std::string_view program_name;

  template <detail::args_token_type t_t>
  pspp_always_inline
  constexpr void add_token() noexcept
  { result.emplace_back(t_t, std::string_view{word_begin, iter}); word_begin = iter; }

  [[nodiscard]] pspp_always_inline
  constexpr char get_current_char_if_can() const noexcept
  {
    if (iter == word_end)
      return 0;
    else return *iter;
  }

  [[nodiscard]]
  constexpr bool is_special_char() const noexcept
  {
    return *iter == '=' or is_separator();
  }

  constexpr void move_to_word_end() noexcept
  {
    recheck:
    if (not is_special_char())
    {
      ++iter;
      if (iter < word_end)
        goto recheck;
    }
  }

  template <detail::args_token_type t_t>
  constexpr void add_word() noexcept
  {
    move_to_word_end();
    add_token<t_t>();
  }

  template <detail::args_token_type t_t>
  constexpr void incr_and_add_token() noexcept
  {
    ++iter;
    add_token<t_t>();
  }

  [[nodiscard]] pspp_always_inline
  constexpr bool is_separator() const noexcept
  { return t_args_separators.strv().contains(*iter); }

  constexpr auto const & tokenize(std::ranges::range auto && range) noexcept
  {
    for (std::string_view part : range)
    {
      word_begin = part.begin();
      word_end = part.end();
      iter = word_begin;

      while (iter < word_end)
      {
        //std::cout << "checking: " << *iter;
        switch(*iter)
        {
          case '-':
            ++iter;
            ++word_begin;
            if (get_current_char_if_can() == '-')
            {

              ++iter;
              auto const next = get_current_char_if_can();
              if (next == ' ' or next == 0)
              {
                --word_begin;
                add_word<detail::args_token_type::entity>();
                std::cout << "adding delimiter: " << result.back().str << '\n';
              }
              else
              {
                std::cout << "was not delimiter: next was: " << (next | as<int>) << '\n';
                ++word_begin;
                add_word<detail::args_token_type::long_opt>();
              }
            }
            else
              add_word<detail::args_token_type::short_opt>();
            break;
          case '=':
            incr_and_add_token<detail::args_token_type::assign>();
            break;
          default:
            if (is_separator())
              incr_and_add_token<detail::args_token_type::separator>();
            else
              add_word<detail::args_token_type::entity>();
        }
      }

      if (word_begin < word_end)
        add_token<detail::args_token_type::entity>();
    }

    return result;
  }
};

std::ostream& operator<<(std::ostream& o, typename detail::args_token_type t) noexcept
{
  using enum detail::args_token_type;
  switch (t)
  {
    case short_opt:
      return o << "short_opt";
    case long_opt:
      return o << "long_opt";
    case assign:
      return o << "assign";
    case separator:
      return o << "separator";
    case entity:
      return o << "entity";
    case comma_arg:
      return o << "comma_arg";
    case _count_:
      return o << "__count__";
    case args_sep:
      return o << "args_sep";
  }
  return o << "_unknown_token_type_:" << std::to_underlying(t);
}

std::ostream& operator<<(std::ostream& o, detail::args_token t) noexcept
{ return o << '{' << t.type << ", " << t.str << '}'; }

} // namespace pspp
