#ifndef PSPP_ARGS_PARSER_H
#define PSPP_ARGS_PARSER_H


#include <pspp/setter.hpp>
#include <pspp/custom_struct_setter.hpp>

#include <pspp/args_parser/args_info_to_res_type_node_converter.hpp>
#include <pspp/args_parser/args_info_part_extractor.hpp>

#include <ranges>


static_val pa_debug = false;


namespace pspp
{

template <auto ... t_separators>
struct args_separators : constr_holder<to_constr<t_separators...>()>
{};

template <bool t_cond, auto t_val>
struct enable_value_if
{ static_val value = t_val; };

template <auto t_val>
struct enable_value_if<false, t_val> {};

struct forbid_short_args_merging {};


template <auto ... t_opts>
struct args_parser
{
	static_assert(sizeof...(t_opts) != 0, "args_parser: options count must be more then 0");

	static_val all_opts = typeseq<decltype(t_opts)...> | remove_cvref;
	static_val short_args_merging_allowed = all_opts | contains<forbid_short_args_merging> | invert;

	static_val global_separator = all_opts | tget_last_like_or<
			is_complex_vtarget_t<args_separators>::is_target,
			args_separators<empty_cstr>> | unwrap;

	static_val opts = all_opts | tremove_like_with_holder<
			is_complex_vtarget_t<args_separators>,
			is_same_as_t<forbid_short_args_merging>
			>;

  std::vector<std::string_view> rest_of_input;

	static constexpr auto parse(int const argc, char const * const * const argv) noexcept
	{
		std::vector<std::string_view> args;
		args.reserve(static_cast<std::size_t>(argc));
		args.shrink_to_fit();

		std::copy(argv, argv + argc, std::back_inserter(args));
		return parse(args);
	}

	static constexpr auto parse(std::ranges::range auto && args) noexcept
		requires (std::same_as<std::remove_cvref_t<decltype(args.front())>, std::string_view>)
	{
		args_tokenizer<global_separator.value> tokenizer{};
		std::vector<detail::args_token_view> tmp;

		auto const & tokenized = tokenizer.tokenize(std::forward<decltype(args)>(args));
		tmp.reserve(tokenized.size());

		for (detail::args_token t : tokenized)
			tmp.emplace_back(t.type, t.str);

		detail::args_to_parse_type to_parse (&tmp.front(), tmp.size());

    if constexpr (pa_debug)
    {
      for (auto [i, arg] : tokenized | std::views::enumerate)
        std::cout << i << ") " << arg << '\n';
    }

    static constexpr auto wrapped = opts | pass_as_values_to<info_scope> | unwrap;
    using Info = args_info_part_handler<
        detail::part_handler_info<short_args_merging_allowed,
        detail::arg_spec_storage_type::not_specified,
        setter_holder<type<result_type>>>{},
        clean_type<wrapped>>;

    std::vector<std::string_view> other_args {};
    result_type result {};

    detail::parse_args<Info::info>(to_parse, result, other_args);

    return std::pair{std::move(result), std::move(other_args)};
	}

private:

	static constexpr auto get_result_type() noexcept
	{
		static constexpr auto transformed = opts | detail::transform_to_res | absorb;

		static_assert(not c_same_as<
				decltype(transformed), empty_type_sequence>,
				"args_parser: passed options results into empty sequence");
		return transformed | pass_to<custom_struct>;
	}

public:
	using result_type = extract_type<get_result_type()>;
};

} // namespace pspp




#endif // PSPP_ARGS_PARSER_H
