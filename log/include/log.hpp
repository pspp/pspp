#ifndef PISOOSPP_PLOG_H
#define PISOOSPP_PLOG_H

#include<source_location>
#include<string>
#include<string_view>

namespace pspp
{
namespace log
{

class logger{
	private:
		struct _use_flags
		{
			private:
			#define ADD_USER_FLAG(_fname) \
				_use_flags* use_ ## _fname ## f();\
				_use_flags* not_use_ ## _fname ## f(); \
				std::string (logger::*_prefix_ ## _fname ## _func_ptr)(const std::source_location*)

			public:
			enum _LOG_USE
			{ 
				TIME, LOCATION, FILE_NAME, FUNCTION_NAME,
				LINE_NUMBER, COLUMN_NUMBER, COUNT
			};


			ADD_USER_FLAG(time);

			ADD_USER_FLAG(location);
			ADD_USER_FLAG(filename);
			ADD_USER_FLAG(funcname);
			ADD_USER_FLAG(line);
			ADD_USER_FLAG(column);


			void _init_use_flag();

			#undef ADD_USER_FLAG
		};

		enum LOG_MODE_C{
			ilog_m, oklog_m, wlog_m, elog_m, unc, uncn, LM_COUNT
		};
		const std::string log_mode_c[LM_COUNT] = {"\033[0m", "\033[32m", "\033[33m", "\033[31m", "\033[0m", "\033[0m\n"};

		
		void _log_nothing(std::string_view message, const std::source_location*);

		bool debug_mode;
		std::string mode;

#define ADD_PREFIX_FUNC_PTR(_fname) \
		std::string _prefix_ ## _fname ## _(const std::source_location*)

		ADD_PREFIX_FUNC_PTR(time);
		ADD_PREFIX_FUNC_PTR(filename);
		ADD_PREFIX_FUNC_PTR(funcname);
		ADD_PREFIX_FUNC_PTR(line);
		ADD_PREFIX_FUNC_PTR(column);

#define ADD_LOG_FUNC_PTR(_fname) \
		void (logger::*_ ## _fname ## _ptr)  (std::string_view message, const std::source_location*);\
		void (logger::*_ ## _fname ## n_ptr)  (std::string_view message, const std::source_location*); \
		void _ ## _fname ## _func (std::string_view message, const std::source_location*); \
		void _ ## _fname ## n_func (std::string_view message, const std::source_location*);


		ADD_LOG_FUNC_PTR(ilog);
		ADD_LOG_FUNC_PTR(wlog);
		ADD_LOG_FUNC_PTR(elog);
		ADD_LOG_FUNC_PTR(oklog);


#undef ADD_LOG_FUNC_PTR

		std::string _prefix_nothing_(const std::source_location*);

		void _init_use_flags();
		std::string _get_time_str() const;
		std::string _get_prefix_str(const _use_flags* _flags, const std::source_location* _loc);



	public:
		_use_flags* ilogf;
		_use_flags* elogf;
		_use_flags* wlogf;
		_use_flags* oklogf;

		_use_flags* log;
		void set_flags_for_all(_use_flags* flags);

		logger();
		logger(const std::string &_mode);

		void log_on ();
		void log_off();

		void ilog   (std::string_view message, std::source_location = std::source_location::current());
		void elog   (std::string_view message,std::source_location = std::source_location::current());
		void wlog   (std::string_view message,std::source_location = std::source_location::current());
		void oklog  (std::string_view message,std::source_location = std::source_location::current());

		void ilogn  (std::string_view message, std::source_location = std::source_location::current());
		void elogn  (std::string_view message,std::source_location = std::source_location::current());
		void wlogn  (std::string_view message,std::source_location = std::source_location::current());
		void oklogn (std::string_view message,std::source_location = std::source_location::current());
}; // class logger

static logger l;

#undef ADD_USER_FLAG
#undef ADD_LOG_FUNC_PTR

} // namespace log
} // namespace pspp

#endif // PISOOSPP_PLOG_H
