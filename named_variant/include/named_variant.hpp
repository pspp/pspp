
#ifndef PSPP_NAMED_VARIANT_HPP
#define PSPP_NAMED_VARIANT_HPP

#include <pspp/meta.hpp>
#include <pspp/custom_struct.hpp>

namespace pspp 
{

template <c_named_custom_value_container ... Datas>
struct named_variant : std::variant<typename Datas::value_type...>
{
	using base = std::variant<typename Datas::value_type...>;
	static_val names = typeseq<typename Datas::key_holder...>;
	static_assert(names | is_unique, "names must be unique");

	template <constr t_name>
	static_val index_res = names | get_hits_of<value_holder<t_name>>;

	template <constr t_name>
	[[nodiscard]] inline constexpr auto & get() noexcept
	{
		static constexpr auto res = index_res<t_name>;

		if constexpr (res.was_found())
			return std::get<res.last()>(*this);
		else
		{ static_assert(pspp::always_false<t_name>(), "name was not found"); return 0; }
	}

	template <constr t_name>
	[[nodiscard]] inline constexpr auto const & get() const noexcept
	{
		static constexpr auto res = index_res<t_name>;

		if constexpr (res.was_found())
			return std::get<res.last()>(*this);
		else
		{ static_assert(pspp::always_false<t_name>(), "name was not found"); return 0; }
	}

	template <constr t_name>
	inline constexpr auto & emplace(auto && value) noexcept
	{
		static constexpr auto res = index_res<t_name>;
		if constexpr (res.was_found())
			return base::template emplace<res.last()>(std::forward<decltype(value)>(value));
		else
		{ static_assert(pspp::always_false<t_name>(), "name was not found"); return 0; }
	}


};

}// namespace pspp

namespace std
{

template <pspp::constr t_name>
inline constexpr auto & get(::pspp::c_complex_target<pspp::named_variant> auto && data) noexcept
{
	return data.template get<t_name>();
}
} // namespace std


#endif // PSPP_NAMED_VARIANT_HPP

