#ifndef PSPP_USROPTS_HPP
#define PSPP_USROPTS_HPP

#include <pspp/compile_time_messages.hpp>

namespace pspp
{

template <typename T>
struct ambiguous_opt_ind
{
	inline static constexpr bool value = false;
};

template <typename T>
concept c_not_ambiguous_opt = not ambiguous_opt_ind<T>::value;


struct defopt_ind {};

template <typename OptT, typename IndT = void>
inline constexpr auto usropt = usropt<OptT, defopt_ind>;

namespace
{
template <typename T>
struct default_option_must_be_specified {};
}

template <c_not_ambiguous_opt T>
inline constexpr auto usropt<T, defopt_ind> = cerror<default_option_must_be_specified<T>{}>{};
}

namespace pops
{
  struct use_static_allocator_for_args {};
  struct acomp_static_heap_size {};
}

#endif // PSPP_USROPTS_HPP
