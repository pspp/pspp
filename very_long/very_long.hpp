#ifndef VERY_LONG_HPP
#define VERY_LONG_HPP

#include <string_view>
#include <string>
#include <bitset>
#include <bit>
#include <concepts>
#include <fmt/format.h>
#include <vector>

namespace v_l_int
{

	template <typename T>
	concept _any_int = requires(T&& t) {
		requires std::convertible_to<T, intmax_t>;
	};

	// 000000000000 dc
	// 000000000001 bt
	// 000000000010 oc
	// 000000000100 hx
	// 000000001000 same_length
	// 000000010000 new_line

	enum str_opt : int
	{
		_none = 			0b00000,
		dec = 				0b00000,
		bin = 				0b00001,
		oct = 				0b00010,
		hex = 				0b00100,
		same_length = 0b01000,
		new_line = 		0b10000,
	};

	//#define VLINT_MAX_STR 20

	class vlint
	{
		public:

			vlint();
	/*		template<typename T>
			requires requires (T t)
			{
				requires _any_int<T>;
			}
			vlint(T&& _num);*/
			vlint(_any_int auto const &&_num);
	//		vlint(const _any_int auto _num);
			vlint(std::string const &_num);
			//operation function
			void sum (const vlint& _b);
			void dif (const vlint& _b);
			void div (const vlint& _n);
			void mult(const vlint& _n);
			void sqrt(const vlint& _n);

			void sum (const _any_int auto& _b);
			void dif (const _any_int auto& _b);
			void div (const _any_int auto& _n);
			void mult(const _any_int auto& _n);
			void sqrt(const _any_int auto& _n);

			static vlint sum (const vlint& _a, const vlint& _b);
			static vlint dif (const vlint& _a, const vlint& _b);
			static vlint div (const vlint& _a, const vlint& _n);
			static vlint mult(const vlint& _a, const vlint& _n);
			static vlint sqrt(const vlint& _a, const vlint& _n);

			static vlint sum (const vlint& _a, const _any_int auto& _b);
			static vlint dif (const vlint& _a, const _any_int auto& _b);
			static vlint div (const vlint& _a, const _any_int auto& _n);
			static vlint mult(const vlint& _a, const _any_int auto& _n);
			static vlint sqrt(const vlint& _a, const _any_int auto& _n);

			std::string str(int options = str_opt::_none, const std::string&& _separator = "") const;
			// operators overloads
			//
			vlint operator+(const vlint& _b);
			vlint operator-(const vlint& _b);
			vlint operator*(const vlint& _b);
			vlint operator/(const vlint& _b);

		private:


			typedef std::vector<uint64_t> _v_l_int;
			typedef char fixed_char;

			static int const _max_length = 19;
//			static uint64_t const _first_bit = 9223372036854775808;

		//	int sign : 1 = 0;
			char sign = '\0';
			void str_to_v_l_int(std::string const &str);

			int degree;
			_v_l_int num;

			std::string _get_format(int _options) const;
			void _replace_with_dec_str(std::string& _str) const;

			std::string bstr(const std::string&& _separator = "") const;
			std::string hstr(const std::string&& _separator = "") const;
			std::string ostr(const std::string&& _separator = "") const;


			size_t sum_dif_degree(const size_t _i, const char _b, const char sign);

			size_t sum_degree (const size_t _i, const fixed_char _b);
			size_t dif_degree (const size_t _i, const fixed_char _b);
			size_t div_degree (const size_t _i, const fixed_char _b);
			size_t mult_degree(const size_t _i, const fixed_char _b);
			size_t sqrt_degree(const size_t _i, const fixed_char _b);

			void fix_degree_sum(const size_t _i, const fixed_char _off);
			void fix_degree_sum_pos(const size_t i, const fixed_char _off);
			void fix_degree_sum_neg(const size_t i, const fixed_char _off);
			void fix_degree_dif(const size_t _i, const fixed_char _off);
			void fix_degree_dif_pos(const size_t _i, const fixed_char _off);
			void fix_degree_dif_neg(const size_t _i, const fixed_char _off);

			std::string _str_with_func(auto _func, const std::string&& separator = "") const;
		/*
			class _OPERATION
			{
				virtual void operation(vlint& _num, size_t _i) ;
			};
			class _OP_PLUS : _OPERATION
			{
				void operation(vlint& _num, size_t _i) override;
			};
			class _OP_MINUS : _OPERATION
			{
				void operation(vlint& _num, size_t _i) override;
			};
		*/
			// check_degree(OPERATION)
	};

	vlint::vlint(const _any_int auto&& _num)
	{
		str_to_v_l_int(
				std::to_string(static_cast<int>(_num))
				);
	}

}

#endif//VERY_LONG_HPP
