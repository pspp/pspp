#include "very_long.hpp"
#include <iostream>

using namespace v_l_int;

vlint::vlint()
{
	num.push_back(0);
}

vlint::vlint(std::string const &_num)
{
	str_to_v_l_int(_num);
}

std::string vlint::_str_with_func(auto _func, const std::string&& _separator) const
{
	std::string res = "";
	res.push_back(sign);

	for (auto i = num.rbegin(); i < num.rend(); i++)
	{
	}

	return "";
}

std::string vlint::_get_format(int _options) const
{
	std::string formats[2][2] = {{"", "#"}, {"", "\n"}};
	std::string f = "{:";
	
	f += formats[0][(bool)(_options & (int)str_opt::same_length)];

	if ( _options & (int)str_opt::hex )
	{
		f.push_back('x');
	}
	else if ( _options & (int)str_opt::oct )
	{
		f.push_back('o');
	}
	else if ( _options & (int)str_opt::bin )
	{
		f.push_back('b');
	}

	f += "}{}";

	f += formats[1][(bool)( _options & (int)str_opt::new_line)];

	return f;
}

void vlint::_replace_with_dec_str(std::string& _str) const
{
	if (_str.length() < 2)
		return;
	if (_str[0] != '0')
		return;

	if (_str[1] == 'x' | _str[1] == 'b')
	{
		return fmt::format(fmt::runtime("{:"+_str[1]+"}"), atoll(_str.c_str()));
	}
}

std::string vlint::str(int _options, const std::string&& _separator) const
{
	std::string res = "";
	res.push_back(sign);

	std::string f = _get_format(_options);

	fmt::print("vlint::str: num size: {}\n", num.size());
	fmt::print("vlint::str: format({}): '{}'\n", _separator, f);
	int j = 0;
	for(auto i = num.rbegin(); i < num.rend(); i++)
	{
		j++;
		res += fmt::format(fmt::runtime(f), *i, _separator);
	}

	fmt::print("vlint::str: j: {}\n", j);
	return res;
}

std::string vlint::bstr(const std::string&& _separator) const
{
/*
	std::string res_str = "";
	res_str.push_back(sign);

	for(auto i = num.rbegin(); i < num.rend(); ++i)
	{
		res_str += fmt::format("{:b}{}", vlint::_first_bit | (*i), _separator);
		res_str[res_str.length() - 65] = '0' + (*i >> 63);
	}

	res_str.erase(res_str.length() - _separator.length());
*/
	return "deleted";
}

void vlint::str_to_v_l_int(std::string const &str)
{
	std::puts("debugging");
	short minus = 0;

	if ( str[0] == '-' )
	{
		minus = 1;
		sign = '-';
	}

	fmt::print("minus: {}\nstr.length(): {}\n", minus, str.length());
	
	std::puts("starting loop");
	for ( int i = str.length() - vlint::_max_length; i >= 0 + minus; i -= vlint::_max_length)
	{
		num.push_back(std::stoull(str.substr(i, vlint::_max_length)));
	}

	if ( str.length() % vlint::_max_length > 0)
		num.push_back(std::stoull(str.substr(0, str.length() % vlint::_max_length)));

	if( *num.end() == 0)
		num.pop_back();
}

void vlint::sum(const vlint& _b)
{
	if (_b.sign != this->sign)
	{
		this->dif(_b);
		return;
	}

	for (int i = 0; auto& num_i : _b.num)
	{
		sum_dif_degree(i++, num_i, _b.sign); 
	}

	/*int i = 0;
	for (auto& num_a : num)
	{
		if ( INTMAX_MAX -  )
		{}
		if( i++ == _b.num.size() )
		{
			break;
		}
	}
	*/
}

void vlint::dif(const vlint& _b)
{
	vlint *longer, *smaller;
	if(_b.sign == this->sign)
	{
		this->sum(_b);
		return;
	}
	for (size_t i = _b.num.size() - 1; i >= 0;)
	{
		i = sum_dif_degree(i, _b.num[i], _b.sign); 
	}
}

void vlint::div(const vlint& _n)
{
}

void vlint::mult(const vlint& _n)
{
}

void vlint::sqrt(const vlint& _n)
{
}


size_t vlint::sum_dif_degree(const size_t _i, const char _b, const char _sign)
{
	if(this->sign != _sign)
	{
		return this->dif_degree(_i, _b);
	}

	return this->sum_degree(_i, _b);
}

size_t vlint::sum_degree(const size_t _i, const fixed_char _b)
{
	if ( _i >= num.size())
	{
		num.push_back(_b);
		return _b;
	}

	num[_i] += _b;

	if (num[_i] > 9)
	{
		sum_degree(_i + 1, 1);
		num[_i] -= 10;
	}

	return num[_i];
}

size_t vlint::dif_degree(const size_t _i, const fixed_char _b)
{
	return 0;
}
size_t vlint::div_degree(const size_t _i, const fixed_char _b)
{
	 return 0;
}
size_t vlint::mult_degree(const size_t _i, const fixed_char _b)
{
	 return 0;
}
size_t vlint::sqrt_degree(const size_t _i, const fixed_char _b)
{
	return 0;
}


void vlint::sum(const _any_int auto& _b)
{
}
void vlint::dif(const _any_int auto& _b)
{
}
void vlint::div(const _any_int auto& _b)
{
}
void vlint::mult(const _any_int auto& _b)
{
}
void vlint::sqrt(const _any_int auto& _b)
{
}

vlint vlint::sum(const vlint& _a, const vlint& _b)
{
	vlint res(_a);

	res.sum(_b);

	return res;
}

vlint vlint::dif(const vlint& _a, const vlint& _b)
{
	vlint res(_a);

	res.dif(_b);

	return res;
}
vlint vlint::div(const vlint& _a, const vlint& _b)
{
	return vlint();
}
vlint vlint::mult(const vlint& _a, const vlint& _b)
{
	return vlint();
}
vlint vlint::sqrt(const vlint& _a, const vlint& _b)
{
	return vlint();
}


vlint vlint::sum(const vlint& _a, const _any_int auto& _b)
{
	return vlint();
}

vlint vlint::dif(const vlint& _a, const _any_int auto& _b)
{
	return vlint();
}

vlint vlint::div(const vlint& _a, const _any_int auto& _b)
{
	return vlint();
}

vlint vlint::mult(const vlint& _a, const _any_int auto& _b)
{
	return vlint();
}

vlint vlint::sqrt(const vlint& _a, const _any_int auto& _b)
{
	return vlint();
}



vlint vlint::operator+(const vlint& b)
{

	for (int i = 0; auto& num_a : num)
	{

		if( i++ == b.num.size() )
		{
			break;
		}
	}

	return vlint();
}

vlint vlint::operator-(const vlint& _b)
{
	return vlint();
}
vlint vlint::operator*(const vlint& _b)
{
	return vlint();
}

vlint vlint::operator/(const vlint& _b)
{
	return vlint();
}

//
/*

void vlint::fix_degree_sum(const size_t _i, const hbyte _b)
{
	num[_i] = _b;

	if ( _i + 1 == num.size())
	{
		num.push_back(1);
	}
	else
	{
		this->sum_degree(_i + 1, 1);
	}
}

void vlint::fix_degree_dif(const size_t _i, const fixed_char _off)
{
	if ( _i + 1 == num.size())
	{
		num.pop_back();

		this->dif_degree(_i - 1, _off);
	}
	else
	{
		this->dif_degree(_i + 1, 1);
//		num[_i] = 10 - _off;
	}
}

*/

