#ifndef PSPP_FLAT_MAP_HPP
#define PSPP_FLAT_MAP_HPP

#include <algorithm>
#include <array>
#include <string_view>
#include <ranges>
#include <utility>
#include <vector>

#include <pspp/compile_time_messages.hpp>
#include <pspp/type_helpers.hpp>


namespace pspp 
{

template <typename KeyT, typename ValueT,
					std::size_t t_size,
					template <typename, typename> class PairT =
						std::pair
					>
struct flat_map
{
	using key_type = KeyT;
	using value_type = ValueT;

	using reference_type = ValueT &;
	using const_reference_type = ValueT const &;


	std::array<PairT<KeyT, ValueT>, t_size> m_data;

	inline constexpr reference_type
		at(key_type const & pos)
	{
		auto res = std::ranges::find_if(m_data, 
				[&pos](auto && iter)
				{
					return iter.first == pos;
				});

		if (res != std::end(m_data))
			return res->second;
		else
			throw std::range_error("not found");
	};

	inline constexpr const_reference_type
		at(key_type const & pos) const
	{
		auto res = std::ranges::find_if(m_data, 
				[&pos](auto && iter)
				{
					return iter.first == pos;
				});

		if (res != std::end(m_data))
			return res->second;
		else
		{
			throw std::range_error("Not Found");
			std::unreachable();
		}
	};

	inline constexpr reference_type
		at_or(key_type const & pos, key_type const & default_value) noexcept
	{
		auto res = std::ranges::find_if(m_data, 
				[&pos](auto && iter)
				{
					return iter.first == pos;
				});

		if (res != std::end(m_data))
			return res->second;
		else
			return default_value;
	};

	inline constexpr const_reference_type
		at_or(key_type const & pos, key_type const & default_value) const noexcept
	{
		auto res = std::ranges::find_if(m_data, 
				[&pos](auto && iter)
				{
					return iter.first == pos;
				});

		if (res != std::end(m_data))
			return res->second;
		else
			return default_value;
	};
};

template <typename KeyT, typename ValueT,
					std::size_t t_size,
					template <typename, typename> class PairT =
						std::pair
				>
struct flat_map_with_default 
{
	using key_type = KeyT;
	using value_type = ValueT;

	using reference_type = ValueT &;
	using const_reference_type = ValueT const &;


	std::array<PairT<KeyT, ValueT>, t_size> m_data;
	PairT<KeyT, ValueT> m_default_value;

	pspp_always_inline constexpr reference_type
		at(key_type const & pos) noexcept
	{
    if consteval {
      auto tmp = std::ranges::find_if(m_data,
                                      [&pos](auto const & iter)
                                      {
                                        return iter.first == pos;
                                      });
      if (tmp == m_data.end())
        return m_default_value.second;
      else return tmp->second;
    }
    else
    {
      return std::ranges::find_if(m_data,
                                  [&pos](auto const & iter)
                                  {
                                    return iter.first == pos;
                                  })->second;
    }
	};

	pspp_always_inline constexpr const_reference_type
		at(key_type const & pos) const noexcept
	{
    if consteval {
      auto tmp = std::ranges::find_if(m_data,
                                  [&pos](auto const & iter)
                                  {
                                    return iter.first == pos;
                                  });
      if (tmp == m_data.end())
        return m_default_value.second;
      else return tmp->second;
    }
    else
    {
      return std::ranges::find_if(m_data,
                                  [&pos](auto const & iter)
                                  {
                                    return iter.first == pos;
                                  })->second;
    }
	}

	pspp_always_inline constexpr key_type
		key_at(const_reference_type value) const noexcept
	{
		return std::ranges::find_if(m_data,
				[&value](auto const & iter)
				{
					return iter.second == value;
				})->first;
	}

  pspp_always_inline constexpr auto &
    data() noexcept
  { return m_data; }

  pspp_always_inline constexpr auto const &
    data() const noexcept
  { return m_data; }

};

template <
    template <typename, typename> class HolderT,
    template <typename, typename> class ... HolderU,
    typename KeyT, typename DataT
    >
flat_map_with_default(HolderT<KeyT, DataT>, HolderU<KeyT, DataT>... other, HolderT<KeyT, DataT>) ->
  flat_map_with_default<KeyT, DataT, sizeof...(other) + 1, HolderT>;

template <
    template <typename, typename> class HolderT,
    typename KeyT, typename DataT, std::size_t t_count
>
flat_map_with_default(std::array<HolderT<KeyT, DataT>, t_count>, HolderT<KeyT, DataT>) ->
flat_map_with_default<KeyT, DataT, t_count, HolderT>;


template <typename T>
struct is_any_flat_map : std::false_type
{};

template <typename DataT, typename ValueT, std::size_t t_size, template <typename, typename> class PairT>
struct is_any_flat_map<flat_map<DataT, ValueT, t_size, PairT>> : std::true_type {};

template <typename DataT, typename ValueT, std::size_t t_size, template <typename, typename> class PairT>
struct is_any_flat_map<flat_map_with_default<DataT, ValueT, t_size, PairT>> : std::true_type {};

template <typename T>
concept c_any_flat_map = is_any_flat_map<std::remove_cvref_t<T>>::value;


} // namespace pspp

namespace std
{
  template <typename DataT, typename ValueT, std::size_t t_size, template <typename, typename> class PairT>
  constexpr std::size_t size(pspp::flat_map<DataT, ValueT, t_size, PairT> const &) noexcept
  { return t_size; }

  template <typename DataT, typename ValueT, std::size_t t_size, template <typename, typename> class PairT>
  constexpr std::size_t size(pspp::flat_map_with_default<DataT, ValueT, t_size, PairT> const &) noexcept
  { return t_size; }

  constexpr auto * begin(pspp::c_any_flat_map auto && map_) noexcept
  { return map_.m_data.begin(); }

  constexpr auto * end(pspp::c_any_flat_map auto && map_) noexcept
  { return map_.m_data.end(); }
}

#endif // PSPP_FLAT_MAP_HPP
