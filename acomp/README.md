

# cross-shell autocompletion lib


work in progress

shells planned to be supported:


already kinda supported:

* sh
* bash
* zsh
* fish
* csh
* tcsh
* yash
* elvish
* xonsh


planned to be:

* pwsh

* nushel
* oil
* ion


## sh | bash | zsh | fish | yash

```
eval "$(./executable)"
```

## csh | tcsh
```
eval `./executable`

set autolist ambiguous # for candidates being displayed
```

## elvish

```elvish
eval (./executable | slurp)
```

## xonsh

```xonsh
execx(./executable)
```


