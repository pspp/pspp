#pragma once

#include <pspp/acomp/base.hpp>

namespace pspp
{

template <>
struct acomp<supported_shells::yash>
{
  template <c_constr_sequence auto t_names, c_sequence auto t_explanations, auto t_defvals>
  static constexpr void complete(static_acomp_input<t_names, t_explanations, t_defvals> info, std::string_view input) noexcept
  {
    if (input.empty())
    {
      info.for_each_explained([]<constr t_name, constr t_expl>{
        print(val<empty_cstr + "-D '" + t_expl + "' '" + t_name + "'#">.value.strv());
      });
    }
    else
    {
      auto subrange = std::views::all | std::views::take(input.size());
      std::size_t printed = 0;
      for (auto [i, name] : info.names_arr
                            | std::views::all
                            | std::views::enumerate
                            | std::views::filter([&subrange, input](auto const & word){
        return ranges::contains_subrange(std::get<1>(word) | subrange, input);
      })
        )
      {
        ++printed;

        print("-D '{}' '{}'#", info.expl_view_at(i), name);
      }
    }
  }

  template <constr t_target>
  static constexpr auto get_completion_script_generator() noexcept
  {
    return [](std::string_view const program_name)
    {
      static constexpr auto completer = '_' + t_target;
      static constexpr auto fmt = empty_cstr + "function completion/" + t_target + R"_( () {{
	export PSPP_COMP_WORDS=""
  export PSPP_TERMINAL="yash"
	typeset i=0
	while [ $i -le ${{WORDS[#]}} ]; do
    if [ -n "${{WORDS[$i]}}" ]; then
		  export PSPP_COMP_WORDS="$PSPP_COMP_WORDS ${{WORDS[$i]}}"
    fi
		i=$((i+1))
	done

	export PSPP_COMP_WORDS="$PSPP_COMP_WORDS $TARGETWORD"

  IFS='#' infos=($({}))
  for info in $infos; do
    eval "complete $info"
  done

	unset PSPP_COMP_WORDS
  unset PSPP_TERMINAL
}}
)_";
      return std::format({fmt.strv()}, program_name);
    };
  }
};

using yash_acomp = acomp<supported_shells::yash>;

}


