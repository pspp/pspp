#pragma once

#include <pspp/acomp/base.hpp>
#include <ranges>
#include <pspp/array.hpp>

namespace pspp
{

template <>
struct acomp<supported_shells::fish>
{
  template <c_constr_sequence auto t_names, c_sequence auto t_explanations, auto t_defvals>
  static constexpr void complete(static_acomp_input<t_names, t_explanations, t_defvals> info, std::string_view input) noexcept
  {
    if (input.empty())
    {
      info.for_each_explained([]<constr t_name, constr t_expl>{
        println(val<t_name + '\t' + t_expl>.value.strv());
      });
    }
    else
    {
      auto subrange = std::views::all | std::views::take(input.size());
      std::size_t printed = 0;
      for (auto [i, name] : info.names_arr
                              | std::views::all
                              | std::views::enumerate
                              | std::views::filter([&subrange, input](auto const & word){
                                return ranges::contains_subrange(std::get<1>(word) | subrange, input);
                              })
                              )
      {
        ++printed;

        println("{}\t{}", name, info.expl_view_at(i));
      }
    }
  }

  template <constr t_target>
  static constexpr auto get_completion_script_generator() noexcept
  {
    return [](std::string_view const program_name)
    {
      static constexpr constr completer = '_' + t_target;
      static constexpr constr fmt = empty_cstr + R"(
#!/usr/bin/fish

function )" + completer + R"(
  set -gx PSPP_TERMINAL fish
  set -gx PSPP_COMP_WORDS (commandline -cp)
  set -gx PSPP_COMP_CWORD (commandline -t)

	set -l response ({})

	for tok in (string split '\n' $response)
		echo -e $tok
	end

  set -e PSPP_TERMINAL
  set -e PSPP_COMP_WORDS
  set -e PSPP_COMP_CWORD
end

complete -f --command )" + t_target + " --arguments '(" + completer + ")'";

      return std::format(fmt.strv(), program_name);
    };
  }
};

using fish_acomp = acomp<supported_shells::fish>;

}