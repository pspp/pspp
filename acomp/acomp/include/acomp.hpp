#pragma once

#include <pspp/acomp/acomp.hpp>
#include <pspp/acomp/base.hpp>
#include <pspp/colors.hpp>
#include <pspp/unsized_constr.hpp>
#include <pspp/temporary_storage.hpp>
#include <pspp/io.hpp>
#include <pspp/simple/string_view.hpp>

#include <pspp/args_parser.hpp>


namespace pspp
{

// autocompletion info:
/*
 *  can have output format? ( opt -- description | opt (description) )
 *
 *  regular argument (can be static):
 *    * can have description (opt)
 *    * can have default arg (opt)
 *    * can have info type:
 *      ** can be enum
 *      ** can be range
 *      ** can be mixed type
 *    * can be dynamicly created -> external info or user input (need to be able to get it)
 *    * can have alias
 *    * can have additional filters
 *    * can have custom prefix/sufix opt=a,b,c,d
 *    * supports -L/lib/path and so on
 *    * can have color output (if supported by shell)
 */

struct acomp_enum
{
  std::vector<std::string_view> vals;
};

struct acomp_col
{
  std::uint8_t r = 0;
  std::uint8_t g = 0;
  std::uint8_t b = 0;

  term_font_type ct = term_font_type::regular;
  bool font_only = false;

  [[nodiscard]] pspp_always_inline
  constexpr auto rgb() const noexcept -> term_rgb
  { return {.r=r, .g=g, .b=b}; }

  [[nodiscard]] pspp_always_inline
  constexpr operator term_rgb() const noexcept
  { return rgb(); }

  [[nodiscard]] pspp_always_inline
  constexpr auto color() const noexcept
  { return ::pspp::color(ct, rgb()); }

  [[nodiscard]] pspp_always_inline
  constexpr auto ft() const noexcept
  { return ::pspp::color(ct); }

  [[nodiscard]] pspp_always_inline
  constexpr auto has_col() const noexcept -> bool
  { return not font_only; }

  constexpr acomp_col() noexcept = default;
  constexpr acomp_col(term_rgb rgb, term_font_type ft = term_font_type::regular) noexcept:
    r{rgb.r}, g{rgb.g}, b{rgb.b}, ct{ft}, font_only{false}
  {}

  constexpr acomp_col(term_font_type ft) noexcept:
    r{nrgb}, g{nrgb}, b{nrgb}, ct{ft}, font_only{true}
  {}

  inline static constexpr std::uint8_t const nrgb = -1 | as<std::uint8_t>;
};

struct acomp_arg_base
{
  simple_string_view arg;
  simple_string_view description = "";
  simple_string_view default_value = "";
  simple_string_view type = "";
};


using simple_acomp_arg = acomp_arg_base;

template <typename PrevT, typename StrT, typename NextT>
struct styled_text
{
  PrevT prev;

  StrT str;
  acomp_col col;

  NextT next;
};

template <typename StrT, typename NextT>
struct styled_text<void, StrT, NextT>
{
  StrT str;
  acomp_col col;

  NextT next;
};

template <typename StrT, typename PrevT>
struct styled_text<PrevT, StrT, void>
{
  PrevT prev;

  StrT str;
  acomp_col col;
};

template <typename StrT>
struct styled_text<void, StrT, void>
{
  StrT str;
  acomp_col col;
};

template <typename StrT, typename ... StyleTs>
constexpr auto styled(StrT && str, StyleTs && ... styles) -> styled_text<void, StrT, void>
{
  return styled_text<void, StrT, void>{std::forward<StrT>(str), std::forward<StyleTs>(styles)...};
}

template <typename PrevT, typename StrT, typename NextT>
constexpr std::ostream & operator<<(std::ostream & o, styled_text<PrevT, StrT, NextT> const & str) noexcept
{
  if constexpr (not std::same_as<PrevT, void>)  o << str.prev;

  if (str.col.has_col()) o << str.col.color();
  else o << str.col.ft();

  o << str.str << "\033[0m";

  if constexpr (not std::same_as<NextT, void>)  o << str.next;

  return o;
}

template <typename LeftT, typename StrT>
constexpr auto operator+(LeftT && left, styled_text<void, StrT, void> st) ->
  styled_text<LeftT, StrT, void>
{
  return {
    std::forward<LeftT>(left),
    std::move(st).str,
    std::move(st).col
  };
}

template <typename StrT, typename RightT>
  requires (not c_complex_target<RightT, styled_text>)
constexpr auto operator+(styled_text<void, StrT, void> st, RightT && right) noexcept -> styled_text<void, StrT, RightT>
{
  return {
    std::move(st).str,
    std::move(st).col,
    std::forward<RightT>(right),
  };
}

template <typename LeftT, typename StrT, typename NextT>
constexpr auto operator+(LeftT && left, styled_text<void, StrT, NextT> st) noexcept -> styled_text<LeftT, StrT, NextT>
{
  return {
    std::forward<LeftT>(left),
    std::move(st).str,
    std::move(st).col,
    std::move(st).next,
  };
}


template <typename PrevT, typename StrT, typename RightT>
  requires (not c_complex_target<RightT, styled_text>)
constexpr auto operator+(styled_text<PrevT, StrT, void> st, RightT && right) noexcept -> styled_text<PrevT, StrT, RightT>
{
  return {
    std::move(st).prev,
    std::move(st).str,
    std::move(st).col,
    std::forward<RightT>(right),
  };
}



template <c_cvc_t ... Ts>
constexpr auto aarg(custom_struct<Ts...> info) noexcept
{
  return simple_acomp_arg{
    .arg            = info.template at<"arg">(),
    .description    = info.template at_or<"description">(std::string_view{}),
    .default_value  = info.template at_or<"default">(std::string_view{}),
    .type           = info.template at_or<"type">(std::string_view{}),
  };
}

template <auto t_val, template <auto> class HolderT>
constexpr auto aarg(HolderT<t_val> info) noexcept
{
  return val<simple_acomp_arg{
    .arg            = t_val.template at<"arg">(),
    .description    = t_val.template at_or<"description">(simple_string_view{}),
    .default_value  = t_val.template at_or<"default">(std::string_view{}),
    .type           = t_val.template at_or<"type">(std::string_view{}),
  }>;
}

inline constexpr auto opt1 = aarg(custom_struct{
  "arg"_k         = "opt",
  "description"_k = "my first dynamic static opt"
});

inline constexpr auto opt2 = aarg(val<custom_struct{
  "arg"_k         = constr{"opt"},
  "description"_k = constr{"my first dynamic static opt"},
}>);


template <typename ... ArgTs>
struct static_acomp_argseq
{
  tmpstorage<ArgTs && ...> storage;

};

template <typename T>
concept c_acomp_arg = requires (T t)
{
  t.get_arg();
  t.get_description();
  t.get_default();
  t.get_type();

  t.has_description();
  t.has_default();
  t.has_type();
};

template <typename T>
concept c_aarg_range = requires (T t)
{
  requires std::ranges::range<T>;
  requires c_acomp_arg<std::ranges::range_value_t<T>>;
};

template <typename T>
concept c_aarg_factory = requires (T t){
  { t.get() } -> c_aarg_range;
};

template <typename T>
concept c_aarg_scope = requires (T t)
{
  { t.entry() } -> c_acomp_arg;
};


template <typename ... Ts>
struct acomp_scope
{

};

struct get_subseq_of_aarg_t
{
  template <
};


template <supported_shells t_shell>
struct acomp_impl
{};

}
}
