#pragma once

#include <pspp/acomp/base.hpp>

namespace pspp
{

template <>
struct acomp<supported_shells::elvish> :
  bash_like_aocmp_info<supported_shells::elvish, "elvish", ' '>
{
  using base = bash_like_aocmp_info<supported_shells::elvish, "elvish", ' '>;

  template <c_constr_sequence auto t_names, c_sequence auto t_explanations, auto t_defvals>
  static constexpr void complete(static_acomp_input<t_names, t_explanations, t_defvals> info, std::string_view input) noexcept
  {
    auto const required = get_and_assert_autocomp_required_info<supported_shells::bash>();
    std::size_t const terminal_width = convert_for<std::size_t>(required[required_names::pspp_column]);

    if (required_arg<supported_shells::bash>::found_bad) std::exit(1);

    if (input.empty())
    {
      info.for_each_explained([info, terminal_width]<constr t_name, constr t_expl>{
        static constexpr constr cmd = t_name + "'" + padder<info.longest + 4> + "-- " + t_expl;
        static constexpr constr opt = empty_cstr + "' " + t_name + "";
        print_padded<info.longest + 4, '\0'>(cmd.strv(), terminal_width + 2);
        printiln(opt.strv());
      });
    }
    else
    {
      auto subrange = std::views::all | std::views::take(input.size());
      std::size_t printed = 0;
      for (auto [i, name] : info.names_arr
                            | std::views::all
                            | std::views::enumerate
                            | std::views::filter([&subrange, input](auto const & word){
        return ranges::contains_subrange(std::get<1>(word) | subrange, input);
      })
        )
      {
        ++printed;

        print("-D '{}' '{}'#", info.expl_view_at(i), name);
      }
    }
  }

  template <constr t_target>
  static constexpr auto get_completion_script_generator() noexcept
  {
    return [](std::string_view const program_name)
    {
/*
 * set edit:completion:arg-completer[tmpt] = {|@args|
for c [(my_completer)] {
  eval 'edit:complex-candidate &code-suffix='''' '$c
}
}
 */

      static constexpr auto fmt = empty_cstr + "set edit:completion:arg-completer[" + t_target + R"(] = {{|@args|
  set E:PSPP_COMP_WORDS = ''

  each {{|arg| set E:PSPP_COMP_WORDS = (echo $E:PSPP_COMP_WORDS $arg) }} $args

  set E:PSPP_TERMINAL = elvish
  set E:PSPP_COLUMNS = (tput cols)
  for c [({})] {{
    eval 'edit:complex-candidate &code-suffix='''' &display='$c
  }}
  unset-env PSPP_COMP_WORDS; unset-env PSPP_TERMINAL; unset-env PSPP_COLUMNS
}}
)";
      return std::format({fmt.strv()}, program_name);
    };
  }
};

using elvish_acomp = acomp<supported_shells::elvish>;

}