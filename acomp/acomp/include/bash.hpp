#pragma once

#include <pspp/acomp/base.hpp>

#include <pspp/input_converter.hpp>
#include <pspp/io.hpp>

namespace pspp
{

template <>
inline constexpr auto required_args<supported_shells::bash> = array{
  std::string_view{"PSPP_COLUMNS"}, "PSPP_COMP_WORDS"
};

template <supported_shells t_shell, constr t_shell_name, char t_padder, constr t_delim = "--", constr t_input_name = "">
struct bash_like_aocmp_info
{
  static constexpr constr bash_default_acomp_loader = R"(  export PSPP_COMP_WORDS="${{COMP_WORDS[@]}}"
  export PSPP_COLUMNS=$COLUMNS

  OLDIFS=$IFS

  IFS=$'\n'
  COMPREPLY=($({}))

  IFS="$OLDIFS"
  unset PSPP_COMP_WORDS
  unset PSPP_COLUMNS
  unset PSPP_TERMINAL
  return 0
}}

complete -F )";

protected:
  constexpr static auto spaces = constr{t_padder, val<1024ul>};
  constexpr static std::string_view spaces_view = spaces.strv();

  enum struct required_names{pspp_column, pspp_comp_words};

  static std::string_view get_dynamic_padder(std::size_t const already_inputted, std::size_t const max) noexcept
  {
    if (max <= already_inputted ) return "";
    else return spaces_view.substr(0, max - already_inputted);
  }

  template <std::size_t t_offset, char t_ch = '\n'>
  static void print_padded(std::string_view const msg, std::size_t const terminal_width)
  {
    if (msg.size() > terminal_width)
    {
      print(val<conditional_v<t_ch == '\0', empty_cstr, constr{t_ch}> + "{}">.value.strv(), msg.substr(0, terminal_width));

      std::string_view moved_view = msg.substr(terminal_width);
      std::size_t const usable_width = terminal_width - t_offset;

      moved_view.remove_prefix(moved_view.front() == ' ');
      std::size_t const count = moved_view.size() / usable_width;
      std::size_t skiped = 0;

      static constexpr constr fmt = padder<t_offset> * empty_cstr + "{}";
      static constexpr constr last_fmt = fmt + "{}";

      for ([[maybe_unused]] auto const _ : std::views::iota(0ul, count))
      {
        print(fmt.data(), moved_view.substr(0, usable_width));
        moved_view.remove_prefix(usable_width);
        moved_view.remove_prefix(moved_view.front() == ' ' and ++skiped); // ++skiped -> always true
      }

      print(last_fmt.data(), moved_view, get_dynamic_padder(moved_view.size() + t_offset, terminal_width));
    }
    else
      print(val<conditional_v<t_ch == '\0', empty_cstr, constr{t_ch}> + "{}{}">.value.strv(), msg, get_dynamic_padder(msg.size(), terminal_width));
  }

public:

  template <c_constr_sequence auto t_names, c_sequence auto t_explanations, auto t_defvals>
  static void complete(static_acomp_input<t_names, t_explanations, t_defvals> info, std::string_view input) noexcept
  {
    if constexpr (not t_input_name.empty())
    {
      auto const custom_words = get_custom_previous_words<t_input_name>();
      if (custom_words.size() < 2) input = "";
      else input = custom_words.back();
    }

    auto const required = get_and_assert_autocomp_required_info<supported_shells::bash>();
    std::size_t const terminal_width = convert_for<std::size_t>(required[required_names::pspp_column]);

    if (required_arg<supported_shells::bash>::found_bad) std::exit(1);
    if (input.empty())
    {
      for (std::string_view str : info.template explained_with_arr<t_padder, t_delim>)
        print_padded<info.longest + 4>(str, terminal_width);

      return;
    }

    [=, s=input.size()]<std::size_t ... t_is>(std::index_sequence<t_is...>){
      auto subrange = std::views::all | std::views::take(s);
      array<bool, t_names | get_size> const mask{
        ranges::contains_subrange(info.template word_at<t_is>().strv() | subrange, input)...
      };

      auto get_next_i = [i=0ul, &mask] mutable {
        while(!mask[i++]);
        return i - 1;
      };

      std::size_t const sum = (0 + ... + mask[t_is]);

      if (sum == 1)
      {
        std::size_t const ind = get_next_i();
        printiln(info.word_view_at(ind));
      }
      else
        for (auto i : std::views::iota(0ul, sum))
        {
          std::size_t const ind = get_next_i();
          print_padded<info.longest + 4>(info.template explained_word_view_with_at<t_padder, t_delim>(ind), terminal_width);
        }
    }(std::make_index_sequence<t_names | get_size>{});
  }

  template <constr t_target>
  static constexpr auto get_completion_script_generator() noexcept
  {
    return [](std::string_view const program_name)
    {
      static constexpr auto completer = '_' + t_target;
      static constexpr auto fmt = completer + R"(() {{
  export PSPP_TERMINAL=')" + t_shell_name + R"('
)" + bash_default_acomp_loader + completer + ' ' + t_target;

      return std::format({fmt.strv()}, program_name);
    };
  }
};


template <>
struct acomp<supported_shells::bash> :
  bash_like_aocmp_info<supported_shells::bash, "bash", ' '>
{};

using bash_acomp = acomp<supported_shells::bash>;

} // namespace pspp
