#pragma once

#include <pspp/acomp/base.hpp>
#include <pspp/custom_struct.hpp>

namespace pspp
{

template <>
struct acomp<supported_shells::zsh>
{
  template <c_constr_sequence auto t_names, c_sequence auto t_explanations, auto t_defvals>
  static constexpr void complete(static_acomp_input<t_names, t_explanations, t_defvals> info, std::string_view input) noexcept
  {
    if (input.empty())
    {
      info.for_each_explained([]<constr t_name, constr t_expl>{
        print(val<t_name + "\\:\"" + t_expl + "\" ">.value.strv());
      });
    }
    else
    {
      [=, s=input.size()]<std::size_t ... t_is>(std::index_sequence<t_is...>)
      {
        auto subrange = std::views::all | std::views::take(s);

        array<bool, t_names | get_size> const mask{
          ranges::contains_subrange(info.template word_at<t_is>().strv() | subrange, input)...
        };

        std::size_t const sum = (0 + ... + mask[t_is]);

        auto get_next_i = [i=0ul, &mask] mutable {
          while(!mask[i++]);
          return i - 1;
        };

        if (sum == 1) printi(info.word_view_at(get_next_i()));
        else
        {
          for ([[maybe_unused]] auto _ : std::views::iota(0ul, sum))
          {
            std::size_t const pos = get_next_i();
            print("{}\\:\"{}\" ", info.word_view_at(pos), info.expl_view_at(pos));
          }
        }
      }(std::make_index_sequence<t_names | get_size>{});
    }
  }

  template <constr t_target, constr t_custom_words = "PSPP_COMP_WORDS">
  static constexpr auto get_completion_script_generator() noexcept
  {
    return [](std::string_view const program_name)
    {
      static constexpr auto completer = '_' + t_target;
      static constexpr auto fmt = completer + R"_((){{
  export PSPP_TERMINAL='-zsh'
  export )_" + t_custom_words + R"_(="${{words[@]}}"
  _alternative "args:custom args:(($({})))"
  unset )_" + t_custom_words + R"_(
}}

compdef )_" + completer + ' ' + t_target;

      return std::format({fmt.strv()}, program_name);
    };
  }
};

using zsh_acomp = acomp<supported_shells::zsh>;

}
