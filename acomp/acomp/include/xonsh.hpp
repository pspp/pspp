#pragma once

#include <pspp/acomp/base.hpp>
#include <pspp/acomp/acomp.hpp>

namespace pspp
{

template <>
struct acomp<supported_shells::xonsh>
{
  template <c_constr_sequence auto t_names, c_sequence auto t_explanations, auto t_defvals>
  static constexpr void complete(static_acomp_input<t_names, t_explanations, t_defvals> info, std::string_view input) noexcept
  {
    if (input.empty())
    {
      info.for_each_explained([]<constr t_name, constr t_expl>{
        println(val<t_name + '#' + t_expl>.value.strv());
      });
    }
    else
    {
      auto subrange = std::views::all | std::views::take(input.size());
      std::size_t printed = 0;
      for (auto [i, name] : info.names_arr
                            | std::views::all
                            | std::views::enumerate
                            | std::views::filter([&subrange, input](auto const & word){
        return ranges::contains_subrange(std::get<1>(word) | subrange, input);
      })
        )
      {
        ++printed;

        print("-D '{}' '{}'#", info.expl_view_at(i), name);
      }
    }
  }

  template <constr t_target>
  static constexpr auto get_completion_script_generator() noexcept
  {
    return [](std::string_view const program_name)
    {
      static constexpr auto completer = '_' + t_target;
      static constexpr auto fmt = empty_cstr +
"from xonsh.completers.tools import *\n"
"@contextual_command_completer_for(\"" + t_target + R"_(")
def )_" + completer + R"_(_impl(context):
    $PSPP_COMP_WORDS="dummy"
    $PSPP_TERMINAL="xonsh"
    input=[l for l in !({}) if l != '']
    del $PSPP_COMP_WORDS
    del $PSPP_TERMINAL
    def _get_opt(raw):
        tmp=raw.split('#')
        return RichCompletion(tmp[0], description=tmp[1])

    return {{_get_opt(raw) for raw in input}}

completer add )_" + completer + " " + completer + "_impl";
      return std::format({fmt.strv()}, program_name);
    };
  }
};

using xonsh_acomp = acomp<supported_shells::xonsh>;

}
