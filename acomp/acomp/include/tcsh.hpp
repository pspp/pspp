#pragma once

#include <pspp/acomp/csh.hpp>

namespace pspp
{
template <>
struct acomp<supported_shells::tcsh> :
  csh_acomp
{};

using tcsh_acomp = acomp<supported_shells::tcsh>;

}