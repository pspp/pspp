#pragma once

#include <pspp/acomp/bash.hpp>

namespace pspp
{

template <>
struct acomp<supported_shells::csh> :
  bash_like_aocmp_info<supported_shells::csh, "-sh", '.', ".|.", "COMMAND_LINE">
{
  template <constr t_target>
  static constexpr auto get_completion_script_generator() noexcept
  {
    return [](std::string_view const program_name)
    {
      static constexpr auto completer = '_' + t_target;
      static constexpr auto fmt =  empty_cstr + "alias " + completer +
        R"( 'setenv PSPP_COMP_WORDS "dummy" && setenv PSPP_TERMINAL "-sh" && setenv PSPP_COLUMNS "`tput cols`" && {}';
complete )" + t_target + " 'p/*/`" + completer + "`/'";

      return std::format({fmt.strv()}, program_name);
    };
  }
};

using csh_acomp = acomp<supported_shells::csh>;

}