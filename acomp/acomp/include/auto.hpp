#pragma once

#include <pspp/acomp/base.hpp>
#include <pspp/acomp/sh.hpp>
#include <pspp/acomp/bash.hpp>
#include <pspp/acomp/zsh.hpp>
#include <pspp/acomp/fish.hpp>
#include <pspp/acomp/csh.hpp>
#include <pspp/acomp/tcsh.hpp>
#include <pspp/acomp/ksh.hpp>
#include <pspp/acomp/yash.hpp>
#include <pspp/acomp/elvish.hpp>
#include <pspp/acomp/xonsh.hpp>

namespace pspp
{

template <>
struct acomp<supported_shells::auto_detect>
{
  template <constr t_target>
  static constexpr auto get_completion_script_generator() noexcept
  {
    return [](std::string_view const program_name)
    {
      std::optional const shell = detect_shell();
      if (shell)
      {
        supported_shells const s = shell.value();
        switch (s)
        {
          using enum supported_shells;
          [[unlikely]]
          case sh:
          {
            static constexpr auto tmp_ = sh_acomp::get_completion_script_generator<t_target>();
            return tmp_(program_name);
          }
          case bash:
          {
            static constexpr auto tmp_ = bash_acomp::get_completion_script_generator<t_target>();
            return tmp_(program_name);
          }
          [[likely]]
          case zsh:
          {
            static constexpr auto tmp_ = zsh_acomp::get_completion_script_generator<t_target>();
            return tmp_(program_name);
          }
          case fish:
          {
            static constexpr auto tmp_ = fish_acomp::get_completion_script_generator<t_target>();
            return tmp_(program_name);
          }
            [[unlikely]]
          case tcsh: [[fallthrough]];
            [[unlikely]]
          case csh:
          {
            static constexpr auto tmp_ = csh_acomp::get_completion_script_generator<t_target>();
            return tmp_(program_name);
          }
            [[unlikely]]
          case ksh:
          {
            static constexpr auto tmp_ = ksh_acomp::get_completion_script_generator<t_target>();
            return tmp_(program_name);
          }
            [[unlikely]]
          case yash:
          {
            static constexpr auto tmp_ = yash_acomp::get_completion_script_generator<t_target>();
            return tmp_(program_name);
          }
          [[unlikely]]
          case elvish:
          {
            static constexpr auto tmp_ = elvish_acomp::get_completion_script_generator<t_target>();
            return tmp_(program_name);
          }
          [[unlikely]]
          case xonsh:
          {
            static constexpr auto tmp_ = xonsh_acomp::get_completion_script_generator<t_target>();
            return tmp_(program_name);
          }
          [[unlikely]]
          case auto_detect: std::unreachable();
        }
        std::unreachable();
      }
      else
      {
        printiln("was:", get_shell_name());
        return std::string{};
      }

    };
  }

  template <c_constr_sequence auto t_names, c_sequence auto t_explanations, auto t_defvals>
  static void complete(static_acomp_input<t_names, t_explanations, t_defvals> info, std::string_view const input) noexcept
  {
    std::optional const shell = detect_shell_by_env<"PSPP_TERMINAL">();
    if (shell)
    {
      supported_shells const s = shell.value();

      switch (s)
      {
        using enum supported_shells;
        [[unlikely]] case tcsh: [[fallthrough]];
        [[unlikely]] case csh:
          csh_acomp::complete(info, input);    break;
        [[unlikely]] case ksh: [[fallthrough]];
        [[unlikely]] case sh:  [[fallthrough]];
        case bash:
          bash_acomp::complete(info, input);   break;
        [[  likely]] case zsh:
          zsh_acomp::complete(info, input);    break;
        case fish:
          fish_acomp::complete(info, input);   break;
        [[unlikely]] case yash:
          yash_acomp::complete(info, input);   break;
        [[unlikely]] case elvish:
          elvish_acomp::complete(info, input); break;
        [[unlikely]] case xonsh :
          xonsh_acomp::complete(info, input);  break;
      }
    }
  }
};

using auto_acomp = acomp<supported_shells::auto_detect>;

}

