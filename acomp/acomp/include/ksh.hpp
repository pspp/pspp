#pragma once

#include <pspp/acomp/bash.hpp>

namespace pspp
{

template <>
struct acomp<supported_shells::ksh> :
  bash_like_aocmp_info<supported_shells::ksh, "ksh", '.'>
{};

using ksh_acomp = acomp<supported_shells::ksh>;
}