#pragma once

#include <pspp/acomp/bash.hpp>

namespace pspp
{

template <>
struct acomp<supported_shells::sh>:
  bash_like_aocmp_info<supported_shells::sh, "sh", ' '>
{};

using sh_acomp = acomp<supported_shells::sh>;

}
