#pragma once

// constexpr autocompletion makes no sence, but have this for situations when
// there is some need to have it as constexpr
#ifdef PSPP_ENABLE_CONSTEXPR_AUTOCOMPLETION
#define AUTOCOMP_CONSTEXPR constexpr
#else
#define AUTOCOMP_CONSTEXPR
#endif

#include <string_view>
#include <charconv>
#include <unistd.h>
#include <fstream>

#include <pspp/array.hpp>
#include <pspp/constr.hpp>
#include <pspp/unsized_array.hpp>
#include <pspp/unsized_constr.hpp>
#include <pspp/custom_struct.hpp>
#include <pspp/runtime_handlers_map.hpp>
#include <pspp/io.hpp>
#include <pspp/usropts.hpp>
#include <pspp/static_allocator.hpp>

namespace pspp
{

template <>
inline constexpr auto usropt<pops::use_static_allocator_for_args, defopt_ind> = true;

template <>
inline constexpr auto usropt<pops::acomp_static_heap_size, defopt_ind> =
  usropt<pops::use_static_allocator_for_args> ? 1024 * 5 : 0;

namespace
{
inline constinit static array<std::byte, usropt<pops::acomp_static_heap_size>> acomp_heap {};
}

enum struct supported_shells {
  auto_detect,
  sh,
  bash,
  zsh,
  fish,
  yash,
  xonsh,
  elvish,

  // kinda supported
  csh,     // possible but the neck is auto-tokenizing with ' ' as delimiter
           // works if save output to val `set input="`./tmp`"; complete tmpt 'p/*/$input'`
  tcsh,    // same as above?

  //unsupported
  ksh,     // maybe possible, but do not managed to make complete work

  nush, // cannot be done through eval $(...)
  pwsh, // same
  oil,  // dnkw
  ion,  // dnkw
  // ...
};

inline constexpr custom_struct supported_shells_info {
  "-zsh"_k   = supported_shells::zsh,
  "zsh"_k    = supported_shells::zsh,
  "bash"_k   = supported_shells::bash,
  "fish"_k   = supported_shells::fish,
  "sh"_k     = supported_shells::sh,
  "yash"_k   = supported_shells::yash,

  "-sh"_k    = supported_shells::csh,
  "-csh"_k   = supported_shells::tcsh,
  //"ksh"_k    = supported_shells::ksh,
  "xonsh"_k  = supported_shells::xonsh,
  "elvish"_k = supported_shells::elvish,
};


template <supported_shells t_shel>
inline constexpr auto required_args = array<std::string_view, 0>{};

template <supported_shells t_shel>
using required_args_t = std::remove_cvref_t<decltype(required_args<t_shel>)>;

inline constexpr struct runtime_getenv_t
{
  using result_type = char const *;

  [[nodiscard]]
  result_type pspp_static_call_const_operator(char const * var_name) noexcept
  { return std::getenv(var_name); }

  template <std::size_t t_n>
  [[nodiscard]]
  result_type pspp_static_call_const_operator(constr<t_n> const & var_name) noexcept
  { return std::getenv(var_name.data()); }

} runtime_getenv {};

template <constr t_str = "COMP_CWORD", typename GetEnvFuncT = runtime_getenv_t const &>
AUTOCOMP_CONSTEXPR std::size_t get_current_word_index(GetEnvFuncT && get_env_func = runtime_getenv) noexcept
{
  std::size_t res{};

  char const * const cword_str = get_env_func(t_str.data());

  if (cword_str != nullptr)
  {
    std::string_view const view {cword_str};

    std::ignore = std::from_chars(view.begin(), view.end(), res);
    return res;
  }
  else return 0;
}

/**
 * @warning WORKS ONLY IF USER MANUALLY EXPORTED PSPP_COMP_WORDS
 * @tparam t_custom_words_name name for manually specified words. Default value is `PSPP_COMP_WORDS`
 * @return array of previous inputed values
 */
template <constr t_custom_words_name = "PSPP_COMP_WORDS", typename GetEnvFuncT = runtime_getenv_t const &>
AUTOCOMP_CONSTEXPR std::vector<std::string_view> get_custom_previous_words(GetEnvFuncT && get_env_func = runtime_getenv) noexcept
{
  char const * const str = get_env_func(t_custom_words_name.data());

  if (str == nullptr) return {};
  else
  {
    std::vector<std::string_view> strs {};
    std::string_view const str_view {str};
    for (auto const part : str_view | std::views::split(' '))
    {
      std::string_view const tmp {part};
      if (not tmp.empty())
        strs.push_back(tmp);
    }

    if (str_view.back() == ' ') strs.emplace_back();

    return strs;
  }
}


template <supported_shells t_shel>
struct required_arg
{
  std::string_view res;
  inline static bool found_bad = false;

  AUTOCOMP_CONSTEXPR required_arg() noexcept = default;

  template <auto t_pos, template <auto> class HolderT >
  AUTOCOMP_CONSTEXPR required_arg(char const * val, HolderT<t_pos>) noexcept:
    res { val ? val : "" }
  {
    if (not val)
    {
      println("ERROR: autocompletion was not set properly: `{}' "
              "was `nullptr'; set it to proper value in autocompletion script file.", required_args<t_shel>[t_pos]);

      found_bad = true;
    }

    else res = std::string_view{val};
  }
};

template <supported_shells t_shel, typename GetEnvFuncT = runtime_getenv_t const &>
AUTOCOMP_CONSTEXPR
required_args_t<t_shel> get_and_assert_autocomp_required_info(GetEnvFuncT && getenv_func = runtime_getenv) noexcept
{
  return [getenv_func = std::forward<GetEnvFuncT>(getenv_func)]<std::size_t ... t_is>(std::index_sequence<t_is...>){
    required_args_t<t_shel> res {
      required_arg<t_shel>(getenv_func(required_args<t_shel>[t_is].data()), val<t_is>).res ...
    };

    //if (required_arg<t_shel>::found_bad) std::exit(EXIT_FAILURE);
    return res;
  }(std::make_index_sequence<type<required_args_t<t_shel>> | get_size>{});
}


template <typename DefValT>
struct compword
{
  std::string_view word;
  std::string_view explanation;

  DefValT default_values;
};

struct simple_compword
{
  std::string_view word;
  std::string_view explanation;
};

template <std::size_t t_n>
struct simple_acomp_input
{
  array<simple_compword, t_n> values;

  template <std::size_t t_i>
  constexpr std::string_view word_at() const noexcept
  { return values[t_i].word; }

  template <std::size_t t_i>
  constexpr std::string_view expl_at() const noexcept
  { return values[t_i].explanation; }
};

template <constr t_name, constr t_explanation, auto t_defval = nulltv>
struct static_acomp_info
{
  static_prop_of constr name = t_name;
  static_prop_of constr explanation = t_explanation;
  static_val defval = t_defval;
};


template <c_constr_sequence auto t_names, c_sequence auto t_explanations, auto t_defvals>
struct static_acomp_input
{
  static_size longest = (t_names | most<[]<auto t_val>{ return t_val.size(); }, greater>).length();

  template <std::size_t t_i>
  constexpr static auto word_at() noexcept
  { return t_names | at<t_i>; }

  static_size size = t_names | get_size;

  static constexpr std::string_view word_view_at(std::size_t i) noexcept
  {
    static constexpr auto arrview = constexpr_array{template_unsized_constr_array{t_names}};
    return arrview[i].strv();
  }

  template <char t_padder = ' '>
  static constexpr std::string_view expl_view_at(std::size_t i) noexcept
  {
    static constexpr auto arrview = []<std::size_t ... t_is>(std::index_sequence<t_is...>)
    {
      return constexpr_array{
        template_unsized_constr_array<
          expl_at<t_is, t_padder>()...
        >{}
      };
    }(std::make_index_sequence<size>{});
    return arrview[i].strv();
  }

  template <std::size_t t_i, char t_padder = ' '>
  constexpr static auto expl_at() noexcept
  {
    if constexpr (t_padder == ' ') return t_explanations | at<t_i>;
    else
    {
      static constexpr auto expl_ = t_explanations | at<t_i>;

      return constr<expl_.size()>(
        expl_ | std::views::transform([](char const c){
        if (c == ' ') return t_padder;
        else return c;
      }));
    }
  }

  template <std::size_t t_i, char t_padder, constr t_delim = "--", std::size_t t_max_length = npos>
  constexpr static auto explained_word_at() noexcept
  {
    return word_at<t_i>() * padder<longest + 1, t_padder> + t_delim + t_padder + expl_at<t_i, t_padder>();
  }

  static constexpr auto explained_arr = []<std::size_t ... t_is>(std::index_sequence<t_is...>){
    return array<std::string_view, t_names | get_size>{
      val<explained_word_at<t_is, ' '>()>.value.strv()...
    };
  }(std::make_index_sequence<t_names | get_size>{});

  template <char t_padder, constr t_delim = "--">
  static constexpr auto explained_with_arr = []<std::size_t ... t_is>(std::index_sequence<t_is...>){
    return array<std::string_view, sizeof...(t_is)>{
      val<explained_word_at<t_is, t_padder, t_delim>()>.value.strv()...
    };
  }(std::make_index_sequence<t_names | get_size>{});

  static constexpr auto names_arr = []<std::size_t ... t_is>(std::index_sequence<t_is...>){
    return array<std::string_view, t_names | get_size>{
      val<word_at<t_is>()>.value.strv()...
    };
  }(std::make_index_sequence<t_names | get_size>{});



  static std::string_view explained_word_view_at(std::size_t i) noexcept
  {
    return explained_arr[i];
  }

  template <char t_padder, constr t_delim = "--">
  static std::string_view explained_word_view_with_at(std::size_t i) noexcept
  {
    return explained_with_arr<t_padder, t_delim>[i];
  }

  template <typename FuncT>
  static constexpr void for_each_explained(FuncT && func) noexcept
  {
    [&func]<std::size_t ... t_is>(std::index_sequence<t_is...>)
    {
      (
        ...,
        func.template operator()<t_names | at<t_is>, t_explanations | at<t_is>>()
      );
    }(std::make_index_sequence<t_names | get_size>{});
  }

  constexpr static_acomp_input() = default;
  constexpr static_acomp_input(c_constr_sequence auto names, c_sequence auto explanations, auto defvals = nulltv)
  {}

  template <c_complex_vtarget<static_acomp_info> ... Ts>
  constexpr static_acomp_input(Ts ...) {}
};

template <c_complex_vtarget<static_acomp_info> ... Ts>
static_acomp_input(Ts...) -> static_acomp_input<
  cstrseq<Ts::name...>, cstrseq<Ts::explanation...>, valseq<Ts::defval...>
>;


template <c_constr_sequence NamesT, c_sequence ExplsT, typename DefvalsT>
static_acomp_input(NamesT, ExplsT, DefvalsT) -> static_acomp_input<NamesT{}, ExplsT{}, DefvalsT{}>;

std::string get_shell_name() noexcept {
  pid_t ppid = getppid();
  std::string cmdline = "/proc/" + std::to_string(ppid) + "/cmdline";

  std::ifstream cmdFile(cmdline);

  std::string res;
  if (cmdFile.is_open()) {
    std::getline(cmdFile, res, '\0');
  }

  return res;
}

template <auto t_shells_info>
static constexpr auto shell_ind_extractor = runtime_handlers_map{
  template_unsized_constr_array{t_shells_info.keys},
  handler<simple_overloaded_lambda{
    []<constr t_key>(value_holder<t_key>)
    {
      return std::optional{t_shells_info.template at<t_key>()};
    },
    []<c_nullt>()
    { return std::optional<
        decltype(auto{t_shells_info.template get<0>()})
      >{std::nullopt}; }
  }>()
};

template <auto t_shells_info = supported_shells_info>
constexpr auto detect_shell() noexcept
{
  std::string const shell_name = get_shell_name();
  return shell_ind_extractor<t_shells_info>.call(std::string_view{shell_name});
}

template <constr t_saved_name, auto t_shells_info = supported_shells_info>
constexpr auto detect_shell_by_env() noexcept
{
  std::string_view shell_name = runtime_getenv(t_saved_name);
  return shell_ind_extractor<t_shells_info>.call(shell_name);
}


template <supported_shells t_shel>
struct acomp
{};



struct acomp_arg
{
  simple_string_view arg;
  simple_string_view description = "";
  simple_string_view default_value = "";
  simple_string_view type = "";

};

} // namespace pspp


