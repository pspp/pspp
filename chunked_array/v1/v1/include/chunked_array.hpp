#ifndef PSPP_CHUNKED_ARRAY_HPP
#define PSPP_CHUNKED_ARRAY_HPP

#include <array>
#include <vector>
#include <valarray>
#include <memory>

#include <pspp/meta.hpp>
#include <pspp/data_holder.hpp>
#include <pspp/utility/utility.hpp>

namespace pspp
{

struct chunked_index 
{
	std::size_t chunk_index;
	std::size_t data_index;

	template <std::size_t t_chunk_size>
	inline constexpr chunked_index minus(auto offset) const noexcept
	{
		std::size_t off = static_cast<std::size_t>(offset);
		return data_index < off ? 
			chunked_index{chunk_index - 1, t_chunk_size - off + data_index} :
			chunked_index{chunk_index, data_index - off};
	}
};




namespace chunked_array_options
{

struct store_indexes_as_vector {};
struct store_indexes_as_valarray {};

struct allow_diferent_sizes {};


template <std::size_t t_allocation_size>
struct use_static_allocation_size : value_holder<t_allocation_size>
{};

struct use_dynamic_constant_allocation_size{};
struct use_dynamic_volatile_allocation_size{};

namespace chunk_type
{

template <template <typename, auto> class StorageT>
struct use_static_type
{
	template <typename DataT, std::size_t t_size>
	using type = StorageT<DataT, t_size>;
};

template <auto t_adaptor_func>
struct use_adaptored_static_type
{
	template <typename DataT, std::size_t t_size>
	using type = extract_type<static_call<t_adaptor_func, DataT, t_size>()>;
};

template <template <typename...> class StorageT>
struct use_dynamic_type
{
	template <typename DataT, std::size_t t_size>
	using type = StorageT<DataT>;
};

template <auto t_adaptor_func>
struct use_adaptored_dynamic_type
{
	template <typename DataT, std::size_t t_size>
	using type = extract_type<static_call<t_adaptor_func, DataT>()>;
};

} // namespace chunk_type

namespace detail
{

template <typename DataT>
struct fixed_size_dynamic_array
{
	DataT * m_data;
	std::size_t m_size;

	using reference_type = DataT &;
	using const_reference_type = DataT const &;

	[[nodiscard]] inline reference_type operator[](std::size_t index) noexcept
	{
		return m_data[index];
	}

	[[nodiscard]] inline const_reference_type operator[](std::size_t index) const noexcept
	{
		return m_data[index];
	}

	template <typename OtherT>
	fixed_size_dynamic_array(fixed_size_dynamic_array<OtherT> &) = delete;

	template <typename OtherT>
	fixed_size_dynamic_array(fixed_size_dynamic_array<OtherT> const &) = delete;

	fixed_size_dynamic_array(std::size_t size) :
		m_data{new DataT[size]}, m_size{size}
	{}

	fixed_size_dynamic_array(fixed_size_dynamic_array<DataT> && old):
		m_data{old.m_data},
		m_size{old.m_size}
	{
		old.m_data = nullptr;
		old.m_size = 0;
	}
};

template <typename T>
struct is_chunk_storage_opt : std::false_type { static_flag is_static = false; };

template <template <typename, auto> class StorageT>
struct is_chunk_storage_opt<chunk_type::use_static_type<StorageT>> : std::true_type 
{
	static_flag is_static = true;
};

template <template <typename ...> class StorageT>
struct is_chunk_storage_opt<chunk_type::use_dynamic_type<StorageT>> : std::true_type
{
	static_flag is_static = false;
};

template <auto t_func>
struct is_chunk_storage_opt<chunk_type::use_adaptored_static_type<t_func>> : std::true_type
{
	static_flag is_static = true;
};

template <auto t_func>
struct is_chunk_storage_opt<chunk_type::use_adaptored_dynamic_type<t_func>> : std::true_type
{
	static_flag is_static = false;
};




inline constexpr std::size_t dynamic_size = static_cast<std::size_t>(-1);

template <typename DataT, typename ... Options>
struct chunked_array_options_set
{
	using options = pspp::to_type_sequence<Options...>;

	static_flag is_different_size_allowed = options::template contains<allow_diferent_sizes>;
	using indexes_storage_opt = typename options::template last_succeed_or<
		some_of<store_indexes_as_vector, store_indexes_as_valarray>,
		store_indexes_as_vector>;

	static_flag do_uses_vector_to_store_indexes = std::same_as<store_indexes_as_vector, indexes_storage_opt>;

	static_prop_of std::size_t static_allocation_size =
		options::template last_succeed_or<
			same_as_complex_value<use_static_allocation_size>,
			use_static_allocation_size<dynamic_size>
		>::value;

	static_flag has_static_allocation_size = (static_allocation_size != dynamic_size);

	using chunk_type_opt = typename options::template last_succeed_or<
		satisfies<is_chunk_storage_opt>, 
		std::conditional_t<
			has_static_allocation_size,
			chunk_type::use_static_type<std::array>,
			chunk_type::use_dynamic_type<fixed_size_dynamic_array>
		>
	>;

	static_assert((not has_static_allocation_size and 
				is_chunk_storage_opt<chunk_type_opt>::is_static)
			or has_static_allocation_size,
		"chunked_array: can not have static storage type with dynamic chunk size");

	using chunk_type = typename chunk_type_opt::template type<DataT, static_allocation_size>;
};


template <std::size_t t_one_chunk_size>
struct chunked_array_impl_one_chunk_size_getter
{
	pspp_always_inline consteval std::size_t get_chunk_size() const noexcept
	{
		return t_one_chunk_size;
	}
};

template <>
struct chunked_array_impl_one_chunk_size_getter<dynamic_size>
{
	std::size_t m_dynamic_size;

	pspp_always_inline std::size_t get_chunk_size() const noexcept
	{
		return m_dynamic_size;
	}
};


template <bool t_do_using_vector>
struct different_size_indexes_storage
{
	std::valarray<std::size_t> m_indexes_data;

	constexpr void reinit(std::size_t start_index) noexcept
	{
		m_indexes_data.resize(1, start_index);
	}

	constexpr void add_new_chunk(std::size_t new_chunk_size) noexcept
	{
		auto tmp = m_indexes_data;

		std::size_t new_index = m_indexes_data[m_indexes_data.size() - 1] + new_chunk_size;

		m_indexes_data.resize(
				m_indexes_data.size() + 1, new_index
			);

		std::copy(std::begin(tmp), std::end(tmp), 
				std::begin(m_indexes_data));
	}

	inline constexpr chunked_index get_real_index(std::size_t index) const noexcept
	{
		std::valarray<bool> tmp = m_indexes_data < index;
		std::valarray<unsigned char> comparison_res (
				reinterpret_cast<unsigned char*>(std::begin(tmp)),
				tmp.size()
			);

		std::size_t chunk_index = static_cast<std::size_t>(comparison_res.sum()) - 1;
		return {chunk_index, index - m_indexes_data[chunk_index]};
	}
};

template <>
struct different_size_indexes_storage<true>
{
	std::vector<std::size_t> m_indexes_data {0};

	constexpr void reinit(std::size_t start_index) noexcept
	{
		m_indexes_data.resize(1, start_index);
		m_indexes_data.reserve(5);
	}

	constexpr void add_new_chunk(std::size_t new_chunk_size) noexcept
	{
		m_indexes_data.push_back(new_chunk_size + m_indexes_data.back());
	}

	inline constexpr chunked_index get_real_index(std::size_t index) const noexcept
	{
		std::size_t chunk {-1ul};
		for (std::size_t ind : m_indexes_data)
		{
			chunk += index > ind;
		}

		return {chunk, index - m_indexes_data[chunk]};
	}
};




template <std::size_t t_size>
struct const_size_indexes_storage
{
	std::vector<std::size_t> m_indexes_data {0};

	static_flag is_mod_of_2 = pspp::is_mod_of_2<t_size>();
	static_size power = sqrt<t_size>();

	constexpr void add_new_chunk() noexcept
	{
		m_indexes_data.push_back(t_size + m_indexes_data.back());
	}

	inline constexpr chunked_index get_real_index(std::size_t index) const noexcept
	{
		if constexpr (is_mod_of_2)
		{
			std::size_t chunk_index = index >> power;
			return {chunk_index, index - m_indexes_data[chunk_index]};
		}
		else
		{
			std::size_t chunk_index = index / t_size;
			return {chunk_index, index - m_indexes_data[chunk_index]};
		}
	}
};

template <>
struct const_size_indexes_storage<dynamic_size>
{
	std::size_t m_one_chunk_size;
	std::vector<std::size_t> m_indexes_data {0};

	constexpr void add_new_chunk() noexcept
	{
		m_indexes_data.push_back(m_one_chunk_size + m_indexes_data.back());
	}

	inline constexpr chunked_index get_real_index(std::size_t index) const noexcept
	{
		std::size_t chunk_index = index / m_one_chunk_size;
		return {chunk_index, index - m_indexes_data[chunk_index]};
	}
};

template <bool t_is_static, bool t_different_size_allowed,
					typename DataT, typename ChunkT, std::size_t t_size,
					bool t_uses_vector>
struct chunked_array_impl :
	private const_size_indexes_storage<t_size>
{
	using value_type = DataT;
	using pointer_type = DataT *;
	using const_pointer_type = DataT const *;
	using reference_type = DataT &;
	using rvalue_reference_type = DataT &&;
	using const_reference_type = DataT const &;
	using chunk_iterator = typename std::vector<ChunkT>::iterator;
	using data_iterator = typename pspp::iterator<DataT>;
	using data_view_iterator = typename pspp::iterator<DataT, pspp::iterator_options::view>;

	inline reference_type operator[](auto && val) noexcept
	{
		chunked_index tmp = get_real_index(val);
		return m_chunks[tmp.chunk_index]->operator[](tmp.data_index);
	}

	inline const_reference_type operator[](auto && val) const noexcept
	{
		chunked_index tmp = get_real_index(val);
		return m_chunks[tmp.chunk_index]->operator[](tmp.data_index);
	}

	[[nodiscard]] inline reference_type operator[](chunked_index index) noexcept
	{
			return m_chunks[index.chunk_index]->operator[](index.data_index);
	}

	[[nodiscard]] inline const_reference_type operator[](chunked_index index) const noexcept
	{
			return m_chunks[index.chunk_index]->operator[](index.data_index);
	}

	[[nodiscard]] inline auto begin() noexcept
	{
		return m_accessors.begin();
	}

	[[nodiscard]] inline auto end() noexcept
	{
		return m_accessors.end();
	}

	[[nodiscard]] inline auto begin() const noexcept
	{
		return m_accessors.begin();
	}

	[[nodiscard]] inline auto end() const noexcept
	{
		return m_accessors.end();
	}

	[[nodiscard]] inline reference_type back() noexcept
	{
			return operator[](m_back_index.minus<t_size>(1));
	}

	[[nodiscard]] inline pointer_type back_ptr() noexcept
	{ return std::addressof(operator[](m_back_index.minus<t_size>(1))); }

	[[nodiscard]] inline const_pointer_type back_ptr() const noexcept
	{ return std::addressof(operator[](m_back_index.minus<t_size>(1))); }

	[[nodiscard]] inline const_reference_type back() const noexcept
	{
			return operator[](m_back_index.minus<t_size>(1));
	}
	[[nodiscard]] inline std::shared_ptr<DataT> shared_back() noexcept
	{
			return {&operator[](m_back_index.minus<t_size>(1))};
	}
	[[nodiscard]] inline std::shared_ptr<DataT const> shared_back() const noexcept
	{
			return {&operator[](m_back_index.minus<t_size>(1))};
	}

	[[nodiscard]] inline reference_type front() noexcept
	{
			return m_chunks[0]->front();
	}
	[[nodiscard]] inline const_reference_type front() const noexcept
	{
			return m_chunks[0]->front();
	}

	inline void push_back(rvalue_reference_type new_data) noexcept
	{
			m_chunks[m_back_index.chunk_index]->operator[](m_back_index.data_index) = new_data;
			m_accessors.back().expand_tail();

			if (m_back_index.data_index == t_size - 1)
					allocate_new_chunk();
			else
					m_back_index.data_index++;
	}


	inline void allocate_new_chunk() noexcept
	{
		m_chunks.emplace_back(std::make_shared<ChunkT>());
		m_back_index.chunk_index++;
		m_back_index.data_index = 0;
		m_accessors.emplace_back(*m_chunks.back(), 0);

		add_new_chunk();
	}

	inline void push_back(const_reference_type new_data) noexcept
	{
		m_chunks[m_back_index.chunk_index]->operator[](m_back_index.data_index) = new_data;
		m_accessors.back().expand_tail();

		if (m_back_index.data_index == t_size - 1)
			allocate_new_chunk();
		else
			m_back_index.data_index++;
	}

	inline std::size_t size() const noexcept
	{
		return m_indexes_data.back() + m_back_index.data_index;
	}

	private:
	using const_size_indexes_storage<t_size>::get_real_index;
	using const_size_indexes_storage<t_size>::add_new_chunk;
	using const_size_indexes_storage<t_size>::m_indexes_data;

	std::vector<std::shared_ptr<ChunkT>> m_chunks {std::make_shared<ChunkT>()};
	std::vector<data_holder<DataT>> m_accessors {data_holder<DataT>(m_chunks.front()->begin(), m_chunks.front()->begin())};

	chunked_index m_active_index {0, 0};
	chunked_index m_back_index {0, 0};
};

template <typename DataT, typename ChunkT, std::size_t t_size, bool t_uses_vector>
struct chunked_array_impl<false, true, DataT, ChunkT, t_size, t_uses_vector> : 
	private different_size_indexes_storage<t_uses_vector>
{
	using value_type = DataT;
	using reference_type = DataT &;
	using const_reference_type = DataT const &;
	using chunk_iterator = typename std::vector<ChunkT>::iterator;
	using data_iterator = typename pspp::iterator<DataT>;
	using data_view_iterator = typename pspp::iterator<DataT, pspp::iterator_options::view>;

	private:
	std::vector<std::shared_ptr<ChunkT>> m_chunks;
};

template <typename DataT, typename ChunkT, std::size_t t_size, bool t_uses_vector>
struct chunked_array_impl<false, false, DataT, ChunkT, t_size, t_uses_vector> : 
	private const_size_indexes_storage<t_size>
{
	using value_type = DataT;
	using reference_type = DataT &;
	using const_reference_type = DataT const &;
	using chunk_iterator = typename std::vector<ChunkT>::iterator;
	using data_iterator = typename pspp::iterator<DataT>;
	using data_view_iterator = typename pspp::iterator<DataT, pspp::iterator_options::view>;

	private:
	std::vector<ChunkT&&> m_chunks;
};

template <typename DataT, typename ... Options>
struct chunked_array_holder
{
	using options = chunked_array_options_set<DataT, Options...>;

	using type = chunked_array_impl
	<
		options::has_static_allocation_size, 
		options::is_different_size_allowed,
		DataT,
		typename options::chunk_type,
		options::static_allocation_size, options::do_uses_vector_to_store_indexes
	>;
};

} // namespace detail

} // namespace chunked_array_options

// data storage -> can be custom structure; -> user gives lambda which takes static/dynamic size of memory,
//  and returns data which will be stored; add ability to partialy specialize some function which doing it instead
//  must give ability to access elements (rw)

template <typename DataT, typename ... Options>
struct chunked_array : 
	chunked_array_options::detail::chunked_array_holder<DataT, Options...>::type
{
	using this_type = chunked_array<DataT, Options...>;
	using pure_this_type = chunked_array<DataT>;
}; 

template <typename DataT, std::size_t t_chunk_size, typename ... Options>
using static_size_chunked_array = chunked_array<DataT,
	chunked_array_options::use_static_allocation_size<t_chunk_size>,
	Options...>;



}// namespace pspp

#endif // PSPP_CHUNKED_ARRAY_HPP
