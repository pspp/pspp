#ifndef PSPP_STORAGE_VIEW
#define PSPP_STORAGE_VIEW

#include <ranges>

#include <pspp/useful/static.hpp>
#include <pspp/useful/sequence.hpp>
#include <pspp/useful/concepts.hpp>

namespace pspp
{
inline namespace stv
{

namespace _storage_view_to_be_ignored
{
	template <typename F, typename ...Args>
	concept callable_with_range_res = requires(F f, Args... args)
	{
		f(args...);
		f(args...).size();
		requires std::ranges::range<decltype(f(args...))>;
	};


} // namespace _storage_view_to_be_ignored



template <typename DataT>
struct storage_view
{
	DataT const * _begin;
	DataT const * _end;

	std::size_t _size;

	struct storage_view_iterator_t
	{
		struct bad_iter : storage_view_iterator_t
		{
			constexpr bad_iter() :
				storage_view_iterator_t{nullptr, 0}{};

			constexpr operator storage_view_iterator_t() const
			{
				static_assert(data != nullptr, "trying to convert storage_view::storage_view_iterator_t to storage_view::bad_iter");
			}
		};

		DataT const * data;
		int pos;
		std::size_t const maxpos;

		[[deprecated("storage_view: trying to create const_iterator with reference to void. Specify the type to do it.")]]
		constexpr storage_view_iterator_t(DataT const * _start_data, int _start_pos, std::size_t _max):
			data{_max < _start_pos ? nullptr : _start_data},
			pos{_start_pos},
			maxpos{_max}
		{}

		constexpr storage_view_iterator_t(DataT const * _start_data, int _start_pos, std::size_t _max)
			requires (not std::is_same_v<DataT, void>)
			:
			data{_max < _start_pos ? nullptr : _start_data},
			pos{_start_pos},
			maxpos{_max}
		{}

		friend struct storage_view;

		inline constexpr storage_view_iterator_t& operator=(storage_view_iterator_t to_copy)
		{
			this->pos = to_copy.pos;
			this->data = to_copy.data;

			return *this;
		}

		/*
		constexpr storage_view_iterator_t& operator=(storage_view_iterator_t const & to_copy)
		{
			this->pos = to_copy.pos;
			this->data = to_copy.data;

			return *this;
		}
		*/
		inline constexpr void go_to(std::size_t pos_)
		{
			data = data - pos + pos_;
			pos = pos_;
		}

		inline constexpr storage_view_iterator_t& operator=(std::size_t pos_)
		{
			go_to(pos_);
			return *this;
		}

		inline constexpr storage_view_iterator_t& operator+=(int offset)
		{
			this->pos += offset;
			this->data += offset;

			return *this;
		}


		inline constexpr storage_view_iterator_t& operator+=(unsigned offset)
		{
			this->pos += offset;
			this->data += offset;

			return *this;
		}

		inline constexpr storage_view_iterator_t& operator-=(int offset)
		{
			this->pos -= offset;
			this->data -= offset;

			return *this;
		}

		inline constexpr void operator++()
		{
			data++;
			pos++;
		}

		inline constexpr void operator++(int)
		{
			data++;
			pos++;
		}

		inline constexpr storage_view_iterator_t operator+(int _off) const
		{
			return storage_view_iterator_t{data + _off, pos + _off, maxpos};
		}

		inline constexpr storage_view_iterator_t operator-(int _off) const
		{
			return storage_view_iterator_t{data - _off, pos - _off, maxpos};
		}

		inline constexpr void operator--()
		{
			data--;
			pos--;
		}

		inline constexpr DataT const & operator*()
		{
			static_assert(not std::is_same_v<DataT, void>, "cannot get value from 'void' type storage_view iterator");

			return *(static_cast<DataT const *>(data));
		}


    [[nodiscard]]
		inline constexpr int operator==(storage_view_iterator_t const & _second) const
		{
			return data == static_cast<DataT const *>(_second) && (pos == _second.pos);
		}

    [[nodiscard]]
		inline constexpr int operator!=(storage_view_iterator_t const & _second) const
		{
			return data != static_cast<DataT const *>(_second.data) && (pos != _second.pos);
		}

		inline constexpr storage_view_iterator_t operator++() const
		{
			return storage_view_iterator_t{data + 1, pos + 1, maxpos};
		}

		inline constexpr storage_view_iterator_t operator--() const
		{
			return storage_view_iterator_t{data - 1, pos - 1, maxpos};
		}

		inline constexpr storage_view_iterator_t operator++(int) const
		{
			return storage_view_iterator_t{data + 1, pos + 1, maxpos};
		}

    [[nodiscard]]
    inline constexpr bool operator<(storage_view_iterator_t const & to_compare) const
    {
      return data < to_compare.data;
    }

    [[nodiscard]]
    inline constexpr bool operator>(storage_view_iterator_t const & to_compare) const
    {
      return data > to_compare.data;
    }

    [[nodiscard]]
    inline constexpr std::strong_ordering operator<=>(storage_view_iterator_t const & to_compare) const = default;

    [[nodiscard]]
    inline constexpr bool operator<(int pos_) const
    {
      return pos < pos_;
    }

    [[nodiscard]]
    inline constexpr bool operator==(int pos_) const
    {
      return pos == pos_;
    }

    [[nodiscard]]
    inline constexpr bool operator!=(int pos_) const
    {
      return pos != pos_;
    }

    [[nodiscard]]
    inline constexpr bool operator>(int pos_) const
    {
      return pos == pos_;
    }

    [[nodiscard]]
    inline constexpr bool operator<=(int pos_) const
    {
      return pos <= pos_;
    }

    [[nodiscard]]
    inline constexpr bool operator>=(int pos_) const
    {
      return pos >= pos_;
    }

    [[nodiscard]]
    inline constexpr std::strong_ordering operator<=>(int pos_) const
    {
      return pos <=> pos_;
    }

    [[nodiscard]]
    inline constexpr operator bool() const
    {
      return (pos < maxpos) & (data != nullptr);
    }

    [[nodiscard]]
    inline constexpr bool isdata_ok() const
    {
      return data != nullptr;
    }


    [[nodiscard("no effect when discarding")]]
		DataT const & cref() const
		{
			return *data;
		}
	};

	public: 

	using const_iterator = storage_view_iterator_t;
	using iterator = storage_view_iterator_t;

	template <class It, class End>
	constexpr storage_view(It begin, End end) : 
		_begin{begin},
		_end{end},
		_size{static_cast<std::size_t>(end - begin)}
	{}

	template <std::ranges::range Range>
	constexpr storage_view(Range const & from) :
		_begin{from.data()},
		_end{from.data() + from.size()},
		_size{static_cast<std::size_t>(from.end() - from.begin())}
	{}

	template <std::size_t pos, typename ReturnDataT = DataT>
	constexpr ReturnDataT const & at() const
	{
		static_assert(pos < _size, "storage_view: at: pos must be lower than storage size");
		static_assert(not std::is_same_v<ReturnDataT, void>, "storage_view: at: return type cannot be void. Default type was not specified while initialization and must be specified at `at` using");
		static_assert((static_cast<ReturnDataT const *>(_begin) + pos) != nullptr, "storage_view: at: returning elements pointer is pointing nullptr");

		return *(static_cast<ReturnDataT const *>(_begin) + pos);
	}

	template <typename ReturnDataT = DataT>
	ReturnDataT const & at (std::size_t pos) const
	{
		static_assert(not std::is_same_v<ReturnDataT, void>, "storage_view: at: return type cannot be void. Default type was not specified while initialization and must be specified at `at` using");

		return *(static_cast<ReturnDataT const *>(_begin) + pos);

	}

	template <std::size_t pos, typename ReturnDataT>
	[[deprecated("storage_view: at: trying to get value with type different from its data declared type is an unpredictable behaviour")]]
	constexpr ReturnDataT const & at() const requires(not std::is_same_v<ReturnDataT, DataT> and not std::is_same_v<DataT, void>)
	{
		return at<pos, ReturnDataT>();
	}

	template <std::ranges::range auto data, typename ToCreateDataT = DataT>
	static consteval storage_view<ToCreateDataT> make_static_storage_view()
	{
		/* constexpr auto & _static_ref_todata = pspp::usf::make_static<data>(); */
		/* return storage_view<ToCreateDataT>(_static_ref_todata.begin(), _static_ref_todata.end()); */
		return storage_view<ToCreateDataT>{data.begin(), data.end()};
	}

	template <std::ranges::range auto data, typename ToCreateDataT = DataT>
	static consteval storage_view<ToCreateDataT> make_static_storage_view() 
	requires (std::is_same_v<std::remove_cvref<decltype(data)>, std::array<ToCreateDataT, data.end() - data.begin()>>)
	{
		constexpr auto & _static_ref_to_data = pspp::usf::make_static<data>();
		return storage_view<ToCreateDataT>(_static_ref_to_data.begin(), _static_ref_to_data.end());
	}

	template <typename ToCreateDataT, auto ... data>
	static consteval storage_view<ToCreateDataT> make_static_storage_view_from()
	{
		using first_t = decltype(pspp::usf::get_first_from_seq<data...>());
		if constexpr (not std::is_same_v<ToCreateDataT, void>)
			pspp::usf::static_apply_for_each(
				[&](auto const & arg){ 
				static_assert(std::is_convertible_v<decltype(arg), ToCreateDataT>,
					"make_static_storage_view: all non-template arguments must be the same or convertible type");
				}, data...);
		else
		{}
		if constexpr(not std::is_same<ToCreateDataT, void>()) 
			static_assert (std::is_same<decltype(pspp::usf::get_first_from_seq<data...>()), ToCreateDataT>(), "make_static_storage_view: non-template arguments `auto ...data` type must be the same as was specified by user");

		constexpr std::array to_be_created{data...};
		return make_static_storage_view<to_be_created, ToCreateDataT>();
	}

	
	template <auto ... data>
	inline static consteval storage_view<DataT> make_static_storage_view_from()
	{
		return make_static_storage_view_from<DataT, data...>();
	}

	constexpr std::size_t size() const
	{
		return _size;
	}

	template <pspp::usf::concepts::not_void ItersDataT = DataT>
	constexpr auto begin() const
	{
		return const_iterator{static_cast<ItersDataT const *>(_begin), 0, _size};
	}

	template <pspp::usf::concepts::not_void ItersDataT = DataT>
	constexpr auto end() const
 	{
		return const_iterator{static_cast<ItersDataT const *>(_end) + 1, _size, _size};
	}
};

template <std::ranges::range auto data, typename ToCreateDataT = void>
inline consteval storage_view<ToCreateDataT> make_static_storage_view()
{
	return storage_view<ToCreateDataT>(data.begin(), data.end());
}


template <typename ToCreateDataT, auto ... data>
inline consteval storage_view<ToCreateDataT> make_static_storage_view_from_data()
{
	return storage_view<ToCreateDataT>::template make_static_storage_view_from<data...>();
}

template <typename ToCreateDataT, auto ...args>
inline consteval storage_view<ToCreateDataT> make_static_storage_view_from_callable(auto callable)
{
	constexpr auto _tmp = pspp::usf::to_static_array<ToCreateDataT, args...>(callable);
	return storage_view<ToCreateDataT>::template make_static_storage_view<_tmp, ToCreateDataT>();
}


template <std::size_t max_size, typename ToCreateDataT, auto ...args>
inline consteval storage_view<ToCreateDataT> make_static_storage_view_from_callable(auto callable)
{
	constexpr auto _tmp = pspp::usf::to_static_array<max_size, ToCreateDataT, args...>(callable);

	return storage_view<ToCreateDataT>::template make_static_storage_view<_tmp, ToCreateDataT>();
}

template <auto ... data>
inline consteval storage_view<void> make_static_storage_view_from_data()
{
	return storage_view<void>::template make_static_storage_view_from<data...>();
}


template <std::size_t max_size, typename ToCreateDataT, auto ...args>
inline consteval storage_view<ToCreateDataT> make_storage_view_from_callable(auto callable)
{
	constexpr auto _tmp = pspp::usf::to_right_sized_array<max_size, ToCreateDataT, args...>(callable);

	return storage_view<ToCreateDataT>{_tmp.begin(), _tmp.end()};
}

template <typename ToCreateDataT, auto ...args>
inline consteval storage_view<ToCreateDataT> make_storage_view_from_callable(auto callable)
{
	constexpr auto _tmp = pspp::usf::to_right_sized_array<ToCreateDataT, args...>(callable);

	return storage_view<ToCreateDataT>{_tmp.begin(), _tmp.end()};
}


} // namespace stv
} // namespace pspp

#endif // PSPP_STORAGE_VIEW


