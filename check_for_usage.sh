#!/usr/bin/bash

function check {
	for f in "$1"/*; do
		if [ -d "$f" ]; then
			check "$f"
		else
			if [ -a "$f" ]; then 
				if grep -q "<ranges>" "$f" ; then
					echo ">>>$f"
				fi
			fi
			# grep const_str "$f"
		fi
	done
}


check "$1" "$2"
