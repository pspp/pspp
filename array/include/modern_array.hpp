#ifndef PSPP_ARRAY_CONVERTION_HPP
#define PSPP_ARRAY_CONVERTION_HPP

#include <pspp/array.hpp>
#include <pspp/modern_main.hpp>

namespace pspp
{

template <typename DataT, std::size_t t_n>
struct range_argument_info<array<DataT, t_n>> :
	static_size_range_argument_info_spec<array, DataT, t_n>
{};

} // namespace pspp


#endif // PSPP_ARRAY_CONVERTION_HPP
