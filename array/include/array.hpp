#ifndef PSPP_TYPED_INDEX_ARRAY
#define PSPP_TYPED_INDEX_ARRAY

#include <array>
#include <functional>
#include <pspp/adaptors.hpp>
#include <pspp/utility/declaration_macros.hpp>
#include <pspp/utility/utility.hpp>
#include <pspp/defops.hpp>
#include <ranges>

namespace pspp
{

template <typename T>
concept castable_to_size_t = requires (T t)
{
	static_cast<std::size_t>(t);
};

template <typename DataType,
					std::size_t t_size
				>
class array :
	public std::array<DataType, t_size>
{
	using base = std::array<DataType, t_size>;

	public:
	using base::operator[];

	[[nodiscard, pspp_attr_always_inline]] inline constexpr typename base::reference
		operator[](castable_to_size_t auto && pos) noexcept
	{
		return operator[](static_cast<std::size_t>(pos));
	}

	[[nodiscard, pspp_attr_always_inline]] inline constexpr typename base::const_reference 
		operator[](castable_to_size_t auto && pos) const noexcept
	{
		return operator[](static_cast<std::size_t>(pos));
	}

  template <castable_to_size_t auto t_ind>
  [[nodiscard, pspp_attr_always_inline]] inline constexpr typename base::const_reference
    operator[](value_holder<t_ind>) const noexcept
  {
    return operator[](static_cast<std::size_t>(t_ind));
  }

  template <castable_to_size_t auto t_ind>
  [[nodiscard, pspp_attr_always_inline]] inline constexpr typename base::reference
  operator[](value_holder<t_ind>) noexcept
  {
    return operator[](static_cast<std::size_t>(t_ind));
  }

  template <auto t_i, auto ... t_is>
  [[nodiscard]] pspp_always_inline constexpr auto &
    at() noexcept
  {
    if constexpr (sizeof...(t_is) == 0)
      return operator[](t_i);
    else
      return operator[](t_i).template at<t_is...>();
  }


  template <auto t_i, auto ... t_is>
  [[nodiscard]] pspp_always_inline constexpr auto const &
  at() const noexcept
  {
    if constexpr (sizeof...(t_is) == 0)
      return operator[](t_i);
    else
      return operator[](t_i).template at<t_is...>();
  }
};

template <typename D, typename ... U>
array(D, U...) -> array<D, sizeof...(U) + 1>;

template <typename NewDataT, typename PrevDataT, std::size_t t_size, typename ConvertFunT = decltype(as<NewDataT>)>
constexpr auto array_cast(array<PrevDataT, t_size> const & data, ConvertFunT && convfun = as<NewDataT>) -> array<NewDataT, t_size>
{
  if constexpr (std::same_as<NewDataT, PrevDataT>)
    return data;
  else
    return [&]<std::size_t ... t_is>(std::index_sequence<t_is...>){
      return array{std::forward<ConvertFunT>(convfun)(data[t_is])...};
    }(std::make_index_sequence<t_size>{});
}

template <typename T>
concept c_static_array_like = requires (T t) {
  t | get_size;
  t.template get<0>();
};

template <typename NewDataT, c_static_array_like ArrLikeT, typename ConvFunT = decltype(as<NewDataT>)>
constexpr auto array_cast(ArrLikeT && arr_like, ConvFunT && convfun = as<NewDataT>) noexcept
{
  return [&]<std::size_t ... t_is>(std::index_sequence<t_is...>){
    return array{std::forward<ConvFunT>(convfun)(std::forward<ArrLikeT>(arr_like).template get<t_is>())...};
  }(make_index_sequence_from_size_of(arr_like));
}


namespace detail 
{

template <auto t_executor, typename T, typename U, std::size_t t_s1, std::size_t t_s2, 
				 auto t_min = std::min(t_s1, t_s2), auto t_max = std::max(t_s1, t_s2)>
constexpr auto execute_for_arrs(std::array<T, t_s1> arr1, std::array<U, t_s2> arr2) noexcept
	-> array<common_type_for_bin_t<t_executor, T, U>, t_max>
	requires (t_max % t_min == 0)
{
	static constexpr bool first_bigger = t_max == t_s1;
	static constexpr auto left_i = [](std::size_t i){
		if constexpr (first_bigger) return i;
		else return i % t_min;
	};
	static constexpr auto right_i = [](std::size_t i){
		if constexpr (first_bigger) return i % t_min;
		else return i;
	};

	return [&arr1, &arr2]<std::size_t ... t_is>(std::index_sequence<t_is...>){
		return array{
			t_executor(arr1[left_i(t_is)], arr2[right_i(t_is)])...
		};
	}(std::make_index_sequence<t_max>{});
}

} // namespace detail

template <typename T>
struct is_array : std::false_type {};

template <typename T, std::size_t t_i>
struct is_array<array<T, t_i>> : std::true_type {};

template <typename T>
concept c_array = is_array<std::remove_cvref_t<T>>::value;

template <typename T>
struct array_data_type : std::type_identity<T> {};

template <typename T, std::size_t t_i>
struct array_data_type<array<T, t_i>> : std::type_identity<T> {};

template <typename T>
using array_data_type_t = typename array_data_type<T>::type;

template <typename DataT, std::size_t t_size>
struct define_ops<array<DataT, t_size>> :
	enable_all_ops,
	ops_mediator<
		[]<auto t_func>(auto && first)
		{
			return [f=pspp_fwd(first)]<std::size_t ... t_is>(std::index_sequence<t_is...>) {
				return array{
					t_func(f[t_is])...
				};
			}(std::make_index_sequence<t_size>{});
		},
		[]<auto t_func>(c_array auto && first, c_array auto && second)
		{
			return detail::execute_for_arrs<t_func>(pspp_fwd(first), pspp_fwd(second));
		},
		[]<auto t_func>(auto && first, std::convertible_to<DataT> auto && second)
		{ return detail::execute_for_arrs<t_func>(pspp_fwd(first), array{pspp_fwd(second)}); },
    []<auto t_func>(std::convertible_to<DataT> auto && first, auto && second)
    { return detail::execute_for_arrs<t_func>(array{pspp_fwd(first)}, second); }
		>
{};

template <typename DataT, std::size_t t_size>
struct forbid_automatic_operators_overload_for<array<DataT, t_size>> :
  enable_ops<pipe_operation_t>
{};

template <bool t_use_loop = false, typename DataT, std::size_t t_n>
constexpr bool is_arrays_equal(array<DataT, t_n> const & left, array<DataT, t_n> const & right) noexcept (noexcept (
  std::declval<DataT>() != std::declval<DataT>()
  ))
{
  if constexpr (t_use_loop)
  {
    for (std::size_t const i : std::views::iota(0ul, t_n))
      if (left[i] != right[i])
        return false;
    return true;
  } else
  {
    return [&]<std::size_t ... t_is>(std::index_sequence<t_is...>) -> bool
    {
      return (0 + ... + (left[t_is] != right[t_is])) == 0;
    }(std::make_index_sequence<t_n>{});
  }
}

} // namespace pspp
#endif // PSPP_TYPED_INDEX_ARRAY
