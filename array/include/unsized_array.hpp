#ifndef PSPP_STATIC_ARRAY_HPP
#define PSPP_STATIC_ARRAY_HPP

#include <pspp/data_holder.hpp>

namespace pspp
{

template <auto t_first, 
					decltype(t_first) ... t_rest>
struct template_array
{
	using data_type = decltype(t_first);
	using holders_seq = type_sequence<value_holder<t_first>, value_holder<t_rest>...>;

	inline static constexpr std::array<data_type, sizeof...(t_rest) + 1> data{t_first, t_rest...};

	template <data_type t_first1, data_type ... t_rest1>
	consteval template_array<t_first, t_rest..., t_first1, t_rest1...>
		operator+(template_array<t_first1, t_rest1...>) const noexcept
	{ return {}; }

  template <template <auto ...> class ValsHolderT>
  template_array(ValsHolderT<t_first, t_rest...>) {}
};

template <auto t_first, auto ... t_rest, template <auto...> class ValsHolderT>
template_array(ValsHolderT<t_first, t_rest...>) -> template_array<t_first, t_rest...>;

template <typename T>
struct is_template_array : std::false_type
{};

template <auto t_first,
					decltype(t_first) ... t_rest>
struct is_template_array<template_array<t_first, t_rest...>> : std::true_type {};

template <typename T>
concept c_template_array = is_template_array<std::remove_cvref_t<T>>::value;



template <typename DataT>
struct constexpr_array : data_holder<DataT const>
{
	constexpr constexpr_array(c_template_array auto data_):
		data_holder<DataT const>(data_.data)
	{}

  template <std::size_t t_n>
  constexpr constexpr_array(std::array<DataT, t_n> const & data_):
    data_holder<DataT const>(data_)
  {}

	constexpr constexpr_array():
			data_holder<DataT const>()
	{}
};

template <auto t_first, auto ... t_rest>
struct template_array_from_creators
{
	using data_type = decltype(t_first());
	using holders_seq = type_sequence<value_holder<t_first>, value_holder<t_rest>...>;

	inline static constexpr std::array<data_type, sizeof...(t_rest) + 1> data{t_first(), t_rest()...};
};

template <auto t_first, auto ... t_rest>
constexpr_array(template_array_from_creators<t_first, t_rest...>) ->
  constexpr_array<decltype(t_first())>;

template <c_template_array ArrT>
constexpr_array(ArrT)->
  constexpr_array<typename ArrT::data_type>;

template <auto t_first, auto ... t_rest>
struct is_template_array<template_array_from_creators<t_first, t_rest...>> :
		std::true_type
{};

template <auto t_first,
					decltype(t_first) ... t_rest>
inline constexpr auto make_template_array = template_array<t_first, t_rest...>{};

template <auto t_first,
					decltype(t_first) ... t_rest>
inline constexpr auto to_constexpr_array = constexpr_array(make_template_array<t_first, t_rest...>);

template <typename DataT,
					DataT t_first, DataT ... t_rest>
inline constexpr auto to_constexpr_array_of = constexpr_array(make_template_array<t_first, t_rest...>);

} // namespace pspp

#endif // PSPP_STATIC_ARRAY_HPP
