#ifndef PSPP_TEMPORARY_STORAGE_HPP
#define PSPP_TEMPORARY_STORAGE_HPP

#include <pspp/type_sequence.hpp>
#include <pspp/type_name.hpp>

namespace pspp
{

template <typename T, typename = decltype([]{})>
concept c_complete_class =
  std::is_class_v<T> and requires { T{}; };

template <typename T>
using storage_type =
  std::conditional_t
  <
    std::is_rvalue_reference_v<T>,
    std::remove_reference_t<T>,
    T
  >;

namespace
{
template <typename T, std::size_t t_i>
struct tmpvalue_container
{
  storage_type<T> m_value;

  constexpr tmpvalue_container(tmpvalue_container && other) :
    m_value{std::forward<T>(other.m_value)}
  {}

  constexpr tmpvalue_container(tmpvalue_container const & other) :
    m_value{other.m_value}
  {}

  constexpr tmpvalue_container & operator=(tmpvalue_container const &) noexcept = default;
  constexpr tmpvalue_container & operator=(tmpvalue_container &&) noexcept = default;

  constexpr tmpvalue_container(T && value):
    m_value{value}
  {}

  [[nodiscard]] pspp_always_inline
  constexpr auto && get(value_holder<t_i>) noexcept
  {
    return std::forward<T>(m_value); }

  [[nodiscard]] pspp_always_inline
  constexpr auto const & get(value_holder<t_i>) const noexcept
  { return m_value; }

  [[nodiscard]] pspp_always_inline
  constexpr auto & get_ref(value_holder<t_i>) noexcept
  { return m_value; }

  [[nodiscard]] pspp_always_inline
  constexpr auto const & get_ref(value_holder<t_i>) const noexcept
  { return m_value; }
};

template <typename ... ValConts>
struct tmpstorage_base :
  ValConts...
{
  using ValConts::get...;
  using ValConts::get_ref...;
};

template <typename ... Ts, std::size_t ... t_is>
constexpr tmpstorage_base<tmpvalue_container<Ts, t_is>...>
  get_tmpstorage_base(std::index_sequence<t_is...> = std::make_index_sequence<sizeof...(Ts)>{});

template <typename ... Ts>
using tmpstorage_base_t = decltype(get_tmpstorage_base<Ts...>(std::make_index_sequence<sizeof...(Ts)>{}));

}

template <typename ... Ts>
struct tmpstorage : tmpstorage_base_t<Ts...>
{
  using types = type_sequence<Ts...>;
  using base = tmpstorage_base_t<Ts...>;

  static_val size = sizeof...(Ts);

  static constexpr auto get_is() noexcept
  { return std::make_index_sequence<sizeof...(Ts)>{}; }

  template <std::size_t t_i>
  [[nodiscard]] pspp_always_inline
  constexpr auto && get() noexcept
  { return base::get(val<t_i>); }

  template <std::size_t t_i>
  [[nodiscard]] pspp_always_inline
  constexpr auto const & get() const noexcept
  { return base::get(val<t_i>); }

  template <std::size_t t_i>
  [[nodiscard]] pspp_always_inline
  constexpr auto & get_ref() noexcept
  { return base::get_ref(val<t_i>); }

  template <std::size_t t_i>
  [[nodiscard]] pspp_always_inline
  constexpr auto const & get_ref() const noexcept
  { return base::get_ref(val<t_i>); }

  constexpr tmpstorage() = default;

  template <std::size_t ... t_is>
  constexpr tmpstorage(tmpstorage_base<tmpvalue_container<Ts, t_is>...> && other) :
    base{{other.get(val<t_is>)}...}
  {}

  template <std::size_t ... t_is>
  constexpr tmpstorage(tmpstorage_base<tmpvalue_container<Ts, t_is>...> const & other) :
    base{{other.get(val<t_is>)}...}
  {}

  constexpr tmpstorage(tmpstorage const &) = default;
  constexpr tmpstorage(tmpstorage &&) = default;

  template <typename U, typename ... Us>
  constexpr tmpstorage(U && first, Us && ... args)
    requires (sizeof...(Ts) > 0 and types {} | head | is_t_same_as<U>) :
    base{{std::forward<U>(first)}, {std::forward<Us>(args)}...}
  {}

  constexpr tmpstorage & operator=(tmpstorage const &) noexcept = default;
  constexpr tmpstorage & operator=(tmpstorage &&) noexcept = default;
};

template <typename T, typename ... Ts>
tmpstorage(T &&, Ts && ...) -> tmpstorage<T &&, Ts &&...>;

template <typename ... Ts>
tmpstorage(tmpstorage<Ts...> const &) -> tmpstorage<Ts...>;

template <typename ... Ts>
tmpstorage(tmpstorage<Ts...> &&) -> tmpstorage<Ts...>;

template <typename T>
concept c_tmpstorage = c_complex_target<T, tmpstorage>;

template <c_tmpstorage LeftT, c_tmpstorage RightT>
constexpr auto operator+(LeftT && left, RightT && right)
{
  return []<std::size_t ... t_is, std::size_t ... t_js>(
    std::index_sequence<t_is...>,
    std::index_sequence<t_js...>,
    LeftT && left, RightT && right)
  {
    return tmpstorage{
      std::forward<LeftT>(left).template get<t_is>()...,
      std::forward<RightT>(right).template get<t_js>()...
    };
  }(std::remove_cvref_t<LeftT>::get_is(),
    std::remove_cvref_t<RightT>::get_is(),
    std::forward<LeftT>(left), std::forward<RightT>(right));
}

} // pspp


namespace std
{
template <std::size_t t_i>
constexpr auto && get(pspp::c_tmpstorage auto && storage) noexcept
{
  return pspp_fwd(storage).template get<t_i>();
}

} 

#endif // PSPP_TEMPORARY_STORAGE_HPP
