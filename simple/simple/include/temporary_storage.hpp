#ifndef PSPP_SIMPLE_TEMPORARY_STORAGE_HPP
#define PSPP_SIMPLE_TEMPORARY_STORAGE_HPP

#include <pspp/type_helpers.hpp>

namespace pspp::simple
{

template <typename T, typename = decltype([]{})>
concept c_complete_class =
  std::is_class_v<T> and requires { T{}; };

template <typename T>
using storage_type =
  std::conditional_t
  <
    std::is_rvalue_reference_v<T>,
    std::remove_reference_t<T>,
    T
  >;

namespace
{
template <typename T, std::size_t t_i>
struct tmpvalue_container
{
  storage_type<T> m_value;

  constexpr tmpvalue_container(tmpvalue_container && other) :
    m_value{std::forward<T>(other.m_value)}
  {}

  constexpr tmpvalue_container(tmpvalue_container const & other) :
    m_value{other.m_value}
  {}


  constexpr tmpvalue_container(T && value):
    m_value{value}
  {}

  [[nodiscard]] pspp_always_inline
  constexpr auto && get(value_holder<t_i>) noexcept
  {
    return std::forward<T>(m_value); }

  [[nodiscard]] pspp_always_inline
  constexpr auto const & get(value_holder<t_i>) const noexcept
  { return m_value; }

  [[nodiscard]] pspp_always_inline
  constexpr auto & get_ref(value_holder<t_i>) noexcept
  { return m_value; }

  [[nodiscard]] pspp_always_inline
  constexpr auto const & get_ref(value_holder<t_i>) const noexcept
  { return m_value; }
};

template <typename ... ValConts>
struct tmpstorage_base :
  ValConts...
{
  using ValConts::get...;
  using ValConts::get_ref...;
};

template <typename ... Ts, std::size_t ... t_is>
constexpr tmpstorage_base<tmpvalue_container<Ts, t_is>...>
  get_tmpstorage_base(std::index_sequence<t_is...> = std::make_index_sequence<sizeof...(Ts)>{});

template <typename ... Ts>
using tmpstorage_base_t = decltype(get_tmpstorage_base<Ts...>(std::make_index_sequence<sizeof...(Ts)>{}));

}

template <typename ... Ts>
struct tmpstorage : tmpstorage_base_t<Ts...>
{
  using base = tmpstorage_base_t<Ts...>;

  static_val size = sizeof...(Ts);

  static constexpr auto get_is() noexcept
  { return std::make_index_sequence<size>{}; }

  template <std::size_t t_i>
  [[nodiscard]] pspp_always_inline
  constexpr auto && get() noexcept
  { return base::get(val<t_i>); }

  template <std::size_t t_i>
  [[nodiscard]] pspp_always_inline
  constexpr auto const & get() const noexcept
  { return base::get(val<t_i>); }

  template <std::size_t t_i>
  [[nodiscard]] pspp_always_inline
  constexpr auto & get_ref() noexcept
  { return base::get_ref(val<t_i>); }

  template <std::size_t t_i>
  [[nodiscard]] pspp_always_inline
  constexpr auto const & get_ref() const noexcept
  { return base::get_ref(val<t_i>); }

  constexpr tmpstorage() = default;

  template <std::size_t ... t_is>
  constexpr tmpstorage(tmpstorage_base<tmpvalue_container<Ts, t_is>...> && other) :
    base{{other.get(val<t_is>)}...}
  {}

  template <std::size_t ... t_is>
  constexpr tmpstorage(tmpstorage_base<tmpvalue_container<Ts, t_is>...> const & other) :
    base{{other.get(val<t_is>)}...}
  {}

  template <typename U, typename ... Us>
  constexpr tmpstorage(U && first, Us && ... args)
    requires (sizeof...(Ts) > 0 and
      c_same_as<decltype(get_first_type<Ts...>()), U>) :
    base{{std::forward<U>(first)}, {std::forward<Us>(args)}...}
  {}
};

template <typename T, typename ... Ts>
tmpstorage(T &&, Ts && ...) -> tmpstorage<T &&, Ts &&...>;

template <typename ... Ts>
tmpstorage(tmpstorage<Ts...> const &) -> tmpstorage<Ts...>;

template <typename ... Ts>
tmpstorage(tmpstorage<Ts...> &&) -> tmpstorage<Ts...>;

template <typename T>
concept c_tmpstorage = c_complex_target<T, tmpstorage>;


template <c_tmpstorage LeftT, c_tmpstorage RightT>
constexpr auto operator+(LeftT && left, RightT && right)
{
  return []<std::size_t ... t_is, std::size_t ... t_js>(
    std::index_sequence<t_is...>,
    std::index_sequence<t_js...>,
    LeftT && left, RightT && right)
  {
    return tmpstorage{
      std::forward<LeftT>(left).template get<t_is>()...,
      std::forward<RightT>(right).template get<t_is>()...
    };
  }(std::remove_cvref_t<LeftT>::get_is(),
    std::remove_cvref_t<RightT>::get_is(),
    std::forward<LeftT>(left), std::forward<RightT>(right));
}

} // pspp



namespace std
{
template <std::size_t t_i, pspp::simple::c_tmpstorage TmpT>
constexpr auto && get(TmpT && storage) noexcept
{
  return std::forward<TmpT>(storage).template get<t_i>();
}

template <typename ... Ts>
struct tuple_size<pspp::simple::tmpstorage<Ts...>>
{ static_size value = sizeof...(Ts); };

/*
template <typename FuncT, pspp::simple::c_tmpstorage TmpStorT>
constexpr auto apply(TmpStorT && storage, FuncT && func) noexcept -> decltype(auto)
{
  return []<std::size_t ... t_is>(std::index_sequence<t_is...>, TmpStorT && storage, FuncT && func) {
    return std::forward<FuncT>(func)(std::forward<storage>(storage).template get<t_is>()...);
  }(
    std::make_index_sequence<std::remove_cvref_t<TmpStorT>::size>{},
    std::forward<TmpStorT>(storage), std::forward<FuncT>(func)
  );
  }
  */

} 

#endif // PSPP_SIMPLE_TEMPORARY_STORAGE_HPP
