#ifndef PSPP_SIMPLE_META_HPP
#define PSPP_SIMPLE_META_HPP

#include <pspp/utility/declaration_macros.hpp>

namespace pspp::simple
{
template <typename T>
struct remove_const { using type = T; };

template <typename T>
struct remove_const<T const> { using type = T; };


template <typename T>
struct remove_reference { using type = T; };

template <typename T>
struct remove_reference<T &> { using type = T; };

template <typename T>
struct remove_reference<T &&> { using type = T; };

template <typename T>
struct remove_volatile { using type = T; };

template <typename T>
struct remove_volatile<T volatile> { using type = T; };

template <typename T>
using remove_const_t = typename remove_const<T>::type;

template <typename T>
using remove_reference_t = typename remove_reference<T>::type;

template <typename T>
using remove_volatile_t = typename remove_volatile<T>::type;

template <typename T>
using remove_cvref_t = remove_volatile_t< remove_const_t< remove_reference_t<T> > >;

template <typename T>
struct type_identity
{ using type = T; };

template <typename T>
using type_identity_t = typename type_identity<T>::type;

template <typename T, typename U>
struct is_same
{
  static_flag value = false;
};

template <typename T>
struct is_same<T, T>
{
  static_flag value = true;
};

template <typename T>
struct is_lvalue_reference
{ static_flag value = false; };

template <typename T>
struct is_lvalue_reference<T &>
{ static_flag value = true; };

template <typename T>
concept c_lvalue_reference = is_lvalue_reference<T>::value;

template <typename T, typename U>
inline constexpr bool is_same_v = false;

template <typename T>
inline constexpr bool is_same_v<T, T> = true;

template <typename T, typename U>
inline constexpr bool is_diff_v = not is_same_v<T, U>;

template <typename T>
struct is_const_lvref { static_flag value = false; };

template <typename T>
struct is_const_lvref<T const &> { static_flag value = true; };

template <typename T>
struct is_mut_lvref { static_flag value = false; };

template <typename T>
struct is_mut_lvref<T const &> { static_flag value = false; };

template <typename T>
struct is_mut_lvref<T &> { static_flag value = true; };

template <typename T>
concept c_const_lvref = is_const_lvref<T>::value;
template <typename T>
concept c_mut_lvref   = is_mut_lvref<T>::value;

template <typename T, typename U>
concept same_as = is_same_v<T, U>;

template <typename T>
[[nodiscard]] pspp_always_inline
constexpr T && forward(remove_reference_t<T> & val) noexcept
{ return static_cast<T&&>(val); }

template <typename T>
[[nodiscard]] pspp_always_inline
constexpr T && forward(remove_reference_t<T> && val) noexcept
{
  static_assert(not c_lvalue_reference<T>);
  return static_cast<T&&>(val);
}

template <typename T>
[[nodiscard]] pspp_always_inline
constexpr remove_reference_t<T> && move(T && val) noexcept
{ return static_cast<remove_reference_t<T>&&>(val); }

struct true_type
{
  using type = bool;
  inline static constexpr type value = true;
};

struct false_type
{
  using type = bool;
  inline static constexpr type value = false;
};


namespace detail
{

template <typename BaseT>
constexpr true_type  is_base_of_impl(BaseT const volatile *) noexcept;
template <typename>
constexpr false_type is_base_of_impl(void  const volatile *) noexcept;

}

template <typename MaybeDerivedT, typename BaseT>
concept derived_from = requires (MaybeDerivedT mder) {
  requires decltype(detail::is_base_of_impl(static_cast<BaseT const *>(&mder)))::value;
};

} // namespace pspp

#endif // PSPP_SIMPLE_META_HPP
