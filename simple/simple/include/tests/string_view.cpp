#include <pspp/simple/string_view.hpp>
#include <string_view>
#include <string>

using namespace pspp;

constexpr char const * emptycstr = "";
constexpr char const * tmpcstr = "kdkajsfd";

// basic
static_assert(
  not simple_string_view{tmpcstr}.empty()            and
  simple_string_view{emptycstr}  .empty()            and
  simple_string_view{tmpcstr}. data()  == tmpcstr    and
  simple_string_view{emptycstr}.data() == emptycstr  and
  simple_string_view{"asd"}  .size()   == 3          and
  simple_string_view{tmpcstr}.size()   == std::string_view{tmpcstr}.size() and
  requires (
    simple_string_view str,
    simple_string_view && rstr, simple_string_view const & cstr,
    std::string std_str, std::string_view std_strv
    )
  {
    simple_string_view{};
    simple_string_view{std::move(rstr)};
    simple_string_view{cstr};

    simple_string_view{std_str};
    simple_string_view{std_strv};

    str = cstr;
    str = std::move(rstr);
  },
  "basic tests failed"
);

static_assert(
  simple_string_view{"asd"}  == simple_string_view{"asd"}  and
  simple_string_view{"asdc"} != simple_string_view{"asd"},
  "compare tests failed"
);

static_assert(
  simple_string_view{"asd"}.size() == 3,
  "static size tests failed"
);

static_assert(
  simple_string_view{" asd"}.substr(1)     == "asd" and
  simple_string_view{" asd"}.substr(1, 1)  == "a"   and
  simple_string_view{" asd"}.substr(1, 10) == "asd" and
  simple_string_view{" asd"}.substr(10, 1) == ""    and
  simple_string_view{" asd"}.substr(1, prechecked)        == "asd" and
  simple_string_view{" asd"}.substr(1, 2, prechecked)     == "as",

  "substr tests failed"
);

static_assert(
  []{
    constexpr auto npos = std::string::npos;

    constexpr simple_string_view teststr{"str"};
    constexpr simple_string_view samestr{"str"};
    constexpr simple_string_view diffstr{"stb"};

    constexpr auto cont_false_res = teststr.contains('a');
    constexpr auto cont_true_res  = teststr.contains('t');

    constexpr auto comp_false_res  = teststr == diffstr;
    constexpr auto comp_true_res   = teststr == samestr;
    constexpr auto ncomp_false_res = teststr != samestr;
    constexpr auto ncomp_true_res  = teststr != diffstr;

    return not cont_false_res                      and
           cont_true_res                           and
           cont_true_res.pos == 1                  and
           cont_false_res.pos == std::string::npos and
           not comp_false_res                      and
           not ncomp_false_res                     and
           ncomp_true_res                          and
           comp_true_res                           and
           comp_false_res.pos  == 2                and
           comp_true_res.pos   == npos             and
           ncomp_true_res.pos  == 2                and
           ncomp_false_res.pos == npos;
  }(),

  "advanced compare tests failed"
);

int main()
{}
