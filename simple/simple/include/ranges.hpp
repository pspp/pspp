
#ifndef PSPP_SIMPLE_RANGES_HPP
#define PSPP_SIMPLE_RANGES_HPP

#include <pspp/simple/meta.hpp>
#include <pspp/ops.hpp>

namespace pspp::ranges
{

namespace
{
inline constexpr struct identity_t
{
  template <typename ValueT>
  constexpr auto pspp_static_call_const_operator(ValueT && val) noexcept -> ValueT &&
  { return simple::forward<ValueT>(val); }

} identity {};

}

/**
 * @brief implements bubble sort for compile-time sort only
 */
inline constexpr struct sort_t
{
  template <typename RangeT,
    typename ComparatorT = greater_t, typename ProjT = identity_t>
  constexpr void pspp_static_call_const_operator(
      RangeT & range,
      ComparatorT comparator = greater,
      ProjT projection = identity
    ) noexcept
  {
    if consteval
    {
      using underlying_type = simple::remove_reference_t<decltype(*range.begin())>;
      using size_type = unsigned long;

      size_type const s = range.size();
      size_type count = 0;

      for (size_type j_ = 0; j_ < s; ++j_)
      {
        size_type ind = 0;
        size_type const last = s - count;

        for (size_type i_ = 1; i_ < last; ++i_)
        {
          if (
            comparator(projection(range[i_]), projection(range[ind]))
            )
            ind = i_;
        }

        if (ind != last - 1)
        {
          underlying_type tmp = simple::move(range[last - 1]);

          range[last - 1] = range[ind];
          range[ind] = simple::move(tmp);
        }

        ++count;
      }
    }
  }
} sort {};

inline constexpr struct sort_copy_t
{
  template <typename RangeT, typename ComparatorT = greater_t, typename ProjT = identity_t>
  constexpr auto pspp_static_call_const_operator(
      RangeT && range,
      ComparatorT comparator = greater,
      ProjT projector = identity
    ) -> simple::remove_cvref_t<RangeT>
  {
    simple::remove_cvref_t<RangeT> tmp = auto{simple::forward<RangeT>(range)};

    sort(tmp, comparator, projector);
    return tmp;
  }
} sort_copy {};


inline constexpr struct transform_t
{
  template <typename RangeT, typename IterT, typename Convertor>
  constexpr void pspp_static_call_const_operator(
      RangeT && range,
      IterT iter,
      Convertor && conv
    ) noexcept
  {
    for (auto & val : range)
      *iter++ = conv(val);
  }
} transform {};


inline constexpr struct transform_to_self_t
{
  template <typename RangeT, typename Convertor
  >
  constexpr void pspp_static_call_const_operator(
    RangeT && range,
    Convertor && conv
    )
  { return transform(range, range.begin(), simple::forward<Convertor>(conv)); }
} transform_to_self {};

inline constexpr struct contains_t
{
  template <typename RangeT, typename ValueT, typename ProjT = identity_t>
  constexpr bool pspp_static_call_const_operator(RangeT && range, ValueT && value, ProjT projector = identity) noexcept
  {
    for (auto const & val: range)
      if (projector(val) == value)
        return true;

    return false;
  }
} contains {};

inline constexpr struct contains_subrange_t
{
  template <typename RangeT, typename SubRangeT,
    typename Comparator = equal_to_t, typename LeftProj = identity_t,
    typename RightProj = identity_t
    >
  constexpr bool pspp_static_call_const_operator(
      RangeT && range, SubRangeT && sub_range,
      Comparator comparator = equal_to,
      LeftProj lproj = identity,
      RightProj rproj = identity
    ) noexcept
  {
    using size_type = unsigned long;

    size_type const left_size  = range.size();
    size_type const right_size = sub_range.size();
    size_type const movable_length = left_size - right_size + 1;

    if (right_size > left_size) return false;

    auto const & first_of_subrange = rproj(*sub_range.begin());

    size_type i_ = 0;
    for (; i_ < movable_length; ++i_)
    {
      if (comparator(lproj(range[i_]), first_of_subrange))
      {
        bool tmp_ = true;

        for (size_type j_ = 1; j_ < right_size; ++j_)
          if (! comparator(lproj(range[i_ + j_]), rproj(sub_range[j_]))) { tmp_ = false; break; };

        if (tmp_) return tmp_;
      }
    }

    return false;
  }
} contains_subrange {};

}

#endif // PSPP_SIMPLE_RANGES_HPP
