#ifndef PSPP_SIMPLE_STRING_VIEW_HPP
#define PSPP_SIMPLE_STRING_VIEW_HPP

#include <pspp/utility/declaration_macros.hpp>

namespace pspp
{

namespace
{
constexpr unsigned long unchecked_strlen_(char const * str) noexcept
{
  unsigned long res = 0;
  while (*(str + res++) != '\0');

  return res - 1;
}

constexpr unsigned long strlen_(char const * str) noexcept
{
  unsigned long res = 0;
  while ((str + res != nullptr) and *(str + res++) != '\0');

  return res - 1;
}
}

inline constexpr struct prechecked_t {} prechecked {};

struct strv_check_result
{
	using size_type = unsigned long;
	static_size npos_ = static_cast<size_type>(-1);

	size_type pos;

	constexpr operator bool() const noexcept
	{ return pos != npos_; }
};

struct rstrv_check_result
{
	using size_type = unsigned long;
	static_size npos_ = static_cast<size_type>(-1);

	size_type pos;

	constexpr operator bool() const noexcept
	{ return pos == npos_; }
};

constexpr strv_check_result operator!(rstrv_check_result r) noexcept
{ return {r.pos}; }

constexpr rstrv_check_result operator!(strv_check_result r) noexcept
{ return {r.pos}; }

struct simple_string_view
{
	using size_type = unsigned long; 
	using char_type = char;
	using reference_type = char const &;
	using const_iterator_type = char_type const *;
	using str_type = char_type const *;

	static_size npos_ = static_cast<size_type>(-1);

	str_type strptr;
	size_type length;

#ifdef PSPP_ENABLE_STRING_VIEW_LAST_SEARCH_CACHE
	size_type last_search_cache = npos_;
#endif

  constexpr simple_string_view() noexcept :
    strptr {nullptr},
    length {0}
  {}

  constexpr simple_string_view & operator=(simple_string_view const & other) = default;
  constexpr simple_string_view & operator=(simple_string_view && other) = default;

  constexpr simple_string_view(simple_string_view const &) noexcept = default;
  constexpr simple_string_view(simple_string_view &&) noexcept = default;

  constexpr simple_string_view(str_type str_, size_type size_) noexcept :
    strptr{str_},
    length{size_}
  {}

  constexpr simple_string_view(str_type str_, int size_) noexcept :
    strptr{str_},
    length{static_cast<size_type>(size_)}
  {}

  template <typename StringT>
    requires requires (StringT str){ str.size(); str.c_str(); }
  constexpr simple_string_view(StringT const & str) :
    strptr{str.c_str()},
    length{str.size()}
  {}

  template <typename StringT>
  requires requires (StringT str){ str.size(); str.data(); }
  constexpr simple_string_view(StringT && str) :
    strptr{str.data()},
    length{str.size()}
  {}

  template <std::size_t t_n>
  constexpr simple_string_view(char_type const (&str)[t_n]) noexcept :
    strptr{str + 0},
    length{t_n}
  {}

  constexpr simple_string_view(str_type str) noexcept :
    strptr{str},
    length{strlen_(str)}
  {}

  constexpr simple_string_view(str_type begin, str_type end) noexcept :
    strptr{begin},
    length{static_cast<size_type>(end - begin)}
  {}

  constexpr simple_string_view(str_type str, prechecked_t) :
    strptr{str},
    length(unchecked_strlen_(str))
  {}

	[[nodiscard]] pspp_always_inline
	constexpr size_type size() const noexcept
	{ return length; }

  [[nodiscard]] pspp_always_inline
  constexpr bool empty() const noexcept
  { return size() == 0; }

  [[nodiscard]] pspp_always_inline
  constexpr str_type data() const noexcept
  { return strptr; }

	[[nodiscard]] pspp_always_inline
	constexpr const_iterator_type begin() const noexcept
	{ return strptr; }

	[[nodiscard]] pspp_always_inline
	constexpr const_iterator_type end() const noexcept
	{ return strptr + length; }

	[[nodiscard]] pspp_always_inline
	constexpr reference_type front() const noexcept
	{ return *begin(); }

	[[nodiscard]] pspp_always_inline
	constexpr reference_type back() const noexcept
	{ return *(end() - 1); }

	[[nodiscard]] pspp_always_inline
	constexpr char_type at(size_type pos) const noexcept
	{ 
		if (pos > size()) return '\0';
		else return *(strptr + pos);
	}

	[[nodiscard]] pspp_always_inline
	constexpr char_type at(size_type pos, prechecked_t) const noexcept
	{ return *(strptr + pos); }

	[[nodiscard]] pspp_always_inline
	constexpr char_type operator[](size_type pos) const noexcept
	{ return at(pos); }

  [[nodiscard]] pspp_always_inline
  constexpr char_type operator[](size_type pos, prechecked_t) const noexcept
  { return at(pos, prechecked); }


	[[nodiscard]] pspp_always_inline
	constexpr simple_string_view substr(size_type start, size_type count, prechecked_t) const noexcept
	{
		return {strptr + start, count};
	}

	[[nodiscard]] pspp_always_inline
	constexpr simple_string_view substr(size_type start, prechecked_t) const noexcept
	{ return { strptr + start, length - start }; }

	[[nodiscard]] pspp_always_inline
	constexpr simple_string_view substr(
			size_type start, size_type count
		) const noexcept
	{
		if (start > length) return {end() - 1, 0};
		else 
		{
			if (static_cast<long>(count) > static_cast<long>(length - start))
        return {strptr + start, length - start};
			else return {strptr + start, count};
		}
	}

  [[nodiscard]] pspp_always_inline
  constexpr simple_string_view substr(
    size_type start
  ) const noexcept
  {
    if (start > length) return {end() - 1, 0};
    else
    {
      return {strptr + start, length - start};
    }
  }

	[[nodiscard]] inline
	constexpr rstrv_check_result operator==(simple_string_view other) const noexcept
	{
		if (other.size() != size()) return {0};

		for (size_type i = 0; i < size(); ++i)
			if (at(i, prechecked) != other.at(i, prechecked)) return {i};

		return {npos_};
	}

	[[nodiscard]] pspp_always_inline
	constexpr strv_check_result operator!=(simple_string_view other) const noexcept
	{
		return !( operator==(other) );
	}

	[[nodiscard]] 
	constexpr inline strv_check_result contains(char_type c) const noexcept
	{ 
		for (size_type i = 0; i < size(); ++i)
			if (at(i) == c) return {i};

		return {npos_};
	}

#ifdef PSPP_ENABLE_STRING_VIEW_LAST_SEARCH_CACHE
	[[nodiscard]] 
	constexpr inline bool contains(char_type c) noexcept
	{
		for (size_type i = 0; i < size(); ++i)
			if (at(i) == c) { last_search_cache = i; return true; }

		last_search_cache = npos_;
		return false;
	}

	[[nodiscard]] 
	constexpr inline bool operator==(simple_string_view other) noexcept
	{
		if (other.size() != size()) { last_search_cache = npos_; return false; }

		for (size_type i = 0; i < size(); ++i)
				if (at_unchecked(i) == other.at_unchecked(i)) { last_search_cache = i; return false; }

		last_search_cache = npos_;
		return true;
	}

	[[nodiscard]] pspp_always_inline
	constexpr bool operator!=(simple_string_view other) noexcept
	{
		return !( operator==(other) );
	}

	[[nodiscard]] pspp_always_inline
	constexpr inline size_type last_search_res() const noexcept
	{ return last_search_cache; }

#endif
};

} // namespace

#endif // PSPP_SIMPLE_STRING_VIEW_HPP
