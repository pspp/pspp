#ifndef PSPP_ITERATOR_OPTIONS_HPP
#define PSPP_ITERATOR_OPTIONS_HPP

#include <pspp/constr.hpp>
#include <pspp/ranges_options.hpp>

namespace pspp::iterator_options 
{

struct reverse {};
struct view {};


struct store_as_value {};


struct operators_returns_by_value {};
struct operators_returns_by_reference {};
struct operators_returns_nothing {};


struct enable_index_convertion {};
struct disable_index_convertion {};


struct print_data_only {};
struct print_address_and_data {};
struct print_address_only {};
struct forbide_print_unprintable {};

template <pspp::constr t_message = "unprintable_object">
struct print_message_for_unprintable :
	cstr_holder<t_message>
{};


struct allow_to_type_conversion {};
struct forbide_to_type_conversion {};
	
template <typename ... Ts>
struct enable_conversion_from_other 
{
	using enabled_types = pspp::type_sequence<Ts...>;
};

template <typename ... Ts>
struct enable_conversion_to_other
{
	using enabled_types = pspp::type_sequence<Ts...>;
};

template <typename ... Ts>
struct disable_conversion_from_other
{
	using disabled_types = pspp::type_sequence<Ts...>;
};

template <typename ... Ts>
struct disable_conversion_to_other
{
	using disabled_types = pspp::type_sequence<Ts...>;
};

template <typename ... Options>
struct options_set
{
	static_val options_seq = pspp::typeseq<Options...>;

	static_assert(
			not ((options_seq | contains<enable_conversion_from_other<>>) and
				(options_seq | contains<disable_conversion_from_other<>>)),
			"iterator: options_set: can not enable and disable conversion from all types"
	);
	static_assert(
			not ((options_seq | contains<enable_conversion_to_other<>>) and
				(options_seq | contains<disable_conversion_to_other<>>)),
			"iterator: options_set: can not enable and disable conversion to all types"
	);

	static_flag enabled_index_convertion = options_seq | contains<enable_index_convertion>;
	static_flag to_type_conversion_allowed = not (options_seq | contains<forbide_to_type_conversion>);

	static_flag operators_has_return_type = not (options_seq | contains<operators_returns_nothing>);
	static_flag do_operators_returns_by_reference = type<operators_returns_by_reference> ==
							(options_seq | get_last_like_or<
															some_of<operators_returns_by_value, operators_returns_by_reference>,
															operators_returns_by_reference
														>);
	/*static_flag do_operators_returns_by_reference = std::same_as<operators_returns_by_reference, 
							typename options_seq::template last_succeed_or<[]<typename T>{
								return std::same_as<T, operators_returns_by_value> or
									std::same_as<T, operators_returns_by_reference>;
							}, operators_returns_by_reference>>;
							*/

	struct print
	{
		static_flag data_only = options_seq | contains<print_data_only>;
		static_flag addres_only = options_seq | contains<print_address_only>;

		static_flag both = options_seq | contains<print_address_and_data> or (
				data_only and addres_only) or (not data_only and not addres_only);

		static_flag can_print_message_for_unprintable = not (options_seq | contains<forbide_print_unprintable>);

		static_val message_for_unprintable_v = options_seq | get_last_like_or<
			same_as_complex_value<print_message_for_unprintable>,
			print_message_for_unprintable<>
		> | unwrap;
		using message_for_unprintable = std::remove_cv_t<decltype(message_for_unprintable_v)>;
	};

	static_flag is_reversed = ((options_seq | count_of<reverse, ranges_options::reverse>) % 2) == 1;
	static_flag is_view = options_seq | contains_some_of<view, ranges_options::view>;


	template <template <typename ...> class enable_t,
						template <typename ...> class disable_t>
	struct _types_flags
	{
		using enabled_types = extract_type<options_seq | get_last_like_or<
			pspp::same_as_complex<enable_t>, nullt>>;
		using disabled_types = extract_type<options_seq | get_last_like_or<
			pspp::same_as_complex<disable_t>, nullt>>;

		static_flag has_enabled = not std::same_as<enabled_types, nullt>;

		static_flag enabled_all = 
				(std::same_as<enabled_types, nullt> or 
					std::same_as<enabled_types, enable_t<>>
				);

		static_flag has_disabled = not (std::same_as<disabled_types, nullt>);

		static_flag disabled_all = 
				((std::same_as<disabled_types, nullt> and not enabled_all)or
					std::same_as<disabled_types, disable_t<>>
				);

		static_flag completely_enabled_all = enabled_all and not has_disabled;
		static_flag completely_disabled_all = disabled_all and not has_enabled;

		template <typename T>
		static instantfunc is_enabled_impl() noexcept
		{
			if constexpr (completely_enabled_all)
				return true;
			else if constexpr (completely_disabled_all)
				return false;
			else 
			{
				constexpr auto contained_by = []<bool enabled>{
					return enabled ? 
						(has_enabled and (enabled_types::enabled_types::template contains<T> or 
															enabled_all)) :
						(has_disabled and (disabled_types::disabled_types::template contains<T> or
															disabled_all));
				};
				constexpr bool enabled = static_call<contained_by, true>();
				constexpr bool disabled = static_call<contained_by, false>();
				
				if constexpr (enabled_all)
					return not disabled;
				else if constexpr (disabled_all)
					return enabled;
				else 
					return enabled and not disabled;
			}
		}

		template <typename T>
		static_flag is_enabled = is_enabled_impl<T>();
	};

	struct from_conversions : 
		_types_flags<
			enable_conversion_from_other,
			disable_conversion_from_other
		>
	{};

	struct to_conversions : 
		_types_flags<
			enable_conversion_to_other, 
			disable_conversion_to_other
		>
	{};

	static_flag do_store_as_value = options_seq | contains<store_as_value>;
};

}

#endif // PSPP_ITERATOR_OPTIONS_HPP
