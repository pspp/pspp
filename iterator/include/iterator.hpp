#ifndef PSPP_ITERATOR_HPP
#define PSPP_ITERATOR_HPP

#include <pspp/with_reason.hpp>
#include <pspp/meta.hpp>
#include <pspp/utility/declaration_macros.hpp>
#include <pspp/iterator_options.hpp>

namespace pspp
{

namespace _private
{
template <typename T>
concept c_offset = requires (T t)
{
	static_cast<std::int64_t>(t);
};

}

template <typename DataT, typename ... Options>
struct iterator
{
	using this_type = iterator<DataT, Options...>;
	using iterator_type = iterator<DataT>;

	using options = iterator_options::options_set<Options...>;

#define CHECK_IF_ALLOWED(which, type_name, caller) \
	if constexpr (not options::do_store_as_value){\
		if constexpr (not std::same_as<type_name, DataT>){\
		if constexpr (options::which ## _conversions::completely_disabled_all)\
			static_assert(not options::which ## _conversions::completely_disabled_all,\
				caller ": conversion " #which " other data type is completely disabled by type specification");\
		else\
		static_assert(options::which ## _conversions::template is_enabled<type_name>,\
				caller ": conversion " #which " other data type is disabled for this type by type specification");\
		}\
	}


	using value_type = std::conditional_t<
		options::is_view,
		DataT const,
		DataT>;

	using stored_value_type = std::conditional_t<
		options::do_store_as_value,
		DataT,
		value_type *
		>;

	using const_value_type = std::remove_const_t<value_type> const;
	using pointer = value_type *;
	using const_pointer = const_value_type *;
	using reference = value_type &;
	using const_reference = const_value_type &;

	using difference_type = std::ptrdiff_t;


	using operator_return_type = std::conditional_t<
			options::do_operators_returns_by_reference,
			this_type &,
			this_type
		>;

	using const_operator_return_type = std::conditional_t<
		options::do_operators_returns_by_reference,
		this_type const &,
		this_type
	>;

	using with_assign_operator_return_type = std::conditional_t<
		options::operators_has_return_type,
		operator_return_type,
		void
		>;

	stored_value_type m_ptr;

	pspp_always_inline constexpr with_assign_operator_return_type
	return_this() noexcept
	{
		if constexpr (options::operators_has_return_type)
			return *this;
	}



	struct _operators
	{
		pspp_always_inline static constexpr auto plus(auto first, auto second) noexcept
		{
			if constexpr (options::is_reversed)
				return first - second;
			else
				return first + second;
		}

		pspp_always_inline static constexpr auto minus(auto first, auto second) noexcept
		{
			if constexpr (options::is_reversed)
				return first + second;
			else
				return second + first;
		}

		pspp_always_inline static constexpr bool bigger(auto first, auto second) noexcept
		{
			if constexpr (options::is_reversed)
				return first < second;
			else
				return first > second;
		}

		pspp_always_inline static constexpr bool smaller(auto first, auto second) noexcept
		{
			if constexpr (options::is_reversed)
				return first > second;
			else
				return first < second;
		}

		pspp_always_inline static constexpr bool bigger_or_equal(auto first, auto second) noexcept
		{
			if constexpr (options::is_reversed)
				return first <= second;
			else
				return first >= second;
		}

		pspp_always_inline static constexpr bool smaller_or_equal(auto first, auto second) noexcept
		{
			if constexpr (options::is_reversed)
				return first >= second;
			else
				return first <= second;
		}
	};

	template <typename OtherDataT>
	pspp_always_inline static constexpr auto _to_stored_type(OtherDataT && other) noexcept
	{
		if constexpr (options::do_store_as_value)
			return static_cast<stored_value_type>(other);
		else
			return reinterpret_cast<stored_value_type>(other);
	}

	pspp_always_inline constexpr auto _plus_assign_to_ptr(auto offset) noexcept
	{
		if constexpr (options::is_reversed)
			m_ptr -= offset;
		else
			m_ptr += offset;
	}

	pspp_always_inline constexpr auto _minus_assign_to_ptr(auto offset) noexcept
	{
		if constexpr (options::is_reversed)
			m_ptr += offset;
		else
			m_ptr -= offset;
	}

	pspp_always_inline static constexpr std::int64_t convert_to_int_if_alloved(auto && val) noexcept
	{
		if constexpr (std::integral<std::remove_cvref_t<decltype(val)>>)
			return val;
		else 
		{
			if constexpr (requires {static_cast<std::int64_t>(val);})
			{
				static_assert(options::enabled_index_convertion, 
					"pspp::iterator: convertion from non-integral values is not allowed by type specification");
				return static_cast<std::int64_t>(val);
			}
			else
			{
				static_assert(sizeof(val) < 0, "pspp::iterator: can not convert to int64_t");
				return 0;
			}
		}

	}

	inline constexpr iterator() requires (not options::do_store_as_value):
		m_ptr{nullptr}
	{}

	template <typename OtherData>
	constexpr iterator(OtherData * ptr)
		requires (not options::do_store_as_value):
		m_ptr{_to_stored_type(ptr)}
	{
		CHECK_IF_ALLOWED(from, OtherData, "iterator(*ptr)");
	}

	template <typename OtherData>
	constexpr iterator(OtherData & ptr):
		m_ptr{reinterpret_cast<pointer>(&ptr)}
	{
		CHECK_IF_ALLOWED(from, OtherData, "iterator(&ptr)");
	}

	template <typename OtherDataT, typename ... OtherOptions>
	constexpr iterator(iterator<OtherDataT, Options...> other_iter):
		m_ptr{reinterpret_cast<pointer>(other_iter.m_ptr)}
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "iterator(iterator<other>)");
	}

	constexpr iterator(DataT * ptr):
		m_ptr{ptr}
	{}

	constexpr iterator(DataT & ptr):
		m_ptr{&ptr}
	{}

	constexpr iterator(this_type &) = default;
	constexpr iterator(this_type const &) = default;
	constexpr iterator(this_type&&) = default;

	template <typename OtherDataT, typename ... OtherOptions>
	constexpr iterator(iterator<OtherDataT, OtherOptions...> & other):
		m_ptr{reinterpret_cast<pointer>(other.m_ptr)} 
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "iterator(iterator<other>&)")
	}

	template <typename OtherDataT, typename ... OtherOptions>
	constexpr iterator(iterator<OtherDataT, OtherOptions...> const & other):
		m_ptr{reinterpret_cast<pointer>(other.m_ptr)} 
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "iterator(const iterator<other>&)")
	}

	template <typename OtherDataT, typename ... OtherOptions>
	constexpr iterator(iterator<OtherDataT, OtherOptions...> && other):
		m_ptr{reinterpret_cast<pointer>(other.m_ptr)} 
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "iterator(iterator<other>&&)")
	}

	template <typename OtherDataT, typename ... OtherOptions>
	inline constexpr operator_return_type operator=(iterator<OtherDataT, OtherOptions...> other) noexcept
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "operator=(iterator<other>)");
		m_ptr = reinterpret_cast<pointer>(other.m_ptr);

		return *this;
	}


	template <typename OtherDataT, typename ... OtherOptions>
	inline constexpr operator_return_type operator=(iterator<OtherDataT, OtherOptions...> const & other) noexcept
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "operator=(const iterator<other>&)");
		m_ptr = reinterpret_cast<const_pointer>(other.m_ptr);

		return *this;
	}

	inline constexpr operator_return_type operator=(this_type const &) noexcept = default;
	inline constexpr operator_return_type operator=(this_type &) noexcept = default;
	inline constexpr operator_return_type operator=(this_type &&) noexcept = default;

	template <typename OtherDataT, typename ... OtherOptions>
	inline constexpr operator_return_type operator=(iterator<OtherDataT, OtherOptions...> & other) noexcept
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "operator=(iterator<other>)");
		m_ptr = reinterpret_cast<pointer>(other.m_ptr);

		return *this;
	}


	/*template <typename OtherDataT, typename ... OtherOptions>
	inline constexpr operator_return_type operator=(iterator<OtherDataT, OtherOptions...> && other) noexcept
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "operator=(iterator<other>&&)");
		m_ptr = other.m_ptr;

		return *this;
	}
	*/


	template <typename ResDataT>
	inline constexpr operator std::conditional_t<
		options::is_view, ResDataT const *, ResDataT*> 
	() noexcept 
	{
		CHECK_IF_ALLOWED(to, ResDataT, "operator const ResData *()");
		return reinterpret_cast<ResDataT*>(m_ptr);
	}

	template <typename ResDataT>
	inline constexpr operator ResDataT const * () const noexcept
	{
		CHECK_IF_ALLOWED(to, ResDataT, "operator const ResData *()");
		return reinterpret_cast<ResDataT const *>(m_ptr);
	}

	template <typename ResDataT>
	inline constexpr operator std::conditional_t<
		options::is_view, ResDataT const, ResDataT> & () noexcept
	{
		CHECK_IF_ALLOWED(to, ResDataT, "operator ResData &()");
		return reinterpret_cast<ResDataT*>(m_ptr);
	}

	template <typename ResDataT>
	inline constexpr operator ResDataT const & () const noexcept
	{
		CHECK_IF_ALLOWED(to, ResDataT, "operator const ResData &()");
		return reinterpret_cast<ResDataT*>(m_ptr);
	}

	inline constexpr operator pointer() noexcept
	{
		return m_ptr;
	}

	inline constexpr operator const_pointer () const noexcept
	{
		return m_ptr;
	}

	inline constexpr operator reference () noexcept
	{
		return m_ptr;
	}

	inline constexpr operator const_reference () const noexcept
	{
		return m_ptr;
	}

	template <typename OtherDataT, typename ... OtherOptions>
	inline constexpr difference_type operator-(iterator<OtherDataT, OtherOptions...> other) const noexcept
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "");
		return m_ptr - reinterpret_cast<DataT const *>(other.m_ptr);
	}

	template <typename OffsetT>
	inline constexpr this_type operator+(OffsetT && offset) const noexcept
	{
		return {_operators::plus(m_ptr, convert_to_int_if_alloved(std::forward<OffsetT>(offset)))};
	}

	template <typename OffsetT>
	inline constexpr this_type operator-(OffsetT && offset) const noexcept
	{
		return {_operators::minus(m_ptr, convert_to_int_if_alloved(std::forward<OffsetT>(offset)))};
	}

	template <typename OffsetT>
	inline constexpr with_assign_operator_return_type 
		operator+=(OffsetT && offset) noexcept
	{
		_plus_assign_to_ptr(
			convert_to_int_if_alloved(std::forward<OffsetT>(offset))
		);
		return return_this();
	}

	template <typename OffsetT>
	inline constexpr with_assign_operator_return_type 
		operator-=(OffsetT && offset) noexcept 
	{
		_minus_assign_to_ptr(
			convert_to_int_if_alloved(std::forward<OffsetT>(offset))
		);
		return return_this();
	}

	inline constexpr with_assign_operator_return_type
		operator++() noexcept
	{
		_plus_assign_to_ptr(1);
		return return_this();
	}

	inline constexpr this_type operator++(int) noexcept
	{
		_plus_assign_to_ptr(1);
		return this_type{_operators::minus(m_ptr, 1)};
	}

	inline constexpr with_assign_operator_return_type 
		operator--() noexcept 
	{
		_minus_assign_to_ptr(1);
		return return_this();
	}

	inline constexpr this_type operator--(int) noexcept
	{
		_minus_assign_to_ptr(1);
		return this_type{_operators::plus(m_ptr, 1)};
	}

	template <typename ... OtherOpts>
	inline constexpr bool operator>(iterator<DataT, OtherOpts...> other) const noexcept
	{
		return _operators::bigger(m_ptr, other.m_ptr);
	}

	template <typename OtherDataT, typename ... OtherOpts>
	inline constexpr bool operator>(iterator<OtherDataT, OtherOpts...> other) const noexcept
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "operator>(iterator<other>)");
		return _operators::bigger(m_ptr, reinterpret_cast<pointer>(other.m_ptr));
	}

	template <typename OtherDataT, typename ... OtherOpts>
	inline constexpr bool operator<(iterator<OtherDataT, OtherOpts...> other) const noexcept
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "operator<(iterator<other>)");
		return _operators::smaller(m_ptr, reinterpret_cast<pointer>(other.m_ptr));
	}

	template <typename OtherDataT, typename ... OtherOpts>
	inline constexpr bool operator==(iterator<OtherDataT, OtherOpts...> other) const noexcept
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "operator==(iterator<other>)");
		return m_ptr == reinterpret_cast<pointer>(other.m_ptr);
	}

	template <typename OtherDataT, typename ... OtherOpts>
	inline constexpr bool operator!=(iterator<OtherDataT, OtherOpts...> other) const noexcept
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "operator!=(iterator<other>)");
		return m_ptr != reinterpret_cast<pointer>(other.m_ptr);
	}

	template <typename OtherDataT, typename ... OtherOpts>
	inline constexpr bool operator >=(iterator<OtherDataT, OtherOpts...> other) const noexcept
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "operator>=(iterator<other>)");
		return _operators::bigger_or_equal(m_ptr, reinterpret_cast<pointer>(other.m_ptr));
	}

	template <typename OtherDataT, typename ... OtherOpts>
	inline constexpr bool operator <=(iterator<OtherDataT, OtherOpts...> other) const noexcept
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "operator<=(iterator<other>)");
		return _operators::smaller_or_equal(m_ptr, reinterpret_cast<pointer>(other.m_ptr));
	}

	template <typename OtherDataT, typename ... OtherOpts>
	inline constexpr auto operator <=>(iterator<OtherDataT, OtherOpts...> other) const noexcept
	{
		CHECK_IF_ALLOWED(from, OtherDataT, "operator<=>(iterator<other>)");
		if constexpr (options::is_reversed)
			return m_ptr <=> reinterpret_cast<pointer>(other.m_ptr);
		else return reinterpret_cast<pointer>(other.m_ptr) <=> m_ptr;
	}

	inline constexpr pointer operator->() noexcept
	{
		return m_ptr;
	}

	inline constexpr const_pointer operator->() const noexcept
	{
		return m_ptr;
	}

	inline constexpr reference operator*() noexcept
	{
		return *m_ptr;
	}

	inline constexpr const_reference operator*() const noexcept
	{
		return *m_ptr;
	}

	inline constexpr operator value_type() noexcept
		requires (pspp::with_reason<options::to_type_conversion_allowed,
			constr{"to type conversion forbiden by type specification; use operator*"}>)

	{
		return *m_ptr;
	}

	inline constexpr operator const_value_type () const noexcept
		requires (pspp::with_reason<options::to_type_conversion_allowed,
			constr{"to type conversion forbiden by type specification; use operator*"}>)
	{
		return *m_ptr;
	}
};

}

#endif // PSPP_ITERATOR_HPP
