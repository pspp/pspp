#ifndef PSPP_MEMORY_POOL_HPP
#define PSPP_MEMORY_POOL_HPP

#include <array>
#include <vector>
#include <cstdint>

#include <pspp/utility/declaration_macros.hpp>
#include <pspp/utility/utility.hpp>

namespace pspp
{

struct memory_pool_info
{
	std::size_t size;
	std::size_t max_chunks_count;


	consteval memory_pool_info(std::integral auto size_):
		size{static_cast<std::size_t>(size_)},
		max_chunks_count{20}
	{}

	consteval memory_pool_info(std::integral auto size_,
			std::integral auto max_chunks_count_):
		size{static_cast<std::size_t>(size_)},
		max_chunks_count{static_cast<std::size_t>(max_chunks_count_)}
	{}
};

struct memory_pool_section_info
{
	std::size_t start_pos;
	std::size_t length;
};

namespace detail
{
template <typename T = void>
pspp_always_inline constexpr std::size_t get_real_length(std::size_t length) noexcept
{
	if constexpr (std::same_as<void, T> or sizeof(T) == 1)
		return length;
	else
		return length << sqrt<sizeof(T)>();
}

struct dynamic_memory_manager
{
	void * mem_point;

	virtual ~dynamic_memory_manager()
	{
		::operator delete(mem_point);
	}
};

} // namespace detail

template <memory_pool_info t_info>
struct memory_pool
{
	static_prop_of std::size_t size = t_info.size;
	static_prop_of std::size_t max_chunks_count = t_info.max_chunks_count;
	static_prop_of std::size_t npos = -1;

	std::array<std::byte, t_info.size> m_data {};
	std::vector<memory_pool_section_info> m_section_infos{ {0, size} };
	std::vector<detail::dynamic_memory_manager> m_dynamic_memory_storages {};

	std::size_t used_chunks_size {1};

	constexpr std::size_t get_smallest_fitable(std::size_t length) const noexcept
	{
		for (std::size_t i = 0; i < m_section_infos.size(); ++i)
		{
			if (m_section_infos[i].length >= length)
				return i;
		}

		return npos;
	}

	template <typename T = void>
	inline constexpr bool can_borrow(std::size_t length) const noexcept
	{
		return m_section_infos.back().length >= detail::get_real_length<T>(length);
	}

	template <typename T = void, bool t_manual_memory_management = false>
	constexpr T* borrow(std::size_t borrow_length)
	{
		if constexpr (not (std::same_as<void, T> or sizeof(T) == 1))
			borrow_length = detail::get_real_length<T>(borrow_length);

		std::size_t section_index = get_smallest_fitable(borrow_length);
		if (section_index == npos)
		{
			if constexpr (t_manual_memory_management)
				return operator new(borrow_length);
			else 
			{
				m_dynamic_memory_storages.emplace_back(operator new(borrow_length));
				return m_dynamic_memory_storages.back().mem_point;
			}
		} else
		{
		}
	}

	constexpr bool was_dynamicly_allocated(void const * __restrict ptr) const noexcept
	{
		return ptr < m_data.data() or
					 (ptr >= (m_data.data() + m_data.size()));
	}

	template <bool t_manual_memory_management>
	pspp_always_inline constexpr void* borrow(std::size_t borrow_length) noexcept
	{
		return borrow<void, t_manual_memory_management>(borrow_length);
	}

	template <typename T = void>
	constexpr void give_back(void * __restrict pos, std::size_t mem_size) noexcept
	{
	}
};


template <std::size_t size, std::size_t max_chunks_count>
using memory_pool_with = memory_pool<memory_pool_info{size, max_chunks_count}>;

void foo()
{
	memory_pool<1000> tmp{};
}

} // namespace pspp

#endif // PSPP_MEMORY_POOL_HPP




