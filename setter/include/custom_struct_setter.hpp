#ifndef PSPP_CUSTOM_STRUCT_SETTER_HPP
#define PSPP_CUSTOM_STRUCT_SETTER_HPP

#include <pspp/custom_struct.hpp>
#include <pspp/setter.hpp>

namespace pspp
{

namespace detail
{

template <typename T>
struct is_custom_struct : is_complex_target_t<custom_struct>::is_target<T> {};

template <typename PtrT>
struct is_setter_with_custom_struct
{ static_val value = is_setter_holder_with<PtrT, is_complex_target_t<custom_struct>::is_target>::value; };

template <typename T>
concept c_setter_with_custom_struct  = is_setter_with_custom_struct<T>::value;

template <typename T>
struct is_type_holder_with_custom_struct : std::false_type
{};

template <c_custom_struct CustomStruct>
struct is_type_holder_with_custom_struct<type_t<CustomStruct>> : std::true_type {};

template <typename T>
concept c_type_holder_with_custom_struct = is_type_holder_with_custom_struct<T>::value;

} // namespace detail

template <detail::c_type_holder_with_custom_struct auto t_cs_holder>
struct setter_holder_fnt<t_cs_holder> : detail::setter_holder_fnt_base<t_cs_holder>
{
	using setter_base = detail::setter_holder_fnt_base<t_cs_holder>;

	template <auto t_caller>
    requires (not is_constr(t_caller))
	static consteval auto get_next() noexcept
	{ return setter_base::template get_next<t_caller>(); }

	template <constr t_next>
	static consteval auto get_next() noexcept
	{ return setter_holder<extract_type<t_cs_holder>::template member_ptr_to<t_next>>; }

	template <auto t_next>
	static_val next = get_next<t_next>();
};

template <detail::c_setter_with_custom_struct auto t_ptr>
struct setter_holder_fnt<t_ptr> : detail::setter_holder_fnt_base<t_ptr>
{
	using setter_base = detail::setter_holder_fnt_base<t_ptr>;

	template <auto t_caller>
    requires (not is_constr(t_caller))
  static consteval auto get_next() noexcept
	{ return setter_base::template next<t_caller>; }

	template <constr t_next>
	static consteval auto get_next() noexcept
	{ return setter_base::template value<
      setter_holder_fnt<
          setter_base::last_underlying_type::template member_ptr_to<t_next>
      >{}
  	>;
	}

	template <auto t_next>
	static_val next = get_next<t_next>();
};

} // namespace pspp

#endif // PSPP_CUSTOM_STRUCT_SETTER_HPP
