#ifndef PSPP_UTILITY_SETTER_HPP
#define PSPP_UTILITY_SETTER_HPP

#include <pspp/utility/declaration_macros.hpp>
#include <pspp/value_sequence.hpp>
#include <pspp/type_sequence.hpp>
#include <pspp/utility/utility.hpp>
#include <pspp/constr.hpp>

#include <utility>

namespace pspp
{

template <typename T>
struct custom_member_ptr_wrapper
{
  using parent_type = T;
  using underlying_type = T;
};

template <typename T, typename P>
struct custom_member_ptr_wrapper<T P::*>
{
  using parent_type = P;
  using underlying_type = T;
};

template <typename T, typename P>
struct custom_member_ptr_wrapper<T P::* const>
{
  using parent_type = P;
  using underlying_type = T;
};


template <typename T>
struct underlying_type_of_member_ptr
{ using type = T; };

template <typename Class, typename Member>
struct underlying_type_of_member_ptr<Member Class::*>
{ using type = Member; };

template <typename Class, typename Member>
struct underlying_type_of_member_ptr<Member Class::* const>
{ using type = Member; };

template <typename T>
struct underlying_type_of_member_ptr<custom_member_ptr_wrapper<T>>
{
  using type = typename custom_member_ptr_wrapper<T>::underlying_type;
};

template <typename T>
struct parent_type_of_member_ptr
{ using type = T; };

template <typename Class, typename Member>
struct parent_type_of_member_ptr<Member Class::*>
{ using type = Class; };

template <typename Class, typename Member>
struct parent_type_of_member_ptr<Member Class::* const>
{ using type = Class; };

template <typename T>
struct parent_type_of_member_ptr<custom_member_ptr_wrapper<T>>
{ using type = typename custom_member_ptr_wrapper<T>::parent_type; };

template <auto t_member_ptr>
using underlying_type_of_member_ptr_t = typename
	underlying_type_of_member_ptr<decltype(t_member_ptr)>::type;

template <typename T>
using tunderlying_type_of_member_ptr_t = typename
    underlying_type_of_member_ptr<T>::type;

template <auto t_member_ptr>
using parent_type_of_member_ptr_t = typename
	parent_type_of_member_ptr<decltype(t_member_ptr)>::type;

template <auto t_getter, auto t_val, bool t_do_not_execute_val = false>
struct recursive_setter_fnt
{
	static_val getter = t_getter;
	static_val value = t_val;
	static_flag do_not_execute_val = t_do_not_execute_val;
};

template <auto t_getter, auto t_val, bool t_do_not_execute_val = false>
inline constexpr recursive_setter_fnt<t_getter, t_val, t_do_not_execute_val> recursive_setter {};

constexpr auto pass_value = [] [[pspp_attr_always_inline]] (auto && val) {
	return pspp_fwd(val);
};

namespace detail
{

template <typename T, constr t_member_name>
inline constexpr auto member_ptr_to_wrapper = T::template get_member_ptr_to<t_member_name>();

template <typename DataT>
struct optional_member_ptr_wrapper
{ static_val t = type<DataT>; };

template <auto t_n, typename ... DataTs>
struct variant_member_ptr_wrapper
{
  static_val ts = typeseq<DataTs...>;
};

template <typename ... DataTs>
struct variant_member_ptr_holder_wrapper
{
  static_val ts = typeseq<DataTs...>;

  template <auto t_n>
  static_val at = ts | at<t_n>;
};

// XXX:
// create struct-wrapper for classes such above
// which have function inside, which allows to
// access an value in proper way

template <typename T>
struct member_ptr_wrapper
{};

template <auto t_ptr>
inline constexpr auto vmember_ptr_wrapper_v = member_ptr_wrapper<clean_type<t_ptr>>{};

template <typename T>
struct member_ptr_wrapper<optional_member_ptr_wrapper<T>>
{
  constexpr T & operator()(std::optional<T> & to_be_setted) const noexcept
  {
    if (not to_be_setted.has_value())
      to_be_setted.emplace();

    return to_be_setted.value();
  }

  using result_type = T;
};

template <auto t_n, typename ... DataTs>
struct member_ptr_wrapper<variant_member_ptr_wrapper<t_n, DataTs...>>
{
  constexpr auto & operator()(std::variant<DataTs...> & to_be_setted) const noexcept
  {
    if constexpr (c_type<clean_type<t_n>>)
    {
      using required_type = typename decltype(t_n)::t;

      if (not std::holds_alternative<required_type>(to_be_setted))
        to_be_setted.template emplace<required_type>();

      return std::get<typename decltype(t_n)::t>(to_be_setted);
    } else
    {
      if (to_be_setted.index() != t_n)
        to_be_setted.template emplace<t_n>();

      return std::get<t_n>(to_be_setted);
    }
  }

  using result_type =
      std::remove_reference_t<
        decltype(
            std::declval<member_ptr_wrapper>()(
                std::declval<std::variant<DataTs...>&>()
            )
       )
     >;
};

template <typename T>
concept c_has_member_ptr_wrapper = c_has_call_operator<
    member_ptr_wrapper<T>
   >;

} // namespace detail

template <auto t_ptr, auto t_val = pass_value, bool t_do_not_execute_val = false>
struct setter_fnt
{
	pspp_always_inline
	constexpr void pspp_static_call_const_operator(auto & to_be_setted, auto && ... args) noexcept 
	{
		if constexpr (c_complex_vtarget<clean_type<t_val>, recursive_setter_fnt>)
		{
			setter_fnt<
				static_call<t_val.getter, underlying_type_of_member_ptr_t<t_ptr>>(), 
				t_val.value, t_do_not_execute_val
			>{}(to_be_setted.*t_ptr, std::forward<decltype(args)>(args)...);
		}
		else
		{
			pspp_execute_if_can( t_val(to_be_setted.*t_ptr, pspp_fwd(args)...) );
			else pspp_execute_if_can( to_be_setted.*t_ptr = t_val );
      else if constexpr ( detail::c_has_member_ptr_wrapper<clean_type<t_ptr>> )
      {
        pspp_execute_if_can( t_val(
            detail::vmember_ptr_wrapper_v<t_ptr>(to_be_setted),
            pspp_fwd(args)...) );
        else pspp_execute_if_can( detail::vmember_ptr_wrapper_v<t_ptr> = t_val );
        else pspp_execute_if_can ( to_be_setted = t_val );
        else if constexpr ( not t_do_not_execute_val )
        {
          pspp_execute_if_can( detail::vmember_ptr_wrapper_v<t_ptr> = t_val(pspp_fwd(args)...) );
          else pspp_execute_if_can( t_ptr = t_val(pspp_fwd(args)...) );
          else static_assert(false, "t_val is not assignalbe");
        } else static_assert(false, "t_val is not assignable");
      }
			else if constexpr (not t_do_not_execute_val)
			{
				pspp_execute_if_can( to_be_setted.*t_ptr = t_val(std::forward<decltype(args)>(args)...) );
				else static_assert(false, "t_val is not assignable");
			}
			else static_assert(false, "t_val is not assignable");
		}
	}

	using underlying_type = underlying_type_of_member_ptr_t<t_ptr>;
	using parent_type = parent_type_of_member_ptr_t<t_ptr>;

	static_val ptr = t_ptr;
	static_val value = t_val;
};

template <setter_fnt ... t_setters>
struct setter_sequence : value_sequence<t_setters...>
{
	pspp_always_inline
	static constexpr void set(auto & val, auto && ... args) noexcept
	{
		(t_setters(val, std::forward<decltype(args)>(args)...), ...);
	}
};

template <setter_fnt ... t_setters>
struct is_sequence<setter_sequence<t_setters...>> : std::true_type {};

template <auto t_ptr, auto t_val = pass_value, bool t_do_not_execute_val = false>
inline constexpr setter_fnt<t_ptr, t_val, t_do_not_execute_val> setter {};

template <typename Class, typename Member>
using sref = Member Class::*;

template <typename Class, typename Member>
using mref = Member Class::*;



template <auto t_ptr>
struct setter_holder_fnt;

template <typename T>
concept c_setter = c_complex_vtarget<std::remove_cvref_t<T>, setter_fnt>;

template <typename T>
concept c_setter_holder = c_complex_vtarget<std::remove_cvref_t<T>, setter_holder_fnt>;


namespace detail
{

template <typename T>
static consteval auto get_last_type_from_setter_holder() noexcept
{
  using UnderT = typename decltype(
      []{
        if constexpr (c_has_member_ptr_wrapper<T>)
          return type<T>;
        else return type<tunderlying_type_of_member_ptr_t<T>>;
      }())::t;

  if constexpr (c_setter<UnderT>)
    return get_last_type_from_setter_holder<clean_type<T::value>>();
  else if constexpr (c_setter_holder<UnderT>)
    return get_last_type_from_setter_holder<underlying_type_of_member_ptr_t<T::ptr>>();
  else return type<UnderT>;
}


template <auto t_ptr>
struct setter_holder_fnt_base
{
  static_val ptr = t_ptr;
  static_val last_ptr = t_ptr;
  using last_underlying_type =
      std::remove_const_t<
        underlying_type_of_member_ptr_t<custom_member_ptr_wrapper<decltype(last_ptr)>{}>>;

  template <auto t_val>
  struct specialization { static_assert(std::same_as<decltype(t_val), void>); };

  template <auto t_val>
  requires (not c_complex_vtarget<decltype(t_val), setter_holder_fnt>)
  struct specialization<t_val> : value_holder<setter<t_ptr, t_val>>
  {};

  template <c_complex_vtarget<setter_holder_fnt> auto t_val>
  struct specialization<t_val> :
      value_holder<
          setter_holder_fnt<setter_fnt<t_ptr, t_val>{}>{}
      >
  {};

  /**
   * @brief returns specialized setter if @param t_val is value/setter or
   * creates next holder.
   *
   * @param t_val value to use for specialization
   */
  template <auto t_val = pass_value>
  static_val value = specialization<t_val>::value;

  template <auto t_caller>
    requires (not is_constr(t_caller))
  static consteval auto get_next() noexcept
  {
    return value<
        setter_holder_fnt<
        static_call<t_caller, underlying_type_of_member_ptr_t<t_ptr>>()
    >{}
    >;
  }

  template <auto t_caller>
  static_val next = get_next<t_caller>();
};

template <type_t t_t>
struct setter_holder_fnt_base<t_t>
{
  static_val ptr = t_t;

  template <auto t_caller>
  static consteval auto get_next() noexcept
  { return setter_holder_fnt<static_call<t_caller, typename decltype(t_t)::t>()>{}; }

  template <auto t_caller>
  static_val next = get_next<t_caller>();
};

template <setter_fnt t_ptr>
struct setter_holder_fnt_base<t_ptr>
{
  static_val ptr = t_ptr;

  /*
  template <auto t_ptr_iter = ptr.value>
  [[nodiscard]]
  static consteval auto get_last_ptr() noexcept
  {
    using PtrT = decltype(t_ptr_iter);
    if constexpr (c_setter<PtrT>)
      return get_last_ptr<PtrT::value>();
    else if constexpr (c_setter_holder<PtrT>)
      return get_last_ptr<PtrT::ptr>();
    else return t_ptr_iter;
  }
   */

  static_val last_ptr = get_last_type_from_setter_holder<clean_type<t_ptr>>() | unwrap;

  using last_underlying_type = std::remove_const_t<underlying_type_of_member_ptr_t<
      custom_member_ptr_wrapper<decltype(last_ptr)>{}
      >>;

  template <auto t_val>
  struct specialization {};

  template <auto t_val>
  requires (not c_complex_vtarget<decltype(t_val), setter_holder_fnt>)
  struct specialization<t_val> : value_holder<
      setter<
          t_ptr.ptr,
          t_ptr.value.template value<t_val>
      >
  >
  {};

  template <c_complex_vtarget<setter_holder_fnt> auto t_val>
  struct specialization<t_val> :
      value_holder<
          setter_holder_fnt<setter_fnt<t_ptr.ptr, t_ptr.value.template value<t_val>>{}>{}
      >
  {};

  template <auto t_val = pass_value>
  static_val value = specialization<t_val>::value;

  template <auto t_caller>
  static consteval auto get_next() noexcept
  {
    return value<
        setter_holder_fnt<
            static_call<t_caller, last_underlying_type>()
        >{}
    >;
  }

  template <auto t_caller>
  static_val next = get_next<t_caller>();
};

template <typename T, template <typename> class CheckerT>
struct is_type_holder_with : std::false_type
{};

template <template <typename> class CheckerT,
    c_satisfies_checker<CheckerT> Target, template <typename> class HolderT>
struct is_type_holder_with<HolderT<Target>, CheckerT> : std::true_type {};


} // namespace detail

template <auto t_ptr>
struct setter_holder_fnt : detail::setter_holder_fnt_base<t_ptr>
{};

template <auto t_ptr>
inline constexpr setter_holder_fnt<t_ptr> setter_holder {};

template <c_complex_target<std::optional> T>
consteval auto get_special_member_ptr_for() noexcept
  requires (c_complex_target<T, std::optional>)
{ return detail::optional_member_ptr_wrapper<holded_type_t<T>>{}; }

template <auto t_n, c_complex_target<std::variant> T>
consteval auto get_special_member_ptr_for() noexcept
{ return detail::variant_member_ptr_wrapper<t_n, holded_type_sequence_t<T>>{}; }

namespace detail
{
template <typename PtrT, template <typename> class Checker>
struct is_setter_holder_with
{
  static_flag value = Checker<typename decltype(get_last_type_from_setter_holder<PtrT>())::t>::value;
};

template <typename PtrT, template <typename> class Checker>
concept c_setter_holder_with = is_setter_holder_with<PtrT, Checker>::value;

template <typename T>
struct has_member_ptr_wrapper
{ static_flag value = c_has_member_ptr_wrapper<std::remove_cvref_t<T>>; };

} // namespace detail

template <
    detail::c_setter_holder_with<
        is_complex_target_t<std::optional>::is_target
        > auto t_ptr>
struct setter_holder_fnt<t_ptr> :
    detail::setter_holder_fnt_base<t_ptr>
{
    using base = detail::setter_holder_fnt_base<t_ptr>;

    template <auto t_caller = 0>
    static consteval auto get_next() noexcept
    {
      return base::template value<
          setter_holder_fnt<
              get_special_member_ptr_for<typename base::last_underlying_type>()
          >{}
        >;
    }

    template <auto t_caller = 0>
    static_val next = get_next<t_caller>();

    static_val optional_setter_with_default = next<>.template value<[](auto const &){
    }>;
};

template <
    detail::c_setter_holder_with<detail::has_member_ptr_wrapper> auto t_ptr>
struct setter_holder_fnt<t_ptr> : detail::setter_holder_fnt_base<t_ptr>
{
  using base = detail::setter_holder_fnt_base<t_ptr>;
//  static_assert(std::same_as<decltype(t_ptr), void>);

  template <auto t_caller> requires (not is_constr(t_caller))
  static consteval auto get_next() noexcept
  {
    return setter_holder_fnt<static_call<
        t_caller,
        typename base::last_underlying_type
    >()>{};
  }

  template <constr t_member_name>
  static consteval auto get_next() noexcept
  {
    return base::template value <
        setter_holder_fnt<
          detail::member_ptr_to_wrapper<typename decltype(base::last_underlying_type::t)::t, t_member_name>
        >{}
      >;
  }

  template <auto t_caller>
  static_val next = get_next<t_caller>();
};

template <
    detail::c_setter_holder_with<
    is_type_holder_with_like<
      is_complex_target_t<std::optional>::is_target
    >::template is_target
   > auto t_opt_holder>
struct setter_holder_fnt<t_opt_holder> : detail::setter_holder_fnt_base<t_opt_holder>
{
  template <auto t_caller = 0>
  static consteval auto get_next() noexcept
  {
    return setter_holder_fnt<
        get_special_member_ptr_for<
            underlying_type_of_member_ptr_t<t_opt_holder>
        >()
      >{};/*.template value<setter_holder_fnt<
      static_call<t_caller, typename decltype(*t_opt_holder)::value_type>()
    >>;*/
  }

  template <auto t_caller = 0>
  static_val next = get_next<t_caller>();

  static_val optional_setter_with_default = next<>.template value<[](auto & to_set){
    to_set.emplace(typename decltype(to_set)::value_type{});
  }>;
};

} // namespace pspp

#endif // PSPP_UTILITY_SETTER_HPP
