#ifndef PSPP_PERROR_H
#define PSPP_PERROR_H

#include <string>
#include <string_view>
#include <variant> 
#include <tuple>
#include <source_location>

#include <pspp/useful/concepts.hpp>

namespace pspp
{
inline namespace err
{
	enum class error_type : int
	{
		unloged, ignorable, warning, serious, critical
	};


	class error {
		mutable bool raised = false;
		mutable int error_code = 0;

		std::string reason;

		error_type type;

		public: 
		error(const std::string& msg = "", error_type type = error_type::ignorable);
		error(const error&);
		error(const error&&);

		error& operator=(error const & old)
		{
			raised = old.raised;
			reason = old.reason;
			type = old.type;
			
			return *this;
		}
		error& operator=(error&& old)
		{
			raised = old.raised;
			reason = old.reason;
			type = old.type;

			return *this;
		}

		inline bool is_ok() const { return !raised; }
		inline bool is_bad() const { return raised; }

		std::string_view what() const;

		[[nodiscard]]
		inline error_type get_type() const;

		void reset(const std::string& msg = "");
		void reset(const std::string& msg, error_type type);
		void reset(std::string_view msg, error_type type);

		void retype(error_type _type);

		void raise_with_type(error_type _type, int error_code = 1, std::source_location location = std::source_location::current()) const;

		inline void raise(int error_code = 1, std::source_location location = std::source_location::current()) const
		{
			raise_with_type(type, error_code, location);
		}
	};

	error rag_error(const std::string& msg = "error occured", error_type _type = error_type::critical, int error_code = 1, std::source_location location = std::source_location::current());

	inline error rag_error_from(error const & _error, error_type _type, int error_code = 1, std::source_location location = std::source_location::current())
	{
		return rag_error(std::string{_error.what().begin(), _error.what().end()}, _type, error_code, location);
	}
	inline error rag_error_from(error const & _error, int error_code, std::source_location location = std::source_location::current())
	{
		return rag_error_from(_error, _error.get_type(), error_code, location);
	}
	inline error rag_error_from(error & _error, error_type _type, int error_code = 1, std::source_location location = std::source_location::current())
	{
		_error.retype(_type);
		_error.raise(error_code, location);

		return _error;
	}

	inline error rag_error_from(error & _error, int error_code = 1, std::source_location location = std::source_location::current())
	{
		_error.raise(error_code, location);
		return _error;
	}



	error ok();


	template <class RaiseFunc>
	class error_custom : private error{

		RaiseFunc* raise_func;

		public: 
		error_custom(const std::string& msg = "", error_type type = error_type::ignorable, RaiseFunc* = nullptr);
		error_custom(const std::string& msg = "", error_type type = error_type::ignorable, RaiseFunc = nullptr);

		template <class ...Args>
		void raise(Args ...args);
	};


	template <class RaiseFunc>
	error_custom<RaiseFunc>::error_custom(const std::string& msg, error_type type , RaiseFunc* func) : error(msg, type), raise_func(func)
	{
		if constexpr (std::is_same<RaiseFunc, std::nullptr_t>())
		{
			throw raise_func;
		}
	}


	/*
	error() ->
	error<std::nullptr_t>;

	
	error(const std::string&, error_type, std::nullptr_t) ->
	error<std::nullptr_t>;

	template <class RaiseFunc>
	error(const std::string&, error_type, RaiseFunc*) ->
	error<RaiseFunc>;


	template <class RaiseFunc>
	concept callable = requires(RaiseFunc* rf) {
		requires std::is_function<RaiseFunc>();
	};
	template <callable RaiseFunc>
	error(const std::string&, error_type, RaiseFunc*) ->
	error<RaiseFunc>;
	*/


	template <class RaiseFunc>
	template <class ...Args>
	void error_custom<RaiseFunc>::raise(Args ...args)
	//void error_custom<RaiseFunc>::raise(Args ...args)
	{
		this->error::raised = true;

		raise_func(args...);
	}

	template <class obj_t>
	using obj_and_err = std::pair<obj_t, error>;

	template <class obj_t>
	using obj_or_err = std::variant<obj_t, error>;

  template <class obj_t>
  class obj_or_err_t
  {
    obj_or_err<obj_t> data;

  public:

/*    obj_or_err_t(obj_or_err<obj_t> obj):
      data{obj}
    {}

    obj_or_err_t(obj_or_err<obj_t> const & obj):
      data{obj}
    {}
		*/

    obj_or_err_t(obj_or_err<obj_t> && obj):
      data{std::move(obj)}
    {}

		/*
		obj_or_err_t(obj_t obj):
			data{obj}
		{}

		obj_or_err_t(obj_t const & obj):
			data{obj}
		{}
		*/

		obj_or_err_t(obj_t && obj):
			data{std::move(obj)}
		{}

		/*

		obj_or_err_t(error err):
			data{err}
		{}

		obj_or_err_t(error const & err):
			data{err}
		{}
		*/

		obj_or_err_t(error && err):
			data{std::move(err)}
		{}

    bool was_ok() const
    {
      return data.index() == 0;
    }
    bool was_bad() const
    {
      return data.index();
    }

    obj_t get_or_die() const
    {
      return std::get<obj_t>(data);
    }

    obj_t get(obj_t default_value) const
    {
      if (was_ok())
        return std::get<obj_t>(data);

      return default_value;
    }

    obj_t get() const requires (pspp::usf::concepts::default_constructable<obj_t>)
    {
      if (was_ok())
        return std::get<obj_t>(data);
      else
        return {};
    }

		error get_failure_reason() const
		{
			if (was_bad())
				return std::get<error>(data);
			
			return ok();
		}
  };

	template <class obj_t>
	obj_t get_or_f(obj_or_err<obj_t>& obj, auto* func, auto& ...parr)
	{
		if (obj.index())
		{
			return std::get<obj_t>(obj);
		}

		func(parr...);
	}

	template <class obj_t>
	obj_t get_or_throw(obj_or_err<obj_t>& obj)
	{
		if (obj.index())
			return std::get<obj_t>(obj);

		throw std::get<error>(obj).what();
	}

	template <class obj_t>
	obj_t get_or_throw(obj_or_err<obj_t>&& obj)
	{
		if (obj.index())
			return std::get<obj_t>(obj);

		throw std::get<error>(obj).what();
	}

	template <class ...obj_t>
	using err_with_objs = std::tuple<error, obj_t...>;

	template <class ...obj_t>
	err_with_objs<obj_t...> pack(obj_t& ...objects)
	{
		return std::make_tuple(ok(), std::move(objects)...);
	}

	template <class ...obj_t>
	err_with_objs<obj_t...> tie(error& err, const obj_t& ...objects){
		return std::make_tuple(std::move(err), std::move(objects)...);
	}

	template <class ...obj_t>
	err_with_objs<obj_t...> tie(error&& err, obj_t&& ...objects){
		return std::make_tuple(err, std::move(objects)...);
	}

	template <class ...obj_t>
	err_with_objs<obj_t...> tie_ok(obj_t&& ...objects)
	{
		return std::make_tuple(ok(), std::move(objects)...);
	}
	template <class ...obj_t>
	err_with_objs<obj_t...> tie_ok(obj_t& ...objects)
	{
		return std::make_tuple(ok(), std::move(objects)...);
	}

} // namespace err
} // namespace pspp

#endif // PSPP_PERROR_H

