#include <pspp/error.hpp>
#include <iostream>

pspp::err::obj_with_err<int> some_func(int a){
	if (a) {
		return 100 / a;
	}

	return pspp::err::rag_error("a must be no 0 value", pspp::err::error_type::critical, 1);
}

pspp::err::objs_with_err<int, std::string> some_func2(int a, int b)
{
	if (!(a + b))
	{
		return pspp::err::pack(pspp::err::error("a + b must not be equal 0", pspp::err::error_type::serious), 0, std::string());
	}
	else
		return pspp::err::pack(a+b,std::string("success"));
}

pspp::err::error some_func_with_not_critical_error(int a)
{
	return pspp::err::rag_error("some warning", pspp::err::error_type::warning, 1);
}

int main()
{
	{
	auto [_, b, s] = some_func2(1, -1);
	}
	{
	auto [_, b, s] = some_func2(1, 2);
	}

	std::ignore = some_func_with_not_critical_error(10);

	int a = 0;
	std::cout << "100 / " << a << " = " << pspp::err::get_or_throw<int>(some_func(a)) << '\n';
	std::cout << "100 / " << 1 << " = " << pspp::err::get_or_throw<int>(some_func(5)) << '\n';


	return 0;

}

