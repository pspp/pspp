#include <pspp/error.hpp>
#include <pspp/log.hpp>

namespace pspp
{
inline namespace err 
{

	error::error(const std::string& _msg, error_type _type) : 
		reason(_msg), type(_type)
	{
		//log::l.set_flags_for_all(log::l.log->use_locationf()->use_timef());
	}

	error::error(const error& _err):
    raised{_err.raised},
		reason{_err.reason},
        type{_err.type}
	{
		//log::l.set_flags_for_all(log::l.log->not_use_locationf());
		//_err.raise();
		//log::l.set_flags_for_all(log::l.log->use_locationf());
	}

	error::error(const error&& _err):
    raised{_err.raised},
		reason{_err.reason},
        type{_err.type}
	{
		//log::l.set_flags_for_all(log::l.log->not_use_locationf());
		_err.raise();
		//log::l.set_flags_for_all(log::l.log->use_locationf());
	}


	/* inline bool error::is_ok() const */ 
	/* { */
	/* 	return !raised; */
	/* } */

	std::string_view error::what() const
	{
		return std::string_view(reason);
	}

	void error::reset(const std::string& msg)
	{
		raised = false;
		reason = msg;
	}

	void error::reset(const std::string& msg, error_type _type)
	{
		raised = false;
		reason = msg;
		type = _type;
	}

	void error::reset(std::string_view msg, error_type _type)
	{
		raised = false;
		reason = msg;
		type = _type;
	}

	[[nodiscard]]
	inline error_type error::get_type() const 
	{
		return type;
	}
	void error::retype(error_type _type)
	{
		type = _type;
	}

	void error::raise_with_type(error_type _type, int code, std::source_location location) const
	{
		raised = true;
		error_code = code;

		if (_type == error_type::unloged)
			return;

		std::putc('\n', stdout);

		log::l.log_on();

		switch (_type) 
		{
			case error_type::ignorable:
				pspp::log::l.ilogn("ERROR_INFO: " + reason, location);
				break;
			case error_type::warning:
				pspp::log::l.wlogn("ERROR_WARNING: " + reason, location);
				break;
			case error_type::serious:
				pspp::log::l.elogn("ERROR: " + reason, location);
				break;
			default:
				pspp::log::l.elogn("CRITICAL_ERROR: " + reason, location);
				exit(code);
		}
	}
	/*
	inline error rag_error_from(error & _error, error_type type, int error_code, std::source_location location)
	{
		_error.retype(type);
		_error.raise(error_code, location);

		return _error;
	}
	*/

	error rag_error(const std::string& msg, error_type type, int error_code, std::source_location location)
	{
		error res(msg, type);
		res.raise(error_code, location);

		return res;
	}

	error ok()
	{
		return error("", error_type::unloged);
	}

}
} // namespace pspp::err

