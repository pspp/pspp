# Type Helpers

Includes: `pspp::type_t` and `pspp::type_sequence`

Helper library for type handling.

## pspp::type\_t 

Used for passing type. Same as std::type\_identity;


## pspp::type\_sequence

Helper struct for type sequence handling.


## Usage Example

### type\_t

```cpp
#include <pspp/type_helpers.hpp>

template <typename T>
consteval auto get_new_type()
{
    if constexpr (std::same_as<T, int>)
        return pspp::type_t<char>{};
    else
        reutnr pspp::type_t<T>{};
}

template <typename T>
using new_type = pspp::extract_type<get_new_type<T>()>;

int main()
{
    using type = new_type<int>; // type = char
}

```

### type\_sequence

```cpp
#include<variant>

#include <pspp/type_sequence.hpp>

template <typename ... Ts>
void handle_types()
{
    using seq = pspp::type_sequence<Ts...>;
    static_assert(seq::count > 2); // same as sizeof...(Ts) > 2

    typename seq::template at<2> val; // val have same type as second type from sequence

    typename seq::unique::template passed_type<std::variant> var_val; 
    //↑ var_val is same as std::variant<Ts...> but without duplicated types
    
    // ...
}

```



