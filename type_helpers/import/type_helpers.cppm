
export module pspp.type_helpers;

import <concepts>;

export namespace pspp::type_helpers
{

struct nullt {};
inline constexpr nullt nulltv{};

template <typename T>
struct type_t
{
	template <typename DataT>
	constexpr type_t (DataT&&) noexcept {} 

	constexpr type_t() noexcept {}

	using t = T;
};

template <typename T>
[[nodiscard]] consteval inline type_t<T> to_type(T) noexcept
{
	return {};
}

consteval inline bool is_type_t_helper(auto) noexcept 
{
	return false;
}

template <typename T>
consteval inline bool is_type_t_helper(type_t<T>) noexcept
{
	return true;
}

template <typename MaybeTypeT>
concept type = is_type_t_helper(MaybeTypeT{});

template <type auto t_t>
using extract_type = typename decltype(t_t)::t;

template <unsigned int t_index>
struct index_t
{
	inline constexpr static unsigned int n() noexcept
	{
		return t_index;
	}
};

//#define index(n) index_t<n>{}

template <typename DataT>
type_t(DataT&&) -> type_t<DataT>;


template <typename MaybeCallableWithTypeT>
concept type_t_handler_fn = requires(MaybeCallableWithTypeT fn)
{
	fn(type_t<nullt>{});
};

template <typename MaybeTypeTCheckerFnT>
concept type_t_checker_fn = requires(MaybeTypeTCheckerFnT fn)
{
	{ fn(type_t<nullt>{}) } -> std::same_as<bool>;
};

template <typename T0, typename ... Ts>
struct type_sequence
{
	template <unsigned int t_n>
		requires (t_n < sizeof...(Ts) + 1)
	[[nodiscard]] inline static constexpr auto get_n(index_t<t_n>) noexcept
	{
		return type_sequence<Ts...>::get_n(index_t<t_n - 1>{});
	}

	[[nodiscard]] inline static constexpr auto get_n(index_t<0>) noexcept
	{
		return type_t<T0>{};
	}

	[[nodiscard]] inline static constexpr auto get_last() noexcept
	{
		return get_n(index_t<sizeof...(Ts)>{});
	}

public:
	using head = T0;
	using tail = typename decltype(get_last())::t;

	template <unsigned int t_n>
	using at = typename decltype(get_n(index_t<t_n>{}))::t;
	
	template <unsigned int t_n>
	using at_from_back = typename decltype(get_n(index_t<sizeof...(Ts) - t_n>{}))::t;

	inline static constexpr unsigned int count = sizeof...(Ts) + 1;

	inline static constexpr auto shift() noexcept
	{
		static_assert(sizeof...(Ts) > 0, "type_sequence: trying to shift when no types left");
		return type_sequence<Ts...>{};
	}

	inline static constexpr auto t_shift() noexcept
	{
		return type_t<std::remove_cvref_t<decltype(shift())>>{};
	}

	inline static constexpr auto sshift() noexcept
	{
		if constexpr (sizeof...(Ts) == 0)
			return type_sequence<nullt>{};
		else
			return type_sequence<Ts...>{};
	}

	template <type_t_checker_fn auto callable>
	inline static consteval bool check_each()
	{
		if constexpr (sizeof...(Ts) == 0)
			return callable(type_t<T0>{});
		else
			return callable(type_t<T0>{}) and type_sequence<Ts...>::template check_each<callable>();
	}

	template <type_t_handler_fn auto callable>
	inline static consteval void for_each()
	{
		callable(type_t<T0>{});

		if constexpr (sizeof...(Ts) == 0)
			return;
		else
			type_sequence<Ts...>::template for_each<callable>();
	}
};

inline consteval bool is_type_sequence(auto)
{
	return false;
}

template <typename T0, typename ...Ts>
inline consteval bool is_type_sequence(type_sequence<T0, Ts...>)
{
	return true;
}

template <typename MaybeTypesSeqT>
concept type_sequence_c = requires (MaybeTypesSeqT mts)
{
	requires is_type_sequence(mts);
};

template <typename ...Ts>
using ts_head = typename type_sequence<Ts...>::head;

template <type_sequence_c TST>
using tst_head = typename TST::head;

template <type_sequence_c auto t_tsv>
using tsv_head = typename decltype(t_tsv)::head;

template <typename ...Ts>
using ts_tail = typename type_sequence<Ts...>::tail;

template <type_sequence_c TST>
using tst_tail = typename TST::tail;

template <type_sequence_c auto t_tsv>
using tsv_tail = typename decltype(t_tsv)::tail;

template <unsigned long t_n, typename ...Ts>
using ts_at = typename type_sequence<Ts...>::template at<t_n>;

template <unsigned int t_n, type_sequence_c TST>
using tst_at = typename TST::template at<t_n>;

template <unsigned long t_n, type_sequence_c auto t_tsv>
using tsv_at = typename decltype(t_tsv)::template at<t_n>;


template <template <typename EmbT> class WithEmbT, typename EmbT>
inline consteval bool has_embeded_fn(type_t<WithEmbT<EmbT>>)
{
	return true;
}

inline consteval bool has_embeded_fn(auto)
{
	return false;
}

template <typename MaybeHasEmbT>
concept has_embeded = requires()
{
	requires has_embeded_fn(type_t<MaybeHasEmbT>{});
};



template <unsigned int t_n = 1, template <typename EmbT> class WithEmbT, typename EmbT>
consteval type_t<EmbT> get_n_embeded_type(type_t<WithEmbT<EmbT>>)
{
	if constexpr (t_n == 1 or t_n == 0)
		return {};
	else 
	{
		static_assert(has_embeded<EmbT>, "get_n_embeded_type: n is to large (no so much embeded types)");
		return get_n_embeded_type<t_n - 1>(type_t<EmbT>{});
	}
}

template <typename MaybeHasNEmbT, unsigned int t_n>
concept has_n_embeded = requires()
{
	get_n_embeded_type<t_n>(type_t<MaybeHasNEmbT>{});
};

template <type auto t_with_emb, unsigned int t_n = 1>
	requires has_n_embeded<extract_type<t_with_emb>, t_n>
using extract_n_embeded = extract_type<get_n_embeded_type<t_n>(t_with_emb)>;

} // 

#define EXTEND(...) __VA_ARGS__

#define ADD_CHECKER_FUNC(type_name, template_decl, template_usage)\
	template < EXTEND template_decl>\
	inline consteval bool is_ ## type_name( type_name < EXTEND template_usage > ) { return true; } \
	\
	inline consteval bool is_ ## type_name( auto ) { return false; } \
	\
	template <typename Maybe ## type_name ## T> \
	concept c_ ## type_name = is_ ## type_name( Maybe ## type_name ## T {} );



