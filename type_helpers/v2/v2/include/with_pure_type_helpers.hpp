#ifndef PSPP_PURE_TYPE_HELPERS_HPP
#define PSPP_PURE_TYPE_HELPERS_HPP

#include <pspp/type_helpers.hpp>
#include <type_traits>

namespace pspp::type_helpers
{

template <typename ...Ts>
using pure_ts_head = std::remove_cvref_t<ts_head<Ts...>>;

template <type_seq TST>
using pure_tst_head = std::remove_cvref_t<tst_head<TST>>;

template <type_seq auto t_tsv>
using pure_tsv_head = std::remove_cvref_t<tsv_head<t_tsv>>;


template <typename ...Ts>
using pure_ts_tail = std::remove_cvref_t<ts_tail<Ts...>>;

template <type_seq TST>
using pure_tst_tail = std::remove_cvref_t<tst_tail<TST>>;

template <type_seq auto t_tsv>
using pure_tsv_tail = std::remove_cvref_t<tsv_tail<t_tsv>>;


template <unsigned int t_n, typename ...Ts>
using pure_ts_at = std::remove_cvref_t<ts_at<t_n, Ts...>>;

template <unsigned int t_n, type_seq TST>
using pure_tst_at = std::remove_cvref_t<tst_at<t_n, TST>>;

template <unsigned int t_n, type_seq auto t_tsv>
using pure_tsv_at = std::remove_cvref_t<tsv_at<t_n, t_tsv>>;

} // pspp::type_helpers

#endif // PSPP_PURE_TYPE_HELPERS_HPP

