
#ifndef pspp_type_helpers
#define pspp_type_helpers

#include <concepts>
#include <typeinfo>

#include <pspp/useful/sequence_helpers.hpp>
#include <pspp/value_holder.hpp>
#include <pspp/nullt.hpp>

namespace pspp
{
inline namespace meta
{

template <typename T>
struct type_t
{
	template <typename DataT>
	constexpr type_t (DataT) noexcept {}

	constexpr type_t() noexcept {}

  static constexpr auto clean() noexcept -> type_t<std::remove_cvref_t<T>>
  { return {}; }

  static constexpr auto declval() noexcept -> T;

	using t = T;
};

} // inline namespace meta
namespace detail
{
	enum class static_call_for_ts
	{
		TTN,
		VhTN,
		TAtN,
		NTAt,
		TAvN,
		NTAv,
		empty,
		not_callable
	};

	template <auto t_func, typename ... Args, typename ... NonTemplateArgs>
	inline consteval static_call_for_ts static_call_for_ts_checker(type_t<NonTemplateArgs> ...) noexcept
	{
		using enum static_call_for_ts;

		if constexpr (requires { t_func.template operator()<Args...>(std::declval<NonTemplateArgs>()...); })
			return TTN;
		else if constexpr (requires { t_func.template operator()<Args::value...>(std::declval<NonTemplateArgs>()...); })
			return VhTN;
		else if constexpr (requires {t_func(type_t<Args>{}..., std::declval<NonTemplateArgs>()...);})
			return TAtN;
		else if constexpr (requires {t_func(std::declval<NonTemplateArgs>()..., type_t<Args>{}...);})
			return NTAt;
		else if constexpr (requires {t_func(Args{}..., std::declval<NonTemplateArgs>()...);})
			return TAvN;
		else if constexpr (requires {t_func(std::declval<NonTemplateArgs>()..., Args{}...);})
			return NTAv;
		else if constexpr (requires {t_func();})
			return empty;
		else return not_callable;
	}

}

inline namespace meta
{

template <auto t_func, typename ... Args, typename ... NonTemplateArgs>
inline constexpr auto static_call(NonTemplateArgs && ... args) noexcept -> decltype(auto)
	requires (detail::static_call_for_ts_checker<t_func, Args...>(type_t<NonTemplateArgs>{}...) != detail::static_call_for_ts::not_callable)
{
  pspp_execute_and_return_if_can(t_func.template operator()<Args...>(std::forward<NonTemplateArgs>(args)...));
  else pspp_execute_and_return_if_can(t_func.template operator()<Args::value...>(std::forward<NonTemplateArgs>(args)...));
  else pspp_execute_and_return_if_can(t_func(type_t<Args>{}..., std::forward<NonTemplateArgs>(args)...));
	else pspp_execute_and_return_if_can(t_func(std::forward<NonTemplateArgs>(args)..., type_t<Args>{}...));
  else pspp_execute_and_return_if_can(t_func(Args{}..., std::forward<NonTemplateArgs>(args)...));
  else pspp_execute_and_return_if_can(t_func(std::forward<NonTemplateArgs>(args)..., Args{}...));
  else pspp_execute_and_return_if_can(t_func());
	else static_assert(false, "static_call: t_func is not callable");
}

template <auto t_func, auto t_arg1, auto ... t_args, typename ... NonTemplateArgs>
inline constexpr auto static_call(NonTemplateArgs && ... args) noexcept
{
	if constexpr (requires { t_func.template operator()<t_arg1, t_args...>(std::forward<NonTemplateArgs>(args)...); })
		return t_func.template operator()<t_arg1, t_args...>(std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires { t_func(t_arg1, t_args..., std::forward<NonTemplateArgs>(args)...); })
		return t_func(t_arg1, t_args..., std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires { t_func(std::forward<NonTemplateArgs>(args)..., t_arg1, t_args...); })
 		return t_func(std::forward<NonTemplateArgs>(args)..., t_arg1, t_args...);
	else if constexpr (requires { t_func(); })
		return t_func();
	else static_assert(sizeof(t_func) < 0, "static_call: t_func is not callable");
}

template <auto t_func, typename Arg1, auto t_arg2, auto ... t_args, typename ... NonTemplateArgs>
inline constexpr auto static_call(NonTemplateArgs && ... args) noexcept
{
	if constexpr (requires { t_func.template operator()<Arg1, t_arg2, t_args...>(std::forward<NonTemplateArgs>(args)...); })
		return t_func.template operator()<Arg1, t_arg2, t_args...>(std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires { t_func(type_t<Arg1>{}, t_arg2, t_args..., std::forward<NonTemplateArgs>(args)...); })
		return t_func(type_t<Arg1>{}, t_arg2, t_args...);
	else if constexpr (requires { t_func.template operator()<Arg1>(t_arg2, t_args..., std::forward<NonTemplateArgs>(args)...); })
		return t_func.template operator()<Arg1>(t_arg2, t_args..., std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires { t_func.template operator()<Arg1>(std::forward<NonTemplateArgs>(args)..., t_arg2, t_args...); })
		return t_func.template operator()<Arg1>(std::forward<NonTemplateArgs>(args)..., t_arg2, t_args...);
	else if constexpr (requires { t_func(Arg1{}, t_arg2, t_args..., std::forward<NonTemplateArgs>(args)...); })
		return t_func(Arg1{}, t_arg2, t_args..., std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires { t_func(std::forward<NonTemplateArgs>(args)..., Arg1{}, t_arg2, t_args...); })
		return t_func(std::forward<NonTemplateArgs>(args)..., Arg1{}, t_arg2, t_args...);
	else if constexpr (requires { t_func(); })
		return t_func();
	else static_assert(sizeof(t_func) < 0, "static_call: t_func is not callable");
}

template <typename T>
[[nodiscard]] consteval inline type_t<T> to_type(T) noexcept
{
	return {};
}

consteval inline bool is_type_t_helper(auto) noexcept
{
	return false;
}

template <typename T>
consteval inline bool is_type_t_helper(type_t<T>) noexcept
{
	return true;
}

template <typename MaybeT>
struct is_type_t : std::false_type {};

template <typename SomeT>
struct is_type_t<type_t<SomeT>> : std::true_type {};

template <typename MaybeTypeT>
concept c_type = is_type_t<std::remove_cvref_t<MaybeTypeT>>::value;

namespace meta_detail
{
	template <typename T>
	struct _extract_type_impl
	{};

	template <typename T, template <typename> class HolderT>
	struct _extract_type_impl<HolderT<T>>
	{ using type = T; };

	template <auto t_val>
	using _extract_type_impl_t = typename _extract_type_impl<decltype(auto{t_val})>::type;
} // namespace meta_detail

template <auto t_t>
using extract_type = meta_detail::_extract_type_impl_t<t_t>;

template <unsigned int t_index>
struct index_t
{
	inline consteval static unsigned int n() noexcept
	{
		return t_index;
	}
};

//#define index(n) index_t<n>{}

template <typename DataT>
type_t(DataT&&) -> type_t<DataT>;

template <typename MaybeCallableWithTypeT>
concept type_t_handler_fn = requires(MaybeCallableWithTypeT fn)
{
	fn(type_t<nullt>{});
};

template <typename MaybeTypeTCheckerFnT>
concept type_t_checker_fn = requires(MaybeTypeTCheckerFnT fn)
{
	{ fn(type_t<nullt>{}) } -> std::same_as<bool>;
};

template <auto t_val_fn, typename ... Args>
inline static constexpr auto types_passed_value = static_call<t_val_fn, Args...>();

/*
template <class ... Bases>
struct inheritance_base : Bases... {};
 */

template <auto t_class_transformer, class ... Bases>
inline static consteval auto inheritance_base_impl()
{
	return static_call<t_class_transformer, Bases...>();
}

template <auto t_class_transformer, class ... Bases>
using inheritance_base_t = extract_type<inheritance_base_impl<t_class_transformer, Bases...>()>;

template <class ... Bases>
using simple_inheritance_base = inheritance_base<Bases...>;

template <template <typename ...> class Target, typename T>
struct is_tstype_t : std::false_type {};

template <template <typename ...> class Target, typename ... Ts>
struct is_tstype_t<Target, Target<Ts...>> : std::true_type {};

template <template <typename ...> class Target>
struct is_tstype
{
	template <typename T>
	using res = is_tstype_t<Target, T>;
};

template <typename T>
struct is_type_holder_with
{
  template <typename U>
  struct is_target : std::false_type {};

  template <template <typename> class HolderT>
  struct is_target<HolderT<T>> : std::true_type {};
};

template <template <typename> class Checker>
struct is_type_holder_with_like
{
  template <typename U>
  struct is_target : std::false_type {};

  template <typename U, template <typename> class HolderT>
  struct is_target<HolderT<U>> : std::bool_constant<Checker<U>::value>
  {};
};

template <auto t_v>
struct is_value_holder_with
{
  template <typename T>
  struct is_target : std::false_type {};

  template <template <auto> class HolderT>
  struct is_target<HolderT<t_v>> : std::true_type {};
};

template <typename T, template <typename> class HolderT>
struct is_type_holder_with<HolderT<T>>
{};

template <auto val>
using clean_type = std::remove_cvref_t<decltype(val)>;

template <typename T>
inline constexpr auto type = type_t<T>{};

template <typename T>
inline constexpr auto unref_type = type_t<std::remove_reference_t<T>>{};

template <typename T>
inline constexpr auto copy_type = type_t<decltype(auto{std::declval<T>()})>{};

template <auto t_val>
inline constexpr auto vtype = type<extract_type<t_val>>;

template <auto t_val>
using decltype_t = type_t<decltype(t_val)>;

template <auto t_val>
using clean_decltype_t = type_t<clean_type<t_val>>;

template <auto t_val>
inline constexpr bool is_vnullt = std::same_as<clean_type<t_val>, nullt>;

template <typename T1, typename T2>
struct tpair
{
	using first = T1;
	using second = T2;
};

template <typename T>
struct type_holder { using type = T; };

template <std::size_t t_size>
struct dummy_type {
  raw_array_t<char, t_size> m_dummy_data {""};
};


template <typename T>
using dummy_type_t = dummy_type<sizeof(T)>;

template <typename T, typename U>
concept c_same_as = std::same_as<std::remove_cvref_t<T>, std::remove_cvref_t<U>>;

template <typename T, typename U>
concept c_different = not c_same_as<T, U>;

inline constexpr type_t<void> void_t {};

template <auto val, typename T>
struct put_new_value_to
{ using type = T; };

template <template <auto> class HolderT, auto old_val, auto new_val>
struct put_new_value_to<new_val, HolderT<old_val>>
{ using type = HolderT<new_val>; };

template <auto t_new_val, typename HolderT>
using put_new_value_to_t = typename put_new_value_to<t_new_val, HolderT>::type;

template <auto t_new_val, auto t_prev_val_holder>
using vput_new_vlue_to_t = copy_extensions_t<decltype(t_prev_val_holder),
	typename put_new_value_to_t<t_new_val,
		std::remove_cvref_t<decltype(t_prev_val_holder)>>::type
	>;

template <typename T>
struct is_values_holder : std::false_type {};

template <template <auto ...> class HolderT, auto ... t_vals>
struct is_values_holder<HolderT<t_vals...>> : std::true_type {};

template <typename T>
struct is_types_holder : std::false_type {};

template <template <typename...> class HolderT, typename ... Ts>
struct is_types_holder<HolderT<Ts...>> : std::true_type {};

template <typename T>
concept c_values_holder = is_values_holder<T>::value;

template <typename T>
concept c_types_holder = is_types_holder<T>::value;

template <typename T>
concept c_holder = c_values_holder<T> or c_types_holder<T>;

template <typename T, typename U>
consteval auto is_same() noexcept
{ return std::same_as<std::remove_cvref_t<T>, std::remove_cvref_t<U>>; }

template <typename T, auto t_u>
consteval auto is_same() noexcept
{ return std::same_as<std::remove_cvref_t<T>, clean_type<t_u>>; }

template <auto t_t, typename U>
consteval auto is_same() noexcept
{ return std::same_as<clean_type<t_t>, std::remove_cvref_t<U>>; }

template <auto t_t, auto t_u>
consteval auto is_same() noexcept
{ return std::same_as<clean_type<t_t>, clean_type<t_u>>; }

template <typename ToBeChecked, typename TargetT>
concept c_type_holder_with = is_type_holder_with<TargetT>::template
    is_target<ToBeChecked>::value;

template <typename ToBeChecked, auto t_target>
concept c_value_holder_with = is_value_holder_with<t_target>::template
    is_target<ToBeChecked>::value;

template <typename T>
struct holded_type
{ using type = T; };

template <typename T, template <typename...> class HolderT>
struct holded_type<HolderT<T>>
{ using type = T; };

template <typename T>
using holded_type_t = typename holded_type<T>::type;

template <typename T, template <typename> class CheckerT>
concept c_type_holder_with_like = is_type_holder_with_like<CheckerT>::template is_target<T>::value;

template <template <typename> class HolderT, template <typename> class CheckerT>
struct is_specific_type_holder_with_like
{
  template <typename U>
  struct is_target : std::false_type {};

  template <typename U>
  struct is_target<HolderT<U>> : std::bool_constant<CheckerT<U>::value>
  {};
};

template <template <typename> class HolderT, typename T>
struct is_specific_type_holder_with
{
  template <typename U>
  struct is_target : std::false_type {};

  template <typename U>
  struct is_target<HolderT<U>> : std::is_same<T, U> {};
};

template <typename T, template <typename> class HolderT, template <typename> class CheckerT>
concept c_specific_type_holder_with_like =
is_specific_type_holder_with_like<HolderT, CheckerT>::template is_target<T>::value;

template <typename T, template <typename> class HolderT, typename U>
concept c_specific_type_holder_with =
is_specific_type_holder_with<HolderT, U>::template is_target<T>::value;

#define typeptr(Typename) &typeid(Typename)
#define fast_tsame(Typename1, Typename2) typeptr(Typename1) == typeptr(Typename2)
#define fast_tdif(Typename1, Typename2) typeptr(Typename1) != typeptr(Typename2)

template <typename T, typename ... Ts>
inline constexpr bool is_some_of_v = (... or (fast_tsame(T, Ts)));

template <typename ... Ts>
struct is_some_of
{
  template <typename T>
  struct is_target
  {
    static_val value = (... or (fast_tsame(T, Ts)));
  };
};

template <typename T, typename ... Ts>
concept c_some_of = is_some_of<Ts...>::template is_target<T>::value;

template <typename T>
struct is_const_lref : std::false_type {};

template <typename T>
struct is_const_lref<T const &> : std::true_type {};

template <typename T>
concept c_const_lref = is_const_lref<T>::value;

template <template <typename> class ... Checker>
inline constexpr auto satisfies = []<typename T>{ return (Checker<T>::value and ...); };

template <typename ... Target>
inline constexpr auto same_as = []<typename T>{
  using t = std::remove_cvref_t<T>;
  return (std::same_as<t, Target> and ...);
};

template <typename ... Target>
inline constexpr auto absolutely_same_as = []<typename T>{
  return (std::same_as<T, Target> and ...);
};


template <typename Target>
struct is_same_as_t
{
  template <typename T>
  struct is_target : value_holder<std::same_as<T, Target>>
  {};
};

template <template <typename ...> class Target>
struct is_complex_target_t
{
  template <typename T>
  struct is_target : std::false_type {};

  template <typename ... Types>
  struct is_target<Target<Types...>> : std::true_type {};
};

template <template <auto ...> class Target>
struct is_complex_vtarget_t
{
  template <typename T>
  struct is_target : std::false_type {};

  template <auto ... t_vals>
  struct is_target<Target<t_vals...>> : std::true_type {};
};

template <template <typename, auto ...> class Target>
struct is_complex_tvtarget_t
{
  template <typename T>
  struct is_target : std::false_type{};

  template <typename T, auto ... t_vals>
  struct is_target<Target<T, t_vals...>> : std::true_type {};
};

template <template <auto, typename...> class Target>
struct is_complex_vttarget_t
{
  template <typename T>
  struct is_target : std::false_type{};

  template <auto t_val, typename ... Ts>
  struct is_target<Target<t_val, Ts...>> : std::true_type {};
};

template <typename T>
struct is_type_holder : std::false_type {};

template <typename T, template <typename> class HolderT>
struct is_type_holder<HolderT<T>>: std::true_type {};


template <typename T>
concept c_type_holder = is_type_holder<std::remove_cvref_t<T>>::value;

template <typename T, template <typename ...> class Target>
concept c_complex_target = is_complex_target_t<Target>::template is_target<std::remove_cvref_t<T>>::value;

template <typename T, template <typename, auto ...> class Target>
concept c_complex_tvtarget = is_complex_tvtarget_t<Target>::template is_target<std::remove_cvref_t<T>>::value;

template <typename T, template <auto, typename ...> class Target>
concept c_complex_vttarget = is_complex_vttarget_t<Target>::template is_target<std::remove_cvref_t<T>>::value;

template <typename T, template <auto ...> class Target>
concept c_complex_vtarget = is_complex_vtarget_t<Target>::template is_target<std::remove_cvref_t<T>>::value;

template <typename T>
concept c_has_index = requires(T t){ t.index; };

template <template <typename ...> class ... Targets>
struct is_some_of_complex_targets_t
{
  template <typename T>
  struct is_target : std::bool_constant<
    (... or c_complex_target<T, Targets>)>
  {};
};

template <template <auto ...> class ... Targets>
struct is_some_of_complex_vtargets_t
{
  template <typename T>
  struct is_target : std::bool_constant<
    (... or c_complex_vtarget<T, Targets>)>
  {};
};

template <template <typename, auto ...> class ... Targets>
struct is_some_of_complex_tvtargets_t
{
  template <typename T>
  struct is_target : std::bool_constant<
    (... or c_complex_tvtarget<T, Targets>)>
  {};
};

template <template <auto, typename ...> class ... Targets>
struct is_some_of_complex_vttargets_t
{
  template <typename T>
  struct is_target : std::bool_constant<
    (... or c_complex_vttarget<T, Targets>)>
  {};
};

template <typename T, template <typename ...> class ... Targets>
concept c_some_of_complex_targets = is_some_of_complex_targets_t<Targets...>::template is_target<T>::value;
template <typename T, template <auto ...> class ... Targets>
concept c_some_of_complex_vtargets = is_some_of_complex_vtargets_t<Targets...>::template is_target<T>::value;
template <typename T, template <auto, typename ...> class ... Targets>
concept c_some_of_complex_vttargets = is_some_of_complex_vttargets_t<Targets...>::template is_target<T>::value;
template <typename T, template <typename, auto ...> class ... Targets>
concept c_some_of_complex_tvtargets = is_some_of_complex_tvtargets_t<Targets...>::template is_target<T>::value;


template <template <typename ...> class Target>
inline constexpr auto same_as_complex = []<typename T>{ return is_complex_target_t<Target>::template is_target<T>::value; };

template <template <auto, typename ...> class Target>
inline constexpr auto same_as_complex_vt = []<typename T>{ return is_complex_vttarget_t<Target>::template is_target<T>::value; };

template <template <typename, auto ...> class Target>
inline constexpr auto same_as_complex_tvalue = []<typename T>{ return is_complex_tvtarget_t<Target>::template is_target<T>::value; };

template <template <auto ...> class Target>
inline constexpr auto same_as_complex_value = []<typename T>{ return is_complex_vtarget_t<Target>::template is_target<T>::value; };

template <typename ... Ts>
inline constexpr auto some_of = []<typename T>{ return (std::same_as<T, Ts> or ...); };

template <typename T>
struct is_member_ptr : std::false_type
{};

template <typename T, typename U>
struct is_member_ptr<T U::*> : std::true_type
{};

template <typename T>
concept c_member_ptr = is_member_ptr<std::remove_cvref_t<T>>::value;


template <typename First, typename ... Rest>
constexpr First get_first_type() noexcept;

template <typename First, typename ... Rest>
constexpr First get_first_type(First, Rest...);

template <>
constexpr nullt get_first_type() noexcept;


template <typename ... Ts>
using inline_head = decltype(get_first_type<Ts...>());

template <template <typename> class Checker, typename ... Ts>
inline constexpr bool is_sequence_one_type_which_v =
  sizeof...(Ts) == 1 and Checker<inline_head<Ts...>>::value;


static_assert(
  std::same_as<inline_head<int, char>, int>
  );

} // meta

template <typename SizeT = std::size_t, typename DataT>
  requires std::is_default_constructible_v<DataT> and (not requires {
    static_size_of<SizeT>(type<DataT>);
  })
constexpr auto size(type_t<DataT> data) noexcept -> SizeT
{ return size(DataT()); }

template <typename SizeT = std::size_t, typename DataT>
  requires requires { static_size_of<SizeT>(type<DataT>); }
constexpr auto size(type_t<DataT> data) noexcept -> SizeT
{ return static_size_of(type<DataT>); }




} // pspp

#endif // pspp_type_helpers


