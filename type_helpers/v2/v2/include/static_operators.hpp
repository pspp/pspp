#ifndef PSPP_STATIC_OPERATORS
#define PSPP_STATIC_OPERATORS

#include <pspp/value_holder.hpp>
#include <pspp/type_helpers.hpp>

namespace pspp
{
namespace op
{
inline namespace basic
{
	struct addt{};
	struct subt{};
	struct productt{};
	struct quotientt{};
	struct remindert{};

	struct equalt{};
	struct lesst{};
	struct greatert{};

	struct landt{};
	struct lort{};

template <c_value_holder First, c_value_holder Second>
inline consteval auto add(type_t<First>, type_t<Second>) noexcept
{
	return type<value_holder<First::value + Second::value>>; 
}

template <c_value_holder First, c_value_holder Second>
inline consteval auto sub(type_t<First>, type_t<Second>) noexcept
{
	return type<value_holder<First::value - Second::value>>; 
}

template <c_value_holder First, c_value_holder Second>
inline consteval auto multiply(type_t<First>, type_t<Second>) noexcept
{
	return type<value_holder<First::value * Second::value>>;
}

template <c_value_holder First, c_value_holder Second>
inline consteval auto divive(type_t<First>, type_t<Second>) noexcept
{
	return type<value_holder<First::value / Second::value>>;
}

template <c_value_holder First, c_value_holder Second>
inline consteval auto modulus(type_t<First>, type_t<Second>) noexcept
{
	return type<value_holder<First::value % Second::value>>;
}

template <c_value_holder First, c_value_holder Second>
inline consteval auto exponantiate(type_t<First>, type_t<Second>)
{
	if constexpr (sub(type<First>, type<value_holder<1>>))
		return type<value_holder<First::value>>;
}

namespace callers
{
} // namespcae callers
inline constexpr auto add_caller = []<c_value_holder First,
					c_value_holder Second>
				{return type<value_holder<First::value + Second::value>>; };

inline constexpr auto sub_caller = []<c_value_holder First,
					c_value_holder Second>
				{ return type<value_holder<First::value - Second::value>>; };

} // namespace basic
} // namespace  op
} // namespace pspp

#endif // PSPP_STATIC_OPERATORS

