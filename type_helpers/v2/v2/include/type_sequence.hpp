#ifndef PSPP_TYPE_SEQUENCE_HPP
#define PSPP_TYPE_SEQUENCE_HPP

#include <pspp/type_helpers.hpp>
#include <pspp/utility/utility.hpp>
#include <pspp/compile_time_messages.hpp>
#include <pspp/useful/sequence.hpp>
#include <pspp/with_reason.hpp>
#include <pspp/adaptor_pipe_closure.hpp>
#include <pspp/simple/temporary_storage.hpp>

#include <pspp/simple/ranges.hpp>


#include <string_view>
#include <optional>
#include <charconv>
#include <cstdint>
#include <typeinfo>
#include <tuple>

namespace pspp::inline meta
{

template <auto>
consteval bool always_false() noexcept { return false; }

template <typename>
consteval bool always_false() noexcept {return false; }



template <typename MaybeTypeCheckerFn>
concept type_requirments_checker_fn = requires (MaybeTypeCheckerFn fn)
{
	{ fn(type<int>) } -> std::same_as<bool>;
};

template <std::size_t t_first, std::size_t ... t_rest>
[[nodiscard]] inline consteval static type_t<std::index_sequence<t_rest...>>
pop_first_from_indexes(std::index_sequence<t_first, t_rest...>)
{
	return {};
}

template <typename T>
struct is_type_sequence : std::false_type {};

template <template <typename ...> class T>
struct is_type_sequence_type : std::false_type {};

template <typename T>
struct is_sequence : std::false_type {};

template <typename T>
concept c_sequence = is_sequence<std::remove_cvref_t<T>>::value and requires {
  std::remove_cvref_t<T>::size;
};

template <typename MaybeTS>
concept c_type_sequence = is_type_sequence<std::remove_cvref_t<MaybeTS>>::value;

template <typename ... Ts>
struct type_sequence;

/*template <typename SizeT = std::size_t, c_sequence SequenceT>
[[nodiscard]]
constexpr auto size(SequenceT &&) noexcept -> SizeT
{
  return std::remove_cvref_t<SequenceT>::size;
}
 */


using type_info = std::type_info const *;

struct type_iter
{
	type_info info;
	std::size_t index;

	constexpr bool operator==(type_iter const & other) const noexcept
	{ return info == other.info; }
	constexpr bool operator!=(type_iter const &) const noexcept = default;

	constexpr auto operator<=>(type_iter const & other) const noexcept
	{ return index <=> other.index; }
};

namespace meta_detail
{
	template <std::size_t t_n, typename T>
	struct _type_node : std::type_identity<T>
	{};

	template <typename ... Ts>
	struct _types : Ts...
	{};

	template <bool t_first, typename Ft, typename St>
	consteval auto get_first_(std::pair<Ft, St>) noexcept
	{
		if constexpr (t_first) return &std::pair<Ft, St>::first;
		else                   return &std::pair<Ft, St>::second; 
	}


	template <std::size_t ... t_indexes, typename ... Ts>
	consteval auto _make_types_impl(std::index_sequence<t_indexes...>, type_sequence<Ts...>)
	-> _types<_type_node<t_indexes, Ts>...>;

	template <typename ... Ts>
	consteval auto _make_types() -> decltype(
	_make_types_impl(std::declval<std::make_index_sequence<sizeof...(Ts)>>(),
	                 std::declval<type_sequence<Ts...>>())
	);

	template <typename ... Ts>
	using _types_t = decltype(_make_types<Ts...>());
}



template <typename ... Ts>
struct type_sequence
{
	using ids_t = std::array<type_iter, sizeof...(Ts)>;

	constexpr static auto const & ids() noexcept
	{
		constexpr static ids_t res = [] {
			std::size_t i{0};
			return ids_t{type_iter{&typeid(Ts), i++}...};
		}();

		return res;
	};

	static_size size = sizeof...(Ts);

	template <std::integral auto t_pos, template <auto...> class Holder>
	consteval static auto operator[](Holder<t_pos>) noexcept
	{
		return type_t<decltype(_at_impl<t_pos>(
				std::declval<std::add_pointer_t<meta_detail::_types_t<Ts...>>>()))>{};
	}

	template <auto t_reproduce_info, std::size_t t_n = std::size(t_reproduce_info)>
	constexpr static auto reproduce() noexcept
	{
		return reproduce_impl<t_reproduce_info>(std::make_index_sequence<t_n>{});
	}

	consteval bool operator==(type_sequence) const noexcept { return true; }
  template <typename ValT>
	consteval bool operator==(ValT) const noexcept { return std::same_as<type_sequence, ValT>; }

  template <typename ValT>
	consteval bool operator!=(ValT) const noexcept {return not std::same_as<type_sequence, ValT>; }

	template <typename ... Us, template <typename ...> class SeqT>
	constexpr auto operator+(SeqT<Us...>) const noexcept
	{ return type_sequence<Ts..., Us...>{}; }

	[[nodiscard]]
	constexpr static bool empty() noexcept { return size == 0; }

  constexpr type_sequence() noexcept = default;
  constexpr type_sequence(const type_sequence &) noexcept = default;
  constexpr type_sequence(type_sequence &&) noexcept = default;

  template <template <typename ...> class HolderT>
  constexpr type_sequence(HolderT<Ts...>) noexcept
    requires (not c_type_sequence<HolderT<Ts...>>)
  {}

  template <template <typename ...> class HolderT>
  constexpr type_sequence(type_t<HolderT<Ts...>>)
  {}

	private:
	template <auto t_reproduce_info, std::size_t ... t_is>
	constexpr static auto reproduce_impl(std::index_sequence<t_is...>) noexcept
	{
		if constexpr (c_has_index<typename decltype(t_reproduce_info)::value_type>)
			return type_sequence<extract_type<operator[](val<t_reproduce_info[t_is].index>)>...>{};
		else
			return type_sequence<extract_type<operator[](val<t_reproduce_info[t_is]      >)>...>{};
	}
};

template <template <typename...> class Holdert, typename ... Ts>
  requires (not c_type_sequence<Holdert<Ts...>>)
type_sequence(Holdert<Ts...>) -> type_sequence<Ts...>;

template <template <typename...> class Holdert, typename ... Ts>
type_sequence(type_t<Holdert<Ts...>>) -> type_sequence<Ts...>;


using empty_type_sequence = type_sequence<>;
inline constexpr auto emptytseq = empty_type_sequence{};


template <typename MaybeTs, typename ... Ts>
concept c_typed_type_sequence = std::same_as<MaybeTs, type_sequence<Ts...>>;

/*template <typename DataT, std::size_t t_n1, std::size_t t_n2>
constexpr static std::array<DataT, t_n1 + t_n2> operator+(std::array<DataT, t_n1> const & f, std::array<DataT, t_n2> const & s) noexcept
{
	std::array<DataT, t_n1 + t_n2> res {};
	std::copy(f.begin(), f.end(), res.begin());
	std::copy(s.begin(), s.end(), res.begin() + f.size());

	return res;
}*/

template <typename T, std::size_t t_index>
struct indexed_type
{
	using type = T;
	static_size index = t_index;
};

/*template <typename T1>
concept c_already_cmprable = requires(T1 t1) { t1 == t1; };

template <typename T1, template <typename> class HolderT>
	requires (not requires {HolderT<T1>{} == HolderT<T1>{}; })
constexpr bool operator==(HolderT<T1>, HolderT<T1>) noexcept
{ return true; }

template <typename T1, typename T2, template <typename> class HolderT>
	requires (not requires {HolderT<T1>{} == HolderT<T2>{}; })
constexpr bool operator==(HolderT<T1>, HolderT<T2>) noexcept
{ return false; }
*/

inline constexpr struct to_value_t {
  template <typename T>
  constexpr T pspp_static_call_const_operator() noexcept (noexcept(T{}))
  { return T{}; }
} to_value {};

namespace meta_detail
{
	template <std::integral auto t_pos, typename T>
	consteval auto _at_impl(_type_node<t_pos, T>*) -> T;

	template <typename T>
	struct is_sequence_handler : std::false_type {};

	template <typename ResT, c_sequence SeqT, typename ... ArgsT>
	struct is_sequence_handler<ResT(SeqT, ArgsT...)> : std::true_type {};

	template <typename T>
	concept c_sequence_handler = is_sequence_handler<T>::value;

	template <c_type_sequence Ts>
	struct _extract_type_impl<Ts>
	{using type = Ts; };

	template <typename T>
	struct _extract_type_impl<type_sequence<T>>
	{using type = type_sequence<T>; };
}

template <template <typename ...> class ReturnTHolder = type_t>
struct return_as
{
	template <typename ... Ts>
	using type = ReturnTHolder<Ts...>;
};

template <typename T>
struct forbid_plus_operator_for : std::false_type {};

template <typename T>
concept c_plus_operator_allowed = not forbid_plus_operator_for<T>::value;


template <template <typename> class HolderU,
					typename T, c_plus_operator_allowed U>
constexpr type_sequence<T, U> operator+(type_t<T>, HolderU<U>) noexcept
    requires (c_plus_operator_allowed<HolderU<U>>)
{ return {}; }

template <template <typename> class HolderU,
    typename T, c_plus_operator_allowed U>
constexpr type_sequence<T, U> operator+(std::type_identity<T>, HolderU<U>) noexcept
requires (c_plus_operator_allowed<HolderU<U>>)
{ return {}; }


template <template <typename> class HolderT, typename T, typename ... Ts>
  requires (not c_type_sequence<HolderT<T>>)
constexpr type_sequence<T, Ts...> operator+(HolderT<T>, type_sequence<Ts...>) noexcept
{ return {}; }

template <typename ... Ts>
struct is_type_sequence<type_sequence<Ts...>> : std::true_type  {};

template <typename ... Ts>
struct is_sequence<type_sequence<Ts...>> : std::true_type {};


namespace mimpl 
{

inline constexpr auto extract_info = [](type_iter const & it) {return it.info; };

template <std::size_t t_pos, bool t_return_as_is = false, template <typename> class ReturnTHolder = type_t>
struct at_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
#ifdef __cpp_pack_indexing
    if constexpr (t_return_as_is) return Ts...[t_pos]{};
    else return ReturnTHolder<Ts...[t_pos]>{};
#else
    using res_t = decltype(_at_impl<t_pos>(
      std::declval<std::add_pointer_t<meta_detail::_types_t<Ts...>>>()));
    if constexpr (t_return_as_is) return res_t{};
    else return ReturnTHolder<res_t>{};
#endif
	}
};

template <std::integral auto t_pos, bool t_return_as_is = false, template <typename ...> class ReturnTHolder = type_t>
struct at_from_back_fnt : adaptor_pipe_closure
{

	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
		requires (t_pos < sizeof...(Ts))
	{
		using res_t = decltype(_at_impl<sizeof...(Ts) - t_pos - 1>(
				std::declval<std::add_pointer_t<meta_detail::_types_t<Ts...>>>()));
		if constexpr (t_return_as_is) return res_t{};
		else return ReturnTHolder<res_t>{};
	}
};

template <auto t_func, auto ... t_extra>
struct pass_index_sequence_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		return []<std::size_t ... t_is>(std::index_sequence<t_is...>){
			return static_call<t_func, t_extra..., t_is...>();
		}(std::make_index_sequence<sizeof...(Ts)>{});
	}
};

template <auto t_mask, bool t_revert = false>
struct reproduce_from_mask_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		static constexpr auto info = []{
			std::array<std::size_t, sizeof...(Ts)> info {};
			std::size_t count = 0;
			for (std::size_t i = 0; i < info.size(); ++i)
				if (t_revert ^ t_mask[i])
					info[count++] = i;

			return std::pair{info, count};
		}();

		return type_sequence<Ts...>{}.template reproduce<info.first, info.second>();
	}
};

struct unique_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
    if constexpr (sizeof...(Ts) == 0) return emptytseq;
    else
    {
      using ts = type_sequence<Ts...>;
      constexpr auto const & ids = ts::ids();

      constexpr auto res = [&ids]{
        std::size_t count {0};
        std::array<std::size_t, ids.size()> new_ids = {};
        std::array<bool, ids.size()> anti_flags{};

        for (; count < ids.size() - 1; ++count)
        {
          if (anti_flags[count]) continue;

          for (std::size_t i = count + 1; i < ids.size(); ++i)
            if(ids[count] == ids[i]) anti_flags[i] = true; //int char char
        }

        std::size_t i = 0;
        count = 0;
        for (bool anti_flag : anti_flags)
          if (anti_flag) { ++i; continue;}
          else new_ids[count++] = i++;

        return std::pair{new_ids, count};
      }();

      return ts::template reproduce<res.first, res.second>();
    }
	}
};

struct is_unique_fnt : adaptor_pipe_closure
{
	constexpr bool pspp_static_call_const_operator(empty_type_sequence) noexcept
	{ return true; }

	template <typename ... Ts>
	constexpr bool pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		using ts = type_sequence<Ts...>;
		constexpr auto const & ids = ts::ids();

		for (std::size_t i = 0; i < ids.size() - 1; ++i)
			for (std::size_t j = i + 1; j < ids.size(); ++j)
				if (ids[i] == ids[j])
					return false;

		return true;
	}
};

template <auto t_comp_fun>
struct custom_unique_fnt : adaptor_pipe_closure
{
	constexpr auto pspp_static_call_const_operator(empty_type_sequence) noexcept
	{ return empty_type_sequence{}; }

	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		static constexpr auto count = sizeof...(Ts);
		using is_t = std::make_index_sequence<count>;

		static constexpr auto vals = 
			[]<std::size_t ... t_is>(std::index_sequence<t_is...>)
			{
				return std::array{
					[]<typename T, std::size_t t_i>
					{
						return std::array{
							(t_i > t_is and static_call<t_comp_fun, T, Ts>())...
						};
					}.template operator()<Ts, t_is>()...
				};
			}(is_t{});

		static constexpr auto mask = []{
			//std::array<std::size_t, count> tmp {};
			std::array<bool, count> tmp {false};
			std::size_t new_count = 0;
			for (auto i = 0; i < count; ++i)
				for (auto j = 0; j < count; ++j)
					tmp[i] |= vals[i][j];
			return tmp;
		}();

		return reproduce_from_mask_fnt<mask, true>{}(type_sequence<Ts...>{});
	}
};

struct subseq_info
{
	std::ptrdiff_t begin;
	std::ptrdiff_t end;
	std::ptrdiff_t step;
	bool valid;

	enum struct reason_t
	{
		ok,
		bad_begin_type,
		bad_end_type,
		bad_step_type,
		begin_bigger_than_size,
		end_bigger_than_size,
	};

	reason_t reason;
};

template <typename T, typename Dest>
concept c_castable_to = requires (T val) { static_cast<Dest>(val); };

template <auto t_begin, auto t_end, auto t_step, std::size_t t_ts_size>
consteval auto check_and_get_subseq_info()
{
	static constexpr auto to_size = []<typename ResT, auto val, ResT def_val>-> std::optional<ResT>
	{
		if constexpr (std::same_as<std::remove_cvref_t<decltype(val)>, std::size_t> and val == npos)
			return def_val;
		else if constexpr (c_castable_to<decltype(val), ResT>) // integral or castable
			return static_cast<ResT>(val);
		else if constexpr (requires { {val.value()} -> c_castable_to<ResT>; })
			return static_cast<ResT>(val.value());
		else if constexpr (requires { {val.operator*()} -> c_castable_to<ResT>; })
			return static_cast<ResT>(*val);
		else return std::nullopt;
	};

	subseq_info res{};
	res.valid = true;
	res.reason = subseq_info::reason_t::ok;

	auto bad_res = [&res](subseq_info::reason_t r){res.valid = false; res.reason = r; return res;};
	using enum subseq_info::reason_t;

	if constexpr (constexpr auto tmp = static_call<to_size, std::ptrdiff_t, t_begin,
			static_cast<std::ptrdiff_t>(t_ts_size - 1)>();
		tmp.has_value())
		res.begin = tmp.value();
	else return bad_res(bad_begin_type);

	if constexpr (constexpr auto tmp = static_call<to_size, std::ptrdiff_t , t_end,
			static_cast<std::ptrdiff_t>(t_ts_size)>();
		tmp.has_value())
		res.end = tmp.value();
	else return bad_res(bad_end_type);

	if constexpr (constexpr auto tmp = static_call<to_size, std::ptrdiff_t , t_step, 1>();
		tmp.has_value())
		res.step = tmp.value();
	else return bad_res(bad_step_type);

	if (res.begin > res.end)
		if (res.step > 0)
			res.step = -res.step;

	if (not in_range<0, t_ts_size - 1>(res.begin))
		return bad_res(begin_bigger_than_size);
	if (not in_range<-1, static_cast<std::ptrdiff_t>(t_ts_size + 1)>(res.end))
		return bad_res(end_bigger_than_size);

	return res;
}

template <typename ... Us>
struct contains_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr bool pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		using ts = type_sequence<Ts...>;
		static constexpr auto const & ids = ts::ids();

		return (
				ranges::contains(ids, &typeid(Us), [](type_iter const & it){return it.info;}) and ...
    );
	}
};

template <typename ... Us>
struct contains_some_of_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		static constexpr auto uids = std::array{&typeid(Us)...};
		static constexpr auto const & ids = type_sequence<Ts...>::ids();

		for (type_iter const & id : ids)
			if (ranges::contains(uids, id.info))
				return true;

		return false;
	}
};

template <auto ... t_checkers>
struct contains_such_as_any_of_fnt : adaptor_pipe_closure
{
};

template <typename ... Us>
struct contains_subseq_fnt : adaptor_pipe_closure
{
	constexpr contains_subseq_fnt() = default;
	constexpr contains_subseq_fnt(type_sequence<Us...>) {}

	template <typename ... Ts>
	constexpr bool pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		return ranges::contains_subrange(
      type_sequence<Ts...>::ids(),
      type_sequence<Us...>::ids(),
      {}, mimpl::extract_info,
      mimpl::extract_info
    );
	}
};


template <auto t_start, auto t_end, auto t_step>
struct subseq_fnt :
		adaptor_pipe_closure
{
	template <
	  typename ... Ts,
	  auto info = check_and_get_subseq_info<t_start, t_end, t_step, sizeof...(Ts)>()
	>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
		requires (with_reason<info.valid, static_cast<std::size_t>(info.reason)>)
	{
			return []<std::size_t ... t_is>(std::index_sequence<t_is...>){
				constexpr std::array<std::size_t, sizeof...(t_is)> tmp {t_is...};
				return type_sequence<Ts...>::template reproduce<tmp, sizeof...(t_is)>();
			}(index_sequence_range<info.begin, info.end, info.step>{});
	}
};

template <auto t_start, auto t_end, auto t_step>
struct subseq_from_end_fnt :
  adaptor_pipe_closure
{

  template <std::size_t t_n>
  static_size true_end = []{
    if constexpr (is_npos_v<t_end>)
      return t_n - 1;
    else return t_end;
  }();

  template <typename ... Ts,
    auto info = check_and_get_subseq_info<
      sizeof...(Ts) - t_start - true_end<sizeof...(Ts)>,
      sizeof...(Ts) - t_start, t_step, sizeof...(Ts)
      >()
  > requires (sizeof...(Ts) > 0)
  constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
  requires (with_reason<info.valid, static_cast<std::size_t>(info.reason)>)
  {
    return []<std::size_t ... t_is>(std::index_sequence<t_is...>){
      constexpr std::array<std::size_t, sizeof...(t_is)> tmp {t_is...};
      return type_sequence<Ts...>::template reproduce<tmp, sizeof...(t_is)>();
    }(index_sequence_range<info.begin, info.end, info.step>{});
  }

  constexpr auto pspp_static_call_const_operator(type_sequence<>) noexcept
  { return type_sequence<>{}; }


};


template <auto t_func, typename ... ExtraTs>
struct bind_to_lambda_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{ return [](auto ... args){ return static_call<t_func, Ts..., ExtraTs...>(args...); }; }
};

template <typename ... Us>
struct append_types_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept -> type_sequence<Ts..., Us...>
	{ return {}; }
};

template <typename ... Us, typename ... ExtraTs>
struct append_types_fnt<type_sequence<Us...>, ExtraTs...> : append_types_fnt<Us..., ExtraTs...> 
{};

template <typename ... Us>
struct append_types_to_front_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept -> type_sequence<Us..., Ts...>
	{ return {}; }
};

template <typename ... Us, typename ... ExtraTs>
struct append_types_to_front_fnt<type_sequence<Us...>, ExtraTs...> : append_types_to_front_fnt<Us..., ExtraTs...>
{};

} // namespace mimpl

template <auto t_begin, auto t_end = npos, auto t_step = 1>
inline constexpr mimpl::subseq_fnt<t_begin, t_end, t_step>                          subseq{};

template <auto t_begin, auto t_end = npos, auto t_step = 1>
inline constexpr mimpl::subseq_from_end_fnt<t_begin, t_end, t_step>                 subseq_from_end{};

template <auto t_func, typename ... ExtraTs>
inline constexpr mimpl::bind_to_lambda_fnt<t_func, ExtraTs...>                      bind_to_lambda {};

template <typename ... Us>
inline constexpr mimpl::append_types_fnt<Us...>                                     append_types {};

template <auto ... t_us>
inline constexpr mimpl::append_types_fnt<extract_type<t_us>...>                     vappend_types {};

template <typename ... Us>
inline constexpr mimpl::append_types_to_front_fnt<Us...>                            append_types_to_front {};



namespace mimpl
{

struct tail_fnt : adaptor_pipe_closure
{
	template <typename T1, typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<T1, Ts...> ts) noexcept
	{ return subseq_fnt<1, npos, 1>{}(ts);}

	template <typename T>
	constexpr auto pspp_static_call_const_operator(type_sequence<T> ts) noexcept
	{ return type_sequence<>{}; }

	constexpr auto pspp_static_call_const_operator(type_sequence<> ts) noexcept
	{ return ts; }
};

template <std::integral auto t_n, bool t_from_begin>
struct pop_from_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...> ts) noexcept
	{
		if constexpr (t_from_begin) return ts | subseq_fnt<t_n, npos, 1>{};
		else return ts | subseq_fnt<0, sizeof...(Ts) - t_n, -1>{};
	}
};

template <typename Filter>
concept c_filter_holder = requires
{
	Filter::value;
	requires c_same_as<decltype(Filter::value), bool>;
};

template <auto t_filter_func>
struct filter_fnt : adaptor_pipe_closure
{
		template <typename ... Ts>
		constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
		{
			using ts = type_sequence<Ts...>;
			constexpr auto get_indexes = []{
				std::size_t i {0};
				std::array mask {std::pair{i++, static_call<t_filter_func, Ts>()}...};
				std::array<std::size_t, sizeof...(Ts)> res {};

				std::size_t n {0};
				for (auto const & [ind, flag] : mask)
					if (flag) res[n++] = ind;

				return std::pair{res, n};
			};

			constexpr auto res = get_indexes();
			return ts::template reproduce<res.first, res.second>();
		}

		constexpr auto pspp_static_call_const_operator(type_sequence<>) noexcept
		{ return emptytseq; }
};

template <template <typename> class Filter>
		requires (c_filter_holder<Filter<void>>)
struct tfilter_fnt : adaptor_pipe_closure
{
	constexpr auto pspp_static_call_const_operator(type_sequence<>)	noexcept
	{ return emptytseq; }

	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		using ts = type_sequence<Ts...>;
		constexpr auto get_indexes = []{
			std::size_t i = 0;
			std::array mask = {std::pair{i++, Filter<Ts>::value}...};
			std::array<std::size_t, sizeof...(Ts)> res {};

			std::size_t n {0};
			for (auto const & [ind, flag] : mask)
				if (flag) res[n++] = ind;

			return std::pair{res, n};
		};

		constexpr auto res = get_indexes();
		return ts::template reproduce<res.first, res.second>();
	}
};

template <c_type_sequence auto t_suceeded, c_type_sequence auto t_failed>
struct extract_result
{
	decltype(t_suceeded) succeeded = t_suceeded;
	decltype(t_failed) failed = t_failed;
};

extract_result(c_type_sequence auto succeeded, c_type_sequence auto failed) ->
	extract_result<decltype(succeeded){}, decltype(failed){}>;

template <auto t_filter_func>
struct extract_like_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		using ts = type_sequence<Ts...>;
		constexpr auto const & ids = ts::ids();
		constexpr auto get_indexes = []{
			std::size_t i = 0;
			std::array mask = {std::pair{i++, static_call<t_filter_func, Ts>()}...};
			std::array<std::size_t, sizeof...(Ts)> res {};
			std::array<std::size_t, sizeof...(Ts)> anti_res {};

			std::size_t n {0};
			std::size_t an {0};
			for (auto const [ind, flag] : mask)
				if (flag) res[n++] = ind;
				else anti_res[an++] = ind;

			return std::pair{
					std::pair{res, n},
					std::pair{anti_res, an}
			};
		};

		constexpr auto res = get_indexes();
		return extract_result{
			ts::template reproduce<res.first.first, res.first.second>(),
			ts::template reproduce<res.second.first, res.second.second>()
		};
	}

	constexpr auto pspp_static_call_const_operator(type_sequence<>) noexcept
	{ return emptytseq; }
};

template <template <typename> class Filter>
requires (c_filter_holder<Filter<void>>)
struct textract_like_fnt : adaptor_pipe_closure
{
	constexpr auto pspp_static_call_const_operator(type_sequence<>)	noexcept
	{ return extract_result<emptytseq, emptytseq>{}; }

	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		using ts = type_sequence<Ts...>;
		constexpr auto const & ids = ts::ids();
		constexpr auto get_indexes = []{
			std::size_t i = 0;
			std::array mask = {std::pair{i++, Filter<Ts>::value}...};
			std::array<std::size_t, sizeof...(Ts)> res {};
			std::array<std::size_t, sizeof...(Ts)> anti_res {};

			std::size_t n {0};
			std::size_t an {0};
			for (auto const [ind, flag] : mask)
				if (flag) res[n++] = ind;
				else anti_res[an++] = ind;

			return std::pair{
					std::pair{res, n},
					std::pair{anti_res, an}
			};
		};

		constexpr auto res = get_indexes();
		return extract_result{
				ts::template reproduce<res.first.first, res.first.second>(),
				ts::template reproduce<res.second.first, res.second.second>()
		};
	}
};

struct is_empty_fnt : adaptor_pipe_closure
{
	template <typename T1, typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<T1, Ts...>) noexcept { return false; }
	constexpr auto pspp_static_call_const_operator(type_sequence<>) noexcept {return true; }
};

template <auto t_filter>
struct remove_like_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...> ts) noexcept
	{ return ts | filter_fnt<[]<typename T>{return not static_call<t_filter, T>(); }>{}; }
};

template <template <typename> class ... FilterT>
struct tremove_like_fnt : adaptor_pipe_closure
{
	template <typename T>
	struct adaptor
	{ static_flag value = not (... or FilterT<T>::value); };

	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...> ts) noexcept
	{ return ts | tfilter_fnt<adaptor>{}; }
};

template <auto t_ts, bool t_invert = false, auto t_rest = nulltv>
struct conditional_holder
{};

template <typename T>
struct bad_else_argument_holder
{ using type = T; };

template <auto t_cond, bool t_invert, auto t_arg>
struct conditional_info_wrapper
{ 
	using condition_type = decltype(t_cond);
	static_val arg = t_arg;

	constexpr conditional_info_wrapper() = default;

	constexpr static bool can_go_() noexcept
	{ return static_cast<bool>(t_cond); }

	constexpr static bool can_go() noexcept
	{ return static_cast<bool>(can_go_() - t_invert); }

	constexpr static auto invert() noexcept
	{ return conditional_info_wrapper<t_cond, not t_invert, t_arg>{}; }
};

template <auto t_cond, bool t_invert, bad_else_argument_holder t_arg>
struct conditional_info_wrapper<t_cond, t_invert, t_arg> : 
	conditional_info_wrapper<t_cond, t_invert, emptytseq>
{
	using value_type = typename decltype(t_arg)::type;
	value_type value;
};


template <auto t_val, template <auto> class ValHolder, typename ... Ts>
conditional_info_wrapper(ValHolder<t_val>, type_sequence<Ts...>) ->
	conditional_info_wrapper<t_val, false, type_sequence<Ts...>{}>;
 
template <auto t_val, template <auto> class ValHolder, bool t_invert, typename ... Ts>
conditional_info_wrapper(ValHolder<t_val>, std::bool_constant<t_invert>, type_sequence<Ts...>) ->
	conditional_info_wrapper<t_val, t_invert, type_sequence<Ts...>{}>;
 

struct _if_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{ return conditional_holder<type_sequence<Ts...>{}>{}; }
};

struct _if_not_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{ return conditional_holder<type_sequence<Ts...>{}, true>{}; }
};

struct _else_fnt : adaptor_pipe_closure
{
	template <auto t_val, bool t_invert, c_type_sequence auto t_ts>
	constexpr auto pspp_static_call_const_operator(conditional_info_wrapper<t_val, t_invert, t_ts> cond) noexcept
	{ return cond.invert(); }

	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{ return conditional_info_wrapper<true, true, type_sequence<Ts...>{}>{}; }

	constexpr auto pspp_static_call_const_operator(auto true_res) noexcept
	{
		return conditional_info_wrapper<true, true, bad_else_argument_holder<decltype(true_res)>{}>{.value = true_res};
	}
};

struct _else_if_fnt 
{};

template <auto t_prev_cond>
struct _and_holder
{};

template <auto t_prev_cond>
struct _and_not_holder
{};

template <auto t_prev_cond>
struct _or_holder
{};

template <auto t_prev_cond>
struct _or_not_holder
{};



template <auto t_val, c_type_sequence auto t_ts>
struct conditional_end
{
	constexpr operator decltype(t_val)() const noexcept
	{ return t_val; }

	constexpr operator decltype(t_ts)() const noexcept
	{ return t_ts; }
};

struct _end_if_fnt : adaptor_pipe_closure
{
	template <auto t_val, bool t_invert, auto t_ts>
		requires (not pspp::c_complex_target<decltype(t_ts), bad_else_argument_holder>)
	constexpr auto pspp_static_call_const_operator(conditional_info_wrapper<t_val, t_invert, t_ts>)
	{ return t_ts; }
	template <auto t_val, bool t_invert, bad_else_argument_holder t_res>
	constexpr auto pspp_static_call_const_operator(conditional_info_wrapper<t_val, t_invert, t_res> cond)
	{ return cond.value; }
	constexpr auto pspp_static_call_const_operator(auto && val) noexcept
	{ return val; }
};

template <typename CondT, template <auto> class HolderT>
struct conditional_op : adaptor_pipe_closure
{
	template <auto t_cond, bool t_invert, auto t_ts>
	constexpr auto pspp_static_call_const_operator(conditional_info_wrapper<t_cond, t_invert, t_ts> prev_cond) noexcept
	{ return HolderT<decltype(prev_cond){}>{}; }
};

struct _and_fnt : conditional_op<_and_fnt, _and_holder> {};
struct _and_not_fnt : conditional_op<_and_not_fnt, _and_holder> {};
struct _or_fnt : conditional_op<_or_fnt, _or_holder> {};
struct _or_not_fnt : conditional_op<_or_not_fnt, _or_not_holder> {};

template <auto t_val>
struct return_value_fnt : adaptor_pipe_closure
{
	inline consteval auto pspp_static_call_const_operator(auto ... vals) noexcept
		requires (... or (not c_complex_vtarget<decltype(vals), conditional_info_wrapper>))
	{ return t_val; }
};

template <auto t_fun>
struct contains_such_as_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr bool pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		using ts = type_sequence<Ts...>;
		return filter_fnt<t_fun>{}(ts{}).size != 0;
	}
};

template <template <typename> class Filter>
struct tcontains_such_as_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr bool pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		using ts = type_sequence<Ts...>;
		return tfilter_fnt<Filter>{}(ts{}).size != 0;
	}
};




template <template <typename ...> class ToPass, typename ... Extra>
struct pass_to_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		return type_t<ToPass<Extra..., Ts...>>{};
	}
};

template <auto t_convertor, template <auto ...> class ToPass, auto ... t_extra>
struct convert_to_vals_and_pass_to_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{ return type_t<ToPass<t_extra..., static_call<t_convertor, Ts>()...>>{}; }
};

template <typename ... Us, typename ... Extra>
struct contains_fnt<type_sequence<Us...>, Extra...> :
		contains_fnt<Us..., Extra...>
{};

template <auto t_func, type_sequence t_extra_ts, typename ... ArgsT>
struct pass_and_execute_with_args_fnt : adaptor_pipe_closure
{
	simple::tmpstorage<ArgsT && ...> m_args;

  template <typename ... ArgUs>
	constexpr pass_and_execute_with_args_fnt(ArgUs && ... args):
    adaptor_pipe_closure{},
		m_args{std::forward<ArgUs>(args)...}
	{}

	template <typename ... Ts>
	constexpr auto operator()(type_sequence<Ts...>) const noexcept
	{ return std::apply(
			t_extra_ts | append_types_to_front<Ts...> | bind_to_lambda<t_func>,
			m_args
		); 
	}
};

template <auto t_func, typename ... ExtraTs>
struct pass_and_execute_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{ return static_call<t_func, Ts..., ExtraTs...>(); }

  template <typename ... ArgTs>
	constexpr auto pspp_static_call_const_operator(ArgTs && ... args) noexcept
    requires (
      not (
        sizeof...(args) == 1 and
        (... or c_type_sequence<std::remove_cvref_t<ArgTs>>)
      )
    )
	{
		return pass_and_execute_with_args_fnt<
		  t_func, type_sequence<ExtraTs...>{}, ArgTs && ...
    >{std::forward<ArgTs>(args)...};
	}
};

template <auto t_func, typename ... ExtraTs>
struct pass_and_execute_with_indexseq_fnt :
  adaptor_pipe_closure
{
  template <typename ... Ts>
  constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>)
    noexcept(noexcept(
      static_call<t_func, ExtraTs..., Ts...>(
        std::make_index_sequence<sizeof...(Ts) + sizeof...(ExtraTs)>{}
      )
    )) -> decltype(auto)
  {
    return static_call<t_func, ExtraTs..., Ts...>(
      std::make_index_sequence<sizeof...(Ts) + sizeof...(ExtraTs)>{}
    );
  }
};

template <auto t_func>
struct pass_as_indexseq_fnt : adaptor_pipe_closure
{
  template <typename ... Ts, template <typename ...> class HolderT>
  constexpr auto pspp_static_call_const_operator(HolderT<Ts...>)
    noexcept(noexcept(static_call<t_func>(std::make_index_sequence<sizeof...(Ts)>{})))
    -> decltype(auto)
  {
    return static_call<t_func>(std::make_index_sequence<sizeof...(Ts)>{});
  }

  template <auto ... t_vals, template <auto...> class HolderT>
  constexpr auto pspp_static_call_const_operator(HolderT<t_vals...>)
  noexcept(noexcept(static_call<t_func>(std::make_index_sequence<sizeof...(t_vals)>{})))
  -> decltype(auto)
  {
    return static_call<t_func>(std::make_index_sequence<sizeof...(t_vals)>{});
  }

};


struct reverse_fnt : adaptor_pipe_closure
{
  template <typename ... Ts>
  constexpr auto pspp_static_call_const_operator(type_sequence<Ts...> ts) noexcept
  {
    if constexpr (sizeof...(Ts) == 0) return ts;
    return ts | subseq<npos, -1>;
  }

  template <auto ... t_vals, template <auto ...> class HolderT>
  constexpr auto pspp_static_call_const_operator(HolderT<t_vals...> arg) noexcept
  {
    if constexpr (sizeof...(t_vals) == 0) return arg;
    else
    return type_sequence<value_holder<t_vals>...>{}
            | subseq<npos, -1>
            | convert_to_vals_and_pass_to_fnt<[]<typename T>{return T::value; }, HolderT>{};
  }

  template <typename T, template <typename, auto...> class HolderT>
  struct pass_helper
  {
    template <auto ... t_vals>
    using type = HolderT<T, t_vals...>;
  };

  template <typename T, auto ... t_vals, template <typename, auto ...> class HolderT>
  constexpr auto pspp_static_call_const_operator(HolderT<T, t_vals...> arg) noexcept
  {
    if constexpr (sizeof...(t_vals) == 0) return arg;
    else
    return type_sequence<value_holder<t_vals>...>{}
    | subseq<npos, -1>
    | convert_to_vals_and_pass_to_fnt<[]<typename U>{return U::value; }, pass_helper<T, HolderT>::template type>{};
  }
};

struct to_be_passed_by_pipe {};

template <auto t_ts, typename ... Extra>
struct to_be_passed_holder
{
  static_val ts = t_ts;
};

template <typename ... Extra>
struct pass_and_execute_fnt<to_be_passed_by_pipe{}, Extra...> :
    adaptor_pipe_closure
{
  template <typename ... Ts>
  constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
  {
    return to_be_passed_holder<type_sequence<Ts...>{}, Extra...>{};
  }
};


template <auto t_convertor, auto t_func, auto ... t_extra_vals>
struct convert_to_vals_and_execute_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		return static_call<t_func, t_extra_vals..., static_call<t_convertor, Ts>()...>();
	}
};


template <typename Target>
struct get_hits_result_node
{
	std::basic_string_view<std::size_t> indexes;
	[[nodiscard]] constexpr bool was_found() const noexcept { return not indexes.empty(); }

	[[nodiscard]] constexpr std::size_t first() const noexcept
	{ return was_found() ? indexes.front() : npos; }

	[[nodiscard]] constexpr std::size_t last() const noexcept
	{ return was_found() ? indexes.back() : npos; }

	[[nodiscard]] constexpr std::size_t count() const noexcept
	{ return indexes.size(); }

	[[nodiscard]] constexpr auto operator()(type_t<Target>) const noexcept
	{ return *this; }
};

template <typename FirstT, typename ... Us>
struct get_hits_result : get_hits_result_node<FirstT>,
		get_hits_result_node<Us>...
{
	using head_base = get_hits_result_node<FirstT>;
	using head_base::operator();
	using get_hits_result_node<Us>::operator()...;

	using head_base::was_found;
	using head_base::first;
	using head_base::last;
	using head_base::count;

	template <typename T>
	constexpr auto get() const noexcept
	{ return this->operator()(type_t<T>{}); }

	constexpr explicit(true) get_hits_result(get_hits_result_node<FirstT> f, get_hits_result_node<Us> ... rest):
			get_hits_result_node<FirstT>{f},
			get_hits_result_node<Us>{rest}...
	{}
};

template <typename First, typename ... Us>
struct get_hits_of_fnt : adaptor_pipe_closure
{
	using result_type = get_hits_result<First, Us...>;

	template <typename ... Ts>
	constexpr result_type pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		using ts = type_sequence<Ts...>;
		static constexpr auto const & ids = ts::ids();

		constexpr auto get_all_hits_of = []<typename T>{
			std::array<std::size_t, ids.size()> res {};
			std::size_t n {0};

			for (type_iter const & it : ids)
				if (it.info == &typeid(T))
					res[n++] = it.index;

			return std::pair{res, n};
		};

		constexpr auto convert_to_node = [get_all_hits_of]<typename T>
		{
			constexpr auto res = get_all_hits_of.template operator()<T>();
			auto _ = pspp::cwarning<res>{};
			
			//static_assert(res.second != 1);
			using t = value_holder<res.first>;
			return get_hits_result_node<T>{std::basic_string_view<std::size_t>{t::value.begin(), res.second}};
		};

		return
			result_type {
				convert_to_node.template operator()<First>(),
				convert_to_node.template operator()<Us>()...
			};
	}
};

template <typename T, bool t_from_back>
struct get_index_of : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr std::size_t pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		static constexpr auto const & ids = type_sequence<Ts...>::ids();

		for (auto id : []{
				if constexpr (t_from_back) return []{
          static constexpr std::size_t csize = sizeof...(Ts) - 1;
          std::array<type_iter, sizeof...(Ts)> rids {};

          for (std::size_t i = ids.size(); i != 0; --i)
            rids[csize - i + 1] = ids[i - 1];

          return rids;
        }();
				else return ids;
				}())
			if (id.info == &typeid(T))
				return id.index;

		return npos;
	}
};

template <bool t_from_back, auto ... t_checkers>
struct get_index_of_like_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		static constexpr auto flags = std::array{
			[]<typename T>{ return (... or static_call<t_checkers, T>()); }.template operator()<Ts>()...
		};

		static constexpr auto count = sizeof...(Ts);
		static constexpr auto shift = [](auto & i_){
			if constexpr (t_from_back)
				--i_;
			else ++i_;
		};

		static constexpr auto check = [](auto i_){
			if constexpr (t_from_back)
				return i_ >= 0;
			else return i_ < count;
		};

		static constexpr auto ind = []{
			int i = conditional_v<t_from_back, count - 1, 0>;

			for (; check(i); shift(i))
				if (flags[i])
					return static_cast<std::size_t>(i);

			return npos;
		}();

		return ind;
	}
};

template <bool t_from_back, template <typename> class ... CheckerTs>
struct tget_index_of_like_fnt : adaptor_pipe_closure
{
  template <typename ... Ts>
  constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
  {
    static constexpr auto flags = std::array{
      []<typename T>{ return (... or CheckerTs<Ts>::value); }.template operator()<Ts>()...
    };

    static constexpr auto count = sizeof...(Ts);
    static constexpr auto shift = [](auto & i_){
      if constexpr (t_from_back)
        --i_;
      else ++i_;
    };

    static constexpr auto check = [](auto i_){
      if constexpr (t_from_back)
        return i_ >= 0;
      else return i_ < count;
    };

    static constexpr auto ind = []{
      int i = conditional_v<t_from_back, count - 1, 0>;

      for (; check(i); shift(i))
        if (flags[i])
          return static_cast<std::size_t>(i);

      return npos;
    }();

    return ind;
  }
};


template <bool t_from_back, auto ... t_checkers>
struct get_info_of_like_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		static constexpr auto ind = get_index_of_like_fnt<t_from_back, t_checkers...>{}(type_sequence<Ts...>{});

		if constexpr (ind == npos) return nulltv;
#ifdef __cpp_pack_indexing
      else return std::pair{type<Ts...[ind]>, ind};
#else
		  else return std::pair{at_fnt<ind>{}(type_sequence<Ts...>{}), ind};
#endif
	}
};

struct index_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		return []<std::size_t ... t_is>(std::index_sequence<t_is...>)
		{return type_sequence<indexed_type<Ts, t_is>...>{}; }(std::make_index_sequence<sizeof...(Ts)>{});
	}
};

template <auto t_ptojector>
struct projector
{
	template <typename T>
	using projected = extract_type<static_call<t_ptojector, T>()>;
};

template <>
struct projector<nulltv>
{ template <typename T> using projected = T; };

/*
struct get_size_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{ return sizeof...(Ts); }
};
 */

template <auto t_func, auto t_projector>
struct transform_fnt : adaptor_pipe_closure
{
	static constexpr auto caller = []<typename T>{
		using result_t = decltype(static_call<t_func, extract_type<static_call<t_projector, T>()>>());
		if constexpr (c_same_as<result_t, void>)
			return emptytseq;
		else if constexpr (c_type_sequence<result_t> or c_type_holder<result_t>) return result_t{};
		else return type_t<result_t>{};
	};

	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{ return emptytseq + (... + caller.template operator()<Ts>());
	}
};

template <auto t_func>
struct transform_fnt<t_func, nulltv> : adaptor_pipe_closure
{
	static constexpr auto caller = []<typename T>{
		using result_t = decltype(static_call<t_func, T>());
		if constexpr (c_same_as<result_t, void>)
			return emptytseq;
		else if constexpr (c_type_sequence<result_t> or c_type_holder<result_t>) return result_t{};
		else return type_t<result_t>{};
	};

	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
    return (emptytseq + ... + caller.template operator()<Ts>());
	}
};

template <template <typename> class TransT>
struct ttransform_fnt : adaptor_pipe_closure
{
	template <typename T>
	struct res
	{ using type = empty_type_sequence; };

	template <typename T>
		requires (requires { typename TransT<T>::type; })
	struct res<T>
	{ using type = typename TransT<T>::type; };

	template <typename T>
	using res_t = std::conditional_t<c_type_sequence<T> or c_type<T>, T, type_t<T>>;

	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{ return (emptytseq + ... + res_t<typename res<Ts>::type>{}); }
};

template <bool t_is_strict, typename ... Us>
struct count_of_fnt : adaptor_pipe_closure
{
	template <typename T>
	static_size is_equal = []{
    if constexpr (t_is_strict)
      return static_cast<std::size_t>((... or std::same_as<T, Us>));
    else return static_cast<std::size_t>((... or c_same_as<T, Us>));
  }();

	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{ return (0 + ... + is_equal<Ts>); }
};

template <typename Prev, typename ... Ts>
struct replacer
{
	template <typename T>
	struct impl : std::enable_if<std::same_as<T, Prev>, type_sequence<Ts...>> {};
};

template <typename T>
struct absorber
{ using type = T; };

struct absorb_recursively_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		static constexpr auto res = type_sequence<Ts...>{} | ttransform_fnt<absorber>{};
		if constexpr (res | tcontains_such_as_fnt<is_type_sequence>{})
			return operator()(res);
		else return res;
	}
};

template <auto t_proj, auto t_comparator>
struct sort_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{	
		static constexpr std::array ids = []{
			std::size_t i = 0;
			auto res = std::array{std::pair{i++, static_call<t_proj, Ts>()}...};

			static constexpr auto second_ptr = decltype(meta_detail::get_first_<false>(
					std::declval<typename decltype(res)::value_type>()
					)){};

			ranges::sort(res, t_comparator, [](auto const & val) { return val.second; });
			std::array <std::size_t, sizeof...(Ts)> ids_only {};
			ranges::transform_to_self(res, [](auto const val) { return val.first; });
			return ids_only;
		}();

		return type_sequence<Ts...>::template reproduce<ids, sizeof...(Ts)>();
	}
};

template <auto t_proj, auto t_comparator>
struct most_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		constexpr auto most_id = []
			{
				std::size_t i = 0;
				std::array tmp {std::pair{i++, static_call<t_proj, Ts>()}...};
				auto const * most = tmp.begin();

				for (auto const & el : tmp)
					if (t_comparator(el.second, most->second))
						most = &el;

				return most->first;
			}();

		return type_sequence<Ts...>{}[val<most_id>];
	}
};

template <bool t_first, typename DefaultT, auto t_func>
struct get_st_like_or_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		constexpr auto tmp = type_sequence<Ts...>{} | filter_fnt<t_func>{};
		if constexpr (tmp.size == 0)
			return type_t<DefaultT>{};
		else // TODO: replace to pack index syntax (C++26)
			if constexpr (t_first)
				return tmp | at_fnt<0> {};
			else return tmp | at_from_back_fnt<0> {};
	}
};

//TODO: replace/add concept filter version (C++26)
template <bool t_first, typename DefaultT, template <typename> class Filter>
struct tget_st_like_or_fnt : adaptor_pipe_closure
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		constexpr auto tmp = type_sequence<Ts...>{} | tfilter_fnt<Filter>{};
		if constexpr (tmp.size == 0)
			return type_t<DefaultT>{};
		else
		if constexpr (t_first) // TODO: replace to pack index syntaxt (C++26)
			return tmp | at_fnt<0> {};
		else return tmp | at_from_back_fnt<0> {};
	}
};


struct unwrap_fnt : adaptor_pipe_closure
{
	template <typename T, template <typename> class HolderT>
	constexpr auto pspp_static_call_const_operator(HolderT<T>) noexcept
	{ return T{}; }

	template <auto t_val, template <auto> class HolderT>
	constexpr auto pspp_static_call_const_operator(HolderT<t_val>) noexcept
	{ return t_val; }
};

template <bool t_first, typename DefaultT, template <typename> class FilterT>
struct tget_st_like_or 
{
	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{
		constexpr auto tmp = type_sequence<Ts...>{} | tfilter_fnt<FilterT>{};
		if constexpr (tmp.size == 0)
			return type_t<DefaultT>{};
		else
			if constexpr (t_first)
				return tmp | at_fnt<0> {};
			else return tmp | at_from_back_fnt<0>{};
	}
};

template <auto t_fun>
struct condition_fnt : adaptor_pipe_closure
{
  template <typename ValT>
	constexpr auto pspp_static_call_const_operator(ValT && val) noexcept
		requires (not (c_complex_vtarget<std::remove_cvref_t<ValT>, conditional_info_wrapper> or
									 c_complex_vtarget<std::remove_cvref_t<ValT>, conditional_holder>))
	{ return t_fun(val); }

	template <typename ... Ts>
	constexpr auto pspp_static_call_const_operator(type_sequence<Ts...>) noexcept
	{ return static_call<t_fun, type_sequence<Ts...>{}>(); }

	template <typename T, template <typename> class HolderT>
	constexpr auto pspp_static_call_const_operator(HolderT<T>) noexcept
	{
		if constexpr (requires { static_call<t_fun, T>(); })
			return static_call<t_fun, T>();
		else return static_call<t_fun, HolderT<T>{}>();
	}
};

template <auto t_val>
struct is_equal_fnt : adaptor_pipe_closure
{
	constexpr auto pspp_static_call_const_operator(auto val) noexcept
	{ return t_val == val; }
};

struct invert_fnt : adaptor_pipe_closure
{
	constexpr bool pspp_static_call_const_operator(bool val) noexcept
	{ return not val; }
};

struct get_holded_value_fnt : adaptor_pipe_closure
{
  template <typename Val>
  constexpr auto pspp_static_call_const_operator(Val const & val) noexcept
  { return Val::value; }
};

template <typename T>
struct is_t_same_as_fnt : adaptor_pipe_closure
{
  template <typename U, template <typename> class HolderT>
  constexpr bool pspp_static_call_const_operator(HolderT<U>) noexcept
  { return c_same_as<T, std::remove_cvref_t<U>>; }
};

template <auto ... t_checkers>
struct is_t_satisfying_fnt : adaptor_pipe_closure
{
  template <typename U, template <typename> class HolderT>
  constexpr bool pspp_static_call_const_operator(HolderT<U>) noexcept
  {
    return (false or ... or static_call<t_checkers, U>());
  }
};

template <auto t_cond_func, typename AdaptorT>
struct repeat_while_impl : adaptor_pipe_closure
{
  AdaptorT adaptor ;

  template <typename ArgT>
  constexpr auto operator()(ArgT) const noexcept
  {
    auto res = adaptor(ArgT{});

    if constexpr (static_call<t_cond_func,
        std::remove_cvref_t<ArgT>,
        std::remove_cvref_t<decltype(res)>
      >())
      return operator()(res);
    else return res;
  }
};

template <auto t_cond_func>
struct repeat_while_fnt : adaptor_pipe_closure
{};

template <typename AdaptorT, auto t_cond_func>
constexpr auto operator&(AdaptorT adaptor, repeat_while_fnt<t_cond_func>) noexcept ->
  repeat_while_impl<t_cond_func, AdaptorT>
{ return {std::forward<AdaptorT>(adaptor)}; }


inline constexpr struct while_not_same_t
{
  template <typename Before, typename After>
  constexpr auto pspp_static_call_const_operator() noexcept
  { return not std::same_as<Before, After>; }
} while_not_same {};


} // namespace mimpl
	// --------------------------------------------------------------------
	//


template <std::integral auto t_n, 
					template <typename> class HolderT = type_t>
inline constexpr mimpl::at_fnt<t_n, false, HolderT>                                   at {};
template <std::integral auto t_n, 
					template <typename> class HolderT = type_t>
inline constexpr mimpl::at_from_back_fnt<t_n, false, HolderT>                         at_from_back {};
template <std::integral auto t_n>
inline constexpr mimpl::at_fnt<t_n, true>                                             atv {};
template <std::integral auto t_n>
inline constexpr mimpl::at_from_back_fnt<t_n, true>                                   at_from_backv {};

template <auto t_filter_func>
inline constexpr mimpl::filter_fnt<t_filter_func>                                     filter {};
template <template <typename> class FilterT>
inline constexpr mimpl::tfilter_fnt<FilterT>                                          tfilter {};
template <class FilterHolderT>
	requires (requires {typename FilterHolderT::template is_target<void>;})
inline constexpr mimpl::tfilter_fnt<FilterHolderT::template is_target>	              tfilter_with_holder {};
template <class FilterHolderT>
requires (requires {typename FilterHolderT::template is_target<void>;})
inline constexpr mimpl::tfilter_fnt<FilterHolderT::template is_target>	              tfilterh {};
template <auto t_filter_func>
inline constexpr mimpl::extract_like_fnt<t_filter_func>                               extract_like {};
template <template <typename> class FilterT>
inline constexpr mimpl::textract_like_fnt<FilterT>                                    textract_like {};
inline constexpr mimpl::unique_fnt                                                    make_unique {};
template <auto t_mask, bool t_revert = false>
inline constexpr mimpl::reproduce_from_mask_fnt<t_mask, t_revert>                     reproduce_from_mask {};
template <auto t_func>
inline constexpr mimpl::custom_unique_fnt<t_func>                                     make_custom_unique {};
inline constexpr mimpl::is_unique_fnt                                                 is_unique {};
inline constexpr mimpl::tail_fnt                                                      tail {};
inline constexpr mimpl::at_fnt<0>                                                     head {};
inline constexpr mimpl::at_fnt<0, true>                                               vhead {};
inline constexpr mimpl::at_from_back_fnt<0>                                           last {};
inline constexpr mimpl::at_from_back_fnt<0, true>                                     vlast {};
template <typename ... Us>
inline constexpr mimpl::contains_fnt<Us...>                                           contains {};
template <typename ... Us>
inline constexpr mimpl::contains_some_of_fnt<Us...>                                   contains_some_of {};
template <auto t_fun>
inline constexpr mimpl::contains_such_as_fnt<t_fun>                                   contains_such_as {};
template <template <typename> class Filter>
inline constexpr mimpl::tcontains_such_as_fnt<Filter>                                 tcontains_such_as {};
template <typename ... Us>
inline constexpr mimpl::contains_subseq_fnt<Us...>                                    contains_subseq {};
inline constexpr mimpl::reverse_fnt                                                   reverse {};
template <template <typename ...> class ToPass, typename ... Extra>
inline constexpr mimpl::pass_to_fnt<ToPass, Extra...>                                 pass_to {};
template <auto t_convertor, template <auto ...> class ToPass, auto ... t_extra>
inline constexpr mimpl::convert_to_vals_and_pass_to_fnt<
	t_convertor, ToPass, t_extra...>                                                    convert_to_vals_and_pass_to {};
template <auto t_convertor, auto t_func, auto ... t_extra>
inline constexpr mimpl::convert_to_vals_and_execute_fnt<
	t_convertor, t_func, t_extra...>                                                    convert_to_vals_and_execute {};

template <template <auto ...> class ToPass, auto ... t_extra>
inline constexpr mimpl::convert_to_vals_and_pass_to_fnt<
  to_value, ToPass, t_extra...>                                                       pass_as_values_to {};
template <auto t_func, typename ... ExtraTs>
inline constexpr mimpl::pass_and_execute_fnt<t_func, ExtraTs...>                      pass_and_execute {};
template <auto t_func, typename ... ExtraTs>
inline constexpr mimpl::pass_and_execute_with_indexseq_fnt<t_func, ExtraTs...>        pass_and_execute_with_indexseq {};
template <auto t_func>
inline constexpr mimpl::pass_as_indexseq_fnt<t_func>                                  pass_as_indexseq {};
inline constexpr mimpl::pass_and_execute_fnt<mimpl::to_be_passed_by_pipe{}>           and_execute {};
template <typename First, typename ... Us>
inline constexpr mimpl::get_hits_of_fnt<First, Us...>                                 get_hits_of {};
template <typename T>
inline constexpr mimpl::get_index_of<T, true>                                         get_last_index_of {};
template <typename T>
inline constexpr mimpl::get_index_of<T, false>                                        get_first_index_of {};
template <auto ... t_checkers>
inline constexpr mimpl::get_index_of_like_fnt<false, t_checkers...>                   get_first_index_of_like {};
template <auto ... t_checkers>
inline constexpr mimpl::get_index_of_like_fnt<true, t_checkers...>                    get_last_index_of_like {};
template <template <typename> class ... CheckerTs>
inline constexpr mimpl::tget_index_of_like_fnt<false, CheckerTs...>                   tget_first_index_of_like {};
template <template <typename> class ... CheckerTs>
inline constexpr mimpl::tget_index_of_like_fnt<true, CheckerTs...>                    tget_last_index_of_like {};
template <auto ... t_checkers>
inline constexpr mimpl::get_info_of_like_fnt<false, t_checkers...>                    get_first_info_of_like {};
template <auto ... t_checkers>
inline constexpr mimpl::get_info_of_like_fnt<true, t_checkers...>                     get_last_info_of_like {};
inline constexpr mimpl::index_fnt                                                     index {};
template <auto t_val>
inline constexpr mimpl::is_equal_fnt<t_val>                                           is_equal {};

template <auto t_func, auto t_projector = nulltv>
inline constexpr mimpl::transform_fnt<t_func, t_projector>                            transform {};
template <template <typename> class TransT>
inline constexpr mimpl::ttransform_fnt<TransT>                                        ttransform {};
template <typename ... Ts>
inline constexpr mimpl::filter_fnt<
	[]<typename T>{ return not (std::same_as<T, Ts> or ...); }>                         remove {};
template <c_type_sequence Ts>
inline constexpr mimpl::filter_fnt<
	[]<typename T>{ return not (Ts{} | contains<T>); }>                                 remove_ts {};

template <typename T, typename ... Ts>
inline constexpr mimpl::ttransform_fnt<mimpl::replacer<T, Ts...>::template impl>      replace_all {};
inline constexpr mimpl::ttransform_fnt<mimpl::absorber>                               absorb {};
inline constexpr mimpl::absorb_recursively_fnt                                        absorb_recursively {};
template <auto t_projector, auto t_comparator>
inline constexpr mimpl::sort_fnt<t_projector, t_comparator>                           sort {};
template <auto t_projector, auto t_comparator>
inline constexpr mimpl::most_fnt<t_projector, t_comparator>                           most {};
template <auto t_func, typename DefaultT>
inline constexpr mimpl::get_st_like_or_fnt<true, DefaultT, t_func>                    get_first_like_or {};
template <auto t_func, typename DefaultT>
inline constexpr mimpl::get_st_like_or_fnt<false, DefaultT, t_func>                   get_last_like_or {};
template <template <typename> class Filter, typename DefaultT>
inline constexpr mimpl::tget_st_like_or_fnt<false, DefaultT, Filter>                  tget_last_like_or {};
template <template <typename> class Filter, typename DefaultT>
inline constexpr mimpl::tget_st_like_or_fnt<true, DefaultT, Filter>                   tget_first_like_or {};
template <typename ... Us>
inline constexpr mimpl::count_of_fnt<true, Us...>                                     count_of {};
template <typename ... Us>
inline constexpr mimpl::count_of_fnt<false, Us...>                                    kind_count_of {};
inline constexpr mimpl::unwrap_fnt                                                    unwrap {};
template <auto t_func>
inline constexpr mimpl::condition_fnt<t_func>                                         condition {};
template <std::integral auto t_n>
inline constexpr mimpl::pop_from_fnt<t_n, true>                                       pop_from_front {};
template <std::integral auto t_n>
inline constexpr mimpl::pop_from_fnt<t_n, false>                                      pop_from_back {};
inline constexpr mimpl::is_empty_fnt                                                  is_empty {};
//inline constexpr mimpl::get_size_fnt                                                  get_size {};
template <auto t_fun>
inline constexpr mimpl::remove_like_fnt<t_fun>                                        remove_like {};
template <template <typename> class ... FilterT>
inline constexpr mimpl::tremove_like_fnt<FilterT...>                                  tremove_like {};
template <class ... FilterHolderT>
requires (... and requires {typename FilterHolderT::template is_target<void>;})
inline constexpr mimpl::tremove_like_fnt<FilterHolderT::template is_target...>        tremove_like_with_holder {};
inline constexpr mimpl::ttransform_fnt<std::remove_cvref>                             remove_cvref {};
inline constexpr mimpl::ttransform_fnt<std::remove_all_extents>                       remove_all_ext {};
inline constexpr mimpl::ttransform_fnt<std::remove_reference>                         remove_ref {};

template <auto t_cond_func>
inline constexpr mimpl::repeat_while_fnt<t_cond_func>                                 repeat_while {};
inline constexpr mimpl::repeat_while_fnt<mimpl::while_not_same>                       repeat_while_not_same {};


inline constexpr mimpl::_if_fnt                                                       _if {};
inline constexpr mimpl::_end_if_fnt                                                   _end_if {};
inline constexpr mimpl::_else_fnt                                                     _else {};
inline constexpr mimpl::_else_if_fnt                                                  _else_if {};
inline constexpr mimpl::_and_fnt                                                      _and {};
inline constexpr mimpl::_or_fnt                                                       _or {};
inline constexpr mimpl::_and_not_fnt                                                  _and_not {};
inline constexpr mimpl::_or_not_fnt                                                   _or_not {};

inline constexpr mimpl::invert_fnt                                                    invert {};
inline constexpr mimpl::get_holded_value_fnt                                          get_holded_value {};
template <typename T>
inline constexpr mimpl::is_t_same_as_fnt<T>                                           is_t_same_as {};
template <auto ... t_checkers>
inline constexpr mimpl::is_t_satisfying_fnt<t_checkers...>                            is_t_satisfying {};


template <auto t_val>
inline constexpr mimpl::return_value_fnt<t_val>                                       return_value {};

template <typename ... Ts>
inline constexpr type_sequence<Ts...> typeseq {};

template <typename ... Ts>
inline constexpr type_sequence<std::remove_cvref_t<Ts>...> clean_typeseq {};

template <typename ... Ts>
using clean_type_sequence = type_sequence<std::remove_cvref_t<Ts>...>;


template <c_type_sequence auto t_sec, template <typename...> class ResT>
using pass_to_t = typename decltype(t_sec | pass_to<ResT>)::t;


template <auto t_checker, typename DefaultT, typename ... ArgTs>
inline constexpr auto last_like_or_v = clean_typeseq<ArgTs...> | get_last_like_or<t_checker, DefaultT> | unwrap;

template <auto t_checker, typename DefaultT, typename ... ArgTs>
using last_like_or_t = decltype(*(clean_typeseq<ArgTs...> | get_last_like_or<t_checker, DefaultT>));

template <template <typename> class CheckerT, typename DefaultT, typename ... ArgTs>
inline constexpr auto tlast_like_or_v = clean_typeseq<ArgTs...> | tget_last_like_or<CheckerT, DefaultT> | unwrap;

template <template <typename> class CheckerT, typename DefaultT, typename ... ArgTs>
using tlast_like_or_t = decltype(*(clean_typeseq<ArgTs...> | tget_last_like_or<CheckerT, DefaultT>));




template <auto ... t_vals>
inline constexpr auto vtypeseq = typeseq<std::remove_cvref_t<decltype(t_vals)>...>;

template <auto t_val, typename T>
inline constexpr bool value_same_as = std::is_same_v<extract_type<t_val>, T>;

template <typename T, auto t_val>
concept c_same_as_val = value_same_as<t_val, T>;



inline namespace const_literal
{
  template <char ... chs>
  consteval auto operator""_c() noexcept
  requires(in_range<'0', '9'>(chs) and ...)
  {
  static constexpr std::array _data {chs..., '\0'};
  return value_holder<[]{
    std::int64_t res{};
    (void)std::from_chars(_data.begin(), _data.end(), res);
    return res;
  }()>{};
  }
}

namespace meta_detail
{
	template <auto t_val, typename FuncT>
	constexpr auto _call_func() noexcept
	{
		using val_t = std::remove_cvref_t<decltype(t_val)>;

		if constexpr (requires { static_call<FuncT{}>(t_val); })
			return static_call<FuncT{}>(t_val);
		else if constexpr (requires { t_val | FuncT{}; })
			return t_val | FuncT{};
		else if constexpr (c_type_sequence<val_t>)
			return t_val | pass_and_execute<FuncT{}>;

		else if constexpr (c_type_holder<val_t>)
		{
			if constexpr (requires { static_call<FuncT{}, extract_type<t_val>>(); })
				return static_call<FuncT{}, extract_type<t_val>>();
			else return static_call<FuncT{}>(t_val);
		}
		else static_assert(always_false<t_val>(), "not callable");
	}
	
} // namespace meta_detail


template <auto t_checker, typename DefValT, typename ... ArgTs>
constexpr auto && get_ell_which_or(DefValT && defval, ArgTs && ... args) noexcept
{
  static constexpr std::size_t pos = typeseq<ArgTs && ...> | get_first_index_of_like<t_checker>;
  if constexpr (pos == npos)
    return std::forward<DefValT>(defval);
  else return get_ell_at<pos>(std::forward<ArgTs>(args)...);
}

template <template <typename> class CheckerT, typename DefValT, typename ... ArgTs>
constexpr auto && get_ell_which_or(DefValT && defval, ArgTs && ... args) noexcept
{
  static constexpr std::size_t pos = typeseq<ArgTs && ...> | tget_first_index_of_like<CheckerT>;
  if constexpr (pos == npos)
    return std::forward<DefValT>(defval);
  else return get_ell_at<pos>(std::forward<ArgTs>(args)...);
}

template <typename T, typename DefValT, typename ... ArgTs>
constexpr auto && get_ell_of_type_or(DefValT && defval, ArgTs && ... args) noexcept
{
  static constexpr std::size_t pos = clean_typeseq<ArgTs...> | get_first_index_of<T>;
  if constexpr (pos == npos)
    return std::forward<DefValT>(defval);
  else return get_ell_at<pos>(std::forward<ArgTs>(args)...);
}

} // namespace pspp::inline meta

template <typename T, std::size_t t_index>
struct std::tuple_size<pspp::indexed_type<T, t_index>>
{ static_size value = 2; };

template <typename T, std::size_t t_index>
struct std::tuple_element<0, pspp::indexed_type<T, t_index>>:
		std::type_identity<pspp::type_t<T>>
{};

template <typename T, std::size_t t_index>
struct std::tuple_element<1, pspp::indexed_type<T, t_index>>:
		std::type_identity<pspp::value_holder<t_index>>
{};

namespace pspp::inline meta
{

template <std::size_t t_n, typename T, std::size_t t_index>
constexpr auto get(pspp::indexed_type<T, t_index>) noexcept
requires (t_n < 2)
{
	if constexpr (t_n == 0)
		return pspp::type_t<T>{};
	else return pspp::val<t_index>;
}

template <auto t_val, bool t_invert, auto t_ts, typename FuncT>
constexpr auto operator|(pspp::mimpl::conditional_info_wrapper<t_val, t_invert, t_ts> cond, FuncT) noexcept
{
	if constexpr (requires {FuncT{}(cond);})
		return FuncT{}(cond);
	else if constexpr (cond.can_go())
	{
		if constexpr (requires { static_call<FuncT{}>(t_ts); })
			return static_call<FuncT{}>(t_ts);
		else if constexpr (requires { t_ts | FuncT{}; })
			return t_ts | FuncT{};
		else if constexpr (c_type_holder<decltype(t_val)>)
		{
			if constexpr (requires { static_call<FuncT{}, extract_type<t_val>>(); })
				return static_call<FuncT{}, extract_type<t_val>>();
			else return static_call<FuncT{}>(t_val);
		}
		else return static_call<FuncT{}, t_val>();
	}
	else return cond;
}

template <auto t_ts, typename ... Extra>
constexpr auto operator|(mimpl::to_be_passed_holder<t_ts, Extra...> , auto t_func) noexcept
{
  return mimpl::pass_and_execute_fnt<decltype(t_func){}, Extra...>{}(t_ts);
}

template <auto t_ts, bool t_invert, typename FuncT, auto t_rest>
constexpr auto operator|(pspp::mimpl::conditional_holder<t_ts, t_invert, t_rest>, FuncT) noexcept
{
	static constexpr auto res = pspp::meta_detail::_call_func<t_ts, FuncT>();
	if constexpr (requires { static_cast<bool>(res); })
	{
		if constexpr (std::same_as<std::remove_cvref_t<decltype(t_rest)>, pspp::nullt>)
			return pspp::mimpl::conditional_info_wrapper<res, t_invert, t_ts>{};
		else return pspp::mimpl::conditional_info_wrapper<res, t_invert, t_rest>{};
	}
	else return pspp::mimpl::conditional_holder<res, t_invert, t_ts>{};
}

template <auto t_prev, typename FuncT>
constexpr auto operator|(pspp::mimpl::_and_holder<t_prev>, FuncT)
{
	if constexpr (not t_prev.can_go())
		return t_prev;
	else return pspp::mimpl::conditional_info_wrapper<t_prev.arg | FuncT{}, false, t_prev.arg>{};
}

template <auto t_prev, typename FuncT>
constexpr auto operator|(pspp::mimpl::_or_holder<t_prev>, FuncT)
{
	if constexpr (t_prev.can_go())
		return t_prev;
	else return pspp::mimpl::conditional_info_wrapper<t_prev.arg | FuncT{}, false, t_prev.arg>{};
}

template <auto t_prev, typename FuncT>
constexpr auto operator|(pspp::mimpl::_and_not_holder<t_prev>, FuncT)
{
	if constexpr (not t_prev.can_go())
		return t_prev;
	else return pspp::mimpl::conditional_info_wrapper<not (t_prev.arg | FuncT{}), false, t_prev.arg>{};
}

template <auto t_prev, typename FuncT>
constexpr auto operator|(pspp::mimpl::_or_not_holder<t_prev>, FuncT)
{
	if constexpr (t_prev.can_go())
		return t_prev;
	else return pspp::mimpl::conditional_info_wrapper<not (t_prev.arg | FuncT{}), false, t_prev.arg>{};
}

template <typename T>
constexpr bool operator==(pspp::type_t<T>, pspp::type_t<T>) noexcept
{ return true; }
template <typename T, typename U>
constexpr bool operator==(pspp::type_t<T>, pspp::type_t<U>) noexcept
{ return false; }

template <typename T>
struct holded_type_sequence
{ using types = T; };

template <typename ... Ts, template <typename...> class HolderT>
struct holded_type_sequence<HolderT<Ts...>>
{ using types = type_sequence<Ts...>; };

template <typename T>
using holded_type_sequence_t = typename holded_type_sequence<T>::types;

template <typename T>
inline constexpr auto holded_typeseq = typename holded_type_sequence<T>::types{};

template <template <typename ...> class TargetT, typename BeingTestedT>
consteval bool is_derived_from_complex() noexcept
{
  if constexpr (not requires { type_sequence{type<BeingTestedT>}; })
    return false;

  using base_t = typename decltype(
    type_sequence{type<BeingTestedT>}
      | pass_to<TargetT>
  )::t;
  return std::derived_from<BeingTestedT, base_t>;
}

template <typename T, template <typename...> class TargetT>
concept c_derived_from_complex = is_derived_from_complex<TargetT, T>();

} // namespace pspp::inline meta

namespace pspp
{

inline constexpr auto all_operations = typeseq<
  plus_t, minus_t, multiplies_t, divides_t, modulus_t, negate_t,

  equal_to_t, not_equal_to_t, greater_t, less_t, greater_equal_t, less_equal_t,

  logical_and_t, logical_or_t, logical_not_t,

  bit_and_t, bit_or_t, bit_xor_t, bit_not_t,
  bit_shift_left_t, bit_shift_right_t
>;

inline constexpr auto all_arithmetic_operations = typeseq<
  plus_t, minus_t, multiplies_t, divides_t, modulus_t, negate_t
>;

inline constexpr auto all_comparison_operations = typeseq<
  equal_to_t, not_equal_to_t, greater_t, less_t, greater_equal_t, less_equal_t
>;

inline constexpr auto all_logical_operations = typeseq<
  logical_and_t, logical_or_t, logical_not_t
>;

inline constexpr auto all_bit_operations = typeseq<
  bit_and_t, bit_or_t, bit_xor_t, bit_not_t,
  bit_shift_left_t, bit_shift_right_t
>;

inline constexpr auto all_bin_operations = typeseq<
  plus_t, minus_t, multiplies_t, divides_t, modulus_t,

  equal_to_t, not_equal_to_t, greater_t, less_t, greater_equal_t, less_equal_t,

  logical_and_t, logical_or_t,

  bit_and_t, bit_or_t, bit_xor_t, bit_shift_left_t, bit_shift_right_t
>;


template <typename ... Ts>
struct enable_all_arithmetic_ops_except :
  decltype(all_arithmetic_operations | remove<Ts...> | pass_to<enable_ops>)::t
{ };

template <typename ... Ts>
struct enable_all_comparison_ops_except :
  decltype(all_comparison_operations | remove<Ts...> | pass_to<enable_ops>)::t
{ };

template <typename ... Ts>
struct enable_all_bit_ops_except :
  decltype(all_bit_operations | remove<Ts...> | pass_to<enable_ops>)::t
{ };

template <typename ... Ts>
struct enable_all_logical_ops_except :
  decltype(all_logical_operations | remove<Ts...> | pass_to<enable_ops>)::t
{ };

template <typename ... Ts>
struct enable_all_bin_ops_except :
  decltype(all_bin_operations | remove<Ts...> | pass_to<enable_ops>)::t
{ };

template <typename ... Ts>
struct enable_all_ops_except :
  decltype(all_operations | remove<Ts...> | pass_to<enable_ops>)::t
{ };

template <auto t_cond_func>
struct forbid_automatic_operators_overload_for<
  mimpl::repeat_while_fnt<t_cond_func>
> :
  enable_ops<logical_and_t>
{};

template <typename T>
concept c_indexed_type = requires {
  typename T::type;
  { T::index } -> c_same_as<std::size_t>;
};

inline constexpr struct extract_index_from_type_t
{
  template <c_indexed_type T>
  constexpr std::size_t pspp_static_call_const_operator() noexcept
  { return T::index; }

} extract_index_from_type {};

} // namespace pspp

// | _if | is_uniqe | _and | contains<int>
//       | append_types<int, char>

// MAYBE TODO: add projection type for: ts | projection<[]<typename T>{...}> | unique | reproject; for example

#endif // PSPP_TYPE_SEQUENCE_HPP
