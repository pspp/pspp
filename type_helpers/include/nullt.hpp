#ifndef PSPP_NULLT_HPP
#define PSPP_NULLT_HPP

namespace pspp
{
inline namespace meta
{

struct nullt{};
inline constexpr nullt nulltv{};

template <typename T>
struct is_nullt
{ inline static constexpr bool value = false; };


template <>
struct is_nullt<nullt>
{ inline static constexpr bool value = true; };

template <typename T>
concept c_nullt = is_nullt<std::remove_cvref_t<T>>::value;

template <auto t_val>
inline constexpr bool is_nullt_v = false;

template <>
inline constexpr bool is_nullt_v<nulltv> = true;

} // namespace type_helpers
} // namespace pspp

#endif // PSPP_NULLT_HPP

