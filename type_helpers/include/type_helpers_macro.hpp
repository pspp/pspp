#ifndef PSPP_TYPE_HELPERS_MACRO_HPP
#define PSPP_TYPE_HELPERS_MACRO_HPP

namespace pspp
{
inline namespace meta
{

#define EXTEND(...) __VA_ARGS__

#define ADD_CHECKER_FUNC(type_name, template_decl, template_usage)\
	template < EXTEND template_decl>\
	inline consteval bool is_ ## type_name( type_name < EXTEND template_usage > ) { return true; } \
	\
	inline consteval bool is_ ## type_name( auto ) { return false; } \
	\
	template <typename Maybe ## type_name ## T> \
	concept c_ ## type_name = is_ ## type_name( Maybe ## type_name ## T {} );

}
}

#endif // PSPP_TYPE_HELPERS_MACRO_HPP

