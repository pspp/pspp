
#ifndef pspp_type_helpers
#define pspp_type_helpers 

#include <concepts>
#include <pspp/useful/sequence_helpers.hpp>
#include <pspp/value_holder.hpp>
#include <pspp/nullt.hpp>

namespace pspp
{
inline namespace meta
{

template <typename T>
struct type_t
{
	template <typename DataT>
	constexpr type_t (DataT) noexcept {} 

	constexpr type_t() noexcept {}

	using t = T;
};

} // inline namespace meta
namespace detail
{
	enum class static_call_for_ts
	{
		TTN,
		TAtN,
		NTAt,
		TAvN,
		NTAv,
		empty,
		not_callable
	};

	template <auto t_func, typename ... Args, typename ... NonTemplateArgs>
	inline consteval static_call_for_ts static_call_for_ts_checker(type_t<NonTemplateArgs> ... args) noexcept
	{
		using enum static_call_for_ts;

		if constexpr (requires { t_func.template operator()<Args...>(std::declval<NonTemplateArgs>()...); })
			return TTN;
		else if constexpr (requires {t_func(type_t<Args>{}..., std::declval<NonTemplateArgs>()...);})
			return TAtN;
		else if constexpr (requires {t_func(std::declval<NonTemplateArgs>()..., type_t<Args>{}...);})
			return NTAt;
		else if constexpr (requires {t_func(Args{}..., std::declval<NonTemplateArgs>()...);})
			return TAvN;
		else if constexpr (requires {t_func(std::declval<NonTemplateArgs>()..., Args{}...);})
			return NTAv;
		else if constexpr (requires {t_func();})
			return empty;
		else return not_callable;
	}

}

inline namespace meta
{

template <auto t_func, typename ... Args, typename ... NonTemplateArgs>
inline constexpr auto static_call(NonTemplateArgs && ... args) noexcept -> decltype(auto)
	requires (detail::static_call_for_ts_checker<t_func, Args...>(type_t<NonTemplateArgs>{}...) != detail::static_call_for_ts::not_callable)
{
	if constexpr (requires { t_func.template operator()<Args...>(std::forward<NonTemplateArgs>(args)...); })
		return t_func.template operator()<Args...>(std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires {t_func(type_t<Args>{}..., std::forward<NonTemplateArgs>(args)...);})
		return t_func(type_t<Args>{}..., std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires {t_func(std::forward<NonTemplateArgs>(args)..., type_t<Args>{}...);})
		return t_func(std::forward<NonTemplateArgs>(args)..., type_t<Args>{}...);
	else if constexpr (requires {t_func(Args{}..., std::forward<NonTemplateArgs>(args)...);})
		return t_func(Args{}..., std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires {t_func(std::forward<NonTemplateArgs>(args)..., Args{}...);})
		return t_func(std::forward<NonTemplateArgs>(args)..., Args{}...);
	else if constexpr (requires {t_func();})
		return t_func();
	else static_assert(sizeof(t_func) < 0, "static_call: t_func is not callable");
}

template <auto t_func, auto t_arg1, auto ... t_args, typename ... NonTemplateArgs>
inline constexpr auto static_call(NonTemplateArgs && ... args) noexcept
{
	if constexpr (requires { t_func.template operator()<t_arg1, t_args...>(std::forward<NonTemplateArgs>(args)...); })
		return t_func.template operator()<t_arg1, t_args...>(std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires { t_func(t_arg1, t_args..., std::forward<NonTemplateArgs>(args)...); })
		return t_func(t_arg1, t_args..., std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires { t_func(std::forward<NonTemplateArgs>(args)..., t_arg1, t_args...); })
 		return t_func(std::forward<NonTemplateArgs>(args)..., t_arg1, t_args...); 
	else if constexpr (requires { t_func(); })
		return t_func();
	else static_assert(sizeof(t_func) < 0, "static_call: t_func is not callable");
}

template <auto t_func, typename Arg1, auto t_arg2, auto ... t_args, typename ... NonTemplateArgs>
inline constexpr auto static_call(NonTemplateArgs && ... args) noexcept
{
	if constexpr (requires { t_func.template operator()<Arg1, t_arg2, t_args...>(std::forward<NonTemplateArgs>(args)...); })
		return t_func.template operator()<Arg1, t_arg2, t_args...>(std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires { t_func(type_t<Arg1>{}, t_arg2, t_args..., std::forward<NonTemplateArgs>(args)...); })
		return t_func(type_t<Arg1>{}, t_arg2, t_args...);
	else if constexpr (requires { t_func.template operator()<Arg1>(t_arg2, t_args..., std::forward<NonTemplateArgs>(args)...); })
		return t_func.template operator()<Arg1>(t_arg2, t_args..., std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires { t_func.template operator()<Arg1>(std::forward<NonTemplateArgs>(args)..., t_arg2, t_args...); })
		return t_func.template operator()<Arg1>(std::forward<NonTemplateArgs>(args)..., t_arg2, t_args...);
	else if constexpr (requires { t_func(Arg1{}, t_arg2, t_args..., std::forward<NonTemplateArgs>(args)...); })
		return t_func(Arg1{}, t_arg2, t_args..., std::forward<NonTemplateArgs>(args)...);
	else if constexpr (requires { t_func(std::forward<NonTemplateArgs>(args)..., Arg1{}, t_arg2, t_args...); })
		return t_func(std::forward<NonTemplateArgs>(args)..., Arg1{}, t_arg2, t_args...);
	else if constexpr (requires { t_func(); })
		return t_func();
	else static_assert(sizeof(t_func) < 0, "static_call: t_func is not callable");
}

template <typename T>
[[nodiscard]] consteval inline type_t<T> to_type(T) noexcept
{
	return {};
}

consteval inline bool is_type_t_helper(auto) noexcept 
{
	return false;
}

template <typename T>
consteval inline bool is_type_t_helper(type_t<T>) noexcept
{
	return true;
}

template <typename MaybeT>
struct is_type_t : std::false_type {};

template <typename SomeT>
struct is_type_t<type_t<SomeT>> : std::true_type {};

template <typename MaybeTypeT>
concept c_type = is_type_t<MaybeTypeT>::value;

template <c_type auto t_t>
using extract_type = typename decltype(t_t)::t;

template <unsigned int t_index>
struct index_t
{
	inline consteval static unsigned int n() noexcept
	{
		return t_index;
	}
};

//#define index(n) index_t<n>{}

template <typename DataT>
type_t(DataT&&) -> type_t<DataT>;

template <typename MaybeCallableWithTypeT>
concept type_t_handler_fn = requires(MaybeCallableWithTypeT fn)
{
	fn(type_t<nullt>{});
};

template <typename MaybeTypeTCheckerFnT>
concept type_t_checker_fn = requires(MaybeTypeTCheckerFnT fn)
{
	{ fn(type_t<nullt>{}) } -> std::same_as<bool>;
};

template <auto t_val_fn, typename ... Args>
inline static constexpr auto types_passed_value = static_call<t_val_fn, Args...>();

template <class ... Bases>
struct inheritance_base_t : Bases... {};

template <auto t_class_transformer, class ... Bases>
inline static consteval auto inheritance_base_impl()
{
	return static_call<t_class_transformer, Bases...>(); 
}

template <auto t_class_transformer, class ... Bases>
using inheritance_base = extract_type<inheritance_base_impl<t_class_transformer, Bases...>()>;

template <class ... Bases>
using simple_inheritance_base = inheritance_base_t<Bases...>;

template <template <typename ...> class Target, typename T>
struct is_tstype_t : std::false_type {};

template <template <typename ...> class Target, typename ... Ts>
struct is_tstype_t<Target, Target<Ts...>> : std::true_type {};

template <template <typename ...> class Target>
struct is_tstype
{
	template <typename T>
	using res = is_tstype_t<Target, T>;
};

template <auto val>
using clean_type = std::remove_cvref_t<decltype(val)>;

template <typename T>
inline constexpr auto type = type_t<T>{};

template <auto t_val>
inline constexpr auto vtype = type_t{t_val};

template <auto t_val>
inline constexpr bool is_vnullt = std::same_as<clean_type<t_val>, nullt>;

template <typename T1, typename T2>
struct tpair 
{
	using first = T1;
	using second = T2;
};

template <typename T>
struct type_holder { using type = T; };

template <std::size_t t_size>
struct dummy_type {
	char m_dummy_dta [t_size] {""};
};

template <typename T>
using to_dummy_type = dummy_type<sizeof(T)>;

} // meta
} // pspp 

#endif // pspp_type_helpers


