#ifndef PSPP_NULLT_HPP
#define PSPP_NULLT_HPP

namespace pspp
{
inline namespace type_helpers
{

struct nullt{};
inline constexpr nullt nulltv{};

} // namespace type_helpers
} // namespace pspp

#endif // PSPP_NULLT_HPP

