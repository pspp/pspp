
#ifndef PSPP_TYPE_SEQUENCE_HPP
#define PSPP_TYPE_SEQUENCE_HPP

#include <pspp/type_helpers.hpp>
#include <pspp/useful/overloaded_lambda.hpp>
#include <numeric>

namespace pspp
{
inline namespace meta
{

template <typename MaybeTypeCheckerFn>
concept type_requirments_checker_fn = requires (MaybeTypeCheckerFn fn)
{
	{ fn(type<int>) } -> std::same_as<bool>;
};

template <std::size_t t_first, std::size_t ... t_rest>
[[nodiscard]] inline consteval static type_t<std::index_sequence<t_rest...>> 
pop_first_from_indexes(std::index_sequence<t_first, t_rest...>)
{
	return {};
}

template <template <typename> class ... Checker>
inline constexpr auto satisfies = []<typename T>{ return (Checker<T>::value and ...); };

template <typename ... Target>
inline constexpr auto same_as = []<typename T>{ return (std::same_as<T, Target> and ...); };

template <template <typename ...> class Target>
struct is_complex_target_t
{
	template <typename T>
	struct is_target : std::false_type {};

	template <typename ... Types>
	struct is_target<Target<Types...>> : std::true_type {};
};

template <template <auto ...> class Target>
struct is_complex_vtarget_t
{
	template <typename T>
	struct is_target : std::false_type {};

	template <auto ... t_vals>
	struct is_target<Target<t_vals...>> : std::true_type {};
};

template <template <typename, auto ...> class Target>
struct is_complex_tvtarget_t
{
	template <typename T>
	struct is_target : std::false_type{};

	template <typename T, auto ... t_vals>
	struct is_target<Target<T, t_vals...>> : std::true_type {};
};

template <template <auto, typename...> class Target>
struct is_complex_vttarget_t
{
	template <typename T>
	struct is_target : std::false_type{};

	template <auto t_val, typename ... Ts>
	struct is_target<Target<t_val, Ts...>> : std::true_type {};
};

template <typename T, template <typename ...> class Target>
concept c_complex_target = is_complex_target_t<Target>::template is_target<T>::value;

template <typename T, template <typename, auto ...> class Target>
concept c_complex_tvtarget = is_complex_tvtarget_t<Target>::template is_target<T>::value;

template <typename T, template <auto, typename ...> class Target>
concept c_complex_vttarget = is_complex_vttarget_t<Target>::template is_target<T>::value;

template <typename T, template <auto ...> class Target>
concept c_complex_vtarget = is_complex_vtarget_t<Target>::template is_target<T>::value;

template <template <typename ...> class Target>
inline constexpr auto same_as_complex = []<typename T>{ return is_complex_target_t<Target>::template is_target<T>::value; };

template <template <auto, typename ...> class Target>
inline constexpr auto same_as_complex_vt = []<typename T>{ return is_complex_vttarget_t<Target>::template is_target<T>::value; };

template <template <typename, auto ...> class Target>
inline constexpr auto same_as_complex_tvalue = []<typename T>{ return is_complex_tvtarget_t<Target>::template is_target<T>::value; };

template <template <auto ...> class Target>
inline constexpr auto same_as_complex_value = []<typename T>{ return is_complex_vtarget_t<Target>::template is_target<T>::value; };

template <typename ... Ts>
inline constexpr auto some_of = []<typename T>{ return (std::same_as<T, Ts> or ...); };

template <typename ... Ts>
struct type_sequence_view;

template <typename ... Ts>
struct type_sequence;

//struct empty_type_sequence;

template <typename MaybeTypesSeqT>
struct is_type_sequence : std::false_type {};

template <typename ... Ts>
struct is_type_sequence<type_sequence<Ts...>> : std::true_type {};

//template <>
//struct is_type_sequence<empty_type_sequence> : std::true_type {};

template <typename ... Ts>
struct is_type_sequence<type_sequence_view<Ts...>> : std::true_type {};

template <typename MaybeTypesSeqT>
inline constexpr bool is_type_sequence_v = is_type_sequence<MaybeTypesSeqT>::value;

template <typename MaybeTypesSeqT>
concept c_type_sequence = is_type_sequence_v<MaybeTypesSeqT>;

template <typename MaybeEmptyTs>
concept c_empty_type_sequence = std::same_as<MaybeEmptyTs, type_sequence<>>;

template <>
struct type_sequence<>
{
	inline static constexpr bool can_be_shifted { false };
	inline static constexpr std::size_t count { 0 };

	template <c_type_sequence ToAdd>
	using append = ToAdd;

	template <typename T0, typename ...Ts>
	using append_types = type_sequence<T0, Ts...>;

	template <typename ... Ts>
	using append_types_to_front = type_sequence<Ts...>;

	template <template <typename ...> class ToBeGiven, typename ... ExtraTs>
	using passed_type = ToBeGiven<ExtraTs...>;

	template <typename ... Ts>
	inline static constexpr bool contains = false;

	template <typename ... Ts>
	static_flag contains_some_of = false;

	template <auto, typename DefT>
	using last_succeed_or = DefT;

	template <typename ... T>
	static_size count_of = 0;

	template <auto _>
	using transform = type_sequence<>;
};

using empty_type_sequence = type_sequence<>;

template <typename ... Ts>
struct base_tail_holder_t
{
	using base_tail = type_sequence_view<Ts...>;
	using base_safe_tail = type_sequence_view<Ts...>;
};

template <typename T0>
struct base_tail_holder_t<T0>
{
	using base_safe_tail = type_sequence_view<T0>;
};

template <typename ... Ts>
struct tail_holder_t
{
	using tail = type_sequence<Ts...>;
	using safe_tail = type_sequence<Ts...>;
};

template <typename T0>
struct tail_holder_t<T0>
{
	using safe_tail = type_sequence<T0>;
};

template <typename MaybeHas>
concept c_has_more_than_one_index = requires {
	[]<std::size_t t_first, std::size_t ... t_rest> (type_t<std::index_sequence<t_first, t_rest...>>)
	{}(type_t<MaybeHas>{});
};

template <typename MaybeHas>
concept c_has_less_than_one_index = not c_has_more_than_one_index<MaybeHas>;


template <bool t_was_ok, typename Succeed, typename Failed>
struct check_res_t;


template <bool t_was_ok, typename Succeed, typename Failed>
struct indexes_poper
{};

template <bool t_was_ok, c_has_more_than_one_index Succed, c_has_more_than_one_index Failed>
struct indexes_poper<t_was_ok, Succed, Failed>
{
	using poped_from_success = check_res_t<t_was_ok,
				extract_type<pop_first_from_indexes(Succed{})>,
				Failed>;

	using poped_from_failed = check_res_t<
				c_has_less_than_one_index<
					extract_type<pop_first_from_indexes(Failed{})>
				>,
				Succed,
				extract_type<pop_first_from_indexes(Failed{})>>;
};

template <bool t_was_ok, c_has_more_than_one_index Succeed, c_has_less_than_one_index Failed>
struct indexes_poper<t_was_ok, Succeed, Failed>
{
	using poped_from_success = check_res_t<t_was_ok,
				extract_type<pop_first_from_indexes(Succeed{})>,
				Failed>;
};

template <bool t_was_ok, c_has_less_than_one_index Succeed, c_has_more_than_one_index Failed>
struct indexes_poper<t_was_ok, Succeed, Failed>
{
		using poped_from_failed = check_res_t<
				c_has_less_than_one_index<
					extract_type<pop_first_from_indexes(Failed{})>
				>, 
				Succeed,
				extract_type<pop_first_from_indexes(Failed{})>>;
};

template <std::size_t t_to_add, std::size_t ...Is>
[[nodiscard]] inline consteval static std::index_sequence<Is..., t_to_add> append_index(std::index_sequence<Is...>) noexcept
{
	return {};
}

template <bool t_was_ok, typename Succeed, typename Failed>
struct check_res_t /*: indexes_poper<t_was_ok, Succeed, Failed>*/
{
	using was_ok = value_holder<t_was_ok>;
	using succeed = Succeed;
	using failed = Failed;
	using has_succeed = value_holder<(succeed::size() > 0)>;

	template <bool t_satisfied>
	inline static consteval auto get_impl() noexcept
	{
		if constexpr (t_satisfied)
			return type_t<succeed>{};
		else
			return type_t<failed>{};
	}

	template <bool t_satisfied>
	using get = extract_type<get_impl<t_satisfied>()>;

	template <std::size_t t_pos, bool t_satisfied = true>
	inline static constexpr std::size_t get_at = get_ell_at_in_seq_holder<t_pos>(get<t_satisfied>{});

	template <bool t_satisfied = true>
	inline static constexpr std::size_t get_last = get_last_from_seq_holder(get<t_satisfied>{});


	template <std::size_t t_n>
	inline static consteval auto succeed_at() noexcept
	{
		return type_t<
			check_res_t<
				t_was_ok & true,
				std::remove_cvref_t<decltype(append_index<t_n>(Succeed{}))>,
				Failed>
		>{};
	}

	template <std::size_t t_n>
	inline static consteval auto failed_at() noexcept
	{
		return type_t<
			check_res_t<
				false,
				Succeed,
				std::remove_cvref_t<decltype(append_index<t_n>(Failed{}))>
			>
		>{};
	}

	template <bool t_res, std::size_t t_n>
	inline static consteval auto add_res() noexcept
	{
		if constexpr (t_res)
			return succeed_at<t_n>();
		else
			return failed_at<t_n>();
	}
};


template <template <typename ...> typename ResT, typename ... Ts>
struct type_sequence_inheritance_base
{
	inline static constexpr std::size_t npos = std::numeric_limits<std::size_t>::max();
	inline static constexpr unsigned int count = sizeof...(Ts);
	inline static constexpr unsigned int size = sizeof...(Ts);
	inline static constexpr bool can_be_shifted = sizeof...(Ts) > 1;

	struct helpers
	{
		template <unsigned int t_n, typename T0, typename ... Rest>
		[[nodiscard]] static consteval auto get_n_impl() noexcept
		{
			if constexpr (t_n == 0)
				return type<T0>;
			else if constexpr (sizeof...(Rest) > 0)
				return get_n_impl<t_n - 1, Rest...>();
			else
				static_assert(sizeof(T0) < 0, "type_sequence: n is to big");
		}

		template <unsigned int t_n>
		[[nodiscard]] static consteval auto get_n() noexcept
		{
			static_assert(sizeof...(Ts) > 0, "empty type sequence");
			return get_n_impl<t_n, Ts...>();
		}
		
		template <typename ... Us>
		[[nodiscard]] static consteval auto get_first() noexcept
		{
			return get_n_impl<0, Us...>();
		}

		[[nodiscard]] inline static constexpr auto get_last() noexcept
		{
			return get_n(index_t<sizeof...(Ts) - 1>{});
		}

		template <auto t_n>
		[[nodiscard]] inline static consteval auto get_n(index_t<t_n>) noexcept
		{
			return get_n<t_n>();
		}

		template <std::size_t ... t_is>
		[[nodiscard]] inline static constexpr auto reverse_impl(std::index_sequence<t_is...>) noexcept
		{
			return type_t<ResT<at_from_back<t_is>...>>{};
		}

		template <std::size_t ...Is>
		[[nodiscard]] inline consteval static type_t<ResT<extract_type<get_n(index_t<Is>{})>...>> 
			sub_sequence_impl(std::index_sequence<Is...>) noexcept
		{
			return {};
		}

		template <std::size_t t_first, std::size_t ... t_rest>
		[[nodiscard]] inline consteval static type_t<empty_type_sequence>
			sub_sequence_impl(std::index_sequence<t_first, t_rest...>) noexcept requires (sizeof...(Ts) < t_first)
		{
			return {};
		}

		[[nodiscard]] inline consteval static type_t<empty_type_sequence>
			sub_sequence_impl(std::integer_sequence<std::size_t>) noexcept
		{
			return {};
		}
	};

	using head = extract_type<helpers::template get_n<0>()>;
	using last = extract_type<helpers::get_last()>;
	
	using this_t = ResT<Ts...>;

	template <auto t_n>
	using at = extract_type<helpers::get_n(index_t<static_cast<unsigned int>(t_n)>{})>;
	template <auto t_n>
	using at_from_back = extract_type<
		helpers::get_n(index_t<sizeof...(Ts) - static_cast<unsigned int>(t_n) - 1>{})>;

	using indexes = decltype(std::make_index_sequence<count>());

	using reverse = extract_type<
		helpers::reverse_impl(std::make_index_sequence<count>())>;

	template < template <typename ...> class ToBeGiven, typename ... ExtraTs>
	using passed_type = ToBeGiven<ExtraTs..., Ts...>;

	template <auto t_func>
	inline constexpr static auto passed_lambda = []{ return t_func(type<Ts>...); };

	template <auto t_func>
	using passed_lambda_type = clean_type<[]{ return t_func(type<Ts>...); }>;

	template <auto t_func>
	inline constexpr static auto indexes_passed_lambda = 
		[]
		{ 
			return []<std::size_t ... Is>(std::index_sequence<Is...>)
			{ return static_call<t_func, Is...>(); }(std::make_index_sequence<count>()); 
		};

	template <auto t_func>
	using indexes_passed_lambda_type = decltype(indexes_passed_lambda<t_func>);

	template <auto t_val_fn>
	inline constexpr static auto passed_value = types_passed_value<t_val_fn, Ts...>;

	template <template <auto...> class ToBePassed, auto t_passer, auto ... t_extra_values>
	using values_passed_type = ToBePassed<t_extra_values..., static_call<t_passer, Ts>()...>;

	template <std::size_t ...Is>
	using sub_sequence_from = ResT<at<Is>...>;

	template <std::size_t t_start = 0, std::size_t t_end = count, std::size_t t_step = 1>
	using sub_sequence = extract_type<helpers::sub_sequence_impl(index_sequence_range<t_start, t_end, t_step>{})>;

	template <typename IndexesT>
	using sub_sequence_from_indexes = extract_type<
		helpers::sub_sequence_impl(IndexesT{})>;

	template <std::size_t t_count>
	using pop_from_back = sub_sequence<0, count - t_count>;
	
	
	/*template <typename ST1, typename ... STs>
	inline static constexpr std::size_t index_of = 
	*/
	// contains_seq <types/seq>
	// count <types>
	// count_seq <types>


};


struct transformer_types
{
	struct leave_as_was {};
	struct ignore {};

	template <typename NewT, typename ... NewTs>
	struct change_to { 
		using new_types = type_sequence_view<NewT, NewTs...>;
	};

	template <auto t_val, auto ... t_vals>
	struct change_to<value_holder<t_val>, value_holder<t_vals>...>
	{
		using new_types = type_sequence<value_holder<t_val>, value_holder<t_vals>...>;
	};
};

inline namespace transformers
{
	inline constexpr transformer_types::leave_as_was leave_as_was{};
	inline constexpr transformer_types::ignore ignore{};

	template <typename NewT, typename ... NewTs>
	inline constexpr transformer_types::change_to<NewT, NewTs...> change_to {};
};


template <typename T>
using extract_types_to_typeseq = extract_type<
	[]<typename ... Types, template <typename ...> class Target>(type_t<Target<Types...>>) 
	{ return type<type_sequence<Types...>>; }(type<T>)
>;

template <typename T>
struct is_type_seq_holder : std::false_type {};

template <c_type_sequence TS>
struct is_type_seq_holder<type_t<TS>> : std::true_type {};

template <typename MaybeTypeSeqHolderT>
concept c_type_seq_holder = is_type_seq_holder<MaybeTypeSeqHolderT>::value;

template <typename MaybeTransformerHandlerFnT>
concept c_transformer_fn = requires (MaybeTransformerHandlerFnT mhf, type_sequence<int> ts){
	{ static_call<MaybeTransformerHandlerFnT{}, decltype(ts)>() } -> c_type_seq_holder;
};

template <c_type_sequence TypeSeq>
using ts_indexised = typename TypeSeq::helpers::template from_base_t<TypeSeq::helpers::indexise_impl()>;

template <c_type_sequence TypeSeq>
using ts_indexised_with_ts = extract_type<
		TypeSeq::helpers::template transform_impl<
			[]<typename T>
			{
				return change_to<
					type_sequence<typename T::first, typename T::second>>;
				//return ignore;
			}, empty_type_sequence, 
			extract_type<TypeSeq::helpers::indexise_impl()>, true
			>()
	>;


using dummy_convertor = decltype([]<typename T>{ return type_t<T>{}; });

template <typename ... Ts>
struct type_sequence_view : base_tail_holder_t<Ts...>, type_sequence_inheritance_base<
														type_sequence_view,Ts...>
{};


template <typename ... Ts>
struct type_sequence : 
	type_sequence_inheritance_base<type_sequence, Ts...>, 
	tail_holder_t<Ts...>, 
	base_tail_holder_t<Ts...>
{
	// helper functions

	using base = type_sequence_view<Ts...>;

	constexpr bool operator==(type_sequence) const noexcept
	{
		return true;
	}

	template <typename U1, typename ... Us>
	constexpr bool operator==(type_sequence<U1, Us...>) const noexcept
	{
		return false;
	}


	struct helpers : type_sequence_view<Ts...>::helpers
	{
		struct functors
		{
			template <auto t_checker, typename T>
			struct two_to_one_adapter 
			{
				template <typename ToCheck>
				inline consteval bool operator()() const noexcept
				{
					return static_call<t_checker, ToCheck, T>();
				}
			};

			template <typename ToRemove1, typename ... ToRemoveRest>
			struct get_all_hits{
				template <typename T>
				inline consteval bool operator()() const noexcept
				{
					return std::is_same_v<T, ToRemove1> or
					(std::is_same_v<T, ToRemoveRest> || ...);
				}
			};
		};

		struct for_base
		{
			template <typename ... AddTs, typename ... Add1Ts>
			[[nodiscard]] inline static consteval auto append_impl(type_sequence_view<AddTs...>,
					type_sequence_view<Add1Ts...>) noexcept
			{
				return type_t<type_sequence_view<AddTs..., Add1Ts...>>{};
			}

			template <typename ... AddTs>
			[[nodiscard]] inline static consteval auto append_impl(empty_type_sequence, type_sequence_view<AddTs...>) noexcept
			{
				return type_t<type_sequence_view<AddTs...>>{};
			}

			template <typename ... AddTs>
			[[nodiscard]] inline static consteval auto append_impl(type_sequence_view<AddTs...>, empty_type_sequence) noexcept
			{
				return type_t<type_sequence_view<AddTs...>>{};
			}

			[[nodiscard]] inline static consteval auto append_impl(empty_type_sequence, empty_type_sequence) noexcept
			{
				return type_t<empty_type_sequence>{};
			}

			template <typename ToAppend1, typename ToAppend2>
			using base_append = extract_type<helpers::for_base::append_impl(ToAppend1{}, ToAppend2{})>;

			template <typename ToAppend1, typename ...ToAdds>
			using base_append_types = extract_type<helpers::for_base::append_impl(ToAppend1{}, type_sequence_view<ToAdds...>{})>;
		};

		/*template <typename BaseT0, typename ... BaseTs>
		[[nodiscard]] inline static consteval type_t<type_sequence<BaseT0, BaseTs...>>
			from_base(type_sequence<BaseT0, BaseTs...>) noexcept
		{ return {}; }
		*/

		template <typename ... BaseTs>
		[[nodiscard]] inline static consteval type_t<type_sequence<BaseTs...>> 
			from_base(type_sequence_view<BaseTs...>) noexcept
		{ return {}; }

		[[nodiscard]] inline static consteval type_t<empty_type_sequence>
			from_base(empty_type_sequence) noexcept
		{ return {}; }

		template <c_type auto view>
		using from_base_t = extract_type<
			from_base(extract_type<view>{})
		>;

		[[nodiscard]] inline static consteval type_t<empty_type_sequence>
			to_view(empty_type_sequence) noexcept { return {}; }

		template <typename ... NotViewTs>
		[[nodiscard]] inline static consteval type_t<type_sequence_view<NotViewTs...>>
			to_view(type_sequence<NotViewTs...>) { return {}; }

		template <typename DonePart, typename ToDoPart, std::size_t t_pos, std::size_t t_i1, std::size_t ...t_is>
		[[nodiscard]] inline static consteval auto remove_at_impl() noexcept
		{
			if constexpr (t_pos == t_i1)
			{
				if constexpr (sizeof...(t_is) > 0)
				{
					static_assert(ToDoPart::can_be_shifted, "remove_at: index out of bound");
					return remove_at_impl<DonePart, typename ToDoPart::template sub_sequence<1>, t_pos + 1, t_is...>();
				}
				else
					if constexpr (ToDoPart::can_be_shifted)
						return from_base(typename helpers::for_base::template base_append<DonePart, 
										typename ToDoPart::template sub_sequence<1>>{});
					else
						return from_base(DonePart{});
			}
			else
			{
				static_assert(ToDoPart::can_be_shifted, "remove_at: index out of bound");
				return remove_at_impl<typename for_base::template base_append_types<DonePart, typename ToDoPart::head>,
							typename ToDoPart::template sub_sequence<1>,
							t_pos + 1, t_i1, t_is...>();
			}
		}

		template <auto t_checker, bool t_satisfied = true>
		inline static consteval auto extract_specified_impl() noexcept
		{
			return from_base(
					typename base::template sub_sequence_from_indexes<
						typename this_t::check_result<t_checker>::template get<t_satisfied>
					>{});
		}

		template <typename Target = type_sequence_view<Ts...>, std::size_t ... t_is>
		[[nodiscard]] inline static consteval auto remove_at(std::integer_sequence<std::size_t, t_is...>)
		{
			if constexpr (sizeof...(t_is) == 0)
				return type_t<this_t>{};
			else
				if constexpr (requires { from_base(Target{}); })
					return remove_at_impl<empty_type_sequence, Target, 0, t_is...>();
				else
					return remove_at_impl<empty_type_sequence, typename Target::base, 0, t_is...>();
		}

		template <typename Indexes, typename Target>
		using base_remove_at = extract_type<helpers::remove_at<Target, 0>(Indexes{})>;

		template <typename ... AddTs>
		[[nodiscard]] inline static consteval auto append_to_front_impl(type_sequence<AddTs...>) noexcept
		{
			return type_t<type_sequence<AddTs..., Ts...>>{};
		}

		[[nodiscard]] inline static consteval auto append_to_front_impl(empty_type_sequence) noexcept
		{
			return type_t<this_t>{};
		}

		template <typename ... AddTs>
		[[nodiscard]] inline static consteval auto append_impl(type_sequence<AddTs...>) noexcept
		{
			return type_t<type_sequence<Ts..., AddTs...>>{};
		}

		[[nodiscard]] inline static consteval auto append_impl(empty_type_sequence) noexcept
		{
			return type_t<this_t>{};
		}

		template <auto t_checker, typename T>
		inline constexpr static bool invoke_checker() noexcept
		{
			return static_call<t_checker, T>();
		}

		template <auto t_checker, typename ToDoPart = type_sequence_view<Ts...>, typename rest = check_res_t<true, 
							std::integer_sequence<std::size_t>, 
							std::integer_sequence<std::size_t>>, std::size_t t_pos = 0>
		inline static constexpr auto check_each()
		{
			constexpr bool check_res = invoke_checker<t_checker, typename ToDoPart::head>();

			using next_res = extract_type<
							rest::template add_res<
								check_res,
								t_pos
							>()
						>;

			if constexpr (ToDoPart::can_be_shifted)
				return check_each<t_checker, typename ToDoPart::template sub_sequence<1>, next_res, t_pos + 1>();
			else
				return type_t<next_res>{};
		}

		template <auto t_checker, typename DefT>
		inline static consteval auto last_succeed_or_impl()
		{
			using all = succeed<t_checker>;
			if constexpr (all::count == 0)
				return type_t<DefT>{};
			else
				return type_t<typename all::last>{};
		}

		template <auto t_checker, typename ToDoPart = type_sequence_view<Ts...>, typename rest = check_res_t<true, 
							std::integer_sequence<std::size_t>, 
							std::integer_sequence<std::size_t>>, std::size_t t_pos = 0>
		inline static consteval auto check_each_without_()
		{
		}

		template <auto t_comparator, bool t_satisfied,
							typename BestMatch = typename base::head, typename ToBeDone = base>
		inline static consteval auto get_most() noexcept -> decltype(auto)
		{
			using next = typename ToBeDone::template sub_sequence_from_indexes<
				typename extract_type<check_each<[]<typename T>{ return static_call<t_comparator, T, BestMatch>(); },
				ToBeDone>()>::template get<t_satisfied>>;

			if constexpr (std::same_as<next, empty_type_sequence> or 
					std::same_as<type_sequence_view<BestMatch>, next>)
				return type_t<BestMatch>{};
			else
				if constexpr (std::same_as<next, ToBeDone>)
					return get_most<t_comparator, t_satisfied, typename next::head, typename next::template sub_sequence<1>>();
				else 
					return get_most<t_comparator, t_satisfied, typename next::head, next>();
		}

		inline static constexpr auto default_transformers_handler = overloaded_lambda<
			[]<c_type_sequence ToTransform, typename, typename NewT, typename ... NewTs> (type_t<transformer_types::change_to<NewT, NewTs...>>) 
			{ return type_t<typename for_base::template base_append_types<ToTransform, NewT, NewTs...>>{}; },

			[]<c_type_sequence ToTransform, typename> (type_t<transformer_types::ignore>) 
			{ return type_t<ToTransform>{}; },

			[]<c_type_sequence ToTransform, typename WasChecked> (type_t<void>)
			{ return type_t<typename for_base::template base_append_types<ToTransform, WasChecked>>{}; },

			[]<c_type_sequence ToTransform, typename WasChecked> (type_t<transformer_types::leave_as_was>)
			{ return type_t<typename for_base::template base_append_types<ToTransform, WasChecked>>{}; }
			>{};

		template <auto t_transf_fn, c_type_sequence ToDoPart, bool t_is_simple>
		inline static consteval auto call_transform_fn()
		{
			if constexpr (t_is_simple)
				return t_transf_fn.template operator()<typename ToDoPart::head>();
			else
				return t_transf_fn.template operator()<typename ToDoPart::head, this_t>();
		}

		template <auto t_transf_fn, c_type_sequence DonePart, c_type_sequence ToDoPart,
							bool t_is_simple, overloaded_lambda handler = default_transformers_handler>
		[[nodiscard]] inline static consteval auto transform_impl() noexcept
		{
			using part_res = decltype(call_transform_fn<t_transf_fn, ToDoPart, t_is_simple>());
			using next = extract_type<handler.template operator()<DonePart, typename ToDoPart::head>(type_t<part_res>{})>;
			if constexpr (ToDoPart::can_be_shifted)
				return transform_impl<t_transf_fn,
					next,
					typename ToDoPart::template sub_sequence<1>, t_is_simple, handler>();
			else
				return type_t<extract_type<helpers::from_base(next{})>>{};
		}

		template <auto t_transf_fn>
		[[nodiscard]] inline static consteval auto transform() noexcept
		{
			return transform_impl<t_transf_fn, empty_type_sequence, base, true>();
		}

		[[nodiscard]] inline static consteval auto indexise_impl() noexcept
		{
			return []<std::size_t t_first, std::size_t ... t_rest>
				(std::index_sequence<t_first, t_rest...>)
				{
					return type<type_sequence_view<
						tpair<Ts, value_holder<t_rest>>...>>;
				}(std::make_index_sequence<sizeof...(Ts) + 1>());
		}

		template <auto t_sorter, typename DoneT = empty_type_sequence, 
							typename ToBeDone = extract_type<indexise_impl()>>
		[[nodiscard]] inline static consteval auto sort_impl() noexcept 
		{
			using best = extract_type<
				get_most<
				[]<typename T1, typename T2>
				{ return static_call<t_sorter, typename T1::first, typename T2::first>(); },
				true, typename ToBeDone::head, ToBeDone>()
			>;

			using without_best = extract_type<
				transform_impl<
					[]<typename T> 
					{
						if constexpr (T::second::value == best::second::value)
						{
							return ignore;
						}
						else return leave_as_was;
					}, empty_type_sequence, 
					ToBeDone, true
				>() 
				>;

			using next = typename for_base::template base_append_types<DoneT, typename best::first>;

			if constexpr (without_best::count > 0)
				return sort_impl<t_sorter, next,
								without_best
							>();
			else
			{
				return type<next>;
			}
		}

	}; // helpers

	// functions

	template <auto t_comparator, c_type_sequence DonePart = empty_type_sequence,
						c_type_sequence ToDoPart = type_sequence_view<Ts...>>
	inline static consteval auto to_unique() noexcept
	{
		using Added = typename helpers::for_base::template base_append_types<DonePart,
			typename ToDoPart::head>;

		using next = typename helpers::template base_remove_at<
				typename check_result<
					typename helpers::functors::template two_to_one_adapter<t_comparator, typename ToDoPart::head>{}, ToDoPart
				>::succeed, ToDoPart
			>;

		if constexpr (next::count > 0)
		{
			return to_unique<t_comparator, Added, next>();
		}
		else
			return helpers::from_base(Added{});
	}

	/*template <auto t_callable,
						auto t_res_handler = [](auto...){}, 
						auto t_previous_res = nulltv,
						typename ... Args>
	inline static consteval void for_each(Args && ... args)
	{
		//callable(type_t<T0>{});

		if constexpr (sizeof...(Ts) == 0)
			return;
		else
			type_sequence<Ts...>::template for_each<callable>();
	}
	*/
	
	// alliases
	using this_t = type_sequence<Ts...>;


	template <auto t_checker, c_type_sequence ToCheck = base>
	using check_result = extract_type<helpers::template check_each<t_checker, ToCheck>()>;

	// using reverse = extract_type<helpers::from_base(typename base::reverse{})>;

	template <typename ToAddTypeSeq>
	using append_to_front = extract_type<
		helpers::append_to_front_impl(ToAddTypeSeq{})>;
	template <typename ToAddTypeSeq>
	using append = extract_type<
		helpers::append_impl(ToAddTypeSeq{})>;

	using shift = extract_type<helpers::from_base(typename base::template sub_sequence<1>{})>;

	template <typename ...AddTs>
	using append_types_to_front = extract_type<helpers::append_to_front_impl(type_sequence<AddTs...>{})>;

	template <typename ...AddTs>
	using append_types =  extract_type<helpers::append_impl(type_sequence<AddTs...>{})>;

	template <auto t_convertor, template <auto ...> class ToBeGiven>
	using converted_values_passed_type = ToBeGiven<
				static_call<t_convertor, Ts>()...>;

	template <auto t_convertor, template <typename ...> class ToBeGiven>
	using converted_passed_type = ToBeGiven<
				extract_type<static_call<t_convertor, Ts>()>...>;

	template <template <typename ... Types> class ToBeGiven, typename ... ExtraTs>
	using passed_type = ToBeGiven<ExtraTs..., Ts...>;

	template <std::size_t t_pos1, std::size_t ... t_poss>
	using remove_at =  extract_type<helpers::remove_at(std::index_sequence<t_pos1, t_poss...>{})>;

	template <typename Indexes>
	using remove_at_indexes =  extract_type<helpers::remove_at(Indexes{})>;

	template <typename ... ToRemoveRest>
	using remove = remove_at_indexes<typename check_result<
	typename helpers::functors::template get_all_hits<ToRemoveRest...>{}
	/*[]<typename T> -> bool{ 
		return std::is_same_v<T, ToRemove1> or
			(std::is_same_v<T, ToRemoveRest> || ...);
	}*/>::succeed>;

	template <c_type_sequence ToRemove>
	using remove_seq = typename ToRemove::template passed_type<this_t::remove>;

	template <auto t_checker>
	using succeed = extract_type<helpers::template extract_specified_impl<t_checker, true>()>;

	template <auto t_checker, typename DefT>
	using last_succeed_or = extract_type<
		helpers::template last_succeed_or_impl<t_checker, DefT>()
	>;
	
	template <auto t_checker>
	using succeed_ind = extract_type<helpers::template extract_specified_impl<t_checker, true>()>;

	template <auto t_checker>
	using failed = extract_type<helpers::template extract_specified_impl<t_checker, false>()>;

	using purified = type_sequence<
				std::remove_cvref_t<Ts>...>;

	template <auto t_sorter>
	using sorted = typename helpers::template from_base_t<
		helpers::template sort_impl<t_sorter>()
	>;

	/*template <std::size_t ...Is>
	using sub_sequence_from =  extract_type<helpers::from_base(typename base::template sub_sequence_from<Is...>{})>;

	template <std::size_t t_start = 0, std::size_t t_end = base::count, std::size_t t_step = 1>
	using sub_sequence =  extract_type<helpers::from_base(typename base::template sub_sequence<t_start, t_end, t_step>{})>;

	template <typename IndexesT>
	using sub_sequence_from_indexes = 
		extract_type<helpers::from_base(typename base::template sub_sequence_from_indexes<IndexesT>{})>;
		*/

	using unique =  extract_type<to_unique<[]<typename T1, typename T2>{ return std::is_same_v<T1, T2>; }> ()>;
	template <auto t_comparator>
	using custom_unique = extract_type<to_unique<t_comparator>()>;


	template <auto t_comparator>
	using most_satisfied = 	extract_type<helpers::template get_most<t_comparator, true>()>;

	template <auto t_comparator>
	using most_failed = 		extract_type<helpers::template get_most<t_comparator, false>()>;


	template <auto t_transf_fn>
	using transform =  extract_type<helpers::template transform<t_transf_fn>()>;


	// remove_seq <types/seq>
	// contains_some_of <types>
	// contains <types>
	// contains_seq <types/seq>
	// count <types>
	// count_seq <types>
	//
	
	using absorbed = transform<[]<typename T>{
		if constexpr (c_empty_type_sequence<T>)
			return ignore;
		else if constexpr (c_type_sequence<T>)
		{
			return T::template passed_value<[]<typename ... Us>{
				return change_to<Us...>;
			}>;
		}
	}>;


	template <typename ... T>
	inline static constexpr std::size_t count_of = extract_type<
		helpers::to_view(succeed<same_as<T...>>{})>::count;

	template <auto t_inheritance_transformer>
	using inheritance_base = inheritance_base<t_inheritance_transformer, Ts...>;

	template <auto t_checker>
	using remove_types_which = failed<t_checker>;

	template <auto t_checker>
	using remove_types_which_not = succeed<t_checker>;

	inline static constexpr bool is_unique = std::is_same_v<this_t, typename this_t::unique>;

	template <typename ... CTs>
	inline static constexpr bool contains_some_of = std::bool_constant<
		check_result<[]<typename Target>
			{ return  
				(std::is_same_v<Target, CTs> || ...);
			}>::has_succeed::get()>::value;

	template <typename ... CTs>
	inline static constexpr bool contains =
		(check_result<[]<typename T>{ return std::is_same_v<CTs, T>; }>::has_succeed::get() && ...);

	template <c_type_sequence CTs>
	inline static constexpr bool contains_seq = CTs::template passed_value<
	[]<typename tT1, typename ...tTs> { return contains<tT1, tTs...>;} >;

	template <auto t_checker>
	inline static constexpr bool contains_such_as = succeed<t_checker>::count != 0;

	template <typename T>
	inline static constexpr std::size_t index_of_last = check_result<same_as<T>>::template get_last<>;

	// constructors 
	constexpr type_sequence() {}
	constexpr type_sequence(auto) {}

};

//>;

template <template <typename ...> class HolderT, typename ... Ts>
type_sequence(HolderT<Ts...>) -> type_sequence<Ts...>;
template <template <typename ...> class HolderT, typename ... Ts>
type_sequence(type_t<HolderT<Ts...>>) -> type_sequence<Ts...>;

template <template <auto ...> class HolderT, auto ... t_vals>
type_sequence(HolderT<t_vals...>) -> type_sequence<value_holder<t_vals...>>;
template <template <auto ...> class HolderT, auto ... t_vals>
type_sequence(type_t<HolderT<t_vals...>>) -> type_sequence<value_holder<t_vals...>>;



template <std::size_t ...Is>
type_sequence(std::index_sequence<Is...>) -> type_sequence<value_holder<Is>...>;

template <typename MaybeTypesSeqT>
concept type_sequence_c = requires (MaybeTypesSeqT mts)
{
	requires is_type_sequence_v<MaybeTypesSeqT>;
};

template <typename MaybeTake, typename Target>
//concept c_do_lambda_take = requires (MaybeTake mt) { static_call<mt, Target>()(); };
concept c_do_lambda_take = requires (MaybeTake mt) { mt.template operator()<Target>(); };

template <typename ...Ts>
using ts_head = typename type_sequence<Ts...>::head;

template <type_sequence_c TST>
using tst_head = typename TST::head;

template <type_sequence_c auto t_tsv>
using tsv_head = typename decltype(t_tsv)::head;

template <typename ...Ts>
using ts_tail = typename type_sequence<Ts...>::tail;

template <type_sequence_c TST>
using tst_tail = typename TST::tail;

template <type_sequence_c auto t_tsv>
using tsv_tail = typename decltype(t_tsv)::tail;

template <unsigned long t_n, typename ...Ts>
using ts_at = typename type_sequence<Ts...>::template at<t_n>;

template <unsigned int t_n, type_sequence_c TST>
using tst_at = typename TST::template at<t_n>;

template <unsigned long t_n, type_sequence_c auto t_tsv>
using tsv_at = typename decltype(t_tsv)::template at<t_n>;


template <template <typename EmbT> class WithEmbT, typename EmbT>
inline consteval bool has_embeded_fn(type_t<WithEmbT<EmbT>>)
{
	return true;
}

inline consteval bool has_embeded_fn(auto)
{
	return false;
}

template <typename MaybeHasEmbT>
concept has_embeded = requires()
{
	requires has_embeded_fn(type_t<MaybeHasEmbT>{});
};



template <unsigned int t_n = 1, template <typename EmbT> class WithEmbT, typename EmbT>
consteval type_t<EmbT> get_n_embeded_type(type_t<WithEmbT<EmbT>>)
{
	if constexpr (t_n == 1 or t_n == 0)
		return {};
	else 
	{
		static_assert(has_embeded<EmbT>, "get_n_embeded_type: n is to large (no so much embeded types)");
		return get_n_embeded_type<t_n - 1>(type_t<EmbT>{});
	}
}

template <typename MaybeHasNEmbT, unsigned int t_n>
concept has_n_embeded = requires()
{
	get_n_embeded_type<t_n>(type_t<MaybeHasNEmbT>{});
};

template <c_type auto t_with_emb, unsigned int t_n = 1>
	requires has_n_embeded<extract_type<t_with_emb>, t_n>
using extract_n_embeded = extract_type<get_n_embeded_type<t_n>(t_with_emb)>;

struct partially_specified
{
	template <template <auto ...> class Target, auto ... t_to_pass>
	struct values_values
	{
		template <auto ... t_rest>
		using complete_type = Target<t_to_pass..., t_rest...>;
	};

	template <template <auto, typename ...> class Target, auto t_to_pass>
	struct value_types
	{
		template <typename ... Rest>
		using complete_type = Target<t_to_pass, Rest...>;
	};

	template <template <typename, auto ...> class Target, typename ToPass>
	struct type_values
	{
		template <auto ... t_rest>
		using complete_type = Target<ToPass, t_rest...>;
	};

	template <template <typename ...> class Target, typename ... ToPass>
	struct types_types
	{
		template <typename ... Rest>
		using complete_type = Target<ToPass..., Rest...>;
	};
};

template <typename ... ArgsTs>
inline consteval auto make_t_type_sequence() noexcept
{
	if constexpr (sizeof...(ArgsTs))
		return type<type_sequence<ArgsTs...>>;
	else
		return type<empty_type_sequence>;
}

template <typename ... ArgsTs>
using to_type_sequence = extract_type<make_t_type_sequence<ArgsTs...>()>;

template <typename T, typename TypeSeq>
concept c_contained_by_seq = TypeSeq::template contains<T>;

} // inline namespace meta
} // namespace pspp

#endif // PSPP_TYPE_SEQUENCE_HPP

