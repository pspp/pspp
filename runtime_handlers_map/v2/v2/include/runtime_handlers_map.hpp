#ifndef PSPP_RUNTIME_HANDLERS_MAP_HPP
#define PSPP_RUNTIME_HANDLERS_MAP_HPP

#include <array>
#include <pspp/unsized_array.hpp>
#include <pspp/compile_time_messages.hpp>

namespace pspp
{

namespace detail
{ 

struct default_caller_t {};
inline constexpr default_caller_t default_caller{};

} // namespace detail

template <auto t_caller>
struct handler_holder : value_holder<t_caller> {};

template <auto t_default, auto ... t_handlers>
struct handlers_set_holder 
{
	using handlers = value_sequence<t_default, t_handlers...>;

	static_val handler = []<c_value_holder IndexHolder, typename ... ArgsT>(ArgsT && ... args)
	{
		return static_call<
			handlers{} | atv<IndexHolder::value>
			>(std::forward<ArgsT>(args)...);
	};
};


template <auto t_caller>
consteval auto handler() noexcept
{
	return handler_holder<t_caller>{};
}

template <template <typename> class CallerT>
consteval auto handler() noexcept
{
	return handler_holder<[]<typename T, typename ... ArgsT>(ArgsT && ... args)
		requires (
				requires {CallerT<T>{}(std::forward<ArgsT>(args)...);}
				or
				requires {CallerT<T>{}();}
			)
			{ 
				if constexpr (requires {CallerT<T>{}(std::forward<ArgsT>(args)...);})
					return CallerT<T>{}(std::forward<ArgsT>(args)...); 
				else if constexpr (requires {CallerT<T>{}();})
					return CallerT<T>{}();
				else static_assert((... + sizeof(ArgsT)) < 0, "caller can not be called");
			}>{};
}

template <auto t_default_handler, auto ... t_handlers>
consteval auto handler_from_static() noexcept
{
}


namespace detail
{
template <
					auto t_caller = detail::default_caller,
					typename TypesToPass = type_sequence<>,
					typename DefaultT = nullt
				>
struct runtime_handlers_map_info
{
	using default_type = DefaultT;
	using types_to_pass = TypesToPass;
	static_val caller = t_caller;
};

} // namespace detail

template <typename T>
concept c_runtime_handlers_map_info = c_complex_vttarget<T, detail::runtime_handlers_map_info>;

using default_runtime_handlers_map_info = detail::runtime_handlers_map_info<>;


template <typename Key, c_runtime_handlers_map_info InfoIndex>
struct runtime_handlers_map
{
	using caller_type = clean_type<InfoIndex::caller>;
	static_val caller = InfoIndex::caller;
	using default_type = typename InfoIndex::default_type;
	using types_to_pass = typename InfoIndex::types_to_pass;

	constexpr_array<Key> m_runtime_values;


	consteval runtime_handlers_map(c_template_array auto data) noexcept:
		m_runtime_values{data} {}

	consteval runtime_handlers_map(c_template_array auto data, handler_holder<caller>) noexcept:
		m_runtime_values{data} {}
	
	consteval runtime_handlers_map(c_template_array auto data, handler_holder<caller>, c_type_sequence auto) noexcept:
		m_runtime_values{data} {}

	consteval runtime_handlers_map(c_template_array auto data, c_type_sequence auto) noexcept:
		m_runtime_values{data} {}

	consteval runtime_handlers_map(c_template_array auto data, c_type auto) noexcept:
		m_runtime_values{data} {}

	consteval runtime_handlers_map(c_template_array auto data, c_type auto, handler_holder<caller>) noexcept:
		m_runtime_values{data} {}
	
	consteval runtime_handlers_map(c_template_array auto data, c_type auto, handler_holder<caller>, c_type_sequence auto) noexcept:
		m_runtime_values{data} {}

	consteval runtime_handlers_map(c_template_array auto data, c_type auto, c_type_sequence auto) noexcept:
		m_runtime_values{data} {}

	template <auto t_default, auto ... t_handlers>
	consteval runtime_handlers_map(c_template_array auto data, handlers_set_holder<t_default, t_handlers...>) noexcept:
		m_runtime_values{data} {}

	template <auto t_one_time_caller, typename KeyUR, typename ... CallerArgs>
	inline constexpr auto call_default(KeyUR && check, CallerArgs && ... caller_args) const noexcept
	{
		if constexpr (requires {
				static_call<t_one_time_caller, default_type>(std::forward<KeyUR>(check));})
			return static_call<t_one_time_caller, default_type>(std::forward<KeyUR>(check));
		else if constexpr (requires {
				static_call<t_one_time_caller, default_type>(std::forward<KeyUR>(check),
					std::forward<CallerArgs>(caller_args)...); })
			return static_call<t_one_time_caller, default_type>(std::forward<KeyUR>(check), std::forward<CallerArgs>(caller_args)...);
		else
			return static_call<t_one_time_caller, default_type>(std::forward<CallerArgs>(caller_args)...);
	}

/*	template <auto t_one_time_caller, auto t_check, std::size_t t_invocation_count = 0, auto ... t_caller_args>
	inline consteval auto call() const noexcept
	{
		constexpr auto begin = m_runtime_values.begin();
		constexpr auto tmp = m_runtime_values | std::find(t_check);

		if constexpr (tmp == m_runtime_values.end())
			return call_default<t_one_time_caller>(t_check, t_caller_args...);
		else
			return static_call<t_one_time_caller, typename TypesToPass::template at<tmp - begin>
	}
	*/

	template <auto t_one_time_caller, std::size_t t_invocation_count = 0, typename KeyUR, typename ... CallerArgs>
	inline constexpr auto call(KeyUR && check, CallerArgs && ... caller_args) const noexcept
	{
		if constexpr (t_invocation_count == types_to_pass::size)
			return call_default<t_one_time_caller>(std::forward<KeyUR>(check), std::forward<CallerArgs>(caller_args)...);
		else
		{
			if (m_runtime_values[t_invocation_count] == std::forward<KeyUR>(check))
			{
				using rest = extract_type<types_to_pass{} | at<t_invocation_count>>;
				return static_call<t_one_time_caller, rest>(std::forward<CallerArgs>(caller_args)...);
			}
			else
				return call<t_one_time_caller, t_invocation_count + 1>(std::forward<KeyUR>(check), std::forward<CallerArgs>(caller_args)...);
		}
	}

	template <template <typename> class CallerT, std::size_t t_invocation_count = 0, typename KeyUR, typename ... CallerArgs>
	pspp_always_inline constexpr auto call(KeyUR && check, CallerArgs && ... caller_args) const noexcept
	{
		return call<handler<CallerT>(), t_invocation_count>
			(std::forward<KeyUR>(check), std::forward<CallerArgs>(caller_args)...);
	}

	template <std::size_t t_invocation_count = 0, typename KeyUR, typename ... CallerArgs>
	inline constexpr auto call(KeyUR && check, CallerArgs && ... caller_args) const noexcept
		requires (not std::same_as<caller_type, detail::default_caller_t>)
	{
		return call<caller, t_invocation_count>(std::forward<KeyUR>(check), std::forward<CallerArgs>(caller_args)...);
	}
};

runtime_handlers_map(c_template_array auto vals) -> 
	runtime_handlers_map<typename decltype(vals)::data_type, 
		detail::runtime_handlers_map_info<
			detail::default_caller, typename decltype(vals)::holders_seq, nullt>
	>;

runtime_handlers_map(c_template_array auto vals, c_type_sequence auto types_to_pass) ->
	runtime_handlers_map<typename decltype(vals)::data_type, 
	detail::runtime_handlers_map_info<
			detail::default_caller, decltype(types_to_pass), nullt>
	>;

template <auto t_caller>
runtime_handlers_map(c_template_array auto vals, handler_holder<t_caller>) -> 
	runtime_handlers_map<typename decltype(vals)::data_type,
	detail::runtime_handlers_map_info<
			t_caller, typename decltype(vals)::holders_seq, nullt>
	>;

template <auto t_caller>
runtime_handlers_map(c_template_array auto vals, handler_holder<t_caller>, c_type_sequence auto types_to_pass) ->
	runtime_handlers_map<typename decltype(vals)::data_type,
	detail::runtime_handlers_map_info<
				t_caller, std::remove_cvref_t<decltype(types_to_pass)>, nullt>
	>;

template <typename T>
runtime_handlers_map(c_template_array auto vals, type_t<T>) -> 
	runtime_handlers_map<typename decltype(vals)::data_type,
	detail::runtime_handlers_map_info<
			detail::default_caller, typename decltype(vals)::holders_seq, T>
	>;

template <typename T>
runtime_handlers_map(c_template_array auto vals, type_t<T>, c_type_sequence auto types_to_pass) ->
	runtime_handlers_map<typename decltype(vals)::data_type,
	detail::runtime_handlers_map_info<
			detail::default_caller, std::remove_cvref_t<decltype(types_to_pass)>, T>
	>;

template <auto t_caller, typename T>
runtime_handlers_map(c_template_array auto vals, type_t<T>, handler_holder<t_caller>) -> 
	runtime_handlers_map<typename decltype(vals)::data_type,
	detail::runtime_handlers_map_info<
				t_caller, typename decltype(vals)::holders_seq, T>
	>;

template <auto t_caller, typename T>
runtime_handlers_map(c_template_array auto vals, type_t<T>, handler_holder<t_caller>, c_type_sequence auto types_to_pass) ->
	runtime_handlers_map<typename decltype(vals)::data_type,
	detail::runtime_handlers_map_info<
				t_caller, std::remove_cvref_t<decltype(types_to_pass)>, T>
	>;


template <auto t_default, auto ... t_handlers>
runtime_handlers_map(c_template_array auto vals, handlers_set_holder<t_default, t_handlers...>) ->
	runtime_handlers_map<typename decltype(vals)::data_type, 
		detail::runtime_handlers_map_info<
			handlers_set_holder<t_default, t_handlers...>::handler, 
			decltype(type_sequence{index_sequence_range<1, decltype(vals)::holders_seq::size>{}}),
			value_holder<0ul>
		>
	>;

} // namespace pspp


#endif // PSPP_RUNTIME_HANDLERS_MAP_HPP
