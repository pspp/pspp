
#ifndef PSPP_IO_HPP
#define PSPP_IO_HPP

#include <pspp/iterator.hpp>
#include <pspp/useful/ostream_overloads.hpp>
#include <sstream>
#include <variant>
#include <format>

namespace pspp
{

inline constexpr struct print_t
{
  template <typename ... ArgTs>
  static constexpr void operator()(std::format_string<ArgTs...> format_str, ArgTs && ... args) noexcept
  {
    std::string const tmp_ =
      std::format(
        format_str,
        std::forward<ArgTs>(args)...
      );

    for (char const c : tmp_)
      std::putchar(c);
  }
} print {};

inline constexpr struct printi_t
{
  template <typename FirstArgT, typename ... ArgTs>
  static constexpr void operator()(FirstArgT && first, ArgTs && ... args) noexcept
  {
    static constexpr ::pspp::constr simple_format =
      (
        ::pspp::constr{"{}"} + ... +
        []{
          static_assert(0 < sizeof(ArgTs)); // NOLINT(*-sizeof-expression)
          return ::pspp::constr{" {}"};
        }()
      );

    print(
      std::format_string<FirstArgT, ArgTs...>{simple_format.strv()},
      std::forward<FirstArgT>(first), std::forward<ArgTs>(args)...
    );
  }
} printi {};


inline constexpr struct println_t
{
  template <typename ... ArgTs>
  static constexpr void operator()(std::format_string<ArgTs...> format_str, ArgTs && ... args) noexcept
  {
    std::string const tmp_ =
      std::format(
        format_str,
        std::forward<ArgTs>(args)...
      );

    if not consteval {
      std::puts(tmp_.c_str() );
    }
  }
} println {};

inline constexpr struct printiln_t
{
  static constexpr void operator()() noexcept
  { println("\n"); }

  template <typename FirstArgT, typename ... ArgTs>
  static constexpr void operator()(FirstArgT && first, ArgTs && ... args) noexcept
  {
    static constexpr ::pspp::constr simple_format =
      (
        ::pspp::constr{"{}"} + ... +
        []{
          static_assert(0 < sizeof(ArgTs)); // NOLINT(*-sizeof-expression)
          return ::pspp::constr{" {}"};
        }()
      );

    println(
      std::format_string<FirstArgT, ArgTs...>{simple_format.strv()},
      std::forward<FirstArgT>(first), std::forward<ArgTs>(args)...
    );
  }
} printiln {};

template <bool t_log>
struct log_t
{
  template <typename ... ArgTs>
  static constexpr void operator()(std::format_string<ArgTs...> format_str, ArgTs && ... args) noexcept
  {
    if constexpr (t_log) print(format_str, std::forward<ArgTs>(args)...);
  }
};

template <bool t_log>
struct logi_t
{
  template <typename FirstArgT, typename ... ArgTs>
  static constexpr void operator()(ArgTs && ... args) noexcept
  {
    if constexpr (t_log) printi(std::forward<ArgTs>(args)...);
  }
};

template <bool t_log>
struct logln_t
{
  template <typename ... ArgTs>
  static constexpr void operator()(std::format_string<ArgTs...> format_str, ArgTs && ... args) noexcept
  {
    if constexpr (t_log) println(format_str, std::forward<ArgTs>(args)...);
  }
};

template <bool t_log>
struct logiln_t
{
  template <typename ... ArgTs>
  static constexpr void operator()(ArgTs && ... args) noexcept
  {
    if constexpr (t_log) printiln(std::forward<ArgTs>(args)...);
  }
};

template <bool t_log>
inline constexpr auto log    = log_t<t_log>{};
template <bool t_log>
inline constexpr auto logi   = logi_t<t_log>{};
template <bool t_log>
inline constexpr auto logln  = logln_t<t_log>{};
template <bool t_log>
inline constexpr auto logiln = logiln_t<t_log>{};

}

namespace pspp
{
template <typename DataT, typename ... Options>
std::ostream& operator<<(std::ostream& out, ::pspp::iterator<DataT, Options...> iter) noexcept
{
  using itert = ::pspp::iterator<DataT, Options...>;

  if constexpr (itert::options::print::addres_only)
  {
    out << iter.m_ptr;
  }
  else if constexpr (itert::options::print::both or itert::options::print::data_only)
  {
    if constexpr (requires {out << *iter.m_ptr;})
    {
      if (iter.m_ptr == nullptr)
        out << "`nullptr`";
      else
        out << *iter.m_ptr;
    }
    else
    {
      static_assert(itert::options::print::can_print_message_for_unprintable,
                    "operator<<: trying to print unprintable object; printing message instead is forbiden by type specification");

      out << itert::options::print::message_for_unprintable::strv;
    }
    if constexpr (itert::options::print::both)
      out << " at: " << iter.m_ptr;
  }

  return out;
}

}

namespace pspp
{
template <typename T>
struct simple_formatter
{
  template <typename ParseContextT>
  constexpr ParseContextT::iterator parse(ParseContextT & ctx) const
  {
    auto it = ctx.begin();
    while (ctx.end() != it and *it != '}')
      ++it;

    return it;
  }

  template <typename InputT, typename FmtContextT>
  constexpr std::remove_cvref_t<FmtContextT>::iterator
  format(InputT && data, FmtContextT && context) const
  {
    return std::ranges::copy(
      [data=std::forward<InputT>(data)] mutable
      {
        std::ostringstream out;
        out << std::forward<InputT>(data);

        return out;
      }().str(), context.out()).out;
  }
};

}
#define FMT_EXPAND(...) __VA_ARGS__

#define _def_sf(tmpl, ...)  template <FMT_EXPAND tmpl >\
struct std::formatter<__VA_ARGS__> : pspp::simple_formatter<__VA_ARGS__> {};



_def_sf((pspp::c_with_known_struct_info KnownT), KnownT)
_def_sf((typename DataT, typename ... OptionTs), pspp::iterator<DataT, OptionTs...>)
_def_sf((bool t_is_noexcept, typename ResT, typename CallerT, pspp::type_sequence t_args),
        pspp::advanced_function_info<ResT, t_args, t_is_noexcept, CallerT>)
_def_sf((bool t_is_noexcept, typename ResT, typename CallerT, pspp::type_sequence t_args),
        pspp::function_info<ResT, t_args, t_is_noexcept, CallerT>)
_def_sf((pspp::c_range RangeT), RangeT)
_def_sf((std::size_t t_n), pspp::constr<t_n>)
_def_sf((typename T), pspp::identification_char_t<T>)
_def_sf((char ... t_chars), pspp::char_sequence<t_chars...>)
_def_sf((typename FirstT, typename SecondT), std::pair<FirstT, SecondT>)
_def_sf((typename ... Ts), std::tuple<Ts...>)
_def_sf((typename DataT), std::optional<DataT>)
_def_sf((typename ... Ts), std::variant<Ts...>)
_def_sf((pspp::c_range DataT, typename ... Options), pspp::with_print_opts_t<DataT, Options...>);


#undef _def_sf
#endif // PSPP_IO_HPP
