#ifndef PSPP_USEFUL_LAMBDA_OVERHEAD_HPP
#define PSPP_USEFUL_LAMBDA_OVERHEAD_HPP

namespace pspp
{
inline namespace usf
{

template <auto ... lambdas>
struct overloaded_lambda : decltype(lambdas)...
{
	using decltype(lambdas)::operator()...;
	template <auto ... other_lambdas>
	consteval overloaded_lambda<lambdas..., other_lambdas...> 
		operator+(overloaded_lambda<other_lambdas...>) const noexcept
	{ return {}; }

	consteval auto operator+(auto lambda) const noexcept
	{ return overloaded_lambda<lambdas..., lambda>{}; }

	template <template <auto...> class HolderT, auto ... other_lambdas>
	consteval auto operator+(HolderT<other_lambdas...>) const noexcept
	{ return overloaded_lambda<lambdas..., other_lambdas...>{}; }
};

} // namespace usf
} // pspp

#endif // PSPP_USEFUL_LAMBDA_OVERHEAD_HPP
