#ifndef PSPP_PCOMP_USEFUL_SEQUENCE_HPP
#define PSPP_PCOMP_USEFUL_SEQUENCE_HPP

#include <pspp/useful/concepts.hpp>
#include <pspp/useful/static.hpp>
#include <pspp/utility/utility.hpp>
#include <pspp/nullt.hpp>

//#include <algorithm>

namespace pspp
{
inline namespace usf
{


template <std::size_t pos, auto first, auto ...rest>
consteval auto get_ell_at()
{
	static_assert(pos < sizeof...(rest) + 1, "get_ell_at: pos must be lower then data args count");

	if constexpr (pos > 0)
		return get_ell_at<pos - 1, rest...>();

	if constexpr (pos == 0)
		return first;
}

template <auto first, auto ...rest>
consteval auto get_first_from_seq()
{
	return first;
}

template <auto t_checker, auto t_default_val, auto t_first, auto ... t_vals>
consteval auto get_last_satisfied_check_value_helper()
{
	using first_t = std::remove_cvref_t<decltype(t_first)>;
	if constexpr (
			t_checker.template operator()<first_t>()
		)
	{
		if constexpr (sizeof...(t_vals) == 0)
			return t_first;
		else
		{
			constexpr auto maybe_last = get_last_satisfied_check_value_helper<t_checker, nullt, t_vals...>();
			if constexpr (not std::is_same_v<
					nullt,
					std::remove_cvref_t<decltype(maybe_last)>
				>
			)
			{
				return maybe_last;
			}
			else
			{
				return t_first;
			}
		}
	}
	else
		if constexpr (sizeof...(t_vals) == 0)
			return t_default_val;
		else 
			return get_last_satisfied_check_value_helper<t_checker, t_default_val, t_vals...>();

}

template <auto t_checker, auto t_default_val, auto ... t_vals>
inline consteval auto get_last_satisfied_check_value()
{
	if constexpr (sizeof...(t_vals) == 0)
		return t_default_val;
	else
		return get_last_satisfied_check_value_helper<t_checker, t_default_val, t_vals...>();
}

template <typename TargetT, auto t_default_val, auto ... t_vals>
inline consteval auto get_last_specified_type_value()
{
	return get_last_satisfied_check_value<
		[]<typename ToBeChecked>{ return std::is_same_v<ToBeChecked, TargetT>; },
		t_default_val, t_vals...>();
}

inline constexpr auto get_first_from_seq(auto first, auto ... rest) -> decltype(first)
{
	return first;
}

template <std::size_t pos, typename FirstT, typename ... RestTs>
[[pspp_attr_always_inline]] inline constexpr auto && get_ell_at(FirstT && first, RestTs && ...rest) noexcept
{
	static_assert(pos < sizeof...(rest) + 1, "get_ell_at: pos must be lower then data args count");

	if constexpr (pos > 0)
		return get_ell_at<pos - 1>(std::forward<RestTs>(rest)...);
	else 
		return first;
}

template <std::size_t t_pos, template <auto ...> class ValsHolder, auto ... vals>
[[pspp_attr_always_inline]] inline consteval auto get_ell_at_in_seq_holder(ValsHolder<vals...>) noexcept
{
	return get_ell_at<t_pos, vals...>();
}

template <std::size_t t_pos, template <typename, auto ...> class ValsHolder, typename T, T ... vals>
[[pspp_attr_always_inline]] inline consteval auto get_ell_at_in_seq_holder(ValsHolder<T, vals...>) noexcept
{
	return get_ell_at<t_pos, vals...>();
}



[[pspp_attr_always_inline]] inline constexpr auto get_last_from_seq(auto first, auto ...rest) noexcept
{
	return get_ell_at<sizeof...(rest)>(first, rest...);
}

template <auto first, auto ...rest>
inline consteval auto get_last_from_seq() noexcept
{
	return get_ell_at<sizeof...(rest), first, rest...>();
}

template <template <auto ...> class ValsHolder, auto ... vals>
[[pspp_attr_always_inline]] inline consteval auto get_last_from_seq_holder(ValsHolder<vals...>) noexcept
{
	return get_last_from_seq<vals...>();
}

template <template <typename, auto ...> class ValsHolder, typename T, T ... vals>
[[pspp_attr_always_inline]] inline consteval auto get_last_from_seq_holder(ValsHolder<T, vals...>) noexcept
{
	return get_last_from_seq<vals...>();
}

template <typename First, typename ...Rest>
using head_t = First;

template <concepts::summable First, concepts::summable ...Rest>
inline constexpr First sum_of_seq(First && _first, Rest && ..._rest)
{
	return _first + (... + _rest);
}

inline constexpr void static_apply_for_each(auto func, auto & arg, auto & ...rest_args)
{
	func(arg);
	(func(rest_args), ...);
}


inline constexpr void static_apply_for_each_pair(auto func, auto & arg1, auto & arg2, auto & ...rest_args)
{
	static_assert(sizeof...(rest_args) % 2 == 0, "static_apply_for_each_pair: number of args must be even");

	func(arg1, arg2);

	if constexpr(sizeof...(rest_args))
		static_apply_for_each_pair(func, rest_args...);
}

constexpr void static_apply_for_each_two(auto func, auto & arg1, auto & arg2, auto & ...rest_args)
{
	func(arg1, arg2);

	if constexpr(sizeof ...(rest_args))
		static_apply_for_each_two(func, arg2, rest_args...);
}


void apply_for_each_two(auto func, auto & arg1, auto & arg2, auto & ...rest_args)
{
	func(arg1, arg2);

	if constexpr(sizeof ...(rest_args))
		apply_for_each_two(func, arg2, rest_args...);
}

template <auto t_start, auto t_end, auto t_step>
struct make_index_sequence_range_impl
{
	//static_assert(t_start <= t_end, "make_index_sequence_range_impl: t_start must be less than or equal to t_end");
	static constexpr auto longer = conditional_v<(t_start > t_end), t_start, t_end>;
	static constexpr auto shorter = t_start + t_end - longer;
	static constexpr auto step_abs = conditional_v<(t_step > 0), t_step, -t_step>;

	template <std::size_t ...Is>
	static consteval auto generate_sequence(std::index_sequence<Is...>) noexcept
	{
		return std::index_sequence<(t_start + Is * t_step)...>{};
	}

	using type = decltype(generate_sequence(std::make_index_sequence<(longer - shorter + step_abs - 1) / step_abs>{}));
};



template <auto t_start, auto t_end, auto t_step = 1>
using index_sequence_range = typename make_index_sequence_range_impl<t_start, t_end, t_step>::type;

template <std::size_t t_to_add, std::size_t ...Is>
[[nodiscard]] inline consteval std::index_sequence<Is..., t_to_add> append_index(std::index_sequence<Is...>) noexcept
{
	return {};
}

template <std::size_t t_to_add, std::size_t ...Is>
[[nodiscard]] inline consteval std::index_sequence<t_to_add, Is...> append_index_to_front(std::index_sequence<Is...>) noexcept
{
	return {};
}


template <std::array t_vals, std::size_t ... t_is>
inline consteval auto array_to_index_sequence(std::index_sequence<t_is...>)
{
	return std::index_sequence<t_vals[t_is]...>{};
}

template <std::size_t ... t_is>
inline consteval auto index_sequence_to_array(std::index_sequence<t_is...>)
{
	return std::array{t_is...};
}

} // namespace usf
} // namespace pspp


#endif // PSPP_PCOMP_USEFUL_SEQUENCE_HPP
