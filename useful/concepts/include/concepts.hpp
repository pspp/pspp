#ifndef PSPP_USEFUL_CONCEPTS_HPP
#define PSPP_USEFUL_CONCEPTS_HPP

#include <concepts>

namespace pspp
{

namespace detail
{
	template <auto data>
	consteval void _get_some_structural_data()
	{}
} // namespace detail

inline namespace concepts
{
	template <typename T>
	concept summable = requires(T const & t1, T const & t2)
	{
		t1 + t2;
		requires not std::same_as<bool, T>;
	};

	template <typename T>
	concept not_void = requires (T)
	{
		requires not std::same_as<T, void>;
	};

	template <typename T> concept has_begin_end = requires (T & t) {
		t.begin();
		t.end();
	};

	template <typename T>
	concept has_size_method = requires (T const & t)
	{
		t.size();
	};

	template <typename T>
	concept default_constructalbe = requires(T)
	{
		T{};
	};
	template <typename T>
	concept default_constructable = requires(T)
	{
		T{};
	};

	template <typename Func, auto ...args>
	concept template_args_lambda = requires(Func f)
	{
		f.template operator()<args...>();
	};

	template <typename Func, typename T, auto ...args>
	concept return_type_is_same = requires(Func f)
	{
		requires []<auto ...lambda_args>(){
			if constexpr (template_args_lambda<Func, lambda_args...>)
			{
				return std::is_same_v<decltype(f.template operator()<lambda_args...>()), T>; 
			}
			else
				return std::is_same_v<decltype(f(lambda_args...)), T>;
		}.template operator()<args...>();
	};

} // namespace concepts

namespace detail
{
	template <typename T>
	struct is_member_function_pointer : std::false_type {};

	template <class Owner, typename ReturnType, typename ... Args>
	struct is_member_function_pointer<ReturnType(Owner::*)(Args...)> : std::true_type {};

	template <class Owner, typename ReturnType, typename ... Args>
	struct is_member_function_pointer<ReturnType(Owner::*)(Args...) const> : std::true_type {};

	template <class Owner, typename ReturnType, typename ... Args>
	struct is_member_function_pointer<ReturnType(Owner::*)(Args...) noexcept> : std::true_type {};

	template <class Owner, typename ReturnType, typename ... Args>
	struct is_member_function_pointer<ReturnType(Owner::*)(Args...) const noexcept> : std::true_type {};
} // namespace detail

inline namespace concepts
{

template <typename T>
concept c_member_function_pointer = detail::is_member_function_pointer<std::remove_cvref_t<T>>::value;


} // namespace concepts
} // namespace pspp

#endif // PSPP_USEFUL_CONCEPTS_HPP
