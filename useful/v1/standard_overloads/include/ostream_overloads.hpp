#ifndef PSPP_USEFUL_OSTREAM_OVERLOADS_HPP
#define PSPP_USEFUL_OSTREAM_OVERLOADS_HPP

#include <iostream>
#include <concepts>
#include <pspp/type_sequence.hpp>
#include <pspp/constr.hpp>
#include <pspp/struct_info.hpp>

namespace pspp
{
	template <typename T>
	struct forbid_ostream_overload : std::false_type {};
}


template <typename MaybePrintable>
concept c_printable = requires (MaybePrintable d) { std::cout << d; 
	requires not pspp::forbid_ostream_overload<std::remove_cvref_t<MaybePrintable>>::value;
};

template <typename T>
struct identification_char_t { inline static constexpr char val = '\0'; };

template <typename T>
inline constexpr auto identification_char = identification_char_t<std::remove_cvref_t<T>>{};

template <typename T>
std::ostream& operator<<(std::ostream& o, identification_char_t<T> c)
{
  if constexpr (identification_char_t<T>::val != '\0')
  	o << identification_char_t<T>::val;
  return o;
}


template <typename T>
struct is_char_arr : std::false_type {};

template <std::size_t t_n>
struct is_char_arr<char[t_n]>: std::true_type {};

template <typename T>
concept standart_printable_range = std::same_as<T, std::string> or 
	std::same_as<T, std::string_view> or
	is_char_arr<T>::value;

template <typename T>
concept c_range = std::ranges::range<T> and 
	not standart_printable_range<std::remove_cvref_t<T>> and
	not pspp::forbid_ostream_overload<std::remove_cvref_t<T>>::value;

template <char ... t_chars>
struct char_sequence {
	template <char t_new>
	using append = char_sequence<t_chars..., t_new>;

	inline constexpr bool operator==(char_sequence) const noexcept { return true; }

	template <char ... t_other_chars>
	inline constexpr bool operator==(char_sequence<t_other_chars...>) const noexcept { return false; }
};

template <pspp::constr t_str, typename Done = char_sequence<>, std::size_t t_n = 0>
inline consteval auto to_char_seq()
{
	if constexpr (t_str.size() == (t_n + 1))
		return pspp::type_t<Done>{};
	else
		return to_char_seq<
			t_str, typename Done::template append<t_str._m_val[t_n]>,
			t_n + 1>();
}

template <pspp::constr t_sep>
using sep = pspp::extract_type<to_char_seq<t_sep>()>;

inline constexpr char_sequence<',', ' '> def_sep {};

template <typename T>
struct is_char_sequence : std::false_type {};

template <char ... t_chars>
struct is_char_sequence<char_sequence<t_chars...>> : std::true_type {};

struct with_quotes_t {};

struct apply_to_all{};

using use_quotes = with_quotes_t;

template <typename DataT, typename ... Options>
struct with_print_opts_t
{
	using opts = pspp::type_sequence<Options...>;
	inline static constexpr bool quotes = opts::template contains<with_quotes_t>;
	inline static constexpr auto sep = typename pspp::type_sequence<
		char_sequence<',', ' '>>::append<
			typename opts::template succeed<[]<typename T>{ return is_char_sequence<T>::value; }>
		>::last{};

	inline static constexpr bool for_all = opts::template contains<apply_to_all>;

	DataT const & data;
};

template <typename DataT>
inline constexpr with_print_opts_t<DataT, with_quotes_t, apply_to_all> with_quotes(DataT const & data)
{
	return {data};
}

template <typename ... Options, typename DataT>
inline constexpr with_print_opts_t<DataT, Options...> print_with_opts(DataT const & data)
{
	return {data};
}

template <typename ... Options, typename DataT>
inline constexpr auto pass_opts(DataT const & data)
{
	using t = with_print_opts_t<DataT, Options...>;
	if constexpr (t::for_all)
		return with_print_opts_t<DataT, Options...>{data};
	else
		return data;
}

template <pspp::c_with_known_struct_info KnownT>
std::ostream& operator<<(std::ostream& o, KnownT && known) noexcept;
template <char ... t_chars>
inline std::ostream& operator<<(std::ostream& o, char_sequence<t_chars...> data) noexcept;
template <c_printable First, c_printable Second>
inline std::ostream& operator<<(std::ostream& o, std::pair<First, Second> const & data) noexcept;
template <c_printable ... Ts>
inline std::ostream& operator<<(std::ostream& o, std::tuple<Ts...> const &  data) noexcept;
template <c_printable ... Ts, typename ... Options>
inline std::ostream& operator<<(std::ostream& o, with_print_opts_t<std::tuple<Ts...>, Options...> const &  data) noexcept;
template <c_range DataT, typename ... Options>
inline std::ostream& operator<<(std::ostream& o, with_print_opts_t<DataT, Options...> const & data) noexcept;
template <typename DataT>
std::ostream& operator<<(std::ostream& o, std::optional<DataT> const & data) noexcept;
template <c_range DataT>
std::ostream& operator<<(std::ostream& o, DataT && data) noexcept;
template <pspp::c_with_known_struct_info KnownT>
std::ostream& operator<<(std::ostream& o, KnownT && known) noexcept;




template <c_printable DataT, typename ... Options>
inline std::ostream& operator<<(std::ostream& o, with_print_opts_t<DataT, Options...> const & data)
{
	using t = with_print_opts_t<DataT, Options...>;
	if constexpr (t::quotes)
		o << '\'' << data.data << '\'';
	else
		o << data.data;

	return o;
}

template <char ... t_chars>
inline std::ostream& operator<<(std::ostream& o, char_sequence<t_chars...> data) noexcept
{
	(std::operator<<(o, t_chars), ...);
	return o;
}

template <c_printable First, c_printable Second>
inline std::ostream& operator<<(std::ostream& o, std::pair<First, Second> const & data) noexcept
{
	o << '{';
	o << data.first;
	o << ';' << ' ';
	o << data.second;
	o << '}';
	return o;
}

template <c_printable ... Ts>
inline std::ostream& operator<<(std::ostream& o, std::tuple<Ts...> const &  data) noexcept
{
	o << '{';
	[&o, &data]<std::size_t ... t_is>(std::index_sequence<t_is...>)
	{
		(..., [&o, &data]{o << std::get<t_is>(data) << def_sep;}());
	}(std::make_index_sequence<sizeof...(Ts) - 1>());

	o << std::get<sizeof...(Ts) - 1>(data) << '}';
	return o;
}

template <c_printable ... Ts, typename ... Options>
inline std::ostream& operator<<(std::ostream& o, with_print_opts_t<std::tuple<Ts...>, Options...> const &  data) noexcept
{
	using t = with_print_opts_t<std::tuple<Ts...>, Options...>;

	o << '{';
	[&o, &data]<std::size_t ... t_is>(std::index_sequence<t_is...>)
	{
		(..., [&o, &data]{o << pass_opts<Options...>(std::get<t_is>(data.data)) << t::sep;}());
	}(std::make_index_sequence<sizeof...(Ts) - 1>());

	o << pass_opts<Options...>(std::get<sizeof...(Ts) - 1>(data.data)) << '}';
	return o;
}

template <c_range DataT, typename ... Options>
inline std::ostream& operator<<(std::ostream& o, with_print_opts_t<DataT, Options...> const & data) noexcept
{
	using t = with_print_opts_t<DataT, Options...>;

	o << identification_char<DataT> << '[';

	auto pre_end = std::end(data.data);
	pre_end--;
	for (auto iter = std::begin(data.data); iter != pre_end; iter++)
		o << pass_opts<Options...>(*iter) << t::sep;

	o << pass_opts<Options...>(*pre_end) << ']' << identification_char<DataT>;

	return o;
}

template <typename DataT>
std::ostream& operator<<(std::ostream& o, std::optional<DataT> const & data) noexcept
{
	if (data)
		o << '{' << data.value() << '}';
	else o << "{}";

	return o;
}

template <c_range DataT>
std::ostream& operator<<(std::ostream& o, DataT && data) noexcept
{
	if (std::size(data) == 0)
		o << "[]";
	else
	{
		o << identification_char<DataT> << '[';
		auto pre_end = std::end(data);
		pre_end--;

		for (auto iter = std::begin(data); iter < pre_end; iter++)
			o << *iter << def_sep;

		o << *pre_end << ']' << identification_char<DataT>;
	}
	return o;
}

namespace pspp
{

template <typename T, constr t_name>
struct with_name :
	cstr_holder<t_name>
{
	using type = T;
};

template <typename T, constr t_prefix, constr t_suffix = "">
struct surrounded
{
	using type = T;
	using preffix_holder = cstr_holder<t_prefix>;
	using suffix_holder = cstr_holder<t_suffix>;
};

template <typename T>
struct hidden : std::type_identity<T>
{};

} // namespace pspp

template <pspp::c_with_known_struct_info KnownT>
std::ostream& operator<<(std::ostream& o, KnownT && known) noexcept
{
	using pure_type = std::remove_cvref_t<decltype(known)>;
	using info = pspp::struct_info<pure_type>;
	o << '{';

	if constexpr (info::types::count == 0)
	{
		o << '}';
		return o;
	}
	else
	{
		auto const & tuple = reinterpret_cast<typename info::access_type const &>(
				std::forward<KnownT>(known));

		auto print_val = [&o, &tuple]<std::size_t t_i, char_sequence sep> [[pspp_attr_always_inline]]
			{
				using raw_types = typename info::raw_types;
				using being_checked = typename raw_types::template at<t_i>;

				if constexpr (not pspp::c_complex_target<
						being_checked,
						pspp::hidden
					>)
				{
					if constexpr (pspp::c_complex_tvtarget<
							being_checked,
							pspp::with_name
						>)
					{
						o << being_checked::strv << ':' << ' '
							<< tuple.template get<t_i>();
					}

					else if constexpr (pspp::c_complex_tvtarget<
							being_checked,
							pspp::surrounded
						>)
					{
						o << being_checked::preffix_holder::strv
							<< tuple.template get<t_i>() 
							<< being_checked::suffix_holder::strv;
					}
					else
					{
						o << tuple.template get<t_i>();
					}

					if constexpr (sep != char_sequence<>{})
						if constexpr (not pspp::c_complex_target<
							typename raw_types::template at<t_i + 1>,
							pspp::hidden
						>)
							o << sep;

				}
			};

		[&print_val]<std::size_t ... t_is>
			[[pspp_attr_always_inline]]
			(std::index_sequence<t_is...>)
		{
			(..., print_val.template operator()<t_is, char_sequence<',', ' '>{}>());
		}(std::make_index_sequence<info::types::count - 1>{});

		print_val.template operator()<info::types::count - 1, char_sequence<>{}>();

		o << '}';
		return o;
	}
}

#endif // PSPP_USEFUL_OSTREAM_OVERLOADS_HPP

