#ifndef PSPP_USEFUL_STATIC_HPP
#define PSPP_USEFUL_STATIC_HPP

#include <array>

#include <pspp/simple/string_view.hpp>
#include <pspp/useful/concepts.hpp>

namespace pspp
{
inline namespace usf
{

namespace
{

template <typename T>
concept _c_range = requires (T t){
  t.begin();
  t.size();
};

}
#define OVERSIZED_ARRAY_DEFAULT_MAX_SIZE 2 * 1024

	template <auto data>
	consteval auto const & make_static()
	{
		return data;
	}

	template <auto data>
	consteval auto const & make_static_from_ptr()
	{
		return *data;
	}

	consteval auto const & make_static(auto data)
	{
		return data;
	}

	consteval auto const & make_static_from_callable(auto callable)
	{
		return callable();
	}

	template <typename T, std::size_t max_size = OVERSIZED_ARRAY_DEFAULT_MAX_SIZE>
	struct oversized_array
	{
		std::array<T, max_size> data;
		std::size_t real_size;

		constexpr std::size_t size() const
		{
			return real_size;
		}

		static constexpr std::size_t capacity()
		{
			return max_size;
		}
	};

	template <typename T, std::size_t max_size = OVERSIZED_ARRAY_DEFAULT_MAX_SIZE,
	  _c_range RangeT, typename DefValT>
	constexpr oversized_array<T, max_size> to_oversized_array(RangeT && data, DefValT && default_value) noexcept
	{
		oversized_array<T, max_size> res;

		res.data.fill(default_value);
		std::copy(data.begin(), data.end(), res.data.begin());

		res.real_size = data.size();

		std::fill_n(res.data.begin() + res.real_size, max_size - res.real_size, default_value);

		return res;
	}

	template <pspp::concepts::default_constructalbe T,
    std::size_t max_size = OVERSIZED_ARRAY_DEFAULT_MAX_SIZE,
    _c_range RangeT>
	inline constexpr oversized_array<T, max_size> to_oversized_array(RangeT && data) noexcept
	{
		return to_oversized_array<T, max_size>(data, T{});
	}

	//main func
	template <std::size_t max_size, typename T, auto ...args, typename CallableT, typename CallableWithDefT>
	constexpr auto to_right_sized_array(CallableT && callable, CallableWithDefT && callable_with_default_value)
	{
		constexpr oversized_array<T, max_size> oversized =
      to_oversized_array<T, max_size>(callable(args...), callable_with_default_value());
		std::array<T, oversized.real_size> result;

		std::copy(oversized.data.begin(), oversized.data.begin() + oversized.real_size, result.begin());
		return result;
	}

	template <std::size_t max_size, typename T, auto ...args>
	constexpr auto to_right_sized_array(pspp::concepts::template_args_lambda<args...> auto callable, auto callable_with_default_value)
	{
		constexpr auto oversized = to_oversized_array<T, max_size>(callable.template operator()<args...>(), callable_with_default_value());
		std::array<T, oversized.real_size> result;

		std::copy(oversized.data.begin(), oversized.data.begin() + oversized.real_size, result.begin());
		return result;
	}

	//default max_size value
	template <typename T, auto ...args>
	inline constexpr auto to_right_sized_array(auto callable, T default_value) 
	{
		return to_right_sized_array<OVERSIZED_ARRAY_DEFAULT_MAX_SIZE, T, args...>(callable, default_value);
	}

	//with default contructable T
	template <std::size_t max_size, pspp::concepts::default_constructalbe T, auto ...args>
	inline constexpr auto to_right_sized_array(auto callable)
	{
		return to_right_sized_array<max_size, T, args...>(callable, [](){return T{};});
	}

	//default max_size value and with default contructable T
	template <pspp::concepts::default_constructalbe T, auto ...args>
	inline constexpr auto to_right_sized_array(auto callable)
	{
		return to_right_sized_array<OVERSIZED_ARRAY_DEFAULT_MAX_SIZE, T, args...>(callable, [](){return T{};});
	}


namespace _to_be_ignored
{
	template <typename T, std::size_t max_size = OVERSIZED_ARRAY_DEFAULT_MAX_SIZE>
	struct _oversized_array
	{
		std::array<T, max_size>data;
		std::size_t size;
	};

	template <typename SaveT, std::size_t max_size, auto ...args, _c_range RangeT>
	constexpr auto _to_oversized_array(RangeT && data)
	{
		_oversized_array<SaveT, max_size> result;
		std::copy(data.begin(), data.end(), result.data.begin());

		result.size = data.size();
		return result;
	}

	template <typename T, std::size_t max_size, auto ...args, typename CallableT>
	consteval auto _to_right_size_array(CallableT && callable)
	{
		constexpr auto oversized = _to_oversized_array<T, max_size>(callable.template operator()<args...>());
		std::array<T, oversized.size> result;

		std::copy(oversized.data.begin(), oversized.data.begin() + oversized.size, result.begin());
		return result;
	}
} // namespace _to_be_ignored


	template <std::size_t max_size, auto ...args>
	consteval auto to_string_view_with_max_size(auto callable) -> simple_string_view
	{
		constexpr auto &static_data = make_static<_to_be_ignored::_to_right_size_array<char, max_size, args...>(callable)>();
		return {static_data.begin(), static_data.end()};
	}

	/* template <std::size_t max_size, auto ...args> */
	/* consteval auto test_to_string_view(auto callable) -> std::string_view */
	/* { */
	/* 	static_assert(std::is_convertible<std::remove_cvref<decltype(callable.template operator()<args...>())>, std::string>(), "callable must return to std::string convertible result"); */

	/* 	constexpr auto &static_data = make_static(_to_be_ignored::_to_right_size_array<char, max_size, args...>(callable)); */
	/* 	return std::string_view{static_data.begin(), static_data.size()}; */
	/* } */



	template <auto ...args>
	inline consteval auto to_string_view(auto callable) -> simple_string_view
	{
		return to_string_view_with_max_size<OVERSIZED_ARRAY_DEFAULT_MAX_SIZE, args...>(callable);
	}

	template <std::size_t max_size, typename DataToSaveT, auto ...args>
	consteval auto to_static_array(auto callable )
	{
		constexpr auto &static_data = make_static<to_right_size_array<DataToSaveT, max_size, args...>(callable)>();
		return static_data;
	}

	template <typename DataToSaveT, auto ...args>
	inline consteval auto to_static_array(auto callable)
	{
		return to_static_array<OVERSIZED_ARRAY_DEFAULT_MAX_SIZE, DataToSaveT, args...>(callable);
	}

	#undef OVERSIZED_ARRAY_DEFAULT_MAX_SIZE
} // namespace usf
} // namespace pspp
#endif // PSPP_USEFUL_STATIC_HPP



