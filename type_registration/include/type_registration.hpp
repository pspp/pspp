#ifndef PSPP_TYPE_REGISTRATION_HPP
#define PSPP_TYPE_REGISTRATION_HPP

#include <pspp/type_sequence.hpp>
#include <pspp/utility/unique_type.hpp>

namespace pspp
{
namespace detail
{
template <typename IndexT = unique_type>
struct index_storage_t : std::type_identity<IndexT> 
{
#ifdef __clang__
	friend consteval auto get_index_storage_t_data(index_storage_t) noexcept;

	template <typename Data>
	struct registrator
	{
		friend consteval auto get_index_storage_t_data(index_storage_t) noexcept
		{
			return pspp::type<Data>;
		}
	};
#else
	template <std::same_as<IndexT> IndexT_>
	friend consteval auto get_index_storage_t_data(index_storage_t<IndexT_>) noexcept;

#endif
};

template <typename IndexT>
consteval bool operator==(index_storage_t<IndexT>, index_storage_t<IndexT>) noexcept
{ return true; }

template <typename IndexT, typename OtherIndex>
consteval bool operator==(index_storage_t<IndexT>, index_storage_t<OtherIndex>) noexcept
{ return false; }

#ifndef __clang__
template <typename IndexT, typename Data>
struct registrator
{
	template <std::same_as<IndexT> OtherIndex>
	friend consteval auto get_index_storage_t_data(index_storage_t<OtherIndex>) noexcept
	{
		return pspp::type<Data>;
	}
};
#endif

}

template <typename IndexT, typename Data>
consteval void register_index() noexcept
{
#ifdef __clang__
	void(typename detail::index_storage_t<IndexT>::template registrator<Data>{});
#else
	void(detail::registrator<IndexT, Data>{});
#endif
}

struct unregistered {};

template <typename TypeT, typename Description>
struct type_describer
{
	using target_type = TypeT;
	using descriptor_type = Description;
};

namespace detail
{
	template <typename IndexT>
	inline constexpr bool is_registered = requires {not std::same_as<decltype(get_index_storage_t_data(index_storage_t<IndexT>{})), unregistered>;};

	template <typename IndexT, bool t_has = is_registered<IndexT>>
	struct registered_type {
		using type = unregistered;
	};
	template <typename IndexT>
	struct registered_type<IndexT, true>
	{
		using type = extract_type<get_index_storage_t_data(index_storage_t<IndexT>{})>;
	};
};

template <typename IndexT>
using registered_type = typename detail::registered_type<IndexT>::type;

};

#endif // PSPP_TYPE_REGISTRATION_HPP
