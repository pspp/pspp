#include <pspp/custom_struct.hpp>
#include <pspp/useful/ostream_overloads.hpp>

int main()
{
	using keys = pspp::cstr_sequence<"a", "b">;
	using types = pspp::type_sequence<int, char>;

	using my_struct = pspp::custom_struct_from<keys, types>;

	my_struct my{};
	my.set<"a", "b">(123, 'k');

	std::cout << my.tget<"a", "b">() << '\n';
}

