#ifndef ADAPTOR_PIPE_CLOSURE_HPP
#define ADAPTOR_PIPE_CLOSURE_HPP

#include <pspp/operation_holder.hpp>
#include <pspp/range_adaptor_closure.hpp>

namespace pspp
{

template <typename OperationPipe, typename AdaptorT>
struct adaptor_closure_operation_pipe :
  OperationPipe
{
  [[no_unique_address]] AdaptorT _m_adaptor;

  constexpr adaptor_closure_operation_pipe() = default;

  constexpr adaptor_closure_operation_pipe(
    OperationPipe pipe,
    AdaptorT adaptor
  ) :
    OperationPipe{pipe},
    _m_adaptor{adaptor}
  {}
};

using adaptor_pipe_closure_t =
  operation_pipe_creator<
    operation_holder,
    operation_pipe_preprocessors
      <
        [](auto && self, auto && value)
        {
          return pspp_fwd(value) | pspp_fwd(self)._m_adaptor;
        }
      >{},
    []<typename OperationHolder>(auto && self, auto && value)
    {
      return adaptor_closure_operation_pipe
        {
          OperationHolder{pspp_fwd(value)},
          pspp_fwd(self)
        };
    }
  >;


struct adaptor_pipe_closure :
  adaptor_pipe_closure_t,
  range_adaptor_closure
{};

} // namespace pspp

#endif // ADAPTOR_PIPE_CLOSURE_HPP