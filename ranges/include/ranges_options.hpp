#ifndef PSPP_RANGES_OPTIONS_HPP
#define PSPP_RANGES_OPTIONS_HPP

namespace pspp::ranges_options
{
	struct view {};
	struct reverse {};
} // namespace pspp::ranges_options

#endif // PSPP_RANGES_OPTIONS_HPP
