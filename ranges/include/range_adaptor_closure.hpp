#ifndef PSPP_RANGE_ADAPTOR_CLOSURE_HPP
#define PSPP_RANGE_ADAPTOR_CLOSURE_HPP

#include <utility>

namespace pspp
{

struct range_adaptor_closure
{};

template <typename RangeT, c_derived_from<range_adaptor_closure> AdaptorT>
  requires (not std::derived_from<std::remove_cvref_t<RangeT>, range_adaptor_closure>)
pspp_always_inline
constexpr decltype(auto) operator|(RangeT && range, AdaptorT && adaptor) noexcept (noexcept(
  std::forward<AdaptorT>(adaptor)(std::forward<RangeT &&>(range))
))
requires requires {
  std::forward<AdaptorT>(adaptor)(std::forward<RangeT &&>(range));
}
{
  return
    std::forward<AdaptorT>(adaptor)(std::forward<RangeT &&>(range));
}

template <typename LeftAdaptor, typename RightAdaptor>
struct adaptors_pipe : range_adaptor_closure
{
  [[no_unique_address]] LeftAdaptor left;
  [[no_unique_address]] RightAdaptor right;

  template <typename ValT>
  pspp_always_inline
  constexpr auto operator()(ValT && value) noexcept (noexcept (
    std::forward<RightAdaptor>(right)(
      std::forward<LeftAdaptor>(left)(std::forward<ValT>(value))
    )
   ))
   requires requires {
     std::forward<RightAdaptor>(right)(
       std::forward<LeftAdaptor>(left)(std::forward<ValT>(value))
     );
    }
  {
    return std::forward<RightAdaptor>(right)(
      std::forward<LeftAdaptor>(left)(std::forward<ValT>(value))
    );
  }

  template <typename ValT>
  pspp_always_inline
  constexpr auto operator()(ValT && value) const noexcept (noexcept (
    right(left(std::forward<ValT>(value)))
    ))
    requires requires { right(left(std::forward<ValT>(value))); }
  {
    return right(left(std::forward<ValT>(value)));
  }
};

template <typename LeftAdaptorT, typename RightAdaptorT>
adaptors_pipe(LeftAdaptorT &&, RightAdaptorT &&) ->
  adaptors_pipe<LeftAdaptorT, RightAdaptorT>;


template <
  c_derived_from<range_adaptor_closure> LeftAdaptor,
  c_derived_from<range_adaptor_closure> RightAdaptor
>
pspp_always_inline
constexpr auto operator|(LeftAdaptor && left, RightAdaptor && right) noexcept (noexcept (
  std::forward<LeftAdaptor>(left)
  ) and noexcept (
  std::forward<RightAdaptor>(right)
  ))
{
  return adaptors_pipe{
    std::forward<LeftAdaptor>(left),
    std::forward<RightAdaptor>(right)
  };
}

} // namespace pspp

#endif // PSPP_RANGE_ADAPTOR_CLOSURE_HPP
