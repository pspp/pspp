#ifndef PSPP_RANGES_HPP
#define PSPP_RANGES_HPP

#include <utility>
#include <pspp/utility/declaration_macros.hpp>
#include <pspp/ranges_options.hpp>


namespace pspp::inline ranges
{

struct view_t
{};

struct reverse_t
{};

inline constexpr view_t view {};
inline constexpr reverse_t reverse {};

template <typename ... Options, 
					template <typename ...> class Container, typename ... Parameters>
pspp_always_inline constexpr Container<Parameters..., Options...> 
		recreate_with_additional_options(Container<Parameters...> && old)
	noexcept(noexcept(Container<Parameters..., Options...>(std::forward<Container<Parameters...>>(old))))
requires (
			requires { Container<Parameters..., Options...>(std::forward<Container<Parameters...>>(old)); }
		)
{
	return Container<Parameters..., Options...>(std::forward<Container<Parameters...>>(old));
}

template <typename T, typename ... Options>
concept c_with_additional_options_recreatable = requires (T t)
{
	recreate_with_additional_options<Options...>(std::forward<T>(t));
};


template <c_with_additional_options_recreatable<ranges_options::view> Container> 
pspp_always_inline constexpr auto operator|(Container&& old, view_t) 
	noexcept(noexcept(
				recreate_with_additional_options<ranges_options::view>(std::forward<Container>(old))
				))
{
	return recreate_with_additional_options<ranges_options::view>(std::forward<Container>(old));
}

template <c_with_additional_options_recreatable<ranges_options::reverse> Container> 
pspp_always_inline constexpr auto operator|(Container&& old, reverse_t) 
	noexcept(noexcept(
				recreate_with_additional_options<ranges_options::reverse>(std::forward<Container>(old))
				))
{
	return recreate_with_additional_options<ranges_options::reverse>(std::forward<Container>(old));
}

} // namespace pspp::inline ranges

#endif // PSPP_RANGES_HPP
