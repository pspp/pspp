#ifndef PSPP_ADAPTORS_HPP
#define PSPP_ADAPTORS_HPP

#include <pspp/adaptor_pipe_closure.hpp>
#include <pspp/simple/temporary_storage.hpp>

namespace pspp
{
namespace detail
{
template <typename SizeT = std::size_t>
struct get_size_as_fnt : adaptor_pipe_closure
{
  template <typename DataT>
  constexpr SizeT pspp_static_call_const_operator(DataT && data)
  pspp_oneline_auto_noexcept_if_possible(
    size<SizeT>(std::forward<DataT>(data))
  )
};

template <typename ResT>
struct as_fnt : adaptor_pipe_closure
{
  template <typename OldT>
  pspp_always_inline
  constexpr auto pspp_static_call_const_operator(OldT && old)
  pspp_oneline_auto_noexcept_if_possible(static_cast<ResT>(std::forward<OldT>(old)))
};

template <typename AsT>
struct fwd_fnt : adaptor_pipe_closure
{
  template <typename T>
    pspp_always_inline
  constexpr decltype(auto) pspp_static_call_const_operator(T && v) noexcept (noexcept (std::forward<AsT>(v)))
  {
    if constexpr (std::is_rvalue_reference_v<AsT &&>) return std::move(v);
    else return std::forward<T>(v);
  }
};



template <auto t_ind, typename SizeT = std::size_t>
struct get_fnt : adaptor_pipe_closure
{
  static constexpr auto ind = t_ind | as_fnt<SizeT>{};

  template <typename DataT>
  static constexpr bool std_get_able    = requires {std::get<ind>( std::declval<DataT&&>() ); };

  template <typename DataT>
  static constexpr bool member_get_able = requires { std::declval<DataT&&>().template get<ind>(); };

  template <typename DataT>
  static constexpr bool abi_get_able    = requires { get<ind>(std::declval<DataT&&>()); };

  template <typename DataT>
  constexpr static bool is_noexcept() noexcept
  {
         if constexpr (std_get_able<DataT>)
      return noexcept ( std::get<t_ind | as_fnt<SizeT>{}>( std::declval<DataT&&>()) );
    else if constexpr (member_get_able<DataT>)
      return noexcept ( std::declval<DataT&&>().template get<ind>() );
    else if constexpr (abi_get_able<DataT>)
      return noexcept ( get<ind>(std::declval<DataT&&>()) );
  }

  template <typename DataT>
  pspp_always_inline
  constexpr decltype(auto) pspp_static_call_const_operator(DataT && data) noexcept (is_noexcept<DataT &&>())
    requires std_get_able<DataT> or member_get_able<DataT> or abi_get_able<DataT>
  {
         if constexpr (std_get_able<DataT>)    return std::get<ind>(std::forward<DataT>(data));
    else if constexpr (member_get_able<DataT>) return std::forward<DataT>(data).template get<ind>();
    else                                       return get<ind>(std::forward<DataT>(data));
  }
};

template <typename FunT, typename TupleLikeT, std::size_t ... t_is>
constexpr static decltype(auto) apply_impl(FunT && fun, TupleLikeT && tuple, std::index_sequence<t_is...>)
pspp_oneline_auto_noexcept_if_possible(
  std::forward<FunT>(fun)( get_fnt<t_is>{}(std::forward<TupleLikeT>(tuple))... )
)


template <typename ArgTs>
struct apply_with_args_fnt
{
  simple::tmpstorage<ArgTs &&> args;

  constexpr static auto is_ = std::make_index_sequence<size(copy_type<ArgTs>)>{};

  template <typename FunT>
  constexpr decltype(auto) operator()(FunT && fun)
  pspp_oneline_auto_noexcept_if_possible(
    apply_impl(std::forward<FunT>(fun), args.template get<0>(), is_)
  )

  template <typename FunT>
  constexpr decltype(auto) operator()(FunT && fun) const
  pspp_oneline_auto_noexcept_if_possible(
   apply_impl(std::forward<FunT>(fun), args.template get<0>(), is_)
  )

};

struct apply_fnt
{
  template <typename FunT, typename TuppleLikeT>
  constexpr decltype(auto) pspp_static_call_const_operator(FunT && fun, TuppleLikeT && tuple_like)
  pspp_oneline_auto_noexcept_if_possible(
    apply_impl(
      std::forward<FunT>(fun), std::forward<TuppleLikeT>(tuple_like),
      std::make_index_sequence<size(copy_type<TuppleLikeT>)>{}
    )
  )

  template <typename TuppleLikeT>
  constexpr auto pspp_static_call_const_operator(TuppleLikeT && tupple_like)
  pspp_oneline_auto_noexcept(
    apply_with_args_fnt<TuppleLikeT>{.args = {std::forward<TuppleLikeT>(tupple_like)}}
  )
};

template <typename FunT>
struct apply_to_fnt_impl : adaptor_pipe_closure
{
  [[no_unique_address]]
  simple::tmpstorage<FunT &&> fun;

  template <typename TuppleLikeT>
  pspp_always_inline
  constexpr decltype(auto) operator()(TuppleLikeT && tupple_like)
  pspp_oneline_auto_noexcept(
    apply_fnt{}(fun.template get<0>(), std::forward<TuppleLikeT>(tupple_like))
  )
};

struct apply_to_fnt : adaptor_pipe_closure
{
  template <typename FunT>
  pspp_always_inline
  constexpr auto pspp_static_call_const_operator(FunT && fun)
  pspp_oneline_auto_noexcept(
    apply_to_fnt_impl<FunT>{.fun = simple::tmpstorage{std::forward<FunT>(fun)}}
  )
};

template <typename FunFwdT, typename ... ArgTs>
struct call_with_args_fnt : adaptor_pipe_closure
{
  simple::tmpstorage<ArgTs && ...> args;

  template <typename FunT>
    requires simple::is_same_v<FunFwdT, void>
  pspp_always_inline
  constexpr auto operator()(FunT && fun)
  pspp_oneline_auto_noexcept_if_possible(
    apply_fnt{}(std::forward<FunT>(fun), std::move(args))
  )

  template <typename FunT>
    requires simple::is_same_v<FunFwdT, void>
  pspp_always_inline
  constexpr auto operator()(FunT && fun) const
  pspp_oneline_auto_noexcept_if_possible(
    apply_fnt{}(std::forward<FunT>(fun), args)
  )

  template <typename FunT>
    requires simple::is_diff_v<FunFwdT, void>
  pspp_always_inline
  constexpr auto operator()(FunT && fun)
  pspp_oneline_auto_noexcept_if_possible(
    apply_fnt{}(std::forward<FunFwdT>(fun), std::move(args))
  )

  template <typename FunT>
    requires simple::is_diff_v<FunFwdT, void>
  pspp_always_inline
  constexpr auto operator()(FunT && fun) const
  pspp_oneline_auto_noexcept_if_possible(
    apply_fnt{}(std::forward<FunFwdT>(fun), args)
  )
};

struct call_fnt : adaptor_pipe_closure
{
  template <typename ... ArgTs>
    requires (sizeof...(ArgTs) > 0)
  pspp_always_inline
  constexpr auto pspp_static_call_const_operator(ArgTs && ... args)
    pspp_oneline_auto_noexcept(
    (call_with_args_fnt<void, ArgTs...>{.args = simple::tmpstorage{std::forward<ArgTs>(args)...}})
  )

  pspp_always_inline
  constexpr auto pspp_static_call_const_operator() noexcept
  { return call_fnt{}; }

  template <c_callable FunT>
  pspp_always_inline
  constexpr decltype(auto) pspp_static_call_const_operator(FunT && fun)
  pspp_oneline_auto_noexcept(
    std::forward<FunT>(fun)()
  )
};



template <typename FunT>
struct fwd_call_fnt : adaptor_pipe_closure
{
  template <typename ... ArgTs>
  requires (sizeof...(ArgTs) > 0)
  pspp_always_inline
  constexpr auto pspp_static_call_const_operator(ArgTs && ... args)
  pspp_oneline_auto_noexcept(
    (call_with_args_fnt<FunT, ArgTs...>{.args = simple::tmpstorage{std::forward<ArgTs>(args)...}})
  )

  pspp_always_inline
  constexpr auto pspp_static_call_const_operator() noexcept
  { return fwd_call_fnt{}; }

  template <c_callable FunU>
    requires c_same_as<FunT, FunU>
  constexpr decltype(auto) pspp_static_call_const_operator(FunU && fun)
    pspp_oneline_auto_noexcept(
      std::forward<FunT>(fun)()
    )
};

template <typename ... ArgTs>
struct get_call_type_with_fnt : adaptor_pipe_closure
{
  template <typename FunT>
  constexpr static auto operator()(FunT && fun) noexcept
  {
    if constexpr (c_type<FunT>)
      return type<decltype( fun.clean().declval()  | call_fnt{}(!dval<ArgTs &&>...))>;
    else
      return type<decltype(std::forward<FunT>(fun) | call_fnt{}(!dval<ArgTs &&>...))>;
  }
};


template <typename ResT>
struct as_unsafe_fnt : adaptor_pipe_closure
{
  template <typename OldT>
  pspp_always_inline
  constexpr auto pspp_static_call_const_operator(OldT && old) noexcept

  -> ResT
  {
    pspp_execute_and_return_if_can(static_cast<ResT>(std::forward<OldT>(old)));
    else
    if constexpr (std::is_pointer_v < ResT >)
    {
      if constexpr (std::is_pointer_v < pspp_declean(old) >)
        return reinterpret_cast<ResT>(old);
      else
        return reinterpret_cast<ResT>(std::addressof(std::forward<OldT>(old)));
    } else if constexpr (not std::is_rvalue_reference_v < OldT > and
                         std::is_reference_v < ResT > and not
                         std::is_const_v < std::remove_reference_t < ResT >>)
    {
      using const_res_t = std::remove_reference_t <ResT> const &;
      // I wish you will NEVER call this one
      return
        const_cast<ResT>(
          static_cast<const_res_t>(
            *reinterpret_cast<std::remove_reference_t <ResT> const *>(
              std::addressof(std::forward<OldT>(old))
            )
          )
        );
    } else
      return
        *reinterpret_cast<std::remove_reference_t <ResT> *>(std::addressof(std::forward<OldT>(old)));
  }
};

template <typename ResT>
constexpr ResT operator|(auto && value, as_unsafe_fnt<ResT>) noexcept
{ return as_unsafe_fnt<ResT>{}(pspp_fwd(value)); }

enum struct cmp_ops_list
{
  eq, ne, gt, ge, ls, le
};

template <typename DataT>
constexpr auto && forward_like(auto && value) noexcept
{
  if constexpr (std::is_rvalue_reference_v < DataT >)
    return std::forward<pspp_declval(value) && >(value);
  else return std::forward<pspp_declval(value) & >(value);
}

template <typename DataT, c_value_holder OpHolder, c_value_holder CanCast>
struct logical_comp_fnt_impl : adaptor_pipe_closure
{
  DataT m_value;
  [[no_unique_address]] OpHolder _m_op_ind;
  [[no_unique_address]] CanCast _m_can_cast;

  constexpr explicit logical_comp_fnt_impl(DataT val, OpHolder, CanCast) noexcept:
    adaptor_pipe_closure{},
    m_value{val}, _m_op_ind{}, _m_can_cast{}
  {}

  static_val op = OpHolder::value;
  static_val can_cast = CanCast::value;

  [[nodiscard]]
  constexpr bool operator()(auto && other) const noexcept
  requires std::same_as<pspp_declean(other), std::remove_cvref_t < DataT>>
    or ( std::is_pointer_v<pspp_declean(other)> and std::same_as<DataT, std::nullptr_t> )
  {
    using
    enum cmp_ops_list;
    if constexpr (op == eq)
      return pspp_fwd(other) == forward_like<decltype(other)>(m_value);
    else if constexpr (op == ne)
      return pspp_fwd(other) != forward_like<decltype(other)>(m_value);
    else if constexpr (op == gt)
      return pspp_fwd(other) > forward_like<decltype(other)>(m_value);
    else if constexpr (op == ge)
      return pspp_fwd(other) >= forward_like<decltype(other)>(m_value);
    else if constexpr (op == ls)
      return pspp_fwd(other) < forward_like<decltype(other)>(m_value);
    else if constexpr (op == le)
      return pspp_fwd(other) <= forward_like<decltype(other)>(m_value);
    else return false;
  }


  [[nodiscard]]
  constexpr bool operator()(auto && other) const noexcept
  requires can_cast and (not std::same_as < DataT, pspp_declean(other) >)
  {
    return operator()(
      static_cast<DataT>(pspp_fwd(other))
    );
  }

  /**
  * can be called if casting is allowed or DataT is pointer and argument is integral
  * @param other value to compare
  * @return result of comparison
  */
  [[nodiscard]]
  constexpr bool operator()(auto * other) const noexcept
  requires std::is_pointer_v<DataT>
  { return operator()(static_cast<DataT>(other)); }
};

struct can_cast_t {};

template <cmp_ops_list t_op>
struct logical_comp_fnt
{
  constexpr auto pspp_static_call_const_operator(auto && value) noexcept
  {
    return logical_comp_fnt_impl{pspp_fwd(value), val<t_op> , val < false > };
  }

  constexpr auto pspp_static_call_const_operator(auto && value, can_cast_t) noexcept
  {
    return logical_comp_fnt_impl{pspp_fwd(value), val<t_op> , val < true > };
  }
};

} // namespace detail

template <typename SizeT = std::size_t>
inline constexpr detail::get_size_as_fnt<SizeT>    get_size_as{};

inline constexpr auto get_size = get_size_as<>;

template <typename ResT>
inline constexpr detail::as_fnt<ResT>               as{};

template <typename AsT>
inline constexpr detail::fwd_fnt<AsT &&>            fwd {};

template <typename AsT>
inline constexpr detail::fwd_fnt<AsT &&>            _fwd_ {[]{
  static_assert(std::same_as<void, AsT &&>); return detail::fwd_fnt<AsT &&>{};
}()};

template <auto t_ind, typename SizeT = std::size_t>
inline constexpr detail::get_fnt<t_ind, SizeT>      get_at{}; // just get? ambiguous?

inline constexpr detail::apply_fnt                  apply {};
inline constexpr detail::apply_to_fnt               apply_to {};
inline constexpr detail::call_fnt                   call {};

inline constexpr detail::as_fnt<std::size_t>        as_size {};
inline constexpr detail::as_fnt<std::size_t>        as_int  {};

template <typename ... ArgTs>
inline constexpr detail::get_call_type_with_fnt<ArgTs...> get_call_type_with {};
inline constexpr detail::get_call_type_with_fnt<>         get_call_type {};

template <typename FunT>
inline constexpr detail::fwd_call_fnt<FunT>               fwd_call {};

/**
 * THE MOST EVIL FUNC I EVER MADE.
 * Do almoust ANY cast.
 *
 * Cast from rval reference to non-const reference is still not possible
 *
 * @tparam ResT type of result
 */
template <typename ResT>
inline constexpr detail::as_unsafe_fnt<ResT> as_unsafe{};

inline constexpr detail::logical_comp_fnt<detail::cmp_ops_list::eq> is_equal_to{};
inline constexpr detail::logical_comp_fnt<detail::cmp_ops_list::ne> is_not_equal_to{};
inline constexpr detail::logical_comp_fnt<detail::cmp_ops_list::gt> is_greater_then{};
inline constexpr detail::logical_comp_fnt<detail::cmp_ops_list::ge> is_greater_or_equal_then{};
inline constexpr detail::logical_comp_fnt<detail::cmp_ops_list::ls> is_less_then{};
inline constexpr detail::logical_comp_fnt<detail::cmp_ops_list::le> is_less_or_equal_then{};

struct force_par {};
struct deduce_constructor {};

template <typename ResT, typename How = deduce_constructor>
struct construct_t : adaptor_pipe_closure
{
  template <typename ... Args>
  constexpr auto pspp_static_call_const_operator(Args && ... args) noexcept
  {
    if constexpr (pspp_is_same(How, force_par))
      return ResT(std::forward<Args>(args)...);
    else pspp_execute_and_return_if_can(ResT{std::forward<Args>(args)...});
    else pspp_execute_and_return_if_can(ResT(std::forward<Args>(args)...));
    else
    {
      static_assert(false, "could not create ResT from provided arguments");
      return nullptr;
    }
  }
};

/*
struct pass_to_cout_t
{
  template <typename ... ArgTs>
  constexpr auto pspp_static_call_const_operator(ArgTs && ... args)
  {
    (std::cout << ... << args);
  }
};
*/
inline constexpr detail::can_cast_t can_cast{};
template <typename ResT, typename How = deduce_constructor>
inline constexpr construct_t<ResT, How> construct {};

// inline constexpr pass_to_cout_t pass_to_cout {};


template <typename LeftT, typename ResT>
pspp_always_inline
constexpr ResT operator/(LeftT && left, detail::as_fnt<ResT> const asf)
{ return asf(std::forward<LeftT>(left)); }

} // namespace pspp

#endif // PSPP_ADAPTORS_HPP