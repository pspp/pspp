#ifndef PSPP_MULTIRANGE_ITER_HPP
#define PSPP_MULTIRANGE_ITER_HPP

#include <pspp/temporary_storage.hpp>
#include <pspp/custom_struct.hpp>
#include <algorithm>
#include <vector>
#include <ranges>
#include <pspp/simple/temporary_storage.hpp>

#include <pspp/io.hpp>

namespace pspp
{

struct cross_options
{
  bool bidirectional = false;
  bool do_not_have_empty = true;
};

namespace detail
{

enum struct iter_op
{
  incr_, decr_, deref_, comp_
};

inline constexpr struct get_begin_wrapper_t
{
  template <typename ValT>
  constexpr auto operator()(ValT && val) const noexcept
  {
    if constexpr (std::is_pointer_v<std::remove_cvref_t<ValT>>)
      return val->begin();
    else return std::forward<ValT>(val).begin();
  }
} get_begin_wrapper ;

inline constexpr struct get_end_wrapper_t
{
  template <typename ValT>
  constexpr auto operator()(ValT && val) const noexcept
  {
    if constexpr (std::is_pointer_v<std::remove_cvref_t<ValT>>)
      return val->end();
    else return std::forward<ValT>(val).end();
  }
} get_end_wrapper ;

inline constexpr struct get_size_wrapper_t
{
  template <typename ValT>
  constexpr auto operator()(ValT && val) const noexcept
  {
    if constexpr (std::is_pointer_v<std::remove_cvref_t<ValT>>)
      return val->size();
    else return std::forward<ValT>(val).size();
  }
} get_size_wrapper ;

inline constexpr struct get_range_wrapper_t
{
  template <typename ValT>
  constexpr auto operator()(ValT && val) const noexcept
  {
    if constexpr (std::is_rvalue_reference_v<ValT &&>)
      return std::move(val);
    else return &val;
  }
} get_range_wrapper ;

template <typename T>
using cross_view_store_value_t = std::conditional_t<
  std::is_rvalue_reference_v<T &&>,
  std::remove_reference_t<T>, std::remove_reference_t<T> *>;

template <typename ... RangeTs>
using cross_range_storage_type = enumerated_custom_struct_fromv<
  typeseq<cross_view_store_value_t<RangeTs>...>
>;


template<typename ResT, std::size_t t_i>
struct cross_range_el_getter
{
  template<typename InputT>
  static constexpr void incr(InputT &&input) noexcept { ++std::forward<InputT>(input).template get<t_i>(); }

  template<typename InputT>
  static constexpr void decr(InputT &&input) noexcept { --std::forward<InputT>(input).template get<t_i>(); }

  template<typename InputT>
  static constexpr ResT deref(InputT &&input) noexcept { return *std::forward<InputT>(input).template get<t_i>(); }

  template<typename InputT>
  static constexpr bool is_eq(InputT const &begs, InputT const &ends) noexcept
  {
    return begs.template get<t_i>() == ends.template get<t_i>();
  }
};


template <iter_op t_op, typename ValT, typename InputT, typename EndT, std::size_t ... t_is>
constexpr auto get_iter_handlers_map_of_impl(std::index_sequence<t_is...>) noexcept
{
  using enum iter_op;
  using incr_fun_type  = void (*)(InputT &);
  using decr_fun_type  = void (*)(InputT &);
  using deref_fun_type = ValT (*)(InputT &);
  using comp_fun_type  = bool (*)(InputT const &, EndT const &);

  if constexpr (t_op == incr_)
  return std::array<incr_fun_type, sizeof...(t_is)>{
    &cross_range_el_getter<ValT, t_is>::incr...
  };
  else if constexpr (t_op == decr_)
    return std::array<decr_fun_type, sizeof...(t_is)>{
      &cross_range_el_getter<ValT, t_is>::decr...
    };
  else if constexpr (t_op == deref_)
    return std::array<deref_fun_type, sizeof...(t_is)>{
      &cross_range_el_getter<ValT, t_is>::deref...
    };
  else if constexpr (t_op == comp_)
    return std::array<comp_fun_type, sizeof...(t_is)>{
      &cross_range_el_getter<ValT, t_is>::is_eq...
    };
}

template <iter_op t_op, typename ValT, typename InputT, typename EndT, typename ... Ts>
constexpr auto get_iter_handlers_map_of() noexcept
{
  return get_iter_handlers_map_of_impl<t_op, ValT, InputT, EndT>(std::make_index_sequence<sizeof...(Ts)>{});
}

}


template <typename ... Ts>
inline constexpr bool contains_not_lval = not (... and std::is_lvalue_reference_v<Ts>);

template <bool t_enable, typename BegT, typename EndT = BegT>
struct cross_beg_end_holder
{
  BegT begins;
  EndT ends;

  constexpr cross_beg_end_holder() = default;
  constexpr cross_beg_end_holder(cross_beg_end_holder const &) = default;
  constexpr cross_beg_end_holder(cross_beg_end_holder &&) = default;

  constexpr cross_beg_end_holder & operator=(cross_beg_end_holder const &) = default;
  constexpr cross_beg_end_holder & operator=(cross_beg_end_holder &&) = default;

  template <typename ... RestTs>
  constexpr cross_beg_end_holder(custom_struct<RestTs...> & range) noexcept :
    begins{range.template for_each<BegT>(detail::get_begin_wrapper)},
    ends{range.template for_each<EndT>(detail::get_end_wrapper)}
  {}
};


template <typename ... Ts>
using cross_range_value_type = std::conditional_t<
  contains_not_lval<Ts...>,
  std::remove_cvref_t<inline_head<Ts...>>, inline_head<Ts...>
>;

template <cross_options t_opts, typename ... RangeTs>
struct cross_range_view
{
  static_size count = sizeof...(RangeTs);

  detail::cross_range_storage_type<RangeTs && ...> storage;

  constexpr cross_range_view() = default;
  constexpr cross_range_view(cross_range_view const &) = default;
  constexpr cross_range_view(cross_range_view &&) = default;

  constexpr cross_range_view & operator=(cross_range_view const &) noexcept = default;
  constexpr cross_range_view & operator=(cross_range_view &&) noexcept = default;

  template <typename FirstT, typename ... RestTs>
  constexpr cross_range_view(FirstT && first, RestTs && ... rest) :
    storage{detail::get_range_wrapper(std::forward<FirstT>(first)),
            detail::get_range_wrapper(std::forward<RestTs>(rest))...},
    begends{storage}
  {}

  template <typename FirstT, typename ... RestTs>
  constexpr cross_range_view(value_holder<t_opts>, FirstT && first, RestTs && ... rest) :
    storage{detail::get_range_wrapper(std::forward<FirstT>(first)),
            detail::get_range_wrapper(std::forward<RestTs>(rest))...},
    begends{storage}
  {}

  using underlying_data_type = cross_range_value_type<decltype(*(std::declval<RangeTs&&>().begin()))...>;
  using iterators_storage_type =
    decltype(
      make_custom_struct(std::declval<RangeTs&&>().begin()...)
    );
  using sentinel_storage_type =
    decltype(
      make_custom_struct(std::declval<RangeTs&&>().end()...)
    );

  cross_beg_end_holder<t_opts.bidirectional, iterators_storage_type, sentinel_storage_type> begends;

  struct sentinel
  {
    sentinel_storage_type ends;
    constexpr sentinel() noexcept = default;
    constexpr sentinel(sentinel const &) = default;
    constexpr sentinel(sentinel &&) = default;
    constexpr sentinel & operator=(sentinel const &) noexcept = default;
    constexpr sentinel & operator=(sentinel &&) noexcept = default;

    constexpr sentinel(RangeTs & ... ranges) :
      ends{std::forward<RangeTs>(ranges).end()...}
    {}
  };

  struct biiterator
  {
    using range_type = cross_range_view;
    using difference_type = std::ptrdiff_t;
    using sentinel_type = sentinel;
    using cross_value_type = underlying_data_type;

    using reference_type = cross_value_type &;

    range_type * parent;
    iterators_storage_type begins;
    std::size_t ind;

    constexpr biiterator() noexcept = default;
    constexpr biiterator(biiterator const &) noexcept = default;
    constexpr biiterator(biiterator &&) noexcept = default;

    constexpr biiterator& operator=(biiterator && ) noexcept = default;
    constexpr biiterator& operator=(biiterator const & ) noexcept = default;

    constexpr biiterator(std::bool_constant<true>, range_type * parent_) noexcept :
      parent{parent_},
      begins{parent_->begends.begins},
      ind{0}
    {}

    constexpr biiterator(std::bool_constant<false>, range_type * parent_) noexcept :
      parent{parent_},
      begins{parent_->begends.ends},
      ind{count - 1}
    {}

    constexpr biiterator & operator++() noexcept
    {
      ++begins[ind];
      //println("++: was: {}", parent->begends.ends[ind] == iters[ind]);
      ind += parent->begends.ends[ind] == begins[ind];
      return *this;
    }

    constexpr biiterator operator++(int) noexcept
    {
      biiterator cp = *this;
      ++begins[ind];
      // println("++: was: {}\t(int)", parent->begends.ends[ind] == begins[ind]);
      ind += parent->begends.ends[ind] == begins[ind];

      return cp;
    }

    constexpr biiterator & operator--() noexcept
    {
      --begins[ind];
      // println("--: was: {}", parent->begends.begins[ind] -1 * (ind == 0) == begins[ind]);
      ind -= parent->begends.begins[ind] - 1 * (ind == 0) == begins[ind];
      return *this;
    }

    constexpr biiterator operator--(int) noexcept
    {
      biiterator cp = *this;

      --begins[ind];
      println("--: was: {}\t(int)", (parent->begends.begins[ind] - 1 * (ind == 0)) == begins[ind]);
      ind -= parent->begends.begins[ind] - 1 * (ind == 0) == begins[ind];

      return cp;
    }

    constexpr reference_type operator*() noexcept
    {
      //println("deref: ({}) {}", ind, ::pspp::iterator{begins[ind]});
      return *begins[ind]; }

    constexpr reference_type operator*() const noexcept
    {
      //println("deref: ({}) {}", ind, ::pspp::iterator{begins[ind]});
      return *begins[ind];
    }

    constexpr bool operator==(biiterator const & other) const noexcept
    {
      //println("==: {{{}}} vs {{{}}}", iters, other.iters);
      return begins[other.ind] == other.begins[other.ind]/* or iters[other.ind] + 1 == other.iters[other.ind];*/;
    }

  };

  struct iterator
  {
    using range_type = cross_range_view;
    using difference_type = std::ptrdiff_t;
    using sentinel_type = sentinel;
    using cross_value_type = underlying_data_type;


    iterators_storage_type begins;

    static constexpr auto incr_funs =
      detail::get_iter_handlers_map_of<
        detail::iter_op::incr_, cross_value_type, iterators_storage_type, sentinel_storage_type, RangeTs...
      >();

    static constexpr auto deref_funs =
      detail::get_iter_handlers_map_of<
        detail::iter_op::deref_, cross_value_type, iterators_storage_type, sentinel_storage_type, RangeTs...
      >();

    static constexpr auto decr_funs =
      detail::get_iter_handlers_map_of<
        detail::iter_op::decr_, cross_value_type, iterators_storage_type, sentinel_storage_type, RangeTs...
      >();

    static constexpr auto comp_funs =
      detail::get_iter_handlers_map_of<
        detail::iter_op::comp_, cross_value_type, iterators_storage_type, sentinel_storage_type, RangeTs...
      >();

    // to satisfy std::ranges::range
    mutable std::size_t ind = 0;

    constexpr iterator() noexcept = default;
    constexpr iterator(RangeTs & ... ranges) :
      begins{std::forward<RangeTs>(ranges).begin()...}
    {}

    constexpr iterator & operator++() noexcept
    {
      incr_funs[ind](begins);
      return *this;
    }

    constexpr iterator operator++(int) noexcept
    {
      iterator copy = *this;
      incr_funs[ind](begins);

      return copy;
    }

    constexpr underlying_data_type operator*() noexcept
    { return deref_funs[ind](begins); }

    constexpr underlying_data_type operator*() const noexcept
    { return deref_funs[ind](begins); }


    constexpr bool operator==(sentinel const & ends) const noexcept
    {
      if (comp_funs[ind](begins, ends.ends))
        ++ind;

      return ind == sizeof...(RangeTs);
    }
  };

  constexpr iterator begin() noexcept
    requires (not t_opts.bidirectional)
  {
    return
    [this]<std::size_t ... t_is>(std::index_sequence<t_is...>)
    {
      return iterator{
        storage.template get<t_is>()...
      };
    }(std::make_index_sequence<sizeof...(RangeTs)>{});
  }

  constexpr sentinel end() noexcept
    requires (not t_opts.bidirectional)
  {
    return
      [this]<std::size_t ... t_is>(std::index_sequence<t_is...>)
      {
        return sentinel{
          storage.template get<t_is>()...
        };
      }(std::make_index_sequence<sizeof...(RangeTs)>{});
  }

  constexpr biiterator begin() noexcept
    requires (t_opts.bidirectional)
  {
    return biiterator{std::true_type{}, this};
  }

  constexpr biiterator end() noexcept
  requires (t_opts.bidirectional)
  {
    return biiterator{std::false_type{}, this};
  }
};

template <typename T>
concept c_cross_range_iter = requires {
  requires c_complex_vttarget<typename T::range_type, cross_range_view>;
};


template <typename FirstT, typename ... RangeTs>
cross_range_view(FirstT &&, RangeTs &&...) -> cross_range_view<cross_options{}, FirstT &&, RangeTs && ...>;

template <cross_options t_opts, typename FirstT, typename ... RangeTs>
cross_range_view(value_holder<t_opts>, FirstT &&, RangeTs &&...) -> cross_range_view<t_opts, FirstT &&, RangeTs && ...>;


template <typename T>
concept c_simple_range =
  requires (T t){
    requires std::is_pointer_v<decltype(t.begin())>;
  };

template <typename BegT, typename EndT>
struct cross_beg_end_holder<false, BegT, EndT>
{
  constexpr cross_beg_end_holder(auto ...) {}
};



template <cross_options t_opts, c_simple_range ... RangeTs>
struct simple_cross_range
{
  detail::cross_range_storage_type<RangeTs && ...> storage;

  using value_type = std::remove_cvref_t<decltype(*(detail::get_begin_wrapper(storage.template get<0>())))>;
  static_size count = sizeof...(RangeTs);
  using iterator_storage_type = std::array<value_type *, count>;

  cross_beg_end_holder<t_opts.bidirectional, iterator_storage_type> begends;

  constexpr simple_cross_range() noexcept = default;
  constexpr simple_cross_range(simple_cross_range const &) noexcept = default;
  constexpr simple_cross_range(simple_cross_range &&) noexcept = default;

  constexpr simple_cross_range & operator=(simple_cross_range const &) noexcept = default;
  constexpr simple_cross_range & operator=(simple_cross_range &&) noexcept = default;

  template <typename FirstT, typename ... RestTs>
    requires (not c_complex_vttarget<FirstT, simple_cross_range>)
  constexpr simple_cross_range(FirstT && first, RestTs && ... rest) :
    storage{detail::get_range_wrapper(std::forward<FirstT>(first)),
            detail::get_range_wrapper(std::forward<RestTs>(rest))...},
    begends{storage}
  {}

  template <typename FirstT, typename ... RestTs>
  requires (not c_complex_vttarget<FirstT, simple_cross_range>)
  constexpr simple_cross_range(value_holder<t_opts>, FirstT && first, RestTs && ... rest) :
    storage{detail::get_range_wrapper(std::forward<FirstT>(first)),
            detail::get_range_wrapper(std::forward<RestTs>(rest))...},
    begends{storage}
  {}

  template <bool t_is_const>
  struct biiterator
  {
    using range_type = simple_cross_range;
    using difference_type = std::ptrdiff_t;
    using cross_value_type = value_type;

    using reference_type = std::conditional_t<t_is_const, value_type const &, value_type &>;
    using const_reference_type = value_type const &;

    range_type * parent;
    std::size_t ind = 0;
    iterator_storage_type iters;

    constexpr biiterator() noexcept = default;
    constexpr biiterator(biiterator const &) noexcept = default;
    constexpr biiterator(biiterator &&) noexcept = default;

    constexpr biiterator& operator=(biiterator && ) noexcept = default;
    constexpr biiterator& operator=(biiterator const & ) noexcept = default;

    constexpr biiterator(std::bool_constant<true>, range_type * parent_) noexcept :
      parent{parent_},
      iters{parent_->begends.begins},
      ind{0}
    {}

    constexpr biiterator(std::bool_constant<false>, range_type * parent_) noexcept :
      parent{parent_},
      iters{parent_->begends.ends},
      ind{count - 1}
    {}

    constexpr biiterator & operator++() noexcept
    {
      ++iters[ind];
      //println("++: was: {}", parent->begends.ends[ind] == iters[ind]);
      ind += parent->begends.ends[ind] == iters[ind];
      return *this;
    }

    constexpr biiterator operator++(int) noexcept
    {
      biiterator cp = *this;
      ++iters[ind];
     // println("++: was: {}\t(int)", parent->begends.ends[ind] == iters[ind]);
      ind += parent->begends.ends[ind] == iters[ind];

      return cp;
    }

    constexpr biiterator & operator--() noexcept
    {
      --iters[ind];
     // println("--: was: {}", parent->begends.begins[ind] -1 * (ind == 0) == iters[ind]);
      ind -= parent->begends.begins[ind] - 1 * (ind == 0) == iters[ind];
      return *this;
    }

    constexpr biiterator operator--(int) noexcept
    {
      biiterator cp = *this;

      --iters[ind];
      println("--: was: {}\t(int)", (parent->begends.begins[ind] - 1 * (ind == 0)) == iters[ind]);
      ind -= parent->begends.begins[ind] - 1 * (ind == 0) == iters[ind];

      return cp;
    }


    constexpr reference_type operator*() noexcept
    {
      //println("deref: ({}) {}", ind, ::pspp::iterator{iters[ind]});
      return *iters[ind]; }

    constexpr reference_type operator*() const noexcept
    {
      //println("deref: ({}) {}", ind, ::pspp::iterator{iters[ind]});
      return *iters[ind];
    }

    constexpr bool operator==(biiterator const & other) const noexcept
    {
      //println("==: {{{}}} vs {{{}}}", iters, other.iters);
      return iters[other.ind] == other.iters[other.ind]/* or iters[other.ind] + 1 == other.iters[other.ind];*/;
    }
  };

  struct iterator
  {
    using range_type = simple_cross_range;
    using difference_type = std::ptrdiff_t;
    using cross_value_type = value_type;

    iterator_storage_type begins;
    mutable int ind = 0;

    constexpr iterator() noexcept = default;
    constexpr iterator(iterator const &) noexcept = default;
    constexpr iterator(iterator &&) noexcept = default;

    constexpr iterator& operator=(iterator && ) noexcept = default;
    constexpr iterator& operator=(iterator const & ) noexcept = default;

    template <bool t_is_begin, typename ... BeginTs>
    constexpr iterator(std::bool_constant<t_is_begin>, BeginTs ... begs) noexcept :
      begins{std::forward<BeginTs>(begs)...},
      ind{conditional_v<t_is_begin, 0, sizeof...(RangeTs) - 1>}
    {}


    constexpr iterator & operator++() noexcept
    { ++begins[ind]; return *this; }

    constexpr iterator operator++(int) noexcept
    {
      iterator tmp = *this;
      ++begins[ind];
      return tmp;
    }

    constexpr value_type & operator*() noexcept
    { return *begins[ind]; }

    constexpr value_type & operator*() const noexcept
    { return *begins[ind]; }

    constexpr bool operator==(iterator const & other) const noexcept
    {
      if (begins[ind] == other.begins[ind]) ++ind;
      return ind == count;
    }
  };

  constexpr iterator begin() noexcept requires (not t_opts.bidirectional)
  {
    return [this]<std::size_t ... t_is>(std::index_sequence<t_is...>)
    {
      return iterator{std::true_type{}, detail::get_begin_wrapper(storage.template get<t_is>())...};
    }(std::make_index_sequence<count>{});
  }

  constexpr iterator end() noexcept requires (not t_opts.bidirectional)
  {
    return [this]<std::size_t ... t_is>(std::index_sequence<t_is...>)
    {
      return iterator{std::false_type{}, detail::get_end_wrapper(storage.template get<t_is>())...};
    }(std::make_index_sequence<count>{});
  }

  constexpr biiterator<false> begin() noexcept requires (t_opts.bidirectional)
  { return biiterator<false>{std::true_type{}, this}; }

  constexpr biiterator<false> end() noexcept requires (t_opts.bidirectional)
  { return biiterator<false>{std::false_type{}, this}; }
};


template <typename FirstT, typename ... RangeTs>
simple_cross_range(FirstT &&, RangeTs && ...) -> simple_cross_range<cross_options{}, FirstT &&, RangeTs &&...>;

template <cross_options t_opts, typename FirstT, typename ... RangeTs>
simple_cross_range(value_holder<t_opts>, FirstT &&, RangeTs && ...) -> simple_cross_range<t_opts, FirstT &&, RangeTs &&...>;

template <cross_options t_opts, typename ... RangeTs>
simple_cross_range(simple_cross_range<t_opts, RangeTs...>) -> simple_cross_range<t_opts, RangeTs ...>;


template <typename T>
concept c_simple_cross_range_iter = requires {
  requires c_complex_vttarget<typename T::range_type, simple_cross_range>;
};

} // namespace pspp

template <pspp::c_cross_range_iter CrossRangeIterT>
struct std::indirectly_readable_traits<CrossRangeIterT>
{ using value_type = typename CrossRangeIterT::cross_value_type; };

template <pspp::c_simple_cross_range_iter CrossRangeIterT>
struct std::indirectly_readable_traits<CrossRangeIterT>
{ using value_type = typename CrossRangeIterT::cross_value_type; };


constexpr int foo()
{
  std::array tmp {1, 2, 3};
  std::array a2 { 1, 2, 3};

  auto tmp2 = pspp::cross_range_view{pspp::val<pspp::cross_options{.bidirectional = true}>, auto{tmp}, auto{a2}};
  auto tmp3 = tmp2.begin();
  tmp3 = tmp2.end();

  return std::ranges::fold_left(pspp::simple_cross_range{tmp, a2}, 0, pspp::plus);
}


#endif // PSPP_MULTIRANGE_ITER_HPP
