#ifndef PSPP_INDEXED_ARGS_HPP
#define PSPP_INDEXED_ARGS_HPP

#include <pspp/type_sequence.hpp>
#include <pspp/custom_struct.hpp>
#include <pspp/functional.hpp>

#include <pspp/useful/sequence.hpp>

namespace pspp
{

template <auto t_index, typename ... DataTs>
struct indexed_args
{
	static_val index = t_index;
	enumerated_custom_struct_from<type_sequence<DataTs...>> args;

	constexpr indexed_args(value_holder<t_index>, DataTs && ... args_) noexcept :
		args{pspp_fwd(args_)...}
	{}

	constexpr indexed_args() = default;
};

template <auto t_index, typename ... DataTs>
indexed_args(value_holder<t_index>, DataTs && ...) -> indexed_args<t_index, DataTs...>;

template <auto t_index, typename ... DataTs>
constexpr auto make_args_for(DataTs && ... args) 
{
	return indexed_args{val<t_index>, pspp_fwd(args)...};
};

template <auto t_index, auto t_func>
struct indexed_func
{
	static_val index = t_index;
	static_val value = t_func;

	consteval indexed_func(value_holder<t_index>, decltype(t_func)) {}
	consteval indexed_func() = default;
};

template <auto t_index>
indexed_func(value_holder<t_index>, auto t_func) ->
	indexed_func<t_index, decltype(t_func){}>;


template <indexed_func ... t_funcs>
struct indexed_funcs_manager
{
	static constexpr auto funcs = vtypeseq<t_funcs...>;

	template <auto t_index>
	inline static constexpr auto checker = 
		[]<typename T>{ return std::same_as<value_holder<T::index>, value_holder<t_index>>; };


	template <auto t_index>
	inline static constexpr auto func_at = funcs | 
		get_last_like_or<checker<t_index>, decltype(non_callable_v)> | unwrap | get_holded_value;

	
	template <auto t_index>
	static constexpr auto call(auto && ... args) noexcept
	{
		static constexpr auto i = 
			typeseq<decltype(args)...> | remove_ref
			                           | get_last_index_of_like<[]<typename T>{
															    	return std::same_as<value_holder<T::index>, value_holder<t_index>>;
															   }>;
		return get_ell_at<i>(pspp_fwd(args)...).args. template apply<func_at<t_index>>();
	}
};

} // namespace pspp

#endif // PSPP_INDEXED_ARGS_HPP
